-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2021 at 08:33 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `integra_community`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_set_next_position_and_all` (IN `spo_id` BIGINT)  NO SQL
BEGIN

DECLARE position varchar(10);
DECLARE spcount_left int;
DECLARE spcount_right int;
DECLARE posi_next varchar(10);
DECLARE default_posi varchar(10);

set @position ='';
set  @spcount_left=0;
set  @spcount_right=0;
set  @posi_next='';
set  @default_posi='';
 select ssm.position,ssm.sponsed_count_left,ssm.sponsed_count_right, ssm.position_next,ssm.position_downline_setup_default into 
 @position,@spcount_left,@spcount_right,@posi_next,@default_posi from `s_members` ssm where ssm.reg_id=spo_id;
if @default_posi = 'zigzag' then
		if @posi_next='left' then 
			set @new_position_next='right'; 
		end if;	
		if @posi_next='right' then 
			set @new_position_next='left'; 
		end if; 
 end if;
 if @default_posi = 'left' then
	if @spcount_right>=2 and @spcount_left>=1 then
		set @new_position_next='left';
	
	else
		if @posi_next='left' then 
			set @new_position_next='right'; 
		end if;	
		if @posi_next='right' then 
			set @new_position_next='left'; 
		end if; 
	end if;
 end if;
 if @default_posi = 'right' then
	if @spcount_left>=2 and @spcount_right>=1 then
		set @new_position_next='right';
	
	else
		if @posi_next='left' then 
			set @new_position_next='right'; 
		end if;	
		if @posi_next='right' then 
			set @new_position_next='left'; 
		end if; 
	end if;
 end if;
 
 update `s_members` set position_next=@new_position_next where reg_id=spo_id;
 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_achievement`
--

CREATE TABLE `m_achievement` (
  `id` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `min_bvleft` int(11) NOT NULL,
  `min_bvright` int(11) NOT NULL,
  `matching` int(11) NOT NULL,
  `cash_amt` decimal(18,2) NOT NULL,
  `min_reference` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_achievement`
--

INSERT INTO `m_achievement` (`id`, `rank`, `min_bvleft`, `min_bvright`, `matching`, `cash_amt`, `min_reference`) VALUES
(1, 1, 15, 15, 15, '1000.00', 4),
(2, 2, 40, 40, 40, '3000.00', 6),
(3, 3, 100, 100, 100, '1000.00', 8),
(4, 4, 400, 400, 400, '3000.00', 12),
(5, 5, 1500, 1500, 1500, '6000.00', 16),
(6, 6, 6000, 6000, 6000, '20000.00', 20),
(7, 7, 25000, 25000, 25000, '60000.00', 26),
(8, 8, 100000, 100000, 100000, '20000000.00', 32),
(9, 9, 500000, 500000, 500000, '60000000.00', 40),
(10, 10, 1500000, 1500000, 1500000, '200000000.00', 50),
(11, 11, 4000000, 4000000, 4000000, '600000000.00', 65);

-- --------------------------------------------------------

--
-- Table structure for table `m_levels`
--

CREATE TABLE `m_levels` (
  `id` int(11) NOT NULL,
  `level_amt` decimal(10,2) NOT NULL,
  `level_amt_renew` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_levels`
--

INSERT INTO `m_levels` (`id`, `level_amt`, `level_amt_renew`) VALUES
(1, '50.00', '50.00'),
(2, '30.00', '30.00'),
(3, '15.00', '15.00'),
(4, '10.00', '10.00'),
(5, '9.00', '9.00'),
(6, '8.00', '8.00'),
(7, '7.00', '7.00'),
(8, '6.00', '6.00'),
(9, '5.00', '5.00'),
(10, '4.00', '4.00'),
(11, '3.00', '3.00'),
(12, '2.00', '2.00');

-- --------------------------------------------------------

--
-- Table structure for table `m_products`
--

CREATE TABLE `m_products` (
  `id` int(11) NOT NULL,
  `pro_name` varchar(100) DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `spec` varchar(500) NOT NULL,
  `price_in_rupee` decimal(10,3) NOT NULL,
  `renewel_charge` decimal(10,3) NOT NULL,
  `version` decimal(5,1) NOT NULL,
  `make` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_products`
--

INSERT INTO `m_products` (`id`, `pro_name`, `description`, `spec`, `price_in_rupee`, `renewel_charge`, `version`, `make`) VALUES
(1, 'Save App The Personal accounting app', 'A small user friendly accounting app in Android to keep all the accounts, whether it is personal or small business to manage ', 'Android app ', '1500.000', '200.000', '1.5', '2021');

-- --------------------------------------------------------

--
-- Table structure for table `m_settings_value`
--

CREATE TABLE `m_settings_value` (
  `id` int(11) NOT NULL,
  `bv` int(11) NOT NULL DEFAULT 1,
  `referal_commission` decimal(10,3) NOT NULL DEFAULT 0.000,
  `one_bv_required_amount` decimal(10,3) NOT NULL DEFAULT 0.000,
  `one_bv_equalant_value` decimal(10,3) NOT NULL DEFAULT 0.000,
  `daily_ceiling` decimal(10,3) NOT NULL DEFAULT 0.000,
  `Percent_tds` decimal(10,3) NOT NULL DEFAULT 0.000,
  `Percent_tds_nonpan` decimal(7,2) NOT NULL DEFAULT 20.00,
  `percent_admin_charges` decimal(10,3) NOT NULL DEFAULT 0.000,
  `renewal_notification_days` int(11) DEFAULT 15,
  `bill_prefix` varchar(5) DEFAULT NULL,
  `cgst` decimal(4,2) NOT NULL DEFAULT 1.50,
  `sgst` decimal(4,2) NOT NULL DEFAULT 1.50,
  `igst` decimal(4,2) NOT NULL DEFAULT 3.00,
  `buffer_count` tinyint(4) NOT NULL DEFAULT 5
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_settings_value`
--

INSERT INTO `m_settings_value` (`id`, `bv`, `referal_commission`, `one_bv_required_amount`, `one_bv_equalant_value`, `daily_ceiling`, `Percent_tds`, `Percent_tds_nonpan`, `percent_admin_charges`, `renewal_notification_days`, `bill_prefix`, `cgst`, `sgst`, `igst`, `buffer_count`) VALUES
(1, 1, '250.000', '1500.000', '100.000', '4000.000', '3.750', '20.00', '10.000', 15, NULL, '1.50', '1.50', '3.00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `m_state`
--

CREATE TABLE `m_state` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `state_name` varchar(50) NOT NULL,
  `state_code` varchar(10) NOT NULL,
  `cess1_name` varchar(15) DEFAULT NULL,
  `cess1_percent` decimal(5,2) NOT NULL DEFAULT 0.00,
  `cess2_name` varchar(15) DEFAULT NULL,
  `cess2_percent` decimal(5,2) NOT NULL DEFAULT 0.00,
  `wef_date` date DEFAULT NULL,
  `countryid` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_state`
--

INSERT INTO `m_state` (`id`, `code`, `state_name`, `state_code`, `cess1_name`, `cess1_percent`, `cess2_name`, `cess2_percent`, `wef_date`, `countryid`) VALUES
(1, '37	', 'Andhra Pradesh', 'AD', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(2, '12', 'Arunachal Pradesh', 'AR', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(3, '18', 'Assam', 'AS', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(4, '10', 'Bihar', 'BR', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(5, '22', 'Chhattisgarh', 'CG', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(6, '30', 'Goa', 'GA', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(7, '24', 'Gujarat', 'GJ', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(8, '06', 'Haryana', 'HR', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(9, '02', 'Himachal Pradesh', 'HP', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(10, '20', 'Jharkhand', 'JH', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(11, '29', 'Karnataka', 'KA', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(12, '32', 'Kerala', 'KL', 'KFC', '1.00', NULL, '0.00', '2020-10-12', 1),
(13, '23', 'Madhya Pradesh', 'MP', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(14, '27', 'Maharashtra', 'MH', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(15, '14', 'Manipur', 'MN', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(16, '17', 'Meghalaya', 'ML', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(17, '15', 'Mizoram', 'MZ', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(18, '13', 'Nagaland', 'NL', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(19, '21', 'Odisha', 'OD', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(20, '03', 'Punjab', 'PB', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(21, '08', 'Rajasthan', 'RJ', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(22, '11', 'Sikkim', 'SK', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(23, '33', 'Tamil Nadu', 'TN', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(24, '36', 'Telangana', 'TS', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(25, '16', 'Tripura', 'TR', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(26, '09', 'Uttar Pradesh', 'UP', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(27, '05', 'Uttarakhand', 'UK', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(28, '19', 'West Bengal', 'WB', NULL, '0.00', NULL, '0.00', '2020-10-12', 1),
(29, '35', 'Andaman and Nicobar Islands', 'AN', NULL, '0.00', NULL, '0.00', '2020-08-28', 1),
(30, '04', 'Chandigarh', 'CH', NULL, '0.00', NULL, '0.00', '2020-10-08', 1),
(31, '26', 'Dadra and Nagar Haveli and Daman and Diu', 'UNHDD', NULL, '0.00', NULL, '0.00', '2020-10-06', 1),
(32, '07', 'Delhi', 'DL', NULL, '0.00', '', '0.00', '2020-10-06', 1),
(33, '01', 'Jammu and Kashmir', 'JK', NULL, '0.00', '', '0.00', '2020-10-06', 1),
(34, '38', 'Ladakh', 'LA', NULL, '0.00', '', '0.00', '2020-10-06', 1),
(35, '31', 'Lakshadweep', 'LD', NULL, '0.00', '', '0.00', '2020-10-06', 1),
(36, '34', 'Puducherry', 'PY', NULL, '0.00', '', '0.00', '2020-10-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `mobileno` varchar(14) NOT NULL,
  `email` varchar(30) NOT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `emp_code` varchar(10) DEFAULT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(35) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `leaving_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`id`, `fullname`, `mobileno`, `email`, `designation`, `status`, `emp_code`, `username`, `password`, `join_date`, `leaving_date`) VALUES
(1, 'Integra', '2223334445', 'mail@integraerp.in', 'director', 1, 'emp001', 'integra', 'integra', '2021-02-01', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `s_achievement_bonus_track`
--

CREATE TABLE `s_achievement_bonus_track` (
  `id` bigint(20) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `achivement_rankid` int(11) NOT NULL,
  `cash_amt` decimal(13,2) NOT NULL DEFAULT 0.00,
  `matching` int(11) NOT NULL,
  `generate_date` datetime NOT NULL DEFAULT current_timestamp(),
  `matching_date` date DEFAULT NULL,
  `payout_status` tinyint(4) NOT NULL DEFAULT 0,
  `process_by` int(11) DEFAULT NULL,
  `closing_status` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `s_bank_trans`
--

CREATE TABLE `s_bank_trans` (
  `id` bigint(20) NOT NULL,
  `payment_category` varchar(30) NOT NULL COMMENT '1,daily,2 weekly,3 monthly',
  `reg_id` bigint(20) NOT NULL,
  `account_holder_name` varchar(45) DEFAULT NULL,
  `account_number` varchar(20) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `IFSC_code` varchar(20) DEFAULT NULL,
  `Branch` varchar(30) DEFAULT NULL,
  `amount` decimal(12,2) NOT NULL,
  `pan_no` varchar(20) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `company_bank_name` varchar(50) DEFAULT NULL,
  `company_account_no` varchar(50) DEFAULT NULL,
  `Company_account` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `s_binary_summary`
--

CREATE TABLE `s_binary_summary` (
  `id` bigint(20) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `total_binary_left` int(11) NOT NULL DEFAULT 0,
  `total_binary_right` int(11) NOT NULL DEFAULT 0,
  `prev_carry_left` int(11) NOT NULL DEFAULT 0,
  `prev_carry_right` int(11) NOT NULL DEFAULT 0,
  `closing_status` int(11) NOT NULL DEFAULT 0,
  `generate_date` datetime NOT NULL DEFAULT current_timestamp(),
  `tail_left` int(11) NOT NULL DEFAULT 0,
  `tail_right` int(11) NOT NULL DEFAULT 0,
  `matching_value` int(11) NOT NULL DEFAULT 0,
  `matching_date` date DEFAULT NULL,
  `after_matchingcarry_left` int(11) NOT NULL DEFAULT 0,
  `after_matchingcarry_right` int(11) NOT NULL DEFAULT 0,
  `payout` decimal(10,2) NOT NULL DEFAULT 0.00,
  `washout` decimal(10,2) NOT NULL DEFAULT 0.00,
  `admcharges` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tdscharges` decimal(10,2) NOT NULL DEFAULT 0.00,
  `netpayment` decimal(10,2) NOT NULL DEFAULT 0.00,
  `hold_staus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 hold, 1 release',
  `cash_release_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_binary_summary`
--

INSERT INTO `s_binary_summary` (`id`, `reg_id`, `reg_code`, `total_binary_left`, `total_binary_right`, `prev_carry_left`, `prev_carry_right`, `closing_status`, `generate_date`, `tail_left`, `tail_right`, `matching_value`, `matching_date`, `after_matchingcarry_left`, `after_matchingcarry_right`, `payout`, `washout`, `admcharges`, `tdscharges`, `netpayment`, `hold_staus`, `cash_release_date`) VALUES
(1, 1, '111111111', 1, 1, 0, 0, 1, '2021-02-25 16:39:14', 0, 0, 1, '2021-02-26', 0, 0, '100.00', '0.00', '10.00', '3.38', '86.62', 0, NULL),
(2, 2, '111222333', 2, 0, 0, 0, 1, '2021-02-25 16:39:18', 0, 0, 0, '2021-02-26', 2, 0, '0.00', '0.00', '0.00', '0.00', '0.00', 0, NULL),
(3, 1, '111111111', 0, 2, 0, 0, 1, '2021-02-25 16:39:22', 0, 0, 0, '2021-02-28', 0, 2, '0.00', '0.00', '0.00', '0.00', '0.00', 0, NULL),
(4, 4, '840678158', 1, 0, 0, 0, 1, '2021-02-25 16:39:25', 0, 0, 0, '2021-02-28', 1, 0, '0.00', '0.00', '0.00', '0.00', '0.00', 0, NULL);

--
-- Triggers `s_binary_summary`
--
DELIMITER $$
CREATE TRIGGER `s_binary_summary_before_update` BEFORE UPDATE ON `s_binary_summary` FOR EACH ROW BEGIN
DECLARE posi varchar(10);
DECLARE bi_matched int;
DECLARE total_binary_left int;
DECLARE total_binary_right int;
DECLARE matching_value int;
DECLARE matching_var int;
DECLARE after_matchingcarry_left int;
DECLARE after_matchingcarry_right int;

DECLARE bv_val int;
DECLARE dailycel decimal(8,2);
DECLARE updatestatus tinyint;
DECLARE tds decimal(8,2);
DECLARE tds_pan decimal(8,2);
DECLARE tds_nonpan decimal(8,2);
DECLARE admch decimal(8,2);
DECLARE payout decimal(8,2);
DECLARE netpayout decimal(8,2);

set @posi="";
set @bi_matched=0;
set @total_binary_left=OLD.total_binary_left;
set @total_binary_right=OLd.total_binary_right;
set @matching_value =0;
set @after_matchingcarry_left =0;
set @after_matchingcarry_right=0;
set @matching_var=0;

set @bv_val=0;
set @dailycel=0.00;
set  @updatestatus=0; 
set  @tds=0.00;
set  @tds_pan=0.00;
set  @tds_nonpan=0.00;
set  @admch=0.00;
set  @payout=0.00;
set @netpayout=0.00;

IF NEW.closing_status=1 and OLD.closing_status=0 then
	SELECT ss.`position`, ss.`binary_matched` into @posi,@bi_matched FROM `s_members` ss WHERE ss.`reg_id`=OLD.reg_id;
	if @bi_matched=0 then
		if @posi='left' then	
			if @total_binary_left>=1 and @total_binary_right>=2 then
				set @total_binary_right=@total_binary_right-1;
				set @matching_var=@total_binary_left-@total_binary_right;
				set NEW.tail_left=0;
				set NEW.tail_right=1;
				if @matching_var>0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = @matching_var;
					set @after_matchingcarry_right=0;
					
				end if;
				if @matching_var<0 then
					set @matching_value =@total_binary_left;				
					set @after_matchingcarry_left =0 ;
					set @after_matchingcarry_right= -1* @matching_var;
				end if;
				if @matching_var=0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = 0;
					set @after_matchingcarry_right=0;
				end if;
			else
				set @matching_value =0;
				set @after_matchingcarry_left =@total_binary_left;
				set @after_matchingcarry_right=@total_binary_right;
				set NEW.tail_left=0;
				set NEW.tail_right=0;
			end if;
		end if;
		if @posi='right' then
			if @total_binary_right>=1 and @total_binary_left>=2 then
				set @total_binary_left=@total_binary_left-1;
				set @matching_var=@total_binary_left-@total_binary_right;
				
				set NEW.tail_left=1;
				set NEW.tail_right=0;
				if @matching_var>0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = @matching_var;
					set @after_matchingcarry_right=0;
				end if;
				if @matching_var<0 then
					set @matching_value =@total_binary_left;				
					set @after_matchingcarry_left =0 ;
					set @after_matchingcarry_right= -1* @matching_var;
				end if;
				if @matching_var=0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = 0;
					set @after_matchingcarry_right=0;
				end if;
			else
				set @matching_value =0;
				set @after_matchingcarry_left =@total_binary_left;
				set @after_matchingcarry_right=@total_binary_right;
				set NEW.tail_left=0;
				set NEW.tail_right=0;
			end if;
		end if;
		
	else
			if @total_binary_right>=1 and @total_binary_left>=1  then
			set @matching_var=@total_binary_left-@total_binary_right;
				set NEW.tail_left=0;
				set NEW.tail_right=0;
				if @matching_var>0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = @matching_var;
					set @after_matchingcarry_right=0;
				end if;
				if @matching_var<0 then
					set @matching_value =@total_binary_left;				
					set @after_matchingcarry_left =0 ;
					set @after_matchingcarry_right= -1* @matching_var;
				end if;
				if @matching_var=0 then
					set @matching_value =@total_binary_right;				
					set @after_matchingcarry_left = 0;
					set @after_matchingcarry_right=0;
				end if;
			else
				set @matching_value =0;
				set @after_matchingcarry_left =@total_binary_left;
				set @after_matchingcarry_right=@total_binary_right;
			end if;
	end if;
	set NEW.matching_value=@matching_value;
	set NEW.matching_date=curdate();
	set NEW.matching_value=@matching_value;
	set NEW.after_matchingcarry_left=@after_matchingcarry_left;
	set NEW.after_matchingcarry_right=@after_matchingcarry_right;
	SELECT mm.`one_bv_equalant_value`,mm.`daily_ceiling`,mm.`Percent_tds`,mm.Percent_tds_nonpan, mm.`percent_admin_charges` 
	into @bv_val, @dailycel, @tds_pan, @tds_nonpan, @admch FROM `m_settings_value` mm WHERE 1; 
SELECT sm.`updatestatus` into @updatestatus from  s_members sm where  sm.`reg_id`=OLD.reg_id;

if @updatestatus=4 then
	set @tds=@tds_pan;
	set NEW.hold_staus=0;
else
	set @tds=@tds_nonpan;
	set NEW.hold_staus=2;
end if;	
	
	
	
	
	set @payout=@matching_value * @bv_val;
	if @payout > @dailycel then
		set NEW.payout=@dailycel;
		set NEW.washout=@payout-@dailycel;
	else 
		set NEW.payout=@payout;
		set NEW.washout=0;
	end if;
	set NEW.admcharges=(NEW.payout*@admch)/100;
	set @netpayout=NEW.payout-NEW.admcharges;
	set NEW.tdscharges=(@netpayout*@tds)/100;
	set NEW.netpayment=@netpayout-NEW.tdscharges;
	
	update s_members set  `carry_left`=@after_matchingcarry_left,`carry_right`=@after_matchingcarry_right,
	`binary_matched`=`binary_matched`+@matching_value,`binary_amt`=`binary_amt`+NEW.payout,
	`binary_lastgn_date`=curdate(),`binary_amt_pre`=`binary_amt` where `reg_id`=OLD.reg_id;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_binary_track`
--

CREATE TABLE `s_binary_track` (
  `id` bigint(20) NOT NULL,
  `reg_id` bigint(11) DEFAULT NULL,
  `reg_code` varchar(10) DEFAULT NULL,
  `pur_amt` decimal(12,3) NOT NULL,
  `binary_left` int(11) NOT NULL,
  `binary_right` int(11) NOT NULL,
  `doner_id` bigint(11) NOT NULL,
  `doner_code` varchar(10) NOT NULL,
  `bill_no` bigint(20) NOT NULL,
  `generate_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_binary_track`
--

INSERT INTO `s_binary_track` (`id`, `reg_id`, `reg_code`, `pur_amt`, `binary_left`, `binary_right`, `doner_id`, `doner_code`, `bill_no`, `generate_date`) VALUES
(1, 1, '111111111', '1500.000', 0, 1, 2, '111222333', 1, '2021-02-08'),
(2, 1, '111111111', '1500.000', 1, 0, 3, '111333444', 2, '2021-02-08'),
(3, 2, '111222333', '1500.000', 1, 0, 4, '840678158', 3, '2021-02-18'),
(4, 1, '111111111', '1500.000', 0, 1, 4, '840678158', 3, '2021-02-18'),
(5, 4, '840678158', '1500.000', 1, 0, 5, '399485234', 4, '2021-02-18'),
(6, 2, '111222333', '1500.000', 1, 0, 5, '399485234', 4, '2021-02-18'),
(7, 1, '111111111', '1500.000', 0, 1, 5, '399485234', 4, '2021-02-18');

--
-- Triggers `s_binary_track`
--
DELIMITER $$
CREATE TRIGGER `s_binary_track_after_insert` AFTER INSERT ON `s_binary_track` FOR EACH ROW BEGIN
DECLARE reg_id bigint;
DECLARE max_id bigint;
DECLARE tot_left int;
DECLARE tot_right int;
DECLARE rowid bigint;
DECLARE after_matchingcarry_left int;
DECLARE after_matchingcarry_right int;
set @reg_id=0;
set @max_id=0;
set @tot_left  =0;
set @tot_right =0;
set @rowid=0;
set @after_matchingcarry_left =0;
set @after_matchingcarry_right=0;
select sb.reg_id,sb.id into @reg_id,@rowid from s_binary_summary sb where sb.reg_id=NEW.reg_id and sb.generate_date=NEW.generate_date;
if @reg_id=0 then
	select max(bn.id) into @max_id from s_binary_summary bn where bn.reg_id=NEW.reg_id;
	select an.`after_matchingcarry_left`, an.`after_matchingcarry_right` into @after_matchingcarry_left,@after_matchingcarry_right
	 from s_binary_summary an where an.id=@max_id;
	set @tot_left  =@after_matchingcarry_left+NEW.binary_left;
	set @tot_right =@after_matchingcarry_right+NEW.binary_right; 
	insert into s_binary_summary(`reg_id`, `reg_code`, `total_binary_left`, `total_binary_right`, `prev_carry_left`, `prev_carry_right`, `closing_status`, `generate_date`)
	 values(NEW.reg_id,NEW.reg_code,@tot_left,@tot_right,@after_matchingcarry_left,@after_matchingcarry_right,0,NEW.generate_date);
end if;
if @reg_id > 0 then

update s_binary_summary  s set s.`total_binary_left`=s.`total_binary_left`+ NEW.binary_left,
	s.`total_binary_right`=s.`total_binary_right`+NEW.binary_right where s.id=@rowid;
end if;
update s_members k set k.binary_left=k.binary_left+NEW.binary_left,
k.binary_right=k.binary_right+NEW.binary_right where k.reg_id=NEW.reg_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_free_app_users`
--

CREATE TABLE `s_free_app_users` (
  `id` bigint(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `profile_image` varchar(300) NOT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `currency` varchar(15) DEFAULT 'rupee',
  `join_date` datetime NOT NULL DEFAULT current_timestamp(),
  `activation_date` datetime DEFAULT current_timestamp(),
  `activation_key` varchar(6) DEFAULT NULL,
  `join_source` varchar(20) DEFAULT NULL,
  `used_link_for_registration` varchar(150) DEFAULT NULL,
  `sp_reg_id` bigint(20) NOT NULL,
  `device_id` varchar(500) NOT NULL,
  `sp_reg_code` varchar(10) DEFAULT NULL,
  `default_lang` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `encr_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_free_app_users`
--

INSERT INTO `s_free_app_users` (`id`, `full_name`, `reg_code`, `country_id`, `state_id`, `mobile`, `profile_image`, `email_id`, `currency`, `join_date`, `activation_date`, `activation_key`, `join_source`, `used_link_for_registration`, `sp_reg_id`, `device_id`, `sp_reg_code`, `default_lang`, `username`, `encr_password`) VALUES
(1, 'Integra Community', '111111111', 1, 12, '9048484687', '', 'mail@integraerp.in', 'rupee', '2021-02-01 11:53:31', '2021-02-01 11:53:31', '1234', 'link', NULL, 0, '', '0', 'eng', 'varsha', '4786f3282f04de5b5c7317c490c6d922'),
(2, 'Varsha', '111222333', 1, 12, '8943716725', '', 'varsha.unni08@gmail.com', 'rupee', '2021-02-04 17:18:52', '2021-02-04 17:18:52', '1235', 'link', NULL, 1, '', '111111111', 'eng', 'v11', 'f56f0edb77f9b8510011dc802169953c'),
(3, 'Prasanth Integra', '111333444', 1, 32, '9048484600', '', 'info@integraerp.in', 'rupee', '2021-02-08 14:44:48', '2021-02-08 14:44:48', '1256', 'link', NULL, 1, '', '111111111', 'eng', 'pr', 'pr'),
(4, 'Unnikrishnan', '840678158', 1, 12, '2332323212', '', 'unni@syzto.com', 'rupee', '2021-02-16 14:12:47', '2021-02-16 14:12:47', NULL, NULL, NULL, 2, '', '111222333', 'English', 'info', 'caf9b6b99962bf5c2264824231d7a40c'),
(5, 'Malavika Unni', '399485234', 1, 12, '2332323212', '', 'aaaaaaaaaa@gmail.com', 'rupee', '2021-02-16 14:16:25', '2021-02-16 14:16:25', NULL, NULL, NULL, 2, '', '111222333', 'English', 'aaa', '4124bc0a9335c27f086f24ba207a4912'),
(6, 'ammu', '192429421', 1, 12, '1111111111', '', 'ammm@gmail.com', 'rupee', '2021-02-16 14:31:03', '2021-02-16 14:31:03', NULL, NULL, NULL, 2, '', '111222333', 'English', 'ssssddd', '81b073de9370ea873f548e31b8adc081'),
(7, 'sssssssss', '683685053', 1, 12, '2323232323', '', 'ss@gghj.com', 'rupee', '2021-02-16 14:46:26', '2021-02-16 14:46:26', NULL, NULL, NULL, 2, '', '111222333', 'English', 'FW123456', '8f51ef3b9040a527832bebba66e286ac'),
(8, 'aaa', '682332054', 1, 12, '3545454456', '', 'aaaa@gmail.com', 'rupee', '2021-02-16 15:08:51', '2021-02-16 15:08:51', NULL, NULL, NULL, 2, '', '111222333', 'English', '2323', '149815eb972b3c370dee3b89d645ae14'),
(10, 'varsha bb', '986582602', 1, 11, '3434343434', '', 'bbb@gmail.com', 'rupee', '2021-02-17 14:33:27', '2021-02-17 14:33:27', NULL, NULL, NULL, 2, '', '111222333', 'English', 'wewe', '2a7d544ccb742bd155e55c796de8e511'),
(11, 'Sruthy', '229157737', 1, 4, '3434343434', '', 'sr@gfgdg.com', 'rupee', '2021-02-17 14:43:35', '2021-02-17 14:43:35', NULL, NULL, NULL, 2, '', '111222333', 'English', 'qwqw', 'e110fb45bc4f7cc5d367b06bfbc8e5c3');

--
-- Triggers `s_free_app_users`
--
DELIMITER $$
CREATE TRIGGER `s_free_app_users_after_update` AFTER UPDATE ON `s_free_app_users` FOR EACH ROW BEGIN
	DECLARE regid bigint;
	set regid=0;
	select f.reg_id into @regid from s_members f where f.reg_id=OLD.id;
	IF @regid>0 then
		IF OLD.encr_password<>NEW.encr_password then
		update s_members set encr_password=NEW.encr_password where reg_id=@regid;
		END IF;
		IF OLD.username<>NEW.username then
		update s_members set username=NEW.username where reg_id=@regid;
		END IF;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_level_track`
--

CREATE TABLE `s_level_track` (
  `id` bigint(20) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `level_no` int(11) NOT NULL,
  `level_amt` int(11) NOT NULL,
  `level_doner_id` bigint(20) NOT NULL,
  `level_doner_code` varchar(10) NOT NULL,
  `generate_date` datetime NOT NULL DEFAULT current_timestamp(),
  `process_status` int(11) NOT NULL DEFAULT 0,
  `process_date` date DEFAULT NULL,
  `processed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `s_members`
--

CREATE TABLE `s_members` (
  `id` bigint(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `currency` varchar(15) DEFAULT NULL,
  `join_date` datetime NOT NULL DEFAULT current_timestamp(),
  `activation_date` datetime DEFAULT current_timestamp(),
  `activation_key` varchar(6) DEFAULT NULL,
  `join_source` varchar(20) DEFAULT NULL,
  `used_link_for_registration` varchar(150) DEFAULT NULL,
  `sponser_reg_id` bigint(20) DEFAULT NULL,
  `sponser_reg_code` varchar(10) DEFAULT NULL,
  `parent_reg_id` bigint(20) NOT NULL,
  `parent_reg_code` varchar(10) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `encr_password` varchar(50) DEFAULT NULL,
  `default_lang` varchar(20) NOT NULL,
  `sponsed_count_left` int(11) NOT NULL,
  `sponsed_count_right` int(11) NOT NULL,
  `position` varchar(10) DEFAULT 'Left',
  `position_downline_setup_default` varchar(10) NOT NULL DEFAULT 'zigzag' COMMENT 'left,right,zigzag',
  `position_next` varchar(10) DEFAULT NULL,
  `bank_account_no` varchar(20) NOT NULL,
  `bank_account_name` varchar(50) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `branch_name` varchar(50) NOT NULL,
  `pan_card` varchar(20) DEFAULT NULL,
  `binary_left` int(11) NOT NULL DEFAULT 0,
  `binary_right` int(11) NOT NULL DEFAULT 0,
  `binary_matched` int(11) NOT NULL DEFAULT 0,
  `carry_left` int(11) NOT NULL DEFAULT 0,
  `carry_right` int(11) NOT NULL DEFAULT 0,
  `binary_amt` decimal(18,3) DEFAULT 0.000,
  `binary_amt_pre` decimal(18,3) DEFAULT 0.000,
  `binary_lastgn_date` date DEFAULT NULL,
  `referral_commission_amt` decimal(18,3) DEFAULT 0.000,
  `referral_commission_amt_pre` decimal(18,3) DEFAULT 0.000,
  `referral_commission_lastgn_date` date DEFAULT NULL,
  `level_amt` decimal(18,3) DEFAULT 0.000,
  `level_amt_pre` decimal(18,3) DEFAULT 0.000,
  `level_lastgn_date` date DEFAULT NULL,
  `achievement_amt` decimal(18,3) DEFAULT 0.000,
  `achievement_amt_pre` decimal(18,3) DEFAULT 0.000,
  `achievement_lastgn_date` date DEFAULT NULL,
  `member_cancel` tinyint(4) NOT NULL DEFAULT 0,
  `member_terminated` tinyint(4) NOT NULL DEFAULT 0,
  `profile_photo` varchar(150) DEFAULT NULL,
  `pan_no` varchar(15) DEFAULT NULL,
  `pan_photo` varchar(150) DEFAULT NULL,
  `adhar_no` varchar(15) DEFAULT NULL,
  `adhar_photo` varchar(150) DEFAULT NULL,
  `bank_checkleaf_photo` varchar(150) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `ifsc` varchar(20) NOT NULL,
  `updatestatus` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0- not commit,1-pending,2-not complete,3-reject,4- approve',
  `remark_varification` varchar(100) DEFAULT 'approval pending please stay on line',
  `last_varified_by` int(11) DEFAULT NULL,
  `last_varified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_members`
--

INSERT INTO `s_members` (`id`, `full_name`, `reg_id`, `reg_code`, `country_id`, `state_id`, `mobile`, `email_id`, `currency`, `join_date`, `activation_date`, `activation_key`, `join_source`, `used_link_for_registration`, `sponser_reg_id`, `sponser_reg_code`, `parent_reg_id`, `parent_reg_code`, `username`, `encr_password`, `default_lang`, `sponsed_count_left`, `sponsed_count_right`, `position`, `position_downline_setup_default`, `position_next`, `bank_account_no`, `bank_account_name`, `bank_name`, `branch_name`, `pan_card`, `binary_left`, `binary_right`, `binary_matched`, `carry_left`, `carry_right`, `binary_amt`, `binary_amt_pre`, `binary_lastgn_date`, `referral_commission_amt`, `referral_commission_amt_pre`, `referral_commission_lastgn_date`, `level_amt`, `level_amt_pre`, `level_lastgn_date`, `achievement_amt`, `achievement_amt_pre`, `achievement_lastgn_date`, `member_cancel`, `member_terminated`, `profile_photo`, `pan_no`, `pan_photo`, `adhar_no`, `adhar_photo`, `bank_checkleaf_photo`, `branch`, `ifsc`, `updatestatus`, `remark_varification`, `last_varified_by`, `last_varified_date`) VALUES
(1, 'Integra', 1, '111111111', 1, 12, '9048484687', 'mail@integgraerp.in', 'rupee', '2021-02-04 11:47:38', '2021-02-04 11:47:38', '1234', 'link', NULL, 0, NULL, 0, '', 'varsha', '4786f3282f04de5b5c7317c490c6d922', 'eng', 0, 0, 'Left', 'zigzag', 'left', '', '', '', '', NULL, 3, 12, 408, 0, 2, '29800.000', '29800.000', '2021-02-26', NULL, NULL, NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, 0, 0, '372-avatar.png', '3445454466', '832-download_(1).jpg', '', '136-download.jpg', '174-screencapture-localhost-fiawin-admin-index-php-sa-office-turnover-singleday-copy-2021-01-27-13_19_06.png', '', '', 0, 'approval pending please stay on line', NULL, NULL),
(6, 'Varsha', 2, '111222333', 1, 12, '8943716725', 'varsha.unni08@gmail.com', '1', '2021-02-04 17:18:52', '2021-02-04 17:18:52', '1235', 'link', NULL, 1, '111111111', 1, '111111111', 'v11', 'f56f0edb77f9b8510011dc802169953c', '', 0, 0, 'right', 'zigzag', 'right', '', '', '', '', NULL, 9, 0, 217, 2, 0, '19200.000', '19200.000', '2021-02-26', '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', 0, NULL, NULL, NULL),
(7, 'Prasanth Integra', 3, '111333444', 1, 12, '9048484600', 'info@integraerp.in', 'rupee', '2021-02-08 14:44:48', '2021-02-08 14:44:48', '1256', 'link', NULL, 1, '111111111', 1, '111111111', 'pr', 'pr', '', 0, 0, 'left', 'zigzag', 'left', '', '', '', '', NULL, 0, 0, 0, 0, 0, '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, 0, 0, NULL, '3445454466', NULL, NULL, NULL, '', '', '', 0, NULL, NULL, NULL),
(15, 'Unnikrishnan', 4, '840678158', 1, 12, '2332323212', 'unni@syzto.com', 'rupee', '2021-02-16 14:12:47', '2021-02-16 14:12:47', NULL, NULL, NULL, 2, '111222333', 2, '111222333', 'info', 'caf9b6b99962bf5c2264824231d7a40c', '', 0, 0, 'left', 'zigzag', 'left', '', '', '', '', NULL, 5, 0, 287, 1, 0, '17300.000', '17300.000', '2021-02-26', '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', 0, NULL, NULL, NULL),
(16, 'Malavika Unni', 5, '399485234', 1, 12, '2332323212', 'aaaaaaaaaa@gmail.com', 'rupee', '2021-02-16 14:16:25', '2021-02-16 14:16:25', NULL, NULL, NULL, 2, '111222333', 4, '840678158', 'aaa', '4124bc0a9335c27f086f24ba207a4912', '', 0, 0, 'left', 'zigzag', NULL, '', '', '', '', NULL, 0, 0, 0, 0, 0, '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, '0.000', '0.000', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '', '', '', 0, NULL, NULL, NULL);

--
-- Triggers `s_members`
--
DELIMITER $$
CREATE TRIGGER `s_members_after_update` AFTER UPDATE ON `s_members` FOR EACH ROW BEGIN
-- add achievement details to achievement rank table when bv updated
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `s_members_before_insert` BEFORE INSERT ON `s_members` FOR EACH ROW BEGIN

DECLARE ct int;
DECLARE parent_reg_id bigint;
DECLARE new_parentid bigint;
DECLARE default_position varchar(10);
DECLARE position_last varchar(10);

DECLARE spo_position varchar(10);
DECLARE position varchar(10);
DECLARE parent_id bigint;
DECLARE position_inner_first tinyint;
DECLARE position_outer_second tinyint;
DECLARE position_inner_third tinyint;

DECLARE new_parentregcode varchar(10);
set @ct=1;
select NEW.`sponser_reg_id` into @parent_reg_id;
set @new_parentid=@parent_reg_id;
set @position='left';
 select m1.position_next  into @position  from s_members m1 where m1.reg_id=NEW.`sponser_reg_id`;

select m4.`reg_id`,m4.`reg_code` into @new_parentid,@new_parentregcode  FROM `s_members` m4  WHERE m4.`parent_reg_id`=@parent_reg_id and m4.`position`=@position;
while @ct>0 do
SELECT count(m5.`reg_id`)  into @ct  FROM `s_members`  m5 WHERE m5.`parent_reg_id`=@parent_reg_id and m5.`position`=@position;
if @ct>0 then
select m6.`reg_id`,m6.`reg_code` into @new_parentid,@new_parentregcode  FROM `s_members` m6  WHERE m6.`parent_reg_id`=@parent_reg_id and m6.`position`=@position;
set @parent_reg_id=@new_parentid;
end if;
end while;
set NEW.`parent_reg_id`=@new_parentid;
set NEW.`parent_reg_code`=@new_parentregcode;
set NEW.`position`=@position;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_payment_summary`
--

CREATE TABLE `s_payment_summary` (
  `id` int(10) NOT NULL,
  `reg_id` int(10) NOT NULL,
  `reg_code` int(20) NOT NULL,
  `sum_amt` decimal(10,0) NOT NULL,
  `type` varchar(4) DEFAULT NULL COMMENT 'BN-binary ,LI-level income,RI-referal income,AB-achivement bonus',
  `process_status` tinyint(4) NOT NULL DEFAULT 0,
  `admin_charge` decimal(11,0) NOT NULL,
  `TDS` decimal(11,0) NOT NULL,
  `net_pay` decimal(11,0) NOT NULL,
  `transfer_status` tinyint(4) NOT NULL,
  `generate_date` date NOT NULL,
  `process_date` date NOT NULL,
  `transfer_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s_payment_summary`
--

INSERT INTO `s_payment_summary` (`id`, `reg_id`, `reg_code`, `sum_amt`, `type`, `process_status`, `admin_charge`, `TDS`, `net_pay`, `transfer_status`, `generate_date`, `process_date`, `transfer_date`) VALUES
(1, 3, 33333, '0', '0', 0, '0', '0', '0', 0, '2021-02-15', '2021-02-16', '0000-00-00');

--
-- Triggers `s_payment_summary`
--
DELIMITER $$
CREATE TRIGGER `s_payment_summary_before_update` BEFORE UPDATE ON `s_payment_summary` FOR EACH ROW BEGIN
DECLARE updatestatus tinyint;
DECLARE tds decimal(8,2);
DECLARE tds_pan decimal(8,2);
DECLARE tds_nonpan decimal(8,2);
DECLARE admch decimal(8,2);
DECLARE netpayout decimal(8,2);

set  @updatestatus=0; 
set  @tds=0.00;
set  @tds_pan=0.00;
set  @tds_nonpan=0.00;
set  @admch=0.00;
set  @netpayout=0.00;

SELECT mm.`Percent_tds`,mm.	Percent_tds_nonpan,mm.`percent_admin_charges`  into @tds_pan, @tds_nonpan, @admch FROM `m_settings_value` mm WHERE 1; 
SELECT sm.`updatestatus` into @updatestatus from  s_members sm where  sm.`reg_id`=OLD.reg_id;

if @updatestatus=4 then
	set @tds=@tds_pan;
	set NEW.transfer_status=0;
else
	set @tds=@tds_nonpan;
	set NEW.transfer_status=2;
end if;
	set @payout=NEW.sum_amt;
	
	set NEW.admin_charge=(NEW.sum_amt*@admch)/100;
	set @netpayout=NEW.sum_amt-NEW.admin_charge;
	set NEW.TDS=(@netpayout*@tds)/100;
	set NEW.net_pay=@netpayout-NEW.TDS;
	
	if OLD.type='LI' then
		update s_members set  `level_amt_pre`=`level_amt`,`level_amt`=`level_amt`+ NEW.sum_amt,
		`level_lastgn_date`=curdate() where `reg_id`=OLD.reg_id;

	end if;
	if OLD.type='RI' then
		update s_members set  `referral_commission_amt_pre`=`referral_commission_amt`,
		`referral_commission_amt`=`referral_commission_amt`+ NEW.sum_amt,
		`referral_commission_lastgn_date`=curdate() where `reg_id`=OLD.reg_id;

	end if;
	if OLD.type='AB' then
		update s_members set  `achievement_amt_pre`=`achievement_amt`,
		`achievement_amt`=`achievement_amt`+ NEW.sum_amt,`achievement_lastgn_date`=curdate()  
		where `reg_id`=OLD.reg_id;
	end if;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_referal_income`
--

CREATE TABLE `s_referal_income` (
  `id` bigint(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `ref_inc_doner_id` bigint(20) NOT NULL,
  `ref_income` decimal(10,2) NOT NULL DEFAULT 0.00,
  `generate_date` datetime NOT NULL DEFAULT current_timestamp(),
  `process_status` int(11) NOT NULL DEFAULT 0,
  `process_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_referal_income`
--

INSERT INTO `s_referal_income` (`id`, `product_id`, `reg_id`, `reg_code`, `ref_inc_doner_id`, `ref_income`, `generate_date`, `process_status`, `process_date`) VALUES
(2, 1, 1, '111111111', 3, '250.00', '2021-02-18 00:00:00', 0, NULL),
(3, 1, 1, '111111111', 2, '250.00', '2021-02-18 00:00:00', 0, NULL),
(4, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(5, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(6, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(7, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(8, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(9, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(10, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(11, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(12, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(13, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(14, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(15, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(16, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(17, 1, 1, '111111111', 2, '250.00', '2021-02-18 00:00:00', 0, NULL),
(18, 1, 1, '111111111', 3, '250.00', '2021-02-18 00:00:00', 0, NULL),
(19, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(20, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(21, 1, 1, '111111111', 2, '250.00', '2021-02-18 00:00:00', 0, NULL),
(22, 1, 1, '111111111', 3, '250.00', '2021-02-18 00:00:00', 0, NULL),
(23, 1, 2, '111222333', 4, '250.00', '2021-02-18 00:00:00', 0, NULL),
(24, 1, 2, '111222333', 5, '250.00', '2021-02-18 00:00:00', 0, NULL),
(25, 1, 1, '111111111', 2, '250.00', '2021-02-08 14:03:57', 0, NULL),
(26, 1, 1, '111111111', 3, '250.00', '2021-02-08 15:06:27', 0, NULL),
(27, 1, 2, '111222333', 4, '250.00', '2021-02-18 14:43:48', 0, NULL),
(28, 1, 2, '111222333', 5, '250.00', '2021-02-18 14:43:48', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `s_sales_info`
--

CREATE TABLE `s_sales_info` (
  `id` bigint(20) NOT NULL,
  `billno_prefix` varchar(3) NOT NULL DEFAULT 'E-',
  `bill_no` bigint(20) NOT NULL,
  `reg_id` bigint(20) NOT NULL,
  `reg_code` varchar(10) NOT NULL,
  `product_id` int(11) NOT NULL,
  `sales_type` varchar(10) DEFAULT 'sales' COMMENT 'sales and renewal',
  `sales_date` datetime NOT NULL DEFAULT current_timestamp(),
  `expe_date` date DEFAULT NULL,
  `amt` decimal(12,3) NOT NULL DEFAULT 0.000,
  `binary_val` int(11) NOT NULL DEFAULT 0,
  `currency` varchar(15) NOT NULL,
  `ex_rate` decimal(10,3) NOT NULL DEFAULT 0.000,
  `rupee_convertion_val` decimal(18,2) NOT NULL DEFAULT 0.00,
  `sponser_reg_id` bigint(20) NOT NULL,
  `sponser_reg_code` varchar(10) NOT NULL,
  `referal_commission_rupee` decimal(10,3) NOT NULL DEFAULT 0.000,
  `sponser_currency` varchar(15) NOT NULL,
  `sponser_cur_ex_rate` decimal(10,3) NOT NULL DEFAULT 0.000,
  `sponser_conversion_value` decimal(10,3) NOT NULL DEFAULT 0.000,
  `binary_gen_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-not generate,1 generate'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `s_sales_info`
--

INSERT INTO `s_sales_info` (`id`, `billno_prefix`, `bill_no`, `reg_id`, `reg_code`, `product_id`, `sales_type`, `sales_date`, `expe_date`, `amt`, `binary_val`, `currency`, `ex_rate`, `rupee_convertion_val`, `sponser_reg_id`, `sponser_reg_code`, `referal_commission_rupee`, `sponser_currency`, `sponser_cur_ex_rate`, `sponser_conversion_value`, `binary_gen_status`) VALUES
(1, 'E-', 1, 2, '111222333', 1, 'sales', '2021-02-08 14:03:57', '2022-02-07', '1500.000', 1, 'rupee', '1.000', '1500.00', 1, '111111111', '250.000', 'rupee', '1.000', '250.000', 1),
(2, 'E-', 2, 3, '111333444', 1, 'sales', '2021-02-08 15:06:27', '2022-02-07', '1500.000', 1, 'rupee', '1.000', '1500.00', 1, '111111111', '250.000', 'rupee', '1.000', '250.000', 1),
(20, 'E-', 3, 4, '840678158', 1, 'sales', '2021-02-18 14:43:48', '2023-02-18', '1500.000', 1, 'rupee', '1.000', '1500.00', 2, '111222333', '0.000', 'rupee', '1.000', '1500.000', 1),
(21, 'E-', 4, 5, '399485234', 1, 'sales', '2021-02-18 14:43:48', '2023-02-18', '1500.000', 1, 'rupee', '1.000', '1500.00', 2, '111222333', '0.000', 'rupee', '1.000', '1500.000', 1);

--
-- Triggers `s_sales_info`
--
DELIMITER $$
CREATE TRIGGER `s_sales_info_after_insert` AFTER INSERT ON `s_sales_info` FOR EACH ROW BEGIN
DECLARE  reg_id bigint;
DECLARE  reg_id_members bigint;
DECLARE  position varchar(10);
DECLARE  posi_after_next varchar(10);

set  @reg_id=NEW.reg_id;
set  @reg_id_members=0;
if @reg_id>0 then
select rr.id into @reg_id_members from s_free_app_users rr where rr.id=@reg_id;
if @reg_id_members=@reg_id then
insert into `s_members`(`full_name`, `reg_id`, `reg_code`, `country_id`,`state_id`, `mobile`, `email_id`, `currency`, `join_date`, `activation_date`, 
`activation_key`, `join_source`, `used_link_for_registration`, `sponser_reg_id`, `sponser_reg_code`,`username`, `encr_password`)
 select `full_name`,NEW.reg_id,  `reg_code`, `country_id`,`state_id`, `mobile`, `email_id`, `currency`, `join_date`, 
`activation_date`, `activation_key`, `join_source`, `used_link_for_registration`, `sp_reg_id`, `sp_reg_code`,`username`, `encr_password`  from s_free_app_users where id=@reg_id_members;
END IF;
END IF;
/*
find the default position settings of sponser and set the next position value
 select m1.position_next  into @position  from s_members m1 where m1.reg_id=NEW.`sponser_reg_id`;
 if @position='left' then 
 set @posi_after_next='right'; 
 end if;
  if @position='right' then 
  set @posi_after_next='left'; 
  end if;
 update s_members set position_next=@posi_after_next where reg_id=NEW.`sponser_reg_id`;
 */
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `s_sales_info_after_update` AFTER UPDATE ON `s_sales_info` FOR EACH ROW BEGIN
DECLARE binary_gen_status tinyint;
		DECLARE parent_reg_id bigint;
		DECLARE reg_id bigint;
		DECLARE sponser_reg_id bigint;
		DECLARE parent_reg_code varchar(10);
		DECLARE sponser_reg_code varchar(10);
		DECLARE position_mem varchar(10);
		DECLARE position varchar(10);
		DECLARE price_per_bv decimal(10,2);
		DECLARE price_bv decimal(10,2);

		DECLARE ct int;
		DECLARE left_gain_bv int;
		DECLARE right_gain_bv int;
		DECLARE mem_terminated tinyint;
		DECLARE mem_cancel tinyint;
		
		DECLARE gl_ref_com tinyint;
		DECLARE ref_com tinyint;
        DECLARE posi_sp varchar(10);
		set @posi_sp='';
		set @gl_ref_com=0;
		set @ref_com=0;
		
		set @mem_cancel=0;
		set @mem_terminated=0;
		
		select OLD.`reg_id` into @reg_id;

		set @position_mem="";
		set @position="";

		set @ct=1;
		select ss.position into @position_mem from `s_members` ss where ss.reg_id=@reg_id;
	 if NEW.binary_gen_status=1 AND OLD.binary_gen_status=0 and OLD.`sales_type`='sales' then
		 
	 set @left_gain_bv=0;
	set @right_gain_bv=0;
	while @ct>0 do
				if @position_mem="left" then
					set @left_gain_bv=OLD.binary_val;
					set @right_gain_bv=0;
				elseif @position_mem='right' then
				   set @left_gain_bv=0;
					set @right_gain_bv=OLD.binary_val;
				end if;
				SELECT dis.`parent_reg_id`, dis.`sponser_reg_id`,dis.`parent_reg_code`, dis.`sponser_reg_code`  into @parent_reg_id, @sponser_reg_id,@parent_reg_code, @sponser_reg_code  FROM `s_members` dis WHERE dis.`reg_id`=@reg_id;
				SELECT dis.`id`,dis.`position`,dis.`member_cancel`,dis.`member_terminated`   into  @codedis,@position,@mem_cancel,@mem_terminated  FROM `s_members` dis WHERE dis.`reg_id`=@parent_reg_id;
				set @position_mem=@position;
				if @parent_reg_id<>0 AND  @mem_cancel =0  and  @mem_terminated=0 then
				
				INSERT INTO `s_binary_track`(`reg_id`, `reg_code`, `pur_amt`, `binary_left`, `binary_right`, `doner_id`, `doner_code`, `bill_no`, `generate_date`) 
				                     VALUES (@parent_reg_id,@parent_reg_code,OLD.amt,@left_gain_bv,@right_gain_bv,OLD.`reg_id`,OLD.`reg_code`,OLD.`bill_no`,OLD.`sales_date`);
				
				end if;
				set @reg_id=@parent_reg_id;
				SELECT count(*)  into @ct FROM `s_members` dis WHERE dis.`reg_id`=@reg_id;
				if @parent_reg_id=0 then
				set @ct=0;
				end if;               
	END while;
	SELECT ms.`referal_commission` into @gl_ref_com FROM `m_settings_value` ms WHERE 1;
	set @ref_com=OLD.binary_val*@gl_ref_com;
	INSERT INTO `s_referal_income`( `product_id`, `reg_id`, `reg_code`, `ref_inc_doner_id`, `ref_income`, `generate_date`) 
	VALUES (OLD.product_id,OLD.sponser_reg_id,OLD.sponser_reg_code,OLD.reg_id, @ref_com, OLD.`sales_date`);

		select sm.position into @posi_sp from `s_members` sm where sm.reg_id=OLD.reg_id;

		if @posi_sp='left' then
		update `s_members` set sponsed_count_left	=sponsed_count_left+1 where reg_id=OLD.sponser_reg_id;
		update `s_members` set position_next	='right' where reg_id=OLD.reg_id;
		else	
		update `s_members` set sponsed_count_right	=sponsed_count_right+1 where reg_id=OLD.sponser_reg_id;
		update `s_members` set position_next	='left' where reg_id=OLD.reg_id;
		end if;
		CALL sp_set_next_position_and_all(OLD.sponser_reg_id);
End if; 
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_achievement`
--
ALTER TABLE `m_achievement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_products`
--
ALTER TABLE `m_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_settings_value`
--
ALTER TABLE `m_settings_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_state`
--
ALTER TABLE `m_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_achievement_bonus_track`
--
ALTER TABLE `s_achievement_bonus_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_bank_trans`
--
ALTER TABLE `s_bank_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_binary_summary`
--
ALTER TABLE `s_binary_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_binary_track`
--
ALTER TABLE `s_binary_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_free_app_users`
--
ALTER TABLE `s_free_app_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_level_track`
--
ALTER TABLE `s_level_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_members`
--
ALTER TABLE `s_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_payment_summary`
--
ALTER TABLE `s_payment_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_referal_income`
--
ALTER TABLE `s_referal_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_sales_info`
--
ALTER TABLE `s_sales_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_achievement`
--
ALTER TABLE `m_achievement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `m_products`
--
ALTER TABLE `m_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_settings_value`
--
ALTER TABLE `m_settings_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_state`
--
ALTER TABLE `m_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_achievement_bonus_track`
--
ALTER TABLE `s_achievement_bonus_track`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_bank_trans`
--
ALTER TABLE `s_bank_trans`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s_binary_summary`
--
ALTER TABLE `s_binary_summary`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `s_binary_track`
--
ALTER TABLE `s_binary_track`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `s_free_app_users`
--
ALTER TABLE `s_free_app_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `s_level_track`
--
ALTER TABLE `s_level_track`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s_members`
--
ALTER TABLE `s_members`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `s_payment_summary`
--
ALTER TABLE `s_payment_summary`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_referal_income`
--
ALTER TABLE `s_referal_income`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `s_sales_info`
--
ALTER TABLE `s_sales_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
