package com.centroid.integraaccounts.paymentdata.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CashFreeTransactionData {

    @SerializedName("cf_order_id")
    @Expose
    private Integer cfOrderId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("order_currency")
    @Expose
    private String orderCurrency;
    @SerializedName("order_amount")
    @Expose
    private Double orderAmount;
    @SerializedName("order_expiry_time")
    @Expose
    private String orderExpiryTime;
    @SerializedName("customer_details")
    @Expose
    private CustomerDetails customerDetails;
    @SerializedName("order_meta")
    @Expose
    private OrderMeta orderMeta;
    @SerializedName("settlements")
    @Expose
    private Settlements settlements;
    @SerializedName("payments")
    @Expose
    private Payments payments;
    @SerializedName("refunds")
    @Expose
    private Refunds refunds;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("order_token")
    @Expose
    private String orderToken;
    @SerializedName("order_note")
    @Expose
    private Object orderNote;
    @SerializedName("payment_link")
    @Expose
    private String paymentLink;
    @SerializedName("order_tags")
    @Expose
    private Object orderTags;
    @SerializedName("order_splits")
    @Expose
    private List<Object> orderSplits = new ArrayList<>();

    public CashFreeTransactionData() {
    }

    public Integer getCfOrderId() {
        return cfOrderId;
    }

    public void setCfOrderId(Integer cfOrderId) {
        this.cfOrderId = cfOrderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getOrderCurrency() {
        return orderCurrency;
    }

    public void setOrderCurrency(String orderCurrency) {
        this.orderCurrency = orderCurrency;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderExpiryTime() {
        return orderExpiryTime;
    }

    public void setOrderExpiryTime(String orderExpiryTime) {
        this.orderExpiryTime = orderExpiryTime;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public OrderMeta getOrderMeta() {
        return orderMeta;
    }

    public void setOrderMeta(OrderMeta orderMeta) {
        this.orderMeta = orderMeta;
    }

    public Settlements getSettlements() {
        return settlements;
    }

    public void setSettlements(Settlements settlements) {
        this.settlements = settlements;
    }

    public Payments getPayments() {
        return payments;
    }

    public void setPayments(Payments payments) {
        this.payments = payments;
    }

    public Refunds getRefunds() {
        return refunds;
    }

    public void setRefunds(Refunds refunds) {
        this.refunds = refunds;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderToken() {
        return orderToken;
    }

    public void setOrderToken(String orderToken) {
        this.orderToken = orderToken;
    }

    public Object getOrderNote() {
        return orderNote;
    }

    public void setOrderNote(Object orderNote) {
        this.orderNote = orderNote;
    }

    public String getPaymentLink() {
        return paymentLink;
    }

    public void setPaymentLink(String paymentLink) {
        this.paymentLink = paymentLink;
    }

    public Object getOrderTags() {
        return orderTags;
    }

    public void setOrderTags(Object orderTags) {
        this.orderTags = orderTags;
    }

    public List<Object> getOrderSplits() {
        return orderSplits;
    }

    public void setOrderSplits(List<Object> orderSplits) {
        this.orderSplits = orderSplits;
    }
}
