package com.centroid.integraaccounts.paymentdata.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderMeta {

    @SerializedName("return_url")
    @Expose
    private Object returnUrl;
    @SerializedName("notify_url")
    @Expose
    private String notifyUrl;
    @SerializedName("payment_methods")
    @Expose
    private Object paymentMethods;

    public OrderMeta() {
    }

    public Object getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(Object returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public Object getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(Object paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
