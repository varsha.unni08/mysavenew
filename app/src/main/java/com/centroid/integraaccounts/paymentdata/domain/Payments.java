package com.centroid.integraaccounts.paymentdata.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payments {
    @SerializedName("url")
    @Expose
    private String url;

    public Payments() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
