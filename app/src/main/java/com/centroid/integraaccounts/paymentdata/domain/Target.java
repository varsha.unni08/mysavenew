package com.centroid.integraaccounts.paymentdata.domain;

public class Target {

    String id="";
    String amount="";
    String initialamount="";
    String date="";
    byte[] icon;
    String note="";
    String categoryid="";

    public Target() {
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInitialamount() {
        return initialamount;
    }

    public void setInitialamount(String initialamount) {
        this.initialamount = initialamount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
