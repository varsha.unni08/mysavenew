package com.centroid.integraaccounts.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BudgetDataAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.AddBudgetActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetBudgetFragment extends Fragment {


    public SetBudgetFragment() {
        // Required empty public constructor
    }

    View view;
    FloatingActionButton fab_addtask;

    Spinner spinnerYear,spinnerAccountName;
    Button submit;

    TextView txtTotal;
    RecyclerView recycler;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_set_budget, container, false);

        fab_addtask=view.findViewById(R.id.fab_addtask);

        spinnerYear=view.findViewById(R.id.spinnerYear);
        spinnerAccountName=view.findViewById(R.id.spinnerAccountName);
        submit=view.findViewById(R.id.submit);
        txtTotal=view.findViewById(R.id.txtTotal);
        recycler=view.findViewById(R.id.recycler);


        Calendar calendar=Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        List<String>yeardata=new ArrayList<>();

        for (int i=1990;i<year+1;i++)
        {

            yeardata.add(i+"");
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, yeardata.toArray(new String[yeardata.size()]));

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYear.setAdapter(spinnerArrayAdapter);

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), AddBudgetActivity.class));

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!spinnerAccountName.getSelectedItem().toString().equalsIgnoreCase("Select account name"))
                {

                    getBudgetData();
                }
                else {


//                    Toast.makeText(getActivity(),"Select account name",Toast.LENGTH_SHORT).show();


                    Utils.showAlertWithSingle(getActivity(), "Select account name", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });




                }




            }
        });

        return view;
    }


    public void getBudgetData()
    {
        List<Budget>budgetsdata=new ArrayList<>();

        try {


            String year = spinnerYear.getSelectedItem().toString();

            String accname = spinnerAccountName.getSelectedItem().toString();

            List<Budget> budgets = new DatabaseHelper(getActivity()).getBudgetData();


            double total=0;

            if (budgets.size() > 0) {

                for (Budget budget : budgets) {

                    JSONObject jsonObject=new JSONObject(budget.getData());


                   String year1= jsonObject.getString("year");
                    String amount= jsonObject.getString("amount");

                   String accountname= jsonObject.getString("accountname");
                   if(year.equalsIgnoreCase(year1)&&accname.equalsIgnoreCase(accountname))
                   {

                       total=total+Double.parseDouble(amount);
                       budgetsdata.add(budget);
                   }



                }


            } else {


            }

            txtTotal.setText("Total : "+total+" Rs");
        }catch (Exception e)
        {

        }



//        BudgetDataAdapter budgetDataAdapter=new BudgetDataAdapter(getActivity(),budgetsdata,SetBudgetFragment.this);
//        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recycler.setAdapter(budgetDataAdapter);

    }

}
