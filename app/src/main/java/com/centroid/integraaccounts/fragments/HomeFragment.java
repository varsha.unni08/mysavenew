package com.centroid.integraaccounts.fragments;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MainItemAdapter;
import com.centroid.integraaccounts.adapter.MyBelongingsAdapter;
import com.centroid.integraaccounts.adapter.MyLifeHomeAdapter;
import com.centroid.integraaccounts.adapter.MyMoneyItemAdapter;
import com.centroid.integraaccounts.adapter.NetWorkSliderAdapter;
import com.centroid.integraaccounts.adapter.UtilitiesAdapter;
import com.centroid.integraaccounts.views.MainActivity;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {

    }
    View v;
    RecyclerView recycler,recycler_mymoney,recycler_mybelongings,recycler_mylife,recycler_utilities;

    TextView txtChart;
    int page=0,delay=2000;

    ImageView imgchart;

    MyMoneyItemAdapter mainItemAdapter;
     MyBelongingsAdapter myBelongingsAdapter;
     MyLifeHomeAdapter myLifeHomeAdapter;
      UtilitiesAdapter utilitiesAdapter;
    Handler handler;

    ViewPager viewpager;
    TabLayout tabslayout;
    int sliderimages[]={R.drawable.img1,R.drawable.img2,R.drawable.img3,R.drawable.img4};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_home, container, false);
        recycler=v.findViewById(R.id.recycler);
        imgchart=v.findViewById(R.id.imgchart);
        txtChart=v.findViewById(R.id.txtChart);
        tabslayout=v.findViewById(R.id.tabslayout);
        viewpager=v.findViewById(R.id.viewpager);
        recycler_mymoney=v.findViewById(R.id.recycler_mymoney);
        recycler_mybelongings=v.findViewById(R.id.recycler_mybelongings);
        recycler_mylife=v.findViewById(R.id.recycler_mylife);
        recycler_utilities=v.findViewById(R.id.recycler_utilities);


        handler=new Handler();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        FrameLayout.LayoutParams layoutParams=(FrameLayout.LayoutParams)viewpager.getLayoutParams();

        double a=width/1.7;

        int h=(int)a;


        layoutParams.width=width;
        layoutParams.height=h;

        viewpager.setLayoutParams(layoutParams);

        tabslayout.removeAllTabs();

        viewpager.setAdapter(new NetWorkSliderAdapter(getActivity(),sliderimages));

        for (int i=0;i<sliderimages.length;i++)
        {
            tabslayout.addTab(tabslayout.newTab());
        }


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabslayout.getTabAt(position).select();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });








         mainItemAdapter=new MyMoneyItemAdapter(getActivity());
        recycler_mymoney.setLayoutManager(new GridLayoutManager(getActivity(),3));

        recycler_mymoney.setAdapter(mainItemAdapter);



        myBelongingsAdapter=new MyBelongingsAdapter(getActivity());
        recycler_mybelongings.setLayoutManager(new GridLayoutManager(getActivity(),3));

        recycler_mybelongings.setAdapter(myBelongingsAdapter);



        recycler_mylife.setLayoutManager(new GridLayoutManager(getActivity(),3));
        myLifeHomeAdapter=new MyLifeHomeAdapter(getActivity());
        recycler_mylife.setAdapter(myLifeHomeAdapter);

        utilitiesAdapter=new UtilitiesAdapter(getActivity());
        recycler_utilities.setLayoutManager(new GridLayoutManager(getActivity(),3));
        recycler_utilities.setAdapter(utilitiesAdapter);

        txtChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new BottomChartFragment().show(getChildFragmentManager(),"dkl");

            }
        });

        imgchart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BottomChartFragment().show(getChildFragmentManager(),"dkl");
            }
        });


        refresh();



        return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        handler.postDelayed(runnable, delay);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    Runnable runnable = new Runnable() {
        public void run() {
            if (tabslayout.getTabCount() == page) {
                page = 0;
            } else {
                page++;
            }
            viewpager.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };



    public void refresh()
    {

        String languagedata = LocaleHelper.getPersistedData(getActivity(), "en");
        Context context= LocaleHelper.setLocale(getActivity(), languagedata);

        Resources resources=context.getResources();

        txtChart.setText(resources.getString(R.string.viewchart));


        if(mainItemAdapter!=null)
        {

            mainItemAdapter.notifyDataSetChanged();
        }

        if(myBelongingsAdapter!=null)
        {

            myBelongingsAdapter.notifyDataSetChanged();
        }

        if(myLifeHomeAdapter!=null)
        {

            myLifeHomeAdapter.notifyDataSetChanged();
        }

        if(utilitiesAdapter!=null)
        {

            utilitiesAdapter.notifyDataSetChanged();
        }
    }

}
