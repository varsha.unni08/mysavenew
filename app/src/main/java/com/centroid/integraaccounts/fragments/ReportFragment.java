package com.centroid.integraaccounts.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MoreAdapter;
import com.centroid.integraaccounts.adapter.ReportAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {


    public ReportFragment() {
        // Required empty public constructor
    }

    View view;
    RecyclerView recyclersettings;

    ReportAdapter reportAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_report, container, false);

        recyclersettings=view.findViewById(R.id.recyclersettings);
        recyclersettings.setLayoutManager(new LinearLayoutManager(getActivity()));

        reportAdapter=new ReportAdapter(getActivity());
        recyclersettings.setAdapter(reportAdapter);

        return view;
    }

    public void updateData()
    {
        if(reportAdapter!=null)
        {
            reportAdapter.notifyDataSetChanged();
        }
    }

}
