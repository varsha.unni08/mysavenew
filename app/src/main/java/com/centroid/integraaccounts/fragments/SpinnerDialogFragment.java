package com.centroid.integraaccounts.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsAdapter;
import com.centroid.integraaccounts.adapter.AccountSetupAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.views.AccountSettingsListActivity;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;
import com.centroid.integraaccounts.views.AddReceiptActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpinnerDialogFragment extends DialogFragment {

    AccountSetupEventListener accountSetupEventListener;
    boolean isincome;

    public SpinnerDialogFragment(boolean isincome,AccountSetupEventListener accountSetupEventListener) {
        // Required empty public constructor

        this.accountSetupEventListener=accountSetupEventListener;
        this.isincome=isincome;
    }

    View view;

    RecyclerView recycler;
    EditText edtSearch;
    List<CommonData> commonData=new ArrayList<>();

    TextView txtSpinner;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_spinner_dialog, container, false);

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//
//        getDialog().getWindow().setLayout(width,height);


        recycler=view.findViewById(R.id.recycler);
        edtSearch=view.findViewById(R.id.edtSearch);
        txtSpinner=view.findViewById(R.id.txtSpinner);

        String languagedata = LocaleHelper.getPersistedData(getActivity(), "en");
        Context context= LocaleHelper.setLocale(getActivity(), languagedata);

        Resources resources=context.getResources();
        txtSpinner.setText(resources.getString(R.string.selectanaccount));
        edtSearch.setHint(resources.getString(R.string.search));


        if(!isincome) {

            commonData = new DatabaseHelper(getActivity()).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        }
        else {

            List<CommonData>commonData1=new DatabaseHelper(getActivity()).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);;

            for (CommonData cm : commonData1) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Income account")) {
                        commonData.add(cm);
                    }


                } catch (Exception e) {

                }

            }


        }










        if(commonData.size()>0) {

            Collections.sort(commonData, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a = 0;

                    try {


                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype = jcmn1.getString("Accountname");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1 = j1.getString("Accountname");

                        a = acctype.compareToIgnoreCase(acctypej1);

                    } catch (Exception e) {

                    }


                    return a;
                }
            });

            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.setAdapter(new AccountSetupAdapter(getActivity(),commonData,SpinnerDialogFragment.this));

        }

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {




            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                List<CommonData>commonData_searched=new ArrayList<>();
                if(!charSequence.toString().equalsIgnoreCase(""))
                {

                    for (CommonData data:commonData
                    ) {


                        try {


                            JSONObject jsonObject = new JSONObject(data.getData());

                            String accountname=jsonObject.getString("Accountname");

                            if (accountname.toLowerCase().contains(charSequence.toString().toLowerCase()) || accountname.toUpperCase().contains(charSequence.toString().toUpperCase()))

                            {
                                commonData_searched.add(data);

                            }





                        }catch (Exception e)
                        {

                        }




                    }

                    if(commonData_searched.size()>0)
                    {

                        recycler.setVisibility(View.VISIBLE);

                        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recycler.setAdapter(new AccountSetupAdapter(getActivity(),commonData_searched,SpinnerDialogFragment.this));




                    }
                    else {

                        recycler.setVisibility(View.GONE);
                    }




                }
                else {

                    recycler.setVisibility(View.VISIBLE);

                    recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recycler.setAdapter(new AccountSetupAdapter(getActivity(),commonData,SpinnerDialogFragment.this));


                }



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }




    public void onClickAccountSetup(CommonData commonData)
    {

        if(accountSetupEventListener!=null)
        {
            dismiss();
            accountSetupEventListener.getSelectedAccountSetup(commonData);
        }

    }

}
