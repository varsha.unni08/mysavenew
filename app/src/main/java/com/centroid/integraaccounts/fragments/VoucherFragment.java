package com.centroid.integraaccounts.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.PaymentVoucherAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class VoucherFragment extends Fragment {


    public VoucherFragment() {

    }

    View view;

    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtdatepick;
    ImageView imgDatepick;

    Button submit;

    int m=0,yea=0;

    Spinner spinnerAccountName;
    TextView txtTotal;
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    PaymentVoucherAdapter paymentVoucherAdapter;

    List<PaymentVoucher>paymentVouchers_selected=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_voucher, container, false);

        paymentVouchers_selected=new ArrayList<>();

        fab_addtask=view.findViewById(R.id.fab_addtask);
        recycler=view.findViewById(R.id.recycler);

        txtdatepick=view.findViewById(R.id.txtdatepick);
        imgDatepick=view.findViewById(R.id.imgDatepick);
        submit=view.findViewById(R.id.submit);
        spinnerAccountName=view.findViewById(R.id.spinnerAccountName);
        txtTotal=view.findViewById(R.id.txtTotal);




        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(!spinnerAccountName.getSelectedItem().toString().equalsIgnoreCase("Select account name"))
//                {

                    if(yea!=0||m!=0)
                    {





                    }
                    else {

                        Toast.makeText(getActivity(),"Select month and year",Toast.LENGTH_SHORT).show();

                    }




//                }
//                else {
//
//                    Toast.makeText(getActivity(),"Select account name",Toast.LENGTH_SHORT).show();
//
//                }



            }
        });



        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthDialog();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthDialog();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getActivity(), AddPaymentVoucherActivity.class);
                startActivity(intent);
            }
        });

        //getVoucherData();

        return view;
    }



    private void showMonthDialog() {

        String month="",yearselected="";


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR)-1;
        int m_calender=calendar.get(Calendar.MONTH);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);
        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setValue(m_calender);


        npmonth.setDisplayedValues(arrmonth);
        final NumberPicker npyear=dialog.findViewById(R.id.npyear);
        npyear.setMinValue(year);
        npyear.setMaxValue(year+5);


        npyear.setValue(year);
        npyear.setWrapSelectorWheel(true);
        dialog.getWindow().setLayout(width,height);
        npmonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {


            }
        });

        npyear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });





        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

               m =  npmonth.getValue();

               yea = npyear.getValue();

              String month=arrmonth[m];

              txtdatepick.setText(month+"/"+yea);

                getVoucherData();

              //getVoucherData(m,yea);


            }
        });


        dialog.show();





    }


    public void getVoucherData()
    {
        String monthinarray=arrmonth[m];

        paymentVouchers_selected.clear();

        try {

            int selected_month = m + 1;

            List<PaymentVoucher> paymentVouchers = new DatabaseHelper(getActivity()).getPaymentVoucherData();



            double total=0;

            for (PaymentVoucher paymentVoucher : paymentVouchers
            ) {

                JSONObject jsonObject=new JSONObject(paymentVoucher.getData());

                String month=jsonObject.getString("month");
                String year1=jsonObject.getString("year");
                String accountname=jsonObject.getString("accountname");
                String amount=jsonObject.getString("amount");;



                if(String.valueOf(selected_month).equalsIgnoreCase(month)&&String.valueOf(yea).equalsIgnoreCase(year1))
                {

                  //  if(accountname.equalsIgnoreCase(spinnerAccountName.getSelectedItem().toString())) {

                        total=total+Double.parseDouble(amount);

                        paymentVouchers_selected.add(paymentVoucher);
                   // }

                }



            }

            //checking budget


            List<Budget> budgets = new DatabaseHelper(getActivity()).getBudgetData();

            if(budgets.size()>0) {

                for (Budget budget : budgets) {

                    JSONObject jsonObject = new JSONObject(budget.getData());


                    String year1 = jsonObject.getString("year");
                    String amount = jsonObject.getString("amount");
                    String month = jsonObject.getString("month");
                    String accountname = jsonObject.getString("accountname");


                    if (year1.equalsIgnoreCase(String.valueOf(yea)) && monthinarray.equalsIgnoreCase(month) && accountname.equalsIgnoreCase(spinnerAccountName.getSelectedItem().toString())) {


                        double budgetamount = Double.parseDouble(amount);

                        if (budgetamount > total) {

                            txtTotal.setText("Total : " + total + " Rs");
                        } else {

                            txtTotal.setText("Total : " + total + " Rs\nTotal amount is greater than budget");

                        }


                    }


                }
            }
            else {

                txtTotal.setText("Total : " + total + " Rs");

            }



//            paymentVoucherAdapter=new PaymentVoucherAdapter(getActivity(),paymentVouchers_selected);
//            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//            recycler.setAdapter(paymentVoucherAdapter);




        }catch (Exception e)
        {

        }
    }



    public void getVouchersBack(List<PaymentVoucher>paymentVouchers)
    {

        String monthinarray=arrmonth[m];

        paymentVouchers_selected.clear();

        try {

            int selected_month = m + 1;





            double total=0;

            for (PaymentVoucher paymentVoucher : paymentVouchers
            ) {

                JSONObject jsonObject=new JSONObject(paymentVoucher.getData());

                String month=jsonObject.getString("month");
                String year1=jsonObject.getString("year");
                String accountname=jsonObject.getString("accountname");
                String amount=jsonObject.getString("amount");;



                if(String.valueOf(selected_month).equalsIgnoreCase(month)&&String.valueOf(yea).equalsIgnoreCase(year1))
                {

                    //  if(accountname.equalsIgnoreCase(spinnerAccountName.getSelectedItem().toString())) {

                    total=total+Double.parseDouble(amount);

                    paymentVouchers_selected.add(paymentVoucher);
                    // }

                }



            }

            //checking budget


            List<Budget> budgets = new DatabaseHelper(getActivity()).getBudgetData();

            if(budgets.size()>0) {

                for (Budget budget : budgets) {

                    JSONObject jsonObject = new JSONObject(budget.getData());


                    String year1 = jsonObject.getString("year");
                    String amount = jsonObject.getString("amount");
                    String month = jsonObject.getString("month");
                    String accountname = jsonObject.getString("accountname");


                    if (year1.equalsIgnoreCase(String.valueOf(yea)) && monthinarray.equalsIgnoreCase(month) && accountname.equalsIgnoreCase(spinnerAccountName.getSelectedItem().toString())) {


                        double budgetamount = Double.parseDouble(amount);

                        if (budgetamount > total) {

                            txtTotal.setText("Total : " + total + " Rs");
                        } else {

                            txtTotal.setText("Total : " + total + " Rs\nTotal amount is greater than budget");

                        }


                    }


                }
            }
            else {

                txtTotal.setText("Total : " + total + " Rs");

            }


//
//            paymentVoucherAdapter=new PaymentVoucherAdapter(getActivity(),paymentVouchers_selected);
//            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//            recycler.setAdapter(paymentVoucherAdapter);




        }catch (Exception e)
        {

        }
    }

}
