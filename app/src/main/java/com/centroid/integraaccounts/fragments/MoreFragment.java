package com.centroid.integraaccounts.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
//import com.cashfree.pg.api.CFPaymentGatewayService;
//import com.cashfree.pg.core.api.CFSession;
//import com.cashfree.pg.core.api.CFTheme;
//import com.cashfree.pg.core.api.callback.CFCheckoutResponseCallback;
//import com.cashfree.pg.core.api.utils.CFErrorResponse;
//import com.cashfree.pg.ui.api.CFDropCheckoutPayment;
//import com.cashfree.pg.ui.api.CFPaymentComponent;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MoreAdapter;
import com.centroid.integraaccounts.com.TokenResult.Tokendata;
import com.centroid.integraaccounts.com.TokenResult.TransactionStatus;
import com.centroid.integraaccounts.data.EnglishNumberToWords;
import com.centroid.integraaccounts.data.SecuredDataHelper;
import com.centroid.integraaccounts.data.domain.Currency;
import com.centroid.integraaccounts.data.domain.MainSettings;
import com.centroid.integraaccounts.data.domain.MemberData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.RazorPayOrder;
import com.centroid.integraaccounts.data.domain.SettingsData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.paymentdata.domain.CashFreeTransactionData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.transaction.TransactionData;
import com.centroid.integraaccounts.views.AppRenewalActivity;
import com.centroid.integraaccounts.views.InvoiceActivity;
import com.centroid.integraaccounts.webserviceHelper.PaymentCheckSumRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.PaymentRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
//import com.paytm.pgsdk.TransactionManager;
//import com.razorpay.Checkout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.centroid.integraaccounts.Constants.Utils.PaytmCredentials.ActivityRequestCode;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment  {


    public MoreFragment() {
        // Required empty public constructor
    }

    View v;

    String url="",description="";

    TransactionData transactionStatus;
    RecyclerView recyclersettings;

    MoreAdapter moreAdapter=null;

    String countryid="0",stateid="0";

    String name="";
    String rs=  "";
    Currency currency=null;
    double total_amount=0;


    String email="",phonenumber="";

    MemberData memberData=null;

    SettingsData settingsData1=null;
    String orderIdString="",txnTokenString,TAG="sdkf";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_more, container, false);

        recyclersettings=v.findViewById(R.id.recyclersettings);
        recyclersettings.setLayoutManager(new LinearLayoutManager(getActivity()));

        moreAdapter=new MoreAdapter(getActivity(),MoreFragment.this);
        recyclersettings.setAdapter(moreAdapter);
//        checkout=new Checkout();
//
//        checkout.setKeyID(Utils.razorkeyid);
//        try {
//            CFPaymentGatewayService.getInstance().setCheckoutCallback(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
       // getMemberData();
        return v;
    }

//
//    @Override
//    public void onPaymentVerify(String s) {
//        Utils.showAlertWithSingle(getActivity(), "Payment Success", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//
//        passPurchaseDataToServer(s);
//    }
//
//    @Override
//    public void onPaymentFailure(CFErrorResponse cfErrorResponse, String s) {
//        Utils.showAlertWithSingle(getActivity(), "Payment failed", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//    }

    public void updateLanguage()
    {
        if(moreAdapter!=null)
        {
            moreAdapter.notifyDataSetChanged();
        }
    }


    public void getProfileData()
    {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {

                                countryid=profiledata.getData().getCountryId();

                                stateid=profiledata.getData().getStateId();
                                name=profiledata.getData().getFullName();
                                email=profiledata.getData().getEmailId();
                                phonenumber=profiledata.getData().getMobile();

                                  payAmount();
                               // getCurrencyCode();

                            }





                        }
                        else {

                            Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();




//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getActivity().getSupportFragmentManager(),"fkjfk");
//
//
//
//
//
//        Call<Profiledata> jsonObjectCall=client.getUserProfile(new PreferenceHelper(getActivity()).getData(Utils.userkey)
//
//        );
//        jsonObjectCall.enqueue(new Callback<Profiledata>() {
//            @Override
//            public void onResponse(Call<Profiledata> call, Response<Profiledata> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//
//                            if(response.body().getData()!=null)
//                            {
//
//                                countryid=response.body().getData().getCountryId();
//
//                                stateid=response.body().getData().getStateId();
//                                name=response.body().getData().getFullName();
//
//                                email=response.body().getData().getEmailId();
//
//                                phonenumber=response.body().getData().getMobile();
//
//                                //  payAmount();
//                                getCurrencyCode();
//
//                            }
//
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(getContext()," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Profiledata> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });
    }



    public void getCurrencyCode()
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {


                    try{


                         currency=new GsonBuilder().create().fromJson(data,Currency.class);

                        if(currency.getStatus()==1)
                        {




//                            payAmount();




                        }
                        else {

                            Toast.makeText(getContext()," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getCurrencyByCountryID+"?="+countryid+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();





//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getActivity().getSupportFragmentManager(),"fkjfk");
//
//
//
//        Call<Currency>currencyCall=client.getCurrencyByCountryID(new PreferenceHelper(getActivity()).getData(Utils.userkey),countryid);
//
//        currencyCall.enqueue(new Callback<Currency>() {
//            @Override
//            public void onResponse(Call<Currency> call, Response<Currency> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//
//
//                            currency=response.body();
//                            payAmount();
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(getContext()," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<Currency> call, Throwable t) {
//                progressFragment.dismiss();
//            }
//        });
    }


    public void payAmount()
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{


                        MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                        if(settingsData.getStatus()==1)
                        {

                            settingsData1=settingsData.getData();





                            rs=  settingsData1.getOneBvRequiredAmount();







                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            ((AppCompatActivity)getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int height = displayMetrics.heightPixels;
                            int width = displayMetrics.widthPixels;
                            final Dialog dialog = new Dialog(getActivity());
                            dialog.setContentView(R.layout.layout_paymentsummary);

                            int b=(int)(height/1.2);
                            dialog.getWindow().setLayout(width - 50,b );

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                            TextView txtActualPrice=dialog.findViewById(R.id.txtActualPrice);
                            LinearLayout layout_actualprice=dialog.findViewById(R.id.layout_actualprice);
                            TextView txtSgst=dialog.findViewById(R.id.txtSgst);
                            TextView txtCgst=dialog.findViewById(R.id.txtCgst);
                            TextView txtIgst=dialog.findViewById(R.id.txtIgst);
                            TextView txtTotal=dialog.findViewById(R.id.txtTotal);
                            TextView txtYourPosition=dialog.findViewById(R.id.txtYourPosition);
                            LinearLayout layout_yourposition=dialog.findViewById(R.id.layout_yourposition);

                            LinearLayout layout_Igst=dialog.findViewById(R.id.layout_Igst);
                            LinearLayout layout_sgst=dialog.findViewById(R.id.layout_sgst);
                            LinearLayout layout_cgst=dialog.findViewById(R.id.layout_cgst);

                            Button btnSubmit=dialog.findViewById(R.id.btnSubmit);


                            if(memberData!=null)
                            {
                                txtYourPosition.setText(memberData.getPositionNext());
                            }
                            else {

                                layout_yourposition.setVisibility(View.GONE);
                            }



                            btnSubmit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();

                                    //  getRazorPayOrder();

                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
                                    String date = df.format(c.getTime());
                                    Random rand = new Random();
                                    int min =1000, max= 9999;
// nextInt as provided by Random is exclusive of the top value so you need to add 1
                                    int randomNum = rand.nextInt((max - min) + 1) + min;
                                    orderIdString =  date+String.valueOf(randomNum);


                                   //  new Sampleasync().execute("");

                                    // generateTransaction();

                                  //  executeWebCall();


                                    Dialog dialog_account=new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                                    dialog_account.setContentView(R.layout.layout_account);

                                    Button btnOk= dialog_account.findViewById(R.id.btnOk);

                                    btnOk.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog_account.dismiss();
                                        }
                                    });

                                    dialog_account.show();





















                                }
                            });


                            if (countryid.equalsIgnoreCase("1"))
                            {
                                //layout_actualprice.setVisibility(View.GONE);

                                if (stateid.equalsIgnoreCase("12"))
                                {


                                    double actualvalue=Double.parseDouble(rs);

                                    double sgstvalue=actualvalue*Double.parseDouble(settingsData1.getSgst())/100;

                                    double cgstvalue=actualvalue*Double.parseDouble(settingsData1.getCgst())/100;

                                    txtActualPrice.setText(rs+" INR ");
                                    txtSgst.setText(sgstvalue+" INR ");
                                    txtCgst.setText(cgstvalue+" INR ");
                                    layout_Igst.setVisibility(View.GONE);

                                    double t=actualvalue+sgstvalue+cgstvalue;


                                    txtTotal.setText(Math.round(t)+" INR ");
                                    total_amount=Math.round(t);
                                }
                                else {
                                    double actualvalue=Double.parseDouble(rs);

                                    txtActualPrice.setText(rs+" INR ");
                                    layout_sgst.setVisibility(View.GONE);
                                    layout_cgst.setVisibility(View.GONE);
                                    layout_Igst.setVisibility(View.VISIBLE);
                                    double Igstvalue=actualvalue*Double.parseDouble(settingsData1.getIgst())/100;

                                    txtIgst.setText(Igstvalue+" INR ");

                                    double t=actualvalue+Igstvalue;

                                    txtTotal.setText(Math.round(t)+" INR ");

                                    total_amount=Math.round(t);
                                }



                            }
                            else {

                                layout_sgst.setVisibility(View.GONE);
                                layout_cgst.setVisibility(View.GONE);
                                layout_Igst.setVisibility(View.GONE);
                                layout_actualprice.setVisibility(View.GONE);

                                double actualvalue=Double.parseDouble(rs);

                                double othertax=Double.parseDouble(settingsData1.getOtherTax());

                                double otprice=actualvalue*othertax/100;

                                double tot=actualvalue+otprice;

                                double conversionval=Double.parseDouble("1.00");



                                double t=tot*conversionval;




                                total_amount=Math.round(t);



                                txtTotal.setText(total_amount+" INR ");

                            }










                            dialog.show();







                        }
                        else {





                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSettingsValue+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();













//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");
//
//
//        String key=  new PreferenceHelper(getActivity()).getData(Utils.userkey);
//
//
//
//
//
//        Call<JsonObject> jsonObjectCall=client.getSettingsValue(key
//        );
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//                        MainSettings settingsData=new GsonBuilder().create().fromJson(response.body().toString(), MainSettings.class);
//
//                        if(settingsData.getStatus()==1)
//                        {
//
//                             settingsData1=settingsData.getData();
//
//
//
//
//
//                            rs=  settingsData1.getOneBvRequiredAmount();
//
//
//
//
//
//
//
//                            DisplayMetrics displayMetrics = new DisplayMetrics();
//                            ((AppCompatActivity)getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                            int height = displayMetrics.heightPixels;
//                            int width = displayMetrics.widthPixels;
//                            final Dialog dialog = new Dialog(getActivity());
//                            dialog.setContentView(R.layout.layout_paymentsummary);
//
//                            int b=(int)(height/1.2);
//                            dialog.getWindow().setLayout(width - 50,b );
//
//                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//                            TextView txtActualPrice=dialog.findViewById(R.id.txtActualPrice);
//                            LinearLayout layout_actualprice=dialog.findViewById(R.id.layout_actualprice);
//                            TextView txtSgst=dialog.findViewById(R.id.txtSgst);
//                            TextView txtCgst=dialog.findViewById(R.id.txtCgst);
//                            TextView txtIgst=dialog.findViewById(R.id.txtIgst);
//                            TextView txtTotal=dialog.findViewById(R.id.txtTotal);
//                            TextView txtYourPosition=dialog.findViewById(R.id.txtYourPosition);
//                            LinearLayout layout_yourposition=dialog.findViewById(R.id.layout_yourposition);
//
//                            LinearLayout layout_Igst=dialog.findViewById(R.id.layout_Igst);
//                            LinearLayout layout_sgst=dialog.findViewById(R.id.layout_sgst);
//                            LinearLayout layout_cgst=dialog.findViewById(R.id.layout_cgst);
//
//                            Button btnSubmit=dialog.findViewById(R.id.btnSubmit);
//
//
//                            if(memberData!=null)
//                            {
//                                txtYourPosition.setText(memberData.getPositionNext());
//                            }
//                            else {
//
//                                layout_yourposition.setVisibility(View.GONE);
//                            }
//
//
//
//                            btnSubmit.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    dialog.dismiss();
//
//                                  //  getRazorPayOrder();
//
//                                    Calendar c = Calendar.getInstance();
//                                    SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
//                                    String date = df.format(c.getTime());
//                                    Random rand = new Random();
//                                    int min =1000, max= 9999;
//// nextInt as provided by Random is exclusive of the top value so you need to add 1
//                                    int randomNum = rand.nextInt((max - min) + 1) + min;
//                                    orderIdString =  date+String.valueOf(randomNum);
//
//
//                                   // new Sampleasync().execute("");
//
//                                    executeWebCall();
//
//                                }
//                            });
//
//
//                            if (countryid.equalsIgnoreCase("1"))
//                            {
//                                //layout_actualprice.setVisibility(View.GONE);
//
//                                if (stateid.equalsIgnoreCase("12"))
//                                {
//
//
//                                    double actualvalue=Double.parseDouble(rs);
//
//                                    double sgstvalue=actualvalue*Double.parseDouble(settingsData1.getSgst())/100;
//
//                                    double cgstvalue=actualvalue*Double.parseDouble(settingsData1.getCgst())/100;
//
//                                    txtActualPrice.setText(rs+" "+currency.getData().getCurrency_code());
//                                    txtSgst.setText(sgstvalue+" "+currency.getData().getCurrency_code());
//                                    txtCgst.setText(cgstvalue+" "+currency.getData().getCurrency_code());
//                                    layout_Igst.setVisibility(View.GONE);
//
//                                    double t=actualvalue+sgstvalue+cgstvalue;
//
//
//                                    txtTotal.setText(Math.round(t)+" "+currency.getData().getCurrency_code());
//                                    total_amount=Math.round(t);
//                                }
//                                else {
//                                    double actualvalue=Double.parseDouble(rs);
//
//                                    txtActualPrice.setText(rs+" "+currency.getData().getCurrency_code());
//                                    layout_sgst.setVisibility(View.GONE);
//                                    layout_cgst.setVisibility(View.GONE);
//                                    layout_Igst.setVisibility(View.VISIBLE);
//                                    double Igstvalue=actualvalue*Double.parseDouble(settingsData1.getIgst())/100;
//
//                                    txtIgst.setText(Igstvalue+" "+currency.getData().getCurrency_code());
//
//                                    double t=actualvalue+Igstvalue;
//
//                                    txtTotal.setText(Math.round(t)+" "+currency.getData().getCurrency_code());
//
//                                    total_amount=Math.round(t);
//                                }
//
//
//
//                            }
//                            else {
//
//                                layout_sgst.setVisibility(View.GONE);
//                                layout_cgst.setVisibility(View.GONE);
//                                layout_Igst.setVisibility(View.GONE);
//                                layout_actualprice.setVisibility(View.GONE);
//
//                                double actualvalue=Double.parseDouble(rs);
//
//                                double othertax=Double.parseDouble(settingsData1.getOtherTax());
//
//                                double otprice=actualvalue*othertax/100;
//
//                                double tot=actualvalue+otprice;
//
//                                double conversionval=Double.parseDouble(currency.getData().getConversion_value());
//
//
//
//                                double t=tot*conversionval;
//
//
//
//
//                                total_amount=Math.round(t);
//
//
//
//                                txtTotal.setText(total_amount+" "+currency.getData().getCurrency_code());
//
//                            }
//
//
//
//
//
//
//
//
//
//
//                            dialog.show();
//
//
//
//
//
//
//
//                        }
//                        else {
//
//
//
//
//
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });







    }



    public void getDataFromSettings()
    {

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try {


                        MainSettings settingsData = new GsonBuilder().create().fromJson(data, MainSettings.class);


                        url=Utils.linkimg+settingsData.getData().getLinkImage();

                        //url="https://mysaving.in/images/saveicon.png";

                       // getBitmapData(url);







                    } catch (Exception e) {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSettingsValue+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();









//        String key=  new PreferenceHelper(getActivity()).getData(Utils.userkey);
//
//
//        Call<JsonObject> jsonObjectCall=client.getSettingsValue(key
//        );
//
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//
//                progressFragment.dismiss();
//                if (response.body() != null) {
//
//                    try {
//
//
//                        MainSettings settingsData = new GsonBuilder().create().fromJson(response.body().toString(), MainSettings.class);
//
//
//                        url=Utils.linkimg+settingsData.getData().getLinkImage();
//
//                        //url="https://mysaving.in/images/saveicon.png";
//
//                        getBitmapData(url);
//
//
////
////                        Intent sendIntent = new Intent();
////                        sendIntent.setAction(Intent.ACTION_SEND);
////                        sendIntent.putExtra(Intent.EXTRA_TEXT, link);
////                        sendIntent.setType("text/plain");
////                        startActivity(sendIntent);
//
//
//
//
//                    } catch (Exception e) {
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                progressFragment.dismiss();
//
//                t.printStackTrace();
//
//            }
//        });






    }




    public void getBitmapData(String url,String description)
    {
        this.url=url;
        this.description=description;

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"df");

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap1 =null;

                try {


                    String permission="";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                        permission=Manifest.permission.READ_MEDIA_IMAGES;

                    }
                    else{

                        permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    }




                        if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {



                            URL ur = new URL(url);
                            try {
                                bitmap1 = BitmapFactory.decodeStream(ur.openConnection().getInputStream());
                            }catch (Exception e)
                            {

                            }




                            if(bitmap1!=null) {
                            String d = Calendar.getInstance().getTimeInMillis() + "";


                            Intent shareIntent;
                            //Bitmap bitmap1= BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
                            String path = getActivity().getExternalCacheDir().getAbsolutePath() + "/" + d + ".png";
                            OutputStream out = null;
                            File file = new File(path);
                            try {

                                if (!file.exists()) {
                                    file.createNewFile();
                                }


                                out = new FileOutputStream(file);
                                bitmap1.compress(Bitmap.CompressFormat.PNG, 100, out);
                                out.flush();
                                out.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            path = file.getPath();
                            // path=file.getPath();

                            Uri bmpUri = null;

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        progressFragment.dismiss();
                                    }
                                });

                            String id = new PreferenceHelper(getActivity()).getData(Utils.userkey);


                            final String link = Utils.newdomain + "/signup?sponserid=" + id;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                bmpUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

                            } else {


                                bmpUri = Uri.parse("file://" + path);
                            }
                            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, " \n" + link);
                            shareIntent.setType("image/png");
                            startActivity(Intent.createChooser(shareIntent, "Share with"));


                        }


                    }
                        else {


                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    progressFragment.dismiss();
                                }
                            });


                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                        }


//                        else{
//                        String id = new PreferenceHelper(getActivity()).getData(Utils.userkey);
//
//
//                        final String link = Utils.domain + "index.php/web/signup?sponserid=" + id;
//
//
//                        Intent sendIntent = new Intent();
//                        sendIntent.setAction(Intent.ACTION_SEND);
//                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more and join now \n"+link);
//                        sendIntent.setType("text/plain");
//                        startActivity(sendIntent);
//                    }



                } catch(Exception e) {
                    System.out.println(e);
                }






            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        getBitmapData(url,description);
    }










//    public class Sampleasync extends AsyncTask<String,String,String>
//    {
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String responseData = "";
//            try {
//                JSONObject paytmParams = new JSONObject();
//
//                JSONObject body = new JSONObject();
//                body.put("requestType", "Payment");
//                body.put("mid", Utils.PaytmCredentials.TestMerchantID);
//                body.put("websiteName", Utils.PaytmCredentials.Website);
//                body.put("orderId", orderIdString);
//                body.put("callbackUrl", "https://merchant.com/callback");
//
//
//                JSONObject txnAmount = new JSONObject();
//                txnAmount.put("value", String.valueOf(total_amount));
//                txnAmount.put("currency", "INR");
//
//                JSONObject userInfo = new JSONObject();
//                userInfo.put("custId", name);
//                body.put("txnAmount", txnAmount);
//                body.put("userInfo", userInfo);
//
//                System.setProperty("com.warrenstrange.googleauth.rng.algorithmProvider", "IBMJCE");
//                String checksum = PaytmChecksum.generateSignature(body.toString(), Utils.PaytmCredentials.TestMerchantKey);
//
//
//
//
//
//
//
//                JSONObject head = new JSONObject();
//                head.put("signature", checksum);
//
//                paytmParams.put("body", body);
//                paytmParams.put("head", head);
//
//                String post_data = paytmParams.toString();
//
//
//                URL url = new URL("https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                /* for Production */
//                // URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                try {
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setRequestMethod("POST");
//                    connection.setRequestProperty("Content-Type", "application/json");
//                    connection.setDoOutput(true);
//
//                    DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
//                    requestWriter.writeBytes(post_data);
//                    requestWriter.close();
//
//                    InputStream is = connection.getInputStream();
//                    BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
//                    if ((responseData = responseReader.readLine()) != null) {
//                        Log.e("Response" , responseData);
//
//
//                    }
//
//
//
//
//
//
//
//
//                    responseReader.close();
//                } catch (Exception exception) {
//                    exception.printStackTrace();
//                }
//
//            }catch (Exception e)
//            {
//
//                Log.e("Exception",e.toString());
//            }
//            return responseData;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            Gson gsonBuilder=new GsonBuilder().create();
//            Tokendata tokendata= gsonBuilder.fromJson(s, Tokendata.class);
//            startPaytmPayment(tokendata.getBody().getTxnToken());
//
//
//
//        }
//    }

//    public void generateTransaction()
//    {
//
//        Map<String,String> params=new HashMap<>();
//        params.put("requestType","payment");
//        params.put("mid", Utils.PaytmCredentials.TestMerchantID);
//        params.put("websiteName",Utils.PaytmCredentials.Website);
//        params.put("orderId",orderIdString);
//        params.put("callbackUrl","https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=");
//        params.put("custId","cust001");
//        params.put("inittrans","https://securegw.paytm.in/theia/api/v1/initiateTransaction");
//        params.put("merchantkey",Utils.PaytmCredentials.TestMerchantKey);
//
//
//        new PaymentRequestHandler(getActivity(), params, new ResponseHandler() {
//            @Override
//            public void onSuccess(String data) {
//
//                if(data!=null) {
//                    Gson gsonBuilder = new GsonBuilder().create();
//                    Tokendata tokendata = gsonBuilder.fromJson(data, Tokendata.class);
//                    startPaytmPayment(tokendata.getBody().getTxnToken());
//                }
//            }
//
//            @Override
//            public void onFailure(String err) {
//
//            }
//        },"",Request.Method.POST);
//    }

//    public class Sampleasync extends AsyncTask<String,String,String>
//    {
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String responseData = "";
//            try {
//
//                new SecuredDataHelper(getActivity()).putData(Utils.merchantkey,Utils.PaytmCredentials.TestMerchantKey);
//
//
//                JSONObject paytmParams = new JSONObject();
//
//                JSONObject body = new JSONObject();
//                body.put("requestType", "Payment");
//                body.put("mid", Utils.PaytmCredentials.TestMerchantID);
//                body.put("websiteName", Utils.PaytmCredentials.Website);
//                body.put("orderId", orderIdString);
//                //
//                // for test
//                //
//               // body.put("callbackUrl", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderIdString);
//                ///
//
//                  body.put("callbackUrl", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderIdString);
//
//
//
//                JSONObject txnAmount = new JSONObject();
//                txnAmount.put("value", total_amount);
//               // txnAmount.put("value", "1.00");
//                txnAmount.put("currency", "INR");
//
//                JSONObject userInfo = new JSONObject();
//                userInfo.put("custId", "CUST_001");
//                body.put("txnAmount", txnAmount);
//                body.put("userInfo", userInfo);
//
//                System.setProperty("com.warrenstrange.googleauth.rng.algorithmProvider", "IBMJCE");
//
//
//             String key=   new SecuredDataHelper(getActivity()).getData(Utils.merchantkey);
//
//
//                String checksum = PaytmChecksum.generateSignature(body.toString(), key);
//
//
//
//
//
//
//
//                JSONObject head = new JSONObject();
//                head.put("signature", checksum);
//
//                paytmParams.put("body", body);
//                paytmParams.put("head", head);
//
//                String post_data = paytmParams.toString();
//
//
//               // URL url = new URL("https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                /* for Production */
// URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                try {
//                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                    connection.setRequestMethod("POST");
//                    connection.setRequestProperty("Content-Type", "application/json");
//                    connection.setDoOutput(true);
//
//                    DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
//                    requestWriter.writeBytes(post_data);
//                    requestWriter.close();
//
//                    InputStream is = connection.getInputStream();
//                    BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
//                    if ((responseData = responseReader.readLine()) != null) {
//                        Log.e("Response" , responseData);
//
//
//                    }
//
//
//
//
//
//
//
//
//                    responseReader.close();
//                } catch (Exception exception) {
//                    exception.printStackTrace();
//                }
//
//            }catch (Exception e)
//            {
//
//                Log.e("Exception",e.toString());
//            }
//            return responseData;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            Gson gsonBuilder=new GsonBuilder().create();
//            Tokendata tokendata= gsonBuilder.fromJson(s, Tokendata.class);
//            startPaytmPayment(tokendata.getBody().getTxnToken());
//
//
//
//        }
//    }

    public void executeWebCall() {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    String responseData = "";
                    /* initialize an object */
                    JSONObject paytmParams = new JSONObject();

                    Random rm=new Random();
                    int m=rm.nextInt(1000-100)+1000;

                    /* body parameters */
                    JSONObject body = new JSONObject();

                    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                    paytmParams.put("order_amount", total_amount);

                    /* Enter your order id which needs to be check status for */
                    paytmParams.put("order_id", m+"");
                    paytmParams.put("order_currency","INR");

/**
 * Generate checksum by parameters we have in body
 * You can get Checksum JAR from https://developer.paytm.com/docs/checksum/
 * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
 */
                    //  String checksum = PaytmChecksum.generateSignature(body.toString(), Utils.PaytmCredentials.TestMerchantKey);
                    /* head parameters */
                    JSONObject head = new JSONObject();

                    /* put generated checksum value here */
                    head.put("customer_id", "1");
                    head.put("customer_name", name);
                    head.put("customer_email", "");
                    head.put("customer_phone", phonenumber);

                    JSONObject order_meta=new JSONObject();
                    order_meta.put("notify_url","https://test.cashfree.com");

                    /* prepare JSON string for request */
                    paytmParams.put("order_meta", order_meta);
                    paytmParams.put("customer_details", head);
                    String post_data = paytmParams.toString();

                    /* for Staging */
                    //  URL url = new URL("https://securegw-stage.paytm.in/v3/order/status");

                    /* for Production */
                    URL url = new URL("https://api.cashfree.com/pg/orders");

                    try {
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setRequestProperty ("x-client-id", "2110088e61c5abc8b72c74b033800112");
                        connection.setRequestProperty ("x-client-secret", "6d61be6225fcc9f4ea900b1fd8fb3aa6cc83b7ea");
                        connection.setRequestProperty ("x-api-version", "2022-01-01");
                        connection.setRequestProperty ("x-request-id", "Antony");

                        connection.setDoOutput(true);


                        DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
                        requestWriter.writeBytes(post_data);
                        requestWriter.close();

                        InputStream is = connection.getInputStream();
                        BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                        if ((responseData = responseReader.readLine()) != null) {
                            System.out.append("Response: " + responseData);
                        }

                        CashFreeTransactionData trs=new GsonBuilder().create().fromJson(responseData,CashFreeTransactionData.class);

                        startPaytmPayment(trs);

                        // doDropCheckoutPayment(trs);



                        // System.out.append("Request: " + post_data);
                        responseReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

//                    transactionStatus=new GsonBuilder().create().fromJson(responseData, TransactionData.class);
//
//
//
//                    // executor.shutdown();
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            showStatus();
//                        }
//                    });





                }catch (Exception e)
                {

                }

               // getCheckSumData();






            }
        });

    }


    public void getCheckSumData()
    {
       // total_amount=1;
        String dateorderid=Utils.getTimestamp();

        orderIdString=dateorderid;

        //  String url=  "https://mysaving.in/IntegraAccount/"+"paytmapi/initTransaction.php?timestamp="+dateorderid;

        String responseData="";
        try{

            Map<String,String>mp=new HashMap<>();

            mp.put("requestType", "Payment");
            mp.put("mid", Utils.PaytmCredentials.TestMerchantID);
            mp.put("websiteName", Utils.PaytmCredentials.Website);
            mp.put("orderId", dateorderid);
            mp.put("callbackUrl", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=");
            mp.put("value", String.valueOf(total_amount));
            mp.put("custId", "cust_001");
            mp.put("inittrans", "https://securegw.paytm.in/theia/api/v1/initiateTransaction");
            mp.put("merchantkey", Utils.PaytmCredentials.TestMerchantKey);
            mp.put("timestamp", dateorderid);


            new PaymentCheckSumRequestHandler(orderIdString,getActivity(), mp, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {


                    if(data!=null)
                    {


                        //Toast.makeText(getActivity(),data,Toast.LENGTH_SHORT).show();

                        Gson gsonBuilder=new GsonBuilder().create();
                        Tokendata tokendata= gsonBuilder.fromJson(data, Tokendata.class);
                        //startPaytmPayment(tokendata.getBody().getTxnToken());
                    }

                }

                @Override
                public void onFailure(String err) {
                    // progressFragment.dismiss();

                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                }
            },Utils.WebServiceMethodes.getSliderImages+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();










        }catch (Exception e)
        {

        }




    }



    public void showStatus()
    {
        String trid=transactionStatus.getBody().getTxnId();
        if(transactionStatus.getBody().getResultInfo().getResultCode().equalsIgnoreCase("01")) {

            passPurchaseDataToServer(trid);
           // showInvoiceDialog( trid);
        }
        else {

            Toast.makeText(getActivity(),"Transaction failed",Toast.LENGTH_SHORT).show();
        }
    }

    public void showInvoiceDialog(String trid)
    {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            double h = displayMetrics.heightPixels / 1.2;
            int height = (int) h;
            int width = displayMetrics.widthPixels;
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_invoiceformat);
            TextView txtbillno = dialog.findViewById(R.id.txtbillno);
            TextView txtDate = dialog.findViewById(R.id.txtDate);
            TextView txtBuyer = dialog.findViewById(R.id.txtBuyer);
            TextView txtActualRate = dialog.findViewById(R.id.txtActualRate);
            TextView txtFullActualRate = dialog.findViewById(R.id.txtFullActualRate);
            TextView txtAmountinWords = dialog.findViewById(R.id.txtAmountinWords);
            TextView txtGSTHead = dialog.findViewById(R.id.txtGSTHead);
            LinearLayout layout_gst = dialog.findViewById(R.id.layout_gst);
            TextView txtsgstPrice = dialog.findViewById(R.id.txtsgstPrice);
            TextView txtcgstPrice = dialog.findViewById(R.id.txtcgstPrice);
            TextView txtIGST = dialog.findViewById(R.id.txtIGST);
            TextView txtNetTotalAmount = dialog.findViewById(R.id.txtNetTotalAmount);
            Button btnDownload = dialog.findViewById(R.id.btnDownload);
            Calendar calendar = Calendar.getInstance();
            int d = calendar.get(Calendar.DAY_OF_MONTH);
            int m = calendar.get(Calendar.MONTH) + 1;
            int y = calendar.get(Calendar.YEAR);
            txtbillno.setText("Bill no. : " + trid);
            txtDate.setText("Date : " + d + "-" + m + "-" + y);

            if (countryid.equalsIgnoreCase("1")) {
                if (stateid.equalsIgnoreCase("12")) {
                    double actualvalue = Double.parseDouble(rs);
                    double sgstvalue = actualvalue * Double.parseDouble(settingsData1.getSgst()) / 100;
                    double cgstvalue = actualvalue * Double.parseDouble(settingsData1.getCgst()) / 100;
                    txtActualRate.setText(rs + " ");
                    txtFullActualRate.setText(rs + " ");
                    txtsgstPrice.setText(sgstvalue + " ");
                    txtcgstPrice.setText(cgstvalue + " ");
                    txtIGST.setVisibility(View.INVISIBLE);
                    double t = actualvalue + sgstvalue + cgstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                } else {
                    double actualvalue = Double.parseDouble(rs);
                    txtActualRate.setText(rs + " ");
                    layout_gst.setVisibility(View.INVISIBLE);
                    double Igstvalue = actualvalue * Double.parseDouble(settingsData1.getIgst()) / 100;
                    txtIGST.setText(Igstvalue + " ");
                    double t = actualvalue + Igstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                }

            } else {

                txtGSTHead.setText("Other Tax");
                layout_gst.setVisibility(View.INVISIBLE);
                txtsgstPrice.setVisibility(View.GONE);
                txtcgstPrice.setVisibility(View.GONE);
                txtIGST.setVisibility(View.VISIBLE);
                double actualvalue = Double.parseDouble(rs);
                double othertax = Double.parseDouble(settingsData1.getOtherTax());
                double otprice = actualvalue * othertax / 100;
                txtIGST.setText(otprice + "");
                double tot = actualvalue + otprice;
                double conversionval = Double.parseDouble(currency.getData().getConversion_value());
                double t = tot * conversionval;
                total_amount = Math.round(t);
                txtNetTotalAmount.setText(total_amount + " ");

            }


            int a=(int)total_amount;
            String d1 = String.valueOf(a);

            long n = Long.parseLong(d1);
            txtAmountinWords.setText("Amount in words : " + EnglishNumberToWords.convert(n) + " " + currency.getData().getCurrency_code());


            dialog.getWindow().setLayout(width, height);

            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {

                        btnDownload.setVisibility(View.GONE);

                        String permission="";

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                            permission=Manifest.permission.READ_MEDIA_IMAGES;

                        }
                        else{

                            permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                        }



                        if(ContextCompat.checkSelfPermission(getActivity(), permission)== PackageManager.PERMISSION_GRANTED) {
                            DisplayMetrics metrics = new DisplayMetrics();
                            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;

                            View v1 = dialog.getWindow().getDecorView().getRootView();


                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);


                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();

                            File fp = new File(getActivity().getExternalCacheDir() + "/Save/Invoice");


                            if (!fp.exists()) {
                                fp.mkdirs();
                            }

                            File f = new File(fp.getAbsolutePath(), "invoice" + ts + ".png");
                            if (!f.exists()) {
                                f.createNewFile();

                            } else {

                                f.delete();
                                f.createNewFile();
                            }


                            FileOutputStream output = new FileOutputStream(f);


                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                            output.close();


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", f);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(photoURI, "image/*");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(intent);

                            } else {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(f), "image/*");
                                startActivity(intent);
                            }

                            dialog.dismiss();
                        }
                        else {

                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);
                        }



                    }catch (Exception e)
                    {

                    }
                }
            });


            dialog.show();
        }catch (Exception e)
        {
            Log.e("TAAGG",e.toString());
        }
    }



    public void startPaytmPayment (CashFreeTransactionData trs){

//        try {
//            CFSession cfSession = new CFSession.CFSessionBuilder()
//                    .setEnvironment(CFSession.Environment.PRODUCTION)
//                    .setOrderToken(trs.getOrderToken())
//                    .setOrderId(trs.getOrderId())
//                    .build();
//            CFPaymentComponent cfPaymentComponent = new CFPaymentComponent.CFPaymentComponentBuilder()
//                    // Shows only Card and UPI modes
//                    .add(CFPaymentComponent.CFPaymentModes.CARD)
//                    .add(CFPaymentComponent.CFPaymentModes.UPI)
//                    .build();
//            // Replace with your application's theme colors
//            CFTheme cfTheme = new CFTheme.CFThemeBuilder()
//                    .setNavigationBarBackgroundColor("#fc2678")
//                    .setNavigationBarTextColor("#ffffff")
//                    .setButtonBackgroundColor("#fc2678")
//                    .setButtonTextColor("#ffffff")
//                    .setPrimaryTextColor("#000000")
//                    .setSecondaryTextColor("#000000")
//                    .build();
//            CFDropCheckoutPayment cfDropCheckoutPayment = new CFDropCheckoutPayment.CFDropCheckoutPaymentBuilder()
//                    .setSession(cfSession)
//                    .setCFUIPaymentModes(cfPaymentComponent)
//                    .setCFNativeCheckoutUITheme(cfTheme)
//                    .build();
//            CFPaymentGatewayService gatewayService = CFPaymentGatewayService.getInstance();
//            gatewayService.doPayment(getActivity(), cfDropCheckoutPayment);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }


//        txnTokenString = token;
//        // for test mode use it
//     //   String host = "https://securegw-stage.paytm.in/";
//        // for production mode use it
//          String host = "https://securegw.paytm.in/";
//
////        String orderDetails = "MID: " + Utils.PaytmCredentials.TestMerchantID + ", OrderId: " + "ORDERID_98765" + ", TxnToken: " + txnTokenString
////                + ", Amount: " + "1.00";
//
//        //Log.e(TAG, "order details "+ orderDetails);
//
//        String callBackUrl = host + "theia/paytmCallback?ORDER_ID="+orderIdString;
//        Log.e(TAG, " callback URL "+callBackUrl);
//
//       PaytmOrder paytmOrder = new PaytmOrder(orderIdString, Utils.PaytmCredentials.TestMerchantID, txnTokenString, total_amount+"", callBackUrl);
//     //   PaytmOrder paytmOrder = new PaytmOrder(orderIdString, Utils.PaytmCredentials.TestMerchantID, txnTokenString, "1.00", callBackUrl);
//
//
//
//
//        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback(){
//
//
//            @Override
//            public void onTransactionResponse(Bundle bundle) {
//                Log.e(TAG, "Response (onTransactionResponse) : "+bundle.toString());
//
//                Log.e(TAG, "Response (onTransactionResponse) : "+bundle.toString());
//
//                String id=bundle.getString("TXNID");
//
//
//
//               // Toast.makeText(getActivity(),"su",Toast.LENGTH_SHORT);
//
//                if(bundle.getString("RESPCODE").equalsIgnoreCase("01"))
//                {
//
//
//                    passPurchaseDataToServer(id);
//                    Utils.showAlertWithSingle(getActivity(), "Transaction completed Successfully \n Transaction ID : "+id, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });
//
//
//                }
//                else {
//
//                    Utils.showAlertWithSingle(getActivity(), "Failed : "+id, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });
//
//
//                }
//
//                // executeTransactionStatus();
//            }
//            @Override
//            public void networkNotAvailable() {
//                //Log.e(TAG, "network not available ");
//                // executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Network not available", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//            @Override
//            public void onErrorProceed(String s) {
//                // Log.e(TAG, " onErrorProcess "+s.toString());
//                // executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Processing error", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//            @Override
//            public void clientAuthenticationFailed(String s) {
//                //Log.e(TAG, "Clientauth "+s);
//                //executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Authentication error", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//            @Override
//            public void someUIErrorOccurred(String s) {
//                // Log.e(TAG, " UI error "+s);
//
//                Utils.showAlertWithSingle(getActivity(), "UI error", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//                // executeTransactionStatus();
//            }
//            @Override
//            public void onErrorLoadingWebPage(int i, String s, String s1) {
//                //Log.e(TAG, " error loading web "+s+"--"+s1);
//                // executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Web page loading error", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//            @Override
//            public void onBackPressedCancelTransaction() {
//                // Log.e(TAG, "backPress ");
//                 //executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Pressed back button", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//            @Override
//            public void onTransactionCancel(String s, Bundle bundle) {
//                // Log.e(TAG, " transaction cancel "+s);
//                // executeTransactionStatus();
//
//                Utils.showAlertWithSingle(getActivity(), "Cancelled transaction", new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//        });
//        transactionManager.setShowPaymentUrl(host + "theia/api/v1/showPaymentPage");
//        transactionManager.setAppInvokeEnabled(false);
//        transactionManager.startTransaction(getActivity(), ActivityRequestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
       // super.onActivityResult(requestCode, resultCode, data);

        Utils.showAlertWithSingle(getActivity(), "Hi hello how are you", new DialogEventListener() {
            @Override
            public void onPositiveButtonClicked() {

            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });

        Log.e(TAG ," result code "+resultCode);

        Toast.makeText(getActivity(), "getResponse", Toast.LENGTH_SHORT).show();

        Toast.makeText(getActivity(), data.getStringExtra("nativeSdkForMerchantMessage")
                + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        // -1 means successful  // 0 means failed
        // one error is - nativeSdkForMerchantMessage : networkError
       // super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Log.e(TAG, key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
                }
            }
            Log.e(TAG, " data "+  data.getStringExtra("nativeSdkForMerchantMessage"));
            Log.e(TAG, " data response - "+data.getStringExtra("response"));


            String trdata=data.getStringExtra("response");

            TransactionStatus transactionStatus=new GsonBuilder().create().fromJson(trdata,TransactionStatus.class);

            String trid=transactionStatus.getTxnid();





//
// data response - {"BANKNAME":"WALLET","BANKTXNID":"1395841115",
// "CHECKSUMHASH":"7jRCFIk6mrep+IhnmQrlrL43KSCSXrmM+VHP5pH0hekXaaxjt3MEgd1N9mLtWyu4VwpWexHOILCTAhybOo5EVDmAEV33rg2VAS/p0PXdk\u003d",
// "CURRENCY":"INR","GATEWAYNAME":"WALLET","MID":"EAc0553138556","ORDERID":"100620202152",
// "PAYMENTMODE":"PPI","RESPCODE":"01","RESPMSG":"Txn Success","STATUS":"TXN_SUCCESS",
// "TXNAMOUNT":"2.00","TXNDATE":"2020-06-10 16:57:45.0","TXNID":"20200610111212800110168328631290118"}

            Toast.makeText(getActivity(), data.getStringExtra("nativeSdkForMerchantMessage")
                    + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        }else{
            Log.e(TAG, " payment failed");
        }
    }

    //    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.e(TAG ," result code "+resultCode);
//        // -1 means successful  // 0 means failed
//        // one error is - nativeSdkForMerchantMessage : networkError
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == ActivityRequestCode && data != null) {
//            Bundle bundle = data.getExtras();
//            if (bundle != null) {
//                for (String key : bundle.keySet()) {
//                    Log.e(TAG, key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
//                }
//            }
//            Log.e(TAG, " data "+  data.getStringExtra("nativeSdkForMerchantMessage"));
//            Log.e(TAG, " data response - "+data.getStringExtra("response"));
//
//
//            String trdata=data.getStringExtra("response");
//
//            TransactionStatus transactionStatus=new GsonBuilder().create().fromJson(trdata,TransactionStatus.class);
//
//            String trid=transactionStatus.getTxnid();
//
//
//
//
//
////
//// data response - {"BANKNAME":"WALLET","BANKTXNID":"1395841115",
//// "CHECKSUMHASH":"7jRCFIk6mrep+IhnmQrlrL43KSCSXrmM+VHP5pH0hekXaaxjt3MEgd1N9mLtWyu4VwpWexHOILCTAhybOo5EVDmAEV33rg2VAS/p0PXdk\u003d",
//// "CURRENCY":"INR","GATEWAYNAME":"WALLET","MID":"EAc0553138556","ORDERID":"100620202152",
//// "PAYMENTMODE":"PPI","RESPCODE":"01","RESPMSG":"Txn Success","STATUS":"TXN_SUCCESS",
//// "TXNAMOUNT":"2.00","TXNDATE":"2020-06-10 16:57:45.0","TXNID":"20200610111212800110168328631290118"}
//
//            Toast.makeText(this, data.getStringExtra("nativeSdkForMerchantMessage")
//                    + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
//        }else{
//            Log.e(TAG, " payment failed");
//        }
//    }


//    public void getRazorPayOrder()
//    {
//
//        try {
//
//            int a=(int)total_amount;
//
//            Random r = new Random();
//            int i1 = r.nextInt(9999 - 1000) + 1000;
//
//
//            Map<String,Object> jsonObject1 = new HashMap<>();
//            jsonObject1.put("receipt", "recipt"+i1);
//            jsonObject1.put("amount", a*100);
//            jsonObject1.put("currency", currency.getData().getCurrency_code());
//
//
//
//            RestService.RestApiInterface client = RestService.getRazorPayClientClient();
//
//            Call<JsonObject> jsonObjectCall = client.postOrders(jsonObject1);
//
//            jsonObjectCall.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                    if(response!=null)
//                    {
//
//                        if(response.body()!=null)
//                        {
//
//                            //  Toast.makeText(getActivity(),response.body().toString(),Toast.LENGTH_SHORT).show();
//
//
//                            RazorPayOrder razorPayOrder=new GsonBuilder().create().fromJson(response.body().toString(),RazorPayOrder.class);
//
//
//                            startPayment(razorPayOrder);
//
//
//
//
//                        }
//                        else {
//
//                            try {
//
//                                byte[] inputStream = response.errorBody().bytes();
//
//
//                                String str = new String(inputStream, StandardCharsets.UTF_8);
//
//                                //Toast.makeText(PurchaseActivity.this,str,Toast.LENGTH_SHORT).show();
//
//                            }catch (Exception e)
//                            {
//
//                            }
//
//
//
//                        }
//
//
//                    }
//
//
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                    Toast.makeText(getActivity(),t.toString(),Toast.LENGTH_SHORT).show();
//
//
//                }
//            });
//
//            //secret id : lvhUGQCw5xOkb7w70DKHkVOd
//
//            // {"id":"order_GsPkjrBYI0URJq","entity":"order","amount":500,"amount_paid":0,"amount_due":500,"currency":"INR","receipt":"sldjfs","offer_id":null,"status":"created","attempts":0,"notes":[],"created_at":1617008845}
//
//
//        }catch (Exception e)
//        {
//
//
//        }
//    }






    public void startPayment(RazorPayOrder razorPayOrder)
    {
        int a=(int)total_amount;

        try {
            JSONObject options = new JSONObject();

            options.put("name", "Century gate");
            options.put("description", "");
//            options.put("image", "http://mysaving.in/images/saveicon.png");
            options.put("image", "https://centroidsolutions.in/IntegraAccount/images/saveicon.png");
            options.put("order_id", razorPayOrder.getId());//from response of step 3.
            options.put("theme.color", "#252c45");
            options.put("currency", currency.getData().getCurrency_code());
            options.put("amount",a*100 );//pass amount in currency subunits
            options.put("prefill.email", email);
            options.put("prefill.contact",phonenumber);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

           // checkout.open(getActivity(), options);

        } catch(Exception e) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e);
        }
    }






//    public void getMemberData()
//    {
//
//
//
//        String key=  new PreferenceHelper(getActivity()).getData(Utils.userkey);
//
//
//        //String key=  new PreferenceHelper(getActivity()).getData(Utils.userkey);
//
//
//
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");
//
//
//        Map<String,String> params=new HashMap<>();
//         params.put("timestamp",Utils.getTimestamp());
//
//        new RequestHandler(getActivity(), params, new ResponseHandler() {
//            @Override
//            public void onSuccess(String data) {
//                progressFragment.dismiss();
//                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
//                if(data!=null)
//                {
//
////                    Member member=new GsonBuilder().create().fromJson(data,Member.class);
////
////
////                    if(member.getData()!=null)
////                    {
////
////                        memberData=member.getData();
////
////
////
////                    }
//
//
//
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(String err) {
//                progressFragment.dismiss();
//
//                Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();
//
//            }
//        },Utils.WebServiceMethodes.showMemberDetails, Request.Method.GET).submitRequest();











//
//
//        Call<Member> jsonObjectCall=client.getPositionBeforePurchase(key
//        );
//
//        jsonObjectCall.enqueue(new Callback<Member>() {
//            @Override
//            public void onResponse(Call<Member> call, Response<Member> response) {
//
//                if(response!=null)
//                {
//
//                    if(response.body()!=null)
//                    {
//
//                        Member member=response.body();
//
//
//                        if(member.getData()!=null)
//                        {
//
//                            memberData=member.getData();
//
//
//
//                        }
//
//
//
//
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Member> call, Throwable t) {
//
//            }
//        });
   // }








    public void passPurchaseDataToServer(String s)
    {

        //@Field("cash_transaction_id")String cash_transaction_id


//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
         params.put("cash_transaction_id",s);
         params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
              //  progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {
//                            if(moreAdapter!=null)
//                            {
//
//                                moreAdapter.showReferDialog();
//                            }
                        JSONObject jsonObject_data=jsonObject.getJSONObject("data");
                        String billno=jsonObject_data.getString("bill_no");
                        String billprefix=jsonObject_data.getString("billno_prefix");

                        Intent intent=new Intent(getActivity(), InvoiceActivity.class);
                        intent.putExtra("settingsdata",settingsData1);
                        intent.putExtra("bill",billprefix+" "+billno);
                        intent.putExtra("currency",currency);
                        intent.putExtra("actualamount",rs);
                        intent.putExtra("countryid",countryid);
                        intent.putExtra("stateid",stateid);
                        intent.putExtra("email",email);
                        intent.putExtra("name",name);
                        intent.putExtra("tid",s);
                        getActivity().startActivity(intent);


                    }
                    else {

                       // Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();

Utils.showAlertWithSingle(getActivity(), "Uploading transaction failed", new DialogEventListener() {
    @Override
    public void onPositiveButtonClicked() {

    }

    @Override
    public void onNegativeButtonClicked() {

    }
});


                    }



                }catch (Exception e)
                {

                }




            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();

                Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.addSalesInfo, Request.Method.POST).submitRequest();









    }




}
