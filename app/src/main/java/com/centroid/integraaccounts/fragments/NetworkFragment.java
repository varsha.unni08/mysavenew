package com.centroid.integraaccounts.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
//import com.cashfree.pg.api.CFPaymentGatewayService;
//import com.cashfree.pg.core.api.CFSession;
//import com.cashfree.pg.core.api.CFTheme;
//import com.cashfree.pg.core.api.callback.CFCheckoutResponseCallback;
//import com.cashfree.pg.core.api.utils.CFErrorResponse;
//import com.cashfree.pg.ui.api.CFDropCheckoutPayment;
//import com.cashfree.pg.ui.api.CFPaymentComponent;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.NetWorkSliderAdapter;
import com.centroid.integraaccounts.adapter.NetworkmainAdapter;
import com.centroid.integraaccounts.com.TokenResult.Tokendata;
import com.centroid.integraaccounts.data.EnglishNumberToWords;
import com.centroid.integraaccounts.data.SecuredDataHelper;
import com.centroid.integraaccounts.data.domain.Currency;
import com.centroid.integraaccounts.data.domain.MainSettings;
import com.centroid.integraaccounts.data.domain.MemberData;
import com.centroid.integraaccounts.data.domain.NetworkDashboard;
import com.centroid.integraaccounts.data.domain.NetworkData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.RazorPayOrder;
import com.centroid.integraaccounts.data.domain.SettingsData;
import com.centroid.integraaccounts.data.domain.Slider;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.paymentdata.domain.CashFreeTransactionData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.transaction.TransactionData;
import com.centroid.integraaccounts.views.AppRenewalActivity;
import com.centroid.integraaccounts.views.InvoiceActivity;
import com.centroid.integraaccounts.webserviceHelper.PaymentCheckSumRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.PaymentRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
//import com.paytm.pgsdk.TransactionManager;
//import com.razorpay.Checkout;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.centroid.integraaccounts.Constants.Utils.PaytmCredentials.ActivityRequestCode;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworkFragment extends Fragment  {

    TransactionData transactionStatus;


    public NetworkFragment() {
        // Required empty public constructor
    }

    View v;

    String url="",imgurl;
    String description="";

    RecyclerView recycler;
    TextView txtnodata,txtnoConnection;
    RelativeLayout layout_nodata;
    Button btnpurchase,btnPurchaseFreemember;

    ImageView imgNetwork;

    public static final int purchasecode=111;

    String arr[]={"Dashboard","Share link","Change position"};

    String countryid="0",stateid="0";

    String name="";

    Currency currency=null;
    double total_amount=0;


    ViewPager viewpager;
    TabLayout tabslayout;
    String rs=  "";

    String email="",phonenumber="";

    MemberData memberData=null;
    SettingsData settingsData1=null;

    String orderIdString="",txnTokenString,TAG="sdkf";

    NetworkData networkDashboard=null;
    LinearLayout layout_noconnection,layout_head;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_network, container, false);
        recycler=v.findViewById(R.id.recycler);
        txtnodata=v.findViewById(R.id.txtnodata);
        layout_nodata=v.findViewById(R.id.layout_nodata);
        btnpurchase=v.findViewById(R.id.btnpurchase);
        viewpager=v.findViewById(R.id.viewpager);
        tabslayout=v.findViewById(R.id.tabslayout);
        imgNetwork=v.findViewById(R.id.imgNetwork);
        btnPurchaseFreemember=v.findViewById(R.id.btnPurchaseFreemember);
        layout_noconnection=v.findViewById(R.id.layout_noconnection);
        layout_head=v.findViewById(R.id.layout_head);
        txtnoConnection=v.findViewById(R.id.txtnoConnection);

//        checkout=new Checkout();
//
//        checkout.setKeyID(Utils.razorkeyid);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        FrameLayout.LayoutParams layoutParams=(FrameLayout.LayoutParams)viewpager.getLayoutParams();

        double a=width/1.89349112;

        int h=(int)a;


        layoutParams.width=width;
        layoutParams.height=h;

        viewpager.setLayoutParams(layoutParams);




        getProfileData(0);
      //  getNetWorkData();
       // getSlider();
        getDataFromSettings();

       // getMemberData();

        btnpurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              getProfileData(1);



            }
        });
        btnPurchaseFreemember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProfileData(1);
            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabslayout.getTabAt(position).select();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        try {
//            CFPaymentGatewayService.getInstance().setCheckoutCallback(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return v;
    }

//
//    @Override
//    public void onPaymentVerify(String s) {
//        Utils.showAlertWithSingle(getActivity(), "Payment Success", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//
//        passPurchaseDataToServer(s);
//    }
//
//    @Override
//    public void onPaymentFailure(CFErrorResponse cfErrorResponse, String s) {
//
//        Utils.showAlertWithSingle(getActivity(), "Payment failed", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//
//    }

    @Override
    public void onResume() {
        super.onResume();
        getProfileData(0);
    }

    public void getDataFromSettings()
    {

//        ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"sdjfn");

        layout_noconnection.setVisibility(View.GONE);
        layout_head.setVisibility(View.VISIBLE);

        Map<String,String> params=new HashMap<>();


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

              //  progressFragment.dismiss();

                if(data!=null)
                {

                    MainSettings settingsData = new GsonBuilder().create().fromJson(data, MainSettings.class);


                    imgurl=Utils.linkimg+settingsData.getData().getNetwork_image();




                    //imgurl="https://mysaving.in/images/saveicon.png";

                    if(!TextUtils.isEmpty(imgurl))
                    {
                        Glide.with(getActivity()).load(imgurl).into(imgNetwork);

                    }




                }

            }

            @Override
            public void onFailure(String err) {
              //   progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();
                layout_noconnection.setVisibility(View.VISIBLE);
                layout_head.setVisibility(View.GONE);
                txtnoConnection.setText(err);
            }
        },Utils.WebServiceMethodes.getSettingsValue+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();








    }






    public void executeWebCall() {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    String responseData = "";
                    /* initialize an object */
                    JSONObject paytmParams = new JSONObject();

                    Random rm=new Random();
                    int m=rm.nextInt(1000-100)+1000;

                    /* body parameters */
                    JSONObject body = new JSONObject();

                    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                    paytmParams.put("order_amount", total_amount);

                    /* Enter your order id which needs to be check status for */
                    paytmParams.put("order_id", m+"");
                    paytmParams.put("order_currency","INR");

/**
 * Generate checksum by parameters we have in body
 * You can get Checksum JAR from https://developer.paytm.com/docs/checksum/
 * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
 */
                    //  String checksum = PaytmChecksum.generateSignature(body.toString(), Utils.PaytmCredentials.TestMerchantKey);
                    /* head parameters */
                    JSONObject head = new JSONObject();

                    /* put generated checksum value here */
                    head.put("customer_id", "1");
                    head.put("customer_name", name);
                    head.put("customer_email", "");
                    head.put("customer_phone", phonenumber);

                    JSONObject order_meta=new JSONObject();
                    order_meta.put("notify_url","https://test.cashfree.com");

                    /* prepare JSON string for request */
                    paytmParams.put("order_meta", order_meta);
                    paytmParams.put("customer_details", head);
                    String post_data = paytmParams.toString();

                    /* for Staging */
                    //  URL url = new URL("https://securegw-stage.paytm.in/v3/order/status");

                    /* for Production */
                    URL url = new URL("https://api.cashfree.com/pg/orders");

                    try {
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setRequestProperty ("x-client-id", "2110088e61c5abc8b72c74b033800112");
                        connection.setRequestProperty ("x-client-secret", "6d61be6225fcc9f4ea900b1fd8fb3aa6cc83b7ea");
                        connection.setRequestProperty ("x-api-version", "2022-01-01");
                        connection.setRequestProperty ("x-request-id", "Antony");

                        connection.setDoOutput(true);


                        DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
                        requestWriter.writeBytes(post_data);
                        requestWriter.close();

                        InputStream is = connection.getInputStream();
                        BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                        if ((responseData = responseReader.readLine()) != null) {
                            System.out.append("Response: " + responseData);
                        }

                        CashFreeTransactionData trs=new GsonBuilder().create().fromJson(responseData,CashFreeTransactionData.class);

                        startPaytmPayment(trs);

                       // doDropCheckoutPayment(trs);



                        // System.out.append("Request: " + post_data);
                        responseReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

//                    transactionStatus=new GsonBuilder().create().fromJson(responseData, TransactionData.class);
//
//
//
//                    // executor.shutdown();
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            showStatus();
//                        }
//                    });





                }catch (Exception e)
                {

                }



            }
        });

//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//
//
//
//getCheckSumData();
//
//
//
//
//
//            }
//        });

    }













    public void getSlider()
    {


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                if(data!=null)
                {

                    Slider slider=new GsonBuilder().create().fromJson(data,Slider.class);



//                        try{
//
//                            tabslayout.removeAllTabs();
//
//                            viewpager.setAdapter(new NetWorkSliderAdapter(getActivity(),slider.getData()));
//
//                            for (int i=0;i<slider.getData().size();i++)
//                            {
//                                tabslayout.addTab(tabslayout.newTab());
//                            }
//
//
//                        }catch (Exception e)
//                        {
//
//
//                        }






















                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSliderImages+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();









//
//
//        Call<Slider>sliderCall=client.getSliderImages(new PreferenceHelper(getActivity()).getData(Utils.userkey));
//
//        sliderCall.enqueue(new Callback<Slider>() {
//            @Override
//            public void onResponse(Call<Slider> call, Response<Slider> response) {
//
//             if(response!=null)
//             {
//
//                 if(response.body()!=null)
//                 {
//
//viewpager.setAdapter(new NetWorkSliderAdapter(getActivity(),response.body().getData()));
//
//for (int i=0;i<response.body().getData().size();i++)
//{
//    tabslayout.addTab(tabslayout.newTab());
//}
//
//
//                 }
//
//
//
//
//             }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Slider> call, Throwable t) {
//
//            }
//        });
    }




    public void getProfileData(int code)
    {



//       final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");
        layout_noconnection.setVisibility(View.GONE);
        layout_head.setVisibility(View.VISIBLE);

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
              //  progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


               // progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {

                                countryid=profiledata.getData().getCountryId();

                                stateid=profiledata.getData().getStateId();
                                name=profiledata.getData().getFullName();
                                email=profiledata.getData().getEmailId();
                                phonenumber=profiledata.getData().getMobile();
                                if(code==1) {

                                    payAmount();
                                }
                                else {

                                    getNetWorkData();

                                }
                               // getCurrencyCode();

                            }





                        }
                        else {

                         //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(getActivity(), "failed", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });


                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();

             //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

                Utils.showAlertWithSingle(getActivity(), err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });

                layout_noconnection.setVisibility(View.VISIBLE);
                layout_head.setVisibility(View.GONE);
                txtnoConnection.setText(err);


            }
        },Utils.WebServiceMethodes.getUserDetails+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();


    }



public void getCurrencyCode()
{


    ProgressFragment progressFragment=new ProgressFragment();
    progressFragment.show(getChildFragmentManager(),"sdjfn");



    Map<String,String> params=new HashMap<>();


    new RequestHandler(getActivity(), params, new ResponseHandler() {
        @Override
        public void onSuccess(String data) {

            progressFragment.dismiss();

            if(data!=null)
            {


                try{


                    currency=new GsonBuilder().create().fromJson(data,Currency.class);

                    if(currency.getStatus()==1)
                    {




                        payAmount();




                    }
                    else {

                     //   Toast.makeText(getContext()," failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(getActivity(), "failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }



                }catch (Exception e)
                {

                }



            }

        }

        @Override
        public void onFailure(String err) {
            progressFragment.dismiss();

            //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

        }
    },Utils.WebServiceMethodes.getCurrencyByCountryID+"?="+countryid+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();




}








    public void payAmount()
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{


                        MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                        if(settingsData.getStatus()==1)
                        {

                            settingsData1=settingsData.getData();





                            rs=  settingsData1.getOneBvRequiredAmount().trim();







                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            ((AppCompatActivity)getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int height = displayMetrics.heightPixels;
                            int width = displayMetrics.widthPixels;
                            final Dialog dialog = new Dialog(getActivity());
                            dialog.setContentView(R.layout.layout_paymentsummary);

                            int b=(int)(height/1.2);
                            dialog.getWindow().setLayout(width - 50,b );

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                            TextView txtActualPrice=dialog.findViewById(R.id.txtActualPrice);
                            LinearLayout layout_actualprice=dialog.findViewById(R.id.layout_actualprice);
                            TextView txtSgst=dialog.findViewById(R.id.txtSgst);
                            TextView txtCgst=dialog.findViewById(R.id.txtCgst);
                            TextView txtIgst=dialog.findViewById(R.id.txtIgst);
                            TextView txtTotal=dialog.findViewById(R.id.txtTotal);
                            TextView txtYourPosition=dialog.findViewById(R.id.txtYourPosition);
                            LinearLayout layout_yourposition=dialog.findViewById(R.id.layout_yourposition);
                            LinearLayout layout_Igst=dialog.findViewById(R.id.layout_Igst);

                            LinearLayout layout_sgst=dialog.findViewById(R.id.layout_sgst);
                            LinearLayout layout_cgst=dialog.findViewById(R.id.layout_cgst);


                            Button btnSubmit=dialog.findViewById(R.id.btnSubmit);



                            if(memberData!=null)
                            {
                                txtYourPosition.setText(memberData.getPositionNext());
                            }
                            else {

                                layout_yourposition.setVisibility(View.GONE);
                            }





                            btnSubmit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();

                                    // getRazorPayOrder();

                                    Calendar c = Calendar.getInstance();
                                    SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
                                    String date = df.format(c.getTime());
                                    Random rand = new Random();
                                    int min =1000, max= 9999;
// nextInt as provided by Random is exclusive of the top value so you need to add 1
                                    int randomNum = rand.nextInt((max - min) + 1) + min;
                                    orderIdString =  date+String.valueOf(randomNum);

                                    //executeWebCall();




                                    Dialog dialog_account=new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                                    dialog_account.setContentView(R.layout.layout_account);

                                    Button btnOk= dialog_account.findViewById(R.id.btnOk);

                                    btnOk.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog_account.dismiss();
                                        }
                                    });

                                    dialog_account.show();









                                    //new Sampleasync().execute("");

                                //    generateTransaction();


                                }
                            });


                            if (countryid.equalsIgnoreCase("1"))
                            {
                                //layout_actualprice.setVisibility(View.GONE);

                                if (stateid.equalsIgnoreCase("12"))
                                {


                                    double actualvalue=Double.parseDouble(rs);

                                    double sgstvalue=actualvalue*Double.parseDouble(settingsData1.getSgst())/100;

                                    double cgstvalue=actualvalue*Double.parseDouble(settingsData1.getCgst())/100;

                                    txtActualPrice.setText(rs+" INR ");
                                    txtSgst.setText(sgstvalue+" INR ");
                                    txtCgst.setText(cgstvalue+" INR ");
                                    layout_Igst.setVisibility(View.GONE);

                                    double t=actualvalue+sgstvalue+cgstvalue;


                                    txtTotal.setText(Math.round(t)+" INR ");
                                    total_amount=Math.round(t);
                                }
                                else {
                                    double actualvalue=Double.parseDouble(rs.trim());

                                    txtActualPrice.setText(rs+" INR ");
                                    layout_sgst.setVisibility(View.GONE);
                                    layout_cgst.setVisibility(View.GONE);
                                    layout_Igst.setVisibility(View.VISIBLE);
                                    double Igstvalue=actualvalue*Double.parseDouble(settingsData1.getIgst())/100;

                                    txtIgst.setText(Igstvalue+" INR ");

                                    double t=actualvalue+Igstvalue;

                                    txtTotal.setText(Math.round(t)+" INR ");

                                    total_amount=Math.round(t);
                                }



                            }
                            else {

                                layout_sgst.setVisibility(View.GONE);
                                layout_cgst.setVisibility(View.GONE);
                                layout_Igst.setVisibility(View.GONE);
                                layout_actualprice.setVisibility(View.GONE);

                                double actualvalue=Double.parseDouble(rs);

                                double othertax=Double.parseDouble(settingsData1.getOtherTax());

                                double otprice=actualvalue*othertax/100;

                                double tot=actualvalue+otprice;

                                double conversionval=Double.parseDouble("1.00");



                                double t=tot*conversionval;




                                total_amount=Math.round(t);



                                txtTotal.setText(total_amount+" INR ");

                            }









                            dialog.show();







                        }
                        else {





                        }



                    }catch (Exception e)
                    {

                    }

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSettingsValue+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();














    }












    public void startPaytmPayment (CashFreeTransactionData trs){


//        try {
//            CFSession cfSession = new CFSession.CFSessionBuilder()
//                    .setEnvironment(CFSession.Environment.PRODUCTION)
//                    .setOrderToken(trs.getOrderToken())
//                    .setOrderId(trs.getOrderId())
//                    .build();
//            CFPaymentComponent cfPaymentComponent = new CFPaymentComponent.CFPaymentComponentBuilder()
//                    // Shows only Card and UPI modes
//                    .add(CFPaymentComponent.CFPaymentModes.CARD)
//                    .add(CFPaymentComponent.CFPaymentModes.UPI)
//                    .build();
//            // Replace with your application's theme colors
//            CFTheme cfTheme = new CFTheme.CFThemeBuilder()
//                    .setNavigationBarBackgroundColor("#fc2678")
//                    .setNavigationBarTextColor("#ffffff")
//                    .setButtonBackgroundColor("#fc2678")
//                    .setButtonTextColor("#ffffff")
//                    .setPrimaryTextColor("#000000")
//                    .setSecondaryTextColor("#000000")
//                    .build();
//            CFDropCheckoutPayment cfDropCheckoutPayment = new CFDropCheckoutPayment.CFDropCheckoutPaymentBuilder()
//                    .setSession(cfSession)
//                    .setCFUIPaymentModes(cfPaymentComponent)
//                    .setCFNativeCheckoutUITheme(cfTheme)
//                    .build();
//            CFPaymentGatewayService gatewayService = CFPaymentGatewayService.getInstance();
//            gatewayService.doPayment(getActivity(), cfDropCheckoutPayment);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }


    }








    public void getBitmapData(String url,String description)
    {

        this.url=url;
        this.description=description;

        //this.url="https://mysaving.in/images/saveicon.png";

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"df");


        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap1 =null;

                try {

                    String permission="";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                        permission=Manifest.permission.READ_MEDIA_IMAGES;

                    }
                    else{

                        permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    }




                    if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {

                        URL ur = new URL(url);
                        try {
                            bitmap1 = BitmapFactory.decodeStream(ur.openConnection().getInputStream());
                        }catch (Exception e)
                        {

                        }

                        if(bitmap1!=null) {

                            String d = Calendar.getInstance().getTimeInMillis() + "";


                            Intent shareIntent;
                            //Bitmap bitmap1= BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
                            String path = getActivity().getExternalCacheDir().getAbsolutePath() + "/" + d + ".png";
                            OutputStream out = null;
                            File file = new File(path);
                            try {

                                if (!file.exists()) {
                                    file.createNewFile();
                                }


                                out = new FileOutputStream(file);
                                bitmap1.compress(Bitmap.CompressFormat.PNG, 100, out);
                                out.flush();
                                out.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            path = file.getPath();
                            // path=file.getPath();

                            Uri bmpUri = null;

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    progressFragment.dismiss();
                                }
                            });

                            String id = new PreferenceHelper(getActivity()).getData(Utils.userkey);


                            final String link = Utils.newdomain + "/signup?sponserid=" + id;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                bmpUri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

                            } else {


                                bmpUri = Uri.parse("file://" + path);
                            }
                            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, " \n" + link);
                            shareIntent.setType("image/png");
                            startActivity(Intent.createChooser(shareIntent, "Share with"));
                        }
                        else {

                            String id = new PreferenceHelper(getActivity()).getData(Utils.userkey);


                            final String link = Utils.newdomain + "/signup?sponserid=" + id;


                            Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, " \n"+link);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        }


                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressFragment.dismiss();
                            }
                        });

                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                    }






                } catch(IOException e) {
                    System.out.println(e);
                }






            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        getBitmapData(url,description);
    }





    public void passPurchaseDataToServer(String trid)
    {


//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("cash_transaction_id",trid);
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
             //   progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {
//                            if(moreAdapter!=null)
//                            {
//
//                                moreAdapter.showReferDialog();
//                            }
                        JSONObject jsonObject_data=jsonObject.getJSONObject("data");
                        String billno=jsonObject_data.getString("bill_no");
                        String billprefix=jsonObject_data.getString("billno_prefix");

                        Intent intent=new Intent(getActivity(), InvoiceActivity.class);
                        intent.putExtra("settingsdata",settingsData1);
                        intent.putExtra("bill",billprefix+" "+billno);
                        intent.putExtra("currency",currency);
                        intent.putExtra("actualamount",rs);
                        intent.putExtra("countryid",countryid);
                        intent.putExtra("stateid",stateid);
                        intent.putExtra("email",email);
                        intent.putExtra("name",name);
                        intent.putExtra("tid",trid);
                        getActivity().startActivity(intent);


                    }
                    else {

                      //  Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(getActivity(), "Uploading transaction failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });



                    }



                }catch (Exception e)
                {

                }




            }

            @Override
            public void onFailure(String err) {
             //   progressFragment.dismiss();

              //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();


                Utils.showAlertWithSingle(getActivity(), err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });




            }
        },Utils.WebServiceMethodes.addSalesInfo, Request.Method.POST).submitRequest();








    }




    public void showStatus()
    {
        String trid=transactionStatus.getBody().getTxnId();
        if(transactionStatus.getBody().getResultInfo().getResultCode().equalsIgnoreCase("01")) {

            passPurchaseDataToServer(trid);
           // showInvoiceDialog( trid);
        }
        else {

            Toast.makeText(getActivity(),"Transaction failed",Toast.LENGTH_SHORT).show();
        }
    }




    public void showInvoiceDialog(String trid)
    {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            double h = displayMetrics.heightPixels / 1.2;
            int height = (int) h;
            int width = displayMetrics.widthPixels;
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_invoiceformat);
            TextView txtbillno = dialog.findViewById(R.id.txtbillno);
            TextView txtDate = dialog.findViewById(R.id.txtDate);
            TextView txtBuyer = dialog.findViewById(R.id.txtBuyer);
            TextView txtActualRate = dialog.findViewById(R.id.txtActualRate);
            TextView txtFullActualRate = dialog.findViewById(R.id.txtFullActualRate);
            TextView txtAmountinWords = dialog.findViewById(R.id.txtAmountinWords);
            TextView txtGSTHead = dialog.findViewById(R.id.txtGSTHead);
            LinearLayout layout_gst = dialog.findViewById(R.id.layout_gst);
            TextView txtsgstPrice = dialog.findViewById(R.id.txtsgstPrice);
            TextView txtcgstPrice = dialog.findViewById(R.id.txtcgstPrice);
            TextView txtIGST = dialog.findViewById(R.id.txtIGST);
            TextView txtNetTotalAmount = dialog.findViewById(R.id.txtNetTotalAmount);
            Button btnDownload = dialog.findViewById(R.id.btnDownload);
            Calendar calendar = Calendar.getInstance();
            int d = calendar.get(Calendar.DAY_OF_MONTH);
            int m = calendar.get(Calendar.MONTH) + 1;
            int y = calendar.get(Calendar.YEAR);
            txtbillno.setText("Bill no. : " + trid);
            txtDate.setText("Date : " + d + "-" + m + "-" + y);

            if (countryid.equalsIgnoreCase("1")) {
                if (stateid.equalsIgnoreCase("12")) {
                    double actualvalue = Double.parseDouble(rs);
                    double sgstvalue = actualvalue * Double.parseDouble(settingsData1.getSgst()) / 100;
                    double cgstvalue = actualvalue * Double.parseDouble(settingsData1.getCgst()) / 100;
                    txtActualRate.setText(rs + " ");
                    txtFullActualRate.setText(rs + " ");
                    txtsgstPrice.setText(sgstvalue + " ");
                    txtcgstPrice.setText(cgstvalue + " ");
                    txtIGST.setVisibility(View.INVISIBLE);
                    double t = actualvalue + sgstvalue + cgstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                } else {
                    double actualvalue = Double.parseDouble(rs);
                    txtActualRate.setText(rs + " ");
                    layout_gst.setVisibility(View.INVISIBLE);
                    double Igstvalue = actualvalue * Double.parseDouble(settingsData1.getIgst()) / 100;
                    txtIGST.setText(Igstvalue + " ");
                    double t = actualvalue + Igstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                }

            } else {

                txtGSTHead.setText("Other Tax");
                layout_gst.setVisibility(View.INVISIBLE);
                txtsgstPrice.setVisibility(View.GONE);
                txtcgstPrice.setVisibility(View.GONE);
                txtIGST.setVisibility(View.VISIBLE);
                double actualvalue = Double.parseDouble(rs);
                double othertax = Double.parseDouble(settingsData1.getOtherTax());
                double otprice = actualvalue * othertax / 100;
                txtIGST.setText(otprice + "");
                double tot = actualvalue + otprice;
                double conversionval = Double.parseDouble(currency.getData().getConversion_value());
                double t = tot * conversionval;
                total_amount = Math.round(t);
                txtNetTotalAmount.setText(total_amount + " ");

            }


            int a=(int)total_amount;
            String d1 = String.valueOf(a);

            long n = Long.parseLong(d1);
            txtAmountinWords.setText("Amount in words : " + EnglishNumberToWords.convert(n) + " " + currency.getData().getCurrency_code());


            dialog.getWindow().setLayout(width, height);

            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {

                        btnDownload.setVisibility(View.GONE);

                        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                            DisplayMetrics metrics = new DisplayMetrics();
                            getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;

                            View v1 = dialog.getWindow().getDecorView().getRootView();


                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);


                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();

                            File fp = new File(getActivity().getExternalCacheDir() + "/Save/Invoice");


                            if (!fp.exists()) {
                                fp.mkdirs();
                            }

                            File f = new File(fp.getAbsolutePath(), "invoice" + ts + ".png");
                            if (!f.exists()) {
                                f.createNewFile();

                            } else {

                                f.delete();
                                f.createNewFile();
                            }


                            FileOutputStream output = new FileOutputStream(f);


                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                            output.close();


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", f);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(photoURI, "image/*");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(intent);

                            } else {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(f), "image/*");
                                startActivity(intent);
                            }

                            dialog.dismiss();
                        }
                        else {

                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);
                        }



                    }catch (Exception e)
                    {

                    }
                }
            });


            dialog.show();
        }catch (Exception e)
        {
            Log.e("TAAGG",e.toString());
        }
    }



    public void getNetWorkData()
    {


        String languagedata = LocaleHelper.getPersistedData(getActivity(), "en");
        Context context= LocaleHelper.setLocale(getActivity(), languagedata);

       Resources resources=context.getResources();


       String arr[]={resources.getString(R.string.dashboard),resources.getString(R.string.sharelink),resources.getString(R.string.changeposition)};


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

//Toast.makeText(getActivity(),data,Toast.LENGTH_SHORT).show();
                if(data!=null)
                {

                    if(data!=null)
                    {

                        try{

                            JSONObject jsonObject=new JSONObject(data);

                            if(jsonObject.getInt("status")==1)
                            {
                                getSlider();

                                recycler.setVisibility(View.VISIBLE);
                                viewpager.setVisibility(View.VISIBLE);
                                txtnodata.setVisibility(View.GONE);
                                viewpager.setVisibility(View.VISIBLE);
                                tabslayout.setVisibility(View.VISIBLE);


                                JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                networkDashboard=new GsonBuilder().create().fromJson(jsonObject1.toString(),NetworkData.class);

                                if(!networkDashboard.getMember_status().equalsIgnoreCase("active"))
                                {

                                    btnPurchaseFreemember.setVisibility(View.VISIBLE);
                                }
                                else {
                                    btnPurchaseFreemember.setVisibility(View.GONE);
                                }

                                if(networkDashboard!=null)
                                {

                                    //Log.e("TAG","Data exist");

                                    List<NetworkDashboard>networkData=new ArrayList<>();

                                    for (String a:arr)
                                    {
                                        NetworkDashboard networkData1=new NetworkDashboard();
                                        networkData1.setData(a);
                                        networkData1.setSelected(0);
                                        networkData.add(networkData1);
                                    }

                                    recycler.setLayoutManager(new LinearLayoutManager(getContext()));
                                    recycler.setAdapter(new NetworkmainAdapter(NetworkFragment.this,getActivity(),networkData,networkDashboard));



                                }


                            }
                            else {

                                recycler.setVisibility(View.GONE);
                                layout_nodata.setVisibility(View.VISIBLE);

                                viewpager.setVisibility(View.GONE);
                                tabslayout.setVisibility(View.GONE);

                                viewpager.setVisibility(View.GONE);


                            }




                        }catch (Exception e)
                        {


                        }









                    }












                }

            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.showMemberDetails+"?mobile="+phonenumber+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }


    public void updatePositionNext(String position,int id)
    {

       // @Field("position")String position,@Field("id")int id

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
         params.put("position",position);
        params.put("id",id+"");
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {

                     //   Toast.makeText(getActivity(),"Position updated successfully",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(getActivity(), "Position updated successfully", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }
                    else {
                        Utils.showAlertWithSingle(getActivity(), "Position updation failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                      //  Toast.makeText(getActivity(),"Position updation failed",Toast.LENGTH_SHORT).show();
                    }




                }catch (Exception e)
                {


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Utils.showAlertWithSingle(getActivity(), err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });

              //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updatePositionNext, Request.Method.POST).submitRequest();









    }



    public void updatePosition(String position,int id)
    {

       // @Field("position")String position,@Field("id")int id

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
         params.put("position",position);
        params.put("id",id+"");
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(getActivity(), params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {

                    //    Toast.makeText(getActivity(),"Position updated successfully",Toast.LENGTH_SHORT).show();
                        Utils.showAlertWithSingle(getActivity(), "Position updated successfully", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }
                    else {


                     //   Toast.makeText(getActivity(),"Position updation failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(getActivity(), "Position updation failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }




                }catch (Exception e)
                {


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

              //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

                Utils.showAlertWithSingle(getActivity(), err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });



            }
        },Utils.WebServiceMethodes.updatePosition, Request.Method.POST).submitRequest();










    }

}
