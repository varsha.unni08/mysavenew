package com.centroid.integraaccounts.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;

import com.centroid.integraaccounts.adapter.LedgerHeadAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.BudgetListActivity;
import com.centroid.integraaccounts.views.IncomexpenditureActivity;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class BottomChartFragment extends BottomSheetDialogFragment {

    public BottomChartFragment() {
        super();
    }

    View v;

    TextView txtDate,txtincpercent,txtexppercent;
    double totalexp=0,totalinc=0;

  //  PieChart pieChart;

    float time[] = {55, 95, 30 , 360 - (55+95+30)};
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    ProgressBar progressBar;

    List<CommonData> cmfiltered_income, cmfiltered_expense;
    String month_selected = "", yearselected = "";

    List<LedgerAccount> ledgerAccountsexpense = new ArrayList<>();

    List<LedgerAccount> ledgerAccountsincome = new ArrayList<>();

    PieChart piechart;

    BarChart chart;

    Spinner spinnerYear;

    Map<Integer,List<Accounts>>accincome=new HashMap<>();

    Map<Integer,List<Accounts>>accexpense=new HashMap<>();


    int months[]={1,2,3,4,5,6,7,8,9,10,11,12};


   int MAX_X_VALUE = 12;
int MAX_Y_VALUE = 50;
  int MIN_Y_VALUE = 5;
 int GROUPS = 3;

    float BAR_SPACE = 0.05f;
     float BAR_WIDTH = 0.2f;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       v=inflater.inflate(R.layout.layout_bottomsheet,container,false);

        cmfiltered_income=new ArrayList<>();
        cmfiltered_expense=new ArrayList<>();

        progressBar=v.findViewById(R.id.progressBar);
        piechart=v.findViewById(R.id.piechart);
        chart=v.findViewById(R.id.barchart);
        spinnerYear=v.findViewById(R.id.spinnerYear);

        txtDate=v.findViewById(R.id.txtDate);
        txtincpercent=v.findViewById(R.id.txtincpercent);
        txtexppercent=v.findViewById(R.id.txtexppercent);


        Calendar calendar=Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        List<String> yeardata=new ArrayList<>();

        for (int i=year+5;i>=year;i--)
        {

            yeardata.add(i+"");
        }

        Collections.reverse(yeardata);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),   android.R.layout.simple_spinner_item, yeardata.toArray(new String[yeardata.size()]));

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYear.setAdapter(spinnerArrayAdapter);





        setupChart();

        spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                getAccounthead();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return v;
    }


    private void setupChart()
    {

        Calendar calendar=Calendar.getInstance();

        int mnth=calendar.get(Calendar.MONTH);


        int m=mnth+1;

        int year=calendar.get(Calendar.YEAR);

        month_selected = m + "";
        yearselected = year + "";







//        txtDate.setText(yearselected);

        getAccounthead();

    }
















    public void getAccounthead() {

        //layout_ledgerhead.setVisibility(View.VISIBLE);

        cmfiltered_expense.clear();
        cmfiltered_income.clear();

        List<CommonData> commonData = new DatabaseHelper(getActivity()).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            //Collections.reverse(commonData);


            for (CommonData cm : commonData) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Income account")) {
                        cmfiltered_income.add(cm);
                    }

                    if (acctype.equalsIgnoreCase("Expense account")) {
                        cmfiltered_expense.add(cm);
                    }


                } catch (Exception e) {

                }

            }




        }
        else {




        }

        checkIncomeHeads();
        // checkExpenseHeads();



    }


    private boolean isAccountAlreadyExist(List<Accounts>accounts, String setupid)
    {

        boolean a=false;
        for (Accounts acc:accounts) {

            if(acc.getACCOUNTS_setupid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }





    private void checkIncomeHeads()
    {


        String year=spinnerYear.getSelectedItem().toString();

        accincome.clear();
        List<Accounts> accountsSorted = Utils.sortAccountByYear(getActivity(),year);

        List<Accounts>accstring=new ArrayList<>();



        for (CommonData cmd:cmfiltered_income) {


            for (Accounts acc : accountsSorted) {


                if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cmd.getId())) {
                    accstring.add(acc);
                }



            }

        }




        try {

            for (int m:months)
            {

                List<Accounts>accountsList=new ArrayList<>();

                for (Accounts acc:accstring) {


                   if(acc.getACCOUNTS_month().equalsIgnoreCase(m+""))
                   {

                     accountsList.add(acc);



                   }






                }

                if(accountsList.size()>0) {

                    accincome.put(m, accountsList);
                }






            }










        } catch (Exception e) {

        }


//        if(ledgerAccountsincome.size()>0)
//        {
//
//            for (LedgerAccount ledgerAccount:ledgerAccountsincome) {
//
//                totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());
//
//
//
//            }
//
//        }

        checkExpenseHeads();

    }


    private void checkExpenseHeads() {


        String year=spinnerYear.getSelectedItem().toString();

        accexpense.clear();
        List<Accounts> accountsSorted = Utils.sortAccountByYear(getActivity(),year);

        List<Accounts>accstring=new ArrayList<>();



        for (CommonData cmd:cmfiltered_expense) {


            for (Accounts acc : accountsSorted) {


                if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cmd.getId())) {
                    accstring.add(acc);
                }



            }

        }




        try {

            for (int m:months)
            {

                List<Accounts>accountsList=new ArrayList<>();

                for (Accounts acc:accstring) {


                    if(acc.getACCOUNTS_month().equalsIgnoreCase(m+""))
                    {

                        accountsList.add(acc);



                    }






                }

                if(accountsList.size()>0) {

                    accexpense.put(m, accountsList);
                }






            }










        } catch (Exception e) {

        }

//        BarData data = createChartData();
//        configureChartAppearance();
//        prepareChartData(data);


       // updateBars();

        if(accexpense.size()>0||accincome.size()>0) {

            chart.setVisibility(View.VISIBLE);

            BarData data = new BarData(getXAxisValues(), getDataSet());
            chart.setData(data);
            chart.setDescription("My Chart");
            chart.animateXY(2000, 2000);
            chart.invalidate();
        }
        else {

            chart.setVisibility(View.INVISIBLE);
        }

    }







    private ArrayList<IBarDataSet> getDataSet() {
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();


        ArrayList<BarEntry> valueSet1 = new ArrayList<>();


        for (int i = 0; i < months.length; i++) {


            List<Accounts>entrylist=accincome.get(i+1);

            totalinc=0;

            if(entrylist!=null) {

                for (Accounts acc : entrylist) {

                    totalinc = totalinc + Double.parseDouble(acc.getACCOUNTS_amount());

                }

                String t = totalinc + "";
                Float totalf = Float.parseFloat(t);

                BarEntry v1e1 = new BarEntry(totalf, i); // Jan

                valueSet1.add(v1e1);
            }
        }


//






        ArrayList<BarEntry> valueSet2 = new ArrayList<>();


        for (int i = 0; i < months.length; i++) {


            List<Accounts>entrylist=accexpense.get(i+1);

            totalexp=0;

            if(entrylist!=null) {

                for (Accounts acc : entrylist) {

                    totalexp = totalexp + Double.parseDouble(acc.getACCOUNTS_amount());

                }

                String t = totalexp + "";
                Float totalf = Float.parseFloat(t);

                BarEntry v2e1 = new BarEntry(totalf, i); // Jan

                valueSet2.add(v2e1);
            }
        }




        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Income");
      
        barDataSet1.setColor(Color.rgb(0, 155, 0));
        barDataSet1.setDrawValues(false);
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Expense");
        barDataSet2.setDrawValues(false);
        barDataSet2.setColor(Color.rgb(155, 0, 100));
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }



    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();

        for (String a:arrmonth)
        {
            xAxis.add(a);
        }

        return xAxis;
    }





































}
