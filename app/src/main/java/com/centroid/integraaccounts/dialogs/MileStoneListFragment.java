package com.centroid.integraaccounts.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MileStoneManipulateAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.data.domain.MileStoneTarget;
import com.centroid.integraaccounts.views.NewTargetActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MileStoneListFragment extends DialogFragment {



    public MileStoneListFragment() {
        // Required empty public constructor
    }




    View v;
    RecyclerView recycler;

    ImageView imgcancel;

    List<MileStone>mileStones=new ArrayList<>();

    TextView txtNodata;

    String id="0";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_mile_stone_list, container, false);

        Bundle bb=this.getArguments();
        id=bb.getString("id");

        recycler=v.findViewById(R.id.recycler);
        imgcancel=v.findViewById(R.id.imgcancel);
        txtNodata=v.findViewById(R.id.txtNodata);

        imgcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });



       setupData();




        return v;

    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


    public void setupData()
    {
        List<CommonData> commondata_target=new DatabaseHelper(getActivity()).getData(Utils.DBtables.TABLE_MILESTONE);



        if(commondata_target.size()>0)
        {

            for(CommonData cmd:commondata_target)
            {

                String jsondata=cmd.getData();

                try{

                    JSONObject js3=new JSONObject(jsondata);

                    String amount=js3.getString("amount");
                    String start_date=js3.getString("start_date");
                    String end_date=js3.getString("end_date");
                    String targetid=js3.getString("targetid");

                    if(targetid.equalsIgnoreCase(id))
                    {
                        MileStone mileStone=new MileStone();
                        mileStone.setId(cmd.getId());
                        mileStone.setSelected(0);
                        mileStone.setEnd_date(end_date);
                        mileStone.setStart_date(start_date);
                        mileStone.setAmount(amount);
                        mileStones.add(mileStone);
                    }




                }catch (Exception e)
                {

                }


            }







        }


        if(mileStones.size()>0)
        {
            txtNodata.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);

            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
           // recycler.setAdapter(new MileStoneManipulateAdapter(getActivity(),mileStones,MileStoneListFragment.this));


        }
        else{

            txtNodata.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);
        }
    }




}