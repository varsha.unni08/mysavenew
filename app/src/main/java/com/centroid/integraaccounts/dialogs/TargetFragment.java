package com.centroid.integraaccounts.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.TargetCategoryAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.interfaces.TargetCategoryServices;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.centroid.integraaccounts.views.NewTargetActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class TargetFragment extends DialogFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    View v;

    TargetCategoryServices targetCategoryServices;

    public TargetFragment(TargetCategoryServices targetCategoryServices) {
        // Required empty public constructor

        this.targetCategoryServices=targetCategoryServices;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TargetFragment.
     */
    // TODO: Rename and change types and number of parameters


    RecyclerView recycler;
    Button btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_target, container, false);
        recycler=v.findViewById(R.id.recycler);
        btnAdd=v.findViewById(R.id.btnAdd);


        List<TargetCategory>tfc=new DatabaseHelper(getActivity()).getTargetCategory();

        if(tfc.size()>0)
        {

            recycler.setLayoutManager(new GridLayoutManager(getActivity(),3));
            recycler.setAdapter(new TargetCategoryAdapter(getActivity(),tfc,TargetFragment.this));


        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();

                ((NewTargetActivity)getActivity()).showNewTargetDialog();





            }
        });



        return v;
    }



    public void OnCategorySelected(TargetCategory tg)
    {
        if(targetCategoryServices!=null)
        {

            dismiss();

            targetCategoryServices.onSelectTargetCategory(tg);
        }



    }
}