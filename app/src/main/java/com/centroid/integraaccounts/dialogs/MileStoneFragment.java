package com.centroid.integraaccounts.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.interfaces.MileStoneListener;


public class MileStoneFragment extends DialogFragment {


    MileStoneListener ml;
    double targetamount=0;


    public MileStoneFragment(MileStoneListener mls, double targetamount) {
        // Required empty public constructor

        this.ml=mls;
        this.targetamount=targetamount;
    }


View v;
    EditText edtNumber;
    RecyclerView recycler;

    ImageView imgcancel,imgtick;

    TextView txttarget,txtTotalAmount,txtNumber;

    Button btnsubmit;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_mile_stone, container, false);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);


        imgtick=v.findViewById(R.id.imgtick);

        imgcancel=v.findViewById(R.id.imgcancel);

        recycler=v.findViewById(R.id.recycler);

        edtNumber=v.findViewById(R.id.edtNumber);

        txttarget=v.findViewById(R.id.txttarget);
        txtTotalAmount=v.findViewById(R.id.txtTotalAmount);

        btnsubmit=v.findViewById(R.id.btnsubmit);
        txtNumber=v.findViewById(R.id.txtNumber);



        txttarget.setText("Target amount : "+targetamount);


        txtNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




            }
        });



        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtNumber.getText().toString().equalsIgnoreCase(""))
                {
                    if(!edtNumber.getText().toString().equalsIgnoreCase("0"))
                    {






//                        int c=Integer.parseInt(edtNumber.getText().toString().trim());
//
//
//                        MileStoneDynamicAdapter mileStoneDynamicAdapter=new MileStoneDynamicAdapter(getActivity(),c);
//                        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//                        recycler.setAdapter(mileStoneDynamicAdapter);


                    }
                    else{



                    }


                }
                else{



                }



            }
        });


        return v;
    }


    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


}