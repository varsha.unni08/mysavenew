package com.centroid.integraaccounts.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.interfaces.MileStoneListener;
import com.centroid.integraaccounts.views.NewTargetActivity;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MileStoneEditFragment extends DialogFragment {

    MileStoneListener mileStoneListener;
    List<MileStone> ml=new ArrayList<>();

    public MileStoneEditFragment(MileStoneListener mileStoneListener,List<MileStone> ml) {
        // Required empty public constructor

        this.mileStoneListener=mileStoneListener;
        this.ml=ml;

    }


    View v;

    MileStone mileStone;


    Button btnsubmit,btndelete;

    TextView txtstartdatepick,txtenddatepick,txtMileStoneAdded,txtwarning;

    ImageView imgendDatepick,imgstartDatepick,imgdropdown;

    EditText edtNumber;

    String id="";

    int foredit=0,position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_mile_stone_edit, container, false);

        Bundle bundle=getArguments();

        if(bundle!=null) {

            mileStone = (MileStone) bundle.getSerializable("milestone");
            id=bundle.getString("targetid");
            foredit=bundle.getInt("foredit");
            position=bundle.getInt("position");
        }


        btnsubmit=v.findViewById(R.id.btnsubmit);
        txtstartdatepick=v.findViewById(R.id.txtstartdatepick);
        txtenddatepick=v.findViewById(R.id.txtenddatepick);
        imgendDatepick=v.findViewById(R.id.imgendDatepick);
        imgstartDatepick=v.findViewById(R.id.imgstartDatepick);
        txtMileStoneAdded=v.findViewById(R.id.txtMileStoneAdded);
        imgdropdown=v.findViewById(R.id.imgdropdown);

        btndelete=v.findViewById(R.id.btndelete);

        edtNumber=v.findViewById(R.id.edtNumber);

        txtwarning=v.findViewById(R.id.txtwarning);

        if(foredit==1) {

            if (!mileStone.getStart_date().equalsIgnoreCase("")) {

                txtstartdatepick.setText(mileStone.getStart_date());
            }


            if (!mileStone.getEnd_date().equalsIgnoreCase("")) {

                txtenddatepick.setText(mileStone.getEnd_date());
            }


            edtNumber.setText(mileStone.getAmount()+"");
            btnsubmit.setText("Update");

            foredit=1;

        }
        else{

            foredit=0;

            mileStone=new MileStone();

            btnsubmit.setText("Add");
        }

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMileStoneDate(1,0);
            }
        });
        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMileStoneDate(1,0);
            }
        });


        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMileStoneDate(0,0);
            }
        });
        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMileStoneDate(0,0);
            }
        });





        edtNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equalsIgnoreCase(""))
                {

                    String amount=charSequence.toString();

                    double d=Double.parseDouble(amount);

                    mileStone.setAmount(d+"");

                }
                else{


                   mileStone.setAmount("0");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatabaseHelper(getActivity()).deleteData(mileStone.getId(), Utils.DBtables.TABLE_MILESTONE);

                //mileStoneListFragment.setupData();

                dismiss();

            }
        });


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                txtwarning.setText("");
                txtwarning.setVisibility(View.GONE);

                Date stdate=null;
                Date eddate=null;
                Date   ml_enddate=null;
                SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");


                if(!mileStone.getStart_date().equalsIgnoreCase(""))
                {
                    if(!mileStone.getEnd_date().equalsIgnoreCase(""))
                    {

                        if(!mileStone.getAmount().equalsIgnoreCase(""))
                        {

                            try {

                                stdate=myFormat.parse(mileStone.getStart_date());
                                eddate=myFormat.parse(mileStone.getEnd_date());

                                boolean isStartdateValid=true;

                                if(ml.size()>0)
                                {

                                    int p=0;

                                    if(ml.size()>1)
                                    {
                                        if(foredit==1)
                                        {
                                            p=ml.size()-2;
                                        }
                                        else{
                                            p=ml.size()-1;
                                        }
                                        MileStone mileStoneData=ml.get(p);

                                        ml_enddate=myFormat.parse(mileStoneData.getEnd_date());

                                        if(stdate.before(ml_enddate))
                                        {

                                            isStartdateValid=false;
                                        }

                                        if(stdate.equals(ml_enddate))
                                        {

                                            isStartdateValid=false;
                                        }

                                    }
                                    else if(ml.size()==1)
                                    {

                                        p=ml.size()-1;

                                        if(foredit==0)
                                        {
                                            MileStone mileStoneData=ml.get(p);

                                            ml_enddate=myFormat.parse(mileStoneData.getEnd_date());

                                            if(stdate.before(ml_enddate))
                                            {

                                                isStartdateValid=false;
                                            }

                                            if(stdate.equals(ml_enddate))
                                            {

                                                isStartdateValid=false;
                                            }
                                        }
                                    }



//                                        p = ml.size() - 1;








                                }



                                if(isStartdateValid) {


                                    if (stdate.before(eddate)) {

                                        if (!stdate.equals(eddate)) {


                                            JSONObject js2 = new JSONObject();
                                            js2.put("amount", mileStone.getAmount());
                                            js2.put("start_date", mileStone.getStart_date());
                                            js2.put("end_date", mileStone.getEnd_date());
                                            js2.put("targetid", mileStone.getTargetid() + "");


                                            if (foredit == 1) {

                                                js2.put("targetid", id + "");

                                                new DatabaseHelper(getActivity()).updateData(mileStone.getId(), js2.toString(), Utils.DBtables.TABLE_MILESTONE);

                                            } else {

                                                js2.put("targetid", id + "");


                                                new DatabaseHelper(getActivity()).addData(Utils.DBtables.TABLE_MILESTONE, js2.toString());


                                            }

                                            if (mileStoneListener != null) {

                                                mileStoneListener.onMileStoneCompleted(mileStone);

                                                dismiss();
                                            }

                                        } else {

                                            txtwarning.setText("Please select Start date and end date properly");
                                            txtwarning.setVisibility(View.VISIBLE);

                                        }


                                    } else {


                                        txtwarning.setText("Please select Start date and end date properly");
                                        txtwarning.setVisibility(View.VISIBLE);
                                    }


                                }
                                else{


                                    txtwarning.setText("The start date should be greater than the end date of the previous milestone.");
                                    txtwarning.setVisibility(View.VISIBLE);

                                }



                            }catch (Exception e)
                            {
                                e.printStackTrace();
                            }




                        }
                        else{


                            Utils.showAlertWithSingle(getActivity(),"select end date",null);
                        }



                    }
                    else{


                        Utils.showAlertWithSingle(getActivity(),"select end date",null);
                    }



                }
                else{

                    Utils.showAlertWithSingle(getActivity(),"select start date",null);

                }

            }
        });


        return v;
    }


    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = (int)getActivity().getResources().getDimension(R.dimen.dimen400dp);
            dialog.getWindow().setLayout(width, height);
        }
    }


    public void setMileStoneDate(int code,int position)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);





        DatePickerDialog datePickerDialog=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    String   startdate = i2 + "-" + m + "-" + i;

                    mileStone.setStart_date(startdate);
                    txtstartdatepick.setText(startdate);



                }
                else {

                    String  endate = i2 + "-" + m + "-" + i;

//                    Date stdate=null;
//                    Date eddate=null;
//                    SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

                   mileStone.setEnd_date(endate);
                   txtenddatepick.setText(endate);


                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();

    }
}