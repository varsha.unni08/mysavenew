package com.centroid.integraaccounts.dialogs;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.InvestmentAdapter;
import com.centroid.integraaccounts.adapter.InvestmentTargetAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.InvestMentListener;
import com.centroid.integraaccounts.views.InvestmentListActivity;
import com.centroid.integraaccounts.views.NewTargetActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class InvestMentFragment extends DialogFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    InvestMentListener investMentListener;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public InvestMentFragment(InvestMentListener investMentListener) {
        // Required empty public constructor

        this.investMentListener=investMentListener;
    }

    View v;

    RecyclerView recycler;

    TextView txtNotFound;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_invest_ment, container, false);

        recycler=v.findViewById(R.id.recycler);
        txtNotFound=v.findViewById(R.id.txtNotFound);


        getInvestMent();


        return  v;
    }








    void getInvestMent()
    {

        List<CommonData> commonData=new DatabaseHelper(getActivity()).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if(commonData.size()>0)
        {
            Collections.reverse(commonData);




            recycler.setVisibility(View.VISIBLE);
            txtNotFound.setVisibility(View.GONE);

            List<CommonData>cd1=new ArrayList<>();


            for(CommonData cd:commonData) {
                try {

                    JSONObject jsonObject = new JSONObject(cd.getData());


                    String accounttype = jsonObject.getString("Accounttype");

                    if(accounttype.equalsIgnoreCase("Investment"))
                    {


                        cd1.add(cd);
                    }




                }catch (Exception e)
                {

                }
            }

            InvestmentTargetAdapter investmentAdapter=new InvestmentTargetAdapter(getActivity(),cd1,InvestMentFragment.this);
            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.setAdapter(investmentAdapter);


        }
        else {

            recycler.setVisibility(View.GONE);
            txtNotFound.setVisibility(View.VISIBLE);


        }


    }


   public void onSelectInvestment(CommonData cmd)
    {



        if(cmd!=null)
        {

            if(investMentListener!=null)
            {

                dismiss();

                investMentListener.onSelectInvestment(cmd);



            }



        }

    }
}