package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.centroid.integraaccounts.R;

public class SamplesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samples);
    }
}