package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.StateListAdapter;
import com.centroid.integraaccounts.data.domain.State;
import com.centroid.integraaccounts.data.domain.StateData;
import com.centroid.integraaccounts.data.domain.User;
import com.centroid.integraaccounts.data.domain.UserDetails;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Query;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class OTPActivity extends AppCompatActivity {



    String randomnum="";

    Button btnSendOtp;

    EditText edtOTP;

    TextView txtResend,txtOtpcode;
    HttpURLConnection urlConnection=null;
    User usr=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        getSupportActionBar().hide();
        edtOTP=findViewById(R.id.edtOTP);
        btnSendOtp=findViewById(R.id.btnSendOtp);
        txtResend=findViewById(R.id.txtResend);
        txtOtpcode=findViewById(R.id.txtOtpcode);


         usr=Utils.usr;



        txtOtpcode.setText("Enter the OTP code that sent to your mobile number "+usr.getPhone()+" and email ID "+usr.getEmail());


        Random random=new Random();
        randomnum = String.format("%04d", random.nextInt(10000));
        sendOTPtoPhone();
        sendEmail();
       // Toast.makeText(OTPActivity.this,randomnum,Toast.LENGTH_SHORT).show();


        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Random random=new Random();
                randomnum = String.format("%04d", random.nextInt(10000));
                //Toast.makeText(OTPActivity.this,randomnum,Toast.LENGTH_SHORT).show();
                sendOTPtoPhone();
                sendEmail();


            }
        });




        btnSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edtOTP.getText().toString().trim().equals(randomnum))
                {



                    registerUser();


                }
                else {

                 //   Toast.makeText(OTPActivity.this,"Enter the otp code correctly",Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(OTPActivity.this,"Enter the otp code correctly", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }




            }
        });

    }


    public void sendEmail()
    {
//
//
//        Call<JsonObject> jsonObjectCall=client.sendMail(usr.getEmail(),randomnum);
//
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//            }
//        });


    }


    public void sendOTPtoPhone()
    {

        try {

            final ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"fkjfk");


            String message = Utils.buildServerMessage(Utils.ServerMessageType.registration, randomnum, usr.getName(), "");

            String u=message.replace(" ","%20");

           // 52626HPNCW

            String urldata= Utils.smsbaseurl+Utils.WebServiceMethodes.smsMethode+"?token="+Utils.ServerMessage.apikey+
             "&sender="+Utils.ServerMessage.sender+
             "&number="+usr.getPhone()+
             "&route="+Utils.ServerMessage.route+
             "&type="+Utils.ServerMessage.type+
             "&sms="+u+"&templateid="+Utils.ServerMessage.registrationtemplateid;

           // Log.e("url",urldata);


            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {

                       //String a= "http://eapoluenterprise.in/httpapi/httpapi?token=bf25917c3254cfe9f50694f24884f23a&sender=CGSAVE&number=9747497967&route=2&type=1&sms="+u+"&templateid=1007625690429475781";

                      // String b= URLEncoder.encode(a, "UTF-8");

                        URL url = new URL(urldata);

                        Log.e("url",urldata);

                        /* for Production */
                        // URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);

                        try {
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("GET");
                           // connection.setRequestProperty("Content-Type", "application/json");
                            connection.setDoOutput(true);

//                            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
//                            requestWriter.writeBytes(post_data);
////                            requestWriter.close();

                            String responseData="";
                            InputStream is = connection.getInputStream();
                            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                            if ((responseData = responseReader.readLine()) != null) {
                                Log.e("OTP DATA RESPONSE" , responseData);


                            }








                            responseReader.close();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                            Log.e("TAG",exception.toString());
                        }
                    }catch (Exception e)
                    {
                        Log.e("TAG",e.toString());
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressFragment.dismiss();
                        }
                    });


                }
            });








        }catch (Exception e)
        {

        }





        // String message="Your one time password (OTP) from Save is "+randomnum;
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");

//
//
//        RestService.RestApiInterface client = RestService.getSmsClient();
//
//        Call<String> jsonObjectCall=client.SendSms(Utils.ServerMessage.apikey,Utils.ServerMessage.sender,usr.getPhone(),Utils.ServerMessage.route,Utils.ServerMessage.type,message,Utils.ServerMessage.registrationtemplateid);
//
//        jsonObjectCall.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//
//                progressFragment.dismiss();
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });


    }

    public void registerUser()
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");




        int spregcode=0;
        int spregid=0;

        String uuid ="";

        try{

            String uniquePseudoID = "35" +
                    Build.BOARD.length() % 10 +
                    Build.BRAND.length() % 10 +
                    Build.DEVICE.length() % 10 +
                    Build.DISPLAY.length() % 10 +
                    Build.HOST.length() % 10 +
                    Build.ID.length() % 10 +
                    Build.MANUFACTURER.length() % 10 +
                    Build.MODEL.length() % 10 +
                    Build.PRODUCT.length() % 10 +
                    Build.TAGS.length() % 10 +
                    Build.TYPE.length() % 10 +
                    Build.USER.length() % 10;
            String serial = Build.getRadioVersion();
             uuid = new UUID(uniquePseudoID.hashCode(), serial.hashCode()).toString();

            // ffffffff-8a93-ca89-ffff-ffffa56d5aee

      ;
        }catch (SecurityException e)
        {

            Log.e("Exception",e.toString());
        }



        if(usr.getSponsor()!=null)
        {
            spregcode=Integer.parseInt(usr.getSponsor().getData().getRegCode());
            spregid=Integer.parseInt(usr.getSponsor().getData().getId());
        }




//                @Field("uuid")String uuid,
//            @Field("country_id")String country_id,
//        @Field("sp_reg_code")int sp_reg_code,@Field("sp_reg_id")
//        int sp_reg_id,@Field("stateid")String stateid,
//        @Field("language")String language,@Field("name")String name
//        ,@Field("mobile") String mobile,@Field("password")String password
//        ,@Field("email")String email);

        //UserAuthenticate(uuid,usr.getCountryid(),spregcode,spregid,usr.getStateid(),usr.getLanguage(),
//                usr.getName(),usr.getPhone(),usr.getPassword(),usr.getEmail()
//        );


        Map<String,String> params=new HashMap<>();
        params.put("uuid",uuid);
        params.put("country_id",usr.getCountryid()+"");
        params.put("sp_reg_code",spregcode+"");
        params.put("sp_reg_id",spregid+"");
        params.put("stateid",usr.getStateid());
        params.put("language",usr.getLanguage());
        params.put("name",usr.getName());
        params.put("mobile",usr.getPhone());
        params.put("password",usr.getPassword());
        params.put("email",usr.getEmail());
        params.put("timestamp",Utils.getTimestamp());




        new RequestHandler(OTPActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
               // progressFragment.dismiss();
                if(data!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {



                           String message=Utils.buildServerMessage(Utils.ServerMessageType.registration_Confirm_password,randomnum,usr.getPhone(),usr.getPassword());



                            final ProgressFragment p1=new ProgressFragment();
                            p1.show(getSupportFragmentManager(),"fkjfk");


                          //  String message = Utils.buildServerMessage(Utils.ServerMessageType.registration, randomnum, usr.getName(), "");

                            String u=message.replace(" ","%20");



                            String urldata= Utils.smsbaseurl+Utils.WebServiceMethodes.smsMethode+"?token="+Utils.ServerMessage.apikey+
                                    "&sender="+Utils.ServerMessage.sender+
                                    "&number="+usr.getPhone()+
                                    "&route="+Utils.ServerMessage.route+
                                    "&type="+Utils.ServerMessage.type+
                                    "&sms="+u+"&templateid="+Utils.ServerMessage.registration_Confirm_templateid;

                            // Log.e("url",urldata);


                            ExecutorService executor = Executors.newSingleThreadExecutor();
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {


                                        URL url = new URL(urldata);

                                        Log.e("url",urldata);

                                        /* for Production */
                                        // URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);

                                        try {
                                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                            connection.setRequestMethod("GET");
                                            // connection.setRequestProperty("Content-Type", "application/json");
                                            connection.setDoOutput(true);



                                            String responseData="";
                                            InputStream is = connection.getInputStream();
                                            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                                            if ((responseData = responseReader.readLine()) != null) {
                                                Log.e("OTP DATA RESPONSE" , responseData);


                                            }








                                            responseReader.close();
                                        } catch (Exception exception) {
                                            exception.printStackTrace();
                                            Log.e("TAG",exception.toString());
                                        }
                                    }catch (Exception e)
                                    {
                                        Log.e("TAG",e.toString());
                                    }

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            p1.dismiss();
                                        }
                                    });


                                }
                            });











                            // String message="Your one time password (OTP) from Save is "+randomnum;
//                            final ProgressFragment progressFragment=new ProgressFragment();
//                            progressFragment.show(getSupportFragmentManager(),"fkjfk");
//                            RestService.RestApiInterface client = RestService.getSmsClient();
//
//                            Call<String> jsonObjectCall=client.SendSms(Utils.ServerMessage.apikey,Utils.ServerMessage.sender,usr.getPhone(),Utils.ServerMessage.route,Utils.ServerMessage.type,message,Utils.ServerMessage.registration_Confirm_templateid);
//
//                            jsonObjectCall.enqueue(new Callback<String>() {
//                                @Override
//                                public void onResponse(Call<String> call, Response<String> response) {
//
//                                    progressFragment.dismiss();
//                                }
//
//                                @Override
//                                public void onFailure(Call<String> call, Throwable t) {
//
//                                    progressFragment.dismiss();
//                                }
//                            });




                            String token=jsonObject.getString("token");

                            new PreferenceHelper(OTPActivity.this).putData(Utils.userkey,token);



                            Intent newIntent = new Intent(OTPActivity.this,LoginActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            finishAffinity();

                            startActivity(newIntent);






                        }
                        else {

                            Utils.showAlertWithSingle(OTPActivity.this,"Login failed", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });

                         //   Toast.makeText(OTPActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

             //   Toast.makeText(OTPActivity.this,err,Toast.LENGTH_SHORT).show();
                Utils.showAlertWithSingle(OTPActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.userAuthenticate, Request.Method.POST).submitRequest();





//        Call<JsonObject> jsonObjectCall=client.UserAuthenticate(uuid,usr.getCountryid(),spregcode,spregid,usr.getStateid(),usr.getLanguage(),
//                usr.getName(),usr.getPhone(),usr.getPassword(),usr.getEmail()
//        );
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        if(jsonObject.getInt("status")==1)
//                        {
//
//
//
//                            String message=Utils.buildServerMessage(Utils.ServerMessageType.registration_Confirm_password,randomnum,usr.getPhone(),usr.getPassword());
//
//
//                            // String message="Your one time password (OTP) from Save is "+randomnum;
//                            final ProgressFragment progressFragment=new ProgressFragment();
//                            progressFragment.show(getSupportFragmentManager(),"fkjfk");
//                            RestService.RestApiInterface client = RestService.getSmsClient();
//
//                            Call<String> jsonObjectCall=client.SendSms(Utils.ServerMessage.apikey,Utils.ServerMessage.sender,usr.getPhone(),Utils.ServerMessage.route,Utils.ServerMessage.type,message,Utils.ServerMessage.registration_Confirm_templateid);
//
//                            jsonObjectCall.enqueue(new Callback<String>() {
//                                @Override
//                                public void onResponse(Call<String> call, Response<String> response) {
//
//                                    progressFragment.dismiss();
//                                }
//
//                                @Override
//                                public void onFailure(Call<String> call, Throwable t) {
//
//                                    progressFragment.dismiss();
//                                }
//                            });
//
//
//
//
//                            String token=jsonObject.getString("token");
//
//                            new PreferenceHelper(OTPActivity.this).putData(Utils.userkey,token);
//
//
////                            Intent i=  new Intent(OTPActivity.this,MainActivity.class);
////
////                            startActivity(i);
//
//                            Intent newIntent = new Intent(OTPActivity.this,MainActivity.class);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(newIntent);
//
//
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(OTPActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });




    }
}
