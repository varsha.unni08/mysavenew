package com.centroid.integraaccounts.views;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.PaymentTypeAdapter;
import com.centroid.integraaccounts.adapter.RechargeRecordAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.rechargedomain.RPlans;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.services.OperatorSelectionListener;
import com.centroid.integraaccounts.views.rechargeViews.CommonRechargeViewActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DTHRechargeActivity extends AppCompatActivity {

    Spinner spinner_dth;
    String selected_operator="",id_transaction="";
    EditText edtAccountno,edtMobno,edtAmount,edtCardNumber;
    TextView txtDetails;
    Button btnsubmit;
    String fullString = "",spkey="";
    String apireqid="";
    String arrmonth[] = {"Sun Direct", "Dish TV", "Videocon D2H", "TATA Sky", "Airtel Digital TV"};

    String recharge_url="https://mysaveapp.com/rechargeAPI/RechargePhone.php?";

    String transactionid="",userid="",amount="",cardnumber="",paymentmode="upi";
    double totalamountToSubmit=0;
    ImageView imgback;
    String rechargeamount="0";
    String stateid = "";
    String countryid = "";
    String mobile = "";
    String email = "";
    String name = "";
    int code=0,code1=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dthrecharge);
        getSupportActionBar().hide();
        txtDetails=findViewById(R.id.txtDetails);
        spinner_dth=findViewById(R.id.spinner_dth);
        edtAccountno=findViewById(R.id.edtAccountno);
        edtCardNumber=findViewById(R.id.edtCardNumber);
        edtMobno=findViewById(R.id.edtMobno);
        edtAmount=findViewById(R.id.edtAmount);
        btnsubmit=findViewById(R.id.btnsubmit);
        imgback=findViewById(R.id.imgback);

        Intent intent=getIntent();
        if(intent!=null) {

         amount=   intent.getStringExtra("amount");
            spkey=  intent.getStringExtra("spkey");
          selected_operator=  intent.getStringExtra("operator");

            rechargeamount=amount;
            txtDetails.setText("Operator : "+selected_operator+"\n\n"+"Amount : "+amount);

            cardnumber=intent.getStringExtra("cardnumber");

            mobile=intent.getStringExtra("mobile");
            if(cardnumber!=null) {

                if (!cardnumber.equalsIgnoreCase("")) {
                    edtCardNumber.setText(cardnumber);
                    edtMobno.setText(mobile);
                    getProfile(amount);
                }
            }

            id_transaction=intent.getStringExtra("transaction_id");
            if(id_transaction!=null) {

                if (id_transaction.equalsIgnoreCase("")) {
                    id_transaction="";
                }
            }
            else{

                id_transaction="";
            }



        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(DTHRechargeActivity.this,   android.R.layout.simple_spinner_item, RechargePlans.DTHPlans.arr_plans);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_dth.setAdapter(spinnerArrayAdapter);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!selected_operator.isEmpty()) {

                    if(!edtCardNumber.getText().toString().isEmpty()) {

                        if (!edtMobno.getText().toString().isEmpty()) {

                            cardnumber=edtCardNumber.getText().toString();

                            getProfile(amount);


                        } else {

                            Utils.showAlertWithSingle(DTHRechargeActivity.this, "Enter mobile number", null);
                        }
                    }
                    else{

                        Utils.showAlertWithSingle(DTHRechargeActivity.this, "Enter Card number", null);
                    }

                }
                else{

                    Utils.showAlertWithSingle(DTHRechargeActivity.this,"Select Operator",null);
                }
            }
        });



    }





    public void rechargePhone(String amountdata)
    {

        int min = 1;
        int max = 10000;
        int random = new Random().nextInt((max - min) + 1) + min;


        String phone=cardnumber;
//       String amount=Amount;
        double amount1=Double.parseDouble(amount);
         apireqid=random+"";
        String customerno=edtMobno.getText().toString();

        String urldata=recharge_url+"phone="+phone+"&amount="+amount1+"&spkey="+spkey+"&apireqid="+apireqid+"&customerno="+customerno;

        Intent intent=new Intent(DTHRechargeActivity.this, CommonRechargeViewActivity.class);
        intent.putExtra("url",urldata);
        intent.putExtra("phone",phone);
        intent.putExtra("operator",selected_operator);
        intent.putExtra("transaction_id",id_transaction);
        startActivity(intent);
        finish();





    }

    public void getProfile(String amountdata)
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(DTHRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {



                                String arr[]={"UPI (no convenience charges)",
                                        "NET Banking (payment gateway charge @1.5% application)",
                                        "Debit Card (payment gateway charge @o.4% application)",
                                        "Credit card (payment gateway charge @2.1% application)"};

                                int arricons[]={R.drawable.upi,R.drawable.netbanking,
                                        R.drawable.debitcard,
                                        R.drawable.creditcard};

                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int height = displayMetrics.heightPixels ;


                                int width = displayMetrics.widthPixels;

                                final Dialog dialog = new Dialog(DTHRechargeActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.layout_servicechargecalculation);

                                Spinner spinner_dth=dialog.findViewById(R.id.spinner_dth);
                                TextView txtAmount=dialog.findViewById(R.id.txtAmount);
                                TextView txtTotalAmount=dialog.findViewById(R.id.txtTotalAmount);
                                ImageView imgicon=dialog.findViewById(R.id.imgicon);
                                TextView txtPaymentSpinner=dialog.findViewById(R.id.txtPaymentSpinner);
                                Button save=dialog.findViewById(R.id.save);

                                txtAmount.setText(amountdata);
                                txtTotalAmount.setText(amountdata);
                                double t=Double.parseDouble(amountdata);
                                totalamountToSubmit=t;
                                amount=totalamountToSubmit+"";

                                txtPaymentSpinner.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        int a=(int)height/2;

                                        final Dialog dialog1 = new Dialog(DTHRechargeActivity.this);
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.layout_paymenttype);
                                        RecyclerView recycler=dialog1.findViewById(R.id.recycler);
                                        PaymentTypeAdapter paymentTypeAdapter=new PaymentTypeAdapter(DTHRechargeActivity.this, new OperatorSelectionListener() {
                                            @Override
                                            public void onOperatorSelected(int i, String selectedoperator) {
                                                imgicon.setImageResource(arricons[i]);
                                                txtPaymentSpinner.setText(arr[i]);
                                                if(i==0)
                                                {
                                                    paymentmode="upi";
                                                    txtTotalAmount.setText(amountdata);
                                                    double t=Double.parseDouble(amountdata);
                                                    totalamountToSubmit=t;
                                                    amount=totalamountToSubmit+"";
                                                }
                                                else if(i==1)
                                                {
                                                    paymentmode="Net Banking";
                                                    DecimalFormat df = new DecimalFormat("0.00");


                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(1.5/100);
                                                    totalamountToSubmit= am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    txtTotalAmount.setText(a+"");
                                                    amount=a;

                                                }
                                                else if(i==2)
                                                {
                                                    paymentmode="Debit card";
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(0.4/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                else if(i==3)
                                                {

                                                    paymentmode="Credit card";
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(2.1/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                dialog1.dismiss();
                                            }
                                        });

                                        recycler.setLayoutManager(new GridLayoutManager(DTHRechargeActivity.this,2));
                                        recycler.setAdapter(paymentTypeAdapter);

                                        dialog1.getWindow().setLayout(width,a);
                                        dialog1.show();

                                    }
                                });

                                imgicon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        int a=(int)height/2;
                                        final Dialog dialog1 = new Dialog(DTHRechargeActivity.this);
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.layout_paymenttype);
                                        RecyclerView recycler=dialog1.findViewById(R.id.recycler);
                                        PaymentTypeAdapter paymentTypeAdapter=new PaymentTypeAdapter(DTHRechargeActivity.this, new OperatorSelectionListener() {
                                            @Override
                                            public void onOperatorSelected(int i, String selectedoperator) {

                                                imgicon.setImageResource(arricons[i]);
                                                txtPaymentSpinner.setText(arr[i]);

                                                if(i==0)
                                                {
                                                    txtTotalAmount.setText(amountdata);
                                                    double t=Double.parseDouble(amountdata);
                                                    totalamountToSubmit=t;
                                                    amount=totalamountToSubmit+"";
                                                }
                                                else if(i==1)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");


                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(1.5/100);
                                                    totalamountToSubmit= am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    txtTotalAmount.setText(a+"");
                                                    amount=a;

                                                }
                                                else if(i==2)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(0.4/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                else if(i==3)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(2.1/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                dialog1.dismiss();
                                            }
                                        });

                                        recycler.setLayoutManager(new GridLayoutManager(DTHRechargeActivity.this,2));
                                        recycler.setAdapter(paymentTypeAdapter);
                                        dialog1.getWindow().setLayout(width,a);
                                        dialog1.show();
                                    }
                                });





                                save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                         stateid = profiledata.getData().getStateId();
                                         countryid = profiledata.getData().getCountryId();
                                         mobile = edtMobno.getText().toString();
                                         email = profiledata.getData().getEmailId();
                                         name = profiledata.getData().getFullName();
                                        userid = profiledata.getData().getId();
                                        postPGData(amount,0);
                                        code=0;



                                    }
                                });

                                dialog.getWindow().setLayout(width,height);

                                dialog.show();


                            }





                        }
                        else {

                            Toast.makeText(DTHRechargeActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }


    private void postTransactionData(MobileRechargeResponse mobileRechargeResponse,int status)
    {
        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dajbb");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        params.put("user_id",userid);
        params.put("mobile_number",mobile);
        params.put("account_number",cardnumber);
        params.put("transaction_id",transactionid);
        params.put("amount",rechargeamount);
        params.put("operator",selected_operator);
        params.put("rp_id",mobileRechargeResponse.getRpid());
        params.put("agent_id",mobileRechargeResponse.getAgentid());
        params.put("status",status+"");
        params.put("recharge_type","2");
        params.put("Payment_Mode",paymentmode);


        new RequestHandler(DTHRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();

                try{

                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.getInt("status")==1)
                    {
                        String msg=jsonObject.getString("message");
                        String id=jsonObject.getString("id");
                        updateTransactionStatus(id);


                    }
                    else{


                        String msg=jsonObject.getString("message");

                        Utils.showAlertWithSingle(DTHRechargeActivity.this, msg, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }




                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.PostTransactionata+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();


    }

    private void updateTransactionStatus(String id)
    {

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dajbb");
        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        params.put("id",id);


        new RequestHandler(DTHRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();

                try{

                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.getInt("status")==1)
                    {
                        String msg=jsonObject.getString("message");
                        String id=jsonObject.getString("id");

                        Utils.showAlertWithSingle(DTHRechargeActivity.this, msg, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }
                    else{


                        String msg=jsonObject.getString("message");

                        Utils.showAlertWithSingle(DTHRechargeActivity.this, msg, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }




                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateTransactionStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }





    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        if (data != null) {

                            if (data.hasExtra("result")) {

                                transactionid = data.getStringExtra("result");

                                updatePaymentStatus(id_transaction,5,code1,"2");

                            }
                        }

                    }

                    else {
                        transactionid="";
                        updatePaymentStatus(id_transaction,6,code1,"0");

                        Utils.showAlertWithSingle(DTHRechargeActivity.this, "Transaction failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }
                }
            });



    public void updatePaymentStatus(String id,int status,int code,String rechargestatus)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
        params.put("transaction_id",transactionid);
        params.put("rechargestatus",rechargestatus);

        new RequestHandler(DTHRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                try{
                    JSONObject jsonObject=new JSONObject(data);

                    if(status==5)
                    {
                        rechargePhone(rechargeamount);
                    }



                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updatePaymentStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }


    public void postPGData(String amountdata,int status)
    {

        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dajbb");

        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("user_id", userid);
        params.put("mobile_number", edtMobno.getText().toString());

        params.put("account_number", edtCardNumber.getText().toString());
        params.put("transaction_id", transactionid);
        params.put("operatorcircle", "");
        params.put("amount", amountdata);
        params.put("rechargeamount", rechargeamount);

//        if (selected_operator.compareTo("Vodafone") == 0) {
//            params.put("operator", "Vi");
//        } else {

            params.put("operator", selected_operator);
       // }
        params.put("rp_id", "");
        params.put("agent_id", "");
        params.put("spkey",spkey);
        params.put("status",  "2");
        params.put("recharge_type", "2");
        params.put("payment_status", "4");
        params.put("Payment_Mode",paymentmode);

        new RequestHandler(DTHRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        String msg = jsonObject.getString("message");
                        id_transaction=jsonObject.getString("id");

                        Intent i = new Intent(DTHRechargeActivity.this, PaymentWebViewActivity.class);
                        i.putExtra("name", name);
                        i.putExtra("email", email);
                        i.putExtra("phone", mobile);
                        i.putExtra("amount", amount+"");
                        i.putExtra("requestid", apireqid);
                        i.putExtra("spkey", spkey);
                        i.putExtra("operator", selected_operator);
                        i.putExtra("rechargeamount", rechargeamount);
                        i.putExtra("id_transaction", id_transaction);
                        someActivityResultLauncher.launch(i);

                    } else {



                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.PostTransactionata + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();







    }

}