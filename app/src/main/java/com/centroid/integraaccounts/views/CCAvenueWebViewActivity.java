package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;

public class CCAvenueWebViewActivity extends AppCompatActivity {

    String responseurl="https://mysaveapp.com";
    String url="https://mysaveapp.com/purchase";
    WebView wv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ccavenue_web_view);
        getSupportActionBar().hide();

        wv1=findViewById(R.id.webview);

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setSupportMultipleWindows(true);
        wv1.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.setWebViewClient(new MyBrowser());
        // wv1.setWebChromeClient(new MyBrowserChromeClient());
        wv1.loadUrl(url);
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            Log.e("TAAGv",url);

            view.loadUrl(url);


            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.e("TAAG",url);





            if(url.equalsIgnoreCase(responseurl))
            {
                onBackPressed();

                //getResponseFromPayment(url);

            }


        }
    }
}