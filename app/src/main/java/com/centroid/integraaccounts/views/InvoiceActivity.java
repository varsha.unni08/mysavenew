package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.EnglishNumberToWords;
import com.centroid.integraaccounts.data.domain.BillAmount;
import com.centroid.integraaccounts.data.domain.Currency;
import com.centroid.integraaccounts.data.domain.SettingsData;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class InvoiceActivity extends AppCompatActivity {

    SettingsData settingsData1;
    String bill="",rs="",countryid="",stateid="";
    //Currency currency=null;

    double total_amount=0;

    String name="",email="",trid="";
    LinearLayout layout_invoice;

    BillAmount billamount=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_invoiceformat);
        getSupportActionBar().hide();
        settingsData1=(SettingsData)getIntent().getSerializableExtra("settingsdata");
        bill=getIntent().getStringExtra("bill");
      //  currency=(Currency)getIntent().getSerializableExtra("currency");
        rs=getIntent().getStringExtra("actualamount");
        countryid=getIntent().getStringExtra("countryid");
        stateid=getIntent().getStringExtra("stateid");
        name=getIntent().getStringExtra("name");
        email=getIntent().getStringExtra("email");
        trid=getIntent().getStringExtra("tid");

        billamount=(BillAmount)getIntent().getSerializableExtra("amount");

        layout_invoice=findViewById(R.id.layout_invoice);
        TextView txtbillno = findViewById(R.id.txtbillno);
        TextView txtDate = findViewById(R.id.txtDate);
        TextView txtBuyer = findViewById(R.id.txtBuyer);
        TextView txtActualRate = findViewById(R.id.txtActualRate);
        TextView txtFullActualRate = findViewById(R.id.txtFullActualRate);
        TextView txtAmountinWords = findViewById(R.id.txtAmountinWords);
        TextView txtGSTHead = findViewById(R.id.txtGSTHead);
        LinearLayout layout_gst = findViewById(R.id.layout_gst);
        TextView txtsgstPrice = findViewById(R.id.txtsgstPrice);
        TextView txtcgstPrice = findViewById(R.id.txtcgstPrice);
        TextView txtIGST = findViewById(R.id.txtIGST);
        TextView txtNetTotalAmount = findViewById(R.id.txtNetTotalAmount);
        Button btnDownload = findViewById(R.id.btnDownload);
        TextView txtcgsthead=findViewById(R.id.txtcgsthead);
        TextView txtSgsthead=findViewById(R.id.txtSgsthead);
        TextView txtTransaction=findViewById(R.id.txtTransaction);

        txtBuyer.setText("Buyer : "+name+"\n"+email);
        txtTransaction.setText("Transaction Id : "+trid);



        txtbillno.setText("Bill no. : " + bill);

        if (billamount.getData().getSalesDate() != null) {


            if (!billamount.getData().getSalesDate().equalsIgnoreCase("")) {

                //   2021-04-30 02:04:14

                try {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                    Date dt = sdf.parse(billamount.getData().getSalesDate());

                           Calendar calendar = Calendar.getInstance();
                           calendar.setTime(dt);
        int d = calendar.get(Calendar.DAY_OF_MONTH);
        int m = calendar.get(Calendar.MONTH) + 1;
        int y = calendar.get(Calendar.YEAR);

                    txtDate.setText("Date : " + d + "-" + m + "-" + y);

                } catch (Exception e) {


                }



            }
        }


//

        if (countryid.equalsIgnoreCase("1")) {
            if (stateid.equalsIgnoreCase("12")) {
                double actualvalue = Double.parseDouble(rs);
                double sgstvalue =  Double.parseDouble(billamount.getData().getSgst()) ;
                double cgstvalue =  Double.parseDouble(billamount.getData().getCgst()) ;
                txtActualRate.setText(rs + " ");
                txtFullActualRate.setText(rs + " ");
                txtsgstPrice.setText(sgstvalue + " ");
                txtcgstPrice.setText(cgstvalue + " ");
                txtIGST.setVisibility(View.INVISIBLE);
                double t = actualvalue + sgstvalue + cgstvalue;
                txtNetTotalAmount.setText(Math.round(t) + " ");
                total_amount = Math.round(t);
            } else {

                txtcgsthead.setVisibility(View.GONE);
                txtSgsthead.setVisibility(View.GONE);
                txtsgstPrice.setVisibility(View.GONE);
                txtcgstPrice.setVisibility(View.GONE);
                double actualvalue = Double.parseDouble(rs);
                txtActualRate.setText(rs + " ");
               // layout_gst.setVisibility(View.INVISIBLE);
                double Igstvalue = Double.parseDouble(billamount.getData().getIgst()) ;
                txtIGST.setText(Igstvalue + " ");
                double t = actualvalue + Igstvalue;
                txtNetTotalAmount.setText(Math.round(t) + " ");
                total_amount = Math.round(t);
            }

        } else {

            txtGSTHead.setText("Other Tax");
            txtsgstPrice.setVisibility(View.GONE);
            txtcgstPrice.setVisibility(View.GONE);
           // layout_gst.setVisibility(View.INVISIBLE);
            txtsgstPrice.setVisibility(View.GONE);
            txtcgstPrice.setVisibility(View.GONE);
            txtIGST.setVisibility(View.VISIBLE);

            double conversionval = Double.parseDouble("1.00");
            double actualvalue = Double.parseDouble(rs);
            double othertax = Double.parseDouble(settingsData1.getOtherTax());
            double otherprice = actualvalue * othertax / 100;
            double otprice=otherprice*conversionval;


            txtIGST.setText(otprice + "");
            double tot = actualvalue + otprice;

            double t = tot * conversionval;
            total_amount = Math.round(t);
            txtNetTotalAmount.setText(total_amount + " ");

        }


        int a=(int)total_amount;
        String d1 = String.valueOf(a);

        long n = Long.parseLong(d1);
        txtAmountinWords.setText("Amount in words : " + EnglishNumberToWords.convert(n) + " INR " );
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    String permission="";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                        permission=Manifest.permission.READ_MEDIA_IMAGES;

                    }
                    else{

                        permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    }




                    if (ContextCompat.checkSelfPermission(InvoiceActivity.this,permission) == PackageManager.PERMISSION_GRANTED) {


                        Bitmap bitmap = Bitmap.createBitmap(layout_invoice.getWidth(), layout_invoice.getHeight(), Bitmap.Config.ARGB_8888);
                        Canvas c = new Canvas(bitmap);
                        layout_invoice.draw(c);
                       // imageView.setImageBitmap(b);

                        Long tsLong = System.currentTimeMillis() / 1000;
                        String ts = tsLong.toString();

                        File fp = new File(getExternalCacheDir() + "/Save/Invoice");


                        if (!fp.exists()) {
                            fp.mkdirs();
                        }

                        File f = new File(fp.getAbsolutePath(), "invoice" + ts + ".png");
                        if (!f.exists()) {
                            f.createNewFile();

                        } else {

                            f.delete();
                            f.createNewFile();
                        }


                        FileOutputStream output = new FileOutputStream(f);


                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                        output.close();


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                            Uri photoURI = FileProvider.getUriForFile(InvoiceActivity.this, getApplicationContext().getPackageName() + ".provider", f);

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(photoURI, "image/*");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);

                        } else {

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(f), "image/*");
                            startActivity(intent);
                        }



                    } else {

                        ActivityCompat.requestPermissions(InvoiceActivity.this,new String[]{permission}, 11);
                    }

                }catch (Exception e)
                {
                    Log.e("TAAG",e.toString());
                }
            }
        });
    }
}
