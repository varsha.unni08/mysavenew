package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BillregisterAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class BillRegisterActivity extends AppCompatActivity {

    RecyclerView recycler;
    ImageView imgback;
    LinearLayout layout_head;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_register);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recycler=findViewById(R.id.recycler);
        layout_head=findViewById(R.id.layout_head);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getBillVoucherData();

    }





    public void getBillVoucherData()
    {
        List<Accounts>accounts=new DatabaseHelper(BillRegisterActivity.this).getAccountsDataByEntryId("0");




        List<Accounts>accounts1=new ArrayList<>();
        if(accounts.size()>0)
        {

            for (Accounts a:accounts
                 ) {
                if(a.getACCOUNTS_vouchertype()== Utils.VoucherType.billvoucher)
                {

                    accounts1.add(a);

                }

            }


            Collections.sort(accounts1, new Comparator<Accounts>() {
                @Override
                public int compare(Accounts accounts, Accounts t1) {

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate=null;

                    Date endDate=null;
                    try {
                        strDate = sdf.parse(accounts.getACCOUNTS_date());
                        endDate = sdf.parse(t1.getACCOUNTS_date());




                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return endDate.compareTo(strDate);
                }
            });


            recycler.setLayoutManager(new LinearLayoutManager(BillRegisterActivity.this));
            recycler.setAdapter(new BillregisterAdapter(BillRegisterActivity.this,accounts1));


        }
        else
        {

            layout_head.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);
        }





    }

}
