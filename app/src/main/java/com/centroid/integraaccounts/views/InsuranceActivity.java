package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.InsuranceAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.customviews.DatePickerDialogNoYear;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.List;

public class InsuranceActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    ImageView imgback;

    TextView txtTotal,txtprofile;

    Resources resources;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);
        getSupportActionBar().hide();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        txtTotal=findViewById(R.id.txtTotal);
        txtprofile=findViewById(R.id.txtprofile);



        String languagedata = LocaleHelper.getPersistedData(InsuranceActivity.this, "en");
        Context context= LocaleHelper.setLocale(InsuranceActivity.this, languagedata);

        resources=context.getResources();

        txtprofile.setText(resources.getString(R.string.insurance));


        Utils.showTutorial(resources.getString(R.string.insurancetutorial),InsuranceActivity.this,Utils.Tutorials.insurancetutorial);




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(InsuranceActivity.this, InsuranceEntryActivity.class));
            }
        });

        getInsuranceData();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getInsuranceData();
    }

    public void getInsuranceData()
    {

        double total=0;
        List<CommonData>commonData=new DatabaseHelper(InsuranceActivity.this).getData(Utils.DBtables.TABLE_INSURANCE);

        if(commonData.size()>0)
        {


            try{





                for (CommonData data:commonData)
                {

                    JSONObject jsonObject=new JSONObject(data.getData());

                    total=total+Double.parseDouble(jsonObject.getString("amount"));


                }


                InsuranceAdapter insuranceAdapter=new InsuranceAdapter(InsuranceActivity.this,commonData);
                recycler.setLayoutManager(new LinearLayoutManager(InsuranceActivity.this));
                recycler.setAdapter(insuranceAdapter);





                txtTotal.setText(resources.getString(R.string.total)+" : "+total+" "+this.getString(R.string.rs));




            }catch (Exception e)
            {

            }



        }
        else {

            txtTotal.setText(resources.getString(R.string.total)+" : "+total+" "+this.getString(R.string.rs));
        }






    }


}
