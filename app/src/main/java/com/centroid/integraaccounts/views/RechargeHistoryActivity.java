package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.PaymentTypeAdapter;
import com.centroid.integraaccounts.adapter.RechargeHistoryDataAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.RechargeHistoryData;
import com.centroid.integraaccounts.data.domain.RechargeHistoryStatus;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.services.OperatorSelectionListener;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RechargeHistoryActivity extends AppCompatActivity {
    
    ImageView imgback;
    RecyclerView recycler;
    String fullString="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_history);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recycler=findViewById(R.id.recycler);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                onBackPressed();
            }
        });
        
        getRechargeHistory();
    }



    public void retryRecharge(RechargeHistoryData rechargeHistoryData)
    {

        String recharge_retry_url="https://mysaveapp.com/rechargeAPI/retryRecharge.php?timestamp="+Utils.getTimestamp()+"&rpid="+rechargeHistoryData.getRpId()+"&agentid="+rechargeHistoryData.getAgentId();
  ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final ProgressFragment progressFragment = new ProgressFragment();
                    progressFragment.show(getSupportFragmentManager(), "fkjfk");

                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
                    String fullurl = recharge_retry_url;

                    fullString = "";
                    URL url = new URL(fullurl);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            progressFragment.dismiss();

                            try {

                                if (!fullString.isEmpty()) {

                                    JSONObject jsonObject = new JSONObject(fullString);

                                    if (jsonObject.getInt("status") == 2) {
                                        updateStatus(rechargeHistoryData.getId(),1);

                                        Utils.showAlertWithSingle(RechargeHistoryActivity.this, "Recharge Successful", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });


                                    } else if (jsonObject.getInt("status") == 1) {

                                        updateStatus(rechargeHistoryData.getId(),2);

                                        Utils.showAlertWithSingle(RechargeHistoryActivity.this, "Recharge is still pending....", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });


                                    } else {

                                        updateStatus(rechargeHistoryData.getId(),0);
                                    }


                                }

                            }catch (Exception e)
                            {

                            }


                        }
                    });


                } catch (Exception e) {


                }
            }
        });


    }

    public void updateStatus(String id,int status)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
        // params.put("device_id",token);

        new RequestHandler(RechargeHistoryActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(RechargeHistoryActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateRechargeTransactionStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }


    
    public void getRechargeHistory()
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(RechargeHistoryActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();


                    try{

                        RechargeHistoryStatus rechargeHistoryStatus=new GsonBuilder().create().fromJson(data,RechargeHistoryStatus.class);
                        if(rechargeHistoryStatus.getStatus().equals(1))
                        {

                            List<RechargeHistoryData>rechargeHistoryData=rechargeHistoryStatus.getData();

                            if(rechargeHistoryData.size()>0)
                            {

                                RechargeHistoryDataAdapter rechargeHistoryDataAdapter=new RechargeHistoryDataAdapter(RechargeHistoryActivity.this,rechargeHistoryData);
                                recycler.setLayoutManager(new LinearLayoutManager(RechargeHistoryActivity.this));
                                recycler.setAdapter(rechargeHistoryDataAdapter);


                            }
                            else{

                                Utils.showAlertWithSingle(RechargeHistoryActivity.this, "No data found", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

                            }



                        }
                        else{


                            Utils.showAlertWithSingle(RechargeHistoryActivity.this, "No data found", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });
                        }







                    }

                    catch (Exception e)
                    {

                    }






            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(RechargeHistoryActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getRechargeHistory+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }
}