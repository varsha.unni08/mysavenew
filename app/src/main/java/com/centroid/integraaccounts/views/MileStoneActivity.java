package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MileStoneDynamicAdapter;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.data.domain.MileStoneTarget;
import com.centroid.integraaccounts.interfaces.DialogEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MileStoneActivity extends AppCompatActivity {


    EditText edtNumber;
    RecyclerView recycler;

    ImageView imgcancel,imgtick;

    TextView txttarget,txtTotalAmount,txtMilestone;

    Button btnsubmit;

    double targetamount=0;

    List<MileStone>mileStones;

    MileStoneDynamicAdapter mileStoneDynamicAdapter;

    MileStoneTarget mileStone;

    Button btn_submit;

    int forAdd=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mile_stone);
        getSupportActionBar().hide();

        imgtick=findViewById(R.id.imgtick);

        imgcancel=findViewById(R.id.imgcancel);

        recycler=findViewById(R.id.recycler);

        edtNumber=findViewById(R.id.edtNumber);

        txttarget=findViewById(R.id.txttarget);
        txtTotalAmount=findViewById(R.id.txtTotalAmount);
        txtMilestone=findViewById(R.id.txtMilestone);

        btnsubmit=findViewById(R.id.btnsubmit);
        btn_submit=findViewById(R.id.btn_submit);

        Intent i=getIntent();

        if(i!=null) {

            String t = i.getStringExtra("target");

            mileStone=(MileStoneTarget)i.getSerializableExtra("mileStone");

            forAdd=i.getIntExtra("forAdd",0);


            if (t != null) {

                if (!t.equalsIgnoreCase("")) {
                    targetamount = Double.parseDouble(t);
                } else {

                    targetamount = 0;
                }


            } else {

                targetamount = 0;
            }

            if(mileStone!=null) {
                if (mileStone.getMls().size() > 0) {

                    mileStones=mileStone.getMls();

                    mileStoneDynamicAdapter=new MileStoneDynamicAdapter(MileStoneActivity.this,mileStones,1);
                    recycler.setLayoutManager(new LinearLayoutManager(MileStoneActivity.this));
                    recycler.setAdapter(mileStoneDynamicAdapter);


                }
            }
            else{

                mileStones=new ArrayList<>();
                MileStone mileStone=new MileStone();
                mileStone.setAmount("0");
                mileStones.add(mileStone);

                mileStoneDynamicAdapter=new MileStoneDynamicAdapter(MileStoneActivity.this,mileStones,1);
                recycler.setLayoutManager(new LinearLayoutManager(MileStoneActivity.this));
                recycler.setAdapter(mileStoneDynamicAdapter);
            }





        }
        else{

            targetamount=0;
        }






        txttarget.setText("Target amount : "+targetamount);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MileStone mileStone=new MileStone();
                mileStone.setAmount("0");
                mileStones.add(mileStone);

                if(mileStoneDynamicAdapter!=null)
                {

                    mileStoneDynamicAdapter.notifyDataSetChanged();
                }

            }
        });

        imgtick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MileStoneTarget mileStoneTarget=new MileStoneTarget();
                mileStoneTarget.setTargetamount(targetamount);

                if(mileStoneDynamicAdapter!=null) {


                 List<MileStone>ml=   mileStoneDynamicAdapter.getMileStoneList();

                 if(ml.size()>0)
                 {

                     boolean isvalid=true;
                     Date stdate=null;
                     Date eddate=null;
                     SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");


                     for(int i=0;i<ml.size();i++)
                     {

                         MileStone m=ml.get(i);

                         if(!m.getStart_date().equalsIgnoreCase("")&&!m.getEnd_date().equalsIgnoreCase(""))
                         {

                             if(i>0)
                             {
                                 try {
                                     Date stdate_compare = myFormat.parse(m.getStart_date());

                                     if(stdate_compare.after(eddate))
                                     {


                                     }
                                     else{


                                         isvalid=false;

                                         break;
                                     }

                                 }catch (Exception e)
                                 {
                                     isvalid=false;

                                     break;
                                 }

                             }


                             if(!m.getAmount().equalsIgnoreCase(""))
                             {
                                 double b=Double.parseDouble(m.getAmount());

                                 if(b!=0)
                                 {


                                     try{



                                         stdate=myFormat.parse(m.getStart_date());
                                         eddate=myFormat.parse(m.getEnd_date());

                                         if(stdate.before(eddate))
                                         {

                                             if(i>0)
                                             {


                                             }







                                         }
                                         else{

                                             isvalid=false;

                                             break;

                                         }





                                     }catch (Exception e)
                                     {

                                         isvalid=false;
                                         break;

                                     }




                                 }
                                 else{

                                     isvalid=false;
                                     break;
                                 }

                             }
                             else{

                                 isvalid=false;
                                 break;
                             }
           }
                         else {

                             isvalid=false;
                             break;
                         }


                     }



                     if(isvalid)
                     {

                         mileStoneTarget.setMls(mileStoneDynamicAdapter.getMileStoneList());
                         Intent intent = new Intent();
                         intent.putExtra("MileStone",mileStoneTarget);
                         setResult(Activity.RESULT_OK, intent);
                         finish();

                     }
                     else{

                         Utils.showAlertWithSingle(MileStoneActivity.this, "Please enter the date properly.Amount should not be zero", new DialogEventListener() {
                             @Override
                             public void onPositiveButtonClicked() {

                             }

                             @Override
                             public void onNegativeButtonClicked() {

                             }
                         });

                     }







                 }





                }


            }
        });

        imgcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        String languagedata = LocaleHelper.getPersistedData(MileStoneActivity.this, "en");
        Context context= LocaleHelper.setLocale(MileStoneActivity.this, languagedata);

        Resources resources=context.getResources();

        txtMilestone.setText(resources.getString(R.string.addmilestone));
        btn_submit.setText(resources.getString(R.string.addmore));


    }
}