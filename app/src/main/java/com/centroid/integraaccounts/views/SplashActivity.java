package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    Thread thread=null;
    String firstpin="";

    TextView textView;
    TextView txtquote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        textView=findViewById(R.id.textView);
        txtquote=findViewById(R.id.txtquote);

        String languagedata = LocaleHelper.getPersistedData(SplashActivity.this, "en");
        Context context= LocaleHelper.setLocale(SplashActivity.this, languagedata);

        Resources resources=context.getResources();
        txtquote.setText("My Personal App");

        if(!new PreferenceHelper(SplashActivity.this).getBoolData(Utils.applockedkey))
        {
         Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.fadein);

            Animation slide = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.blink);
            txtquote.setAnimation(slide);
            textView.setAnimation(animFadeIn);


            animFadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    gotoNextActivity();


                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });






           // gotoNextActivity();
        }
        else {

            List<CommonData> cm = new DatabaseHelper(SplashActivity.this).getData(Utils.DBtables.TABLE_APP_PIN);
            if (cm.size() > 0) {


                showPINDialog();


            } else {

               // gotoNextActivity();

                Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fadein);

                Animation slide = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.blink);
                txtquote.setAnimation(slide);
                textView.setAnimation(animFadeIn);

                animFadeIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        gotoNextActivity();


                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        }


    }


    public void showPINDialog()
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        double h=displayMetrics.heightPixels/1.2;
        int height = (int)h;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_pindialog);
        dialog.setCancelable(false);

        final EditText edtName=dialog.findViewById(R.id.edtName);
        ImageView imgclear=dialog.findViewById(R.id.imgclear);


        //isfirstpinsubmitted=false;
        final   List<CommonData>cm=new DatabaseHelper(SplashActivity.this).getData(Utils.DBtables.TABLE_APP_PIN);
        if(cm.size()>0)
        {

            edtName.setHint("Enter your current PIN");
        }
        else {
            edtName.setHint("Enter your PIN");
        }

        Button btn1=dialog.findViewById(R.id.btn1);
        Button btn2=dialog.findViewById(R.id.btn2);
        Button btn3=dialog.findViewById(R.id.btn3);
        Button btn4=dialog.findViewById(R.id.btn4);
        Button btn5=dialog.findViewById(R.id.btn5);
        Button btn6=dialog.findViewById(R.id.btn6);
        Button btn7=dialog.findViewById(R.id.btn7);
        Button btn8=dialog.findViewById(R.id.btn8);
        Button btn9=dialog.findViewById(R.id.btn9);
        Button btn0=dialog.findViewById(R.id.btn0);
        Button btnsubmit=dialog.findViewById(R.id.btnsubmit);

        edtName.setInputType(InputType.TYPE_NULL);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p=edtName.getText().toString();

              //  if(!isfirstpinsubmitted) {

                    if(firstpin.length()<6) {
                        firstpin = firstpin + "1";

                    }




                    edtName.setText(getDotedtextbyLength(firstpin.length()));



            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p=edtName.getText().toString();


                    if(firstpin.length()<6) {
                        firstpin = firstpin + "2";
                    }

                    // edtName.setText(firstpin);

                    edtName.setText(getDotedtextbyLength(firstpin.length()));
              //  }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String p=edtName.getText().toString();

                    if(firstpin.length()<6) {

                        firstpin = firstpin + "3";
                    }

                    // edtName.setText(firstpin);
                    edtName.setText(getDotedtextbyLength(firstpin.length()));
             //   }

            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = edtName.getText().toString();



                    if(firstpin.length()<6) {
                        firstpin = firstpin + "4";
                    }

                    edtName.setText(getDotedtextbyLength(firstpin.length()));

                    // edtName.setText(firstpin);
                //}

            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p=edtName.getText().toString();



                    if(firstpin.length()<6) {
                        firstpin = firstpin + "5";
                    }

                    // edtName.setText(firstpin);

                    edtName.setText(getDotedtextbyLength(firstpin.length()));
               // }
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if(firstpin.length()<6) {
                        firstpin = firstpin + "6";
                    }

                    // edtName.setText(firstpin);

                    edtName.setText(getDotedtextbyLength(firstpin.length()));
               // }

            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String p=edtName.getText().toString();


                    if(firstpin.length()<6) {
                        firstpin = firstpin + "7";
                    }

                    //edtName.setText(firstpin);

                    edtName.setText(getDotedtextbyLength(firstpin.length()));
              //  }

            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if(firstpin.length()<6) {
                        firstpin = firstpin + "8";
                    }
                    edtName.setText(getDotedtextbyLength(firstpin.length()));

                    // edtName.setText(firstpin);
              //  }

            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    if(firstpin.length()<6) {
                        firstpin = firstpin + "9";
                    }

                    edtName.setText(getDotedtextbyLength(firstpin.length()));

                    // edtName.setText(firstpin);
               // }

            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if(firstpin.length()<6) {
                        firstpin = firstpin + "0";
                    }

                    edtName.setText(getDotedtextbyLength(firstpin.length()));

                    //edtName.setText(firstpin);
              //  }

            }
        });

        imgclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtName.setText("");

                    firstpin = "";



            }
        });



        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {








                   if(cm.size()>0) {

                    String data = cm.get(0).getData();

                    if (data.equalsIgnoreCase(firstpin)) {
                        firstpin="";
                        edtName.setText("");
                        edtName.setHint("Enter PIN");
                        dialog.dismiss();

                        gotoNextActivity();
                     //   cm.clear();

                    }
                    else {

                        Toast.makeText(SplashActivity.this, "Wrong PIN", Toast.LENGTH_SHORT).show();
                    }
                }











            }
        });








        dialog.getWindow().setLayout(width,height);

        dialog.show();
    }

    public String  getDotedtextbyLength(int l)
    {
        String data="";
        for (int i=0;i<l;i++)
        {

            data=data+"\u2022";


        }

        return data;
    }


    private void gotoNextActivity()
    {

        thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try{

                    thread.sleep(2000);

                }catch (Exception e)
                {

                }

                if (new PreferenceHelper(SplashActivity.this).getData(Utils.userkey).equals("")) {

                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                } else {


                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();

                }

            }
        });

        thread.start();




    }
}
