package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;

public class AddBankVoucherActivity extends AppCompatActivity {

    Spinner spinnerBankaccount,banktype,spinnerCashaccount;

    EditText edtAmount,edtremarks;

    ImageView imgDate,imgback,imgDelete;

    TextView txtdate,txtHead;

    Button btnSave,btndelete;

    String date="",bankid="",cashid="";

    Accounts commonData;

    FloatingActionButton fabadd,fabaddcash;

    String  month_selected = "", yearselected = "";

    Resources resources;

    int index=0;

    String cashentryid="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_voucher);
        getSupportActionBar().hide();

        commonData=(Accounts) getIntent().getSerializableExtra("Account");

        edtremarks=findViewById(R.id.edtremarks);
        fabaddcash=findViewById(R.id.fabaddcash);
        spinnerCashaccount=findViewById(R.id.spinnerCashaccount);


        edtAmount=findViewById(R.id.edtAmount);
        spinnerBankaccount=findViewById(R.id.spinnerBankaccount);
        banktype=findViewById(R.id.banktype);
        imgDate=findViewById(R.id.imgDate);
        txtdate=findViewById(R.id.txtdate);
        btnSave=findViewById(R.id.btnSave);
        imgback=findViewById(R.id.imgback);
        txtHead=findViewById(R.id.txtHead);
        imgDelete=findViewById(R.id.imgDelete);
        fabadd=findViewById(R.id.fabadd);
        btndelete=findViewById(R.id.btndelete);

        String languagedata = LocaleHelper.getPersistedData(AddBankVoucherActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddBankVoucherActivity.this, languagedata);

        resources=context.getResources();
        edtAmount.setHint(Utils.getCapsSentences(context,resources.getString(R.string.amount)));
        edtremarks.setHint(Utils.getCapsSentences(context,resources.getString(R.string.enterremarks)));
        txtdate.setText(Utils.getCapsSentences(context,resources.getString(R.string.selectdate)));
        btnSave.setText(Utils.getCapsSentences(context,resources.getString(R.string.save)));

        txtHead.setText(Utils.getCapsSentences(context,resources.getString(R.string.bankvouchersetup)));


        String arr[]=resources.getStringArray(R.array.bankbalanceType);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        banktype.setAdapter(spinnerArrayAdapter);




        banktype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                index=i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(AddBankVoucherActivity.this, AccountsettingsActivity.class);
                i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsBankRequestcode);
                startActivityForResult(i,Utils.Requestcode.ForAccountSettingsBankRequestcode);


            }
        });


        if(commonData!=null)
        {



            try{

                btndelete.setVisibility(View.VISIBLE);
                txtHead.setText(resources.getString(R.string.editbankvoucher));
                btndelete.setText(resources.getString(R.string.delete));

                String Amount=commonData.getACCOUNTS_amount();
              //  String Type=jsonObject.getString("Type");
                String dat=commonData.getACCOUNTS_date();

              //  if(jsonObject.has("remarks")) {
                    String remarks = commonData.getACCOUNTS_remarks();
                   edtremarks.setText(remarks);
                   bankid=commonData.getACCOUNTS_setupid();

               // }

                txtdate.setText(dat);

                date=dat;
                edtAmount.setText(Amount);

                month_selected=commonData.getACCOUNTS_month();
                yearselected=commonData.getACCOUNTS_year();

                if(commonData.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit+""))
                {

                    banktype.setSelection(1);
                    index=1;

                }
                else {

                    banktype.setSelection(0);
                    index=0;


                }


                List<Accounts>accounts=new DatabaseHelper(AddBankVoucherActivity.this).getAccountsDataByEntryId(commonData.getACCOUNTS_id()+"");

                if(accounts.size()>0)
                {
                    cashentryid=accounts.get(0).getACCOUNTS_id()+"";
                    cashid=accounts.get(0).getACCOUNTS_setupid();
                }




            }catch (Exception e)
            {

            }




        }
        else {

            setDate();
        }


        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(commonData!=null)
                {

                   // new DatabaseHelper(AddBankVoucherActivity.this).deleteWalletData(commonData.getId());





                        AlertDialog.Builder builder = new AlertDialog.Builder(AddBankVoucherActivity.this);
                        builder.setMessage(resources.getString(R.string.deleteconfirm));
                        builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                dialogInterface.dismiss();


                                Executor executor=new DirectExecutor();

                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        //  updateData();


                                        try {

                                            List<Accounts>acc=new DatabaseHelper(AddBankVoucherActivity.this).getAccountsDataByEntryId(commonData.getACCOUNTS_id()+"");

                                            if(acc.size()>0)
                                            {
                                                new DatabaseHelper(AddBankVoucherActivity.this).deleteAccountDataById(acc.get(0).getACCOUNTS_id()+"");


                                            }

                                            new DatabaseHelper(AddBankVoucherActivity.this).deleteAccountDataById(commonData.getACCOUNTS_id()+"");




                                            onBackPressed();

                                        }catch (Exception e)
                                        {

                                        }







                                    }
                                });




















                            }
                        });
                        builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });

                        builder.show();
                    }




            }
        });





        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonData cm = (CommonData) spinnerBankaccount.getSelectedItem();

                CommonData cm_cash = (CommonData) spinnerCashaccount.getSelectedItem();


                if(!date.equalsIgnoreCase(""))
                {

                if(!edtAmount.getText().toString().equalsIgnoreCase(""))
                {



                    if(cm!=null) {

                        if(cm_cash!=null) {


//                        if(!edtremarks.getText().toString().equalsIgnoreCase(""))


//                        {

                        try {


                            if (commonData != null) {


                                Executor executor = new DirectExecutor();
                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {

                                        updateData();
                                    }
                                });

                            } else {

                                Executor executor = new DirectExecutor();
                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {

                                        addData();
                                    }
                                });


                            }


                        } catch (Exception e) {

                        }



                        

                        }

                        else {


                            Toast.makeText(AddBankVoucherActivity.this,"Select cash account",Toast.LENGTH_SHORT).show();
                        }


                    }

                    else {


                        Toast.makeText(AddBankVoucherActivity.this,"Select bank account",Toast.LENGTH_SHORT).show();
                    }


                }
                else {


                    Toast.makeText(AddBankVoucherActivity.this,resources.getString(R.string.amount),Toast.LENGTH_SHORT).show();
                }

                }
                else {


                    Toast.makeText(AddBankVoucherActivity.this,resources.getString(R.string.selectdate),Toast.LENGTH_SHORT).show();
                }

            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });


        imgDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDatePicker();
            }
        });

        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDatePicker();
            }
        });




        addBankAccountData();

    }


    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdate.setText(dayOfMonth + "-" + m + "-" + year);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        addBankAccountData();
    }

    public void addData()
    {

        CommonData cm = (CommonData) spinnerBankaccount.getSelectedItem();

       CommonData cm_cash = (CommonData) spinnerCashaccount.getSelectedItem();

        Accounts accounts = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.bankvoucher);
        accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());

        if(index==0) {
            accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
        }
        else {

            accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");

        }
        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);
        //  accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account+"");


long insertid=new DatabaseHelper(AddBankVoucherActivity.this).addAccountsData(accounts);



        Accounts accounts1 = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts1.setACCOUNTS_entryid(insertid+"");
        accounts1.setACCOUNTS_date(date);
        accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.bankvoucher);
        accounts1.setACCOUNTS_setupid(cm_cash.getId());//setup id is the account setup id from the dropdown
        accounts1.setACCOUNTS_amount(edtAmount.getText().toString());

        if(index==0) {
            accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
        }
        else {

            accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");

        }
        accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts1.setACCOUNTS_month(month_selected);
        accounts1.setACCOUNTS_year(yearselected);

        new DatabaseHelper(AddBankVoucherActivity.this).addAccountsData(accounts1);
        Utils.playSimpleTone(AddBankVoucherActivity.this);

        date = "";
        month_selected = "";
        yearselected = "";
        spinnerBankaccount.setSelection(0);
        edtAmount.setText("");
       // edtvoucher.setText("");
        edtremarks.setText("");
        txtdate.setText(resources.getString(R.string.selectdate));
       // spinnerBankdata.setSelection(0);


        setDate();

    }


    public void updateData()
    {



        CommonData cm = (CommonData) spinnerBankaccount.getSelectedItem();

         CommonData cm_cash = (CommonData) spinnerCashaccount.getSelectedItem();

        Accounts accounts = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.bankvoucher);
        accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());

        if(index==0) {
            accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
        }
        else {

            accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");

        }
        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);
        //  accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account+"");


      new DatabaseHelper(AddBankVoucherActivity.this).updateAccountsData(commonData.getACCOUNTS_id()+"",accounts);
        Utils.playSimpleTone(AddBankVoucherActivity.this);


        Accounts accounts1 = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts1.setACCOUNTS_entryid(commonData.getACCOUNTS_id()+"");
        accounts1.setACCOUNTS_date(date);
        accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.bankvoucher);
        accounts1.setACCOUNTS_setupid(cm_cash.getId());//setup id is the account setup id from the dropdown
        accounts1.setACCOUNTS_amount(edtAmount.getText().toString());

        if(index==0) {
            accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
        }
        else {

            accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");

        }
        accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts1.setACCOUNTS_month(month_selected);
        accounts1.setACCOUNTS_year(yearselected);

        new DatabaseHelper(AddBankVoucherActivity.this).updateAccountsData(cashentryid,accounts1);
        Utils.playSimpleTone(AddBankVoucherActivity.this);

        date = "";
        month_selected = "";
        yearselected = "";
        spinnerBankaccount.setSelection(0);
        edtAmount.setText("");
        // edtvoucher.setText("");
        edtremarks.setText("");
        txtdate.setText(resources.getString(R.string.selectdate));
        // spinnerBankdata.setSelection(0);


        setDate();


        onBackPressed();



    }




    public void addBankAccountData()
    {

        List<CommonData>commonData_filtered=new ArrayList<>();

        List<CommonData>commonData=new DatabaseHelper(AddBankVoucherActivity.this).getAccountSettingsData();

        for (CommonData cm:commonData)
        {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accounttype");

                if(acctype.equalsIgnoreCase("Bank"))
                {
                    commonData_filtered.add(cm);
                }


                //




            }catch (Exception e)
            {

            }

        }


        BankSpinnerAdapter bankSpinnerAdapter=new BankSpinnerAdapter(AddBankVoucherActivity.this,commonData_filtered);
        spinnerBankaccount.setAdapter(bankSpinnerAdapter);


        for (int i=0;i<commonData_filtered.size();i++)
        {

            if(commonData_filtered.get(i).getId().equalsIgnoreCase(bankid))
            {

                spinnerBankaccount.setSelection(i);
                break;
            }
        }



        List<CommonData>commonData_filtered_cash=new ArrayList<>();

        for (CommonData cm:commonData)
        {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accounttype");

                if(acctype.equalsIgnoreCase("Cash"))
                {
                    commonData_filtered_cash.add(cm);
                }


                //




            }catch (Exception e)
            {

            }

        }

        BankSpinnerAdapter bankSpinnerAdapter1=new BankSpinnerAdapter(AddBankVoucherActivity.this,commonData_filtered_cash);
        spinnerCashaccount.setAdapter(bankSpinnerAdapter1);


        for (int i=0;i<commonData_filtered_cash.size();i++)
        {

            if(commonData_filtered_cash.get(i).getId().equalsIgnoreCase(cashid))
            {

                spinnerCashaccount.setSelection(i);
                break;
            }
        }



    }




    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        final int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddBankVoucherActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                date=i2+"-"+m+"-"+i;
                
                month_selected=m+"";
                yearselected=year+"";

                txtdate.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }
}
