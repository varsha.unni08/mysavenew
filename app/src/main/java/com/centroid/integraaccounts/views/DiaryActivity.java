package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;


import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.CustomTypefaceSpan;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.DiaryDataAdapter;
import com.centroid.integraaccounts.adapter.DiaryHolderAdapter;
import com.centroid.integraaccounts.adapter.DiarySubjectAdapter;
import com.centroid.integraaccounts.adapter.DiarySubjectDataAdapter;
import com.centroid.integraaccounts.app.CustomDocumentRenderer;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.DiarySubject;
import com.centroid.integraaccounts.data.domain.Diarydata;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;


//import org.apache.poi.xwpf.usermodel.XWPFDocument;
//import org.apache.poi.xwpf.usermodel.XWPFParagraph;
//import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.json.JSONObject;
//import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
//import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
//import org.apache.poi.xwpf.usermodel.XWPFDocument;
//import org.apache.poi.xwpf.usermodel.XWPFParagraph;
//import org.apache.poi.xwpf.usermodel.XWPFRun;
//import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;

public class DiaryActivity extends AppCompatActivity {

    ImageView imgback,imgstartDatepick,imgendDatepick;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    List<CommonData> diarydata_selected=null;

    String startdate="",endate="";

    Resources resources;
    TextView txtHead,txtstartdatepick,txtenddatepick,txtdiarysubject;

    ImageView imgdropdown;

    Button btnSearch;
    Spinner spinnerSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtHead=findViewById(R.id.txtHead);

        imgendDatepick=findViewById(R.id.imgendDatepick);
        imgstartDatepick=findViewById(R.id.imgstartDatepick);

        txtstartdatepick=findViewById(R.id.txtstartdatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);
        spinnerSubject=findViewById(R.id.spinnerSubject);
        txtdiarysubject=findViewById(R.id.txtdiarysubject);
        imgdropdown=findViewById(R.id.imgdropdown);

        txtdiarysubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAllDiarySubjectDialog();
            }
        });

        imgdropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAllDiarySubjectDialog();
            }
        });


        String languagedata = LocaleHelper.getPersistedData(DiaryActivity.this, "en");
        Context context= LocaleHelper.setLocale(DiaryActivity.this, languagedata);

        resources=context.getResources();

        txtHead.setText(resources.getString(R.string.diary));
        txtstartdatepick.setText(Utils.getCapsSentences(DiaryActivity.this,resources.getString(R.string.selectstartdate)));
        txtenddatepick.setText(Utils.getCapsSentences(DiaryActivity.this,resources.getString(R.string.selectenddate)));
        btnSearch.setText(resources.getString(R.string.search));

        Utils.showTutorial(resources.getString(R.string.mydiarytutorial),DiaryActivity.this,Utils.Tutorials.mydiarytutorial);
        getAllSubjects();



        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });



        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (!startdate.equalsIgnoreCase("")) {
                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate))
                            {


getDiaryDataFromdate(strDate,endDate);


                            }

                            else {

                                Toast.makeText(DiaryActivity.this, resources.getString(R.string.selectdateproperly), Toast.LENGTH_SHORT).show();
                            }





                        } else {

                            Toast.makeText(DiaryActivity.this, resources.getString(R.string.selectenddate), Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(DiaryActivity.this, resources.getString(R.string.selectstartdate), Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e)
                {

                }

            }
        });






        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(DiaryActivity.this,WriteDiaryActivity.class));

            }
        });
        getDiaryData();

    }


    public void showAllDiarySubjectDialog()
    {
        List<CommonData>commonData = new DatabaseHelper(DiaryActivity.this).getData(Utils.DBtables.DIARYSUBJECT_table);

        Dialog  dialog = new Dialog(DiaryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_allsubs);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        RecyclerView recycler=dialog.findViewById(R.id.recycler);
        FloatingActionButton fabadd=dialog.findViewById(R.id.fabadd);
        TextView txtNodata=dialog.findViewById(R.id.txtNodata);

        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                showAddSubjectDialog();







            }
        });

        if(commonData.size()>0)
        {
            recycler.setLayoutManager(new LinearLayoutManager(DiaryActivity.this));
            recycler.setAdapter(new DiarySubjectDataAdapter(DiaryActivity.this, commonData));

        }
        else {
            recycler.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);

        }






        dialog.show();




    }

    public void showAddSubjectDialog()
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog=new Dialog(DiaryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialogsubject);



        dialog.getWindow().setLayout(width,height);


        final   EditText edtdiarycontent=dialog.findViewById(R.id.edtdiarycontent);

        Button btnAdd=dialog.findViewById(R.id.btnAdd);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!edtdiarycontent.getText().toString().equalsIgnoreCase(""))
                {


                    dialog.dismiss();

                    new DatabaseHelper(DiaryActivity.this).addData(Utils.DBtables.DIARYSUBJECT_table,edtdiarycontent.getText().toString());
                    Utils.playSimpleTone(DiaryActivity.this);

                    getAllSubjects();



                }
                else {

//                            Toast.makeText(WriteDiaryActivity.this,"Enter the subject",Toast.LENGTH_SHORT).
//                                    show();


                    Utils.showAlertWithSingle(DiaryActivity.this, "Enter the subject", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }


            }
        });




        dialog.show();
    }



    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(DiaryActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else {

                    endate = i2 + "-" + m + "-" + i;

                    txtenddatepick.setText(i2 + "-" + m + "-" + i);
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }



    public void deleteDataAccordingtoSubject(String subjectid)
    {
        List<CommonData> diarydata = new DatabaseHelper(DiaryActivity.this).getData(Utils.DBtables.DIARY_table);
        for (CommonData cd : diarydata) {

            try {


                JSONObject jsonObject = new JSONObject(cd.getData());

                String subject = jsonObject.getString("subject");

                if(subject.equalsIgnoreCase(subjectid))
                {

                    new DatabaseHelper(DiaryActivity.this).deleteData(cd.getId(),Utils.DBtables.DIARY_table);
                    Utils.playSimpleTone(DiaryActivity.this);
                }




            }catch (Exception e)
            {

            }


        }


        getDiaryData();
        getAllSubjects();

    }



    public void getDiaryDataFromdate(Date startdate,Date enddate)
    {

        try {




            CommonData cm_subject=(CommonData) spinnerSubject.getSelectedItem();

            List<CommonData> sorteddate = new ArrayList<>();

            List<DiarySubject>diarySubjects=new ArrayList<>();

            Set<String>diarySubjectsid=new HashSet<>();


            List<CommonData> diarydata = new DatabaseHelper(DiaryActivity.this).getData(Utils.DBtables.DIARY_table);


            Collections.reverse(diarydata);

            String subid="0";


            for (CommonData cd : diarydata) {






                    JSONObject jsonObject=new JSONObject(cd.getData());

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date diarydate = sdf.parse(jsonObject.getString("date"));

                String subject=jsonObject.getString("subject");

                if(diarydate.equals(startdate)&&diarydate.before(enddate)) {
                    if (subject.equalsIgnoreCase(cm_subject.getId())) {


                        sorteddate.add(cd);
                    }


                }


                else  if(diarydate.after(startdate) && diarydate.equals(enddate))
                {
                    if (subject.equalsIgnoreCase(cm_subject.getId())) {


                        sorteddate.add(cd);
                    }


                }

                else if(diarydate.equals(startdate) && diarydate.equals(enddate))
                {
                    if (subject.equalsIgnoreCase(cm_subject.getId())) {


                        sorteddate.add(cd);
                    }

                }

                 else if (diarydate.after(startdate) && diarydate.before(enddate)) {
                        if (subject.equalsIgnoreCase(cm_subject.getId())) {


                            sorteddate.add(cd);
                        }
                    }






            }

            for (CommonData data:sorteddate)
            {
                JSONObject jsonObject=new JSONObject(data.getData());
                String subject=jsonObject.getString("subject");
                subid=subject;
                diarySubjectsid.add(subid);
            }


            Iterator<String> it = diarySubjectsid.iterator();
            while(it.hasNext()){


                DiarySubject diarySubject=new DiarySubject();
                diarySubject.setData(it.next());
                diarySubject.setSelected(0);
                diarySubjects.add(diarySubject);


            }

            DiaryHolderAdapter diaryHolderAdapter=new DiaryHolderAdapter(DiaryActivity.this,diarySubjects,sorteddate);
            recycler.setLayoutManager(new LinearLayoutManager(DiaryActivity.this));
            recycler.setAdapter(diaryHolderAdapter);





//            DiaryDataAdapter diaryDataAdapter = new DiaryDataAdapter(DiaryActivity.this, sorteddate);
//            recycler.setLayoutManager(new LinearLayoutManager(DiaryActivity.this));
//            recycler.setAdapter(diaryDataAdapter);
        }catch (Exception e)
        {

        }

    }

    public void getAllSubjects()
    {

        List<CommonData>commonData = new DatabaseHelper(DiaryActivity.this).getData(Utils.DBtables.DIARYSUBJECT_table);


        if(commonData.size()>0)
        {

            txtdiarysubject.setText(commonData.get(0).getData());


         //   spinnerSubject.setAdapter(new DiarySubjectAdapter(DiaryActivity.this,commonData));



        }
        else {

            txtdiarysubject.setText("");
        }






    }



    public void getDiaryData()
    {

        try {
            Set<String>diarySubjectsid=new HashSet<>();
            List<DiarySubject>diarySubjects=new ArrayList<>();

            List<CommonData> diarydata = new DatabaseHelper(DiaryActivity.this).getData(Utils.DBtables.DIARY_table);

            Collections.reverse(diarydata);

            for (CommonData data : diarydata) {
                JSONObject jsonObject = new JSONObject(data.getData());
                String subject = jsonObject.getString("subject");
               // subid = subject;
                diarySubjectsid.add(subject);
            }


            Iterator<String> it = diarySubjectsid.iterator();
            while (it.hasNext()) {


                DiarySubject diarySubject = new DiarySubject();
                diarySubject.setData(it.next());
                diarySubject.setSelected(0);
                diarySubjects.add(diarySubject);


            }

            DiaryHolderAdapter diaryHolderAdapter = new DiaryHolderAdapter(DiaryActivity.this, diarySubjects,diarydata);
            recycler.setLayoutManager(new LinearLayoutManager(DiaryActivity.this));
            recycler.setAdapter(diaryHolderAdapter);

        }catch (Exception e)
        {

        }

//        DiaryDataAdapter diaryDataAdapter=new DiaryDataAdapter(DiaryActivity.this,diarydata);
//        recycler.setLayoutManager(new LinearLayoutManager(DiaryActivity.this));
//        recycler.setAdapter(diaryDataAdapter);


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDiaryData();
        getAllSubjects();

        String languagedata = LocaleHelper.getPersistedData(DiaryActivity.this, "en");
        Context context= LocaleHelper.setLocale(DiaryActivity.this, languagedata);

        Resources resources=context.getResources();

        txtHead.setText(resources.getString(R.string.diary));
    }



    public void exportPdf(CommonData diarydata)
    {
        //this.diarydata_selected=diarydata;

      if(ContextCompat.checkSelfPermission(DiaryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
      {


          ActivityCompat.requestPermissions(DiaryActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);

      }
      else {

         // export();
         // showFullData();

      }

    }


    public void showFullData()
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        double h=displayMetrics.heightPixels/1.2;
        int height = (int)h;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(DiaryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_fulldiarydata);

        final TextView txtTitle=dialog.findViewById(R.id.txtTitle);

        final TextView txtData=dialog.findViewById(R.id.txtData);
        final Button btnDownload=dialog.findViewById(R.id.btnDownload);
        final ScrollView iv=dialog.findViewById(R.id.scroll);

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();
                try {

                  File folder = new File(DiaryActivity.this.getExternalCacheDir() + "/" + "IntegraAccounts");
//
                    if (!folder.exists()) {
                        folder.mkdir();
                    }
//
//
//                    Long tsLong = System.currentTimeMillis() / 1000;
//                    String ts = tsLong.toString();
//
//                    File file = new File(folder.getAbsolutePath() + "/" + ts + ".pdf");
//
//                    if (!file.exists()) {
//                        file.createNewFile();
//                    }

                    Bitmap bitmap = Bitmap.createBitmap(
                            iv.getChildAt(0).getWidth() * 2,
                            iv.getChildAt(0).getHeight() * 2,
                            Bitmap.Config.ARGB_8888);
                    Canvas c = new Canvas(bitmap);
                    c.scale(2.0f, 2.0f);
                    c.drawColor(Color.parseColor("#000000"));
                    iv.getChildAt(0).draw(c);

                    String ff = System.currentTimeMillis()+"";
                    File file1 = new File(folder.getAbsolutePath() + "/" + ff + ".png");

                    if (!file1.exists()) {
                        file1.createNewFile();
                    }
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();






//write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file1);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();




                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        Uri photoURI = FileProvider.getUriForFile(DiaryActivity.this, getApplicationContext().getPackageName() + ".provider", file1);

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(photoURI, "image/*");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);

                    } else {

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file1), "image/*");
                        startActivity(intent);
                    }

                }catch (Exception e)
                {

                }


            }
        });


        try{
            JSONObject jsonObject = new JSONObject(diarydata_selected.get(0).getData());

            String subject = jsonObject.getString("subject");
            String contentsubject = "";

            List<CommonData> commonData = new DatabaseHelper(DiaryActivity.this).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);
StringBuilder stringBuilder=new StringBuilder();
            if (commonData.size() > 0) {

                contentsubject = commonData.get(0).getData();
                SpannableString content = new SpannableString(contentsubject);
                content.setSpan(new UnderlineSpan(), 0, contentsubject.length()-1, 0);



                stringBuilder.append("Subject : "+content.toString()+"\n\n");
            }
            txtTitle.setText(stringBuilder.toString());

            for (CommonData data:diarydata_selected) {


                StringBuilder ss=new StringBuilder();
                JSONObject jsonObject1 = new JSONObject(data.getData());




                String date = jsonObject1.getString("date");
                String content = jsonObject1.getString("content");
                ss.append(date+"\n");
//                stringBuilder.append(content+"\n\n");

                ss.append("പത്ത് വര്ഷങ്ങള്ക്ക് ശേഷവും അതേ ആകാംക്ഷയോടെയാണ് ഇവിടേയ്ക്ക് വരുന്നത്; സിംഗപ്പൂര് യാത്രയില് അഹാനയും കുടുംബവും"+"\n\nഅവലു ജനിപാൻ ഗിൽവാഹന ധമപദടക്ഷം ദീയഷീം ഇൻമാതുള ലോപന കഥിനെതി പൂപ്പകുമഗ്രി ല ശയചിവി ബില്ലായി കസ്ഗമില്ല ഈമാരാകുലെയ ബേഷനോ. കേസിത്തമാക്കാർ സംബതി ദന ആഞ്ഞിളംഞ്ഞാഴിക്കുട പാനതാനി എന്താലി ബാനി ന്നിക്കു എന്നവ്യാ ജെരിയാം ദസേ. മാമണ്ടി നിസ്\u200Cമാർണിതം ന്നിക്കു ഭടികി പാർകിസിമിതം പ്രധിബന്ന തിണതാനം കിളിമാര പിണി. കീഴ്നട്ട പാന്തേച്ചി ആറുമേടി സുസ്പെംദിഷ അവമിത്തിൽ മർക്കശ്ശേന റൂ കുകുടച്ച പോദിരി ഗോത്രീഹ കിണി ഉളഗ് മാര്\u200Dക്കിസര്\u200D മോച ധമപദടക്ഷം. ചുറാഗി മാലകരൂപ്പച്ചി പ്രവാലം ഗോവൽ ടൈലോമുതുഖ്യം നലസാഹി ആരികി ഓത്തി സുടുവിസ്താ ദുശ്ചിഗ ലേറി ളാഹജ കമെസ്സായിണു മിനാസ്\u200Cഥ അമെത്.അവലു ജനിപാൻ ഗിൽവാഹന ധമപദടക്ഷം ദീയഷീം ഇൻമാതുള ലോപന കഥിനെതി പൂപ്പകുമഗ്രി ല ശയചിവി ബില്ലായി കസ്ഗമില്ല ഈമാരാകുലെയ ബേഷനോ. കേസിത്തമാക്കാർ സംബതി ദന ആഞ്ഞിളംഞ്ഞാഴിക്കുട പാനതാനി എന്താലി ബാനി ന്നിക്കു എന്നവ്യാ ജെരിയാം ദസേ. മാമണ്ടി നിസ്\u200Cമാർണിതം ന്നിക്കു ഭടികി പാർകിസിമിതം പ്രധിബന്ന തിണതാനം കിളിമാര പിണി. കീഴ്നട്ട പാന്തേച്ചി ആറുമേടി സുസ്പെംദിഷ അവമിത്തിൽ മർക്കശ്ശേന റൂ കുകുടച്ച പോദിരി ഗോത്രീഹ കിണി ഉളഗ് മാര്\u200Dക്കിസര്\u200D മോച ധമപദടക്ഷം. ചുറാഗി മാലകരൂപ്പച്ചി പ്രവാലം ഗോവൽ ടൈലോമുതുഖ്യം നലസാഹി ആരികി ഓത്തി സുടുവിസ്താ ദുശ്ചിഗ ലേറി ളാഹജ കമെസ്സായിണു മിനാസ്\u200Cഥ അമെത്.അവലു ജനിപാൻ ഗിൽവാഹന ധമപദടക്ഷം ദീയഷീം ഇൻമാതുള ലോപന കഥിനെതി പൂപ്പകുമഗ്രി ല ശയചിവി ബില്ലായി കസ്ഗമില്ല ഈമാരാകുലെയ ബേഷനോ. കേസിത്തമാക്കാർ സംബതി ദന ആഞ്ഞിളംഞ്ഞാഴിക്കുട പാനതാനി എന്താലി ബാനി ന്നിക്കു എന്നവ്യാ ജെരിയാം ദസേ. മാമണ്ടി നിസ്\u200Cമാർണിതം ന്നിക്കു ഭടികി പാർകിസിമിതം പ്രധിബന്ന തിണതാനം കിളിമാര പിണി. കീഴ്നട്ട പാന്തേച്ചി ആറുമേടി സുസ്പെംദിഷ അവമിത്തിൽ മർക്കശ്ശേന റൂ കുകുടച്ച പോദിരി ഗോത്രീഹ കിണി ഉളഗ് മാര്\u200Dക്കിസര്\u200D മോച ധമപദടക്ഷം. ചുറാഗി മാലകരൂപ്പച്ചി പ്രവാലം ഗോവൽ ടൈലോമുതുഖ്യം നലസാഹി ആരികി ഓത്തി സുടുവിസ്താ ദുശ്ചിഗ ലേറി ളാഹജ കമെസ്സായിണു മിനാസ്\u200Cഥ അമെത്.അവലു ജനിപാൻ ഗിൽവാഹന ധമപദടക്ഷം ദീയഷീം ഇൻമാതുള ലോപന കഥിനെതി പൂപ്പകുമഗ്രി ല ശയചിവി ബില്ലായി കസ്ഗമില്ല ഈമാരാകുലെയ ബേഷനോ. കേസിത്തമാക്കാർ സംബതി ദന ആഞ്ഞിളംഞ്ഞാഴിക്കുട പാനതാനി എന്താലി ബാനി ന്നിക്കു എന്നവ്യാ ജെരിയാം ദസേ. മാമണ്ടി നിസ്\u200Cമാർണിതം ന്നിക്കു ഭടികി പാർകിസിമിതം പ്രധിബന്ന തിണതാനം കിളിമാര പിണി. കീഴ്നട്ട പാന്തേച്ചി ആറുമേടി സുസ്പെംദിഷ അവമിത്തിൽ മർക്കശ്ശേന റൂ കുകുടച്ച പോദിരി ഗോത്രീഹ കിണി ഉളഗ് മാര്\u200Dക്കിസര്\u200D മോച ധമപദടക്ഷം. ചുറാഗി മാലകരൂപ്പച്ചി പ്രവാലം ഗോവൽ ടൈലോമുതുഖ്യം നലസാഹി ആരികി ഓത്തി സുടുവിസ്താ ദുശ്ചിഗ ലേറി ളാഹജ കമെസ്സായിണു മിനാസ്\u200Cഥ അമെത്.അവലു ജനിപാൻ ഗിൽവാഹന ധമപദടക്ഷം ദീയഷീം ഇൻമാതുള ലോപന കഥിനെതി പൂപ്പകുമഗ്രി ല ശയചിവി ബില്ലായി കസ്ഗമില്ല ഈമാരാകുലെയ ബേഷനോ. കേസിത്തമാക്കാർ സംബതി ദന ആഞ്ഞിളംഞ്ഞാഴിക്കുട പാനതാനി എന്താലി ബാനി ന്നിക്കു എന്നവ്യാ ജെരിയാം ദസേ. മാമണ്ടി നിസ്\u200Cമാർണിതം ന്നിക്കു ഭടികി പാർകിസിമിതം പ്രധിബന്ന തിണതാനം കിളിമാര പിണി. കീഴ്നട്ട പാന്തേച്ചി ആറുമേടി സുസ്പെംദിഷ അവമിത്തിൽ മർക്കശ്ശേന റൂ കുകുടച്ച പോദിരി ഗോത്രീഹ കിണി ഉളഗ് മാര്\u200Dക്കിസര്\u200D മോച ധമപദടക്ഷം. ചുറാഗി മാലകരൂപ്പച്ചി പ്രവാലം ഗോവൽ ടൈലോമുതുഖ്യം നലസാഹി ആരികി ഓത്തി സുടുവിസ്താ ദുശ്ചിഗ ലേറി ളാഹജ കമെസ്സായിണു മിനാസ്\u200Cഥ അമെത്.123");

                txtData.setText(ss.toString());
            }



        }catch (Exception e)
        {


        }







        dialog.getWindow().setLayout(width,height);


        dialog.show();

    }






    public void exportFullSubject( List<CommonData> diarydataList)
    {
        this.diarydata_selected=diarydataList;

        if(ContextCompat.checkSelfPermission(DiaryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {


            ActivityCompat.requestPermissions(DiaryActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);

        }
        else {

            String languagedata = LocaleHelper.getPersistedData(DiaryActivity.this, "en");
            Context context= LocaleHelper.setLocale(DiaryActivity.this, languagedata);

            resources=context.getResources();

            String arr[]={resources.getString(R.string.sharetext),resources.getString(R.string.sharepdf)};



            new AlertDialog.Builder(DiaryActivity.this)
                    .setSingleChoiceItems(arr, 0, null)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                            // Do something useful withe the position of the selected radio button
try {
    if (selectedPosition == 0) {
        String sharetext = "";
        StringBuilder stringBuilder=new StringBuilder();
        JSONObject jsonObject = new JSONObject(diarydata_selected.get(0).getData());

        String subject = jsonObject.getString("subject");

        String contentsubject = "";
        String language ="";

        List<CommonData> commonData = new DatabaseHelper(DiaryActivity.this).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);

        if (commonData.size() > 0) {

            contentsubject = commonData.get(0).getData();
            SpannableString content = new SpannableString(contentsubject);
            content.setSpan(new UnderlineSpan(), 0, contentsubject.length()-1, 0);



            stringBuilder.append("Subject : "+content.toString()+"\n\n");
        }


        for (CommonData data:diarydata_selected) {


            JSONObject jsonObject1 = new JSONObject(data.getData());




            String date = jsonObject1.getString("date");
            String content = jsonObject1.getString("content");

            language = jsonObject1.getString("language");
            stringBuilder.append(date+"\n");
            stringBuilder.append(content+"\n\n");

            // stringBuilder.append("പത്ത് വര്ഷങ്ങള്ക്ക് ശേഷവും അതേ ആകാംക്ഷയോടെയാണ് ഇവിടേയ്ക്ക് വരുന്നത്; സിംഗപ്പൂര് യാത്രയില് അഹാനയും കുടുംബവും"+"\n\n");

        }


        sharetext = stringBuilder.toString();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);

        shareIntent.setType("text/plain");

        shareIntent.putExtra(Intent.EXTRA_TEXT, sharetext);

        startActivity(Intent.createChooser(shareIntent, "Share Diary as"));
    } else if (selectedPosition == 1) {
        export();
    }
//                                else  if(selectedPosition==2){
//                                    shareVisitCardAsPdf(card);
//
//                                }

}catch (Exception e)
{

}

                        }
                    })
                    .show();





           // showFullData();


        }
    }








    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

       // exportPdf(diarydata_selected);
    }

    public void export()
    {
        try{
      //  PdfWriter pdfWriter

            File folder=new File(this.getExternalCacheDir()+"/"+"IntegraAccounts");

            if(!folder.exists())
            {
                folder.mkdir();
            }


            Long tsLong = System.currentTimeMillis()/1000;
            String ts = tsLong.toString();






            SpannableStringBuilder stringBuilder=new SpannableStringBuilder();


            JSONObject jsonObject = new JSONObject(diarydata_selected.get(0).getData());

            String subject = jsonObject.getString("subject");

            File file=new File(folder.getAbsolutePath()+"/"+subject+"_"+ts+".docx");

            if(!file.exists())
            {
                file.createNewFile();
            }

            String contentsubject = "";
            String language ="";






            List<CommonData> commonData = new DatabaseHelper(DiaryActivity.this).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);

            if (commonData.size() > 0) {

                contentsubject = commonData.get(0).getData();
                SpannableString content = new SpannableString(contentsubject);
                content.setSpan(new UnderlineSpan(), 0, contentsubject.length()-1, 0);



                stringBuilder.append(content.toString()+"\n\n");
            }


            for (CommonData data:diarydata_selected) {


                JSONObject jsonObject1 = new JSONObject(data.getData());




                String date = jsonObject1.getString("date");
                String content = jsonObject1.getString("content");

                 language = jsonObject1.getString("language");
                stringBuilder.append(date+"\n\n");
                stringBuilder.append(content+"\n\n");


            }

            FileWriter writer = new FileWriter(file);
           writer.append(stringBuilder.toString());
                    writer.flush();
            writer.close();















            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(DiaryActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "*/*");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
              startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "*/*");
                startActivity(intent);
            }




        }catch (Exception e)
        {
Log.e("TAAG",e.toString());
        }



    }
}
