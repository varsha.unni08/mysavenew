package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.LiabilitiesAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.List;

public class LiabilitiesActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    ImageView imgback;

    TextView txtTotal,txtprofile;

    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liabilities);
        getSupportActionBar().hide();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        txtTotal=findViewById(R.id.txtTotal);
        txtprofile=findViewById(R.id.txtprofile);


        String languagedata = LocaleHelper.getPersistedData(LiabilitiesActivity.this, "en");
        Context context= LocaleHelper.setLocale(LiabilitiesActivity.this, languagedata);

         resources=context.getResources();

        txtprofile.setText(Utils.getCapsSentences(LiabilitiesActivity.this,resources.getString(R.string.listmyliabilities)));

        Utils.showTutorial(resources.getString(R.string.liabilitytutorial),LiabilitiesActivity.this,Utils.Tutorials.liabilitytutorial);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(LiabilitiesActivity.this, AddLiabilitiesActivity.class));
            }
        });

        getAllLiabilities();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getAllLiabilities();
    }

    public void getAllLiabilities()
    {








        List<CommonData>commonData=new DatabaseHelper(LiabilitiesActivity.this).getData(Utils.DBtables.TABLE_LIABILITY);


        if(commonData.size()>0)
        {

            LiabilitiesAdapter liabilitiesAdapter=new LiabilitiesAdapter(LiabilitiesActivity.this,commonData);
            recycler.setLayoutManager(new LinearLayoutManager(LiabilitiesActivity.this));
            recycler.setAdapter(liabilitiesAdapter);

//            {"loantype":"EMI","amount":"2500","emicount":"25","loan":"{\"data\":\"{\\\"name\\\":\\\"house loan\\\",\\\"amount\\\":\\\"8500\\\"}\",\"id\":\"4\"}","dateofpayment":27}

            double t=0;

            for(CommonData cm:commonData)
            {

                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String Amount = jsonObject.getString("openingbalance");



                    t = t + Double.parseDouble(Amount);
                }catch (Exception e)
                {

                }

            }

            txtTotal.setText(resources.getString(R.string.total)+" : "+ t+ resources.getString(R.string.rs));


        }
        else {


            txtTotal.setText(resources.getString(R.string.total)+" : 0 "+resources.getString(R.string.rs));
        }



    }
}
