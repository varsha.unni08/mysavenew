package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.TransactionsAdapter;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TransactionsActivity extends AppCompatActivity {

    ImageView imgback,imgstartDatepick,imgendDatepick;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    CommonData diarydata_selected=null;

    String startdate="",endate="";

    LinearLayout layout_head;

    Resources resources;
    TextView txtHead,txtstartdatepick,txtenddatepick;

    Button btnSearch;

    String month_selected="",yearselected="";
    //Spinner spinnerSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtHead=findViewById(R.id.txtHead);
        layout_head=findViewById(R.id.layout_head);

        imgendDatepick=findViewById(R.id.imgendDatepick);
        imgstartDatepick=findViewById(R.id.imgstartDatepick);

        txtstartdatepick=findViewById(R.id.txtstartdatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);
        String languagedata = LocaleHelper.getPersistedData(TransactionsActivity.this, "en");
        Context context= LocaleHelper.setLocale(TransactionsActivity.this, languagedata);

        Resources resources=context.getResources();
        btnSearch.setText(resources.getString(R.string.search));

        Utils.showTutorial(resources.getString(R.string.transactiontutorial),TransactionsActivity.this,Utils.Tutorials.transactiontutorial);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });



        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (!startdate.equalsIgnoreCase("")) {

                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate)||endDate.equals(strDate))
                            {

                                List<Accounts>accountsbydate= Utils.getAccountsBetweenDates(TransactionsActivity.this,strDate,endDate);




                                showTransactions(accountsbydate);

                            }

                            else {

                                Toast.makeText(TransactionsActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();
                            }


                        } else {

                            Toast.makeText(TransactionsActivity.this, "Select end date", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(TransactionsActivity.this, "Select start date", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e)
                {

                }
            }
        });

        getBillListByDate();


    }




    public void getBillListByDate()
    {
        try {
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH) + 1;
            month_selected = m + "";
            yearselected = calendar.get(Calendar.YEAR) + "";
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);


            startdate = "1" + "-" + m + "-" + yearselected;

            endate = dayOfMonth + "-" + m + "-" + yearselected;

            txtstartdatepick.setText(startdate);
            txtenddatepick.setText(endate);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(endate);

            if (endDate.after(strDate) || endDate.equals(strDate)) {

                List<Accounts>accountsbydate= Utils.getAccountsBetweenDates(TransactionsActivity.this,strDate,endDate);


        showTransactions(accountsbydate);

                // showTransactions(accountsbydate);

            }


            // getAccounthead();

        }catch (Exception e)
        {

        }




    }







    public void showTransactions(List<Accounts>accounts)
    {

        if(accounts.size()>0)
        {
            layout_head.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.VISIBLE);


            Collections.sort(accounts, new Comparator<Accounts>() {
                @Override
                public int compare(Accounts accounts, Accounts t1) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate=null;

                    Date endDate=null;
                    try {


                        strDate = sdf.parse(accounts.getACCOUNTS_date());
                        endDate = sdf.parse(t1.getACCOUNTS_date());




                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return endDate.compareTo(strDate);
                }
            });


           // Collections.reverse(accounts);



            recycler.setLayoutManager(new LinearLayoutManager(TransactionsActivity.this));
            recycler.setAdapter(new TransactionsAdapter(TransactionsActivity.this,accounts));
        }
        else {
            layout_head.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);
        }



    }



    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(TransactionsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else {

                    endate = i2 + "-" + m + "-" + i;

                    txtenddatepick.setText(i2 + "-" + m + "-" + i);
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }
}
