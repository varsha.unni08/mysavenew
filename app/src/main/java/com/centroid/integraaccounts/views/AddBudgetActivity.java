package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.fragments.ExpenseDialogFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class AddBudgetActivity extends AppCompatActivity {

    NumberPicker npmonth;

    Spinner npyear;
    Button save;
    TextView txtHead,txtSelectYear,txtSpinner;

    ImageView imgback,imgDelete;
    Spinner spinnerAccountName;
    EditText edtAmount;

    List<CommonData>cmfiltered;

    final String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    CommonData budget;
    String accountname="0";

    FloatingActionButton fabadd;

    Resources resources;

    CommonData commonData_selected=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);
        getSupportActionBar().hide();

        cmfiltered=new ArrayList<>();
        budget=(CommonData) getIntent().getSerializableExtra("budget");

        edtAmount=findViewById(R.id.edtAmount);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        imgback=findViewById(R.id.imgback);
        save=findViewById(R.id.save);
        npmonth=findViewById(R.id.npmonth);
        npyear=findViewById(R.id.npyear);
        txtHead=findViewById(R.id.txtHead);
        fabadd=findViewById(R.id.fabadd);
        imgDelete=findViewById(R.id.imgDelete);
        txtSelectYear=findViewById(R.id.txtSelectYear);
        txtSpinner=findViewById(R.id.txtSpinner);



        String languagedata = LocaleHelper.getPersistedData(AddBudgetActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddBudgetActivity.this, languagedata);

        resources=context.getResources();

        txtHead.setText(resources.getString(R.string.edit));
        edtAmount.setHint(resources.getString(R.string.amount));
        txtSelectYear.setText(resources.getString(R.string.selectyear));
        save.setText(resources.getString(R.string.update));


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // Toast.makeText(AddBudgetActivity.this,"Add account settings in Insurance category",Toast.LENGTH_SHORT).show();

                startActivity(new Intent(AddBudgetActivity.this, AccountsettingsActivity.class));




            }
        });


        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(budget!=null)
                {

                    new DatabaseHelper(AddBudgetActivity.this).deleteData(budget.getId(),Utils.DBtables.TABLE_BUDGET);
                    Utils.playSimpleTone(AddBudgetActivity.this);
                    onBackPressed();


                }

            }
        });




       // Calendar calendar=Calendar.getInstance();
        //final int year=calendar.get(Calendar.YEAR);



        Calendar calendar=Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int m=calendar.get(Calendar.MONTH);
        List<String> yeardata=new ArrayList<>();

        for (int i=year;i<year+5;i++)
        {

            yeardata.add(i+"");
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(AddBudgetActivity.this,   android.R.layout.simple_spinner_item, yeardata.toArray(new String[yeardata.size()]));

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        npyear.setAdapter(spinnerArrayAdapter);

        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setDisplayedValues(arrmonth);
        npmonth.setValue(m);







        if(budget!=null)
        {
            try{

                imgDelete.setVisibility(View.VISIBLE);

//                txtHead.setText("Edit Budget");
//                save.setText("Update");

                JSONObject jsonObject=new JSONObject(budget.getData());
                int year1=   jsonObject.getInt("year");
                String  month= jsonObject.getString("month");
                 accountname= jsonObject.getString("accountname");
                String amount= jsonObject.getString("amount");

                edtAmount.setText(amount);

                for (int i=0;i<yeardata.size();i++)
                {
                    if(yeardata.get(i).equalsIgnoreCase(year+""))
                    {
                        npyear.setSelection(i);
                        break;
                    }


                }




                for (int i=0;i<arrmonth.length;i++)
                {

                   if(arrmonth[i].equalsIgnoreCase(month)) {

                       npmonth.setValue(i);
                       break;
                   }
                }



            }catch (Exception e)
            {

            }


        }











        addAccountSettings();

        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ExpenseDialogFragment expenseDialogFragment=new ExpenseDialogFragment(new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {


                        try {

                            commonData_selected=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = Utils.getCapsSentences(context,jcmn1.getString("Accountname"));

                            txtSpinner.setText(Utils.getCapsSentences(context,acctype));

                           // isAlreadybudgetAdded=false;

                           // getBudgetData();


                        }catch (Exception e)
                        {

                        }

                    }
                });

                expenseDialogFragment.show(getSupportFragmentManager(),"cdxkv");

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                if(!spinnerAccountName.getSelectedItem().toString().equalsIgnoreCase("Select account name"))
//                {
                    if(!edtAmount.getText().toString().equalsIgnoreCase(""))
                    {



                        int year=Integer.parseInt(npyear.getSelectedItem().toString());
                        int index=npmonth.getValue();

                        String month=arrmonth[index];

                        try{

                            if(budget!=null)
                            {

                                CommonData data=commonData_selected;

                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("year", year);
                                jsonObject.put("month", month);
                                jsonObject.put("accountname", data.getId());
                                jsonObject.put("amount", edtAmount.getText().toString());

                                new DatabaseHelper(AddBudgetActivity.this).updateData(budget.getId(),jsonObject.toString(),Utils.DBtables.TABLE_BUDGET);

                                Utils.playSimpleTone(AddBudgetActivity.this);

                                spinnerAccountName.setSelection(0);
                                edtAmount.setText("");

                                Toast.makeText(AddBudgetActivity.this,resources.getString(R.string.datasaved),Toast.LENGTH_SHORT).show();

                                onBackPressed();
                            }
                            else {


                                CommonData data=commonData_selected;



                                for (int i=0;i<arrmonth.length;i++) {

                                    String m=arrmonth[i];

                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("year", year);
                                    jsonObject.put("month", m);
                                    jsonObject.put("accountname", data.getId());
                                    jsonObject.put("amount", edtAmount.getText().toString());


                                    new DatabaseHelper(AddBudgetActivity.this).addData(Utils.DBtables.TABLE_BUDGET,jsonObject.toString());
                                    Utils.playSimpleTone(AddBudgetActivity.this);
                                    Toast.makeText(AddBudgetActivity.this,resources.getString(R.string.datasaved),Toast.LENGTH_SHORT).show();

                                }

                                spinnerAccountName.setSelection(0);
                                edtAmount.setText("");

                            }



                        }catch (Exception e)
                        {

                        }




                    }
                    else {

                        Toast.makeText(AddBudgetActivity.this,resources.getString(R.string.enteramount),Toast.LENGTH_SHORT).show();
                    }

//                }
//                else {
//
//                    Toast.makeText(AddBudgetActivity.this,"Select account name",Toast.LENGTH_SHORT).show();
//                }

            }
        });



    }

    public void addAccountSettings()
    {

        try {


            List<CommonData> commonData = new DatabaseHelper(AddBudgetActivity.this).getAccountSettingsData();

            if (commonData.size() > 0) {

                Collections.reverse(commonData);


                AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddBudgetActivity.this, commonData);
                spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);


                for (int j = 0; j < commonData.size(); j++) {


                    if (commonData.get(j).getId().equalsIgnoreCase(accountname)) {

                        JSONObject jcmn1 = new JSONObject(commonData.get(j).getData());
                        String acctype = jcmn1.getString("Accountname");
                        txtSpinner.setText(acctype);
                        commonData_selected=commonData.get(j);


                        spinnerAccountName.setSelection(j);
                        break;
                    }


                }

            }

        }catch (Exception e)
        {

        }


    }
}
