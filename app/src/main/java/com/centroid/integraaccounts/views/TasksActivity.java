package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.TaskDataAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.TaskData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TasksActivity extends AppCompatActivity {

    ImageView imgback,imgDatepick,imgEndDatepick;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    TextView txtprofile;

    TextView txtdatepick,txtenddatepick;

    Button btnSearch;

    String date = "", month_selected = "", yearselected = "",enddate="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtprofile=findViewById(R.id.txtprofile);
        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);

        imgEndDatepick=findViewById(R.id.imgEndDatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);


        String languagedata = LocaleHelper.getPersistedData(TasksActivity.this, "en");
        Context conte= LocaleHelper.setLocale(TasksActivity.this, languagedata);

        final Resources resources=conte.getResources();

        txtprofile.setText(resources.getString(R.string.task));



//        txtdatepick.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                showDatePicker(0);
//            }
//        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!date.equalsIgnoreCase(""))
                {

                    if(!enddate.equalsIgnoreCase(""))
                    {



                        getTaskData();
                    }
                    else {

                        Utils.showAlertWithSingle(TasksActivity.this,"Select end date",null);
                    }
                }
                else {

                    Utils.showAlertWithSingle(TasksActivity.this,"Select start date",null);
                }

            }
        });

        imgEndDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);
            }
        });
        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(TasksActivity.this,AddnewTaskActivity.class));

            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

      //  getTaskData();

        setDate();


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getTaskData();
    }


    public void showDatePicker(int code) {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(TasksActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m = i1 + 1;
                month_selected = m + "";
                yearselected = i + "";

                String month="";
                if(m/10<1)
                {
                    month="0"+m;
                }
                else {
                    month=""+m;
                }

                String day="";

                if(i2/10<1)
                {
                    day="0"+i2;
                }
                else {

                    day=i2+"";
                }


                if(code==0) {

                    date = day + "-" + month + "-" + i;

                    txtdatepick.setText(day + "-" + month + "-" + i);
                }
                else if(code==1) {
                    enddate = day + "-" + month + "-" + i;

                    txtenddatepick.setText(day + "-" + month + "-" + i);

                }

                //getTaskData();


            }
        }, year, month, dayOfMonth);

        datePickerDialog.show();
    }

    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        String m1="";

        if(m/10<1)
        {
            m1="0"+m;
        }
        else {
            m1=""+m;
        }

        String day="";

        if(dayOfMonth/10<1)
        {
            day="0"+dayOfMonth;
        }
        else {

            day=dayOfMonth+"";
        }

        date = day + "-" + m1 + "-" + year;

        txtdatepick.setText(day + "-" + m1 + "-" + year);

        enddate = day + "-" + m1 + "-" + year;

        txtenddatepick.setText(enddate);

        getTaskData();
    }

    public void getTaskData()
    {

        List<CommonData>taskData=new DatabaseHelper(TasksActivity.this).getData(Utils.DBtables.TABLE_TASK);

        Collections.reverse(taskData);

        List<CommonData>commonData_selected=new ArrayList<>();

        try {

//            Calendar calendar=Calendar.getInstance();
//            int y=calendar.get(Calendar.YEAR);
//            int m=calendar.get(Calendar.MONTH)+1;
//            int d=calendar.get(Calendar.DAY_OF_MONTH);
//
//            String date=d+"-"+m+"-"+y;



            for (CommonData cm:taskData) {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String status = jsonObject.getString("status");
                String date1 = jsonObject.getString("date");

                DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");

                Date cmdate=dateFormat.parse(date1);

                Date currentDate=dateFormat.parse(date);

                Date end_date=dateFormat.parse(enddate);

                if(end_date.after(currentDate)||end_date.equals(currentDate)) {
//                                if (cmdate.after(currentDate)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }
//                               else if (cmdate.equals(currentDate)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }
//
//                             else   if (cmdate.before(end_date)) {
//                                    commonData_selected.add(cm);
//                                }
//
//                             else   if (cmdate.equals(end_date)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }

                    if(cmdate.equals(currentDate)&&cmdate.before(end_date)) {

                        commonData_selected.add(cm);

                    }
                    else  if(cmdate.after(currentDate) && cmdate.equals(end_date))
                    {
                        commonData_selected.add(cm);
                    }
                    else if(cmdate.equals(currentDate) && cmdate.equals(end_date))
                    {

                        commonData_selected.add(cm);
                    }
                    else if (cmdate.after(currentDate) && cmdate.before(end_date)) {

                        commonData_selected.add(cm);
                    }




                }
                else {

                    Utils.showAlertWithSingle(TasksActivity.this,"Select  Date Properly",null);
                }

                if(commonData_selected.size()>0) {


                    Collections.sort(commonData_selected, new Comparator<CommonData>() {
                        @Override
                        public int compare(CommonData lhs, CommonData rhs) {
                            Date cmdate1 = null;
                            Date cmdate2 = null;

                            try {

                                JSONObject jsonObject1 = new JSONObject(lhs.getData());
                                JSONObject jsonObject2 = new JSONObject(rhs.getData());
                                String date1 = jsonObject1.getString("date");
                                String date2 = jsonObject2.getString("date");
                                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                cmdate1 = dateFormat.parse(date1);
                                cmdate2 = dateFormat.parse(date2);


                            } catch (Exception e) {

                            }

                            return cmdate1.compareTo(cmdate2);
                        }
                    });
                }







            }

            TaskDataAdapter taskDataAdapter = new TaskDataAdapter(TasksActivity.this, commonData_selected);
            recycler.setLayoutManager(new LinearLayoutManager(TasksActivity.this));
            recycler.setAdapter(taskDataAdapter);

        }catch (Exception e)
        {

        }

        //Log.e("Tasksize",taskData.size()+"");




    }
}
