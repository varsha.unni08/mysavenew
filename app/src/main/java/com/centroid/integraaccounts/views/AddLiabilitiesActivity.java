package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.adapter.LoanSpinnerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.receivers.DateTimeReceiver;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsInvestmentRequestcode;
import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsLiabilityRequestcode;

public class AddLiabilitiesActivity extends AppCompatActivity {

    TextView txtloan, txtdatepayment,txtdateMonthpayment,txtprofile,txtReminddate;

    Spinner spLoan, sploantype;
    EditText edtamount, edtemiAmount, edtNoEmi,edtOpeningBalance;

    ImageView imgDatepayment, imgback,imgDateMonthpayment,imgRemind;
    Button btnAdd;

    String dateofpayment = "",newaccount="",reminddate;

    FloatingActionButton fabadd;

    String liabilityid = "",taskid="";

    int dtofpayment=0;

    Resources resources;

    int index=0;

    RelativeLayout relremindDate,reldateMonth;

    CommonData cmdata;

    int noofMonths=0;

    List<String>taskids;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_liabilities);
        getSupportActionBar().hide();

        cmdata=(CommonData)getIntent().getSerializableExtra("CommonData");

        taskids=new ArrayList<>();
        txtloan = findViewById(R.id.txtloan);
        imgRemind=findViewById(R.id.imgRemind);
        relremindDate=findViewById(R.id.relremindDate);
        txtReminddate=findViewById(R.id.txtReminddate);

        reldateMonth=findViewById(R.id.reldateMonth);

        imgDatepayment = findViewById(R.id.imgDatepayment);
        edtNoEmi = findViewById(R.id.edtNoEmi);
        edtemiAmount = findViewById(R.id.edtemiAmount);
        edtOpeningBalance=findViewById(R.id.edtOpeningBalance);
        edtamount = findViewById(R.id.edtamount);
        sploantype = findViewById(R.id.sploantype);
        spLoan = findViewById(R.id.spLoan);
        txtdatepayment = findViewById(R.id.txtdatepayment);
        btnAdd = findViewById(R.id.btnAdd);
        imgback = findViewById(R.id.imgback);
        fabadd = findViewById(R.id.fabadd);
        imgDateMonthpayment=findViewById(R.id.imgDateMonthpayment);
        txtdateMonthpayment=findViewById(R.id.txtdateMonthpayment);
        txtprofile=findViewById(R.id.txtprofile);
      //  txtvisitlogin=findViewById(R.id.txtvisitlogin);


        String languagedata = LocaleHelper.getPersistedData(AddLiabilitiesActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddLiabilitiesActivity.this, languagedata);

        resources=context.getResources();
        txtprofile.setText(Utils.getCapsSentences(context,resources.getString(R.string.openingliabilities)));
      //  txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        edtOpeningBalance.setHint(resources.getString(R.string.openingbalance));

        edtamount.setHint(resources.getString(R.string.amount));
        edtemiAmount.setHint("EMI "+resources.getString(R.string.amount));

        txtdateMonthpayment.setText(resources.getString(R.string.dtofpayment));
        txtdatepayment.setText(resources.getString(R.string.selectclosingdate));
        btnAdd.setText(resources.getString(R.string.save));
        edtNoEmi.setHint(resources.getString(R.string.noofemi));
        txtReminddate.setText(Utils.getCapsSentences(context,resources.getString(R.string.reminddate)));


        String arr[]=resources.getStringArray(R.array.loantype);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item,
                        arr);
        spinnerArrayAdapter.setDropDownViewResource(R.layout
                .spinner_dropdown_item);
        sploantype.setAdapter(spinnerArrayAdapter);


        if(cmdata!=null)
        {

           // {"loantype":"EMI","amount":"33","emicount":"3","loan":"45","dateofpayment":"01-05-2021"}

            try{



                JSONObject jsonObject=new JSONObject(cmdata.getData());

                taskid=jsonObject.getString("task");
               // {"loantype":"EMI","amount":"300","emicount":"33","loan":"45","PaymentMonth":1,"dateofpayment":"01-11-2023"}

                if(jsonObject.getString("loantype").equalsIgnoreCase("EMI"))
                {

                    sploantype.setSelection(0);

                    edtemiAmount.setText(jsonObject.getString("amount"));
                    edtNoEmi.setText(jsonObject.getString("emicount"));
                    dateofpayment=jsonObject.getString("dateofpayment");
                    liabilityid=jsonObject.getString("loan");
                    dtofpayment=jsonObject.getInt("PaymentMonth");
                    txtdateMonthpayment.setText(dtofpayment+"");
                    txtdatepayment.setText(dateofpayment);


                    JSONArray jsonArray=new JSONArray(taskid);

                    for (int i=0;i<jsonArray.length();i++)
                    {

                        JSONObject jsonObject1=jsonArray.getJSONObject(i);

                        taskids.add(jsonObject1.getString("taskid"));

                    }




                }
                else {


                    sploantype.setSelection(1);

                    edtamount.setText(jsonObject.getString("amount"));
                   // edtNoEmi.setText(jsonObject.getString("emicount"));
                    dateofpayment=jsonObject.getString("dateofpayment");
                    liabilityid=jsonObject.getString("loan");
                    txtdatepayment.setText(dateofpayment);
                    reminddate=jsonObject.getString("reminddate");

                    txtReminddate.setText(reminddate);

                }







            }catch (Exception e)
            {

            }


        }




        edtNoEmi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                String number=charSequence.toString();

               if(dtofpayment!=0)
               {
                   calculateClosingDate();
               }



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                      Intent i= new Intent(AddLiabilitiesActivity.this, AccountsettingsActivity.class);
                i.putExtra(Utils.Requestcode.AccVoucherAll,ForAccountSettingsLiabilityRequestcode);
                startActivityForResult(i,ForAccountSettingsLiabilityRequestcode);

            }
        });

        txtReminddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showReminDatePickerDialog();

            }
        });

        imgRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showReminDatePickerDialog();
            }
        });


        sploantype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                index=i;
                if (i==0) {


                    edtNoEmi.setVisibility(View.VISIBLE);
                    edtemiAmount.setVisibility(View.VISIBLE);
                    edtamount.setVisibility(View.GONE);
                    relremindDate.setVisibility(View.GONE);
                    reldateMonth.setVisibility(View.VISIBLE);

                } else {


                    edtNoEmi.setVisibility(View.GONE);
                    edtemiAmount.setVisibility(View.GONE);
                    edtamount.setVisibility(View.VISIBLE);
                    relremindDate.setVisibility(View.VISIBLE);
                    reldateMonth.setVisibility(View.GONE);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        imgDateMonthpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDate();
            }
        });


        txtdateMonthpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDate();
            }
        });









        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtloan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(AddLiabilitiesActivity.this, LoanListActivity.class));


            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (String tid:taskids)
                {

                    new DatabaseHelper(AddLiabilitiesActivity.this).deleteData(tid,Utils.DBtables.TABLE_TASK);
                    Utils.playSimpleTone(AddLiabilitiesActivity.this);
                }

                if (index==1) {


                    if (!edtamount.getText().toString().equalsIgnoreCase("")) {

                        if (!dateofpayment.equalsIgnoreCase("")) {

                            if(!reminddate.equalsIgnoreCase(""))
                            {

                            try {

                                CommonData data = (CommonData) spLoan.getSelectedItem();


                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("loantype", sploantype.getSelectedItem().toString());
                                jsonObject.put("amount", edtamount.getText().toString());
                                jsonObject.put("loan", data.getId());
                                jsonObject.put("openingbalance",edtOpeningBalance.getText().toString());

                                jsonObject.put("dateofpayment", dateofpayment);
                                jsonObject.put("reminddate",reminddate);


                            long id=    addSingleTask();

                                jsonObject.put("task",id+"");

                                uploadData(jsonObject.toString());



                                onBackPressed();


                            } catch (Exception e) {


                            }

                            } else {


                                Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.reminddate), Toast.LENGTH_SHORT).show();
                            }


                        } else {


                            Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.selectpaymentdteverymonth), Toast.LENGTH_SHORT).show();
                        }


                    } else {


                        Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.amount), Toast.LENGTH_SHORT).show();
                    }


                } else {

                    if (!edtemiAmount.getText().toString().equalsIgnoreCase("")) {

                        if (!edtNoEmi.getText().toString().equalsIgnoreCase("")) {

                            if (!dateofpayment.equalsIgnoreCase("")) {


                                try {

                                    CommonData data = (CommonData) spLoan.getSelectedItem();

                                    String json = new Gson().toJson(data);


                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("loantype", sploantype.getSelectedItem().toString());
                                    jsonObject.put("amount", edtemiAmount.getText().toString());
                                    jsonObject.put("emicount", edtNoEmi.getText().toString());
                                    jsonObject.put("loan", data.getId());
                                    jsonObject.put("PaymentMonth",dtofpayment);
                                    jsonObject.put("openingbalance",edtOpeningBalance.getText().toString());
                                    jsonObject.put("dateofpayment", dateofpayment);

                                         addToTask();

                                    JSONArray jsonArray=new JSONArray();

                                    for (String taskid:taskids)
                                    {
                                        JSONObject jsonObject1=new JSONObject();
                                        jsonObject1.put("taskid",taskid+"");
                                        jsonArray.put(jsonObject1);
                                    }

                                    jsonObject.put("task",jsonArray.toString());

                                    uploadData(jsonObject.toString());

                                    Utils.playSimpleTone(AddLiabilitiesActivity.this);
                                    onBackPressed();


                                } catch (Exception e) {


                                }

                            } else {


                                Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.selectclosingdate), Toast.LENGTH_SHORT).show();
                            }

                        } else {


                            Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.noofemi), Toast.LENGTH_SHORT).show();
                        }

                    } else {


                        Toast.makeText(AddLiabilitiesActivity.this, resources.getString(R.string.enteremiamount), Toast.LENGTH_SHORT).show();
                    }

                }


            }
        });


        imgDatepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthYear();

            }
        });

        txtdatepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthYear();

            }
        });


        setLiabilityAdapter();

        spLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {

                    CommonData cm = (CommonData) spLoan.getSelectedItem();

                    JSONObject jsonObject = new JSONObject(cm.getData());
                    String Amount = jsonObject.getString("Amount");

                    String Type=jsonObject.getString("Type");
                    double amt =0;

                     amt = Double.parseDouble(Amount);

                    if(Type.equalsIgnoreCase("Credit"))
                    {

                        if(amt!=0) {
                            amt = amt * -1;
                        }
                    }

                    String id = cm.getId();

                    Calendar calendar = Calendar.getInstance();
                    int m = calendar.get(Calendar.MONTH) + 1;
                    int d = calendar.get(Calendar.DAY_OF_MONTH);
                    int y = calendar.get(Calendar.YEAR);
                    String enddate = d + "-" + m + "-" + y;

                  //  if(cmdata==null) {
                        double closingbalance = getClosingBalance(amt, id, enddate, "");
                    if(closingbalance<0) {
                        closingbalance = closingbalance * -1;
                    }
                    edtOpeningBalance.setText(closingbalance + "");
                     //   edtemiAmount.setText(closingbalance+"");
                  //  }
                }catch (Exception e)
                {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public long addSingleTask()
    {
        long id=0;
        CommonData data = (CommonData) spLoan.getSelectedItem();
        try {
            JSONObject jsonObject = new JSONObject(data.getData());


            String data_task = jsonObject.getString("Accountname");
            if(taskid.equalsIgnoreCase("")) {
                JSONObject js = new JSONObject();
                js.put("name", " " + data_task);
                js.put("date", dateofpayment);
                js.put("time", "");
                js.put("status", 0);
                id = new DatabaseHelper(AddLiabilitiesActivity.this).addData(Utils.DBtables.TABLE_TASK, js.toString());
//                Utils.playSimpleTone(AddLiabilitiesActivity.this);

            }
            else {

                JSONObject js = new JSONObject();
                js.put("name", " " + data_task);
                js.put("date", dateofpayment);
                js.put("time", "");
                js.put("status", 0);
                new DatabaseHelper(AddLiabilitiesActivity.this).updateData(taskid, js.toString(),Utils.DBtables.TABLE_TASK);
//                Utils.playSimpleTone(AddLiabilitiesActivity.this);
                id=Long.parseLong(taskid);


            }

            try {
                          Calendar c1 = Calendar.getInstance();

                          DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                           Date dtc=dateFormat.parse(dateofpayment);
                           c1.setTime(dtc);


                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                myIntent.putExtra("Task", data_task);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, c1.getTimeInMillis(), pendingIntent);

            }catch (Exception e)

            {

            }


        } catch (Exception e) {

        }

        return id;
    }








    public long addToTask()
    {
        long id=0;




            CommonData data = (CommonData) spLoan.getSelectedItem();
            try {
                JSONObject jsonObject = new JSONObject(data.getData());


                String data_task = jsonObject.getString("Accountname");
              //  if(taskids.size()==0) {

                    Calendar calendar = Calendar.getInstance();
                    int y = calendar.get(Calendar.YEAR);
                    int m = calendar.get(Calendar.MONTH)+1;
                    String strt_date = dtofpayment + "-" + m + "-" + y;

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date startdate = sdf.parse(strt_date);

                    calendar.setTime(startdate);

                   for (int i=0;i<noofMonths;i++) {






                           String datee=sdf.format(calendar.getTime());







                       JSONObject js = new JSONObject();
                       js.put("name", " " + data_task);
                       js.put("date", datee);
                       js.put("time", "");
                       js.put("status", 0);
                       id = new DatabaseHelper(AddLiabilitiesActivity.this).addData(Utils.DBtables.TABLE_TASK, js.toString());
//                       Utils.playSimpleTone(AddLiabilitiesActivity.this);

                       taskids.add(id+"");

                       calendar.add(Calendar.MONTH,1);
                       try {

//                           Calendar c1 = Calendar.getInstance();
//
////                           DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
////                           Date dtc=dateFormat.parse(rmdate.getDate());
//                           c1.setTime(datee);


//                           Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
//                           myIntent.putExtra("Task", data_task);
//                           PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);
//
//                           AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//                           alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

                       }catch (Exception e)

                       {

                       }









                   }

//                }
//                else {


//
//                    JSONObject js = new JSONObject();
//                    js.put("name", " " + data_task);
//                    js.put("date", dateofpayment);
//                    js.put("time", "");
//                    js.put("status", 0);
//                  new DatabaseHelper(AddLiabilitiesActivity.this).updateData(taskid, js.toString(),Utils.DBtables.TABLE_TASK);
//
//                  id=Long.parseLong(taskid);





              //  }



            } catch (Exception e) {

            }


        return id;
        }

    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(AddLiabilitiesActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }

    public void showReminDatePickerDialog()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddLiabilitiesActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                reminddate=i2+"-"+m+"-"+i;

                txtReminddate.setText(i2+"-"+m+"-"+i);






            }
        },year,month,dayOfMonth);

        datePickerDialog.show();

    }


    public void calculateClosingDate()
    {
        try {
            noofMonths=0;

            int numberofEmi = Integer.parseInt(edtNoEmi.getText().toString());

            Calendar calendar = Calendar.getInstance();
            int y = calendar.get(Calendar.YEAR);
            int m = calendar.get(Calendar.MONTH);
            String strt_date = dtofpayment + "-" + m + "-" + y;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date startdate = sdf.parse(strt_date);

            calendar.setTime(startdate);

            for (int i=0;i<numberofEmi;i++)
            {
                calendar.add(Calendar.MONTH,1);
                noofMonths++;

            }

            String closingdate=sdf.format(calendar.getTime());

            txtdatepayment.setText(closingdate);
            dateofpayment=closingdate;








        }catch (Exception e)
        {

        }

    }















    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== ForAccountSettingsLiabilityRequestcode&&resultCode==RESULT_OK&&data!=null)
        {

            newaccount=data.getStringExtra("AccountAdded");


            setLiabilityAdapter();



        }
    }

    public void uploadData(String data) {

        if(cmdata==null) {

            new DatabaseHelper(AddLiabilitiesActivity.this).addData(Utils.DBtables.TABLE_LIABILITY, data);

            Utils.playSimpleTone(AddLiabilitiesActivity.this);

        }
        else {

            new DatabaseHelper(AddLiabilitiesActivity.this).updateData(cmdata.getId(),data,Utils.DBtables.TABLE_LIABILITY);

            Utils.playSimpleTone(AddLiabilitiesActivity.this);
        }
        edtamount.setText("");
        edtemiAmount.setText("");
        edtNoEmi.setText("");
        txtdatepayment.setText(resources.getString(R.string.selectclosingdate));

      //  addToReminder();


    }



    public void showDate()
    {

               Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels / 2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(AddLiabilitiesActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set = dialog.findViewById(R.id.date_time_set);


        final NumberPicker npmonth = dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(1);
        npmonth.setMaxValue(31);


        final NumberPicker npyear = dialog.findViewById(R.id.npyear);


        npyear.setMinValue(0);
        npyear.setMaxValue(11);

        npyear.setVisibility(View.GONE);


        // npyear.setDisplayedValues(arrmonth);

        dialog.getWindow().setLayout(width, height);


        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                dtofpayment = npmonth.getValue();

                txtdateMonthpayment.setText(npmonth.getValue() + "");

                if(!edtNoEmi.getText().toString().equalsIgnoreCase(""))
                {

                    calculateClosingDate();
                }


            }
        });


        dialog.show();

    }















    public void showMonthYear() {




        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddLiabilitiesActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                dateofpayment=i2+"-"+m+"-"+i;

                txtdatepayment.setText(i2+"-"+m+"-"+i);






            }
        },year,month,dayOfMonth);

        datePickerDialog.show();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        setLiabilityAdapter();
    }

    public void setLiabilityAdapter() {


        List<CommonData> commonData_filtered = new ArrayList<>();

        List<CommonData> commonData = new DatabaseHelper(AddLiabilitiesActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        for (CommonData cm : commonData) {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype = jsonObject.getString("Accounttype");

                if (acctype.equalsIgnoreCase("Liability account")) {


                    commonData_filtered.add(cm);
                }


                //


            } catch (Exception e) {

            }

        }


        BankSpinnerAdapter bankSpinnerAdapter = new BankSpinnerAdapter(AddLiabilitiesActivity.this, commonData_filtered);
        spLoan.setAdapter(bankSpinnerAdapter);


        for (int i = 0; i < commonData_filtered.size(); i++) {

            CommonData cm = commonData_filtered.get(i);

            if (cm.getId().equalsIgnoreCase(liabilityid)) {

                spLoan.setSelection(i);
                break;
            }


        }



        if(!newaccount.equalsIgnoreCase(""))
        {
            for (int i = 0; i < commonData_filtered.size(); i++) {

                CommonData cm = commonData_filtered.get(i);

                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accountname");

                    if (acctype.equalsIgnoreCase(newaccount)) {

                        spLoan.setSelection(i);
                        newaccount="";
                        break;
                    }

                }catch (Exception e)
                {

                }


            }




        }




    }



    public void addToReminder()
    {

        try {



            String subject = "";


            String id = ((CommonData) spLoan.getSelectedItem()).getId();


            List<CommonData> cms = new DatabaseHelper(AddLiabilitiesActivity.this).getDataByID(id, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if (cms.size() > 0) {
                //subject=


                JSONObject jsonObject = new JSONObject(cms.get(0).getData());


                subject = jsonObject.getString("Accountname");

            }

















            String cldate[] = dateofpayment.split("-");

            int dateofEveryMonth = dtofpayment;


            Calendar calendar = Calendar.getInstance();
            int currentyear = calendar.get(Calendar.YEAR);
            int currentmonth = calendar.get(Calendar.MONTH);
            int currentday = calendar.get(Calendar.DAY_OF_MONTH);


            int closeyear = Integer.parseInt(cldate[2]);
            int closemonth = Integer.parseInt(cldate[1]);

            int m = currentmonth;


            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);


            String time = hour + ":" + minute;

            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
            Date dat = null;
            try {
                dat = fmt.parse(time);
            } catch (ParseException e) {

                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");

            String timeselected = fmtOut.format(dat);


            JSONObject js = new JSONObject();
            js.put("name", subject);
            js.put("date", currentday + "-" + currentmonth + "-" + currentyear);
            js.put("time", timeselected);
            js.put("status", 0);


            new DatabaseHelper(AddLiabilitiesActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());

            Utils.playSimpleTone(AddLiabilitiesActivity.this);


            while (currentyear <= closeyear) {


                for (int i = m; i < 12; i++) {




                    Calendar c = Calendar.getInstance();

                    Log.e("HOUR OF DAY",calendar.get(Calendar.HOUR)+"");

                    c.set(Calendar.MONTH, i);
                    c.set(Calendar.YEAR, currentyear);
                    c.set(Calendar.DAY_OF_MONTH, dateofEveryMonth);

                    c.set(Calendar.HOUR, hour);
                    c.set(Calendar.MINUTE, minute);
                    c.set(Calendar.SECOND, 0);



                    c.set(Calendar.AM_PM, Calendar.AM);


                    Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                    myIntent.putExtra("Task",subject);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent,0);

                    AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC, c.getTimeInMillis(), pendingIntent);











                    if (i == 11) {
                        m = 0;
                        currentyear++;
                        break;
                    } else if (currentyear == closeyear && i == closemonth) {
                        currentyear++;
                        break;

                    }
                }


            }


        }catch (Exception e)
        {

        }

    }








}
