package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
//import com.cashfree.pg.core.api.callback.CFCheckoutResponseCallback;
//import com.cashfree.pg.core.api.utils.CFErrorResponse;
import com.centroid.integraaccounts.BuildConfig;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.HomeAdapter;
import com.centroid.integraaccounts.com.TokenResult.TransactionStatus;
import com.centroid.integraaccounts.data.CheckExpiry;
import com.centroid.integraaccounts.data.CheckTrialPeriod;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.fragments.HomeFragment;
import com.centroid.integraaccounts.fragments.MoreFragment;
import com.centroid.integraaccounts.fragments.NetworkFragment;
import com.centroid.integraaccounts.fragments.ReportFragment;
import com.centroid.integraaccounts.fragments.SettingsFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.ExpirydateCheckListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.centroid.integraaccounts.Constants.Utils.PaytmCredentials.ActivityRequestCode;


public class MainActivity extends AppCompatActivity  {

    //getSalesData.php

    TabLayout tabLayout;
    ViewPager viewpager;
    ImageView imgsettings,imgaccountsettings,imgmenu,imgprofile,imgusers,imgnotifications;

    TextView txtdashboard;

    LinearLayout layout_addfragment;

    HomeFragment homeFragment = null;
    MoreFragment moreFragment = null;
    ReportFragment reportFragment = null;
    NetworkFragment networkFragment = null;
    String TAG="xvf";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        Utils.scheduleJob(MainActivity.this);

        imgmenu=findViewById(R.id.imgmenu);
        tabLayout = findViewById(R.id.tabLayout);
        viewpager = findViewById(R.id.viewpager);
        imgsettings = findViewById(R.id.imgsettings);
        imgaccountsettings=findViewById(R.id.imgaccountsettings);
        txtdashboard=findViewById(R.id.txtdashboard);
        imgprofile=findViewById(R.id.imgprofile);
        layout_addfragment=findViewById(R.id.layout_addfragment);
//        imgusers=findViewById(R.id.imgusers);
        imgnotifications=findViewById(R.id.imgnotifications);

//        imgusers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                startActivity(new Intent(
//                        MainActivity.this,ProfileActivity.class
//                ));
//            }
//        });

        imgnotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(
                        MainActivity.this,NotificationsActivity.class
                ));
            }
        });

        tabLayout.addTab(tabLayout.newTab());
       // tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());




        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");


       String logined= new PreferenceHelper(MainActivity.this).getData(Utils.firstloginedtime);

       if(!logined.equalsIgnoreCase(""))
       {
           String bkptime= new PreferenceHelper(MainActivity.this).getData(Utils.databackuptime);

           if(!bkptime.equalsIgnoreCase(""))
           {
               try {
                   Date curr_date = new Date();

                   Date bkpdate = sdf.parse(bkptime);

                   int num=getDaysDifference(curr_date,bkpdate);

                   if(num>=2)
                   {
                       Utils.showAlertWithSingle(MainActivity.this, "Do you want to backup your data now ?", new DialogEventListener() {
                           @Override
                           public void onPositiveButtonClicked() {

                               Toast.makeText(MainActivity.this,"Choose data backup option",Toast.LENGTH_SHORT).show();
                               startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                           }

                           @Override
                           public void onNegativeButtonClicked() {

                           }
                       });
                   }


               }
               catch (Exception e)
               {

               }






           }
           else{

               Utils.showAlertWithSingle(MainActivity.this, "Do you want to backup your data now ?", new DialogEventListener() {
                   @Override
                   public void onPositiveButtonClicked() {
                       Toast.makeText(MainActivity.this,"Choose data backup option",Toast.LENGTH_SHORT).show();
                       startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                   }

                   @Override
                   public void onNegativeButtonClicked() {

                   }
               });


           }






       }




        addExpenseList();
       getVisiCardFirst();
       getEmergencyData();

          getProfileData();
        updateDeviceOSID();

        getSalesData();
        checkBackupDate();
        //getSalesData();

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.POST_NOTIFICATIONS)!= PackageManager.PERMISSION_GRANTED)
        {


            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.POST_NOTIFICATIONS},111);
        }
        else{


        }



        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent5=new Intent(MainActivity.this,ProfileActivity.class);
                startActivity(intent5);
            }
        });


        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int width = (int)MainActivity.this.getResources().getDimension(R.dimen.dimen_270dp);
                LinearLayout.LayoutParams layoutParams= new LinearLayout.LayoutParams( width,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_menupopup);
                ImageView imgJournal=dialog.findViewById(R.id.imgJournal);
                ImageView imgbankvoucher=dialog.findViewById(R.id.imgbankvoucher);
                ImageView imgaccsetting=dialog.findViewById(R.id.imgaccsetting);
                ImageView imgvisitingcard=dialog.findViewById(R.id.imgvisitingcard);
                ImageView imgpasswordmanager=dialog.findViewById(R.id.imgpasswordmanager);
                ImageView imgdocumentmanager=dialog.findViewById(R.id.imgdocumentmanager);
                ImageView imgTargetacheive=dialog.findViewById(R.id.imgTargetacheive);

                TextView journalvoucher=dialog.findViewById(R.id.journalvoucher);
                TextView bankvoucher=dialog.findViewById(R.id.bankvoucher);
                TextView visitcard=dialog.findViewById(R.id.visitcard);
                TextView liabilitysetup=dialog.findViewById(R.id.liabilitysetup);
                TextView insurancesetup=dialog.findViewById(R.id.insurancesetup);
                TextView accountsettings=dialog.findViewById(R.id.accountsettings);
                TextView savepassword=dialog.findViewById(R.id.savepassword);
                TextView txtTarget=dialog.findViewById(R.id.txtTarget);

                TextView docmanager=dialog.findViewById(R.id.docmanager);

                String languagedata = LocaleHelper.getPersistedData(MainActivity.this, "en");
                Context context= LocaleHelper.setLocale(MainActivity.this, languagedata);

                Resources   resources=context.getResources();

                journalvoucher.setText(resources.getString(R.string.journalvoucher));
                bankvoucher.setText(resources.getString(R.string.bankvoucher));
                visitcard.setText(resources.getString(R.string.visitingcard));
//                assetsetup.setText(resources.getString(R.string.assetsetup));
//                liabilitysetup.setText(resources.getString(R.string.liabilitysetup));
//                insurancesetup.setText(resources.getString(R.string.insurancesetup));
                accountsettings.setText(resources.getString(R.string.accountsettings));
                savepassword.setText(resources.getString(R.string.passwordmanager));
                docmanager.setText(resources.getString(R.string.documentmanager));




//              dialog.getWindow().setLayout(width,height);
                txtTarget.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,TargetActivity.class);
                        startActivity(intent5);
                    }
                });


              imgTargetacheive.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      dialog.dismiss();
                      Intent  intent5=new Intent(MainActivity.this,TargetActivity.class);
                      startActivity(intent5);
                  }
              });





                imgpasswordmanager.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,SavedPasswordActivity.class);
                        startActivity(intent5);
                    }
                });

                docmanager.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,DocumentManagerActivity.class);
                        startActivity(intent5);
                    }
                });

                imgdocumentmanager.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,DocumentManagerActivity.class);
                        startActivity(intent5);
                    }
                });


                savepassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,SavedPasswordActivity.class);
                        startActivity(intent5);

                    }
                });

                accountsettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,AccountSettingsListActivity.class);
                        startActivity(intent5);
                    }
                });
                imgaccsetting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,AccountSettingsListActivity.class);
                        startActivity(intent5);
                    }
                });

                visitcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();


                        Intent  intent5=new Intent(MainActivity.this,VisitCardHistoryActivity.class);
                        startActivity(intent5);



                    }
                });

                imgvisitingcard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();


                        Intent  intent5=new Intent(MainActivity.this,VisitCardHistoryActivity.class);
                        startActivity(intent5);
                    }
                });





                imgJournal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,JournalvoucherListActivity.class);
                        startActivity(intent5);
                    }
                });



                journalvoucher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,JournalvoucherListActivity.class);
                        startActivity(intent5);
                    }
                });

                bankvoucher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,BankVoucherActivity.class);
                        startActivity(intent5);

                    }
                });

                imgbankvoucher.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent  intent5=new Intent(MainActivity.this,BankVoucherActivity.class);
                        startActivity(intent5);
                    }
                });



                dialog.getWindow().setLayout(layoutParams.width,layoutParams.height);
                dialog.show();

                dialog.getWindow().setGravity(Gravity.TOP|Gravity.LEFT);



            }
        });



        imgsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, SettingsActivity.class));

            }
        });

        imgaccountsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(MainActivity.this, NotificationsActivity.class));
            }
        });


        for (int i = 0; i < 3; i++) {

            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.layout_customtab, null);

            ImageView img = view.findViewById(R.id.img);
            TextView textView2 = view.findViewById(R.id.textView2);


            if (i == 0) {

                img.setImageResource(R.drawable.ic_home_);
                img.setColorFilter(Color.parseColor("#ffffff"));
                textView2.setTextColor(Color.WHITE);
                textView2.setText("Home");

            }

//            else if (i == 1) {
//                img.setImageResource(R.drawable.ic_network);
//                img.setColorFilter(Color.parseColor("#e8e8e8"));
//                textView2.setTextColor(Color.BLACK);
//                textView2.setText("Network");
//
//            }

            else if (i == 1) {
                img.setImageResource(R.drawable.ic_assignment);
                img.setColorFilter(Color.parseColor("#e8e8e8"));
                textView2.setTextColor(Color.BLACK);
                textView2.setText("Report");

            } else if (i == 2) {

                img.setImageResource(R.drawable.ic_more_horiz_black_);
                img.setColorFilter(Color.parseColor("#e8e8e8"));
                textView2.setTextColor(Color.BLACK);
                textView2.setText("More");
            }

            tabLayout.getTabAt(i).setCustomView(view);
        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());

                View view = tab.getCustomView();
                ImageView img = view.findViewById(R.id.img);
                TextView textView2 = view.findViewById(R.id.textView2);


                img.setColorFilter(Color.parseColor("#ffffff"));
                textView2.setTextColor(Color.WHITE);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                View view = tab.getCustomView();
                ImageView img = view.findViewById(R.id.img);
                TextView textView2 = view.findViewById(R.id.textView2);


                img.setColorFilter(Color.parseColor("#e8e8e8"));
                textView2.setTextColor(Color.BLACK);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        List<Fragment> fr = new ArrayList<>();


        homeFragment = new HomeFragment();
        fr.add(homeFragment);


//        networkFragment = new NetworkFragment();
//        fr.add(networkFragment);


        reportFragment = new ReportFragment();
        fr.add(reportFragment);



        moreFragment = new MoreFragment();
        fr.add(moreFragment);


        viewpager.setAdapter(new HomeAdapter(getSupportFragmentManager(), fr));
        viewpager.setOffscreenPageLimit(fr.size());
        getFireBaseToken();
        updateUI();
    }





    public static void setForceShowIcon(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


//    @Override
//    public void onPaymentVerify(String s) {
//        Utils.showAlertWithSingle(MainActivity.this, "Payment Success", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//    }
//
//    @Override
//    public void onPaymentFailure(CFErrorResponse cfErrorResponse, String s) {
//        Utils.showAlertWithSingle(MainActivity.this, "Payment failed", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//    }

    public  int getDaysDifference(Date fromDate, Date toDate)
    {
        if(fromDate==null||toDate==null)
            return 0;

        return (int)( (toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
    }

//    @Override
//    public void onPaymentSuccess(String s) {
//
//     //   passPurchaseDataToServer(s);
//
//        Utils.showAlertWithSingle(MainActivity.this, "Payment Success on MainActivity", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//
//    }
//
//    @Override
//    public void onPaymentError(int i, String s) {
//
//
//
//    }


    public void getSalesData()
    {
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Calendar c=Calendar.getInstance();
        int day=c.get(Calendar.DAY_OF_MONTH);
        int month=c.get(Calendar.MONTH);
        int year=c.get(Calendar.YEAR);

        int m=month+1;
        String mnth=m+"";

        if(m>10)
        {
            mnth=m+"";
        }
        else {

            mnth="0"+m;
        }



        String currentdate=day+"-"+mnth+"-"+year;

        // Toast.makeText(MainActivity.this,currentdate,Toast.LENGTH_SHORT).show();


        List<CommonData>commonData_doc=new DatabaseHelper(MainActivity.this).getData(Utils.DBtables.TABLE_RENEWALMSG);
        boolean a=false;

        if(commonData_doc.size()>0)
        {

            for(CommonData cd: commonData_doc)
            {
                if(currentdate.equalsIgnoreCase(cd.getData()))
                {

                    a=true;
                    break;
                }

            }


        }

        if(!a) {


            new PreferenceHelper(MainActivity.this).putBooleanData(Utils.Needversionupdate, false);

            Map<String, String> params = new HashMap<>();
//        params.put("cash_transaction_id",trid);
            params.put("timestamp", Utils.getTimestamp());

            new RequestHandler(MainActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    //  progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                    try {

                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.getInt("status") == 1) {

                            // {"status":1,"message":"User exists","data":"195"}

                            String nodata = jsonObject.getString("data");

                            Log.e(TAG, "onSuccess: " + nodata);

                            if (!nodata.equalsIgnoreCase("")) {

                                int numberdata = Integer.parseInt(nodata);

                                if (numberdata == 0) {
                                    new PreferenceHelper(MainActivity.this).putBooleanData(Utils.Needversionupdate, true);
//                                    Utils.showUpdateAlert(MainActivity.this, "Your free update period is expired. Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//                                            Intent newIntente = new Intent(MainActivity.this, AppRenewalActivity.class);
//
//                                            startActivity(newIntente);
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
////                                        new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");
////                                        Intent newIntente = new Intent(MainActivity.this, LoginActivity.class);
////
////                                        startActivity(newIntente);
////
////                                        finish();
//
//
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//
//
//
//                                        }
//                                    });

                                } else if (-15 <= numberdata && numberdata < 0) {
                                    int a = Math.abs(numberdata);

//
//                                    Utils.showUpdateAlert(MainActivity.this, "You have only " + a + " days  to expire the application.Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                        }
//                                    });
//

                                } else if (numberdata > 0) {

                                    new PreferenceHelper(MainActivity.this).putBooleanData(Utils.Needversionupdate, true);
//                                    Utils.showUpdateAlert(MainActivity.this, "Your free update period is expired. Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                            Intent newIntente = new Intent(MainActivity.this, AppRenewalActivity.class);
//
//                                            startActivity(newIntente);
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
////                                        new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");
////                                        Intent newIntente = new Intent(MainActivity.this, LoginActivity.class);
////
////                                        startActivity(newIntente);
////
////                                        finish();
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//
//                                        }
//                                    });
//


                                }


                            }


                        } else {

                            //  Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(MainActivity.this, "Failed", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {
                                    Calendar c=Calendar.getInstance();
                                    int day=c.get(Calendar.DAY_OF_MONTH);
                                    int month=c.get(Calendar.MONTH);
                                    int year=c.get(Calendar.YEAR);
                                    int m=month+1;
                                    String mnth=m+"";
                                    if(m>10)
                                    {
                                        mnth=m+"";
                                    }
                                    else {

                                        mnth="0"+m;
                                    }
                                    String currentdate=day+"-"+mnth+"-"+year;
                                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);

                                }

                                @Override
                                public void onNegativeButtonClicked() {
                                    Calendar c=Calendar.getInstance();
                                    int day=c.get(Calendar.DAY_OF_MONTH);
                                    int month=c.get(Calendar.MONTH);
                                    int year=c.get(Calendar.YEAR);
                                    int m=month+1;
                                    String mnth=m+"";
                                    if(m>10)
                                    {
                                        mnth=m+"";
                                    }
                                    else {

                                        mnth="0"+m;
                                    }
                                    String currentdate=day+"-"+mnth+"-"+year;
                                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);

                                }
                            });


                        }


                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(String err) {
                    //  progressFragment.dismiss();

                    //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();


                    Utils.showAlertWithSingle(MainActivity.this, err, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }
            }, Utils.WebServiceMethodes.getSalesData + "?timstap=" + Utils.getTimestamp(), Request.Method.GET).submitRequest();


        }
    }





    public void passPurchaseDataToServer(String trid)
    {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("cash_transaction_id",trid);
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {
//                            if(moreAdapter!=null)
//                            {
//
//                                moreAdapter.showReferDialog();
//                            }
                        JSONObject jsonObject_data=jsonObject.getJSONObject("data");
                        String billno=jsonObject_data.getString("bill_no");
                        String billprefix=jsonObject_data.getString("billno_prefix");

                       // Utils.showAlertWithSingle(MainActivity.this,"Pa");

//                        Intent intent=new Intent(MainActivity.this, InvoiceActivity.class);
//                        intent.putExtra("settingsdata",settingsData1);
//                        intent.putExtra("bill",billprefix+" "+billno);
//                        intent.putExtra("currency",currency);
//                        intent.putExtra("actualamount",rs);
//                        intent.putExtra("countryid",countryid);
//                        intent.putExtra("stateid",stateid);
//                        intent.putExtra("email",email);
//                        intent.putExtra("name",name);
//                        intent.putExtra("tid",trid);
//                        getActivity().startActivity(intent);


                    }
                    else {

                        //  Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(MainActivity.this, "Uploading transaction failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });



                    }



                }catch (Exception e)
                {

                }




            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();


                Utils.showAlertWithSingle(MainActivity.this, err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });




            }
        },Utils.WebServiceMethodes.addSalesInfo, Request.Method.POST).submitRequest();








    }


    public void addVersionAndDeviceType()
    {

        try {

            String version = BuildConfig.VERSION_NAME;


        Map<String,String> params=new HashMap<>();
        params.put("version",version);
        params.put("platform","android");
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {



                if(data!=null)
                {
                    //getFeedBacks();

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {
                           // new PreferenceHelper(MainActivity.this).putData(Utils.userLoginFirst,"Updated");

                            //  Toast.makeText(MainActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();


                        }
                        else {

                            // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateVersionPlatform+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public void checkAndUpdateActivationUser()
    {

        if(new PreferenceHelper(MainActivity.this).getData(Utils.userLoginFirst).equalsIgnoreCase(""))
        {




            Map<String,String> params=new HashMap<>();
            //params.put("message",message);
            params.put("timestamp",Utils.getTimestamp());


            new RequestHandler(MainActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {



                    if(data!=null)
                    {
                        //getFeedBacks();

                        try{

                            JSONObject jsonObject=new JSONObject(data);

                            if(jsonObject.getInt("status")==1)
                            {
                                new PreferenceHelper(MainActivity.this).putData(Utils.userLoginFirst,"Updated");

                              //  Toast.makeText(MainActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();


                            }
                            else {

                                // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                            }



                        }catch (Exception e)
                        {

                        }




                    }

                }

                @Override
                public void onFailure(String err) {

                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                }
            },Utils.WebServiceMethodes.updateActivationdate+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



        }



    }




    public void getProfileData()
    {


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
       // params.put("device_id",token);

        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try{


                    Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);



                    if(profiledata.getStatus()==1)
                    {


                        if(profiledata.getData()!=null)
                        {

                            if(profiledata.getData().getActivationDate()!=null) {

                                if (TextUtils.isEmpty(profiledata.getData().getActivationDate().toString())) {

                                    checkAndUpdateActivationUser();


                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    Date date=new Date();
                                    String dt= sdf.format(date);

                                    new PreferenceHelper(MainActivity.this).putData(Utils.firstloginedtime,dt);



                                }

                            }
                            else
                            {
                                checkAndUpdateActivationUser();

                            }





                            checkSalesInfo(profiledata.getData().getMobile());


                            if(!TextUtils.isEmpty(profiledata.getData().getProfileImage()))
                            {
                                Glide.with(MainActivity.this).load(Utils.imgbaseurl+profiledata.getData().getProfileImage()).apply(RequestOptions.circleCropTransform()).into(imgprofile);

                            }

                        }





                    }
                    else {

                       // Toast.makeText(MainActivity.this," failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(MainActivity.this," failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

              //  Toast.makeText(MainActivity.this,err,Toast.LENGTH_SHORT).show();

                Utils.showAlertWithSingle(MainActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });


            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();



//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");



//
//
//        Call<Profiledata> jsonObjectCall=client.getUserProfile(new PreferenceHelper(MainActivity.this).getData(Utils.userkey)
//
//        );
//        jsonObjectCall.enqueue(new Callback<Profiledata>() {
//            @Override
//            public void onResponse(Call<Profiledata> call, Response<Profiledata> response) {
//
//             //   progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//
//                            if(response.body().getData()!=null)
//                            {
//
//
//
//
//                                if(!TextUtils.isEmpty(response.body().getData().getProfileImage()))
//                                {
//                                    Glide.with(MainActivity.this).load(Utils.imgbaseurl+response.body().getData().getProfileImage()).apply(RequestOptions.circleCropTransform()).into(imgprofile);
//
//                                }
//
//                            }
//
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(MainActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Profiledata> call, Throwable t) {
//
//              //  progressFragment.dismiss();
//            }
//        });
    }


    private void getFireBaseToken() {

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {

                if(task!=null) {
                    String token = task.getResult();

                    updateDeviceID(token);
                }




            }
        });
    }


    private void updateDeviceID(String token) {

        Map<String,String> params=new HashMap<>();
        params.put("device_id",token);
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(LoginActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateDeviceId, Request.Method.POST).submitRequest();


    }


    private void updateDeviceOSID() {

        String uniquePseudoID = "35" +
                Build.BOARD.length() % 10 +
                Build.BRAND.length() % 10 +
                Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 +
                Build.HOST.length() % 10 +
                Build.ID.length() % 10 +
                Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 +
                Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 +
                Build.TYPE.length() % 10 +
                Build.USER.length() % 10;
        String serial = Build.getRadioVersion();
      String  uuid = new UUID(uniquePseudoID.hashCode(), serial.hashCode()).toString();




        Map<String,String> params=new HashMap<>();
        params.put("device_id",uuid);
        params.put("timestamp",Utils.getTimestamp());
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();

                //Toast.makeText(LoginActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateOSDeviceid, Request.Method.POST).submitRequest();


    }


    @Override
    protected void onResume() {
        super.onResume();
        getProfileData();

        addVersionAndDeviceType();
//        if(networkFragment!=null)
//        {
//            networkFragment.getProfileData(0);
//        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateUI();
        //getSalesData();
     if (homeFragment != null) {
          homeFragment.refresh();



        }

     if(moreFragment!=null)
     {
         moreFragment.updateLanguage();
     }

     if(reportFragment!=null)
     {
         reportFragment.updateData();
     }
     if(networkFragment!=null)
     {
         networkFragment.getProfileData(0);
     }

     getProfileData();

    }

    private void checkBackupDate()
    {

        Calendar c=Calendar.getInstance();
        int day=c.get(Calendar.DAY_OF_MONTH);
        int month=c.get(Calendar.MONTH);
        int year=c.get(Calendar.YEAR);

        int m=month+1;
        String mnth=m+"";

        if(m>10)
        {
            mnth=m+"";
        }
        else {

            mnth="0"+m;
        }



        String currentdate=day+"-"+mnth+"-"+year;

       // Toast.makeText(MainActivity.this,currentdate,Toast.LENGTH_SHORT).show();


        List<CommonData>commonData_doc=new DatabaseHelper(MainActivity.this).getData(Utils.DBtables.TABLE_BACKUP);
        boolean a=false;

        if(commonData_doc.size()>0)
        {

            for(CommonData cd: commonData_doc)
            {
                if(currentdate.equalsIgnoreCase(cd.getData()))
                {

                   a=true;
                   break;
                }

            }


        }

        if(!a)
        {

            if(!Utils.checkDBTablempty(MainActivity.this)) {

                Utils.showUpdateAlert(MainActivity.this, "Dear User \n\n\n\n  Kind attention please:-  SAVE App is successfully installed and a database is created on this device.  Please take data backup at regular intervals and keep it in your google drive.", new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        Calendar c = Calendar.getInstance();
                        int day = c.get(Calendar.DAY_OF_MONTH);
                        int month = c.get(Calendar.MONTH);
                        int year = c.get(Calendar.YEAR);
                        int m = month + 1;
                        String mnth = m + "";
                        if (m > 10) {
                            mnth = m + "";
                        } else {

                            mnth = "0" + m;
                        }
                        String currentdate = day + "-" + mnth + "-" + year;
                        new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_BACKUP, currentdate);
                        Intent newIntente = new Intent(MainActivity.this, SettingsActivity.class);

                        startActivity(newIntente);
                    }

                    @Override
                    public void onNegativeButtonClicked() {

                        Calendar c = Calendar.getInstance();
                        int day = c.get(Calendar.DAY_OF_MONTH);
                        int month = c.get(Calendar.MONTH);
                        int year = c.get(Calendar.YEAR);
                        int m = month + 1;
                        String mnth = m + "";
                        if (m > 10) {
                            mnth = m + "";
                        } else {

                            mnth = "0" + m;
                        }
                        String currentdate = day + "-" + mnth + "-" + year;
                        new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_BACKUP, currentdate);
//                    new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");
//                    Intent newIntente = new Intent(MainActivity.this, LoginActivity.class);
//
//                    startActivity(newIntente);
//
//                    finish();
                    }
                });

            }
        }

    }


    public void updateUI()
    {
        String languagedata = LocaleHelper.getPersistedData(MainActivity.this, "en");
        Context context= LocaleHelper.setLocale(MainActivity.this, languagedata);

        Resources resources=context.getResources();
        txtdashboard.setText("My Personal App");


    //    txtdashboard.setText(resources.getString(R.string.dashboard));

        Animation marquee = AnimationUtils.loadAnimation(this, R.anim.marqueeanim);
      //  txtdashboard.startAnimation(marquee);

        for (int i = 0; i < 3; i++) {

            View view =tabLayout.getTabAt(i).getCustomView();

            ImageView img = view.findViewById(R.id.img);
            TextView textView2 = view.findViewById(R.id.textView2);


            if (i == 0) {

                img.setImageResource(R.drawable.ic_home_);
                img.setColorFilter(Color.parseColor("#ffffff"));
                textView2.setTextColor(Color.WHITE);
                textView2.setText(resources.getString(R.string.home));

            }

//            else if (i == 1) {
//                img.setImageResource(R.drawable.ic_network);
//                img.setColorFilter(Color.parseColor("#e8e8e8"));
//                textView2.setTextColor(Color.BLACK);
//                textView2.setText(resources.getString(R.string.network));
//
//            }

            else if (i == 1) {
                img.setImageResource(R.drawable.ic_assignment);
                img.setColorFilter(Color.parseColor("#e8e8e8"));
                textView2.setTextColor(Color.BLACK);
                textView2.setText(resources.getString(R.string.report));

            } else if (i == 2) {

                img.setImageResource(R.drawable.ic_more_horiz_black_);
                img.setColorFilter(Color.parseColor("#e8e8e8"));
                textView2.setTextColor(Color.BLACK);
                textView2.setText(resources.getString(R.string.more));
            }

            // tabLayout.getTabAt(0).select();
        }
    }





    public void checkSalesInfo(String ph)
    {

        String key=  new PreferenceHelper(MainActivity.this).getData(Utils.userkey);



//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(MainActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
              //  progressFragment.dismiss();
                // Toast.makeText(MainActivity.this,"salesinfo : "+data,Toast.LENGTH_SHORT).show();
                try {

                    if(data!=null)
                    {

                        try{

                            JSONObject jsonObject=new JSONObject(data);

                            if(jsonObject.getInt("status")==1)
                            {
                                String d=jsonObject.getString("data");

                                JSONObject jsonObject1_data=new JSONObject(d);

                             if(jsonObject1_data.getString("member_status").equalsIgnoreCase("free"))
                             {
                                 checkTrialUser();
                             }
                             else {

                                 new PreferenceHelper(MainActivity.this).putIntData(Utils.expirycompleted, 0);
                                 new PreferenceHelper(MainActivity.this).putIntData(Utils.expirywarning, 0);


                                // checkExpiryDate();
                             }



                            }
                            else {

                                checkTrialUser();




                            }



                        }catch (Exception e)
                        {

                        }



                    }



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {

                Utils.showAlertWithSingle(MainActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.showMemberDetails+"?mobile="+ph, Request.Method.GET).submitRequest();

















//
//
//        Call<JsonObject> jsonObjectCall=client.getNetworkData(key
//        );
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//               // progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        if(jsonObject.getInt("status")==1)
//                        {
////                            new PreferenceHelper(context).putIntData(Utils.trialcompleted, 0);
////                            new PreferenceHelper(context).putIntData(Utils.trialwarning, 0);
//
//                            //showReferDialog();
//                            checkExpiryDate();
//
//
//
//                        }
//                        else {
//
//                            checkTrialUser();
//
//                            // Toast.makeText(context," You are not purchased thi application",Toast.LENGTH_SHORT).show();
//
////                            Utils.showAlert(MainActivity.this, "You are not purchased this application.do you want to purchase now ? ", new DialogEventListener() {
////                                @Override
////                                public void onPositiveButtonClicked() {
////
////
////                                    moreFragment.passPurchaseDataToServer();
////
////                                }
////
////                                @Override
////                                public void onNegativeButtonClicked() {
////
////                                }
////                            });
//
//
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//               // progressFragment.dismiss();
//            }
//        });





    }








    public void checkTrialUser()
    {

        new CheckTrialPeriod(MainActivity.this, new ExpirydateCheckListener() {
            @Override
            public void OnExpirydateChecked(JSONObject jsonObject) {

                try {

                    String data=jsonObject.toString();

                //    Toast.makeText(MainActivity.this,"trialdata : "+data,Toast.LENGTH_SHORT).show();


                    JSONObject jsonObject1=new JSONObject(data);

                    if(jsonObject1.getInt("status")==0)
                    {

                        new PreferenceHelper(MainActivity.this).putIntData(Utils.trialcompleted, 1);
                        new PreferenceHelper(MainActivity.this).putIntData(Utils.trialwarning, 0);

                        Utils.showAlertWithSingle(MainActivity.this, Utils.Messages.error, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }
                    else if(jsonObject1.getInt("status")==2)
                    {

                        new PreferenceHelper(MainActivity.this).putIntData(Utils.trialwarning, 1);
                        new PreferenceHelper(MainActivity.this).putIntData(Utils.trialcompleted, 0);

                        Utils.showAlertWithSingle(MainActivity.this, Utils.Messages.warning, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }





                }catch (Exception e)
                {

                }

            }

            @Override
            public void OnFailed() {

            }
        }).checkTrialPeriodData();
    }





//    public void checkExpiryDate()
//    {
//
//        new CheckExpiry(MainActivity.this, new ExpirydateCheckListener() {
//            @Override
//            public void OnExpirydateChecked(JSONObject jsonObject1) {
//
//                try {
//
//                   // Toast.makeText(MainActivity.this,"exprydate : "+jsonObject1.toString(),Toast.LENGTH_SHORT).show();
//
//
////                    String data = jsonObject.toString();
////
////                    JSONObject jsonObject1 = new JSONObject(data);
//
//                    if(jsonObject1.getInt("status")==0)
//                    {
//
//                        new PreferenceHelper(MainActivity.this).putIntData(Utils.expirycompleted, 1);
//                        new PreferenceHelper(MainActivity.this).putIntData(Utils.expirywarning, 0);
//
//
//
//                    }
//                    else if(jsonObject1.getInt("status")==2)
//                    {
//
//
//                        new PreferenceHelper(MainActivity.this).putIntData(Utils.expirycompleted, 0);
//                        new PreferenceHelper(MainActivity.this).putIntData(Utils.expirywarning, 1);
//
//                    }
//
//
//                }catch (Exception e)
//                {
//
//                }
//
//
//
//
//            }
//
//            @Override
//            public void OnFailed() {
//
//            }
//        }).checkExpiryDate();
//    }







    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Do you want to logout now ? ");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();

                new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");

                Intent newIntent = new Intent(MainActivity.this, LoginActivity.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

           startActivity(newIntent);



            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });

        builder.show();



    }

    public void getEmergencyData(){

        if(new PreferenceHelper(MainActivity.this).getData(Utils.EmergencyFirst).equalsIgnoreCase("")) {




            for (int i = 0; i < Utils.emergency_utilities.length; i++) {

                String emergencydata=Utils.emergency_utilities[i];
                String emergencyno=Utils.emergency_utilitiesphone[i];


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("emergencydata", emergencydata);
                    jsonObject.put("emergencyno", emergencyno);
                    jsonObject.put("isAllowDelete", "0");


                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_EMERGENCY,jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            new PreferenceHelper(MainActivity.this).putData(Utils.EmergencyFirst,"completed");

        }

    }



    public void addExpenseList()
    {

        Calendar c=Calendar.getInstance();
        int y=c.get(Calendar.YEAR);


        if(new PreferenceHelper(MainActivity.this).getData(Utils.AccSettingsFirst).equalsIgnoreCase("")) {


          //  new DatabaseHelper(MainActivity.this).deleteAllData();

            String arrexp[] = this.getResources().getStringArray(R.array.array_expenses);

            for (int i = 0; i < arrexp.length; i++) {


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("Accountname", arrexp[i]);
                    jsonObject.put("Accounttype", "Expense account");
                    jsonObject.put("Amount", "0");
                    jsonObject.put("Type", "Debit");
                    jsonObject.put("year", String.valueOf(y));

                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            String arr[] = this.getResources().getStringArray(R.array.receiptacc);

            for (int i = 0; i < arr.length; i++) {


                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("Accountname", arr[i]);
                    jsonObject.put("Accounttype", "Income account");
                    jsonObject.put("Amount", "0");
                    jsonObject.put("Type", "Credit");
                    jsonObject.put("year", String.valueOf(y));

                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("Accountname", "Cash");
                jsonObject.put("Accounttype", "Cash");
                jsonObject.put("Amount", "0");
                jsonObject.put("Type", "Debit");

                new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }











            new PreferenceHelper(MainActivity.this).putData(Utils.AccSettingsFirst,"Completed");

        }


        if(new PreferenceHelper(MainActivity.this).getData(Utils.target_first_key).equalsIgnoreCase(""))
        {

            for (int i = 0; i < Utils.arr_target.length; i++) {



                try {


                    Drawable drawable = ContextCompat.getDrawable(MainActivity.this,Utils.arr_target_iconimgs[i]);
                    Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] bitMapData = stream.toByteArray();

                    new DatabaseHelper(MainActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,Utils.arr_target[i],bitMapData);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            new PreferenceHelper(MainActivity.this).putData(Utils.target_first_key,"1");

        }

        if(new PreferenceHelper(MainActivity.this).getData(Utils.target_second_key).equalsIgnoreCase(""))
        {

//            for (int i = 0; i < Utils.arr_target.length; i++) {



            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("Accountname", "My Saving");
                jsonObject.put("Accounttype", "Investment");
                jsonObject.put("Amount", "0");
                jsonObject.put("Type", "Debit");

                new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


//            }

            new PreferenceHelper(MainActivity.this).putData(Utils.target_second_key,"1");

        }




        }



        public void getVisiCardFirst()
        {
            if(new PreferenceHelper(MainActivity.this).getData(Utils.VisitcardFirst).equalsIgnoreCase("")) {

                try{

                    for (int i=0;i<Utils.arrimgs.length;i++) {

                        Drawable d=ContextCompat.getDrawable(MainActivity.this,Utils.arrimgs[i]); // the drawable (Captain Obvious, to the rescue!!!)
                        Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] bitmapdata = stream.toByteArray();

                     long ll=   new DatabaseHelper(MainActivity.this).addVisitcardImgData(bitmapdata);

                       // Utils.showAlertWithSingle(MainActivity.this,"Visiting card  added : "+ll+"",null);
                    }

                    new PreferenceHelper(MainActivity.this).putData(Utils.VisitcardFirst,"CompletedVisitcard");



                }catch (Exception e)
                {
                    Utils.showAlertWithSingle(MainActivity.this,e.toString(),null);
                    e.printStackTrace();
                }
            }
            else{


            //    Utils.showAlertWithSingle(MainActivity.this,"Visiting card cannot added",null);

            }
        }



}
