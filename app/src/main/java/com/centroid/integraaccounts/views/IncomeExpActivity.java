package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.IncomeExpMainAdapter;
import com.centroid.integraaccounts.adapter.LedgerAccountAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.IncExpHead;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class IncomeExpActivity extends AppCompatActivity {

    RecyclerView recycler;

    List<LedgerAccount>ledgerAccounts_income=new ArrayList<>();
    List<LedgerAccount>ledgerAccounts_expense=new ArrayList<>();

    ImageView imgstartDatepick,imgendDatepick,imgdownload,imgback;

    TextView txtstartdatepick,txtenddatepick,txtHead;

    String startdate="",endate="";

    Button btnSearch;
    List<CommonData> cmfiltered_income, cmfiltered_expense;

    List<Accounts>accountsbydate=new ArrayList<>();

    String month_selected = "", yearselected = "";

    String monthyear = "";

    TextView txtStatement;
    double totalinc=0;
    double totalexp=0;
    Resources resources;

    int ye=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_exp);
        getSupportActionBar().hide();
        recycler=findViewById(R.id.recycler);

        imgstartDatepick = findViewById(R.id.imgstartDatepick);
        imgendDatepick = findViewById(R.id.imgendDatepick);
        txtstartdatepick = findViewById(R.id.txtstartdatepick);
        txtenddatepick = findViewById(R.id.txtenddatepick);
        txtStatement=findViewById(R.id.txtStatement);
        txtHead=findViewById(R.id.txtHead);
        btnSearch=findViewById(R.id.btnSearch);
        imgdownload=findViewById(R.id.imgdownload);
        imgback=findViewById(R.id.imgback);
        cmfiltered_income=new ArrayList<>();
        cmfiltered_expense=new ArrayList<>();

        String languagedata = LocaleHelper.getPersistedData(IncomeExpActivity.this, "en");
        Context contex= LocaleHelper.setLocale(IncomeExpActivity.this, languagedata);

         resources=contex.getResources();
        txtHead.setText(resources.getString(R.string.incomeandexpenditure));

        Utils.showTutorial(resources.getString(R.string.incomexptutorial),IncomeExpActivity.this,Utils.Tutorials.incomexptutorial);

        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });


        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


downloadPdf();

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (!startdate.equalsIgnoreCase("")) {

                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate)||endDate.equals(strDate))
                            {


                                String arr[]=startdate.split("-");





                                accountsbydate= Utils.getAccountsBetweenDates(IncomeExpActivity.this,strDate,endDate);



                                getAccounthead();



                            }

                            else {

                             //   Toast.makeText(IncomeExpActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();


                                Utils.showAlertWithSingle(IncomeExpActivity.this,"Select date properly", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });



                            }


                        } else {

                           // Toast.makeText(IncomeExpActivity.this, "Select end date", Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(IncomeExpActivity.this,"Select end date", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }


                    } else {

                      //  Toast.makeText(IncomeExpActivity.this, "Select start date", Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(IncomeExpActivity.this,"Select start date", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });



                    }

                }catch (Exception e)
                {

                }

            }
        });





        showCurrentMonthYear();

    }




    private void downloadPdf()
    {

        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "Save/" + "IncomeExpenditure");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/Save" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();





            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk("Income \n\n", mOrderDetailsTitleFont);// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph);

            document.add(new Paragraph(" \n\n"));


            PdfPTable table = new PdfPTable(3);
            table.addCell("Account");
            table.addCell("Debit");
            table.addCell("Credit");



            int aw=0;

            int a= ledgerAccounts_income.size();

            while (aw<a){

                List<CommonData> commonData = new DatabaseHelper(IncomeExpActivity.this).getAccountSettingsByID(ledgerAccounts_income.get(aw).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table.addCell(jsonObject.getString("Accountname"));


                }




                if(ledgerAccounts_income.get(aw).getDebitcreditopening().equalsIgnoreCase(Utils.Cashtype.debit+""))
                {
                    table.addCell(ledgerAccounts_income.get(aw).getClosingbalance());
                    table.addCell("");
                }
                else {

                    table.addCell("");

                    double d=Double.parseDouble(ledgerAccounts_income.get(aw).getClosingbalance());

                    if(d<0||String.valueOf(d).contains("-"))
                    {
                        d=d*-1;
                    }


                    table.addCell(d+"");

                }





                aw++;
            }


            document.add(table);

            document.add(new Paragraph("\nTotal Income : "+totalinc+"  \n"));


            Font mOrderDetailsTitleFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk1 = new Chunk("Expense \n", mOrderDetailsTitleFont1);// Creating Paragraph to add...


            Paragraph mOrderDetailsTitleParagraph1 = new Paragraph(mOrderDetailsTitleChunk1);
            mOrderDetailsTitleParagraph1.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph1);






            document.add(new Paragraph(" \n\n"));



            PdfPTable table1 = new PdfPTable(3);
            table1.addCell("Account");
            table1.addCell("Debit");
            table1.addCell("Credit");


            aw=0;

            a= ledgerAccounts_expense.size();

            while (aw<a){

                List<CommonData> commonData = new DatabaseHelper(IncomeExpActivity.this).getAccountSettingsByID(ledgerAccounts_expense.get(aw).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table1.addCell(jsonObject.getString("Accountname"));


                }

                //  table1.addCell(ledgerAccountsexpense.get(aw).getClosingbalance());


                if(ledgerAccounts_expense.get(aw).getDebitcreditopening().equalsIgnoreCase(Utils.Cashtype.debit+""))
                {
                    table1.addCell(ledgerAccounts_expense.get(aw).getClosingbalance());
                    table1.addCell("");
                }
                else {

                    table1.addCell("");

                    double d=Double.parseDouble(ledgerAccounts_expense.get(aw).getClosingbalance());

                    if(d<0||String.valueOf(d).contains("-"))
                    {
                        d=d*-1;
                    }
                    table1.addCell(d+"");

                }

                aw++;
            }

            document.add(table1);

            document.add(new Paragraph("\nTotal Expense : "+totalexp+"  \n"));




            if(totalinc>totalexp)
            {
                if(totalexp<0)
                {
                    totalexp=totalexp*-1;
                }


                double d=totalinc-totalexp;
                document.add(new Paragraph("\n Income over expense "+d+" "+" \n"));

            }
            else if (totalexp>totalinc){

                double d=totalinc-totalexp;

                document.add(new Paragraph(("\n Expense over Income"+d+" "+""+" \n")));
            }
            else {

                document.add(new Paragraph("\n Expense equal to Income\n"));
            }









            document.close();

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(IncomeExpActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }




        }catch (Exception e)
        {

        }
    }












    public void showCurrentMonthYear()
    {


        try {
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH) + 1;
            month_selected = m + "";
            yearselected = calendar.get(Calendar.YEAR) + "";

            ye=Integer.parseInt(yearselected);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);


            startdate = "1" + "-" + m + "-" + yearselected;

            endate = dayOfMonth + "-" + m + "-" + yearselected;

            txtstartdatepick.setText(startdate);
            txtenddatepick.setText(endate);

            String arr[]=startdate.split("-");


            String currentYearStartdate="1-1-"+arr[2];




            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(endate);



            if (endDate.after(strDate) || endDate.equals(strDate)) {

                           accountsbydate=new DatabaseHelper(IncomeExpActivity.this).getAllAccounts();
                getAccounthead();



            }


        }catch (Exception e)
        {

        }

    }






    public void getAccounthead() {
        recycler.setVisibility(View.VISIBLE);

        cmfiltered_income.clear();
        cmfiltered_expense.clear();

        ledgerAccounts_expense.clear();
        ledgerAccounts_income.clear();

        List<CommonData> commonData = new DatabaseHelper(IncomeExpActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            for (CommonData cm : commonData) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Income account")) {
                        cmfiltered_income.add(cm);
                    }

                    if (acctype.equalsIgnoreCase("Expense account")) {
                        cmfiltered_expense.add(cm);
                    }


                } catch (Exception e) {

                }

            }
        }
        
        checkIncomeHeads();

    }


    private boolean isAccountAlreadyExist(List<Accounts>accounts,String setupid)
    {

        boolean a=false;
        for (Accounts acc:accounts) {

            if(acc.getACCOUNTS_setupid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }
    
  


    private void checkIncomeHeads()
    {

        //  List<LedgerAccount> ledgerAccounts = new ArrayList<>();

        ledgerAccounts_income.clear();
        List<Accounts> accountsSorted = accountsbydate;

        List<Accounts>accstring=new ArrayList<>();

        for (Accounts acc:accountsSorted) {

            if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
            {
                accstring.add(acc);
            }

        }

        String selected_date = startdate;
        String amount="0";
        double d=0;

        try {

            for (CommonData cm : cmfiltered_income) {

                JSONObject jobj=new JSONObject(cm.getData());

                if(jobj.has("year"))
                {
                    int y=Integer.parseInt(jobj.getString("year"));

                    if(ye>y)
                    {
                        amount="0";
                        d=Double.parseDouble(amount);
                    }
                    else {
                        amount=jobj.getString("Amount");
                        d=Double.parseDouble(amount);

                    }

                }
                else{
                     amount=jobj.getString("Amount");
                     d=Double.parseDouble(amount);
                }



                // Log.e("Amount",d+"");


                if(d>0)
                {


                    double openingbalance = 0;

                    String Type="";

                    if (!cm.getData().equalsIgnoreCase("")) {

                        JSONObject jsonObject = new JSONObject(cm.getData());

                        openingbalance = d;

                        Type=jsonObject.getString("Type");


                    } else {

                        openingbalance = 0;
                    }

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        openingbalance=openingbalance*-1;
                    }





                    double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

//                    double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");

                    double finalclosingbalance = 0;

                    finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                    LedgerAccount ledgerAccount = new LedgerAccount();
                    ledgerAccount.setAccountheadid(cm.getId());
                    ledgerAccount.setAccountheadname("");
                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate+"");

                    if(closingbalancebeforedate<0)
                    {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                    }
                    else {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                    }





                    if(finalclosingbalance<0) {

                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                    }
                    else {
                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                    }
                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                    ledgerAccounts_income.add(ledgerAccount);

                }









                for (Accounts acc:accstring) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                        double openingbalance = 0;

                        String Type="";


                        if (!cm.getData().equalsIgnoreCase("")) {

                            JSONObject jsonObject = new JSONObject(cm.getData());

                            if(jsonObject.has("year")) {
                                int y = Integer.parseInt(jsonObject.getString("year"));

                                if(ye>y)
                                {
                                    amount = "0";
                                    openingbalance = Double.parseDouble(amount);
                                }
                                else {
                                    amount = jobj.getString("Amount");
                                    openingbalance = Double.parseDouble(amount);
                                }


                            }
                            else {
                                openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                            }


                            Type=jsonObject.getString("Type");

                        } else {

                            openingbalance = 0;
                        }

                        if(Type.equalsIgnoreCase("Credit"))
                        {
                            if(openingbalance>0) {
                                openingbalance = openingbalance * -1;
                            }
                        }






                        double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

                        // double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");
                        double finalclosingbalance = 0;


                        finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                        if(!isAccountExist(ledgerAccounts_income,cm.getId())) {


                            LedgerAccount ledgerAccount = new LedgerAccount();
                            ledgerAccount.setAccountheadid(cm.getId());
                            ledgerAccount.setAccountheadname("");
                            ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");

                            if(closingbalancebeforedate<0)
                            {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                            }
                            else {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                            }

                            if (finalclosingbalance<0) {

                                ledgerAccount.setDebitorcredit(Utils.Cashtype.credit + "");

                            } else {
                                ledgerAccount.setDebitorcredit(Utils.Cashtype.debit + "");
                            }
                            ledgerAccount.setClosingbalance(finalclosingbalance + "");

                            ledgerAccounts_income.add(ledgerAccount);
                        }

                    }

                    // }
                }
//
            }
//

        } catch (Exception e) {

        }


//        if(ledgerAccounts_income.size()>0)
//        {
//
//            for (LedgerAccount ledgerAccount:ledgerAccounts_income) {
//
//                totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());
//
//
//
//            }
//            txtTotalIncome.setText("Total Income : "+totalinc+" "+resources.getString(R.string.rs));
//
//            layout_inchead.setVisibility(View.VISIBLE);
//            recyclerincome.setVisibility(View.VISIBLE);
//            imgdownload.setVisibility(View.VISIBLE);
//            //txtStatement.setVisibility(View.VISIBLE);
//
//            recyclerincome.setLayoutManager(new LinearLayoutManager(IncomexpenditureActivity.this));
//            recyclerincome.setAdapter(new LedgerAccountAdapter(IncomexpenditureActivity.this, ledgerAccounts_income,startdate,endate));
//
//        }
//        else {
//
//            txtTotalIncome.setText("Total Income : "+0+" "+resources.getString(R.string.rs));
//            txtTotalIncome.setVisibility(View.INVISIBLE);
//
//            layout_inchead.setVisibility(View.INVISIBLE);
//            recyclerincome.setVisibility(View.INVISIBLE);
//            imgdownload.setVisibility(View.INVISIBLE);
//        }
//
        checkExpenseHeads();

    }


    private void checkExpenseHeads() {



        ledgerAccounts_expense.clear();

        List<Accounts> accountsSorted = accountsbydate;

        List<Accounts>accstring=new ArrayList<>();

        for (Accounts acc:accountsSorted) {

            if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
            {
                accstring.add(acc);
            }

        }

        String selected_date = startdate;
        String amount = "";
        double d = 0;

        try {

            for (CommonData cm : cmfiltered_expense) {

                JSONObject jobj=new JSONObject(cm.getData());
                if(jobj.has("year")) {
                    int y = Integer.parseInt(jobj.getString("year"));
                    if(ye>y)
                    {
                         amount = "0";
                         d = Double.parseDouble(amount);
                    }
                    else{
                         amount = jobj.getString("Amount");
                         d = Double.parseDouble(amount);

                    }

                }
                else {

                     amount = jobj.getString("Amount");
                     d = Double.parseDouble(amount);
                }
                // Log.e("Amount",d+"");


                if(d>0)
                {


                    double openingbalance = d;

                    String Type="";

                    if (!cm.getData().equalsIgnoreCase("")) {

                        JSONObject jsonObject = new JSONObject(cm.getData());
                        if(jsonObject.has("year")) {
                            int y = Integer.parseInt(jsonObject.getString("year"));
                            if(ye>y)
                            {
                                amount = "0";
                                openingbalance = Double.parseDouble(amount);
                            }
                            else{
                                amount = jsonObject.getString("Amount");
                                openingbalance = Double.parseDouble(amount);

                            }

                        }
                        else {

                            openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                        }

                        Type=jsonObject.getString("Type");


                    } else {

                        openingbalance = 0;
                    }

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        openingbalance=openingbalance*-1;
                    }





                    double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

//                    double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");

                    double finalclosingbalance = 0;

                    finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                    LedgerAccount ledgerAccount = new LedgerAccount();
                    ledgerAccount.setAccountheadid(cm.getId());
                    ledgerAccount.setAccountheadname("");
                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate+"");

                    if(closingbalancebeforedate<0)
                    {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                    }
                    else {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                    }





                    if(finalclosingbalance<0) {

                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                    }
                    else {
                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                    }
                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                    ledgerAccounts_expense.add(ledgerAccount);

                }









                for (Accounts acc:accstring) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                        double openingbalance = 0;

                        String Type="";


                        if (!cm.getData().equalsIgnoreCase("")) {

                            JSONObject jsonObject = new JSONObject(cm.getData());

                          //  openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                            Type=jsonObject.getString("Type");

                            if(jsonObject.has("year")) {
                                int y = Integer.parseInt(jsonObject.getString("year"));
                                if(ye>y)
                                {
                                    amount = "0";
                                    openingbalance = Double.parseDouble(amount);
                                }
                                else{
                                    amount = jsonObject.getString("Amount");
                                    openingbalance = Double.parseDouble(amount);

                                }

                            }
                            else {

                                openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                            }


                        } else {

                            openingbalance = 0;
                        }

                        if(Type.equalsIgnoreCase("Credit"))
                        {
                            openingbalance=openingbalance*-1;
                        }






                        double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

                        // double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");
                        double finalclosingbalance = 0;


                        finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                        if(!isAccountExist(ledgerAccounts_expense,cm.getId())) {


                            LedgerAccount ledgerAccount = new LedgerAccount();
                            ledgerAccount.setAccountheadid(cm.getId());
                            ledgerAccount.setAccountheadname("");
                            ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");

                            if(closingbalancebeforedate<0)
                            {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                            }
                            else {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                            }

                            if (finalclosingbalance<0) {

                                ledgerAccount.setDebitorcredit(Utils.Cashtype.credit + "");

                            } else {
                                ledgerAccount.setDebitorcredit(Utils.Cashtype.debit + "");
                            }
                            ledgerAccount.setClosingbalance(finalclosingbalance + "");

                            ledgerAccounts_expense.add(ledgerAccount);
                        }

                    }
                    

                    // }
                }
//
            }
//

        } catch (Exception e) {

        }


        totalinc=0;

        totalexp=0;


        for (LedgerAccount ledgerAccount:ledgerAccounts_expense) {

            totalexp=totalexp+Double.parseDouble(ledgerAccount.getClosingbalance());



        }




        for (LedgerAccount ledgerAccount:ledgerAccounts_income) {

            totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());



        }


        if(totalinc<0)
        {
            totalinc=totalinc*-1;
        }

        if(totalexp<0)
        {
            totalexp=totalexp*-1;
        }






        if(totalexp<totalinc)
        {
            double d1=totalinc-totalexp;

//            if(d<0)
//            {
//                d=d*-1;
//            }


            txtStatement.setText(Utils.getCapsSentences(IncomeExpActivity.this,resources.getString(R.string.incomeoverexpense))+" : "+d1);
        }
        else if(totalexp>totalinc) {

            double d1=totalexp-totalinc;


            txtStatement.setText(Utils.getCapsSentences(IncomeExpActivity.this,resources.getString(R.string.expenseoverincome))+" : "+d1);
        }
        else if(totalexp==totalinc) {

            if(totalinc!=0&&totalexp!=0) {


                txtStatement.setText(Utils.getCapsSentences(IncomeExpActivity.this,resources.getString(R.string.incexpbalance)));

            }
            else {
                txtStatement.setText("");

                recycler.setVisibility(View.GONE);
            }
        }





        List<IncExpHead>incExpHeads=new ArrayList<>();


        IncExpHead incExpHead=new IncExpHead();
        incExpHead.setSelected(0);
        incExpHead.setTotalamount(totalinc);
        incExpHead.setLedgerAccounts(ledgerAccounts_income);
        incExpHeads.add(incExpHead);

        IncExpHead incExpHead1=new IncExpHead();
        incExpHead1.setSelected(0);
        incExpHead.setTotalamount(totalexp);
        incExpHead1.setLedgerAccounts(ledgerAccounts_expense);
        incExpHeads.add(incExpHead1);


        recycler.setLayoutManager(new LinearLayoutManager(IncomeExpActivity.this));
        recycler.setAdapter(new IncomeExpMainAdapter(IncomeExpActivity.this,incExpHeads,startdate,endate));


    }






    private boolean isAccountExist(List<LedgerAccount>accounts,String setupid)
    {

        boolean a=false;
        for (LedgerAccount acc:accounts) {

            if(acc.getAccountheadid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }

    private double getClosingbalancebeforedate(double openingbalance, String id, String selected_date,String type) {

        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(IncomeExpActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());


                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());


                        }


                    }
                }
//                else if(id.equalsIgnoreCase("0")&&acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {
//
//
//                    if(type.equalsIgnoreCase("Income"))
//                    {
//                        openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//                    else {
//
//                        openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//
//
//
//
//                }


            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;
        }

        return closingbalancebeforemonth;
    }





    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(IncomeExpActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                    ye=i;
                }
                else {

                    endate = i2 + "-" + m + "-" + i;

                    txtenddatepick.setText(i2 + "-" + m + "-" + i);

                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

}
