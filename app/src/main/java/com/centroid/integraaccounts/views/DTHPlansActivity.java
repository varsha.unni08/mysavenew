package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.DTHPlansAdapter;
import com.centroid.integraaccounts.adapter.DTHoperatorAdapter;
import com.centroid.integraaccounts.adapter.MobileOperatorAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.DTHPlansFromServer;
import com.centroid.integraaccounts.data.domain.DTHValidity;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.services.OperatorSelectionListener;
import com.centroid.integraaccounts.views.rechargeViews.dth.DTHRechargePack;
import com.centroid.integraaccounts.views.rechargeViews.dth.Pack;
import com.centroid.integraaccounts.views.rechargeViews.dth.Price;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DTHPlansActivity extends AppCompatActivity {

    ImageView imgback;
    RecyclerView rec_plans;
//    Spinner f;
    String Selected_operator_code="", spkey="",fullString="",Selected_operator="",Selected_operator_to_server="",rechargeAmount="0",cardnumber="";
    String fullurl="";
    List<DTHPlansFromServer>dthPlans=new ArrayList<>();
    String dthplans_base_url="https://mysaveapp.com/rechargeAPI/newrecharge/DTHplans.php";
    String dthopfind_base_url="https://mysaveapp.com/rechargeAPI/getDTHOperator.php";
    ImageView imgoperator,imgdthoperator;
    TextView txtSpinner,txtprofile;
    EditText edtPhone,edtSearchAmount;
    LinearLayout layout_operator;
    Button btnsubmit;
    List<Pack> packs=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dthplans);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgdthoperator=findViewById(R.id.imgdthoperator);
        rec_plans=findViewById(R.id.rec_plans);
        txtprofile=findViewById(R.id.txtprofile);
        edtSearchAmount=findViewById(R.id.edtSearchAmount);
//        spinner_dth=findViewById(R.id.spinner_dth);
        imgoperator=findViewById(R.id.imgoperator);
        txtSpinner=findViewById(R.id.txtSpinner);
        edtPhone=findViewById(R.id.edtPhone);
        layout_operator=findViewById(R.id.layout_operator);
        btnsubmit=findViewById(R.id.btnsubmit);
        Intent intent=getIntent();
        spkey=intent.getStringExtra("spkey");
//        cardnumber=intent.getStringExtra("cardnumber");
        Selected_operator=intent.getStringExtra("selectedoperator");
        Selected_operator_to_server=intent.getStringExtra("selectedoperator_to_SERVER");
        Selected_operator_code=intent.getStringExtra("Selected_operator_code");

        for(int j=0;j<RechargePlans.DTHPlans.arr_plans.length;j++) {

            if(RechargePlans.DTHPlans.arr_plans[j].equalsIgnoreCase(Selected_operator))
            {

                imgdthoperator.setImageResource(RechargePlans.DTHPlans.arr_plans_img[j]);
                txtprofile.setText(RechargePlans.DTHPlans.arr_plans[j]);
                break;


            }


        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getRechargePlans();

        edtSearchAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equalsIgnoreCase(""))
                {
                    List<Pack> packsfiltered=new ArrayList<>();

                    for(Pack pack:packs)
                    {
                      List<Price>prices=pack.getPrices();

                      for (Price price:prices)
                      {
                          if(price.getAmount().toString().contains(charSequence.toString()))
                          {
                              packsfiltered.add(pack);

                          }


                      }


                    }


                    rec_plans.setLayoutManager(new LinearLayoutManager(DTHPlansActivity.this));
                    rec_plans.setAdapter(new DTHPlansAdapter(DTHPlansActivity.this, packsfiltered));

                }
                else{

                    rec_plans.setLayoutManager(new LinearLayoutManager(DTHPlansActivity.this));
                    rec_plans.setAdapter(new DTHPlansAdapter(DTHPlansActivity.this, packs));
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

//    private void getOperator(String cardnumber)
//    {
//        final ProgressFragment progressFragment = new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(), "fkjfk");
//        ExecutorService executorService= Executors.newSingleThreadExecutor();
//        Handler handler = new Handler(Looper.getMainLooper());
//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//
//                    Map<String, String> params = new HashMap<>();
//                    params.put("timestamp", Utils.getTimestamp());
////                    String fullurl=urldata+"&timestamp="+Utils.getTimestamp();
//                    fullurl=dthopfind_base_url+"?timestamp="+ Utils.getTimestamp()+"&cardnumber="+cardnumber;
//                    fullString = "";
//                    URL url = new URL(fullurl);
//                    URLConnection connection  = url.openConnection();
//                    connection.setConnectTimeout(1000);
//                    connection.setReadTimeout(2000);
//                    connection.connect();
//                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
//                    String line;
//                    while ((line = reader.readLine()) != null) {
//                        fullString += line;
//                    }
//                    reader.close();
//
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            progressFragment.dismiss();
//
//                            try{
//
//                                Log.e("DTHOperator Response",fullString);
//                                JSONObject jsonObject=new JSONObject(fullString);
//
////                                {"tel":"70474619916","records":{"Operator":"SunDirect","status":1}}
//
//
//
//                                    JSONObject js=jsonObject.getJSONObject("records");
//
//
//
//                                    if(js.has("status"))
//                                    {
//
//                                        int status=js.getInt("status");
//
//                                        if(status==1)
//                                        {
//
//                                            String operator=js.getString("Operator");
//
//
//
//                                            for(int i=0;i<RechargePlans.DTHPlans.arr_plans.length;i++)
//                                            {
//
//                                                double d=Utils.similarity(RechargePlans.DTHPlans.arr_plans[i].trim(),operator);
//                                                Log.e("Similarity Response",d+","+RechargePlans.DTHPlans.arr_plans[i]);
//                                                if(d>=0.500)
//                                                {
//
//                                                    spkey=RechargePlans.DTHPlans.spkeys[i];
//                                                    Selected_operator_to_server=RechargePlans.DTHPlans.arr_plans_toServer[i];
//                                                    Selected_operator=RechargePlans.DTHPlans.arr_plans[i];
//                                                    rec_plans.setVisibility(View.VISIBLE);
//                                                    layout_operator.setVisibility(View.VISIBLE);
//                                                    dthPlans.clear();
//                                                    getRechargePlans();
//
//                                                    imgoperator.setImageResource(RechargePlans.DTHPlans.arr_plans_img[i]);
//                                                    imgoperator.setVisibility(View.VISIBLE);
//                                                    txtSpinner.setText(RechargePlans.DTHPlans.arr_plans[i]);
//
//                                                    break;
//                                                }
//
//
//                                            }
//
//
//                                        }
//                                        else{
//
////                                            Utils.showAlertWithSingle(DTHPlansActivity.this,"No plans available",null);
//
//                                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Customer not found", Snackbar.LENGTH_LONG);
//
//
//                                            snackbar.show();
//                                        }
//
//
//
//
//
//                                    }
//                                    else{
//
////                                        Utils.showAlertWithSingle(DTHPlansActivity.this,"No plans available",null);
//
//                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "No plans available", Snackbar.LENGTH_LONG);
//
//
//                                        snackbar.show();
//                                    }
//
//
//
//
//
//
//
//
//
//
//
//                            }catch (Exception e)
//                            {
//                               // Utils.showAlertWithSingle(DTHPlansActivity.this,"Unable to fetch plans",null);
//                                progressFragment.dismiss();
//
//                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Unable to fetch plans", Snackbar.LENGTH_LONG);
//
//
//                                snackbar.show();
//                            }
//
////                            Utils.showAlertWithSingle(DTHPlansActivity.this,fullString,null);
//
//
//
//
//
//                        }
//                    });
//
//
//
//                } catch (Exception e) {
//
//
//                }
//            }
//        });
//    }


    private void getRechargePlans()
    {

        ExecutorService executorService= Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final ProgressFragment progressFragment = new ProgressFragment();
                    progressFragment.show(getSupportFragmentManager(), "fkjfk");

                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
//                    String fullurl=urldata+"&timestamp="+Utils.getTimestamp();
                    fullurl=dthplans_base_url+"?timestamp="+ Utils.getTimestamp()+"&operatorcode="+Selected_operator_code;
                   fullString = "";
                    URL url = new URL(fullurl);
                    URLConnection connection  = url.openConnection();
                    connection.setConnectTimeout(1000);
                    connection.setReadTimeout(2000);
                    connection.connect();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            progressFragment.dismiss();

                            try{


                                DTHRechargePack dthRechargePack=new GsonBuilder().create().fromJson(fullString, DTHRechargePack.class);



                              if(dthRechargePack.getStatus().compareTo("OK")==0) {
                                  packs.clear();
                                  packs.addAll(dthRechargePack.getPacks());
                                  rec_plans.setLayoutManager(new LinearLayoutManager(DTHPlansActivity.this));
                                  rec_plans.setAdapter(new DTHPlansAdapter(DTHPlansActivity.this, packs));
                              }

//                                JSONObject jsonObject=new JSONObject(fullString);
//                                if(jsonObject.getInt("status")==1)
//                                {
//
//                                    JSONObject js=jsonObject.getJSONObject("records");
//
//                                    if(js.has("Plan"))
//                                    {
//                                        JSONArray jsonArray=js.getJSONArray("Plan");
//
//                                        if(jsonArray.length()>0)
//                                        {
//
//
//
//                                            for (int i=0;i<jsonArray.length();i++)
//                                            {
//                                                JSONObject jsonObject1=  jsonArray.getJSONObject(i).getJSONObject("rs");
//                                                String description=jsonArray.getJSONObject(i).getString("desc");
//                                                String plan_name=jsonArray.getJSONObject(i).getString("plan_name");
//
//                                                HashMap<String, String> planMap = new GsonBuilder().create().fromJson(jsonObject1.toString(), HashMap.class);
//
//                                                List<DTHValidity>dthValidities=new ArrayList<>();
//                                                for (Map.Entry<String,String> entry : planMap.entrySet()) {
//
//                                                    DTHValidity dthValidity=new DTHValidity();
//                                                    dthValidity.setValidity(entry.getKey());
//                                                    dthValidity.setAmount(entry.getValue());
//                                                    dthValidities.add(dthValidity);
//
//
//                                                    System.out.println("Key = " + entry.getKey() +
//                                                            ", Value = " + entry.getValue());
//                                                }
//
//                                                DTHPlansFromServer dthPlansFromServer=new DTHPlansFromServer();
//                                                dthPlansFromServer.setPlan_name(plan_name);
//                                                dthPlansFromServer.setDescription(description);
//                                                dthPlansFromServer.setDthValidities(dthValidities);
//
//                                                dthPlans.add(dthPlansFromServer);
//
////need to create a domain class
//
//                                            }
//
//
//rec_plans.setLayoutManager(new LinearLayoutManager(DTHPlansActivity.this));
//                                            rec_plans.setAdapter(new DTHPlansAdapter(DTHPlansActivity.this,dthPlans));
//
//
//                                        }
//
//
//
//
//
//
//                                    }
//
//
//
//
//
//
//
//                                }
//                                else{
//
//
//                                    Utils.showAlertWithSingle(DTHPlansActivity.this,"No Plans Available",null);
//                                }



                            }catch (Exception e)
                            {
                                Utils.showAlertWithSingle(DTHPlansActivity.this,"Unable to Fetch Plans",null);
                            }

//                            Utils.showAlertWithSingle(DTHPlansActivity.this,fullString,null);





                        }
                    });



                } catch (Exception e) {


                }
            }
        });




    }


    public void redirectToRecharge(String amount)
    {
     this.rechargeAmount=amount;
                                    Intent intent=new Intent(DTHPlansActivity.this, DTHRechargeActivity.class);
                            intent.putExtra("amount",rechargeAmount);
        intent.putExtra("spkey",spkey);
        intent.putExtra("operator",Selected_operator);
//        intent.putExtra("cardnumber",cardnumber);
                         startActivity(intent);

    }
}