package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.adapter.PaymentVoucherAdapter;
import com.centroid.integraaccounts.adapter.VoucherPagerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.fragments.SetBudgetFragment;
import com.centroid.integraaccounts.fragments.VoucherFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class PaymentVoucherActivity extends AppCompatActivity {

    TextView txtHead,txtbudget;

    ImageView imgback,imgaacsettings,imgbudget;



    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtdatepick;
    ImageView imgDatepick;
    LinearLayout layout_head;

    Button submit;

    int m=0,yea=0;

    Spinner spinnerAccountName;
    TextView txtTotal;
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    PaymentVoucherAdapter paymentVoucherAdapter;

    List<Accounts>paymentVouchers_selected=new ArrayList<>();

//    Button btnsetBudget;


    List<CommonData>cmfiltered;

    Resources resources;

    TextView txtdate,txtaccount,txtamount,txttype,txtaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher);
        getSupportActionBar().hide();
        cmfiltered=new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgaacsettings=findViewById(R.id.imgaacsettings);
        imgbudget=findViewById(R.id.imgbudget);
        layout_head=findViewById(R.id.layout_head);
        txtbudget=findViewById(R.id.txtbudget);



        txtdate=findViewById(R.id.txtdate);
        txtaccount=findViewById(R.id.txtaccount);
        txtamount=findViewById(R.id.txtamount);
        txttype=findViewById(R.id.txttype);
        txtaction=findViewById(R.id.txtaction);






        paymentVouchers_selected=new ArrayList<>();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);

        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);
        submit=findViewById(R.id.submit);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        txtTotal=findViewById(R.id.txtTotal);



        String languagedata = LocaleHelper.getPersistedData(PaymentVoucherActivity.this, "en");
        Context context= LocaleHelper.setLocale(PaymentVoucherActivity.this, languagedata);

        resources=context.getResources();
        submit.setText(resources.getString(R.string.search));
        txtHead.setText(resources.getString(R.string.paymentvoucher));
        txtbudget.setText(resources.getString(R.string.setbudget));

        txtdate.setText(resources.getString(R.string.date));
        txtaccount.setText(resources.getString(R.string.accountname));
        txtamount.setText(resources.getString(R.string.amount));
        txttype.setText(resources.getString(R.string.cashdata)+"/"+resources.getString(R.string.bank));
        txtaction.setText(resources.getString(R.string.action));


        if(new PreferenceHelper(PaymentVoucherActivity.this).getIntData(Utils.trialcompleted)==1)
        {
           // Toast.makeText(PaymentVoucherActivity.this,Utils.Messages.error,Toast.LENGTH_SHORT).show();


            Utils.showAlertWithSingle(PaymentVoucherActivity.this, Utils.Messages.error, new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });


            fab_addtask.hide();
        }

        if(new PreferenceHelper(PaymentVoucherActivity.this).getIntData(Utils.expirycompleted)==1)
        {
           // Toast.makeText(PaymentVoucherActivity.this,Utils.Messages.expiryerror,Toast.LENGTH_SHORT).show();

            Utils.showAlertWithSingle(PaymentVoucherActivity.this, Utils.Messages.expiryerror, new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });


            fab_addtask.hide();
        }





        txtbudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent=new Intent(PaymentVoucherActivity.this, BudgetListActivity.class);
                startActivity(intent);
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(!spinnerAccountName.getSelectedItem().toString().equalsIgnoreCase("Select account name"))
//                {

//                if(yea!=0||m!=0)
//                {
//
//
//
//                    getVoucherData();
//
//
//                }
//                else {
//
//                    Toast.makeText(PaymentVoucherActivity.this,resources.getString(R.string.selectmonthyear),Toast.LENGTH_SHORT).show();
//
//                }




//                }
//                else {
//
//                    Toast.makeText(PaymentVoucherActivity.this,"Select account name",Toast.LENGTH_SHORT).show();
//
//                }



            }
        });



        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthDialog();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthDialog();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(new PreferenceHelper(PaymentVoucherActivity.this).getIntData(Utils.trialwarning)==1)
                {
                   // Toast.makeText(PaymentVoucherActivity.this,Utils.Messages.warning,Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(PaymentVoucherActivity.this, Utils.Messages.warning, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }

                if(new PreferenceHelper(PaymentVoucherActivity.this).getIntData(Utils.expirywarning)==1)
                {
                   // Toast.makeText(PaymentVoucherActivity.this,Utils.Messages.expirywarning,Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(PaymentVoucherActivity.this, Utils.Messages.expirywarning, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }



                Intent intent=new Intent(PaymentVoucherActivity.this, AddPaymentVoucherActivity.class);
                startActivity(intent);
            }
        });

        imgbudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent=new Intent(PaymentVoucherActivity.this, BudgetListActivity.class);
//                startActivity(intent);

            }
        });



        
      //  btnsetBudget=findViewById(R.id.btnsetBudget);














        imgaacsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(PaymentVoucherActivity.this, AccountsettingsActivity.class));

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        addAccountSettings();
        getDateMonthInitial();

        Utils.showTutorial(resources.getString(R.string.paymentvouchertutorial),PaymentVoucherActivity.this,Utils.Tutorials.paymentvouchertutorial);


    }







    private void getDateMonthInitial()
    {

        Calendar calendar=Calendar.getInstance();




        m =  calendar.get(Calendar.MONTH);

        yea = calendar.get(Calendar.YEAR);

        String month=arrmonth[m];

        txtdatepick.setText(month+"/"+yea);

        getVoucherData();

    }




    @Override
    protected void onRestart() {
        super.onRestart();

getVoucherData();
    }


    private void showMonthDialog() {

        String month="",yearselected="";


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR)-1;
        int m_calender=calendar.get(Calendar.MONTH);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        PaymentVoucherActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(PaymentVoucherActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);
        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setValue(m_calender);


        npmonth.setDisplayedValues(arrmonth);
        final NumberPicker npyear=dialog.findViewById(R.id.npyear);
        npyear.setMinValue(year);
        npyear.setMaxValue(year+5);


        npyear.setValue(year);
        npyear.setWrapSelectorWheel(true);
        dialog.getWindow().setLayout(width,height);
        npmonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {


            }
        });

        npyear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });





        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                m =  npmonth.getValue();

                yea = npyear.getValue();

                String month=arrmonth[m];

                txtdatepick.setText(month+"/"+yea);
                getVoucherData();

               // getVoucherData();

                //getVoucherData(m,yea);


            }
        });


        dialog.show();





    }


    public void getVoucherData()
    {
        String monthinarray=arrmonth[m];

        paymentVouchers_selected.clear();

        try {

            int selected_month = m + 1;

            //call get data here

            List<Accounts> paymentVouchers = new DatabaseHelper(PaymentVoucherActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.paymentvoucher+"");


            Log.e("TAG",paymentVouchers.size()+"");

            double total=0;

            for (Accounts paymentVoucher : paymentVouchers
            ) {



                String month=paymentVoucher.getACCOUNTS_month();
                String year1=paymentVoucher.getACCOUNTS_year();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                int vouchertype=paymentVoucher.getACCOUNTS_vouchertype();
               // String setupid=paymentVoucher.getACCOUNTS_setupid();
                String amount=paymentVoucher.getACCOUNTS_amount();;
               // CommonData cm=(CommonData) spinnerAccountName.getSelectedItem();

//&&setupid.equalsIgnoreCase(cm.getId())

                if(String.valueOf(selected_month).equalsIgnoreCase(month)&&String.valueOf(yea).equalsIgnoreCase(year1))
                {



                    if(entryid.equalsIgnoreCase("0")) {


                        total = total + Double.parseDouble(amount);
                        paymentVouchers_selected.add(paymentVoucher);
                    }







                     }





            }


            txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

            //checking budget


            List<Budget> budgets = new DatabaseHelper(PaymentVoucherActivity.this).getBudgetData();

            if(budgets.size()>0) {

//                for (Budget budget : budgets) {
//
//                    JSONObject jsonObject = new JSONObject(budget.getData());
//
//
//                    String year1 = jsonObject.getString("year");
//                    String amount = jsonObject.getString("amount");
//                    String month = jsonObject.getString("month");
//                    String accountname = jsonObject.getString("accountname");
//
//                    CommonData cm=(CommonData) spinnerAccountName.getSelectedItem();
//
//                    //accountname.equalsIgnoreCase(cm.getId())
//
//
//                    if (year1.equalsIgnoreCase(String.valueOf(yea)) && monthinarray.equalsIgnoreCase(month)) {
//
//
//                        double budgetamount = Double.parseDouble(amount);
//
//                        if (budgetamount > total) {
//
//                            txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));
//                        } else {
//
//                            txtTotal.setText(resources.getString(R.string.total)+" : " + total + getResources().getString(R.string.rs)+" \n"+resources.getString(R.string.totalexceedbudget));
//
//                        }
//
//
//                    }
//
//
//                }
            }
            else {

//
//                if(budgets.size()==0&&paymentVouchers_selected.size()==0)
//                {
//
//                    txtTotal.setText(resources.getString(R.string.total)+" : " + total + " " + getResources().getString(R.string.rs) );
//
//
//                }
//                else {
//
//
//                    txtTotal.setText(resources.getString(R.string.total)+" : " + total + " " + getResources().getString(R.string.rs) + " \n"+resources.getString(R.string.totalexceedbudget));
//
//
//                }
            }




            if(paymentVouchers_selected.size()>0) {


                Collections.sort(paymentVouchers_selected, new Comparator<Accounts>() {
                    @Override
                    public int compare(Accounts accounts, Accounts t1) {

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate=null;

                        Date endDate=null;
                        try {
                            strDate = sdf.parse(accounts.getACCOUNTS_date());
                            endDate = sdf.parse(t1.getACCOUNTS_date());




                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        return strDate.compareTo(endDate);
                    }
                });




                layout_head.setVisibility(View.VISIBLE);
                recycler.setVisibility(View.VISIBLE);

                Collections.reverse(paymentVouchers_selected);


                paymentVoucherAdapter = new PaymentVoucherAdapter(PaymentVoucherActivity.this, paymentVouchers_selected);
                recycler.setLayoutManager(new LinearLayoutManager(PaymentVoucherActivity.this));
                recycler.setAdapter(paymentVoucherAdapter);
            }
            else {

                layout_head.setVisibility(View.GONE);
                recycler.setVisibility(View.INVISIBLE);
            }




        }catch (Exception e)
        {

        }
    }




    public void addAccountSettings()
    {


        List<CommonData>commonData=new DatabaseHelper(PaymentVoucherActivity.this).getAccountSettingsData();

        if(commonData.size()>0)
        {

            Collections.sort(commonData, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a=0;

                    try{



                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype= jcmn1.getString("Accounttype");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1= j1.getString("Accounttype");

                    a=     acctype.compareTo(acctypej1);

                    }
                    catch (Exception e)
                    {

                    }





                    return a;
                }
            });


//            for (CommonData cm:commonData) {
//
//
//
//                try {
//
//                    JSONObject jsonObject = new JSONObject(cm.getData());
//
//                    String acctype= jsonObject.getString("Accounttype");
//
//                    if(acctype.equalsIgnoreCase("Payment account"))
//                    {
//                        cmfiltered.add(cm);
//                    }
//
//
//                    //
//
//
//
//
//                }catch (Exception e)
//                {
//
//                }
//
//            }


            AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(PaymentVoucherActivity.this, commonData);
            spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);

        }


    }
}
