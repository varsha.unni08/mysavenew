package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.UserDetails;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoginActivity extends AppCompatActivity {

    Button btnCreate,btnlogin,btnForgotpassword;

    EditText edtPhone,edtPassword;
    String  randomnum="";
    ImageView img;
    TextView txtquote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        btnCreate=findViewById(R.id.btnCreate);
        btnlogin=findViewById(R.id.btnlogin);
        img=findViewById(R.id.img);
        txtquote=findViewById(R.id.txtquote);
        btnForgotpassword=findViewById(R.id.btnForgotpassword);

        edtPassword=findViewById(R.id.edtPassword);
        edtPhone=findViewById(R.id.edtPhone);

        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fadein);

        img.setAnimation(animFadeIn);
        txtquote.setAnimation(animFadeIn);




        String languagedata = LocaleHelper.getPersistedData(LoginActivity.this, "en");
        Context context= LocaleHelper.setLocale(LoginActivity.this, languagedata);

        Resources resources=context.getResources();
        txtquote.setText("My Personal App");





        btnForgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.setContentView(R.layout.layout_phonedialog);
                dialog.setTitle(R.string.app_name);
                int b=(int)(height/1.2);
                dialog.getWindow().setLayout(width - 50,b );

                final EditText edtPh=dialog.findViewById(R.id.edtPh);

                Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String mob=   new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser);


                        if(!edtPh.getText().toString().equals("")) {
                            //  String mob=   new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser);

                            if (!mob.equalsIgnoreCase("")) {

                                if (edtPh.getText().toString().trim().equalsIgnoreCase(mob)) {

                                    dialog.dismiss();

                                    //   checkMobile(edtPh.getText().toString().trim(),dialog);

                                    checkMobileExist(edtPh.getText().toString().trim());


                                } else {

                                    Utils.showAlertWithSingle(LoginActivity.this, "Sorry, This mobile number is not logged in this device", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });

                                    //Toast.makeText(LoginActivity.this,"Enter a valid phone number",Toast.LENGTH_SHORT).show();
                                }
                            }
                            else{


                                dialog.dismiss();

                                //   checkMobile(edtPh.getText().toString().trim(),dialog);

                                checkMobileExist(edtPh.getText().toString().trim());

                            }



                        }

                        else {

                            Utils.showAlertWithSingle(LoginActivity.this, "Enter your phone number", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });

                           // Toast.makeText(LoginActivity.this,"Enter your phone number",Toast.LENGTH_SHORT).show();
                        }




                    }
                });


                dialog.show();



            }
        });


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!edtPhone.getText().toString().equals(""))
                {

                    if(!edtPassword.getText().toString().equals(""))
                    {


                        if(!Utils.isConnectionAvailable(LoginActivity.this)) {


                            if (!new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser).equalsIgnoreCase("") && !new PreferenceHelper(LoginActivity.this).getData(Utils.firstpassword).equalsIgnoreCase("")) {


                                if (new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser).equalsIgnoreCase(edtPhone.getText().toString().trim())) {
                                    String base64pass = Utils.getBase64Password(edtPassword.getText().toString().trim());

                                    if (new PreferenceHelper(LoginActivity.this).getData(Utils.firstpassword).equalsIgnoreCase(base64pass)) {


                                      //  String token = new PreferenceHelper(LoginActivity.this).getData(Utils.backuptoken);

                                     //   new PreferenceHelper(LoginActivity.this).putData(Utils.userkey, token);


                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);

                                        startActivity(i);
                                        finish();

                                    } else {


                                        Utils.showAlertWithSingle(LoginActivity.this, "Wrong password", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    }


                                } else {

                                    Utils.showAlertWithSingle(LoginActivity.this, "Sorry , This device is already authenticated by another number", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });

                                }

                            }
                        }

                        else{







                      //  if (new PreferenceHelper(LoginActivity.this).getData(Utils.firstpassword).equalsIgnoreCase(base64pass)) {




                            String uuid = "";

                                try {

                                    String uniquePseudoID = "35" +
                                            Build.BOARD.length() % 10 +
                                            Build.BRAND.length() % 10 +
                                            Build.DEVICE.length() % 10 +
                                            Build.DISPLAY.length() % 10 +
                                            Build.HOST.length() % 10 +
                                            Build.ID.length() % 10 +
                                            Build.MANUFACTURER.length() % 10 +
                                            Build.MODEL.length() % 10 +
                                            Build.PRODUCT.length() % 10 +
                                            Build.TAGS.length() % 10 +
                                            Build.TYPE.length() % 10 +
                                            Build.USER.length() % 10;
                                    String serial = Build.getRadioVersion();

                                    if(uniquePseudoID!=null)
                                    {

                                        uniquePseudoID="xjhhbvjxcvjjxcvbjjdbfhgfdbgfdbjbhbfhvjbvjvb";
                                    }


                                    uuid = new UUID(uniquePseudoID.hashCode(), serial.hashCode()).toString();

                                    // ffffffff-8a93-ca89-ffff-ffffa56d5aee

                                    ;
                                } catch (SecurityException e) {

                                    Log.e("Exception", e.toString());
                                }


                                final ProgressFragment progressFragment = new ProgressFragment();
                                progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                Map<String, String> params = new HashMap<>();
                                params.put("uuid", uuid);
                                params.put("mobile", edtPhone.getText().toString().trim());
                                params.put("password", edtPassword.getText().toString().trim());
                                params.put("timestamp", Utils.getTimestamp());


                                new RequestHandler(LoginActivity.this, params, new ResponseHandler() {
                                    @Override
                                    public void onSuccess(String data) {
                                        progressFragment.dismiss();
                                        // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                                        try {

                                            JSONObject jsonObject = new JSONObject(data);

                                            if(!new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser).equalsIgnoreCase(""))
                                            {

                                                if (new PreferenceHelper(LoginActivity.this).getData(Utils.firstuser).equalsIgnoreCase(edtPhone.getText().toString().trim()))
                                                {

                                                    String base64pass=Utils.getBase64Password(edtPassword.getText().toString().trim());



                                                        parseLogin(jsonObject);




                                                } else {

                                                    Utils.showAlertWithSingle(LoginActivity.this, "Sorry , This device is already authenticated by another number", new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });

                                                }

                                                // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();


                                            } else {

                                                parseLogin(jsonObject);

                                              //  Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {

                                        }

                                    }

                                    @Override
                                    public void onFailure(String err) {
                                        progressFragment.dismiss();

                                      //  Toast.makeText(LoginActivity.this, err, Toast.LENGTH_SHORT).show();

                                    }
                                }, Utils.WebServiceMethodes.login, Request.Method.POST).submitRequest();



                       }



                    }
                    else {

                       // Toast.makeText(LoginActivity.this,"Enter the password",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(LoginActivity.this, "Enter the password", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }

                }
                else {

                    //Toast.makeText(LoginActivity.this,"Enter the phone number",Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(LoginActivity.this, "Enter the phone number", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }

                //startActivity(new Intent(LoginActivity.this,MainActivity.class));
            }
        });



        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));


            }
        });
    }



    public void parseLogin(JSONObject jsonObject)
    {
        try {


            final ProgressFragment progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "fkjfk");

            Map<String, String> params = new HashMap<>();

            params.put("mobile", edtPhone.getText().toString().trim());

            params.put("timestamp", Utils.getTimestamp());


            new RequestHandler(LoginActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();

                    Log.e("TAG", "onSuccess: "+data );
                   // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                    try {

                        JSONObject jsonObject2 = new JSONObject(data);

                        if(jsonObject2.getInt("status")==1)
                        {

                            String appdata=jsonObject2.getString("data");
                            if(appdata.compareTo("save-app")==0)
                            {
                                if (jsonObject.getInt("status") == 1) {


                                    String token = jsonObject.getString("token");

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.backuptoken, token);

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.userkey, token);
                                    new PreferenceHelper(LoginActivity.this).putData(Utils.firstuser, edtPhone.getText().toString().trim());

                                    String basepass=Utils.getBase64Password(edtPassword.getText().toString().trim());

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.firstpassword, basepass);


                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);

                                    startActivity(i);
                                    finish();


                                } else if (jsonObject.getInt("status") == 2) {


                                    // new PreferenceHelper(LoginActivity.this).putData(Utils.userkey,token);
                                    String token = jsonObject.getString("token");

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.userkey, token);
                                    new PreferenceHelper(LoginActivity.this).putData(Utils.firstuser, edtPhone.getText().toString().trim());

                                    String basepass=Utils.getBase64Password(edtPassword.getText().toString().trim());

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.firstpassword, basepass);

                                    new PreferenceHelper(LoginActivity.this).putData(Utils.backuptoken, token);


                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);

                                    startActivity(i);
                                    finish();



                                } else {

                                    Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                                }


                            }
                            else{

                                Utils.showAlertWithSingle(LoginActivity.this, "Your active product is wonder-maths.So you cannot login to this app", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

                                //redirect to wonder maths
                            }



                        }
                        else{

                            if (jsonObject.getInt("status") == 1) {


                                String token = jsonObject.getString("token");

                                new PreferenceHelper(LoginActivity.this).putData(Utils.backuptoken, token);

                                new PreferenceHelper(LoginActivity.this).putData(Utils.userkey, token);
                                new PreferenceHelper(LoginActivity.this).putData(Utils.firstuser, edtPhone.getText().toString().trim());

                                String basepass=Utils.getBase64Password(edtPassword.getText().toString().trim());

                                new PreferenceHelper(LoginActivity.this).putData(Utils.firstpassword, basepass);


                                Intent i = new Intent(LoginActivity.this, MainActivity.class);

                                startActivity(i);
                                finish();


                            } else if (jsonObject.getInt("status") == 2) {


                                // new PreferenceHelper(LoginActivity.this).putData(Utils.userkey,token);
                                String token = jsonObject.getString("token");

                                new PreferenceHelper(LoginActivity.this).putData(Utils.userkey, token);
                                new PreferenceHelper(LoginActivity.this).putData(Utils.firstuser, edtPhone.getText().toString().trim());

                                String basepass=Utils.getBase64Password(edtPassword.getText().toString().trim());

                                new PreferenceHelper(LoginActivity.this).putData(Utils.firstpassword, basepass);

                                new PreferenceHelper(LoginActivity.this).putData(Utils.backuptoken, token);


                                Intent i = new Intent(LoginActivity.this, MainActivity.class);

                                startActivity(i);
                                finish();



                            } else {

                                Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                            }

                        }


//                        {
//                            "status": 1,
//                                "data": "save-app",
//                                "message": "New information"
//                        }







                    } catch (Exception e) {

                    }

                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    //  Toast.makeText(LoginActivity.this, err, Toast.LENGTH_SHORT).show();

                }
            }, Utils.WebServiceMethodes.checkApp, Request.Method.POST).submitRequest();









        }catch (Exception e)
        {

        }
    }











    public void checkMobileExist(String phone)
    {



        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");




        Map<String,String> params=new HashMap<>();
        //params.put("countryid",countryid+"");
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(LoginActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try{

                    //  JSONObject jsonObject=new JSONObject(response.body().toString());\

                    //                        JSONObject jsonObject=new JSONObject(response.body().toString());

                    UserDetails userData=new GsonBuilder().create().fromJson(data,UserDetails.class);

                    if(userData.getStatus()==1)
                    {

                        sendOtp(phone);
                        confirmotp(phone);

                    }
                    else {







                    }




                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(LoginActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserByMobile+"?mobile="+phone+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();








//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");
//
//
//
//
//
//        Call<JsonObject> jsonObjectCall=client.getUserByMobile(
//                phone.trim()
//        );
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//
//                if(response.body()!=null)
//                {
//
//                    try{
//
////                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        UserDetails userData=new GsonBuilder().create().fromJson(response.body().toString(),UserDetails.class);
//
//                        if(userData.getStatus()==1)
//                        {
//
//                            sendOtp(phone);
//                            confirmotp(phone);
//
//                        }
//                        else {
//
//
//
//
//
//
//
//                        }

//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });




    }




    public void confirmotp(final String ph)
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.layout_otp);
        dialog.setTitle("My Save");
        int b=(int)(height/1.2);
        dialog.getWindow().setLayout(width - 30,b );

        final EditText edtPh=dialog.findViewById(R.id.edtPh);

        TextView txtOtpcode=dialog.findViewById(R.id.txtOtpcode);

        TextView txtResend=dialog.findViewById(R.id.txtResend);

        txtOtpcode.setText("We sent a code to verify your mobile number "+ph);



        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);


        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                sendOtp(ph);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!edtPh.getText().toString().equals(""))
                {


                    if(edtPh.getText().toString().equals(randomnum))
                    {


                        dialog.dismiss();

                        Intent intent=new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                        intent.putExtra("phone",ph);




                        startActivity(intent);



                    }

                    else {

                        Toast.makeText(LoginActivity.this,"Enter a valid otp code",Toast.LENGTH_SHORT).show();
                    }



                }

                else {

                    Toast.makeText(LoginActivity.this,"Enter your otp code",Toast.LENGTH_SHORT).show();
                }




            }
        });


        dialog.show();




    }


    public void sendOtp(String phonenumber)
    {
        Random random=new Random();
        randomnum = String.format("%04d", random.nextInt(10000));



        String message=Utils.buildServerMessage(Utils.ServerMessageType.forgot_password,randomnum,"","");


        final ProgressFragment p1=new ProgressFragment();
        p1.show(getSupportFragmentManager(),"fkjfk");


        //  String message = Utils.buildServerMessage(Utils.ServerMessageType.registration, randomnum, usr.getName(), "");

        String u=message.replace(" ","%20");



        String urldata= Utils.smsbaseurl+Utils.WebServiceMethodes.smsMethode+"?token="+Utils.ServerMessage.apikey+
                "&sender="+Utils.ServerMessage.sender+
                "&number="+phonenumber+
                "&route="+Utils.ServerMessage.route+
                "&type="+Utils.ServerMessage.type+
                "&sms="+u+"&templateid="+Utils.ServerMessage.forgotpasstemplateid;

        // Log.e("url",urldata);


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {


                    URL url = new URL(urldata);

                    Log.e("url",urldata);

                    /* for Production */
                    // URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);

                    try {
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        // connection.setRequestProperty("Content-Type", "application/json");
                        connection.setDoOutput(true);



                        String responseData="";
                        InputStream is = connection.getInputStream();
                        BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                        if ((responseData = responseReader.readLine()) != null) {
                            Log.e("OTP DATA RESPONSE" , responseData);


                        }








                        responseReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Log.e("TAG",exception.toString());
                    }
                }catch (Exception e)
                {
                    Log.e("TAG",e.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        p1.dismiss();
                    }
                });


            }
        });
    }
}
