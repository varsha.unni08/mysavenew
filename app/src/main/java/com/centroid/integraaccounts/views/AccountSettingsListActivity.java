package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class AccountSettingsListActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    ImageView imgback;

    EditText edtSearch;
    List<CommonData>commonData;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings_list);
        getSupportActionBar().hide();
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        edtSearch=findViewById(R.id.edtSearch);

        commonData=new ArrayList<>();

        String languagedata = LocaleHelper.getPersistedData(AccountSettingsListActivity.this, "en");
        Context context= LocaleHelper.setLocale(AccountSettingsListActivity.this, languagedata);

        Resources resources=context.getResources();

        Utils.showTutorial(resources.getString(R.string.accsetuptutorial),AccountSettingsListActivity.this,Utils.Tutorials.accsetuptutorial);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(AccountSettingsListActivity.this, AccountsettingsActivity.class));
            }
        });

        getAccountSettings();


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


showSortedList(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public void getAccountSettings()
    {

       commonData=new DatabaseHelper(AccountSettingsListActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        Collections.sort(commonData, new Comparator<CommonData>() {
            @Override
            public int compare(CommonData commonData, CommonData t1) {

                String acc1="",acc2="";
                try {


                    JSONObject jsonObject = new JSONObject(commonData.getData());
                    JSONObject jsonObject1 = new JSONObject(t1.getData());


                     acc1=jsonObject.getString("Accountname");
                     acc2=jsonObject1.getString("Accountname");





                }catch (Exception e)
                {

                }



                return acc1.compareTo(acc2);
            }
        });

        if(commonData.size()>0)
        {
            recycler.setVisibility(View.VISIBLE);
            AccountSettingsAdapter accountSettingsAdapter=new AccountSettingsAdapter(AccountSettingsListActivity.this,commonData);
            recycler.setLayoutManager(new LinearLayoutManager(AccountSettingsListActivity.this));

            recycler.setAdapter(accountSettingsAdapter);



        }





    }

    @Override
    protected void onRestart() {
        super.onRestart();



        getAccountSettings();

        if(!edtSearch.getText().toString().equalsIgnoreCase(""))
        {

showSortedList(edtSearch.getText().toString());
        }
    }


    public void showSortedList(String charSequence)
    {
        List<CommonData>commonData_searched=new ArrayList<>();

        if(!charSequence.equalsIgnoreCase(""))
        {


            for (CommonData data:commonData
            ) {


                try {


                    JSONObject jsonObject = new JSONObject(data.getData());

                    String accountname=jsonObject.getString("Accountname");

                    if (accountname.toLowerCase().contains(charSequence.toLowerCase()) || accountname.toUpperCase().contains(charSequence.toUpperCase()))

                    {
                        commonData_searched.add(data);

                    }





                }catch (Exception e)
                {

                }




            }

            if(commonData_searched.size()>0)
            {

                recycler.setVisibility(View.VISIBLE);

                AccountSettingsAdapter accountSettingsAdapter=new AccountSettingsAdapter(AccountSettingsListActivity.this,commonData_searched);
                recycler.setLayoutManager(new LinearLayoutManager(AccountSettingsListActivity.this));

                recycler.setAdapter(accountSettingsAdapter);



            }
            else {

                recycler.setVisibility(View.GONE);
            }


        }
        else {

            if(commonData.size()>0)
            {


                Collections.sort(commonData, new Comparator<CommonData>() {
                    @Override
                    public int compare(CommonData commonData, CommonData t1) {

                        String acc1="",acc2="";
                        try {


                            JSONObject jsonObject = new JSONObject(commonData.getData());
                            JSONObject jsonObject1 = new JSONObject(t1.getData());


                            acc1=jsonObject.getString("Accountname");
                            acc2=jsonObject1.getString("Accountname");





                        }catch (Exception e)
                        {

                        }



                        return acc1.compareTo(acc2);
                    }
                });







                recycler.setVisibility(View.VISIBLE);
                AccountSettingsAdapter accountSettingsAdapter=new AccountSettingsAdapter(AccountSettingsListActivity.this,commonData);
                recycler.setLayoutManager(new LinearLayoutManager(AccountSettingsListActivity.this));

                recycler.setAdapter(accountSettingsAdapter);



            }


        }

    }
}
