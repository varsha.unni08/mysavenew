package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.DescriptionLangagebaseddata;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;

public class DescriptionActivity extends AppCompatActivity {

    TextView txtHead,txtDescription;

    ImageView imgback;

    WebView txtcontent;

   public static int content=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        getSupportActionBar().hide();

        content=getIntent().getIntExtra(Utils.Contents.Content,0);

        String languagedata = LocaleHelper.getPersistedData(DescriptionActivity.this, "en");
        Context contex= LocaleHelper.setLocale(DescriptionActivity.this, languagedata);

        Resources resources=contex.getResources();

        txtcontent=findViewById(R.id.txtcontent);
        txtDescription=findViewById(R.id.txtDescription);
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


            if(content==Utils.Contents.howtouse)
            {

               // txtcontent.setText(resources.getString(R.string.howtousetxt));
                String text;
                text = "";
                ;
                String item = LocaleHelper.getPersistedData(DescriptionActivity.this, "en");
                Context context= LocaleHelper.setLocale(DescriptionActivity.this, languagedata);
                 resources=context.getResources();



//                if (item.equals("en")) {
//                  text= DescriptionLangagebaseddata.Engdata;
//                }
//
//                if (item.equals("te")) {
//                    text= DescriptionLangagebaseddata.tel;
//                }
//                if (item.equals("en")) {
//                    text= DescriptionLangagebaseddata.Engdata;
//                }
//
//                if (item.equals("mr")) {
//                    text= DescriptionLangagebaseddata.marathi;
//                }
//                if (item.equals("ta")) {
//                    text= DescriptionLangagebaseddata.tamil;
//                }
//
//                if (item.equals("kn")) {
//                    text= DescriptionLangagebaseddata.kn;
//                }
//
//                if (item.equals("hi")) {
//                    text= DescriptionLangagebaseddata.hindidata;
//                }
//
//
//                if (item.equals("ml")) {
//                    text= DescriptionLangagebaseddata.mldata;
//                }

               // Context context = LocaleHelper.setLocale(DescriptionActivity.this, languagedata);

               // resources = context.getResources();

                txtHead.setText(resources.getString(R.string.howtouse));


                txtDescription.setText(resources.getString(R.string.usermanualtxt));





                txtcontent.loadData(text, "text/html", "utf-8");


            }
            else if (content==Utils.Contents.termsandcondition)
            {


                txtHead.setText(resources.getString(R.string.termsandconditions));


                String text;
                text = "<html><body><p align=\"justify\">";
                text+= resources.getString(R.string.termsandconditiontxt);
                text+= "</p></body></html>";
                txtcontent.loadData(text, "text/html", "utf-8");

            }

            else if (content==Utils.Contents.privacypolicy)
            {


                txtHead.setText(resources.getString(R.string.privacypolicy));


                String text;
                text = "<html><body><p align=\"justify\">";
                text+= resources.getString(R.string.privacypolicytxt);
                text+= "</p></body></html>";
                txtcontent.loadData(text, "text/html", "utf-8");

            }

            else if (content==Utils.Contents.aboutus)
            {


                txtHead.setText(resources.getString(R.string.aboutus));


                String text;
                text = "<html><body><p align=\"justify\">";
                text+= resources.getString(R.string.aboutustxt);
                text+= "</p></body></html>";
                txtcontent.loadData(text, "text/html", "utf-8");

            }





    }
}
