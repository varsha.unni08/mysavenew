package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.adapter.PaymentVoucherAdapter;
import com.centroid.integraaccounts.adapter.ReceiptAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ReciptListActivity extends AppCompatActivity {


    TextView txtHead;

    ImageView imgback,imgaacsettings,imgbudget;

    LinearLayout layout_head;



    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtdatepick;
    ImageView imgDatepick;

    Button submit;

    int m=0,yea=0;

    Spinner spinnerAccountName;
    TextView txtTotal;
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    PaymentVoucherAdapter paymentVoucherAdapter;

    List<PaymentVoucher> paymentVouchers_selected=new ArrayList<>();

//    Button btnsetBudget;


    List<CommonData>cmfiltered;


    Resources resources;

    TextView txtdate,txtaccount,txtamount,txttype,txtaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipt_list);
        getSupportActionBar().hide();

        cmfiltered=new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgaacsettings=findViewById(R.id.imgaacsettings);
        imgbudget=findViewById(R.id.imgbudget);
        layout_head=findViewById(R.id.layout_head);




        paymentVouchers_selected=new ArrayList<>();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);

        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);
        submit=findViewById(R.id.submit);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        txtTotal=findViewById(R.id.txtTotal);


        txtdate=findViewById(R.id.txtdate);
        txtaccount=findViewById(R.id.txtaccount);
        txtamount=findViewById(R.id.txtamount);
        txttype=findViewById(R.id.txttype);
        txtaction=findViewById(R.id.txtaction);


        String languagedata = LocaleHelper.getPersistedData(ReciptListActivity.this, "en");
        Context context= LocaleHelper.setLocale(ReciptListActivity.this, languagedata);

        resources=context.getResources();
        submit.setText(resources.getString(R.string.search));
        txtHead.setText(resources.getString(R.string.receipt));
      //  txtbudget.setText(resources.getString(R.string.setbudget));

        txtdate.setText(resources.getString(R.string.date));
        txtaccount.setText(resources.getString(R.string.accountname));
        txtamount.setText(resources.getString(R.string.amount));
        txttype.setText(resources.getString(R.string.cashdata)+"/"+resources.getString(R.string.bank));
        txtaction.setText(resources.getString(R.string.action));

        Utils.showTutorial(resources.getString(R.string.reciptvouchertutorial),ReciptListActivity.this,Utils.Tutorials.reciptvouchertutorial);


        if(new PreferenceHelper(ReciptListActivity.this).getIntData(Utils.trialcompleted)==1)
        {
          //  Toast.makeText(ReciptListActivity.this,Utils.Messages.error,Toast.LENGTH_SHORT).show();



            Utils.showAlertWithSingle(ReciptListActivity.this,Utils.Messages.error, new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });


            fab_addtask.hide();
        }

        if(new PreferenceHelper(ReciptListActivity.this).getIntData(Utils.expirycompleted)==1)
        {
           // Toast.makeText(ReciptListActivity.this,Utils.Messages.expiryerror,Toast.LENGTH_SHORT).show();

            Utils.showAlertWithSingle(ReciptListActivity.this,Utils.Messages.expiryerror, new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });



            fab_addtask.hide();
        }



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        addAccountSettings();
        getReceiptListByDate();


        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthDialog();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthDialog();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(new PreferenceHelper(ReciptListActivity.this).getIntData(Utils.trialwarning)==1)
                {
                   // Toast.makeText(ReciptListActivity.this,Utils.Messages.warning,Toast.LENGTH_SHORT).show();
                    Utils.showAlertWithSingle(ReciptListActivity.this,Utils.Messages.warning, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }

                if(new PreferenceHelper(ReciptListActivity.this).getIntData(Utils.expirywarning)==1)
                {
                 //   Toast.makeText(ReciptListActivity.this,Utils.Messages.expirywarning,Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(ReciptListActivity.this,Utils.Messages.expirywarning, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }

                Intent intent=new Intent(ReciptListActivity.this, AddReceiptActivity.class);
                startActivity(intent);
            }
        });


//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                if(yea!=0||m!=0)
//                {
//
//
//
////                    getReceiptList();
//
//
//                }
//                else {
//
//                    Toast.makeText(ReciptListActivity.this,"Select month and year",Toast.LENGTH_SHORT).show();
//
//                }
//
//
//            }
//        });



    }



    public void getReceiptListByDate()
    {
        Calendar calendar=Calendar.getInstance();

        yea=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);

        m=month;


        txtdatepick.setText(arrmonth[m]+"/"+yea);
        getReceiptList();



    }











    public void addAccountSettings()
    {


        List<CommonData>commonData=new DatabaseHelper(ReciptListActivity.this).getAccountSettingsData();

        if(commonData.size()>0)
        {

            Collections.reverse(commonData);


            for (CommonData cm:commonData) {



                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype= jsonObject.getString("Accounttype");

                    if(acctype.equalsIgnoreCase("Receipt account"))
                    {
                        cmfiltered.add(cm);
                    }


                    //




                }catch (Exception e)
                {

                }

            }


            AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(ReciptListActivity.this, cmfiltered);
            spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);

        }


    }


    private void showMonthDialog() {

        String month="",yearselected="";


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR)-1;
        int m_calender=calendar.get(Calendar.MONTH);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ReciptListActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(ReciptListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);
        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setValue(m_calender);


        npmonth.setDisplayedValues(arrmonth);
        final NumberPicker npyear=dialog.findViewById(R.id.npyear);
        npyear.setMinValue(year);
        npyear.setMaxValue(year+5);


        npyear.setValue(year);
        npyear.setWrapSelectorWheel(true);
        dialog.getWindow().setLayout(width,height);
        npmonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {


            }
        });

        npyear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });





        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                m =  npmonth.getValue();

                yea = npyear.getValue();

                String month=arrmonth[m];

                txtdatepick.setText(month+"/"+yea);
                getReceiptList();

                // getVoucherData();

                //getVoucherData(m,yea);


            }
        });


        dialog.show();





    }


    @Override
    protected void onRestart() {
        super.onRestart();

        getReceiptList();
    }

    public void getReceiptList()
    {

        String monthinarray=arrmonth[m];

        List<Accounts>commonData_selected=new ArrayList<>();

        List<Accounts>commonData=new DatabaseHelper(ReciptListActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.receiptvoucher+"");

        Log.e("RECEIPTTAGG",commonData.size()+"");




        try {

            int selected_month = m + 1;

           // List<PaymentVoucher> paymentVouchers = new DatabaseHelper(ReciptListActivity.this).getPaymentVoucherData();


            double total = 0;

            for (Accounts paymentVoucher : commonData
            ) {

               // JSONObject jsonObject = new JSONObject(paymentVoucher.getData());

                String month = paymentVoucher.getACCOUNTS_month();
                String year1 = paymentVoucher.getACCOUNTS_year();
//                String accountname = jsonObject.getString("accountname");
               String amount = paymentVoucher.getACCOUNTS_amount();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                ;


                if (String.valueOf(selected_month).equalsIgnoreCase(month) && String.valueOf(yea).equalsIgnoreCase(year1)) {

                   // CommonData cm = (CommonData) spinnerAccountName.getSelectedItem();

                  //  if (accountname.equalsIgnoreCase(cm.getId())) {

                    if(entryid.equalsIgnoreCase("0")) {

                        total = total + Double.parseDouble(amount);

                        commonData_selected.add(paymentVoucher);
                    }

                }


            }


            if (commonData_selected.size() > 0) {


                Collections.sort(commonData_selected, new Comparator<Accounts>() {
                    @Override
                    public int compare(Accounts accounts, Accounts t1) {

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate=null;

                        Date endDate=null;
                        try {
                            strDate = sdf.parse(accounts.getACCOUNTS_date());
                            endDate = sdf.parse(t1.getACCOUNTS_date());




                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        return strDate.compareTo(endDate);
                    }
                });



                Collections.reverse(commonData_selected);


                recycler.setVisibility(View.VISIBLE);
                layout_head.setVisibility(View.VISIBLE);

                txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

                recycler.setLayoutManager(new LinearLayoutManager(ReciptListActivity.this));
                recycler.setAdapter(new ReceiptAdapter(ReciptListActivity.this, commonData_selected));


            }
            else {

                txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

                recycler.setVisibility(View.INVISIBLE);
                layout_head.setVisibility(View.GONE);
            }


        }catch (Exception e)
        {

        }

    }
}
