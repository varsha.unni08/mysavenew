package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.PaymentVoucherAdapter;
import com.centroid.integraaccounts.adapter.WalletvoucherAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class WalletListActivity extends AppCompatActivity {

    TextView txtHead,txtbudget,txtWalletbalancehead,txtaction,txtamount;

    ImageView imgback,imgaacsettings,imgbudget;



    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtdatepick;
    ImageView imgDatepick;
    LinearLayout layout_head,layout_date;

    String date_opening="";

    Button submit;

    int m=0,yea=0;
    double t=0;

    Spinner spinnerAccountName;
    TextView txtTotal;
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    WalletvoucherAdapter paymentVoucherAdapter;

    List<Accounts> paymentVouchers_selected=new ArrayList<>();

//    Button btnsetBudget;


    List<CommonData>cmfiltered;

    Resources resources;

    double walletbalance=0;

    TextView txtdate,txtaccount,txttype,txtWalletbalance,txtApplyaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_list);
        getSupportActionBar().hide();
       // getSupportActionBar().hide();
        cmfiltered=new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgaacsettings=findViewById(R.id.imgaacsettings);
        imgbudget=findViewById(R.id.imgbudget);
        layout_head=findViewById(R.id.layout_head);
        txtbudget=findViewById(R.id.txtbudget);
        layout_date=findViewById(R.id.layout_date);



        txtdate=findViewById(R.id.txtdate);
        txtaccount=findViewById(R.id.txtaccount);
        txtamount=findViewById(R.id.txtamount);
        txttype=findViewById(R.id.txttype);
        txtaction=findViewById(R.id.txtaction);
        txtApplyaction=findViewById(R.id.txtApplyaction);
        txtWalletbalancehead=findViewById(R.id.txtWalletbalancehead);



        txtWalletbalance=findViewById(R.id.txtWalletbalance);


        paymentVouchers_selected=new ArrayList<>();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);

        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);
        submit=findViewById(R.id.submit);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        txtTotal=findViewById(R.id.txtTotal);



        String languagedata = LocaleHelper.getPersistedData(WalletListActivity.this, "en");
        Context context= LocaleHelper.setLocale(WalletListActivity.this, languagedata);

        resources=context.getResources();
        submit.setText(resources.getString(R.string.search));
        txtHead.setText(resources.getString(R.string.wallet));
        txtaction.setText(resources.getString(R.string.accountname));
        txtamount.setText(resources.getString(R.string.amount));
        txtdate.setText(resources.getString(R.string.date));
        txtWalletbalancehead.setText(resources.getString(R.string.openingbalance));
        txtApplyaction.setText(resources.getString(R.string.action));
        txtbudget.setText("Add money");

//        txtdate.setText(resources.getString(R.string.date));
//        txtaccount.setText(resources.getString(R.string.debit));
//        txtamount.setText(resources.getString(R.string.amount));
//        txttype.setText(resources.getString(R.string.credit));
//        txtaction.setText(resources.getString(R.string.action));
        Utils.showTutorial(resources.getString(R.string.wallettutorial),WalletListActivity.this,Utils.Tutorials.wallettutorial);


        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthDialog();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthDialog();
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent=new Intent(WalletListActivity.this,WalletActivity.class);
                intent.putExtra("balance",t);

startActivity(intent);

            }
        });

        getDateMonthInitial();

    }


    private void showMonthDialog() {

        String month="",yearselected="";


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR)-1;
        int m_calender=calendar.get(Calendar.MONTH);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WalletListActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(WalletListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);
        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setValue(m_calender);


        npmonth.setDisplayedValues(arrmonth);
        final NumberPicker npyear=dialog.findViewById(R.id.npyear);
        npyear.setMinValue(year);
        npyear.setMaxValue(year+5);


        npyear.setValue(year);
        npyear.setWrapSelectorWheel(true);
        dialog.getWindow().setLayout(width,height);
        npmonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {


            }
        });

        npyear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });





        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                m =  npmonth.getValue();

                yea = npyear.getValue();

                String month=arrmonth[m];

                txtdatepick.setText(month+"/"+yea);
              //  getVoucherData();

                getVoucherData();

                //getVoucherData(m,yea);


            }
        });


        dialog.show();





    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDateMonthInitial();
    }

    private void getDateMonthInitial()
    {

        Calendar calendar=Calendar.getInstance();




        m =  calendar.get(Calendar.MONTH);

        yea = calendar.get(Calendar.YEAR);

        String month=arrmonth[m];

        txtdatepick.setText(month+"/"+yea);

        getVoucherData();

    }


    public void getVoucherData()
    {

        date_opening="";

        layout_date.setVisibility(View.VISIBLE);
        String monthinarray=arrmonth[m];

        paymentVouchers_selected.clear();

        try {

            int selected_month = m + 1;




            List<CommonData>commonData=new DatabaseHelper(WalletListActivity.this).getData(Utils.DBtables.TABLE_WALLET);

            double openingbal=0,totalw=0;
            if(commonData.size()>0) {






                Collections.sort(commonData, new Comparator<CommonData>() {
                    @Override
                    public int compare(CommonData commonData, CommonData t1) {


                        int a=0;
                        try {


                            JSONObject jsonObject = new JSONObject(commonData.getData());
                            JSONObject jsonObject1 = new JSONObject(t1.getData());

                            String date1 = jsonObject.getString("date");
                            String date2 = jsonObject1.getString("date");

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(date1);
                            Date endDate = sdf.parse(date2);

                            a=strDate.compareTo(endDate);



                        }catch (Exception e)
                        {

                        }

                        return a;
                    }
                });


                for (CommonData cm : commonData) {





                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String Walletbalance=jsonObject.getString("Walletbalance");


                    String month_wallet = jsonObject.getString("month");
                    String year_wallet = jsonObject.getString("year");
                    String amount_wallet = jsonObject.getString("amount");

                    String date=jsonObject.getString("date");

                    if(String.valueOf(selected_month).equalsIgnoreCase(month_wallet)&&String.valueOf(yea).equalsIgnoreCase(year_wallet)) {


                        totalw=totalw+Double.parseDouble(amount_wallet);


                        if(date_opening.equalsIgnoreCase("")) {

                            openingbal = Double.parseDouble(amount_wallet);


                            date_opening = date;
                        }

                        Accounts accounts = new Accounts();

                        //entry id is equal to zero in the base of accounts section from account setup
                        accounts.setACCOUNTS_id(Integer.parseInt(cm.getId()));
                        accounts.setACCOUNTS_entryid("0");
                        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.wallet);
                        accounts.setACCOUNTS_date(date);
                        accounts.setACCOUNTS_setupid("0");//setup id is the account setup id from the dropdown
                        accounts.setACCOUNTS_amount(amount_wallet);
                        accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
                        accounts.setACCOUNTS_remarks("");
                        accounts.setACCOUNTS_month(month_wallet);
                        accounts.setACCOUNTS_year(year_wallet);

                        paymentVouchers_selected.add(accounts);
                       // accounts.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
                        //accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account + "");





                    }



                }

            }

            txtWalletbalance.setText(openingbal+" "+resources.getString(R.string.rs));




















            //call get data here

            List<Accounts> paymentVouchers = new DatabaseHelper(WalletListActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.paymentvoucher+"");


            Log.e("TAG",paymentVouchers.size()+"");

            double total=0;

            for (Accounts paymentVoucher : paymentVouchers
            ) {



                String month=paymentVoucher.getACCOUNTS_month();
                String year1=paymentVoucher.getACCOUNTS_year();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                int vouchertype=paymentVoucher.getACCOUNTS_vouchertype();
                // String setupid=paymentVoucher.getACCOUNTS_setupid();
                String amount=paymentVoucher.getACCOUNTS_amount();;
                // CommonData cm=(CommonData) spinnerAccountName.getSelectedItem();

//&&setupid.equalsIgnoreCase(cm.getId())

                if(String.valueOf(selected_month).equalsIgnoreCase(month)&&String.valueOf(yea).equalsIgnoreCase(year1))
                {



                    if(entryid.equalsIgnoreCase("0")) {



                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate = sdf.parse(date_opening);
                        Date endDate = sdf.parse(paymentVoucher.getACCOUNTS_date());

                        if(endDate.after(strDate)||endDate.equals(strDate)) {


                            total = total + Double.parseDouble(amount);
                            paymentVouchers_selected.add(paymentVoucher);



                        }
                    }







                }





            }



           // txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

            //checking budget







            if(paymentVouchers_selected.size()>0) {

                layout_head.setVisibility(View.VISIBLE);


                Collections.sort(paymentVouchers_selected, new Comparator<Accounts>() {
                    @Override
                    public int compare(Accounts accounts, Accounts t1) {

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate=null;

                        Date endDate=null;
                        try {
                            strDate = sdf.parse(accounts.getACCOUNTS_date());
                            endDate = sdf.parse(t1.getACCOUNTS_date());




                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        return strDate.compareTo(endDate);
                    }
                });




                layout_head.setVisibility(View.VISIBLE);
                recycler.setVisibility(View.VISIBLE);
                txtTotal.setVisibility(View.VISIBLE);

                Collections.reverse(paymentVouchers_selected);


                paymentVoucherAdapter = new WalletvoucherAdapter(WalletListActivity.this, paymentVouchers_selected,t);
                recycler.setLayoutManager(new LinearLayoutManager(WalletListActivity.this));
                recycler.setAdapter(paymentVoucherAdapter);

                 t=totalw-total;

                txtTotal.setText(resources.getString(R.string.closingbalance)+" : "+t+" "+resources.getString(R.string.rs));
            }
            else {

                layout_head.setVisibility(View.GONE);
                recycler.setVisibility(View.INVISIBLE);
                txtTotal.setVisibility(View.GONE);
            }




        }catch (Exception e)
        {

        }
    }

}
