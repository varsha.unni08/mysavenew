package com.centroid.integraaccounts.views.customviews;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;

public class DatePickerDialogNoYear extends DatePickerDialog {




    @TargetApi(24)
    public DatePickerDialogNoYear(@NonNull Context context) {


            super(context);

            hideYear();

    }

    @TargetApi(24)
    public DatePickerDialogNoYear(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        hideYear();
    }

    public DatePickerDialogNoYear(@NonNull Context context, @Nullable OnDateSetListener listener, int year, int month, int dayOfMonth) {
        super(context, listener, year, month, dayOfMonth);
        hideYear();
    }

    public DatePickerDialogNoYear(@NonNull Context context, int themeResId, @Nullable OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        super(context, themeResId, listener, year, monthOfYear, dayOfMonth);
        hideYear();
    }


    private void hideYear() {
        try {
            Field f[] = this.getClass().getDeclaredFields();
            for (Field field : f) {
                if (field.getName().equals("mYearPicker")) {
                    field.setAccessible(true);
                    ((View) field.get(this)).setVisibility(View.GONE);
                }
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            Log.d("ERROR", e.getMessage());
        }
    }
}
