package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MyWebLinkAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.List;

public class MyWebLinksActivity extends AppCompatActivity {


    ImageView imgback;
    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtNodata;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_web_links);
        getSupportActionBar().hide();
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        txtNodata=findViewById(R.id.txtNodata);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                final Dialog dialog = new Dialog(MyWebLinksActivity.this);
                dialog.setContentView(R.layout.layout_myweblinks);
                dialog.setTitle("My Save");
                int b=(int)(height/1.3);
                dialog.getWindow().setLayout(width - 30,b );

                EditText edtWeblink=dialog.findViewById(R.id.edtWeblink);
                EditText edtPassword=dialog.findViewById(R.id.edtPassword);
                EditText edtUsername=dialog.findViewById(R.id.edtUsername);
                EditText edtDescription=dialog.findViewById(R.id.edtDescription);
                Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!edtWeblink.getText().toString().equalsIgnoreCase(""))
                        {

//                            if(!edtUsername.getText().toString().equalsIgnoreCase("")) {


//                                if (!edtPassword.getText().toString().equalsIgnoreCase("")) {

                                    if(!edtDescription.getText().toString().equalsIgnoreCase("")) {


                                        try {

                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("WebLink", edtWeblink.getText().toString());
                                            jsonObject.put("Username", edtUsername.getText().toString());
                                            jsonObject.put("Password", edtPassword.getText().toString());
                                            jsonObject.put("Description", edtDescription.getText().toString());

                                            new DatabaseHelper(MyWebLinksActivity.this).addData(Utils.DBtables.TABLE_WEBLINKS, jsonObject.toString());

                                            getWebLinksData();
                                            dialog.dismiss();


                                        } catch (Exception e) {


                                        }

                                    }
                                    else{


                                        Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Description", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });

                                    }


//                                } else {
//
//
//                                    Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Website password", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
//
//                                        }
//                                    });
//                                }

//                            }
//                            else{
//
//                                Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the User Name", new DialogEventListener() {
//                                    @Override
//                                    public void onPositiveButtonClicked() {
//
//                                    }
//
//                                    @Override
//                                    public void onNegativeButtonClicked() {
//
//                                    }
//                                });
//
//
//                            }


                        }
                        else{


                            Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Website link", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });




                        }





                    }
                });






                dialog.show();






            }
        });


        getWebLinksData();
    }





    public void showDialogForEdit(CommonData commonData)
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(MyWebLinksActivity.this);
        dialog.setContentView(R.layout.layout_myweblinks);
        dialog.setTitle("My Save");
        int b=(int)(height/1.3);
        dialog.getWindow().setLayout(width - 30,b );

        EditText edtWeblink=dialog.findViewById(R.id.edtWeblink);
        EditText edtPassword=dialog.findViewById(R.id.edtPassword);
        EditText edtUsername=dialog.findViewById(R.id.edtUsername);
        EditText edtDescription=dialog.findViewById(R.id.edtDescription);
        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setText("Update");


        try{

            JSONObject j=new JSONObject(commonData.getData());
            String WebLink=j.getString("WebLink");
            String Password=j.getString("Password");

            if(j.has("Username")) {

                String Username = j.getString("Username");

                if(!Username.isEmpty())
                {
                    edtUsername.setText(Username);

                }
                else{


                }



            }
            if(j.has("Description")) {
                String Description = j.getString("Description");
                edtDescription.setText(Description);
            }





            edtWeblink.setText(WebLink);

            if(!Password.isEmpty()) {



                edtPassword.setText(Password);
//                holder.layout_password.setVisibility(View.VISIBLE);
            }
            else{

//                holder.layout_password.setVisibility(View.GONE);

            }


        }catch (Exception e)
        {

        }










        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtWeblink.getText().toString().equalsIgnoreCase(""))
                {

//                            if(!edtUsername.getText().toString().equalsIgnoreCase("")) {


//                                if (!edtPassword.getText().toString().equalsIgnoreCase("")) {

                    if(!edtDescription.getText().toString().equalsIgnoreCase("")) {


                        try {

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("WebLink", edtWeblink.getText().toString());
                            jsonObject.put("Username", edtUsername.getText().toString());
                            jsonObject.put("Password", edtPassword.getText().toString());
                            jsonObject.put("Description", edtDescription.getText().toString());

                            new DatabaseHelper(MyWebLinksActivity.this).updateData(commonData.getId(),jsonObject.toString(),   Utils.DBtables.TABLE_WEBLINKS);

                            getWebLinksData();
                            dialog.dismiss();


                        } catch (Exception e) {


                        }

                    }
                    else{


                        Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Description", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }


//                                } else {
//
//
//                                    Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Website password", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
//
//                                        }
//                                    });
//                                }

//                            }
//                            else{
//
//                                Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the User Name", new DialogEventListener() {
//                                    @Override
//                                    public void onPositiveButtonClicked() {
//
//                                    }
//
//                                    @Override
//                                    public void onNegativeButtonClicked() {
//
//                                    }
//                                });
//
//
//                            }


                }
                else{


                    Utils.showAlertWithSingle(MyWebLinksActivity.this, "Enter the Website link", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });




                }





            }
        });






        dialog.show();

    }




  public void   getWebLinksData()
    {

        List<CommonData> commondata= new DatabaseHelper(MyWebLinksActivity.this).getData(Utils.DBtables.TABLE_WEBLINKS);


        if (commondata.size()>0)
        {
            txtNodata.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);

            recycler.setLayoutManager(new LinearLayoutManager(MyWebLinksActivity.this));
            recycler.setAdapter(new MyWebLinkAdapter(MyWebLinksActivity.this,commondata));





        }
        else{

            txtNodata.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);

        }




    }
}