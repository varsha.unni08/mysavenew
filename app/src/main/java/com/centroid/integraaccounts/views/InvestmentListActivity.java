package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.InvestmentAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class InvestmentListActivity extends AppCompatActivity {

    ImageView imgback,imgDelete;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;
    TextView txtTotal,txtprofile,txtNotFound;

    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investment_list);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgDelete=findViewById(R.id.imgDelete);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtTotal=findViewById(R.id.txtTotal);
        txtprofile=findViewById(R.id.txtprofile);
        txtNotFound=findViewById(R.id.txtNotFound);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(InvestmentListActivity.this,AddinvestActivity.class));

            }
        });


        String languagedata = LocaleHelper.getPersistedData(InvestmentListActivity.this, "en");
        Context context= LocaleHelper.setLocale(InvestmentListActivity.this, languagedata);

        resources=context.getResources();


        Utils.showTutorial(resources.getString(R.string.investmenttutorial),InvestmentListActivity.this,Utils.Tutorials.investmenttutorial);

        getInvestments();

        addTitle();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        String languagedata = LocaleHelper.getPersistedData(InvestmentListActivity.this, "en");
        Context context= LocaleHelper.setLocale(InvestmentListActivity.this, languagedata);

        resources=context.getResources();


        getInvestments();

addTitle();



    }


    public void addTitle()
    {



        txtprofile.setText(resources.getString(R.string.investment));
    }







    public void getInvestments()
    {
        List<CommonData>commonData=new DatabaseHelper(InvestmentListActivity.this).getData(Utils.DBtables.INVESTMENT_table);

        if(commonData.size()>0)
        {
            Collections.reverse(commonData);
            double total=0;

            try{

                for (CommonData data:commonData) {

                    JSONObject jsonObject = new JSONObject(data.getData());

                    total = total + Double.parseDouble(jsonObject.getString("amount"));
                    
                }

                txtTotal.setText(resources.getString(R.string.total)+" : "+total+" "+getString(R.string.rs));

            }catch (Exception e)
            {

            }



            recycler.setVisibility(View.VISIBLE);
            txtNotFound.setVisibility(View.GONE);

            InvestmentAdapter investmentAdapter=new InvestmentAdapter(InvestmentListActivity.this,commonData);
            recycler.setLayoutManager(new LinearLayoutManager(InvestmentListActivity.this));
            recycler.setAdapter(investmentAdapter);


        }
        else {

            recycler.setVisibility(View.GONE);
            txtNotFound.setVisibility(View.VISIBLE);

            txtTotal.setText(resources.getString(R.string.total)+" : "+0+" "+getString(R.string.rs));
        }

    }
}
