
package com.centroid.integraaccounts.views.rechargeViews.dth;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Price {

    @SerializedName("amount")
    @Expose
    private Double amount=0.0;
    @SerializedName("validityMonths")
    @Expose
    private Integer validityMonths=0;
    @SerializedName("effectiveMonthlyPrice")
    @Expose
    private Double effectiveMonthlyPrice=0.0;
    @SerializedName("validity")
    @Expose
    private String validity="";
    @SerializedName("ncf")
    @Expose
    private Boolean ncf=false;
    @SerializedName("extraValidityDays")
    @Expose
    private Integer extraValidityDays=0;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getValidityMonths() {
        return validityMonths;
    }

    public void setValidityMonths(Integer validityMonths) {
        this.validityMonths = validityMonths;
    }

    public Double getEffectiveMonthlyPrice() {
        return effectiveMonthlyPrice;
    }

    public void setEffectiveMonthlyPrice(Double effectiveMonthlyPrice) {
        this.effectiveMonthlyPrice = effectiveMonthlyPrice;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public Boolean getNcf() {
        return ncf;
    }

    public void setNcf(Boolean ncf) {
        this.ncf = ncf;
    }

    public Integer getExtraValidityDays() {
        return extraValidityDays;
    }

    public void setExtraValidityDays(Integer extraValidityDays) {
        this.extraValidityDays = extraValidityDays;
    }

}
