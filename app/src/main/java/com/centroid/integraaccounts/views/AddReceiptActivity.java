package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.fragments.SpinnerDialogFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;

public class AddReceiptActivity extends AppCompatActivity {


    ImageView imgback, imgDatepick, imgdownload;
    TextView txtdatepick,txtHead,txtSpinner;
    Spinner spinnerAccountName, spinnerAccounttype, spinnerBankdata;
    EditText edtAmount, edtvoucher, edtremarks;
    Button btnSave,btndelete;

    String date = "", month_selected = "", yearselected = "";

    Accounts paymentVoucher;

    LinearLayout layout_bankdata;
    List<CommonData> cmfiltered = new ArrayList<>();
    List<CommonData> commonDataListfiltered = new ArrayList<>();
    FloatingActionButton fabadd, fabaddbank;
    String bankid = "0", bankname = "", debitaccountid = "0";


    Thread thread=null;

    Handler handler;

    Resources resources;

    int index=0,generatebill=0;
    Accounts accounts=null;

    int forupdate=0;

    RadioButton rbCash,rbBank;

    String newAccount="";

    CommonData commonData_selected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receipt);
        getSupportActionBar().hide();

        cmfiltered=new ArrayList<>();
        generatebill=getIntent().getIntExtra("generate",0);
        forupdate=getIntent().getIntExtra("forupdate",0);

        paymentVoucher=(Accounts) getIntent().getSerializableExtra("receipt");
        handler=new Handler();

        cmfiltered = new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        rbBank=findViewById(R.id.rbBank);
        rbCash=findViewById(R.id.rbCash);

       // paymentVoucher = (Accounts) getIntent().getSerializableExtra("paymentVouchers");
        imgback = findViewById(R.id.imgback);
        imgDatepick = findViewById(R.id.imgDatepick);
        fabadd = findViewById(R.id.fabadd);
        fabaddbank = findViewById(R.id.fabaddbank);
        imgdownload = findViewById(R.id.imgdownload);
        txtdatepick = findViewById(R.id.txtdatepick);
        layout_bankdata = findViewById(R.id.layout_bankdata);
        txtSpinner=findViewById(R.id.txtSpinner);

        spinnerAccountName = findViewById(R.id.spinnerAccountName);
        spinnerAccounttype = findViewById(R.id.spinnerAccounttype);
        spinnerBankdata = findViewById(R.id.spinnerBankdata);
        edtAmount = findViewById(R.id.edtAmount);
        btndelete=findViewById(R.id.btndelete);

        edtvoucher = findViewById(R.id.edtvoucher);
        edtremarks = findViewById(R.id.edtremarks);
        btnSave = findViewById(R.id.btnSave);
       // btndownloadReceipt=findViewById(R.id.btndownloadReceipt);


        String languagedata = LocaleHelper.getPersistedData(AddReceiptActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddReceiptActivity.this, languagedata);

        resources=context.getResources();
        edtAmount.setHint(resources.getString(R.string.amount));
        edtremarks.setHint(resources.getString(R.string.enterremarks));
        txtdatepick.setText(resources.getString(R.string.selectdate));
        btnSave.setText(resources.getString(R.string.save));

        txtHead.setText(Utils.getCapsSentences(context,resources.getString(R.string.addreceiptvoucher)));

        txtSpinner.setText(resources.getString(R.string.selectanaccount));
        rbBank.setText(resources.getString(R.string.bank));
        rbCash.setText(resources.getString(R.string.cashreal));

        String arr[]=resources.getStringArray(R.array.paymentacc);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerAccounttype.setAdapter(spinnerArrayAdapter);




        addBankData();


        rbBank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b) {
                    index = 1;

                    addBankData();
                }
            }
        });


        rbCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    index = 0;
                    addBankData();
                }
            }
        });
        layout_bankdata.setVisibility(View.VISIBLE);


        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SpinnerDialogFragment spinnerDialogFragment=new SpinnerDialogFragment(false,new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {

                        try {

                            commonData_selected=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = jcmn1.getString("Accountname");

                            txtSpinner.setText(Utils.getCapsSentences(AddReceiptActivity.this,acctype));


                        }catch (Exception e)
                        {

                        }


                    }
                });


                spinnerDialogFragment.show(getSupportFragmentManager(),"skjdf");
            }
        });




        addAccountSettings();

        setDate();
        //addAccountSettings();


        Executor executor=new DirectExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {



                setEditMode();

            }
        });

        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(AddReceiptActivity.this, AccountsettingsActivity.class);
                i.putExtra(Utils.Requestcode.AccVoucherAll,Utils.Requestcode.ForAccountSettingswithoutRequestcode);



                startActivityForResult(i,Utils.Requestcode.ForAccountSettingswithoutRequestcode);
            }
        });




        fabaddbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  Toast.makeText(AddReceiptActivity.this, "Add account setup in Bank account category", Toast.LENGTH_SHORT).show();

                Intent i=new Intent(AddReceiptActivity.this, AccountsettingsActivity.class);

                if(index==1) {
                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsBankRequestcode);

                }
                else if(index==0) {

                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsCashRequestcode);

                }

                startActivityForResult(i,Utils.Requestcode.ForAccountSettingsBankRequestcode);

            }
        });

//        imgdownload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });




//        spinnerAccounttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                index=i;
//                if (i==0) {
//                    layout_bankdata.setVisibility(View.VISIBLE);
//
//                } else if (i==1) {
//                    layout_bankdata.setVisibility(View.VISIBLE);
//                   // index=1;
//
//                    //  addBankData();
//                }
//                addBankData();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });



        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(ContextCompat.checkSelfPermission(AddReceiptActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {


                    ActivityCompat.requestPermissions(AddReceiptActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
                }
                else {


                    showReceiptVoucherPdf(paymentVoucher);
                }
            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });


        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(paymentVoucher!=null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddReceiptActivity.this);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                           // new DatabaseHelper(AddReceiptActivity.this).deleteReceiptData(paymentVoucher.getId());
                            Executor executor=new DirectExecutor();

                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    //  updateData();


                                    try {


                                        if(paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash+"")) {

                                            new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(debitaccountid + "");

                                            //  new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                                        }

                                        else  if(paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.bank+"")) {


                                            //  new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                                            new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(debitaccountid + "");
                                        }
                                        else {

                                            // new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            List<Accounts>accounts2=new DatabaseHelper(AddReceiptActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id()+"");


                                            if(accounts2.size()>0) {

                                                new DatabaseHelper(AddReceiptActivity.this).deleteAccountDataById(accounts2.get(0).getACCOUNTS_id()+"");

                                            }

                                        }



                                        onBackPressed();

                                    }catch (Exception e)
                                    {

                                    }







                                }
                            });





                            //onBackPressed();

                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();
                }

            }
        });






        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                if(!date.equalsIgnoreCase(""))
                {


                    if(!edtAmount.getText().toString().equalsIgnoreCase(""))
                    {



                            try {

                                if(paymentVoucher!=null&&generatebill!=0)
                                {






                                    Executor executor=new DirectExecutor();

                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {

                                            String Accounttype="";
                                            try{
                                                JSONObject jsonObject=new JSONObject(commonData_selected.getData());

                                                Accounttype=jsonObject.getString("Accounttype");



                                            }catch (Exception e)
                                            {

                                            }


                                            if(Accounttype.compareTo("Income account")==0)
                                            {

                                                    addData();

                                                Utils.showReciptAlert(context, "Hi, Save something...  Do not save what is left after spending, but spend what is left after saving.", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                        Intent i=new Intent(AddReceiptActivity.this,AddPaymentVoucherActivity.class);
                                                        i.putExtra("fromReceipt",1);
                                                        startActivity(i);

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });


                                            }
                                            else {

                                                addData();
                                            }
                                        }
                                    });



                                }

                             else    if (paymentVoucher != null&&forupdate==1) {



                                    Executor executor=new DirectExecutor();

                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {

                                            String Accounttype="";
                                            try{
                                                JSONObject jsonObject=new JSONObject(commonData_selected.getData());

                                                Accounttype=jsonObject.getString("Accounttype");



                                            }catch (Exception e)
                                            {

                                            }


                                            if(Accounttype.compareTo("Income account")==0)
                                            {

                                                updateData();

                                                Utils.showReciptAlert(context, "Hi, Save something...  Do not save what is left after spending, but spend what is left after saving.", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                        Intent i=new Intent(AddReceiptActivity.this,AddPaymentVoucherActivity.class);
                                                        i.putExtra("fromReceipt",1);
                                                        startActivity(i);

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });


                                            }
                                            else {

                                                updateData();
                                            }



                                        }
                                    });


                                } else {

                                    Executor executor=new DirectExecutor();

                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {

                                            String Accounttype="";
                                            try{
                                                JSONObject jsonObject=new JSONObject(commonData_selected.getData());

                                                Accounttype=jsonObject.getString("Accounttype");



                                            }catch (Exception e)
                                            {

                                            }


                                            if(Accounttype.compareTo("Income account")==0)
                                            {

                                                addData();
                                                Utils.showReciptAlert(context, "Hi, Save something...  Do not save what is left after spending, but spend what is left after saving.", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                        Intent i=new Intent(AddReceiptActivity.this,AddPaymentVoucherActivity.class);
                                                        i.putExtra("fromReceipt",1);
                                                        startActivity(i);
                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });


                                            }
                                            else {

                                                addData();
                                            }




                                           // addData();
                                        }
                                    });


                                    

                                }


                                }catch(Exception e)
                                {


                                }















                    }
                    else {

                   //     Toast.makeText(AddReceiptActivity.this,"Enter amount",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(context, "Enter amount", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }



                }
                else {

                  //  Toast.makeText(AddReceiptActivity.this,"Enter date",Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(context, "Enter date", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }



            }
        });





    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==Utils.Requestcode.ForAccountSettingswithoutRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addAccountSettings();

        }
        else   if(requestCode==Utils.Requestcode.ForAccountSettingsBankRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addBankData();

        }
        else   if(requestCode==Utils.Requestcode.ForAccountSettingsCashRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addBankData();

        }



    }
    
    public void updateData()
    {







        CommonData cm = commonData_selected;

        CommonData cm_bank = (CommonData) spinnerBankdata.getSelectedItem();

        if(cm_bank!=null) {
            accounts = new Accounts();

            accounts.setACCOUNTS_entryid("0");
            accounts.setACCOUNTS_date(date);
            accounts.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
            accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
            accounts.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");
            accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts.setACCOUNTS_month(month_selected);
            accounts.setACCOUNTS_year(yearselected);
            accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account + "");

            if (generatebill != 0) {
                accounts.setACCOUNTS_billId(paymentVoucher.getACCOUNTS_id());
            }

            new DatabaseHelper(AddReceiptActivity.this).updateAccountsData(debitaccountid + "", accounts);

            accounts.setACCOUNTS_id(Integer.parseInt(debitaccountid));
            Utils.playSimpleTone(AddReceiptActivity.this);



            if (index == 0) {

                Accounts accounts1 = new Accounts();

                accounts1.setACCOUNTS_entryid(debitaccountid + "");
                accounts1.setACCOUNTS_date(date);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.cash + "");

                    List<Accounts> accounts2 = new DatabaseHelper(AddReceiptActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");


                    if (accounts2.size() > 0) {

                        new DatabaseHelper(AddReceiptActivity.this).updateAccountsData(accounts2.get(0).getACCOUNTS_id() + "", accounts1);
                        Utils.playSimpleTone(AddReceiptActivity.this);
                    }



            } else {

                Accounts accounts1 = new Accounts();

                accounts1.setACCOUNTS_entryid(debitaccountid + "");
                accounts1.setACCOUNTS_date(date);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank + "");

                    List<Accounts> accounts2 = new DatabaseHelper(AddReceiptActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");


                    if (accounts2.size() > 0) {

                        new DatabaseHelper(AddReceiptActivity.this).updateAccountsData(accounts2.get(0).getACCOUNTS_id() + "", accounts1);
                        Utils.playSimpleTone(AddReceiptActivity.this);
                    }
//                }


            }


            date = "";
            month_selected = "";
            yearselected = "";
            spinnerAccountName.setSelection(0);
            edtAmount.setText("");
            edtvoucher.setText("");
            edtremarks.setText("");
            spinnerAccounttype.setSelection(0);
            rbCash.setChecked(true);
            imgdownload.setVisibility(View.VISIBLE);
            paymentVoucher = accounts;
            // onBackPressed();
        }
        else {

           // Toast.makeText(AddReceiptActivity.this,"Please select any cash/bank account head",Toast.LENGTH_SHORT).show();

            Utils.showAlertWithSingle(AddReceiptActivity.this, "Please select any cash/bank account head", new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });


        }













    }
    
    
    
    
    
    
    
    
    
    
    



    public void addData()
    {

        CommonData cm = commonData_selected;

        CommonData cm_bank = (CommonData) spinnerBankdata.getSelectedItem();

        if(cm_bank!=null) {





            accounts = new Accounts();

            //entry id is equal to zero in the base of accounts section from account setup
            accounts.setACCOUNTS_entryid("0");
            accounts.setACCOUNTS_date(date);
            accounts.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
            accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
            accounts.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");
            accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts.setACCOUNTS_month(month_selected);
            accounts.setACCOUNTS_year(yearselected);
            accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account + "");

            if (generatebill == 1) {
                accounts.setACCOUNTS_billId(paymentVoucher.getACCOUNTS_id());
            }
            // else {

            //  generatebill=-1;
            //   }


            long insertid = new DatabaseHelper(AddReceiptActivity.this).addAccountsData(accounts);
            Utils.playSimpleTone(AddReceiptActivity.this);
            accounts.setACCOUNTS_id(Integer.parseInt(insertid + ""));

            paymentVoucher = accounts;

            if (index == 0) {


                Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
                accounts1.setACCOUNTS_entryid(insertid + "");
                accounts1.setACCOUNTS_date(date);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
                //setupid must be zero in the base of cash
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.cash + "");

                new DatabaseHelper(AddReceiptActivity.this).addAccountsData(accounts1);
                Utils.playSimpleTone(AddReceiptActivity.this);

            } else {

////bank section
                Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
                accounts1.setACCOUNTS_entryid(insertid + "");
                accounts1.setACCOUNTS_date(date);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.receiptvoucher);
                //setupid must be bank account id
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank + "");

                new DatabaseHelper(AddReceiptActivity.this).addAccountsData(accounts1);
                Utils.playSimpleTone(AddReceiptActivity.this);

            }


            date = "";
            month_selected = "";
            yearselected = "";
            spinnerAccountName.setSelection(0);
            edtAmount.setText("");
            edtvoucher.setText("");
            edtremarks.setText("");
            txtdatepick.setText(resources.getString(R.string.selectdate));
            spinnerAccounttype.setSelection(0);
            rbCash.setChecked(true);
            setDate();

            // if(paymentVoucher!=null&&generatebill!=0) {


            imgdownload.setVisibility(View.VISIBLE);


            //  }

        }
        else {

         //   Toast.makeText(AddReceiptActivity.this,"Please select any cash/bank account head",Toast.LENGTH_SHORT).show();
            Utils.showAlertWithSingle(AddReceiptActivity.this, "Please select any cash/bank account head", new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });
        }

    }



    public void showReceiptVoucherPdf(Accounts payment)
    {

        try {

           // LayoutInflater layoutInflater=(LayoutInflater) AddReceiptActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final Dialog card=new Dialog(AddReceiptActivity.this);
            card.setContentView(R.layout.layout_amount);

            LinearLayout layout=card.findViewById(R.id.layoutpdf);




            TextView txtheade=card.findViewById(R.id.txtheade);
            TextView txtDatehead=card.findViewById(R.id.txtDatehead);
            TextView txtDate=card.findViewById(R.id.txtDate);
            TextView txtcustomerhead=card.findViewById(R.id.txtcustomerhead);
            TextView txtcustomer=card.findViewById(R.id.txtcustomer);
            TextView txtamounthead=card.findViewById(R.id.txtamounthead);
            TextView txtAmount=card.findViewById(R.id.txtAmount);
            TextView txtremarkshead=card.findViewById(R.id.txtremarkshead);
            TextView txtremarks=card.findViewById(R.id.txtremarks);

            final Button btnDownload=card.findViewById(R.id.btnDownload);

            txtheade.setText(resources.getString(R.string.receipt));
            txtDatehead.setText(resources.getString(R.string.date));
            txtremarkshead.setText(resources.getString(R.string.remarks));

            txtDate.setText(payment.getACCOUNTS_date()+"");
            txtcustomerhead.setText(resources.getString(R.string.customer));
            txtamounthead.setText(resources.getString(R.string.amount));
            txtAmount.setText(resources.getString(R.string.rs)+" "+payment.getACCOUNTS_amount());

            txtremarks.setText(payment.getACCOUNTS_remarks());


                String debitsetupid = "";
//
//                if (accounts.size() > 0) {

                    // debitaccountid=accounts.get(0).getACCOUNTS_id()+"";
                    debitsetupid = paymentVoucher.getACCOUNTS_setupid();
               // }


                List<CommonData> commonDatalist = new DatabaseHelper(AddReceiptActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                for (int i = 0; i < commonDatalist.size(); i++) {


                    if (commonDatalist.get(i).getId().equalsIgnoreCase(debitsetupid)) {


                        JSONObject jsonObject = new JSONObject(commonDatalist.get(i).getData());

                        txtcustomer.setText(jsonObject.getString("Accountname"));

                        break;

                    }

                }



          //  }


            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    btnDownload.setVisibility(View.GONE);

                    card.dismiss();

                    try {
                        String permission="";

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                            permission=Manifest.permission.READ_MEDIA_IMAGES;

                        }
                        else{

                            permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                        }

                        if (ContextCompat.checkSelfPermission(AddReceiptActivity.this,permission) == PackageManager.PERMISSION_GRANTED) {

                            DisplayMetrics metrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(metrics);
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;

                            View v1 = card.getWindow().getDecorView().getRootView();


                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);


                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();

                            File fp = new File(AddReceiptActivity.this.getExternalCacheDir() + "/Save/Receipt");


                            if (!fp.exists()) {
                                fp.mkdirs();
                            }

                            File f = new File(fp.getAbsolutePath() + "/recipt_" + ts + ".png");
                            if (!f.exists()) {
                                f.createNewFile();

                            } else {

                                f.delete();
                                f.createNewFile();
                            }


                            FileOutputStream output = new FileOutputStream(f);


                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                            output.close();


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                Uri photoURI = FileProvider.getUriForFile(AddReceiptActivity.this, getApplicationContext().getPackageName() + ".provider", f);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.setType("*/*");
                                intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                                startActivity(Intent.createChooser(intent, "Share Reciept"));

                            } else {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.setType("*/*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                                startActivity(Intent.createChooser(intent, "Share Reciept"));
                            }

                        }
                    }

                    catch (Exception e)
                    {

                    }

                    onBackPressed();

                }
            });


            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int heightPixels = metrics.heightPixels;
            int widthPixels = metrics.widthPixels;

            card.getWindow().setLayout(widthPixels,heightPixels);


            card.show();


           // onBackPressed();


        }catch (Exception e)
        {

            Log.e("EXCEPTION TAG",e.toString());
        }

    }



    @Override
    protected void onRestart() {
        super.onRestart();

        addBankData();
    }

    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);
    }

    public void addAccountSettings()
    {


        List<CommonData>commonData=new DatabaseHelper(AddReceiptActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if(commonData.size()>0)
        {

            Collections.sort(commonData, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a=0;

                    try{



                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype= jcmn1.getString("Accountname");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1= j1.getString("Accountname");

                        a=     acctype.compareToIgnoreCase(acctypej1);

                    }
                    catch (Exception e)
                    {

                    }





                    return a;
                }
            });


            for (CommonData cm:commonData) {



                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype= jsonObject.getString("Accounttype");

//                    if(acctype.equalsIgnoreCase("Income account"))
//                    {
                        cmfiltered.add(cm);
                  //  }


                    //




                }catch (Exception e)
                {

                }

            }


            AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddReceiptActivity.this, cmfiltered);
            spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);

            if(!newAccount.equalsIgnoreCase("")) {

                for (int i = 0; i < cmfiltered.size(); i++) {
                    try {
                        JSONObject jcmn1 = new JSONObject(cmfiltered.get(i).getData());
                        String acctype= jcmn1.getString("Accountname");

                        if(acctype.trim().equalsIgnoreCase(newAccount.trim()))
                        {

                            spinnerAccountName.setSelection(i);
                            break;
                        }



                    } catch (Exception e) {

                    }


                }

            }


        }


    }


    public void addBankData()
    {

        if(index==0)
        {

            commonDataListfiltered.clear();

            List<CommonData> commonDataList = new DatabaseHelper(AddReceiptActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


            try {

                if (commonDataList.size() > 0) {

                    for (CommonData commonData : commonDataList) {


                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        if (jsonObject.getString("Accounttype").equalsIgnoreCase("Cash")) {
                            // holder.txtmonth.setText(jsonObject.getString("Type"));

                            commonDataListfiltered.add(commonData);
                        }

                    }

                }


               // if (commonDataListfiltered.size() > 0) {

                    BankSpinnerAdapter bankSpinnerAdapter = new BankSpinnerAdapter(AddReceiptActivity.this, commonDataListfiltered);
                    spinnerBankdata.setAdapter(bankSpinnerAdapter);
                    for (int i = 0; i < commonDataListfiltered.size(); i++) {

                        if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                            spinnerBankdata.setSelection(i);
                            break;
                        }

                    }

              //  } else {

                    //     layout_bankdata.setVisibility(View.GONE);


                    // Toast.makeText(AddReceiptActivity.this,"No Data Found",Toast.LENGTH_SHORT).show();


              //  }



                if(!newAccount.equalsIgnoreCase("")) {

                    for (int i = 0; i < commonDataListfiltered.size(); i++) {
                        try {
                            JSONObject jcmn1 = new JSONObject(commonDataListfiltered.get(i).getData());
                            String acctype= jcmn1.getString("Accountname");

                            if(acctype.trim().equalsIgnoreCase(newAccount.trim()))
                            {

                                spinnerBankdata.setSelection(i);
                                break;
                            }



                        } catch (Exception e) {

                        }


                    }

                }


            } catch (Exception e) {

            }


        }

        else if(index==1) {

            commonDataListfiltered.clear();

            List<CommonData> commonDataList = new DatabaseHelper(AddReceiptActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


            try {

                if (commonDataList.size() > 0) {

                    for (CommonData commonData : commonDataList) {


                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        if (jsonObject.getString("Accounttype").equalsIgnoreCase("Bank")) {
                            // holder.txtmonth.setText(jsonObject.getString("Type"));

                            commonDataListfiltered.add(commonData);
                        }

                    }

                }


               // if (commonDataListfiltered.size() > 0) {

                    BankSpinnerAdapter bankSpinnerAdapter = new BankSpinnerAdapter(AddReceiptActivity.this, commonDataListfiltered);
                    spinnerBankdata.setAdapter(bankSpinnerAdapter);

                    for (int i = 0; i < commonDataListfiltered.size(); i++) {

                        if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                            spinnerBankdata.setSelection(i);
                            break;
                        }

                    }
                if(!newAccount.equalsIgnoreCase("")) {

                    for (int i = 0; i < commonDataListfiltered.size(); i++) {
                        try {
                            JSONObject jcmn1 = new JSONObject(commonDataListfiltered.get(i).getData());
                            String acctype= jcmn1.getString("Accountname");

                            if(acctype.trim().equalsIgnoreCase(newAccount.trim()))
                            {

                                spinnerBankdata.setSelection(i);
                                break;
                            }



                        } catch (Exception e) {

                        }


                    }

                }



//                } else {
//
//
//
//                }

            } catch (Exception e) {

            }

        }




    }

    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddReceiptActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;
                month_selected=m+"";
                yearselected=i+"";

                date=i2+"-"+m+"-"+i;

                txtdatepick.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }



    public void setEditMode() {

        if (paymentVoucher != null) {

            try {

                if(generatebill!=0)
                {
                    txtHead.setText(resources.getString(R.string.receipt));
                    btnSave.setText(resources.getString(R.string.save));
                }
                else {

                    txtHead.setText(resources.getString(R.string.editreciptvoucher));
                    btndelete.setVisibility(View.VISIBLE);

                    btnSave.setText(resources.getString(R.string.update));
                }



                btndelete.setText(resources.getString(R.string.delete));

                String month = paymentVoucher.getACCOUNTS_month();
                String year1 = paymentVoucher.getACCOUNTS_year();
                String dat = paymentVoucher.getACCOUNTS_date();
                String amount = paymentVoucher.getACCOUNTS_amount();
                String accountname = paymentVoucher.getACCOUNTS_setupid();
                String accounttype = paymentVoucher.getACCOUNTS_type();
                String remarks = paymentVoucher.getACCOUNTS_remarks();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                String cashbank=paymentVoucher.getACCOUNTS_cashbanktype();



                month_selected = month;
                yearselected = year1;
                date = dat;

                txtdatepick.setText(date);
                edtAmount.setText(amount);

                edtremarks.setText(remarks);



                if (cashbank.equalsIgnoreCase(Utils.CashBanktype.bank+"")) {

                    List<Accounts> cmd = new DatabaseHelper(AddReceiptActivity.this).getAccountsDataBYid(paymentVoucher.getACCOUNTS_entryid());
                    if (cmd.size() > 0) {
                        accountname = cmd.get(0).getACCOUNTS_setupid();
                        debitaccountid = cmd.get(0).getACCOUNTS_id() + "";
                    }

                    spinnerAccounttype.setSelection(1);


                    rbCash.setChecked(false);
                    rbBank.setChecked(true);

                    bankid = paymentVoucher.getACCOUNTS_setupid();


                }
                else if(cashbank.equalsIgnoreCase(Utils.CashBanktype.account+"")) {

                    List<Accounts> bankdata = new DatabaseHelper(AddReceiptActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");
                    debitaccountid = paymentVoucher.getACCOUNTS_id() + "";
                    accountname = paymentVoucher.getACCOUNTS_setupid();

                    if (bankdata.size() > 0) {
                        accountname = paymentVoucher.getACCOUNTS_setupid();
                        debitaccountid = paymentVoucher.getACCOUNTS_id() + "";

                        spinnerAccounttype.setSelection(1);

                        rbCash.setChecked(false);
                        rbBank.setChecked(true);

                        bankid = bankdata.get(0).getACCOUNTS_setupid();

                        if(bankdata.get(0).getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash+""))
                        {
                            spinnerAccounttype.setSelection(0);

                            rbCash.setChecked(true);
                            rbBank.setChecked(false);
                        }
                        else {

                            spinnerAccounttype.setSelection(1);
                            rbCash.setChecked(false);
                            rbBank.setChecked(true);
                        }



                    }

                    else {

                        spinnerAccounttype.setSelection(0);

                        rbCash.setChecked(true);
                        rbBank.setChecked(false);
                    }
                }

                else if(cashbank.equalsIgnoreCase(Utils.CashBanktype.cash+"")) {

                    List<Accounts> cmd = new DatabaseHelper(AddReceiptActivity.this).getAccountsDataBYid(paymentVoucher.getACCOUNTS_entryid());
                    if (cmd.size() > 0) {
                        accountname = cmd.get(0).getACCOUNTS_setupid();
                        debitaccountid = cmd.get(0).getACCOUNTS_id() + "";
                    }

                    spinnerAccounttype.setSelection(0);
                    rbCash.setChecked(true);
                    rbBank.setChecked(false);

                    bankid = "0";

                }

                CommonData cmselected =null;


                        List<CommonData> cm = new DatabaseHelper(AddReceiptActivity.this).getDataByID(accountname, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

             cmselected=  cm.get(0);



                if(generatebill!=0)
                {



                    List<Accounts>  acc1=new DatabaseHelper(AddReceiptActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id()+"");

                    if(acc1.size()>0)
                    {

                        String setupid=acc1.get(0).getACCOUNTS_setupid();

                        List<CommonData> commonDa = new DatabaseHelper(AddReceiptActivity.this).getDataByID(setupid, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                        if (commonDa.size() > 0) {

                            cmselected=  commonDa.get(0);

//                            JSONObject jso = new JSONObject(commonDa.get(0).getData());
//
//
//                            holder.txtaccount.setText(jso.getString("Accountname"));

                        }

                    }



                }











                Log.e("Paymentvoucherid",paymentVoucher.getACCOUNTS_id()+"");
                Log.e("debitaccountid",debitaccountid+"");






                for (int i = 0; i < cmfiltered.size(); i++) {
                    if (cmfiltered.get(i).getId().equalsIgnoreCase(cmselected.getId())) {

                        commonData_selected=cmfiltered.get(i);

                        try {


                            JSONObject jcmn1 = new JSONObject(commonData_selected.getData());
                            String acctype = jcmn1.getString("Accountname");
                            txtSpinner.setText(acctype);

                        }catch (Exception e)
                        {

                        }



                        break;
                    }
                }











                for (int i = 0; i < commonDataListfiltered.size(); i++) {

                    if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                        spinnerBankdata.setSelection(i);
                        break;
                    }

                }

















            } catch (Exception e) {

            }


        }
    }

}
