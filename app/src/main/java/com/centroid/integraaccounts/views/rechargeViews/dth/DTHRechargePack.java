
package com.centroid.integraaccounts.views.rechargeViews.dth;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DTHRechargePack {

    @SerializedName("status")
    @Expose
    private String status="";
    @SerializedName("packs")
    @Expose
    private List<Pack> packs=new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Pack> getPacks() {
        return packs;
    }

    public void setPacks(List<Pack> packs) {
        this.packs = packs;
    }

}
