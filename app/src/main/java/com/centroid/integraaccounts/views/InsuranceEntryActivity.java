package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.receivers.DateTimeReceiver;
import com.centroid.integraaccounts.views.customviews.DatePickerDialogNoYear;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsAssetRequestcode;
import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsInsuranceRequestcode;

public class InsuranceEntryActivity extends AppCompatActivity {

    ImageView imgback,imgdop,imgCloseDate;

    TextView txtdop,txtclosedate;

    Spinner splicnumber;

    EditText edtAmount,edtremarks,edtpremiumAmount;

    Spinner spinnerinsurancetype;

    String closedate="",dateofpayment="";

    Button btnSave;

    CommonData commonData;

    TextView txtHead;

    String insuranceno="";
    FloatingActionButton fabadd;

    Resources resources;

    String newaccount="";

    List<String>taskid;



    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_entry);
        getSupportActionBar().hide();

        commonData=(CommonData) getIntent().getSerializableExtra("insurance");
        taskid=new ArrayList<>();

        imgback=findViewById(R.id.imgback);
        btnSave=findViewById(R.id.btnSave);
        txtHead=findViewById(R.id.txtHead);
        fabadd=findViewById(R.id.fabadd);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        splicnumber=findViewById(R.id.splicnumber);
        edtAmount=findViewById(R.id.edtAmount);
        edtremarks=findViewById(R.id.edtremarks);
        edtpremiumAmount=findViewById(R.id.edtpremiumAmount);

        spinnerinsurancetype=findViewById(R.id.spinnerinsurancetype);
        txtdop=findViewById(R.id.txtdop);
        txtclosedate=findViewById(R.id.txtclosedate);

        imgdop=findViewById(R.id.imgdop);
        imgCloseDate=findViewById(R.id.imgCloseDate);




        String languagedata = LocaleHelper.getPersistedData(InsuranceEntryActivity.this, "en");
        Context context= LocaleHelper.setLocale(InsuranceEntryActivity.this, languagedata);

        resources=context.getResources();
        edtremarks.setHint(resources.getString(R.string.enterremarks));
        txtclosedate.setText(resources.getString(R.string.selectclosingdate));
        txtdop.setText(resources.getString(R.string.dtofpayment));
        btnSave.setText(resources.getString(R.string.save));
        edtAmount.setHint(resources.getString(R.string.amount));
        edtpremiumAmount.setHint(Utils.getCapsSentences(InsuranceEntryActivity.this,resources.getString(R.string.premiumamount)));
        txtHead.setText(resources.getString(R.string.insuranceentry));

        String arr[]=resources.getStringArray(R.array.insurancetype);

       // Spinner spinner = new Spinner(this);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arr); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerinsurancetype.setAdapter(spinnerArrayAdapter);









        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  Toast.makeText(InsuranceEntryActivity.this,resources.getString(R.string.accsettinginsurance),Toast.LENGTH_SHORT).show();

               // startActivity(new Intent(InsuranceEntryActivity.this, AccountsettingsActivity.class));

                Intent i= new Intent(InsuranceEntryActivity.this, AccountsettingsActivity.class);
                i.putExtra(Utils.Requestcode.AccVoucherAll,ForAccountSettingsInsuranceRequestcode);
                startActivityForResult(i,ForAccountSettingsInsuranceRequestcode);
            }
        });

        //setSuggestions();


        if(commonData!=null)
        {

            try{



                btnSave.setText(resources.getString(R.string.update));


                JSONObject jsonObject=new JSONObject(commonData.getData());


                String insurance_no= jsonObject.getString("insurance_no");

                String amount= jsonObject.getString("amount");
                String type=jsonObject.getString("type");

                String dateofpat= jsonObject.getString("dateofpayment");

                String close_date= jsonObject.getString("close_date");
                String remarks=jsonObject.getString("remarks");

                if(jsonObject.has("premium_amount"))
                {
                    String premium_amount=jsonObject.getString("premium_amount");
                    edtpremiumAmount.setText(premium_amount);

                }


                edtremarks.setText(remarks);
                insuranceno=insurance_no;
               // edtlicnumber.setText();
                edtAmount.setText(amount);

                closedate=close_date;
                txtclosedate.setText(closedate);

                String dop[]=dateofpat.split("/");

                dateofpayment=dateofpat;

                txtdop.setText(dop[0]+"/"+arrmonth[Integer.parseInt(dop[1])]);


                String typearr[]=resources.getStringArray(R.array.insurancetype);

                for (int i=0;i<typearr.length;i++)
                {
                    if(type.equalsIgnoreCase(typearr[i]))
                    {
                        spinnerinsurancetype.setSelection(i);
                        break;
                    }
                }



                String taskarray=jsonObject.getString("Task");

                if(!taskarray.equalsIgnoreCase(""))
                {
                    JSONArray jsonArray=new JSONArray(taskarray);

                    for (int i=0;i<jsonArray.length();i++)
                    {

                        JSONObject jsonObj=jsonArray.getJSONObject(i);

                        String Taskid=jsonObj.getString("Taskid");

                        taskid.add(Taskid);

                    }



                }




                // npmonth.getValue()+"/"+npyear.getValue()



            }catch (Exception e)
            {

            }



        }








        txtclosedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showCloseDatePickerDialog();

            }
        });

        imgCloseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showCloseDatePickerDialog();

            }
        });


        txtdop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showMonthYear();

            }
        });

        imgdop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showMonthYear();

            }
        });
        getInsuranceData();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                try {

                    for (String tid:taskid)
                    {

                        new DatabaseHelper(InsuranceEntryActivity.this).deleteData(tid,Utils.DBtables.TABLE_TASK);
                        Utils.playSimpleTone(InsuranceEntryActivity.this);
                    }

                    if (!edtAmount.getText().toString().equalsIgnoreCase("")) {
                        if (!edtpremiumAmount.getText().toString().equalsIgnoreCase("")) {

                            if (!spinnerinsurancetype.getSelectedItem().toString().equalsIgnoreCase("Select Insurance type")) {

                                if (!dateofpayment.equalsIgnoreCase("")) {


                                    if (!closedate.equalsIgnoreCase("")) {

//


                                        //  if (!edtremarks.getText().toString().equalsIgnoreCase("")) {


                                        try {


                                            String id = ((CommonData) splicnumber.getSelectedItem()).getId();

                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("insurance_no", id);
                                            jsonObject.put("amount", edtAmount.getText().toString());
                                            jsonObject.put("type", spinnerinsurancetype.getSelectedItem().toString());
                                            jsonObject.put("dateofpayment", dateofpayment);
                                            jsonObject.put("close_date", closedate);
                                            jsonObject.put("remarks", edtremarks.getText().toString());
                                            jsonObject.put("premium_amount", edtpremiumAmount.getText().toString());


                                            addToTask(closedate, dateofpayment);

                                            //  addTaskReminder();

                                            JSONArray jsonArray = new JSONArray();

                                            for (String tid : taskid) {
                                                JSONObject jsonObject1 = new JSONObject();

                                                jsonObject1.put("Taskid", tid);
                                                jsonArray.put(jsonObject1);

                                            }

                                            jsonObject.put("Task", jsonArray.toString());
                                            Utils.playSimpleTone(InsuranceEntryActivity.this);
                                            uploadData(jsonObject.toString());


                                        } catch (Exception e) {

                                        }


//                                    } else {
//
//                                        Toast.makeText(InsuranceEntryActivity.this, "Enter remarks", Toast.LENGTH_SHORT).show();
//                                    }


//                                    } else {
//
//                                        Toast.makeText(InsuranceEntryActivity.this, "Closing date must be after date of payment", Toast.LENGTH_SHORT).show();
//                                    }


                                    } else {

                                        Toast.makeText(InsuranceEntryActivity.this, "Select closing date", Toast.LENGTH_SHORT).show();
                                    }


                                } else {

                                    Toast.makeText(InsuranceEntryActivity.this, "Select date of payment", Toast.LENGTH_SHORT).show();
                                }


                            } else {

                                Toast.makeText(InsuranceEntryActivity.this, "Select Insurance type", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{

                            Toast.makeText(InsuranceEntryActivity.this, "Enter the premium amount", Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        Toast.makeText(InsuranceEntryActivity.this, "Enter the amount", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e)
                {


                }


//                }
//                else {
//
//                    Toast.makeText(InsuranceEntryActivity.this,"Enter the insurance number",Toast.LENGTH_SHORT).show();
//                }



            }
        });

        splicnumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {

                    CommonData cm = (CommonData) splicnumber.getSelectedItem();

                    JSONObject jsonObject = new JSONObject(cm.getData());
                    String Amount = jsonObject.getString("Amount");

                   // double amt = Double.parseDouble(Amount);

                    double amt =0;


                    amt = Double.parseDouble(Amount);

                    String Type=jsonObject.getString("Type");


                    //  amt = Double.parseDouble(Amount);

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        amt=amt*-1;
                    }

                    String id = cm.getId();

                    Calendar calendar = Calendar.getInstance();
                    int m = calendar.get(Calendar.MONTH) + 1;
                    int d = calendar.get(Calendar.DAY_OF_MONTH);
                    int y = calendar.get(Calendar.YEAR);
                    String enddate = d + "-" + m + "-" + y;

                    if(commonData==null) {
                        double closingbalance = getClosingBalance(amt, id, enddate, "");
                        edtAmount.setText(closingbalance + "");
                    }
                }catch (Exception e)
                {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null&&requestCode== ForAccountSettingsInsuranceRequestcode&&resultCode==RESULT_OK)
        {


            newaccount=data.getStringExtra("AccountAdded");

            getInsuranceData();



        }



    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getInsuranceData();
    }

    public void uploadData(String data)
    {
        if(commonData!=null)
        {

            new DatabaseHelper(InsuranceEntryActivity.this).updateData(commonData.getId(),data, Utils.DBtables.TABLE_INSURANCE);
            Utils.playSimpleTone(InsuranceEntryActivity.this);
            onBackPressed();

        }
        else {

            new DatabaseHelper(InsuranceEntryActivity.this).addData(Utils.DBtables.TABLE_INSURANCE,data);
            Utils.playSimpleTone(InsuranceEntryActivity.this);
           // new DatabaseHelper(InsuranceEntryActivity.this).addInsuranceNumber(edtlicnumber.getText().toString());
        }

        //setSuggestions();

        edtAmount.setText("");
//        edtlicnumber.setText("");
        edtremarks.setText("");
        spinnerinsurancetype.setSelection(0);
        closedate="";
        txtclosedate.setText("Select closing date");
        dateofpayment="";
        txtdop.setText("Select date of payment");

        onBackPressed();

    }

    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(InsuranceEntryActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }



//
//    public void setSuggestions()
//    {
//        List<String>insurancenum=new DatabaseHelper(InsuranceEntryActivity.this).getInsuranceNumbers();
//
//
//        if(insurancenum.size()>0) {
//
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                    (this, android.R.layout.select_dialog_item, insurancenum);
//
//
//            edtlicnumber.setThreshold(1);
//            edtlicnumber.setAdapter(adapter);
//            edtlicnumber.setTextColor(Color.BLACK);
//        }
//    }











    public void showCloseDatePickerDialog()
    {


            Calendar mCalender = Calendar.getInstance();
            int year = mCalender.get(Calendar.YEAR);
            int month = mCalender.get(Calendar.MONTH);
            int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog=new DatePickerDialog(InsuranceEntryActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                    int m=i1+1;

                    closedate=i2+"-"+m+"-"+i;

                    txtclosedate.setText(i2+"-"+m+"-"+i);


                }
            },year,month,dayOfMonth);

            datePickerDialog.show();

    }









    public void showMonthYear()
    {

        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
     getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(InsuranceEntryActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(1);
        npmonth.setMaxValue(31);



        final NumberPicker npyear=dialog.findViewById(R.id.npyear);



        npyear.setMinValue(0);
        npyear.setMaxValue(11);


        npyear.setDisplayedValues(arrmonth);

        dialog.getWindow().setLayout(width,height);







        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                dateofpayment=npmonth.getValue()+"/"+npyear.getValue();

                txtdop.setText(npmonth.getValue()+"/"+arrmonth[npyear.getValue()]);



            }
        });


        dialog.show();

    }


    public void getInsuranceData()
    {

        // spinvestname


        List<CommonData> commonData_filtered=new ArrayList<>();

        List<CommonData>commonData=new DatabaseHelper(InsuranceEntryActivity.this).getAccountSettingsData();

        for (CommonData cm:commonData)
        {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accounttype");

                if(acctype.equalsIgnoreCase("Insurance"))
                {



                    commonData_filtered.add(cm);
                }


                //




            }catch (Exception e)
            {

            }

        }


        BankSpinnerAdapter bankSpinnerAdapter=new BankSpinnerAdapter(InsuranceEntryActivity.this,commonData_filtered);
        splicnumber.setAdapter(bankSpinnerAdapter);



        for (int i=0;i<commonData_filtered.size();i++)

        {

            CommonData cm=commonData_filtered.get(i);

            if(cm.getId().equalsIgnoreCase(insuranceno))
            {

                splicnumber.setSelection(i);
                break;
            }





        }


        for (int i=0;i<commonData_filtered.size();i++)

        {
            try {

                CommonData cm = commonData_filtered.get(i);

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accountname");

                if (acctype.equalsIgnoreCase(newaccount)) {

                    splicnumber.setSelection(i);
                    break;
                }


            }catch (Exception e)
            {

            }


        }


    }


    public void addToTask(String closingdate,String monthyear)
    {

        String type=spinnerinsurancetype.getSelectedItem().toString();

        try {
            Calendar calendar = Calendar.getInstance();
            int y = calendar.get(Calendar.YEAR);

            int mnnth=calendar.get(Calendar.MONTH);
            int dt=calendar.get(Calendar.DAY_OF_MONTH);
            int m = 0;

            String my[] = monthyear.split("/");

          int d1=  Integer.parseInt(my[1]);
            int date = Integer.parseInt(my[0]);

          if(d1<mnnth)
          {
              y=y+1;
          }
          else if(d1==mnnth)
          {

              if(date<dt)
              {
                  y=y+1;
              }

          }

            for (int i = 0; i < arrmonth.length; i++) {
                if (i==Integer.parseInt(my[1])) {
                    m = i;
                    m++;
                    break;
                }
            }

            int d = Integer.parseInt(my[0]);

            String startdate = d + "-" + m + "-" + y;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(closingdate);

            int a=monthsBetween(strDate,endDate);
            Log.e("TAAG",a+"");

            Calendar calendar1=Calendar.getInstance();
            calendar1.setTime(strDate);

            Date dat_calendar=calendar1.getTime();


            if(dat_calendar.before(endDate))
            {

                String subject = "";


                String id = ((CommonData) splicnumber.getSelectedItem()).getId();


                List<CommonData> cms = new DatabaseHelper(InsuranceEntryActivity.this).getDataByID(id, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if (cms.size() > 0) {
                    //subject=


                    JSONObject jsonObject = new JSONObject(cms.get(0).getData());


                    subject = jsonObject.getString("Accountname");

                }


                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                String time = hour + ":" + minute;
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date dat = null;
                try {
                    dat = fmt.parse(time );
                } catch (ParseException e) {

                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                String   timeselected=fmtOut.format(dat);



                int currd=calendar1.get(Calendar.DAY_OF_MONTH);

                int m1=calendar1.get(Calendar.MONTH)+1;

                int curr_year=calendar1.get(Calendar.YEAR);


                JSONObject js = new JSONObject();
                js.put("name", subject);
                js.put("date", currd+"-"+m1+"-"+curr_year);
                js.put("time", timeselected);
                js.put("status", 0);
                long rowid=  new DatabaseHelper(InsuranceEntryActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());
                taskid.add(rowid+"");



                try {

                    Calendar c = Calendar.getInstance();

                    DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                    Date dtc=dateFormat.parse(currd+"-"+m1+"-"+curr_year);
                    c.setTime(dtc);


                    Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                    myIntent.putExtra("Task", subject);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC, c.getTimeInMillis(), pendingIntent);

                }catch (Exception e)

                {

                }



            }







            for (int i=0;i<a;i++)
            {

                if(type.equalsIgnoreCase("Quarterly"))
                {
                    calendar1.add(Calendar.MONTH, 3);

                }

                if(type.equalsIgnoreCase("Half yearly"))
                {
                    calendar1.add(Calendar.MONTH, 6);

                }

                if(type.equalsIgnoreCase("Monthly"))
                {

                    calendar1.add(Calendar.MONTH, 1);
                }

                if(type.equalsIgnoreCase("Yearly"))
                {

                    calendar1.add(Calendar.YEAR, 1);
                }




        Date date_calendar=calendar1.getTime();


        if(date_calendar.before(endDate))
        {

            String subject = "";


            String id = ((CommonData) splicnumber.getSelectedItem()).getId();


            List<CommonData> cms = new DatabaseHelper(InsuranceEntryActivity.this).getDataByID(id, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if (cms.size() > 0) {
                //subject=


                JSONObject jsonObject = new JSONObject(cms.get(0).getData());


                subject = jsonObject.getString("Accountname");

            }


            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            String time = hour + ":" + minute;
            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
            Date dat = null;
            try {
                dat = fmt.parse(time );
            } catch (ParseException e) {

                e.printStackTrace();
            }
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
            String   timeselected=fmtOut.format(dat);



            int currd=calendar1.get(Calendar.DAY_OF_MONTH);

            int m1=calendar1.get(Calendar.MONTH)+1;

            int curr_year=calendar1.get(Calendar.YEAR);


            JSONObject js = new JSONObject();
            js.put("name", subject);
            js.put("date", currd+"-"+m1+"-"+curr_year);
            js.put("time", timeselected);
            js.put("status", 0);
          long rowid=  new DatabaseHelper(InsuranceEntryActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());
            taskid.add(rowid+"");



        }




            }


        }catch (Exception e)
        {

        }


    }

   public int monthsBetween(Date a, Date b) {
        Calendar cal = Calendar.getInstance();
        if (a.before(b)) {
            cal.setTime(a);
        } else {
            cal.setTime(b);
            b = a;
        }
        int c = 0;
        while (cal.getTime().before(b)) {
            cal.add(Calendar.MONTH, 1);
            c++;
        }
        return c - 1;
    }




    public void addTaskReminder()
    {

        try {


            String subject = "";


            String id = ((CommonData) splicnumber.getSelectedItem()).getId();


            List<CommonData> cms = new DatabaseHelper(InsuranceEntryActivity.this).getDataByID(id, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if (cms.size() > 0) {
                //subject=


                JSONObject jsonObject = new JSONObject(cms.get(0).getData());


                subject = jsonObject.getString("Accountname");

            }





//            String dop[] = dateofpayment.split("/");
//
//            String closedt[] = closedate.split("-");

//            int closeyear = Integer.parseInt(closedt[2]);
//
//            int closemonth=Integer.parseInt(closedt[1]);
//
//            int month = Integer.parseInt(dop[1]);
//
//            int date = Integer.parseInt(dop[0]);

            Calendar calendar = Calendar.getInstance();


           // int currentyear = calendar.get(Calendar.YEAR);






            int curr_year=calendar.get(Calendar.YEAR);
            int m=calendar.get(Calendar.MONTH);
            int currd=calendar.get(Calendar.DAY_OF_MONTH);

            int m1=m+1;




            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);


            String time = hour + ":" + minute;

            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
            Date dat = null;
            try {
                dat = fmt.parse(time );
            } catch (ParseException e) {

                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");

            String   timeselected=fmtOut.format(dat);





            JSONObject js = new JSONObject();
            js.put("name", subject);
            js.put("date", currd+"-"+m1+"-"+curr_year);
            js.put("time", timeselected);
            js.put("status", 0);


            new DatabaseHelper(InsuranceEntryActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());



        }catch (Exception e)
        {

        }

    }
}
