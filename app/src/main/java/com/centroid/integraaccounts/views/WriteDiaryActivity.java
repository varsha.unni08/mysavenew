package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.DiarySubjectAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Diarydata;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

public class WriteDiaryActivity extends AppCompatActivity {

    ImageView imgback,imgDatepick;
    Spinner spLanguage;

    TextView txtdatepick,txtHead;
    EditText edtSubject,edtdiarycontent;
    Button btnAdd;

    String date="";

    Resources resources;

    CommonData diarydata;

    Spinner spinnerSubject;

    FloatingActionButton fabadd;

    String subject="";

    int clicked=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_diary);
        getSupportActionBar().hide();

        diarydata=(CommonData) getIntent().getSerializableExtra("diary");


        imgback=findViewById(R.id.imgback);
        imgDatepick=findViewById(R.id.imgDatepick);
        spLanguage=findViewById(R.id.spLanguage);
        btnAdd=findViewById(R.id.btnAdd);
        fabadd=findViewById(R.id.fabadd);

        spinnerSubject=findViewById(R.id.spinnerSubject);


        txtdatepick=findViewById(R.id.txtdatepick);
        edtSubject=findViewById(R.id.edtSubject);
        edtdiarycontent=findViewById(R.id.edtdiarycontent);
        txtHead=findViewById(R.id.txtHead);







        if(diarydata!=null)
        {
            try{
                JSONObject jsonObject=new JSONObject(diarydata.getData());
                subject=jsonObject.getString("subject");
                String dat=jsonObject.getString("date");
                String content=jsonObject.getString("content");
                date=dat;
                txtdatepick.setText(date);
                //  edtSubject.setText(subject);
                edtdiarycontent.setText(content);
                btnAdd.setText(resources.getString(R.string.update));


            }catch (Exception e)
            {

            }

        }

        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
              getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels/2;
                int width = displayMetrics.widthPixels;

                final Dialog dialog=new Dialog(WriteDiaryActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_dialogsubject);



                dialog.getWindow().setLayout(width,height);


              final   EditText edtdiarycontent=dialog.findViewById(R.id.edtdiarycontent);

                Button btnAdd=dialog.findViewById(R.id.btnAdd);


                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if(!edtdiarycontent.getText().toString().equalsIgnoreCase(""))
                        {


                            dialog.dismiss();

                            new DatabaseHelper(WriteDiaryActivity.this).addData(Utils.DBtables.DIARYSUBJECT_table,edtdiarycontent.getText().toString());


                            getAllSubjects();



                        }
                        else {

//                            Toast.makeText(WriteDiaryActivity.this,"Enter the subject",Toast.LENGTH_SHORT).
//                                    show();


                            Utils.showAlertWithSingle(WriteDiaryActivity.this, "Enter the subject", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }


                    }
                });




                dialog.show();








            }
        });





        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDatePicker();

            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDatePicker();

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!date.equals(""))
                {
                    CommonData subj=((CommonData)spinnerSubject.getSelectedItem());

                    if(subj!=null)
                    {


                        if(!edtdiarycontent.getText().toString().equals(""))
                        {


                            try{


                                String lan=LocaleHelper.getPersistedData(WriteDiaryActivity.this,"en");

                                if(diarydata==null) {




                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("date", date);
                                    jsonObject.put("subject", subj.getId());
                                    jsonObject.put("content", edtdiarycontent.getText().toString());
                                    jsonObject.put("language", lan);


                                    new DatabaseHelper(WriteDiaryActivity.this).addData(Utils.DBtables.DIARY_table,jsonObject.toString());

                                    String languagedata = LocaleHelper.getPersistedData(WriteDiaryActivity.this, "en");
                                    Context context = LocaleHelper.setLocale(WriteDiaryActivity.this, languagedata);

                                    Resources resources = context.getResources();

                                    updateUI(resources);
                                    edtSubject.setText("");
                                    edtdiarycontent.setText("");
                                    //date = "";
                                }
                                else {

                                    String subjid=((CommonData)spinnerSubject.getSelectedItem()).getId();

                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("date", date);
                                    jsonObject.put("subject", subj.getId());
                                    jsonObject.put("content", edtdiarycontent.getText().toString());
                                    jsonObject.put("language", lan);

                                    new DatabaseHelper(WriteDiaryActivity.this).updateData(diarydata.getId(),jsonObject.toString(),Utils.DBtables.DIARY_table);


                                    onBackPressed();

                                }


                            }catch (Exception e)
                            {

                            }




                        }
                        else {



                            //  if( resources!=null) {

                        //    Toast.makeText(WriteDiaryActivity.this,resources.getString(R.string.enterdata) ,Toast.LENGTH_SHORT).show();


                            Utils.showAlertWithSingle(WriteDiaryActivity.this, resources.getString(R.string.enterdata), new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                            // }

                        }

                    }
                    else {




                        Utils.showAlertWithSingle(WriteDiaryActivity.this, "Press + button to create new subject ", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });




                    }



                }
                else {

                    if( resources!=null) {

                      //  Toast.makeText(WriteDiaryActivity.this, resources.getString(R.string.selectdate),Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(WriteDiaryActivity.this, resources.getString(R.string.selectdate), new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });



                    }
                }



            }
        });





        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String item=spLanguage.getSelectedItem().toString();

                String languagedata= LocaleHelper.getPersistedData(WriteDiaryActivity.this,"en");





                    if (item.equals("Select your language")) {
                        languagedata = "en";
                    }

                    if (item.equals("తెలుగు")) {
                        languagedata = "te";
                    }
                    if (item.equals("English")) {
                        languagedata = "en";
                    }

                    if (item.equals("मराठी")) {
                        languagedata = "mr";
                    }
                    if (item.equals("தமிழ்")) {
                        languagedata = "ta";
                    }

                    if (item.equals("ಕನ್ನಡ")) {
                        languagedata = "kn";
                    }

                    if (item.equals("हिंदी")) {
                        languagedata = "hi";
                    }


                    if (item.equals("മലയാളം")) {
                        languagedata = "ml";
                    }

                if (item.equals("اَلْعَرَبِيَّة")) {
                    languagedata = "ar";
                }

                    Context context = LocaleHelper.setLocale(WriteDiaryActivity.this, languagedata);

                    resources = context.getResources();
                    updateUI(resources);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



//        spLanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                clicked=1;
//            }
//        });







        getAllSubjects();

        checkLanguage();

    }


    public void checkLanguage()
    {



//        if (item.equals("Select your language")) {
//            languagedata = "en";
//        }
//
//        if (item.equals("తెలుగు")) {
//            languagedata = "te";
//        }
//        if (item.equals("English")) {
//            languagedata = "en";
//        }
//
//        if (item.equals("मराठी")) {
//            languagedata = "mr";
//        }
//        if (item.equals("தமிழ்")) {
//            languagedata = "ta";
//        }
//
//        if (item.equals("ಕನ್ನಡ")) {
//            languagedata = "kn";
//        }
//
//        if (item.equals("हिंदी")) {
//            languagedata = "hi";
//        }
//
//
//        if (item.equals("മലയാളം")) {
//            languagedata = "ml";
//        }



            String languagedata = LocaleHelper.getPersistedData(WriteDiaryActivity.this, "en");
        Context context= LocaleHelper.setLocale(WriteDiaryActivity.this, languagedata);
//          <item>Select your language</item>
//        <item>हिंदी</item>
//        <item>English</item>
//        <item>മലയാളം</item>
//
//        <item>தமிழ்</item>
//        <item>ಕನ್ನಡ</item>
//        <item>తెలుగు</item>
//        <item>मराठी</item>

        switch (languagedata)
        {

            case "ml":

                spLanguage.setSelection(3);


                break;

            case "en":
                spLanguage.setSelection(2);

                break;

            case "hi":

                spLanguage.setSelection(1);
                break;

            case "kn":
                spLanguage.setSelection(5);

                break;

            case "mr":
                spLanguage.setSelection(7);

                break;

            case "ta":
                spLanguage.setSelection(4);

                break;

            case "te":
                spLanguage.setSelection(6);

                break;


                default:
                    spLanguage.setSelection(0);


        }



//        resources=context.getResources();
//        txtHead.setText(resources.getString(R.string.writemydiary));
//
//        txtdatepick.setText(resources.getString(R.string.selectdate));
//        edtdiarycontent.setHint(resources.getString(R.string.enterdata));
//        edtSubject.setHint(resources.getString(R.string.subject));
//        btnAdd.setText(resources.getString(R.string.save));
    }









    public void getAllSubjects()
    {

        List<CommonData>commonData = new DatabaseHelper(WriteDiaryActivity.this).getData(Utils.DBtables.DIARYSUBJECT_table);


        if(commonData.size()>0)
        {

            spinnerSubject.setAdapter(new DiarySubjectAdapter(WriteDiaryActivity.this,commonData));


            for (int i=0;i<commonData.size();i++)
            {

                if(commonData.get(i).getId().equalsIgnoreCase(subject))
                {
                    spinnerSubject.setSelection(i);
                    break;
                }



            }

        }






    }


    public void updateUI(Resources resources)
    {

        txtdatepick.setText(resources.getString(R.string.selectdate));
        edtdiarycontent.setHint(resources.getString(R.string.enterdata));
        edtSubject.setHint(resources.getString(R.string.subject));
        txtHead.setText(resources.getString(R.string.writemydiary));

        btnAdd.setText(resources.getString(R.string.save));


        Calendar mCalender=Calendar.getInstance();

        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m=month+1;

        date=dayOfMonth+"-"+m+"-"+year;

        txtdatepick.setText(date);



        if(diarydata!=null)
        {
            try{



                JSONObject jsonObject=new JSONObject(diarydata.getData());

                 subject=jsonObject.getString("subject");

//                String contentsubj="";
//
//                List<CommonData>commonData=new DatabaseHelper(WriteDiaryActivity.this).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);
//
//                if(commonData.size()>0)
//                {
//
//                    contentsubj=commonData.get(0).getData();
//                }



                String dat=jsonObject.getString("date");
                String content=jsonObject.getString("content");
                date=dat;


                txtdatepick.setText(date);
              //  edtSubject.setText(subject);
                edtdiarycontent.setText(content);

                btnAdd.setText(resources.getString(R.string.update));





            }catch (Exception e)
            {

            }

        }

    }

    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(WriteDiaryActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                date=i2+"-"+m+"-"+i;

                txtdatepick.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

}
