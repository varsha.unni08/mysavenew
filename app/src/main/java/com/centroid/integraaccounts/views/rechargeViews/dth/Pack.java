
package com.centroid.integraaccounts.views.rechargeViews.dth;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pack {

    @SerializedName("id")
    @Expose
    private Integer id=0;
    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("type")
    @Expose
    private String type="";
    @SerializedName("languages")
    @Expose
    private List<String> languages=new ArrayList<>();
    @SerializedName("pictureQuality")
    @Expose
    private String pictureQuality="";
    @SerializedName("prices")
    @Expose
    private List<Price> prices=new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getPictureQuality() {
        return pictureQuality;
    }

    public void setPictureQuality(String pictureQuality) {
        this.pictureQuality = pictureQuality;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

}
