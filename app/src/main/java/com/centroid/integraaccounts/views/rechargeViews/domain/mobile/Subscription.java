
package com.centroid.integraaccounts.views.rechargeViews.domain.mobile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Subscription {

    @SerializedName("code")
    @Expose
    private String code="";
    @SerializedName("logo")
    @Expose
    private String logo="";
    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("popularity")
    @Expose
    private Integer popularity=0;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

}
