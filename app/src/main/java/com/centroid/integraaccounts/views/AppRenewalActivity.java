package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
//import com.cashfree.pg.api.CFPaymentGatewayService;
//import com.cashfree.pg.core.api.CFSession;
//import com.cashfree.pg.core.api.CFTheme;
//import com.cashfree.pg.core.api.callback.CFCheckoutResponseCallback;
//import com.cashfree.pg.core.api.utils.CFErrorResponse;
//import com.cashfree.pg.ui.api.CFDropCheckoutPayment;
//import com.cashfree.pg.ui.api.CFPaymentComponent;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.ShareSliderAdapter;
import com.centroid.integraaccounts.com.TokenResult.Tokendata;
import com.centroid.integraaccounts.data.CheckExpiry;
import com.centroid.integraaccounts.data.CheckTrialPeriod;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.EnglishNumberToWords;
import com.centroid.integraaccounts.data.SecuredDataHelper;
import com.centroid.integraaccounts.data.domain.AppSales;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Currency;
import com.centroid.integraaccounts.data.domain.MainSettings;
import com.centroid.integraaccounts.data.domain.MemberData;
import com.centroid.integraaccounts.data.domain.NetworkData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.RazorPayOrder;
import com.centroid.integraaccounts.data.domain.SettingsData;
import com.centroid.integraaccounts.data.domain.SliderShareImage;
import com.centroid.integraaccounts.data.domain.TrialStatus;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.ExpirydateCheckListener;
import com.centroid.integraaccounts.paymentdata.domain.CashFreeTransactionData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.transaction.TransactionData;
import com.centroid.integraaccounts.webserviceHelper.PaymentCheckSumRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.PaymentRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
//import com.paytm.pgsdk.TransactionManager;
//import com.razorpay.Checkout;
//import com.razorpay.PaymentResultListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.centroid.integraaccounts.Constants.Utils.PaytmCredentials.ActivityRequestCode;

import javax.net.ssl.HttpsURLConnection;

public class AppRenewalActivity extends AppCompatActivity  {

    ImageView imgback;
    TextView txtDateitle,txtdateactivation,txtExpirytitle,txtdateexpiry;

    int trial_expirycompleted=1;

    int trial=0;

    int purchased=0;

    Button btnRenew;

    boolean isrenewal=false;

    CardView card;
    Currency currency=null;
    double total_amount=0;

    int LAUNCH_SECOND_ACTIVITY = 1;

  String  countryid="0",stateid="0",name="",email="",phoneNumber="";

//  Checkout checkout=null;

    TransactionData transactionStatus=null;

  MemberData memberData=null;
    SettingsData settingsData1=null;

    String rs=  "";
    String orderIdString="",txnTokenString,TAG="sdkf";

    Handler handler=null;

    String url="",description="";


    String customername="",customerphone="",customeremail="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_renewal);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);

        txtDateitle=findViewById(R.id.txtDateitle);
        txtdateactivation=findViewById(R.id.txtdateactivation);
        txtExpirytitle=findViewById(R.id.txtExpirytitle);
        txtdateexpiry=findViewById(R.id.txtdateexpiry);
        btnRenew=findViewById(R.id.btnRenew);
//        btnShare=findViewById(R.id.btnShare);
        card=findViewById(R.id.card);
//        checkout=new Checkout();
//        checkout.setKeyID(Utils.razorkeyid);

        getUserprofile();
//        try {
//            CFPaymentGatewayService.getInstance().setCheckoutCallback(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        btnRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                AlertDialog.Builder builder=new AlertDialog.Builder(AppRenewalActivity.this);
//                builder.setMessage("Please contact My save team");
//
//                builder.setNegativeButton("ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//
//                    }
//                });
//
//                builder.show();


                if(purchased==1&&trial==0)
                {


                    isrenewal=true;

                    getProfileData(true);


                }
                else if(purchased==0&&trial==1)
                {

                    isrenewal=false;
                    getProfileData(false);

                }
                else if(purchased==0&&trial==0)
                {
                    isrenewal=true;
                    getProfileData(true);

                }


            }
        });

//        btnShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                final ProgressFragment progressFragment=new ProgressFragment();
//                progressFragment.show(getSupportFragmentManager(),"fkjfk");
//
//
//                Map<String,String> params=new HashMap<>();
//                params.put("timestamp",Utils.getTimestamp());
//
//                new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
//                    @Override
//                    public void onSuccess(String data) {
//                        progressFragment.dismiss();
//                        // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
//                        Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);
//
//
//                        progressFragment.dismiss();
//                        if(profiledata!=null)
//                        {
//
//                            try{
//
//
//
//                                if(profiledata.getStatus()==1)
//                                {
//
//
//                                    if(profiledata.getData()!=null)
//                                    {
//
//
//                                        String phonenumber=profiledata.getData().getMobile();
//                                        getNetWorkData(phonenumber,AppRenewalActivity.this);
//
//                                        // getCurrencyCode();
//
//                                    }
//
//
//
//
//
//                                }
//                                else {
//
//                                    //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();
//
//                                    Utils.showAlertWithSingle(AppRenewalActivity.this, "failed", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
//
//                                        }
//                                    });
//
//
//                                }
//
//
//
//                            }catch (Exception e)
//                            {
//
//                            }
//
//
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(String err) {
//                        progressFragment.dismiss();
//
//                        //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();
//
//                        Utils.showAlertWithSingle(AppRenewalActivity.this, err, new DialogEventListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        });
//
//
//                    }
//                },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();
//
//
//
//              //  getDataFromSettings();
//
//
//
////                String id=new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey);
////
////
////                final String link=Utils.domain+"index.php/web/signup?sponserid="+id;
////
////                Intent sendIntent = new Intent();
////                sendIntent.setAction(Intent.ACTION_SEND);
////                sendIntent.putExtra(Intent.EXTRA_TEXT, link);
////                sendIntent.setType("text/plain");
////                startActivity(sendIntent);
//
//
//            }
//        });
    }

//
//    @Override
//    public void onPaymentVerify(String s) {
//        Utils.showAlertWithSingle(AppRenewalActivity.this, "Payment Success", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//
//       // passPurchaseDataToServer(s);
//    }
//
//    @Override
//    public void onPaymentFailure(CFErrorResponse cfErrorResponse, String s) {
//        Utils.showAlertWithSingle(AppRenewalActivity.this, "Payment failed", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });
//    }

    public void getNetWorkData(String phonenumber, Context ctx) {

        Map<String, String> params = new HashMap<>();
        new RequestHandler(ctx, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                if (data != null) {

                    if (data != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(data);

                            if (jsonObject.getInt("status") == 1) {
                                // showReferDialog();

                                JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                NetworkData  networkDashboard=new GsonBuilder().create().fromJson(jsonObject1.toString(),NetworkData.class);


                                getDataFromSettings(networkDashboard);


                            } else {

                                Utils.showAlertWithSingle(AppRenewalActivity.this, "Sorry,you are not an active member ", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {


                                        //moreFragment.getProfileData();

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

//                                Utils.showAlert(context, "You are not purchased this application.do you want to purchase now ? ", new DialogEventListener() {
//                                    @Override
//                                    public void onPositiveButtonClicked() {
//
//
//                                        moreFragment.getProfileData();
//
//                                    }
//
//                                    @Override
//                                    public void onNegativeButtonClicked() {
//
//                                    }
//                                });
                            }


                        } catch (Exception e) {


                        }


                    }


                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.showMemberDetails + "?mobile=" + phonenumber+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();
    }

    public void getDataFromSettings(NetworkData  networkDashboard)
    {

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{


                        JSONObject jsonObject=new JSONObject(data);


                        //  MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                        if(jsonObject.getInt("status")==1)
                        {

                            JSONArray jsonArray=jsonObject.getJSONArray("data");
                            List<SliderShareImage>sliderShareImages=new ArrayList<>();

                            for (int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                SliderShareImage sliderShare=new GsonBuilder().create().fromJson(jsonObject1.toString(),SliderShareImage.class);
                                sliderShareImages.add(sliderShare);
                            }


                            DisplayMetrics displayMetrics = new DisplayMetrics();
                          getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int height = displayMetrics.heightPixels;
                            int width = displayMetrics.widthPixels;
                            final Dialog dialog = new Dialog(AppRenewalActivity.this);
                            dialog.setContentView(R.layout.layout_settingsshare);
                            dialog.setTitle(R.string.app_name);

                            dialog.getWindow().setLayout(width,height );

                            ViewPager viewPager=dialog.findViewById(R.id.viewpager);
                            TabLayout tabslayout=dialog.findViewById(R.id.tabslayout);

                            final TextView txtDescription=dialog.findViewById(R.id.txtDescription);
                            TextView txtSubDescription=dialog.findViewById(R.id.txtSubDescription);
                            Button btnShare=dialog.findViewById(R.id.btnShare);
                            Button btnpurchase=dialog.findViewById(R.id.btnpurchase);
                            Button btncopy=dialog.findViewById(R.id.btncopy);
                            TextView txtlink=dialog.findViewById(R.id.txtlink);


                            String languagedata = LocaleHelper.getPersistedData(AppRenewalActivity.this, "en");
                            Context contex= LocaleHelper.setLocale(AppRenewalActivity.this, languagedata);

                            Resources resources=contex.getResources();

                            btncopy.setText(resources.getString(R.string.copylink));



                            String id = new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey);
                            final String link = Utils.newdomain + "/signup?sponserid=" + id;
                            txtlink.setText(  link );

                            btncopy.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    int sdk = android.os.Build.VERSION.SDK_INT;
                                    if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) AppRenewalActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                                        clipboard.setText(link);

                                        Utils.showAlertWithSingle(AppRenewalActivity.this, resources.getString(R.string.linkcopied), new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    } else {
                                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) AppRenewalActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                                        android.content.ClipData clip = android.content.ClipData.newPlainText("text label",link);
                                        clipboard.setPrimaryClip(clip);

                                        Utils.showAlertWithSingle(AppRenewalActivity.this,  resources.getString(R.string.linkcopied), new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    }
                                }
                            });

                            txtSubDescription.setText(resources.getString(R.string.sharelinkmsg));



                            FrameLayout.LayoutParams layoutParams=(FrameLayout.LayoutParams)viewPager.getLayoutParams();
                            int w=(int)getResources().getDimension(R.dimen.dimen_270dp);
                            double a=w/0.609375;
                            int h=(int)a;
                            layoutParams.width=w;
                            layoutParams.height=h;
                            viewPager.setLayoutParams(layoutParams);

                            tabslayout.removeAllTabs();

                            viewPager.setAdapter(new ShareSliderAdapter(AppRenewalActivity.this,sliderShareImages));

                            SliderShareImage sliderShareImg=sliderShareImages.get(0);
                            txtDescription.setText(sliderShareImg.getDescription());

                            for (SliderShareImage sliderShareImage:sliderShareImages)
                            {
                                tabslayout.addTab(tabslayout.newTab());



                            }


                            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                }

                                @Override
                                public void onPageSelected(int position) {
                                    tabslayout.getTabAt(position).select();

                                    txtDescription.setText(sliderShareImages.get(position).getDescription());

                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });


                            tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    viewPager.setCurrentItem(tab.getPosition());

                                    txtDescription.setText(sliderShareImages.get(tab.getPosition()).getDescription());

                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {

                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {

                                }
                            });

                            if(!networkDashboard.getMember_status().equalsIgnoreCase("active"))
                            {



                                btnpurchase.setVisibility(View.VISIBLE);
                            }
                            else {
                                // btnPurchaseFreemember.setVisibility(View.GONE);


                            }


//                            btnShare.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//
//
//
//                                    SliderShareImage sliderShareImage=sliderShareImages.get(viewPager.getCurrentItem());
//
//                                    String imgurl=Utils.sliderimageurl+sliderShareImage.getImage();
//
//                                getBitmapData(imgurl,sliderShareImage.getDescription());
//
//                                }
//                            });





                            dialog.show();



//                        SettingsData settingsData1=settingsData.getData();
//
//                        String url=Utils.linkimg+settingsData1.getLinkImage();
//
//                        //  String  url="https://mysaving.in/images/saveicon.png";
//
//                        networkFragment.getBitmapData(url);
//
//                                        String imgurl=Utils.imgbaseurl+url;









                        }
                        else {





                        }



                    }catch (Exception e)
                    {

                    }



                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSettingsSlider+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }


    public void getBitmapData(String url,String description)
    {
        this.url=url;
        this.description=description;


        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdfj");


        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap1 =null;

                try {




                   // if(bitmap1!=null) {

                        if (ContextCompat.checkSelfPermission(AppRenewalActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            try {
                                URL ur = new URL(url);
                                bitmap1 = BitmapFactory.decodeStream(ur.openConnection().getInputStream());

                            }catch (Exception e)
                            {

                            }

                            String d = Calendar.getInstance().getTimeInMillis() + "";


                            Intent shareIntent;
                            //Bitmap bitmap1= BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
                            String path = getExternalCacheDir().getAbsolutePath() + "/" + d + ".png";
                            OutputStream out = null;
                            File file = new File(path);
                            try {

                                if (!file.exists()) {
                                    file.createNewFile();
                                }


                                out = new FileOutputStream(file);
                                bitmap1.compress(Bitmap.CompressFormat.PNG, 100, out);
                                out.flush();
                                out.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            path = file.getPath();
                            // path=file.getPath();

                            Uri bmpUri = null;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    progressFragment.dismiss();
                                }
                            });

                            String id = new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey);


                            final String link = Utils.newdomain + "/signup?sponserid=" + id;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                bmpUri = FileProvider.getUriForFile(AppRenewalActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                            } else {


                                bmpUri = Uri.parse("file://" + path);
                            }
                            shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, description+" \n" + link);
                            shareIntent.setType("image/png");
                            startActivity(Intent.createChooser(shareIntent, "Share with"));


                        } else {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    progressFragment.dismiss();
                                }
                            });


                            ActivityCompat.requestPermissions(AppRenewalActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
                        }

//                    }
//                    else {
//
//                        String id = new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey);
//
//
//                        final String link = Utils.domain + "index.php/web/signup?sponserid=" + id;
//
//
//                        Intent sendIntent = new Intent();
//                        sendIntent.setAction(Intent.ACTION_SEND);
//                        sendIntent.putExtra(Intent.EXTRA_TEXT,"Read more and join now \n"+ link);
//                        sendIntent.setType("text/plain");
//                        startActivity(sendIntent);
//                    }



                } catch(Exception e) {
                    System.out.println(e);
                }






            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        getBitmapData(url,description);
    }





    public void payAmount(final boolean isrenewal)
    {


        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{


                        MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                        if(settingsData.getStatus()==1)
                        {

                            settingsData1=settingsData.getData();



                            if(isrenewal) {

                                rs=  settingsData1.getRenewalCharge();

                            }
                            else {

                                rs=  settingsData1.getOneBvRequiredAmount();

                            }





                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            int height = displayMetrics.heightPixels;
                            int width = displayMetrics.widthPixels;
                            final Dialog dialog = new Dialog(AppRenewalActivity.this);
                            dialog.setContentView(R.layout.layout_paymentsummary);

                            int b=(int)(height/1.2);
                            dialog.getWindow().setLayout(width - 50,b );

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                            TextView txtActualPrice=dialog.findViewById(R.id.txtActualPrice);
                            LinearLayout layout_actualprice=dialog.findViewById(R.id.layout_actualprice);
                            TextView txtSgst=dialog.findViewById(R.id.txtSgst);
                            TextView txtCgst=dialog.findViewById(R.id.txtCgst);
                            TextView txtIgst=dialog.findViewById(R.id.txtIgst);
                            TextView txtTotal=dialog.findViewById(R.id.txtTotal);
                            TextView txtYourPosition=dialog.findViewById(R.id.txtYourPosition);
                            LinearLayout layout_yourposition=dialog.findViewById(R.id.layout_yourposition);

                            LinearLayout layout_Igst=dialog.findViewById(R.id.layout_Igst);
                            LinearLayout layout_sgst=dialog.findViewById(R.id.layout_sgst);
                            LinearLayout layout_cgst=dialog.findViewById(R.id.layout_cgst);

                            Button btnSubmit=dialog.findViewById(R.id.btnSubmit);


                            if(memberData!=null)
                            {
                                txtYourPosition.setText(memberData.getPositionNext());
                            }
                            else {

                                layout_yourposition.setVisibility(View.GONE);
                            }



                            btnSubmit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();

                                    // getRazorPayOrder();

                                    Calendar calendar=Calendar.getInstance();

                                    long l= calendar.getTimeInMillis();

                                    orderIdString=l+"";
//                                    createTransaction();

//                                    ServiceHandler sh = new ServiceHandler();





                                 generateTransaction();




                                }
                            });


                            if (countryid.equalsIgnoreCase("1"))
                            {
                                //layout_actualprice.setVisibility(View.GONE);

                                if (stateid.equalsIgnoreCase("12"))
                                {


                                    double actualvalue=Double.parseDouble(rs);

                                    double sgstvalue=actualvalue*Double.parseDouble(settingsData1.getSgst())/100;

                                    double cgstvalue=actualvalue*Double.parseDouble(settingsData1.getCgst())/100;

                                    txtActualPrice.setText(rs+" INR ");
                                    txtSgst.setText(sgstvalue+" INR ");
                                    txtCgst.setText(cgstvalue+" INR ");
                                    layout_Igst.setVisibility(View.GONE);

                                    double t=actualvalue+sgstvalue+cgstvalue;


                                    txtTotal.setText(Math.round(t)+" INR ");
                                    total_amount=Math.round(t);
                                }
                                else {
                                    double actualvalue=Double.parseDouble(rs);

                                    txtActualPrice.setText(rs+" INR ");
                                    layout_sgst.setVisibility(View.GONE);
                                    layout_cgst.setVisibility(View.GONE);
                                    layout_Igst.setVisibility(View.VISIBLE);
                                    double Igstvalue=actualvalue*Double.parseDouble(settingsData1.getIgst())/100;

                                    txtIgst.setText(Igstvalue+" INR ");

                                    double t=actualvalue+Igstvalue;

                                    txtTotal.setText(Math.round(t)+" INR ");

                                    total_amount=Math.round(t);
                                }



                            }
                            else {

                                layout_sgst.setVisibility(View.GONE);
                                layout_cgst.setVisibility(View.GONE);
                                layout_Igst.setVisibility(View.GONE);
                                layout_actualprice.setVisibility(View.GONE);

                                double actualvalue=Double.parseDouble(rs);

                                double othertax=Double.parseDouble(settingsData1.getOtherTax());

                                double otprice=actualvalue*othertax/100;

                                double tot=actualvalue+otprice;

                                double conversionval=Double.parseDouble("1.00");



                                double t=tot*conversionval;




                                total_amount=Math.round(t);



                                txtTotal.setText(total_amount+" INR ");

                            }





                            dialog.show();








                        }
                        else {





                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getSettingsValue+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }



    public void createTransaction()
    {

        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");

        Map<String, String> params = new HashMap<>();
        params.put("access_code", "AVBF79JL71CC24FBCC");
        params.put("currency", "INR");
        params.put("amount", "1.0");
//        params.put("timestamp", Utils.getTimestamp());


        new PaymentRequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                 Toast.makeText(AppRenewalActivity.this,data,Toast.LENGTH_SHORT).show();
                try {

                    //JSONObject jsonObject = new JSONObject(data);


                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //  Toast.makeText(LoginActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.CCAvenueCredentials.jsonurl, Request.Method.POST).submitRequest();


//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
////
////                    List<NameValuePair> params = new ArrayList<NameValuePair>();
////// Add required parameters
////                    String jsonStr = sh.makeServiceCall(Utils.CCAvenueCredentials.jsonurl, ServiceHandler.POST, params);
////
//////modifying string as per requirement
////                    jsonStr = jsonStr.substring(0, jsonStr.length() - 1).replace("processData(", "");
////                    JSONArray payOptList = new JSONArray(jsonStr);
////
////// looping through All Payment Options
////                    for (int i = 0; i < payOptList.length(); i++) {
////                        JSONObject payOpt = payOptList.getJSONObject(i);
////                        String payOptStr = payOpt.getString("payOpt");
////                        try {
////                            if (payOpt.getString(payOptStr) != null) {
////                                payOptionList.add(payOptions.get(payOptStr));//Add payment option only if it includes any card
////
////                                JSONArray cardArr = new JSONArray(payOpt.getString(payOptStr));
////                                for (int j = 0; j < cardArr.length(); j++) {
////                                    JSONObject card = cardArr.getJSONObject(j);
////                                    CardTypeDTO cardTypeDTO = new CardTypeDTO();
////                                    // set values in the above dto and add the dto to respective list objects
////                                }
////                            }
////                        } catch (Exception e) {
////                        }
////                    }
//
//
//                    String response="";
//
//                    URL url = new URL(Utils.CCAvenueCredentials.jsonurl);
//                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//                    conn.setReadTimeout(10000);
//                    conn.setConnectTimeout(15000);
//                    conn.setRequestMethod("POST");
//                    conn.setDoInput(true);
//                    conn.setDoOutput(true);
//
//                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//                    params.add(new BasicNameValuePair("access_code", Utils.CCAvenueCredentials.access_code));
//                    params.add(new BasicNameValuePair("currency", "INR"));
//                    params.add(new BasicNameValuePair("amount", "1"));
//
//                    OutputStream os = conn.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(getQuery(params));
//                    writer.flush();
//                    writer.close();
//                    os.close();
//
//                    conn.connect();
//
//                    int responseCode=conn.getResponseCode();
//
//                    if (responseCode == HttpsURLConnection.HTTP_OK) {
//                        String line;
//                        BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
//                        while ((line=br.readLine()) != null) {
//                            response+=line;
//                        }
//                    }
//                    else {
//                        response="";
//
//                    }
//
//
//
//                    Log.d("PaymentResponse",response);
//
//
//
//
//                }catch (Exception e)
//                {
//                    Log.d("PaymentResponse",e.toString());
//                }
//
//
//            }
//        });

    }


    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }



    public void generateTransaction()
    {

//        Intent i=new Intent(AppRenewalActivity.this, PaymentWebViewActivity.class);
//        i.putExtra("name",name);
//        i.putExtra("email",email);
//        i.putExtra("phone",phoneNumber);
//        i.putExtra("amount",String.valueOf(total_amount));
//
//        startActivityForResult(i,ActivityRequestCode);


        Intent i=new Intent(AppRenewalActivity.this, CCAvenueWebViewActivity.class);


        startActivity(i);




    }










    public void executeWebCall() {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String responseData = "";
                    /* initialize an object */
                    JSONObject paytmParams = new JSONObject();

                    Random rm=new Random();
                    int m=rm.nextInt(1000-100)+1000;

                    /* body parameters */
                    JSONObject body = new JSONObject();

                    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
                    paytmParams.put("order_amount", total_amount);

                    /* Enter your order id which needs to be check status for */
                    paytmParams.put("order_id", m+"");
                    paytmParams.put("order_currency","INR");

/**
 * Generate checksum by parameters we have in body
 * You can get Checksum JAR from https://developer.paytm.com/docs/checksum/
 * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
 */
                    //  String checksum = PaytmChecksum.generateSignature(body.toString(), Utils.PaytmCredentials.TestMerchantKey);
                    /* head parameters */
                    JSONObject head = new JSONObject();

                    /* put generated checksum value here */
                    head.put("customer_id", "1");
                    head.put("customer_name", customername);
                    head.put("customer_email", "");
                    head.put("customer_phone", phoneNumber);

                    JSONObject order_meta=new JSONObject();
                    order_meta.put("notify_url","https://test.cashfree.com");

                    /* prepare JSON string for request */
                    paytmParams.put("order_meta", order_meta);
                    paytmParams.put("customer_details", head);
                    String post_data = paytmParams.toString();

                    /* for Staging */
                    //  URL url = new URL("https://securegw-stage.paytm.in/v3/order/status");

                    /* for Production */
                    URL url = new URL("https://api.cashfree.com/pg/orders");

                    try {
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setRequestProperty ("x-client-id", "2110088e61c5abc8b72c74b033800112");
                        connection.setRequestProperty ("x-client-secret", "6d61be6225fcc9f4ea900b1fd8fb3aa6cc83b7ea");
                        connection.setRequestProperty ("x-api-version", "2022-01-01");
                        connection.setRequestProperty ("x-request-id", "Antony");

                        connection.setDoOutput(true);


                        DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
                        requestWriter.writeBytes(post_data);
                        requestWriter.close();

                        InputStream is = connection.getInputStream();
                        BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
                        if ((responseData = responseReader.readLine()) != null) {
                            System.out.append("Response: " + responseData);
                        }

                        CashFreeTransactionData trs=new GsonBuilder().create().fromJson(responseData,CashFreeTransactionData.class);

                        startPaytmPayment(trs);

                        // doDropCheckoutPayment(trs);



                        // System.out.append("Request: " + post_data);
                        responseReader.close();
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }

//                    transactionStatus=new GsonBuilder().create().fromJson(responseData, TransactionData.class);
//
//
//
//                    // executor.shutdown();
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            showStatus();
//                        }
//                    });





                }catch (Exception e)
                {

                }



               // getCheckSumData();

//                String responseData = "";
//                try {
//                    JSONObject paytmParams = new JSONObject();
//
//                    JSONObject body = new JSONObject();
//                    body.put("requestType", "Payment");
//                    body.put("mid", Utils.PaytmCredentials.TestMerchantID);
//                    body.put("websiteName", Utils.PaytmCredentials.Website);
//                    body.put("orderId", orderIdString);
//                    //
//                    // for test
//                    //
//                    body.put("callbackUrl", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderIdString);
//                    ///
//
//                    //  body.put("callbackUrl", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderIdString);
//
//
//                    JSONObject txnAmount = new JSONObject();
//                    txnAmount.put("value", String.valueOf(total_amount));
//                    txnAmount.put("currency", "INR");
//
//                    JSONObject userInfo = new JSONObject();
//                    userInfo.put("custId", name);
//                    body.put("txnAmount", txnAmount);
//                    body.put("userInfo", userInfo);
//
//                    // System.setProperty("com.warrenstrange.googleauth.rng.algorithmProvider", "IBMJCE");
//                    String checksum = PaytmChecksum.generateSignature(body.toString(), Utils.PaytmCredentials.TestMerchantKey);
//
//
//
//
//
//
//
//                    JSONObject head = new JSONObject();
//                    head.put("signature", checksum);
//
//                    paytmParams.put("body", body);
//                    paytmParams.put("head", head);
//
//                    String post_data = paytmParams.toString();
//
//
//                    URL url = new URL("https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                    /* for Production */
//                    // URL url = new URL("https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid="+Utils.PaytmCredentials.TestMerchantID+"&orderId="+orderIdString);
//
//                    try {
//                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                        connection.setRequestMethod("POST");
//                        connection.setRequestProperty("Content-Type", "application/json");
//                        connection.setDoOutput(true);
//
//                        DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
//                        requestWriter.writeBytes(post_data);
//                        requestWriter.close();
//
//                        InputStream is = connection.getInputStream();
//                        BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
//                        if ((responseData = responseReader.readLine()) != null) {
//                            Log.e("Response" , responseData);
//
//
//                        }
//
//
//
//
//
//
//
//
//                        responseReader.close();
//                    } catch (Exception exception) {
//                        exception.printStackTrace();
//                    }
//
//                }catch (Exception e)
//                {
//
//                    Log.e("Exception",e.toString());
//                }
//
//
//
//                Gson gsonBuilder=new GsonBuilder().create();
//                Tokendata tokendata= gsonBuilder.fromJson(responseData, Tokendata.class);
//                startPaytmPayment(tokendata.getBody().getTxnToken());







            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();

        getUserprofile();
        getSalesData();
    }


    public void getSalesData()
    {
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");





            new PreferenceHelper(AppRenewalActivity.this).putBooleanData(Utils.Needversionupdate, false);

            Map<String, String> params = new HashMap<>();
//        params.put("cash_transaction_id",trid);
            params.put("timestamp", Utils.getTimestamp());

            new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    //  progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                    try {

                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.getInt("status") == 1) {

                            // {"status":1,"message":"User exists","data":"195"}

                            String nodata = jsonObject.getString("data");

                            Log.e(TAG, "onSuccess: " + nodata);

                            if (!nodata.equalsIgnoreCase("")) {

                                int numberdata = Integer.parseInt(nodata);

                                if (numberdata == 0) {
                                    new PreferenceHelper(AppRenewalActivity.this).putBooleanData(Utils.Needversionupdate, true);
//                                    Utils.showUpdateAlert(MainActivity.this, "Your free update period is expired. Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//                                            Intent newIntente = new Intent(MainActivity.this, AppRenewalActivity.class);
//
//                                            startActivity(newIntente);
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
////                                        new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");
////                                        Intent newIntente = new Intent(MainActivity.this, LoginActivity.class);
////
////                                        startActivity(newIntente);
////
////                                        finish();
//
//
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//
//
//
//                                        }
//                                    });

                                } else if (-15 <= numberdata && numberdata < 0) {
                                    int a = Math.abs(numberdata);

                                    new PreferenceHelper(AppRenewalActivity.this).putBooleanData(Utils.Needversionupdate, true);

//                                    Utils.showUpdateAlert(MainActivity.this, "You have only " + a + " days  to expire the application.Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
                                            Calendar c=Calendar.getInstance();
                                            int day=c.get(Calendar.DAY_OF_MONTH);
                                            int month=c.get(Calendar.MONTH);
                                            int year=c.get(Calendar.YEAR);
                                            int m=month+1;
                                            String mnth=m+"";
                                            if(m>10)
                                            {
                                                mnth=m+"";
                                            }
                                            else {

                                                mnth="0"+m;
                                            }
                                            String currentdate=day+"-"+mnth+"-"+year;
                                            new DatabaseHelper(AppRenewalActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                        }
//                                    });


                                } else if (numberdata > 0) {

                                    new PreferenceHelper(AppRenewalActivity.this).putBooleanData(Utils.Needversionupdate, true);
//                                    Utils.showUpdateAlert(MainActivity.this, "Your free update period is expired. Please renew the application for new updates and be eligible for the renewal incentive.", new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                            Intent newIntente = new Intent(MainActivity.this, AppRenewalActivity.class);
//
//                                            startActivity(newIntente);
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
////                                        new PreferenceHelper(MainActivity.this).putData(Utils.userkey,"");
////                                        Intent newIntente = new Intent(MainActivity.this, LoginActivity.class);
////
////                                        startActivity(newIntente);
////
////                                        finish();
//
//                                            Calendar c=Calendar.getInstance();
//                                            int day=c.get(Calendar.DAY_OF_MONTH);
//                                            int month=c.get(Calendar.MONTH);
//                                            int year=c.get(Calendar.YEAR);
//                                            int m=month+1;
//                                            String mnth=m+"";
//                                            if(m>10)
//                                            {
//                                                mnth=m+"";
//                                            }
//                                            else {
//
//                                                mnth="0"+m;
//                                            }
//                                            String currentdate=day+"-"+mnth+"-"+year;
//                                            new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//
//                                        }
//                                    });
                                }


                            }


                        } else {

                            //  Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();
                           // new PreferenceHelper(AppRenewalActivity.this).putBooleanData(Utils.Needversionupdate, true);

                            Calendar c=Calendar.getInstance();
                            int day=c.get(Calendar.DAY_OF_MONTH);
                            int month=c.get(Calendar.MONTH);
                            int year=c.get(Calendar.YEAR);
                            int m=month+1;
                            String mnth=m+"";
                            if(m>10)
                            {
                                mnth=m+"";
                            }
                            else {

                                mnth="0"+m;
                            }
                            String currentdate=day+"-"+mnth+"-"+year;
                            new DatabaseHelper(AppRenewalActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);


//                            Utils.showAlertWithSingle(MainActivity.this, "Failed", new DialogEventListener() {
//                                @Override
//                                public void onPositiveButtonClicked() {
//
//                                }
//
//                                @Override
//                                public void onNegativeButtonClicked() {
//                                    Calendar c=Calendar.getInstance();
//                                    int day=c.get(Calendar.DAY_OF_MONTH);
//                                    int month=c.get(Calendar.MONTH);
//                                    int year=c.get(Calendar.YEAR);
//                                    int m=month+1;
//                                    String mnth=m+"";
//                                    if(m>10)
//                                    {
//                                        mnth=m+"";
//                                    }
//                                    else {
//
//                                        mnth="0"+m;
//                                    }
//                                    String currentdate=day+"-"+mnth+"-"+year;
//                                    new DatabaseHelper(MainActivity.this).addData(Utils.DBtables.TABLE_RENEWALMSG,currentdate);
//
//                                }
//                            });


                        }


                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(String err) {
                    //  progressFragment.dismiss();

                    //  Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();


//                    Utils.showAlertWithSingle(MainActivity.this, err, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });


                }
            }, Utils.WebServiceMethodes.getSalesData + "?timstap=" + Utils.getTimestamp(), Request.Method.GET).submitRequest();



    }





    public void startPaytmPayment (CashFreeTransactionData trs){


//        try {
//            CFSession cfSession = new CFSession.CFSessionBuilder()
//                    .setEnvironment(CFSession.Environment.PRODUCTION)
//                    .setOrderToken(trs.getOrderToken())
//                    .setOrderId(trs.getOrderId())
//                    .build();
//            CFPaymentComponent cfPaymentComponent = new CFPaymentComponent.CFPaymentComponentBuilder()
//                    // Shows only Card and UPI modes
//                    .add(CFPaymentComponent.CFPaymentModes.CARD)
//                    .add(CFPaymentComponent.CFPaymentModes.UPI)
//                    .build();
//            // Replace with your application's theme colors
//            CFTheme cfTheme = new CFTheme.CFThemeBuilder()
//                    .setNavigationBarBackgroundColor("#fc2678")
//                    .setNavigationBarTextColor("#ffffff")
//                    .setButtonBackgroundColor("#fc2678")
//                    .setButtonTextColor("#ffffff")
//                    .setPrimaryTextColor("#000000")
//                    .setSecondaryTextColor("#000000")
//                    .build();
//            CFDropCheckoutPayment cfDropCheckoutPayment = new CFDropCheckoutPayment.CFDropCheckoutPaymentBuilder()
//                    .setSession(cfSession)
//                    .setCFUIPaymentModes(cfPaymentComponent)
//                    .setCFNativeCheckoutUITheme(cfTheme)
//                    .build();
//            CFPaymentGatewayService gatewayService = CFPaymentGatewayService.getInstance();
//            gatewayService.doPayment(AppRenewalActivity.this, cfDropCheckoutPayment);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//        txnTokenString = token;
//        // for test mode use it
//      //  String host = "https://securegw-stage.paytm.in/";
//        // for production mode use it
//         String host = "https://securegw.paytm.in/";
//
////        String orderDetails = "MID: " + Utils.PaytmCredentials.TestMerchantID + ", OrderId: " + "ORDERID_98765" + ", TxnToken: " + txnTokenString
////                + ", Amount: " + "1.00";
//
//        //Log.e(TAG, "order details "+ orderDetails);
//
//        String callBackUrl = host + "theia/paytmCallback?ORDER_ID="+orderIdString;
//        Log.e(TAG, " callback URL "+callBackUrl);
//
//        PaytmOrder paytmOrder = new PaytmOrder(orderIdString, Utils.PaytmCredentials.TestMerchantID, txnTokenString, total_amount+"", callBackUrl);
//
//       // PaytmOrder paytmOrder = new PaytmOrder(orderIdString, Utils.PaytmCredentials.TestMerchantID, txnTokenString, "1.00", callBackUrl);
//
//
//
//        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback(){
//            @Override
//            public void onTransactionResponse(Bundle bundle) {
//
//                //Bundle[{STATUS=TXN_SUCCESS,
//                // CHECKSUMHASH=rSPxAwmNGiGKE6DMu3R9naA2tQ5LiVRnOouP6Yqbpec2NMCE8qPXbG2ggoo8MOgwA3MjqsLrzqsflJ+AjBh19C1OrxPkyKx9DaJYX+E+7kE=, BANKNAME=SBI, ORDERID=260420219991, TXNAMOUNT=1.00, TXNDATE=2021-04-26 12:23:33.0, MID=eAoUUb21380278750445, TXNID=20210426111212800110168918602577413, RESPCODE=01, PAYMENTMODE=NB, BANKTXNID=12783111587, CURRENCY=INR, GATEWAYNAME=SBI, RESPMSG=Txn Success}]
//
//                Log.e(TAG, "Response (onTransactionResponse) : "+bundle.toString());
//
//                String id=bundle.getString("TXNID");
//
//                if(bundle.getString("RESPCODE").equalsIgnoreCase("01"))
//                {
//
//                    passPurchaseDataToServer(id);
//
//                    Utils.showAlertWithSingle(AppRenewalActivity.this, "Transaction completed Successfully \n Transaction ID   : "+id, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });
//
//
//
//
//                }
//                else {
//
//                    Utils.showAlertWithSingle(AppRenewalActivity.this, "Failed : "+id, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });
//
//
//                }
//            }
//            @Override
//            public void networkNotAvailable() {
//                Log.e(TAG, "network not available ");
//            }
//            @Override
//            public void onErrorProceed(String s) {
//                Log.e(TAG, " onErrorProcess "+s.toString());
//            }
//            @Override
//            public void clientAuthenticationFailed(String s) {
//                Log.e(TAG, "Clientauth "+s);
//            }
//            @Override
//            public void someUIErrorOccurred(String s) {
//                Log.e(TAG, " UI error "+s);
//            }
//            @Override
//            public void onErrorLoadingWebPage(int i, String s, String s1) {
//                Log.e(TAG, " error loading web "+s+"--"+s1);
//            }
//            @Override
//            public void onBackPressedCancelTransaction() {
//                Log.e(TAG, "backPress ");
//
//             //   executeTransactionStatus();
//            }
//            @Override
//            public void onTransactionCancel(String s, Bundle bundle) {
//                Log.e(TAG, " transaction cancel "+s);
//            }
//        });
//        transactionManager.setShowPaymentUrl(host + "theia/api/v1/showPaymentPage");
//        transactionManager.setAppInvokeEnabled(false);
//        transactionManager.startTransaction(AppRenewalActivity.this, ActivityRequestCode);
    }







    public void showInvoiceDialog(String trid)
    {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
          getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            double h = displayMetrics.heightPixels / 1.2;
            int height = (int) h;
            int width = displayMetrics.widthPixels;
            final Dialog dialog = new Dialog(AppRenewalActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_invoiceformat);
            TextView txtbillno = dialog.findViewById(R.id.txtbillno);
            TextView txtDate = dialog.findViewById(R.id.txtDate);
            TextView txtBuyer = dialog.findViewById(R.id.txtBuyer);
            TextView txtActualRate = dialog.findViewById(R.id.txtActualRate);
            TextView txtFullActualRate = dialog.findViewById(R.id.txtFullActualRate);
            TextView txtAmountinWords = dialog.findViewById(R.id.txtAmountinWords);
            TextView txtGSTHead = dialog.findViewById(R.id.txtGSTHead);
            LinearLayout layout_gst = dialog.findViewById(R.id.layout_gst);
            TextView txtsgstPrice = dialog.findViewById(R.id.txtsgstPrice);
            TextView txtcgstPrice = dialog.findViewById(R.id.txtcgstPrice);
            TextView txtIGST = dialog.findViewById(R.id.txtIGST);
            TextView txtNetTotalAmount = dialog.findViewById(R.id.txtNetTotalAmount);
            Button btnDownload = dialog.findViewById(R.id.btnDownload);
            Calendar calendar = Calendar.getInstance();
            int d = calendar.get(Calendar.DAY_OF_MONTH);
            int m = calendar.get(Calendar.MONTH) + 1;
            int y = calendar.get(Calendar.YEAR);
            txtbillno.setText("Bill no. : " + trid);
            txtDate.setText("Date : " + d + "-" + m + "-" + y);

            if (countryid.equalsIgnoreCase("1")) {
                if (stateid.equalsIgnoreCase("12")) {
                    double actualvalue = Double.parseDouble(rs);
                    double sgstvalue = actualvalue * Double.parseDouble(settingsData1.getSgst()) / 100;
                    double cgstvalue = actualvalue * Double.parseDouble(settingsData1.getCgst()) / 100;
                    txtActualRate.setText(rs + " ");
                    txtFullActualRate.setText(rs + " ");
                    txtsgstPrice.setText(sgstvalue + " ");
                    txtcgstPrice.setText(cgstvalue + " ");
                    txtIGST.setVisibility(View.INVISIBLE);
                    double t = actualvalue + sgstvalue + cgstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                } else {
                    double actualvalue = Double.parseDouble(rs);
                    txtActualRate.setText(rs + " ");
                    layout_gst.setVisibility(View.INVISIBLE);
                    double Igstvalue = actualvalue * Double.parseDouble(settingsData1.getIgst()) / 100;
                    txtIGST.setText(Igstvalue + " ");
                    double t = actualvalue + Igstvalue;
                    txtNetTotalAmount.setText(Math.round(t) + " ");
                    total_amount = Math.round(t);
                }

            } else {

                txtGSTHead.setText("Other Tax");
                layout_gst.setVisibility(View.INVISIBLE);
                txtsgstPrice.setVisibility(View.GONE);
                txtcgstPrice.setVisibility(View.GONE);
                txtIGST.setVisibility(View.VISIBLE);
                double actualvalue = Double.parseDouble(rs);
                double othertax = Double.parseDouble(settingsData1.getOtherTax());
                double otprice = actualvalue * othertax / 100;
                txtIGST.setText(otprice + "");
                double tot = actualvalue + otprice;
                double conversionval = Double.parseDouble(currency.getData().getConversion_value());
                double t = tot * conversionval;
                total_amount = Math.round(t);
                txtNetTotalAmount.setText(total_amount + " ");

            }


            int a=(int)total_amount;
            String d1 = String.valueOf(a);

            long n = Long.parseLong(d1);
            txtAmountinWords.setText("Amount in words : " + EnglishNumberToWords.convert(n) + " " + currency.getData().getCurrency_code());


            dialog.getWindow().setLayout(width, height);

            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    try {

                        btnDownload.setVisibility(View.GONE);


                        if(ContextCompat.checkSelfPermission(AppRenewalActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                            DisplayMetrics metrics = new DisplayMetrics();
                            AppRenewalActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                            int heightPixels = metrics.heightPixels;
                            int widthPixels = metrics.widthPixels;

                            View v1 = dialog.getWindow().getDecorView().getRootView();


                            v1.setDrawingCacheEnabled(true);
                            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                            v1.setDrawingCacheEnabled(false);


                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();

                            File fp = new File(AppRenewalActivity.this.getExternalCacheDir() + "/Save/Invoice");


                            if (!fp.exists()) {
                                fp.mkdirs();
                            }

                            File f = new File(fp.getAbsolutePath(), "invoice" + ts + ".png");
                            if (!f.exists()) {
                                f.createNewFile();

                            } else {

                                f.delete();
                                f.createNewFile();
                            }


                            FileOutputStream output = new FileOutputStream(f);


                            bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                            output.close();


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                Uri photoURI = FileProvider.getUriForFile(AppRenewalActivity.this, AppRenewalActivity.this.getApplicationContext().getPackageName() + ".provider", f);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(photoURI, "image/*");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(intent);

                            } else {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(f), "image/*");
                                startActivity(intent);
                            }

                            dialog.dismiss();
                        }
                        else {

                            ActivityCompat.requestPermissions(AppRenewalActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);
                        }



                    }catch (Exception e)
                    {

                    }
                }
            });


            dialog.show();
        }catch (Exception e)
        {
            Log.e("TAAGG",e.toString());
        }
    }







    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG ," result code "+resultCode);
        // -1 means successful  // 0 means failed
        // one error is - nativeSdkForMerchantMessage : networkError
       // super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode && data != null&&resultCode==RESULT_OK) {
//            Bundle bundle = data.getExtras();

            String trid=data.getStringExtra("result");

           // Utils.showAlertWithSingle(AppRenewalActivity.this,trid,null);

            passPurchaseDataToServer(trid);



//            String trdata=data.getStringExtra("response");
//
//            TransactionStatus transactionStatus=new GsonBuilder().create().fromJson(trdata,TransactionStatus.class);
//
//            String trid=transactionStatus.getTxnid();

//            if(networkFragment!=null)
//            {
//                networkFragment.passPurchaseDataToServer(trid);
//
//            }
//            else if(moreFragment!=null)
//            {
//
//                moreFragment.passPurchaseDataToServer(trid);
//            }

          //  passPurchaseDataToServer(trid);

           // executeTransactionStatus();

//
// data response - {"BANKNAME":"WALLET","BANKTXNID":"1395841115",
// "CHECKSUMHASH":"7jRCFIk6mrep+IhnmQrlrL43KSCSXrmM+VHP5pH0hekXaaxjt3MEgd1N9mLtWyu4VwpWexHOILCTAhybOo5EVDmAEV33rg2VAS/p0PXdk\u003d",
// "CURRENCY":"INR","GATEWAYNAME":"WALLET","MID":"EAc0553138556","ORDERID":"100620202152",
// "PAYMENTMODE":"PPI","RESPCODE":"01","RESPMSG":"Txn Success","STATUS":"TXN_SUCCESS",
// "TXNAMOUNT":"2.00","TXNDATE":"2020-06-10 16:57:45.0","TXNID":"20200610111212800110168328631290118"}

//            Toast.makeText(this, data.getStringExtra("nativeSdkForMerchantMessage")
//                    + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        }else{
            Log.e(TAG, " payment failed");
        }
    }


//    public void getRazorPayOrder()
//    {
//
//        try {
//
//            int a=(int)total_amount;
//
//            Random r = new Random();
//            int i1 = r.nextInt(9999 - 1000) + 1000;
//
//
//            Map<String,Object> jsonObject1 = new HashMap<>();
//            jsonObject1.put("receipt", "recipt"+i1);
//            jsonObject1.put("amount", a*100);
//            jsonObject1.put("currency", currency.getData().getCurrency_code());
//
//
//
//            RestService.RestApiInterface client = RestService.getRazorPayClientClient();
//
//            Call<JsonObject> jsonObjectCall = client.postOrders(jsonObject1);
//
//            jsonObjectCall.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                    if(response!=null)
//                    {
//
//                        if(response.body()!=null)
//                        {
//
//                          //  Toast.makeText(AppRenewalActivity.this,response.body().toString(),Toast.LENGTH_SHORT).show();
//
//
//                            RazorPayOrder razorPayOrder=new GsonBuilder().create().fromJson(response.body().toString(),RazorPayOrder.class);
//
//
//                            startPayment(razorPayOrder);
//
//
//
//
//                        }
//                        else {
//
//                            try {
//
//                                byte[] inputStream = response.errorBody().bytes();
//
//
//                                String str = new String(inputStream, StandardCharsets.UTF_8);
//
//                                //Toast.makeText(PurchaseActivity.this,str,Toast.LENGTH_SHORT).show();
//
//                            }catch (Exception e)
//                            {
//
//                            }
//
//
//
//                        }
//
//
//                    }
//
//
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                    Toast.makeText(AppRenewalActivity.this,t.toString(),Toast.LENGTH_SHORT).show();
//
//
//                }
//            });
//
//            //secret id : lvhUGQCw5xOkb7w70DKHkVOd
//
//            // {"id":"order_GsPkjrBYI0URJq","entity":"order","amount":500,"amount_paid":0,"amount_due":500,"currency":"INR","receipt":"sldjfs","offer_id":null,"status":"created","attempts":0,"notes":[],"created_at":1617008845}
//
//
//        }catch (Exception e)
//        {
//
//
//        }
//    }



    public void startPayment(RazorPayOrder razorPayOrder)
    {
        int a=(int)total_amount;

        try {
            JSONObject options = new JSONObject();

            options.put("name", "Century gate");
            options.put("description", "");
//            options.put("image", "http://mysaving.in/images/saveicon.png");
            options.put("image", "https://centroidsolutions.in/IntegraAccount/images/saveicon.png");
            options.put("order_id", razorPayOrder.getId());//from response of step 3.
            options.put("theme.color", "#252c45");
            options.put("currency", currency.getData().getCurrency_code());
            options.put("amount",a*100 );//pass amount in currency subunits
            options.put("prefill.email", email);
            options.put("prefill.contact",phoneNumber);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

           // checkout.open(AppRenewalActivity.this, options);

        } catch(Exception e) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e);
        }
    }

    public void getProfileData(boolean isrenewal)
    {


//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");
//

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {

                                countryid=profiledata.getData().getCountryId();

                                stateid=profiledata.getData().getStateId();
                                name=profiledata.getData().getFullName();
                                email=profiledata.getData().getEmailId();
                                phoneNumber=profiledata.getData().getMobile();

                                customeremail=email;
                                customername=name;
                                customerphone=phoneNumber;

                                  payAmount(isrenewal);
                              //  getCurrencyCode(isrenewal);

                            }





                        }
                        else {

                           // Toast.makeText(AppRenewalActivity.this," failed",Toast.LENGTH_SHORT).show();



                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(AppRenewalActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();










    }



//    public void getCurrencyCode(boolean isrenewall)
//    {
//
//        ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"sdjfn");
//
//
//
//        Map<String,String> params=new HashMap<>();
//
//
//        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
//            @Override
//            public void onSuccess(String data) {
//
//                progressFragment.dismiss();
//
//                if(data!=null)
//                {
//
//
//                    try{
//
//
//                        currency=new GsonBuilder().create().fromJson(data,Currency.class);
//
//                        if(currency.getStatus()==1)
//                        {
//
//
//
//                            //currency=response.body();
//                            payAmount(isrenewall);
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(AppRenewalActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(String err) {
//                progressFragment.dismiss();
//
//                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();
//
//            }
//        },Utils.WebServiceMethodes.getCurrencyByCountryID+"?="+countryid+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();
//
//
////        final ProgressFragment progressFragment=new ProgressFragment();
////        progressFragment.show(getSupportFragmentManager(),"fkjfk");
////
////
////
////
////
////        Call<Currency>currencyCall=client.getCurrencyByCountryID(new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey),countryid);
////
////        currencyCall.enqueue(new Callback<Currency>() {
////            @Override
////            public void onResponse(Call<Currency> call, Response<Currency> response) {
////
////                progressFragment.dismiss();
////                if(response.body()!=null)
////                {
////
////                    try{
////
////
////
////                        if(response.body().getStatus()==1)
////                        {
////
////
////
////                            currency=response.body();
////                            payAmount(isrenewall);
////
////
////
////
////                        }
////                        else {
////
////                            Toast.makeText(AppRenewalActivity.this," failed",Toast.LENGTH_SHORT).show();
////                        }
////
////
////
////                    }catch (Exception e)
////                    {
////
////                    }
////
////
////
////                }
////
////            }
////
////            @Override
////            public void onFailure(Call<Currency> call, Throwable t) {
////                progressFragment.dismiss();
////            }
////        });
//    }


    public void passPurchaseDataToServer(String trid)
    {

//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("cash_transaction_id",trid);
        params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try{

                    JSONObject jsonObject=new JSONObject(data);

                    if(jsonObject.getInt("status")==1)
                    {
                        getUserprofile();



//                        checkSalesInfo();
//
//                        getMemberData();


//                            if(moreAdapter!=null)
//                            {
//
//                                moreAdapter.showReferDialog();
//                            }
                        JSONObject jsonObject_data=jsonObject.getJSONObject("data");
                        String billno=jsonObject_data.getString("bill_no");
                        String billprefix=jsonObject_data.getString("billno_prefix");

                        if(isrenewal) {

                            Utils.showAlertWithSingle(AppRenewalActivity.this,"Your renewal is completed",null);

                            renewApplication();
                        }
                        else{

                            Utils.showAlertWithSingle(AppRenewalActivity.this,"Your activation is completed",null);

                        }


                        Intent intent=new Intent(AppRenewalActivity.this, InvoiceActivity.class);
                        intent.putExtra("settingsdata",settingsData1);
                        intent.putExtra("bill",billprefix+" "+billno);
                        intent.putExtra("currency",currency);
                        intent.putExtra("actualamount",rs);
                        intent.putExtra("countryid",countryid);
                        intent.putExtra("stateid",stateid);
                        intent.putExtra("email",email);
                        intent.putExtra("name",name);
                        intent.putExtra("tid",trid);
                        startActivity(intent);

                        onBackPressed();


                    }
                    else {

                      //  Toast.makeText(AppRenewalActivity.this,"Failed",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(AppRenewalActivity.this, "Uploading transaction failed", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }



                }catch (Exception e)
                {

                }




            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(AppRenewalActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.addSalesInfo, Request.Method.POST).submitRequest();



















    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode==178&&(RESULT_OK == resultCode) || (resultCode == 11)) {
//            if (data != null) {
//                String trxt = data.getStringExtra("response");
//                Log.e("UPI", "onActivityResult: " + trxt);
//                ArrayList<String> dataList = new ArrayList<>();
//                dataList.add(trxt);
//                 upiPaymentDataOperation(dataList);
//            } else {
//                Log.e("UPI", "onActivityResult: " + "Return data is null");
//                ArrayList<String> dataList = new ArrayList<>();
//                dataList.add("nothing");
//                 upiPaymentDataOperation(dataList);
//            }
//        }
//    }

//    private void upiPaymentDataOperation(ArrayList<String> data) {
//        if (Utils.isConnectionAvailable(AppRenewalActivity.this)) {
//            String str = data.get(0);
//            Log.e("UPIPAY", "upiPaymentDataOperation: "+str);
//            String paymentCancel = "";
//            if(str == null) str = "discard";
//            String status = "";
//            String approvalRefNo = "";
//            String response[] = str.split("&");
//            for (int i = 0; i < response.length; i++) {
//                String equalStr[] = response[i].split("=");
//                if(equalStr.length >= 2) {
//                    if (equalStr[0].toLowerCase().equals("Status".toLowerCase())) {
//                        status = equalStr[1].toLowerCase();
//                    }
//                    else if (equalStr[0].toLowerCase().equals("ApprovalRefNo".toLowerCase()) || equalStr[0].toLowerCase().equals("txnRef".toLowerCase())) {
//                        approvalRefNo = equalStr[1];
//                    }
//                }
//                else {
//                    paymentCancel = "Payment cancelled by user.";
//                }
//            }
//            if (status.equals("success")) {
//                //Code to handle successful transaction here.
//                // Toast.makeText(AppRenewalActivity.this, "Transaction successful.", Toast.LENGTH_SHORT).show();
//                Log.e("UPI", "payment successfull: "+approvalRefNo);
//
//                if(isrenewal)
//                {
//                    renewApplication();
//                }
//                else {
//
//                    //passPurchaseDataToServer("");
//                }
//            }
//            else if("Payment cancelled by user.".equals(paymentCancel)) {
//                // Toast.makeText(AppRenewalActivity.this, "Payment cancelled by user.", Toast.LENGTH_SHORT).show();
//                Log.e("UPI", "Cancelled by user: "+approvalRefNo);
//            }
//            else {
//                // Toast.makeText(AppRenewalActivity.this, "Transaction failed.Please try again", Toast.LENGTH_SHORT).show();
//                Log.e("UPI", "failed payment: "+approvalRefNo);
//            }
//        } else {
//            Log.e("UPI", "Internet issue: ");
//            // Toast.makeText(AppRenewalActivity.this, "Internet connection is not available. Please check and try again", Toast.LENGTH_SHORT).show();
//        }
//    }


    public void renewApplication()
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");

        String key=  new PreferenceHelper(AppRenewalActivity.this).getData(Utils.userkey);



//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getChildFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
         params.put("timestamp",Utils.getTimestamp());

        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                try {

                   // String data = response.body().toString();

                    JSONObject jsonObject1 = new JSONObject(data);

                    if(jsonObject1.getInt("status")==1)
                    {

                        checkSalesInfo();

                    }


                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(AppRenewalActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateRenewalStatus, Request.Method.POST).submitRequest();



    }


    public void getSmemberdata(String ph) {


        Map<String, String> params = new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                if (data != null) {

                    if (data != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(data);

                            if (jsonObject.getInt("status") == 1) {



                                checkSalesInfo();

                                JSONObject jsonObject1=jsonObject.getJSONObject("data");

                              NetworkData  networkDashboard=new GsonBuilder().create().fromJson(jsonObject1.toString(),NetworkData.class);

                                if(!networkDashboard.getMember_status().equalsIgnoreCase("active"))
                                {

                                    trial=1;
                                    purchased=1;

                                    btnRenew.setText("Purchase");

                                 //   btnPurchaseFreemember.setVisibility(View.VISIBLE);
                                }
                                else {
                                   // btnPurchaseFreemember.setVisibility(View.GONE);


                                }



                            } else {


                                trial=1;
                                purchased=0;

                                btnRenew.setText("Purchase");

                                checkTrialUser();

                             //   btnShare.setVisibility(View.GONE);

                            }


                        } catch (Exception e) {


                        }


                    }


                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.showMemberDetails+"?mobile="+ph+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }


    public void getUserprofile()
    {
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
              //  progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


               // progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {


                                String phonenumber=profiledata.getData().getMobile();

                                customername=profiledata.getData().getFullName();
                                customerphone=phonenumber;
                                customeremail=profiledata.getData().getEmailId();
                               // getNetWorkData(,context);
                                getSmemberdata(phonenumber);
                                // getCurrencyCode();

                            }





                        }
                        else {

                            //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(AppRenewalActivity.this, "failed", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });


                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
              //  progressFragment.dismiss();

                //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

                Utils.showAlertWithSingle(AppRenewalActivity.this, err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });


            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();
    }


























    public void checkSalesInfo()
    {

//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(((AppCompatActivity)AppRenewalActivity.this).getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                  progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {

                    if(data!=null)
                    {

                        try{

                            AppSales appSales=new GsonBuilder().create().fromJson(data,AppSales.class);

                            if(appSales.getStatus()==1){



                                btnRenew.setText("Renew");
                           //     btnShare.setVisibility(View.VISIBLE);



                                txtExpirytitle.setText("Date of expiry");
                                txtDateitle.setText("Date of activation");


                                if(appSales.getData()!=null) {


                                    DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    Date activationdate=dateFormat.parse(appSales.getData().getSalesDate());

                                    DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                                    String salesdate=dateFormat1.format(activationdate);

                                    DateFormat dateFormat2=new SimpleDateFormat("yyyy-MM-dd");

                                    Date expdt=dateFormat2.parse(appSales.getData().getExpeDate());

                                    String expirydate=dateFormat1.format(expdt);

                                    if(expdt.before(new Date()))
                                    {
                                        btnRenew.setVisibility(View.GONE);
                                        txtdateactivation.setText(salesdate);
                                        txtdateexpiry.setText(expirydate);

                                        getRenewalDetails();


                                    }
                                    else{
                                        txtdateactivation.setText(salesdate);
                                        txtdateexpiry.setText(expirydate);
                                        btnRenew.setVisibility(View.VISIBLE);

                                    }





                                }



                                checkExpiryDate();



                            }
                            else {

                                trial=1;
                                purchased=0;

                                btnRenew.setText("Purchase");

                                checkTrialUser();

                              //  btnShare.setVisibility(View.GONE);



                            }



                        }catch (Exception e)
                        {

                        }



                    }



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                 progressFragment.dismiss();

                Toast.makeText(AppRenewalActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.checkSalesInfo+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();




    }



    public void getRenewalDetails()
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(((AppCompatActivity)AppRenewalActivity.this).getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(AppRenewalActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
              //   Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {

                    if(data!=null)
                    {

                        try{

                            AppSales appSales=new GsonBuilder().create().fromJson(data,AppSales.class);

                            if(appSales.getStatus()==1){



                                btnRenew.setText("Renew");
                           //     btnShare.setVisibility(View.VISIBLE);



                                txtExpirytitle.setText("Date of expiry");
                                txtDateitle.setText("Date of activation");


                                if(appSales.getData()!=null) {


                                    DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    Date activationdate=dateFormat.parse(appSales.getData().getSalesDate());

                                    DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                                    String salesdate=dateFormat1.format(activationdate);

                                    DateFormat dateFormat2=new SimpleDateFormat("yyyy-MM-dd");

                                    Date expdt=dateFormat2.parse(appSales.getData().getExpeDate());

                                    String expirydate=dateFormat1.format(expdt);

//                                    if(expdt.before(new Date()))
//                                    {
//                                        btnRenew.setVisibility(View.GONE);
//

                 new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirycompleted, 0);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirywarning, 0);
//
//                                    }
//                                    else{
                                        txtdateactivation.setText(salesdate);
                                        txtdateexpiry.setText(expirydate);
                                        btnRenew.setVisibility(View.GONE);

                                 //   }





                                }



                              //  checkExpiryDate();



                            }
                            else {

//                                trial=1;
//                                purchased=0;
//
//                                btnRenew.setText("Purchase");
//
//                                checkTrialUser();
//
//                                btnShare.setVisibility(View.GONE);



                            }



                        }catch (Exception e)
                        {

                        }



                    }



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(AppRenewalActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getRenewalDetails+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();

    }






    public void checkTrialUser()
    {

        new CheckTrialPeriod(AppRenewalActivity.this, new ExpirydateCheckListener() {
            @Override
            public void OnExpirydateChecked(JSONObject jsonObject) {

                try {

                    TrialStatus trialData=new GsonBuilder().create().fromJson(jsonObject.toString(),TrialStatus.class);


                    if(trialData.getStatus()==0)
                    {

                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialcompleted, 1);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialwarning, 0);

                        trial_expirycompleted=2;

                        txtExpirytitle.setText("Trial days");
                        txtDateitle.setText("Date of Registration");

                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date activationdate=dateFormat.parse(trialData.getData().getJoin_date());

                        DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");



                        String salesdate=dateFormat1.format(activationdate);

                        Date sl_date=dateFormat1.parse(salesdate);

                        Calendar calendar=Calendar.getInstance();
                        calendar.setTime(sl_date);

                       // calendar.add(Calendar.DAY_OF_MONTH,30);

                        Date finaldate=calendar.getTime();

                        int num=getDaysDifference(new Date(),finaldate);













                        DateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date tsd=df.parse(trialData.getData().getTrialstart_date());

                        DateFormat df11=new SimpleDateFormat("dd-MM-yyyy");

                        String regdate=df11.format(tsd);


                        txtdateactivation.setText(regdate);






                        txtdateexpiry.setText(num+" days");

                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialcompleted, 1);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialwarning, 0);

                        Utils.showAlertWithSingle(AppRenewalActivity.this, Utils.Messages.error, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }
                    else if(trialData.getStatus()==2)
                    {

                        trial_expirycompleted=2;

                        txtExpirytitle.setText("Trial days");
                        txtDateitle.setText("Date of registration");

                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date activationdate=dateFormat.parse(trialData.getData().getJoin_date());

                        DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                        String salesdate=dateFormat1.format(activationdate);


                        Date sl_date=dateFormat1.parse(salesdate);

                        Calendar calendar=Calendar.getInstance();
                        calendar.setTime(sl_date);

                       // calendar.add(Calendar.DAY_OF_MONTH,30);

                        Date finaldate=calendar.getTime();

                        int num=getDaysDifference(new Date(),finaldate);




                        DateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date tsd=df.parse(trialData.getData().getTrialstart_date());

                        DateFormat df11=new SimpleDateFormat("dd-MM-yyyy");

                        String regdate=df11.format(tsd);


                        txtdateactivation.setText(regdate);
                        txtdateexpiry.setText(num+" days");


                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialcompleted, 0);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialwarning, 1);





                        Utils.showAlertWithSingle(AppRenewalActivity.this, Utils.Messages.warning, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }
                    else if(trialData.getStatus()==1){

                        txtExpirytitle.setText("Trial days");

                        txtDateitle.setText("Date of registration");

                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date activationdate=dateFormat.parse(trialData.getData().getJoin_date());

                        DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                        String salesdate=dateFormat1.format(activationdate);



                        Date sl_date=dateFormat1.parse(salesdate);

                        Calendar calendar=Calendar.getInstance();
                        calendar.setTime(sl_date);

                      //  calendar.add(Calendar.DAY_OF_MONTH,30);

                        Date finaldate=calendar.getTime();

                        int num=getDaysDifference(new Date(),finaldate);

                        txtdateexpiry.setText(num+" days");




                        DateFormat df=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date tsd=df.parse(trialData.getData().getTrialstart_date());

                        DateFormat df11=new SimpleDateFormat("dd-MM-yyyy");

                        String regdate=df11.format(tsd);


                        txtdateactivation.setText(regdate);


                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialcompleted, 0);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.trialwarning, 0);

                    }





                }catch (Exception e)
                {

                }

            }

            @Override
            public void OnFailed() {

            }
        }).checkTrialPeriodData();
    }


    public  int getDaysDifference(Date fromDate,Date toDate)
    {
        if(fromDate==null||toDate==null)
            return 0;

        return (int)( (toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
    }


    public void checkExpiryDate()
    {

        new CheckExpiry(AppRenewalActivity.this, new ExpirydateCheckListener() {
            @Override
            public void OnExpirydateChecked(JSONObject jsonObject1) {

                try {

                    //String data = jsonObject.toString();

                   // JSONObject jsonObject1 = new JSONObject(data);

                    if(jsonObject1.getInt("status")==0)
                    {

                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirycompleted, 1);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirywarning, 0);

                        trial_expirycompleted=1;

                        btnRenew.setVisibility(View.VISIBLE);



                    }
                    else if(jsonObject1.getInt("status")==2)
                    {
                        trial_expirycompleted=1;

                        purchased=1;
                        trial=0;


                        btnRenew.setVisibility(View.VISIBLE);


                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirycompleted, 0);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirywarning, 1);

                    }
                    else if(jsonObject1.getInt("status")==1)
                    {

                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirycompleted, 0);
                        new PreferenceHelper(AppRenewalActivity.this).putIntData(Utils.expirywarning, 0);

                        btnRenew.setVisibility(View.GONE);
                    }


                }catch (Exception e)
                {

                }




            }

            @Override
            public void OnFailed() {

            }
        }).checkExpiryDate();
    }


//    @Override
//    public void onPaymentSuccess(String s) {
//
//
//
//    }
//
//    @Override
//    public void onPaymentError(int i, String s) {
//
//    }
}
