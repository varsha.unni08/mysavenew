package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.app.NotificationPublisher;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.TaskData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.receivers.DateTimeReceiver;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddnewTaskActivity extends AppCompatActivity {

    ImageView imgback,imgDatepick,imgTimePick,imgRemindDatepick,imgRemindDateUptopick;

    TextView txtdatepick,txtTimepick,txtReminddatepick,txtReminddateUptopick;
    String date="",timeselected="",status="",reminddate="",reminddateupto="";

    Button btnAdd;
    EditText edtName;
    LinearLayout layoutStatus;
    RelativeLayout layout_uptodate;

    CommonData taskData;

    Spinner spStatus,Remindtype;

    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnew_task);
        getSupportActionBar().hide();
        taskData=(CommonData) getIntent().getSerializableExtra("task");

        edtName=findViewById(R.id.edtName);
        imgback=findViewById(R.id.imgback);
        imgDatepick=findViewById(R.id.imgDatepick);
        imgTimePick=findViewById(R.id.imgTimePick);

        txtdatepick=findViewById(R.id.txtdatepick);
        txtTimepick=findViewById(R.id.txtTimepick);
        btnAdd=findViewById(R.id.btnAdd);
        layoutStatus=findViewById(R.id.layoutStatus);
        spStatus=findViewById(R.id.spStatus);
        Remindtype=findViewById(R.id.Remindtype);
      //  txtvisitlogin=findViewById(R.id.txtvisitlogin);

        imgRemindDatepick=findViewById(R.id.imgRemindDatepick);
        txtReminddatepick=findViewById(R.id.txtReminddatepick);

        imgRemindDateUptopick=findViewById(R.id.imgRemindDateUptopick);
        txtReminddateUptopick=findViewById(R.id.txtReminddateUptopick);
        layout_uptodate=findViewById(R.id.layout_uptodate);



        String languagedata = LocaleHelper.getPersistedData(AddnewTaskActivity.this, "en");
        Context conte= LocaleHelper.setLocale(AddnewTaskActivity.this, languagedata);

        final Resources resources=conte.getResources();

        edtName.setHint(resources.getString(R.string.name));
        txtTimepick.setText(resources.getString(R.string.selecttime));
        txtdatepick.setText(resources.getString(R.string.selectdate));
        btnAdd.setText(resources.getString(R.string.save));
      //  txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        txtReminddatepick.setText(Utils.getCapsSentences(AddnewTaskActivity.this,resources.getString(R.string.remindingdate)));
        txtReminddateUptopick.setText(resources.getString(R.string.remindupto));


        Remindtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String data=Remindtype.getSelectedItem().toString();

                if(!data.equalsIgnoreCase("OneTime"))
                {

                    layout_uptodate.setVisibility(View.VISIBLE);


                }
                else{

                    layout_uptodate.setVisibility(View.GONE);


                }





            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if(!reminddate.equalsIgnoreCase("")) {


            layout_uptodate.setVisibility(View.VISIBLE);

        }
        else {

            layout_uptodate.setVisibility(View.GONE);

        }





        imgRemindDateUptopick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(2);

            }
        });

        txtReminddateUptopick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(2);
            }
        });


        imgRemindDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });

        txtReminddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });



        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                status=spStatus.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(taskData!=null)
        {
            try {

                JSONObject jsonObject = new JSONObject(taskData.getData());

                String name = jsonObject.getString("name");
                String da = jsonObject.getString("date");
                String time = jsonObject.getString("time");
                int status=jsonObject.getInt("status");

                if(jsonObject.has("reminddate")) {

                    reminddate= jsonObject.getString("reminddate");

                    txtReminddatepick.setText(reminddate);
                }


                if(jsonObject.has("reminddateupto")) {

                    reminddateupto= jsonObject.getString("reminddateupto");
                    txtReminddateUptopick.setText(reminddateupto);
                }
                if(jsonObject.has("remindPeriod")) {

                    String rm= jsonObject.getString("remindPeriod");


                    String arr[]=getResources().getStringArray(R.array.remindtype);

                    for(int i=0;i<arr.length;i++)
                    {

                        if(rm.equalsIgnoreCase(arr[i]))
                    {

                        Remindtype.setSelection(i);
                        
                        break;

                    }



                    }




                }








                spStatus.setSelection(status);

                date=da;
                timeselected=time;

                edtName.setText(name);
                txtdatepick.setText(da);
                txtTimepick.setText(time);


            }catch (Exception e)
            {

            }

            layoutStatus.setVisibility(View.VISIBLE);
            btnAdd.setText(resources.getString(R.string.update));
        }
        else {

            layoutStatus.setVisibility(View.GONE);

        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtName.getText().toString().equals(""))
                {
                    if(!date.equals(""))
                    {

                        if(!reminddate.equals(""))
                        {
                            if(!reminddateupto.equals(""))
                            {


addReminders();



                            }





                        }

                            if(taskData==null) {


                                try {


                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("name", edtName.getText().toString());
                                    jsonObject.put("date", date);
                                    jsonObject.put("time", timeselected);
                                    jsonObject.put("status", 0);

                                    if(!reminddate.equals(""))
                                    {

                                        jsonObject.put("reminddate",reminddate);
                                    }

                                    if(!reminddateupto.equals(""))
                                    {

                                        jsonObject.put("reminddateupto",reminddateupto);
                                    }

                                    jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

                                   // checkReminder(reminddate,timeselected,edtName.getText().toString());


                                    new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());


                                    edtName.setText("");
                                    date = "";
                                    timeselected = "";

//                                    txtdatepick.setText("Select date");
//                                    txtTimepick.setText("Select time");

                                    String languagedata = LocaleHelper.getPersistedData(AddnewTaskActivity.this, "en");
                                    Context conte= LocaleHelper.setLocale(AddnewTaskActivity.this, languagedata);

                                    final Resources resources=conte.getResources();

                                    edtName.setHint(resources.getString(R.string.name));
                                    txtTimepick.setText(resources.getString(R.string.selecttime));
                                    txtdatepick.setText(resources.getString(R.string.selectdate));
                                    btnAdd.setText(resources.getString(R.string.save));
                                //    txtvisitlogin.setText(resources.getString(R.string.visitlogin));
                                    txtReminddatepick.setText(resources.getString(R.string.remindingdate));


                                } catch (Exception e) {

                                }

                            }
                            else {

                                try {


                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("name", edtName.getText().toString());
                                    jsonObject.put("date", date);
                                    jsonObject.put("time", timeselected);
                                    jsonObject.put("reminddate",reminddate);

                                    if(!reminddate.equals(""))
                                    {

                                        jsonObject.put("reminddate",reminddate);
                                    }

                                    if(!reminddateupto.equals(""))
                                    {

                                        jsonObject.put("reminddateupto",reminddateupto);
                                    }

                                    jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());


                                    if(status.equals("Initial"))
                                    {
                                        jsonObject.put("status", 0);
                                        new DatabaseHelper(AddnewTaskActivity.this).updateData(taskData.getId(),jsonObject.toString(),Utils.DBtables.TABLE_TASK);


                                      //  checkReminder(reminddate,timeselected,edtName.getText().toString());
                                      //  new DatabaseHelper(AddnewTaskActivity.this).updateData(taskData.getId(),jsonObject.toString(),Utils.DBtables.TABLE_TASK);


                                        edtName.setText("");
                                        date = "";
                                        timeselected = "";

                                        txtdatepick.setText("Select date");
                                        txtTimepick.setText("Select time");

                                        onBackPressed();
                                    }
                                    else if(status.equals("Completed"))
                                    {
                                        jsonObject.put("status", 1);
                                        new DatabaseHelper(AddnewTaskActivity.this).updateData(taskData.getId(),jsonObject.toString(),Utils.DBtables.TABLE_TASK);
                                        edtName.setText("");
                                        date = "";
                                        timeselected = "";

                                        txtdatepick.setText("Select date");
                                        txtTimepick.setText("Select time");

                                        onBackPressed();
                                    }

                                    else if(status.equals("Postponed"))
                                    {
                                        jsonObject.put("status", 2);

                                        Calendar cal=Calendar.getInstance();
                                        int d=cal.get(Calendar.DAY_OF_MONTH);
                                        int m=cal.get(Calendar.MONTH)+1;
                                        int y=cal.get(Calendar.YEAR);

                                        String day=d+"-"+m+"-"+y;

                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                        Date todaydate = sdf.parse(day);
                                        Date taskdate = sdf.parse(date);




                                        if (todaydate.after(taskdate)||todaydate.equals(taskdate))
                                        {

                                            Toast.makeText(AddnewTaskActivity.this,"Your task is going to postponed, Please change the current date",Toast.LENGTH_SHORT).show();
                                        }

                                        else {

                                            edtName.setText("");
                                            date = "";
                                            timeselected = "";

                                            txtdatepick.setText("Select date");
                                            txtTimepick.setText("Select time");

                                            onBackPressed();

                                            new DatabaseHelper(AddnewTaskActivity.this).updateData(taskData.getId(),jsonObject.toString(),Utils.DBtables.TABLE_TASK);


                                        }





                                    }








                                } catch (Exception e) {

                                }











                            }



                           //


//                        }
//
//                        else {
//
//                            Toast.makeText(AddnewTaskActivity.this,"Select remind date",Toast.LENGTH_SHORT).show();
//                        }
                    }

                    else {

                        Toast.makeText(AddnewTaskActivity.this,"Select date",Toast.LENGTH_SHORT).show();
                    }

                }

                else {

                    Toast.makeText(AddnewTaskActivity.this,"Enter name",Toast.LENGTH_SHORT).show();
                }

            }
        });





        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);
            }
        });

        imgTimePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTimePickerdialog();

            }
        });


        txtTimepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerdialog();

            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });


    }


    public void addReminders()
    {
try {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    Date rmdate = sdf.parse(reminddate);
    Date rmdateupto = sdf.parse(reminddateupto);

    Date taskdate=sdf.parse(date);

    String data=Remindtype.getSelectedItem().toString();

    if(data.equalsIgnoreCase("OneTime"))
    {
        try {

            Calendar calendar1=Calendar.getInstance();
            calendar1.setTime(rmdate);

//            Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
//            myIntent.putExtra("Task", edtName.getText().toString());
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);
//
//            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//            alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);

            DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

            String remindeddate=dateFormat1.format(calendar1.getTime());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", edtName.getText().toString());
            jsonObject.put("date", remindeddate);
            jsonObject.put("time", timeselected);
            jsonObject.put("status", 0);

            if(!reminddate.equals(""))
            {
                jsonObject.put("reminddate",reminddate);
            }

            if(!reminddateupto.equals(""))
            {
                jsonObject.put("reminddateupto",reminddateupto);
            }

            jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

       long id=     new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());
            Utils.playSimpleTone(AddnewTaskActivity.this);
       int notificationid=Integer.parseInt(id+"");
            scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),rmdate);


        }catch (Exception e)

        {
            Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });
        }

    }

    if(data.equalsIgnoreCase("Daily"))
    {
        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);

        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.DAY_OF_MONTH, 1);
            calendar_taskdate.add(Calendar.DAY_OF_MONTH,1);
            DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

            String remindeddate=dateFormat1.format(calendar1.getTime());

            String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());




            try {



//                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
//                myIntent.putExtra("Task", edtName.getText().toString());
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);
//
//                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);





                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

                long id=       new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());
                Utils.playSimpleTone(AddnewTaskActivity.this);

                int notificationid=Integer.parseInt(id+"");
                scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),calendar_taskdate.getTime());
            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }

    }

    if(data.equalsIgnoreCase("Weekly"))
    {
        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);

        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.DAY_OF_MONTH, 7);
            calendar_taskdate.add(Calendar.DAY_OF_MONTH,7);
            DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

            String remindeddate=dateFormat1.format(calendar1.getTime());

            String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());




            try {



//                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
//                myIntent.putExtra("Task", edtName.getText().toString());
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);
//
//                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);





                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

              long id=  new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());

                Utils.playSimpleTone(AddnewTaskActivity.this);

                int notificationid=Integer.parseInt(id+"");
                scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),calendar_taskdate.getTime());


            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }

    }


    if(data.equalsIgnoreCase("Monthly"))
    {

        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);

        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.MONTH, 1);
            calendar_taskdate.add(Calendar.MONTH,1);
            DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

            String remindeddate=dateFormat1.format(calendar1.getTime());

            String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());




            try {



//                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
//                myIntent.putExtra("Task", edtName.getText().toString());
//                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);
//
//                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);





                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

             long id=   new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());

                Utils.playSimpleTone(AddnewTaskActivity.this);

                int notificationid=Integer.parseInt(id+"");
                scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),calendar_taskdate.getTime());


            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }



    }
    if(data.equalsIgnoreCase("Quarterly"))
    {

        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);

        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.MONTH, 3);
            calendar_taskdate.add(Calendar.MONTH,3);

            try {



                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                myIntent.putExtra("Task", edtName.getText().toString());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);

                DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");
                String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());

                String remindeddate=dateFormat1.format(calendar1.getTime());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

              long id=  new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());
                Utils.playSimpleTone(AddnewTaskActivity.this);
                int notificationid=Integer.parseInt(id+"");
                scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),calendar_taskdate.getTime());

            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }
    }
    if(data.equalsIgnoreCase("Half yearly"))
    {
        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);

        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.MONTH, 6);
            calendar_taskdate.add(Calendar.MONTH,6);

            try {



                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                myIntent.putExtra("Task", edtName.getText().toString());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);

                DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                String remindeddate=dateFormat1.format(calendar1.getTime());
                String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

             long id=   new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());
                Utils.playSimpleTone(AddnewTaskActivity.this);
                int notificationid=Integer.parseInt(id+"");
                scheduleNotification(AddnewTaskActivity.this,5,notificationid,"myreminder","Reminder",edtName.getText().toString(),calendar_taskdate.getTime());

            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }

    }
    if(data.equalsIgnoreCase("Yearly"))
    {

        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(rmdate);
        Calendar calendar_taskdate=Calendar.getInstance();
        calendar_taskdate.setTime(taskdate);


        while (calendar1.getTime().before(rmdateupto))
        {
            calendar1.add(Calendar.MONTH, 12);
            calendar_taskdate.add(Calendar.MONTH,12);

            try {



                Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                myIntent.putExtra("Task", edtName.getText().toString());
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);

                DateFormat dateFormat1=new SimpleDateFormat("dd-MM-yyyy");

                String remindeddate=dateFormat1.format(calendar1.getTime());
                String selectedtaskdate=dateFormat1.format(calendar_taskdate.getTime());
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", edtName.getText().toString());
                jsonObject.put("date", selectedtaskdate);
                jsonObject.put("time", timeselected);
                jsonObject.put("status", 0);

                if(!reminddate.equals(""))
                {
                    jsonObject.put("reminddate",remindeddate);
                }

                if(!reminddateupto.equals(""))
                {
                    jsonObject.put("reminddateupto",reminddateupto);
                }

                jsonObject.put("remindPeriod",Remindtype.getSelectedItem().toString());

                new DatabaseHelper(AddnewTaskActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonObject.toString());

                Utils.playSimpleTone(AddnewTaskActivity.this);
            }catch (Exception e)

            {
                Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }


        }
    }









}catch (Exception e)
{


    Utils.showAlertWithSingle(AddnewTaskActivity.this, e.toString(), new DialogEventListener() {
        @Override
        public void onPositiveButtonClicked() {

        }

        @Override
        public void onNegativeButtonClicked() {

        }
    });
}





    }


    public void showDatePicker(int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddnewTaskActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if(code==0) {
                    date = i2 + "-" + m + "-" + i;


                    txtdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else if(code==1) {
                    reminddate= i2 + "-" + m + "-" + i;

                    txtReminddatepick.setText(i2 + "-" + m + "-" + i);

                }
                else if(code==2) {
                    reminddateupto= i2 + "-" + m + "-" + i;

                    txtReminddateUptopick.setText(i2 + "-" + m + "-" + i);

                }

            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

    public void showTimePickerdialog()
    {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddnewTaskActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
               // eReminderTime.setText( ""selectedHour + ":" + selectedMinute);

                String time = selectedHour + ":" + selectedMinute;

                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time );
                } catch (ParseException e) {

                    e.printStackTrace();
                }

                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");

                timeselected=fmtOut.format(date);

                txtTimepick.setText(""+timeselected);
            }
        }, hour, minute,false);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }



//
//    public void checkReminder(String date,String time,String name)
//    {
//
////        28-1-2021
////
////        01:25 PM
//
//
//
//
//       // Log.e("date",date);
//       // Log.e("time",time);
//
//
//        String arr_date[]=date.split("-");
//
//        if(time.equalsIgnoreCase(""))
//        {
//            time="09:00 AM";
//        }
//
//        String timearr[]=time.split(":");
//
//        String othertimevar[]=timearr[1].split(" ");
//
//
//        int y=Integer.parseInt(arr_date[2]);
//        int d=Integer.parseInt(arr_date[0]);
//        int m=Integer.parseInt(arr_date[1]);
//
//
//        int h=Integer.parseInt(timearr[0]);
//        int minute=Integer.parseInt(othertimevar[0]);
//
//        String ampm=othertimevar[1];
//
//
//
//        Calendar calendar = Calendar.getInstance();
//
//        Log.e("HOUR OF DAY",calendar.get(Calendar.HOUR)+"");
//
//        calendar.set(Calendar.MONTH, m-1);
//        calendar.set(Calendar.YEAR, y);
//        calendar.set(Calendar.DAY_OF_MONTH, d);
//
//        calendar.set(Calendar.HOUR, h);
//        calendar.set(Calendar.MINUTE, minute);
//        calendar.set(Calendar.SECOND, 0);
//
//        if(ampm.equalsIgnoreCase("PM")) {
//
//            calendar.set(Calendar.AM_PM, Calendar.PM);
//        }
//        else {
//
//            calendar.set(Calendar.AM_PM, Calendar.AM);
//        }
//
////        Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
////        myIntent.putExtra("Task",name);
////        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent,0);
////
////        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
////        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
//    }



    public void scheduleNotification(Context context, long delay, int notificationId, String channelId, String title, String message,Date d) {

        // Create notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        // Create notification builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        // Schedule notification
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, builder.build());
        PendingIntent pendingIntent =null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            pendingIntent = PendingIntent.getActivity
                    (this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
        }
        else
        {
            pendingIntent = PendingIntent.getActivity
                    (this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        long futureInMillis =d.getTime();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

}
