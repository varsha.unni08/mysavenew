package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.LedgerAccountAdapter;
import com.centroid.integraaccounts.adapter.LedgerHeadAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LedgerActivity extends AppCompatActivity {

    ImageView imgback,imgstartDatepick,imgendDatepick,imgdownload;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    CommonData diarydata_selected=null;

    String startdate="",endate="";

    LinearLayout layout_head;

    Resources resources;
    TextView txtHead,txtstartdatepick,txtenddatepick,txtStatement;

    Button btnSearch;

    String month_selected = "", yearselected = "";

    String monthyear = "";

    double totalexp=0,totalinc=0;

    int m = 0, yea = 0;

    List<CommonData> cmfiltered_;
    List<LedgerAccount> ledgerAccounts = new ArrayList<>();

    int debitorCredit=0,Bankcash=0;

    String arrmonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    List<Accounts>accountsbydate;

    TextView txtAccount,txtDebit,txtcredit,txtAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger);
        getSupportActionBar().hide();
        String languagedata = LocaleHelper.getPersistedData(LedgerActivity.this, "en");
        Context context= LocaleHelper.setLocale(LedgerActivity.this, languagedata);
        Bankcash=getIntent().getIntExtra("Bankcash",0);



        resources=context.getResources();
        cmfiltered_=new ArrayList<>();
        imgback=findViewById(R.id.imgback);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtHead=findViewById(R.id.txtHead);
        layout_head=findViewById(R.id.layout_head);
        imgdownload=findViewById(R.id.imgdownload);

        imgendDatepick=findViewById(R.id.imgendDatepick);
        imgstartDatepick=findViewById(R.id.imgstartDatepick);

        txtstartdatepick=findViewById(R.id.txtstartdatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);
        txtStatement=findViewById(R.id.txtStatement);

        txtAction=findViewById(R.id.txtAction);
        txtAccount=findViewById(R.id.txtAccount);
        txtDebit=findViewById(R.id.txtDebit);
        txtcredit=findViewById(R.id.txtcredit);



        resources=context.getResources();

        txtAccount.setText(resources.getString(R.string.accountname));
        txtAction.setText(resources.getString(R.string.action));
        txtDebit.setText(resources.getString(R.string.debit));
        txtcredit.setText(resources.getString(R.string.credit));
        btnSearch.setText(resources.getString(R.string.search));

        if(Bankcash==1)
        {

            txtHead.setText(resources.getString(R.string.cash));

            Utils.showTutorial(resources.getString(R.string.cashbankbalancetutorial),LedgerActivity.this,Utils.Tutorials.cashbankbalancetutorial);


        }
        else {

            txtHead.setText(resources.getString(R.string.ledger));



            Utils.showTutorial(resources.getString(R.string.ledgertutorial),LedgerActivity.this,Utils.Tutorials.ledgertutorial);
        }




        accountsbydate=new ArrayList<>();
        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ledgerAccounts.size()>0)
                {


                    Utils.showAlert(LedgerActivity.this, resources.getString(R.string.doyouwantdownloadpdf), new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            if(ContextCompat.checkSelfPermission(LedgerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {


                                ActivityCompat.requestPermissions(LedgerActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
                            }
                            else {

                                downloadPdf();
                            }

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

                //showMonthYear();
            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (!startdate.equalsIgnoreCase("")) {

                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate)||endDate.equals(strDate))
                            {


                                String arr[]=startdate.split("-");

                                String currentYearStartdate="1-1-"+arr[2];

                                Date curr_ystdate = sdf.parse(currentYearStartdate);



                              accountsbydate= new DatabaseHelper(LedgerActivity.this).getAllAccounts();



                                getAccounthead();



                            }

                            else {

                                Toast.makeText(LedgerActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();
                            }


                        } else {

                            Toast.makeText(LedgerActivity.this, "Select end date", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        Toast.makeText(LedgerActivity.this, "Select start date", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e)
                {

                }





            }
        });


//        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showMonthYear();
//            }
//        });

        showCurrentMonthYear();

    }


    public void showCurrentMonthYear()
    {


        try {
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH) + 1;
            month_selected = m + "";
            yearselected = calendar.get(Calendar.YEAR) + "";
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);


            startdate = "1" + "-" + m + "-" + yearselected;

            endate = dayOfMonth + "-" + m + "-" + yearselected;

            txtstartdatepick.setText(startdate);
            txtenddatepick.setText(endate);

            String arr[]=startdate.split("-");


            String currentYearStartdate="1-1-"+arr[2];




            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(endate);

          //  Date currystdate=sdf.parse(currentYearStartdate);

            Date curr_ystdate = sdf.parse(currentYearStartdate);

            if (endDate.after(strDate) || endDate.equals(strDate)) {

               // accountsbydate = Utils.getAccountsBetweenDates(LedgerActivity.this, curr_ystdate, endDate);

accountsbydate=new DatabaseHelper(LedgerActivity.this).getAllAccounts();
                getAccounthead();

                // showTransactions(accountsbydate);

            }


           // getAccounthead();

        }catch (Exception e)
        {

        }

    }


    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(LedgerActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    yearselected=String.valueOf(i);

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else {

                    endate = i2 + "-" + m + "-" + i;
                    yearselected=String.valueOf(i);
                    txtenddatepick.setText(i2 + "-" + m + "-" + i);
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }



    private void downloadPdf()
    {

        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "Save/" + "IncomeExpenditure");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/IncomeExpenditure" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();





            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk("Ledger \n\n", mOrderDetailsTitleFont);// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph);

            document.add(new Paragraph(" \n\n"));


            PdfPTable table = new PdfPTable(3);
            table.addCell("Account");
            table.addCell("Debit");
            table.addCell("Credit");


            int aw=0;

            int a= ledgerAccounts.size();

            while (aw<a){

                List<CommonData> commonData = new DatabaseHelper(LedgerActivity.this).getAccountSettingsByID(ledgerAccounts.get(aw).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table.addCell(jsonObject.getString("Accountname"));


                }



                if(ledgerAccounts.get(aw).getDebitorcredit().equalsIgnoreCase(Utils.Cashtype.debit+""))
                {
                    double d=Double.parseDouble(ledgerAccounts.get(aw).getClosingbalance());

                    if(d<0||String.valueOf(d).contains("-"))
                    {
                        d=d*-1;
                    }

                    table.addCell(d+"");
                    table.addCell("");
                }
                else {

                    double d=Double.parseDouble(ledgerAccounts.get(aw).getClosingbalance());

                    if(d<0||String.valueOf(d).contains("-"))
                    {
                        d=d*-1;
                    }
                    table.addCell("");
                    table.addCell(d+"");

                }




                aw++;
            }


            document.add(table);





            document.close();

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(LedgerActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }




        }catch (Exception e)
        {

        }
    }
















    public void showMonthYear() {

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels / 2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(LedgerActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set = dialog.findViewById(R.id.date_time_set);


        final NumberPicker npmonth = dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);

        npmonth.setDisplayedValues(arrmonth);


        final NumberPicker npyear = dialog.findViewById(R.id.npyear);

        Calendar calendar1 = Calendar.getInstance();
        int year1 = calendar1.get(Calendar.YEAR);
        int mm = calendar1.get(Calendar.MONTH);

        npyear.setMinValue(year1);
        npyear.setMaxValue(year1 + 5);
        npyear.setValue(year1);
        npmonth.setValue(mm);


        dialog.getWindow().setLayout(width, height);


        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                int m = npmonth.getValue() + 1;
                month_selected = m + "";
                yearselected = npyear.getValue() + "";

                monthyear = npmonth.getValue() + "/" + npyear.getValue();

                txtstartdatepick.setText(arrmonth[npmonth.getValue()] + "/" + npyear.getValue());

               // getIncomeExpenseAccountHead();
                //   getBankBalancedata();

                getAccounthead();


            }
        });


        dialog.show();

    }


    public void getAccounthead() {

        layout_head.setVisibility(View.VISIBLE);
        recycler.setVisibility(View.VISIBLE);

        cmfiltered_.clear();


        List<CommonData> commonData = new DatabaseHelper(LedgerActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            //Collections.reverse(commonData);


            for (CommonData cm : commonData) {

                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String Accounttype=jsonObject.getString("Accounttype");


                    if(Bankcash==0) {



                        String arr[]={"Cash","Bank"};

                        boolean isexist=false;

                        for (String a:arr)
                        {

                            if(a.equalsIgnoreCase(Accounttype))
                            {
                                isexist=true;

                                break;

                            }



                        }
                        if(!isexist)
                        {
                            cmfiltered_.add(cm);
                        }

                    }
                    else {


                        String arr[]={"Cash","Bank"};

                        boolean isexist=false;

                        for (String a:arr)
                        {

                            if(a.equalsIgnoreCase(Accounttype))
                            {
                                isexist=true;

                                break;

                            }



                        }
                        if(isexist)
                        {
                            cmfiltered_.add(cm);
                        }


                    }




                }catch (Exception e)
                {


                }

            }

            try {



            }catch (Exception e)
            {

            }



        }
        else {

            layout_head.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);


        }



        checkHeads();




    }


    private void checkHeads()
    {

        ledgerAccounts.clear();

        List<Accounts> accountsSorted = accountsbydate;

        List<Accounts>accstring=new ArrayList<>();

        for (Accounts acc:accountsSorted) {

            if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
            {
                accstring.add(acc);
            }

        }

        String selected_date = startdate;

        try {

            for (CommonData cm : cmfiltered_) {

                String amount = "";
                double d =0;

                JSONObject jobj=new JSONObject(cm.getData());

                if(jobj.has("year"))
                {
                    Calendar c=Calendar.getInstance();
                    int y=Integer.parseInt(jobj.getString("year"));

                    int currentyear=Integer.parseInt(yearselected);

                    if(currentyear>y)
                    {
                        amount = "0";
                        d = Double.parseDouble(amount);
                    }
                    else {
                        amount = jobj.getString("Amount");
                        d = Double.parseDouble(amount);
                    }




                }
                else {

                     amount = jobj.getString("Amount");
                     d = Double.parseDouble(amount);

                }
               // Log.e("Amount",d+"");


                if(d>0)
                {


                    double openingbalance = 0;

                    String Type="";

                    if (!cm.getData().equalsIgnoreCase("")) {

                        JSONObject jsonObject = new JSONObject(cm.getData());

                        openingbalance = d;

                        Type=jsonObject.getString("Type");


                    } else {

                        openingbalance = 0;
                    }

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        openingbalance=openingbalance*-1;
                    }





                    double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

//                    double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");

                    double finalclosingbalance = 0;

                    finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                    LedgerAccount ledgerAccount = new LedgerAccount();
                    ledgerAccount.setAccountheadid(cm.getId());
                    ledgerAccount.setAccountheadname("");
                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate+"");

                    if(closingbalancebeforedate<0)
                    {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                    }
                    else {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                    }





                    if(finalclosingbalance<0) {

                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                    }
                    else {
                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                    }
                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                    ledgerAccounts.add(ledgerAccount);

                }









                for (Accounts acc:accstring) {


                        if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                            double openingbalance = 0;

                            String Type="";


                            if (!cm.getData().equalsIgnoreCase("")) {

                                JSONObject jsonObject = new JSONObject(cm.getData());
                                if(jsonObject.has("year")) {
                                    Calendar c = Calendar.getInstance();
                                    int y = Integer.parseInt(jsonObject.getString("year"));

                                    int currentyear=Integer.parseInt(yearselected);
                                    if(currentyear>y)
                                    {
                                        amount = "0";
                                        openingbalance= Double.parseDouble(amount);
                                    }
                                    else{
                                        amount = jsonObject.getString("Amount");
                                        openingbalance = Double.parseDouble(amount);
                                    }
                                }
                                else {

                                    openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                                }
                                Type=jsonObject.getString("Type");

                            } else {

                                openingbalance = 0;
                            }

                            if(Type.equalsIgnoreCase("Credit"))
                            {
                                openingbalance=openingbalance*-1;
                            }






                            double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

                           // double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");
                            double finalclosingbalance = 0;


                            finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                            if(!isAccountExist(ledgerAccounts,cm.getId())) {


                                LedgerAccount ledgerAccount = new LedgerAccount();
                                ledgerAccount.setAccountheadid(cm.getId());
                                ledgerAccount.setAccountheadname("");
                                ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");

                                if(closingbalancebeforedate<0)
                                {

                                    ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                                }
                                else {

                                    ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                                }

                                if (finalclosingbalance<0) {

                                    ledgerAccount.setDebitorcredit(Utils.Cashtype.credit + "");

                                } else {
                                    ledgerAccount.setDebitorcredit(Utils.Cashtype.debit + "");
                                }
                                ledgerAccount.setClosingbalance(finalclosingbalance + "");

                                ledgerAccounts.add(ledgerAccount);
                            }

                        }

                   // }
                }
//
            }
//

        } catch (Exception e) {

        }


        if(ledgerAccounts.size()>0)
        {

            for (LedgerAccount ledgerAccount:ledgerAccounts) {

                totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());



            }
            txtStatement.setText("Total  : "+totalinc+" "+resources.getString(R.string.rs));

            layout_head.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.VISIBLE);
            imgdownload.setVisibility(View.VISIBLE);
            //txtStatement.setVisibility(View.VISIBLE);

            recycler.setLayoutManager(new LinearLayoutManager(LedgerActivity.this));
            recycler.setAdapter(new LedgerAccountAdapter(LedgerActivity.this, ledgerAccounts,startdate,endate));

        }
        else {

            txtStatement.setText("Total  : "+0+" "+resources.getString(R.string.rs));

            layout_head.setVisibility(View.INVISIBLE);
            recycler.setVisibility(View.INVISIBLE);
            imgdownload.setVisibility(View.INVISIBLE);
        }



    }


    private boolean isAccountExist(List<LedgerAccount>accounts,String setupid)
    {

        boolean a=false;
        for (LedgerAccount acc:accounts) {

            if(acc.getAccountheadid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }


    private boolean isAccountAlreadyExist(List<Accounts>accounts,String setupid)
    {

        boolean a=false;
        for (Accounts acc:accounts) {

            if(acc.getACCOUNTS_setupid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }

    private double getClosingbalancebeforedate(double openingbalance, String id, String selected_date,String type) {

        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(LedgerActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                           // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                           // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;


            List<CommonData>accsetupdata=new DatabaseHelper(LedgerActivity.this).getDataByID(id,Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if(accsetupdata.size()>0)
            {
                try {

                    CommonData data = accsetupdata.get(0);

                    JSONObject jsonObject=new JSONObject(data.getData());

                    String Type=jsonObject.getString("Type");

//                    if(Type.equalsIgnoreCase("Debit"))
//                    {
//                        debitorCredit=0;
//                       // closingbalancebeforemonth=closingbalancebeforemonth;
//                    }
//                    else {
//
//                        closingbalancebeforemonth=closingbalancebeforemonth*-1;
//
//                        debitorCredit=1;
//                    }




                }catch (Exception e)
                {

                }



            }


        }

        return closingbalancebeforemonth;
    }





    private double getFinalClosingBalance(String month,String year1,String id,String type)
    {
        double finalclosingbalance=0;

        List<Accounts> accountsSorted = accountsbydate;



        for (Accounts accounts : accountsSorted) {



            if (id.equalsIgnoreCase(accounts.getACCOUNTS_setupid())) {



                    if(!id.equalsIgnoreCase("0")) {


                        if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            finalclosingbalance = finalclosingbalance - Double.parseDouble(accounts.getACCOUNTS_amount());


                         //   debitorCredit=Utils.Cashtype.credit;
                        } else if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());

                           // debitorCredit=Utils.Cashtype.debit;
                        }

                    }
                    else {



                    }




            }
        }






        return finalclosingbalance;


    }

}
