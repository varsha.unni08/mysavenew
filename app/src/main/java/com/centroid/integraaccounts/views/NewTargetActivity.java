package com.centroid.integraaccounts.views;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.data.domain.MileStoneTarget;
import com.centroid.integraaccounts.dialogs.CalculatorDialog;
import com.centroid.integraaccounts.dialogs.InvestMentFragment;
import com.centroid.integraaccounts.dialogs.TargetFragment;
import com.centroid.integraaccounts.interfaces.CalculatorService;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.InvestMentListener;
import com.centroid.integraaccounts.interfaces.TargetCategoryServices;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.google.api.client.util.Data;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewTargetActivity extends AppCompatActivity {

    ImageView imgback,imgDatepick,imgselectMilestone;
    TextView txtTargetAmount,txtSavedAmount,txtdatepick,txtSpinner,txtSelectInvestment,txtMilestone,txtprofile;
    EditText edtNotes;

    double targetamount=0,savedamount=0;

    String date = "", month_selected = "", yearselected = "";

    TargetCategory tgf=null;

    int pick=33;

    ImageView imgicon=null;

    File file_pick=null;

    Bitmap selectedImage=null;

    Button btnAdd,btndelete;

    CommonData data;

    String id="";

    CommonData selectedinvestment=null;

    ActivityResultLauncher<Intent> someActivityResultLauncher;

    MileStoneTarget ml=null;

    EditText edtTargetName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_target);
        getSupportActionBar().hide();

        Intent i=getIntent();
        id=i.getStringExtra("data");

        if(id!=null) {

            if (!id.equalsIgnoreCase("")) {

                List<CommonData> cmd = new DatabaseHelper(NewTargetActivity.this).getDataByID(id, Utils.DBtables.TABLE_TARGET);
                if (cmd.size() > 0) {
                    data = cmd.get(0);
                } else {

                    data = null;
                }

            } else {

                data = null;
            }
        }
        else{

            data = null;

        }

        imgback=findViewById(R.id.imgback);
        txtTargetAmount=findViewById(R.id.txtTargetAmount);
        txtSavedAmount=findViewById(R.id.txtSavedAmount);
        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);
        txtSpinner=findViewById(R.id.txtSpinner);
        txtSelectInvestment=findViewById(R.id.txtSelectInvestment);
        imgselectMilestone=findViewById(R.id.imgselectMilestone);
        txtMilestone=findViewById(R.id.txtMilestone);
        btndelete=findViewById(R.id.btndelete);

        btnAdd=findViewById(R.id.btnAdd);
        edtNotes=findViewById(R.id.edtNotes);
        edtTargetName=findViewById(R.id.edtTargetName);
        txtprofile=findViewById(R.id.txtprofile);


        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();

                            if(data!=null)
                            {
                                 ml=(MileStoneTarget)data.getSerializableExtra("MileStone");

                                 if(ml.getMls().size()>0)
                                 {
                                     txtMilestone.setText("View milestone");

                                 }
                                 else{

                                   Utils.showAlert(NewTargetActivity.this, "No milestone added.Do you want to add now ?", new DialogEventListener() {
                                       @Override
                                       public void onPositiveButtonClicked() {
                                           Intent intent = new Intent(NewTargetActivity.this, MileStoneActivity.class);
                                           intent.putExtra("target",targetamount+"");
                                           someActivityResultLauncher.launch(intent);
                                       }

                                       @Override
                                       public void onNegativeButtonClicked() {

                                       }
                                   });

                                 }









                            }


                        }
                    }
                });



        if(data!=null)
        {

            try {

                btnAdd.setText("Update");
                btndelete.setVisibility(View.VISIBLE);

                JSONObject js = new JSONObject(data.getData());

                String target_amount = js.getString("targetamount");
                String saved_amount = js.getString("savedamount");
                String target_date = js.getString("target_date");
                String note = js.getString("note");

                String target_categoryid = js.getString("target_categoryid");

                if(js.has("targetname"))
                {

                    String targetname=js.getString("targetname");
                    edtTargetName.setText(targetname);
                }


                TargetCategory tg=new DatabaseHelper(NewTargetActivity.this).getTargetCategoryById(target_categoryid);

                if(tg!=null)
                {

              this.tgf=tg;

                    txtSpinner.setText(tgf.getTask_category());



                }

                edtNotes.setText(note);
                this.date=target_date;
                txtdatepick.setText(date);
                targetamount=Double.parseDouble(target_amount);
                savedamount=Double.parseDouble(saved_amount);

                txtTargetAmount.setText(target_amount);
                txtSavedAmount.setText(saved_amount);


                if(js.has("investment_id"))
                {

                    String investment_id=js.getString("investment_id");


                        List<CommonData> data=new DatabaseHelper(NewTargetActivity.this).getAccountSettingsByID(investment_id);

                        if(data.size()>0)
                        {

                            selectedinvestment=data.get(0);

                            JSONObject jso=new JSONObject(data.get(0).getData());


                            txtSelectInvestment.setText(jso.getString("Accountname"));

                            // holder.txtvoucher.setText();

                        }



                    }




              //  }

                List<CommonData>commondata_target=new DatabaseHelper(NewTargetActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);

                List<MileStone>mileStones=new ArrayList<>();
                ml=new MileStoneTarget();
                if(commondata_target.size()>0)
                {

                    for(CommonData cmd:commondata_target)
                    {

                        String jsondata=cmd.getData();

                        try{

                            JSONObject js3=new JSONObject(jsondata);



                            String amount=js3.getString("amount");
                            String start_date=js3.getString("start_date");
                            String end_date=js3.getString("end_date");
                            String targetid=js3.getString("targetid");

                            if(targetid.equalsIgnoreCase(data.getId()))
                            {


                                MileStone mileStone=new MileStone();
                                mileStone.setId(cmd.getId());
                                mileStone.setSelected(0);
                                mileStone.setEnd_date(end_date);
                                mileStone.setStart_date(start_date);
                                mileStone.setAmount(amount);

                                mileStones.add(mileStone);



                            }




                        }catch (Exception e)
                        {

                        }


                    }

                    ml.setTargetamount(targetamount);
                    ml.setMls(mileStones);

                    if(ml.getMls().size()>0)
                    {
                        txtMilestone.setText("View mile stones");

                    }


                }


            }catch ( Exception e)
            {

            }
        }

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder=new AlertDialog.Builder(NewTargetActivity.this);
                builder.setMessage("Your Goal and its milestone details will be deleted.Do you want to delete your goal now ? ");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                        if(data!=null)
                        {

                            try {

                                btnAdd.setText("Update");
                                btndelete.setVisibility(View.GONE);

                                JSONObject js = new JSONObject(data.getData());

                                List<CommonData>commondata_target=new DatabaseHelper(NewTargetActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);

                                if(commondata_target.size()>0)
                                {

                                    for(CommonData cmd:commondata_target)
                                    {

                                        String jsondata=cmd.getData();

                                        try{

                                            JSONObject js3=new JSONObject(jsondata);



                                            String amount=js3.getString("amount");
                                            String start_date=js3.getString("start_date");
                                            String end_date=js3.getString("end_date");
                                            String targetid=js3.getString("targetid");

                                            if(targetid.equalsIgnoreCase(data.getId()))
                                            {

                                                new DatabaseHelper(NewTargetActivity.this).deleteData(cmd.getId(),Utils.DBtables.TABLE_MILESTONE);

                                            }




                                        }catch (Exception e)
                                        {

                                        }


                                    }


                                }

                                List<CommonData>cmd=new DatabaseHelper(NewTargetActivity.this).getData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);



                                for(CommonData cm:cmd) {


                                    JSONObject j = new JSONObject(cm.getData());
                                    String tid = j.getString("targetid");
                                    if(tid.equalsIgnoreCase(data.getId()))
                                    {

                                        new DatabaseHelper(NewTargetActivity.this).deleteData(cm.getId(),Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);

                                    }



                                }

                                new DatabaseHelper(NewTargetActivity.this).deleteData(data.getId(),Utils.DBtables.TABLE_TARGET);


                            }catch ( Exception e)
                            {

                            }


                            onBackPressed();


                        }




                    }
                });
                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });

                builder.show();






            }
        });


        txtMilestone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(data!=null)
                {

                    Intent i=new Intent(NewTargetActivity.this, MileStoneEditActivity.class);
                    i.putExtra("data",data.getId());

                    startActivity(i);
                }
                else {


                    Intent intent = new Intent(NewTargetActivity.this, MileStoneActivity.class);
                    intent.putExtra("target", targetamount + "");
                    if (ml != null) {
                        if (ml.getMls().size() > 0) {

                            intent.putExtra("mileStone", ml);
                        }
                    }

                    someActivityResultLauncher.launch(intent);
                }

            }
        });


        imgselectMilestone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data!=null)
                {

                    Intent i=new Intent(NewTargetActivity.this, MileStoneEditActivity.class);
                    i.putExtra("data",data.getId());

                    startActivity(i);
                }
                else {

                    Intent intent = new Intent(NewTargetActivity.this, MileStoneActivity.class);
                    intent.putExtra("target", targetamount + "");
                    intent.putExtra("mileStone", ml);
                    i.putExtra("forAdd", 1);

                    if (ml != null) {
                        if (ml.getMls().size() > 0) {

                            intent.putExtra("mileStone", ml);
                        }
                    }
                    someActivityResultLauncher.launch(intent);
                }
            }
        });


        txtSelectInvestment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InvestMentFragment inv=new InvestMentFragment(new InvestMentListener() {
                    @Override
                    public void onSelectInvestment(CommonData cmd) {

                        if(cmd!=null)
                        {

                            selectedinvestment=cmd;

                            try{


                                    JSONObject jso1=new JSONObject(cmd.getData());


                                    txtSelectInvestment.setText(jso1.getString("Accountname"));

                                String amount= jso1.getString("Amount");

                                if(amount!=null) {

                                    if(!amount.equalsIgnoreCase("")) {

                                        double closingbalance=   getClosingBalance(Double.parseDouble(amount),cmd.getId(),"","");


                                        savedamount = closingbalance;

                                        txtSavedAmount.setText(closingbalance + "");
                                    }
                                }




                            }catch (Exception e)
                            {

                            }

                        }


                    }
                });


                inv.show(getSupportFragmentManager(),"");
            }
        });






        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                TargetFragment tf=new TargetFragment(new TargetCategoryServices() {
                    @Override
                    public void onSelectTargetCategory(TargetCategory tgt) {

                        if(tgt!=null)
                        {
                            tgf=tgt;

                            txtSpinner.setText(tgf.getTask_category());

                        }


                    }
                });
                tf.show(getSupportFragmentManager(),"");



            }
        });




        txtTargetAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    CalculatorDialog dialogFragment = new CalculatorDialog(new CalculatorService() {
                        @Override
                        public void setAmount(double amount) {

                            DecimalFormat df = new DecimalFormat("0.00");

                            String a = df.format(amount);

                            targetamount = Double.parseDouble(a);

                            txtTargetAmount.setText(a + "");


                        }
                    });
                    dialogFragment.show(getSupportFragmentManager(), "My  Fragment");

            }
        });


        txtSavedAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedinvestment==null) {
                CalculatorDialog dialogFragment=new CalculatorDialog(new CalculatorService() {
                    @Override
                    public void setAmount(double amount) {

                        DecimalFormat df = new DecimalFormat("0.00");

                        String a=  df.format(amount);

                        savedamount=Double.parseDouble(a);

                        txtSavedAmount.setText(a + "");




                    }
                });
                dialogFragment.show(getSupportFragmentManager(),"My  Fragment");

                }
                else{

                    Utils.showAlertWithSingle(NewTargetActivity.this, "You already selected investment account", null);


                }
            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });


        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(tgf!=null)
                {

                    if(!edtTargetName.getText().toString().equalsIgnoreCase("")) {

                        if (targetamount != 0) {


                            if (targetamount > savedamount) {

                                if (!date.equalsIgnoreCase("")) {


                                    try {

                                        JSONObject js = new JSONObject();
                                        js.put("targetname", edtTargetName.getText().toString() + "");
                                        js.put("targetamount", targetamount + "");
                                        js.put("savedamount", 0 + "");
                                        js.put("target_date", date + "");
                                        js.put("note", edtNotes.getText().toString() + "");

                                        if (selectedinvestment != null) {

                                            js.put("investment_id", selectedinvestment.getId());
                                            JSONObject js1 = new JSONObject(selectedinvestment.getData());
                                          //  js1.put("amount", savedamount);

                                            new DatabaseHelper(NewTargetActivity.this).updateData(selectedinvestment.getId(), js1.toString(), Utils.DBtables.INVESTMENT_table);


                                        }


                                        js.put("target_categoryid", tgf.getId() + "");


                                        if (data != null) {
                                            new DatabaseHelper(NewTargetActivity.this).updateData(data.getId(), js.toString(), Utils.DBtables.TABLE_TARGET);

                                        } else {


                                            long isid = new DatabaseHelper(NewTargetActivity.this).addData(Utils.DBtables.TABLE_TARGET, js.toString());

                                            if (selectedinvestment ==null) {
                                                Calendar c = Calendar.getInstance();
                                                int y = c.get(Calendar.YEAR);
                                                int m = c.get(Calendar.MONTH);
                                                int d = c.get(Calendar.DAY_OF_MONTH);

                                                int m1 = m + 1;

                                                String date = d + "-" + m1 + "-" + y;

                                                JSONObject jsn = new JSONObject();
                                                jsn.put("date", date);
                                                jsn.put("savedamount", savedamount);
                                                jsn.put("targetid", isid);

                                                new DatabaseHelper(NewTargetActivity.this).addData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE, jsn.toString());

                                            }


                                            if (ml != null) {
                                                if (ml.getMls().size() > 0) {
                                                    for (MileStone mls : ml.getMls()) {
                                                        String id = mls.getId();
                                                        String amount = "";
                                                        String start_date = "", end_date = "";
                                                        if (!mls.getAmount().equalsIgnoreCase("")) {
                                                            amount = mls.getAmount();
                                                        }

                                                        if (!mls.getStart_date().equalsIgnoreCase("")) {
                                                            start_date = mls.getStart_date();
                                                        }

                                                        if (!mls.getEnd_date().equalsIgnoreCase("")) {
                                                            end_date = mls.getEnd_date();
                                                        }

                                                        if (!start_date.equalsIgnoreCase("") && !end_date.equalsIgnoreCase("") && !amount.equalsIgnoreCase("")) {

                                                            try {

                                                                JSONObject js2 = new JSONObject();
                                                                js2.put("amount", amount);
                                                                js2.put("start_date", start_date);
                                                                js2.put("end_date", end_date);
                                                                js2.put("targetid", isid + "");
                                                                new DatabaseHelper(NewTargetActivity.this).addData(Utils.DBtables.TABLE_MILESTONE, js2.toString());


                                                            } catch (Exception e) {
                                                                onBackPressed();

                                                            }

                                                        }


                                                    }


                                                } else {

                                                    onBackPressed();
                                                }
                                            } else {

                                                onBackPressed();
                                            }


                                        }

                                        onBackPressed();


                                    } catch (Exception e) {

                                    }


                                } else {


                                    Utils.showAlertWithSingle(NewTargetActivity.this, "Select target date", null);
                                }

                            } else {

                                Utils.showAlertWithSingle(NewTargetActivity.this, "Target amount must be greater than saved amount", null);


                            }


                        } else {


                            Utils.showAlertWithSingle(NewTargetActivity.this, "please enter Target amount", null);
                        }

                    }
                    else{

                        Utils.showAlertWithSingle(NewTargetActivity.this,"Select Target name",null);
                    }

                }
                else{


                    Utils.showAlertWithSingle(NewTargetActivity.this,"Select Target category",null);
                }




            }
        });

        String languagedata = LocaleHelper.getPersistedData(NewTargetActivity.this, "en");
        Context context= LocaleHelper.setLocale(NewTargetActivity.this, languagedata);

        Resources resources=context.getResources();

        txtprofile.setText(resources.getString(R.string.mydream));
        txtSpinner.setText(resources.getString(R.string.selecttarget));
        edtTargetName.setHint(resources.getString(R.string.targetname));
        txtTargetAmount.setText(resources.getString(R.string.targetamount));
        txtSelectInvestment.setText(resources.getString(R.string.selectinvestment));
        txtSavedAmount.setText(resources.getString(R.string.savedamount));

        txtdatepick.setText(resources.getString(R.string.selecttargetdate));
        txtMilestone.setText(resources.getString(R.string.addmilestone));
        edtNotes.setHint(resources.getString(R.string.Notes));
        btnAdd.setText(resources.getString(R.string.add));


    }

    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = new DatabaseHelper(NewTargetActivity.this).getAllAccounts();

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }


    public void showDatePicker() {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(NewTargetActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m = i1 + 1;
                month_selected = m + "";
                yearselected = i + "";

                date = i2 + "-" + m + "-" + i;

                txtdatepick.setText(i2 + "-" + m + "-" + i);


            }
        }, year, month, dayOfMonth);

        datePickerDialog.show();
    }


    public void showNewTargetDialog()
    {
        //new target dialogue


        if(ContextCompat.checkSelfPermission(NewTargetActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {



            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

            double h = displayMetrics.heightPixels / 1.2;
            int height = (int) h;
            int width = displayMetrics.widthPixels;

            final Dialog dialog = new Dialog(NewTargetActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_add_new_target);

            imgicon = dialog.findViewById(R.id.imgicon);
            EditText edtTargetAmount = dialog.findViewById(R.id.edtTargetAmount);
            Button btnsubmit = dialog.findViewById(R.id.btnsubmit);

            TextView txtwarning = dialog.findViewById(R.id.txtwarning);


            imgicon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, pick);
                }
            });


            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    txtwarning.setText("");

                    if (!edtTargetAmount.getText().toString().equalsIgnoreCase("")) {

                        if(file_pick!=null)
                        {

                            dialog.dismiss();
                            try {
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bitMapData = stream.toByteArray();

                                new DatabaseHelper(NewTargetActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,edtTargetAmount.getText().toString(),bitMapData);
                                Utils.playSimpleTone(NewTargetActivity.this);



                            }catch (Exception e)
                            {

                            }

                        }
                        else{

                            txtwarning.setText("Pick a image icon from gallery");
                        }





                    } else {

                        txtwarning.setText("Enter category name");
                    }


                }
            });


            dialog.getWindow().setLayout(width, height);


            dialog.show();
        }
        else{


            ActivityCompat.requestPermissions(NewTargetActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},11);



        }

    }


    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK&&data!=null&&requestCode==pick)
        {

            String res = null;
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(data.getData(), proj, null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                res = cursor.getString(column_index);






                try {

                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                   selectedImage = BitmapFactory.decodeStream(imageStream);

                    file_pick = new File(res);

                    int file_size = Integer.parseInt(String.valueOf(file_pick.length() / 1024));

                    if (file_size<100) {

                        imgicon.setImageBitmap(selectedImage);
                    }
                    else{


                        Utils.showAlertWithSingle(NewTargetActivity.this,"File size must be less than 100kb",null);
                    }

                }catch (Exception e)
                {

                }
            }
            cursor.close();

        }
    }
}