package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.adapter.BudgetDataAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.fragments.ExpenseDialogFragment;
import com.centroid.integraaccounts.fragments.SetBudgetFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class BudgetListActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    EditText edtAmount;

    Spinner spinnerYear,spinnerAccountName;
    Button submit;

    TextView txtTotal,txtSpinner;
    RecyclerView recycler;

    List<CommonData>cmfiltered;

    LinearLayout layout_title;

    ImageView imgback;
    TextView txtHead;

    Resources resources=null;

    TextView txtmonth,txtamount,txtaction;

    CommonData commonData_selected;

    boolean isAlreadybudgetAdded=false;

    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_list);
        getSupportActionBar().hide();

        cmfiltered=new ArrayList<>();
        fab_addtask=findViewById(R.id.fab_addtask);
        txtHead=findViewById(R.id.txtHead);

        spinnerYear=findViewById(R.id.spinnerYear);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        submit=findViewById(R.id.submit);
        txtTotal=findViewById(R.id.txtTotal);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        layout_title=findViewById(R.id.layout_title);
        txtaction=findViewById(R.id.txtaction);
        txtamount=findViewById(R.id.txtamount);
        txtmonth=findViewById(R.id.txtmonth);
        edtAmount=findViewById(R.id.edtAmount);
        txtSpinner=findViewById(R.id.txtSpinner);


        String languagedata = LocaleHelper.getPersistedData(BudgetListActivity.this, "en");
        Context context= LocaleHelper.setLocale(BudgetListActivity.this, languagedata);

         resources=context.getResources();
        Utils.showTutorial(resources.getString(R.string.budgettutorial),BudgetListActivity.this,Utils.Tutorials.budgettutorial);
        txtHead.setText(resources.getString(R.string.budget));
        submit.setText("Submit");

        txtmonth.setText(resources.getString(R.string.month));
        txtamount.setText(resources.getString(R.string.amount));
        txtaction.setText(resources.getString(R.string.action));
        edtAmount.setHint(resources.getString(R.string.amountpermonth));
        submit.setText(resources.getString(R.string.submit));


        Calendar calendar=Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        List<String> yeardata=new ArrayList<>();
        int y=year-1;

        for (int i=year;i<y+5;i++)
        {

            yeardata.add(i+"");
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(BudgetListActivity.this,   android.R.layout.simple_spinner_item, yeardata.toArray(new String[yeardata.size()]));

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYear.setAdapter(spinnerArrayAdapter);
        spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               isAlreadybudgetAdded=false;

                getBudgetData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinnerAccountName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                isAlreadybudgetAdded=false;
                getBudgetData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if(!isAlreadybudgetAdded)
                    {

                        if(commonData_selected!=null)
                        {

                    if(!edtAmount.getText().toString().equalsIgnoreCase("")) {


                        String year = spinnerYear.getSelectedItem().toString();
                        CommonData data = commonData_selected;


                        // String month = arrmonth[index];

                        for (int i = 0; i < arrmonth.length; i++) {

                            String m = arrmonth[i];

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("year", year);
                            jsonObject.put("month", m);
                            jsonObject.put("accountname", data.getId());
                            jsonObject.put("amount", edtAmount.getText().toString());


                            new DatabaseHelper(BudgetListActivity.this).addData(Utils.DBtables.TABLE_BUDGET, jsonObject.toString());

                            Utils.playSimpleTone(BudgetListActivity.this);
                        }

                        getBudgetData();

                     //  Toast.makeText(BudgetListActivity.this, resources.getString(R.string.datasaved), Toast.LENGTH_SHORT).show();
                        Utils.showAlertWithSingle(BudgetListActivity.this,resources.getString(R.string.datasaved), new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }
                    else {

                      //  Toast.makeText(BudgetListActivity.this,"Enter amount",Toast.LENGTH_SHORT).show();
                        Utils.showAlertWithSingle(BudgetListActivity.this,"Enter amount", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }

                        }
                        else {

                          //  Toast.makeText(BudgetListActivity.this,"Select an expense account",Toast.LENGTH_SHORT).show();
                            Utils.showAlertWithSingle(BudgetListActivity.this,"Select an expense account", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });

                        }


                    }
                    else {
                        Utils.showAlertWithSingle(BudgetListActivity.this,"Budget is already added", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                     //   Toast.makeText(BudgetListActivity.this,"Budget is already added",Toast.LENGTH_SHORT).show();

                    }


                }catch (Exception e)
                {


                }






            }
        });
        addAccountSettings();


        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ExpenseDialogFragment expenseDialogFragment=new ExpenseDialogFragment(new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {


                        try {

                            commonData_selected=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = jcmn1.getString("Accountname");

                            txtSpinner.setText(Utils.getCapsSentences(BudgetListActivity.this,acctype));

                            isAlreadybudgetAdded=false;

                            getBudgetData();


                        }catch (Exception e)
                        {

                        }

                    }
                });

                expenseDialogFragment.show(getSupportFragmentManager(),"cdxkv");



            }
        });



    }

    @Override
    protected void onRestart() {
        super.onRestart();

        getBudgetData();
    }

    public void getBudgetData()
    {
        List<CommonData>budgetsdata=new ArrayList<>();

        try {


            String year = spinnerYear.getSelectedItem().toString();

            String accname = commonData_selected.getId();

            List<CommonData> budgets = new DatabaseHelper(BudgetListActivity.this).getData(Utils.DBtables.TABLE_BUDGET);


            double total=0;

            if (budgets.size() > 0) {

                for (CommonData budget : budgets) {

                    JSONObject jsonObject=new JSONObject(budget.getData());


                    String year1= jsonObject.getString("year");
                    String amount= jsonObject.getString("amount");

                    String accountname= jsonObject.getString("accountname");
                    if(year.equalsIgnoreCase(year1)&&accname.equalsIgnoreCase(accountname))
                    {

                        total=total+Double.parseDouble(amount);
                        budgetsdata.add(budget);
                    }



                }

                if(budgetsdata.size()>0) {

                    layout_title.setVisibility(View.VISIBLE);

                    recycler.setVisibility(View.VISIBLE);
                    txtTotal.setVisibility(View.VISIBLE);

                    isAlreadybudgetAdded=true;
                    txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+this.getString(R.string.rs));
                }
                else {

                    layout_title.setVisibility(View.GONE);

                    recycler.setVisibility(View.GONE);

                    txtTotal.setVisibility(View.GONE);
                    isAlreadybudgetAdded=false;

                    //Toast.makeText(BudgetListActivity.this,resources.getString(R.string.nodatafound),Toast.LENGTH_SHORT).show();

                }

            } else {
                layout_title.setVisibility(View.GONE);

                recycler.setVisibility(View.GONE);

                txtTotal.setVisibility(View.GONE);
                isAlreadybudgetAdded=false;

               // Toast.makeText(BudgetListActivity.this,resources.getString(R.string.nodatafound),Toast.LENGTH_SHORT).show();



            }


        }catch (Exception e)
        {

        }



        BudgetDataAdapter budgetDataAdapter=new BudgetDataAdapter(BudgetListActivity.this,budgetsdata, null);
        recycler.setLayoutManager(new LinearLayoutManager(BudgetListActivity.this));
        recycler.setAdapter(budgetDataAdapter);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }


    public void addAccountSettings()
    {


        List<CommonData> commonData=new DatabaseHelper(BudgetListActivity.this).getAccountSettingsData();

        if(commonData.size()>0)
        {

            Collections.reverse(commonData);


            for (CommonData cm:commonData) {



                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype= jsonObject.getString("Accounttype");

                    if(acctype.equalsIgnoreCase("Expense account"))
                    {
                        cmfiltered.add(cm);
                   }


                    //




                }catch (Exception e)
                {

                }

            }


            AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(BudgetListActivity.this, cmfiltered);
            spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);

        }


    }
}
