package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BusinesscardBgAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.VisitCard;
import com.centroid.integraaccounts.data.domain.VisitCardImg;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class VisitCardFillActivity extends AppCompatActivity {

    ImageView imgback;

    EditText edtLandPhone,edtSaveApplink, edtCouponcode, edtYoutubelink, edtInstagram, edtfacebook, edtName,edtPhone,edtemail,edtAddress,edtwhatsapp,edtDesignation,edtWebsite,edtCompany;
    Button btnsubmit;

    ViewPager viewpager;

    FloatingActionButton fabupload;
    TabLayout tabslayout;

    int PICK_IMAGE=11,VISITCARD_BG_PICK_IMAGE=12;

    ImageView imglogo;
    Bitmap cropped =null;

    TextView txtHead,txtCard,txtlogohead;

    int changeposition=0;
    String vcardid="0";

    List<VisitCardImg>vcardimg_renewd=new ArrayList<>();



    VisitCard visitCard=null;
   // int arrimgs[]={R.drawable.cone,R.drawable.ctwo,R.drawable.cthree,R.drawable.cfour,R.drawable.cfive,R.drawable.csix,R.drawable.cseven,R.drawable.ceight};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_card_fill);
        getSupportActionBar().hide();




        imgback=findViewById(R.id.imgback);
        edtName=findViewById(R.id.edtName);
        edtPhone=findViewById(R.id.edtPhone);
        edtemail=findViewById(R.id.edtemail);
        edtAddress=findViewById(R.id.edtAddress);
        btnsubmit=findViewById(R.id.btnsubmit);
        viewpager=findViewById(R.id.viewpager);
        tabslayout=findViewById(R.id.tabslayout);
        fabupload=findViewById(R.id.fabupload);
        imglogo=findViewById(R.id.imglogo);
        edtwhatsapp=findViewById(R.id.whatsapp);
        edtDesignation=findViewById(R.id.edtDesignation);
        edtWebsite=findViewById(R.id.edtWebsite);
        edtCompany=findViewById(R.id.edtCompany);
        txtHead=findViewById(R.id.txtHead);
        txtCard=findViewById(R.id.txtCard);
        txtlogohead=findViewById(R.id.txtlogohead);

        edtfacebook=findViewById(R.id.edtfacebook);
        edtInstagram=findViewById(R.id.edtInstagram);
        edtYoutubelink=findViewById(R.id.edtYoutubelink);
        edtCouponcode=findViewById(R.id.edtCouponcode);
        edtSaveApplink=findViewById(R.id.edtSaveApplink);
        edtLandPhone=findViewById(R.id.edtLandPhone);




        visitCard=(VisitCard)getIntent().getSerializableExtra("data");
        showVisitCardDesigns();

        String languagedata = LocaleHelper.getPersistedData(VisitCardFillActivity.this, "en");
        Context context= LocaleHelper.setLocale(VisitCardFillActivity.this, languagedata);

        Resources resources=context.getResources();
        txtHead.setText(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.yourbusinesscarddetails)));
        txtCard.setText(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.businesscardbg)));
        edtName.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.name)));
        edtPhone.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.phonenumber)));
        edtwhatsapp.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.whatsappno)));
        edtemail.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.email)));
        edtCompany.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.companyname)));
        edtDesignation.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.designationprof)));
        edtAddress.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.companyaddress)));
        edtWebsite.setHint(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.website)));
        txtlogohead.setText(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.companylogo)));
        btnsubmit.setText(Utils.getCapsSentences(VisitCardFillActivity.this,resources.getString(R.string.submit)));



        if(visitCard!=null)
        {
            try {


                JSONObject jsonObject1 = new JSONObject(visitCard.getData());
                edtName.setText(jsonObject1.getString("name"));
                edtPhone.setText(jsonObject1.getString("phone"));
                edtemail.setText(jsonObject1.getString("email"));

                edtAddress.setText(jsonObject1.getString("address"));
                edtwhatsapp.setText(jsonObject1.getString("whatsapp"));

                edtDesignation.setText(jsonObject1.getString("designation"));
                edtWebsite.setText(jsonObject1.getString("website"));

                edtCompany.setText(jsonObject1.getString("company"));

                if(jsonObject1.has("couponcode"))
                {
                    edtCouponcode.setText(jsonObject1.getString("couponcode"));
                }



//

                if(jsonObject1.has("fb"))
                {
                    edtfacebook.setText(jsonObject1.getString("fb"));
                }

                if(jsonObject1.has("youtube"))
                {
                    edtYoutubelink.setText(jsonObject1.getString("youtube"));
                }

                if(jsonObject1.has("instagram"))
                {
                    edtInstagram.setText(jsonObject1.getString("instagram"));
                }


                if(jsonObject1.has("landphone"))
                {
                    edtLandPhone.setText(jsonObject1.getString("landphone"));
                }

                if(jsonObject1.has("saveapplink"))
                {
                    edtSaveApplink.setText(jsonObject1.getString("saveapplink"));
                }




                int d=jsonObject1.getInt("cardbg");

              //  int arrimgs[]={R.drawable.cardone,R.drawable.cardtwo,R.drawable.cardthree,R.drawable.cardfour,R.drawable.cardfive,R.drawable.vcseven};

                List<VisitCardImg>vcardimg=new DatabaseHelper(VisitCardFillActivity.this).getImgData();

                for (int i=0;i<vcardimg.size();i++)
                {

                    if(d==i)
                    {


                        viewpager.setCurrentItem(i);

                        break;

                    }

                }


                if(visitCard.getBimage()!=null)
                {

                    byte[]imgbytes=visitCard.getBimage();

                   cropped = BitmapFactory.decodeByteArray(imgbytes, 0,
                            imgbytes.length);
                    imglogo.setImageBitmap(cropped);


                }




            }catch (Exception e)
            {

            }


        }






        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showVisitCard();



            }
        });

        fabupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(VisitCardFillActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {

                    ActivityCompat.requestPermissions(VisitCardFillActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},111);

                }
                else {

                    pickImage();

                }

            }
        });
    }



    public void pickImage()
    {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == VISITCARD_BG_PICK_IMAGE && resultCode == RESULT_OK && data != null) {

           Uri uri= data.getData();

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File f=new File(picturePath);

            if(f.exists()) {

                int file_size = Integer.parseInt(String.valueOf(f.length() / 1024));

                if(file_size<100) {


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;

                    final Dialog d = new Dialog(VisitCardFillActivity.this);
                    d.setContentView(R.layout.layout_cropdialog);

                    ImageView imgback = d.findViewById(R.id.imgback);

                    ImageView imgcrop = d.findViewById(R.id.imgcrop);

                    final CropImageView cropImageView = d.findViewById(R.id.cropImageView);

                    cropImageView.setImageUriAsync(uri);

                    imgback.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            d.dismiss();
                        }
                    });

                    imgcrop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            d.dismiss();

                            try {
                                Bitmap cp = cropImageView.getCroppedImage();





                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                cp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                byte[] bitmapdata = stream.toByteArray();


                                new DatabaseHelper(VisitCardFillActivity.this).updateVisitcardImgData(bitmapdata, vcardid + "");
                                Utils.playSimpleTone(VisitCardFillActivity.this);

                                showVisitCardDesigns();


                            } catch (Exception e) {

                            }


                        }
                    });

                    d.getWindow().setLayout(width, height);
                    d.show();
                }
                else{

                    Utils.showAlertWithSingle(VisitCardFillActivity.this,"File size must be less than 100kb",null);
                }

            }
            else{

                Utils.showAlertWithSingle(VisitCardFillActivity.this,"File not found",null);
            }

        }


      else  if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {

            Uri uri= data.getData();

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File f=new File(picturePath);

            if(f.exists()) {

                int file_size = Integer.parseInt(String.valueOf(f.length() / 1024));

                if(file_size<100) {


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;

                    final Dialog d = new Dialog(VisitCardFillActivity.this);
                    d.setContentView(R.layout.layout_cropdialog);

                    ImageView imgback = d.findViewById(R.id.imgback);

                    ImageView imgcrop = d.findViewById(R.id.imgcrop);

                    final CropImageView cropImageView = d.findViewById(R.id.cropImageView);

                    cropImageView.setImageUriAsync(data.getData());

                    imgback.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            d.dismiss();
                        }
                    });

                    imgcrop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            d.dismiss();

                            try {
                                cropped = cropImageView.getCroppedImage();


                                imglogo.setImageBitmap(cropped);


                            } catch (Exception e) {

                                Log.e("TAAAAG", e.toString());
                            }


                        }
                    });

                    d.getWindow().setLayout(width, height);
                    d.show();
                }
                else{

                    Utils.showAlertWithSingle(VisitCardFillActivity.this,"File size must be less than 100kb",null);


                }

            }
            else{

                Utils.showAlertWithSingle(VisitCardFillActivity.this,"File not found",null);

            }


        }

    }





    public void showVisitCardDesigns()
    {
        List<Drawable>img=new ArrayList<>();

        List<VisitCardImg>vcardimg=new DatabaseHelper(VisitCardFillActivity.this).getImgData();


        vcardimg_renewd.clear();
        viewpager.removeAllViews();

//        if(vcardimg.size()>0)
//        {
//
//            Utils.showAlertWithSingle(VisitCardFillActivity.this,"image exist",null);
//
//        }
//        else{
//
//            Utils.showAlertWithSingle(VisitCardFillActivity.this,"image does not exist exist",null);
//        }



        if(tabslayout.getTabCount()>0)
        {
            tabslayout.removeAllTabs();
        }


        for (int i=0;i<vcardimg.size();i++)
        {

            VisitCardImg v=new VisitCardImg();

            Drawable image = new BitmapDrawable(BitmapFactory.decodeByteArray(vcardimg.get(i).getArrdata(), 0, vcardimg.get(i).getArrdata().length));


            img.add(image);

            v.setDr(image);
            v.setId(vcardimg.get(i).getId());
            v.setArrdata(vcardimg.get(i).getArrdata());
            vcardimg_renewd.add(v);

            tabslayout.addTab(tabslayout.newTab());

        }


//        for (int i=0;i<Utils.arrimgs.length;i++)
//        {
//
//            img.add(Utils.arrimgs[i]);
//
//            tabslayout.addTab(tabslayout.newTab());
//
//        }

        viewpager.setAdapter(new BusinesscardBgAdapter(VisitCardFillActivity.this,img,vcardimg_renewd));

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                tabslayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewpager.setCurrentItem(changeposition);
    }


    public void changeBackground(int position,String id)
    {

        changeposition=position;
        vcardid=id;


        if(ContextCompat.checkSelfPermission(VisitCardFillActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(gallery, VISITCARD_BG_PICK_IMAGE);


        }
        else{


            ActivityCompat.requestPermissions(VisitCardFillActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);

        }

    }


    public VisitCardFillActivity() {
        super();
    }

    public void showVisitCard()
    {

        try {
            byte[] bArray = null;

            if (cropped != null) {

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                cropped.compress(Bitmap.CompressFormat.PNG, 100, bos);
                bArray = bos.toByteArray();
            }

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("name", edtName.getText().toString());
            jsonObject1.put("phone", edtPhone.getText().toString());
            jsonObject1.put("email", edtemail.getText().toString());

            jsonObject1.put("address", edtAddress.getText().toString());
            jsonObject1.put("whatsapp", edtwhatsapp.getText().toString());
            jsonObject1.put("designation", edtDesignation.getText().toString());
            jsonObject1.put("website", edtWebsite.getText().toString());
            jsonObject1.put("company", edtCompany.getText().toString());

            jsonObject1.put("cardbg", viewpager.getCurrentItem());
            jsonObject1.put("couponcode", edtCouponcode.getText().toString());

            jsonObject1.put("landphone", edtLandPhone.getText().toString());

            jsonObject1.put("saveapplink", edtSaveApplink.getText().toString());


            jsonObject1.put("fb", edtfacebook.getText().toString());
            jsonObject1.put("youtube", edtYoutubelink.getText().toString());
            jsonObject1.put("instagram", edtInstagram.getText().toString());

            List<VisitCardImg>vcardimg=new DatabaseHelper(VisitCardFillActivity.this).getImgData();

            byte[] arrcard=null;

            for(int i=0;i<vcardimg.size();i++)
            {
                if(i==viewpager.getCurrentItem()) {
                    arrcard = vcardimg.get(i).getArrdata();
                }


            }



            addVisitCardtoDatabase(jsonObject1.toString(), bArray,arrcard);
        }catch (Exception e)
        {

        }



        TextView txtname,txtEmail,txtphone,txtAddress,txtDesignation,txtwhatsapp,txtCompany,txtWebsite,txtSaveApplink,txtCouponCode;
        TextView txtfblink,txtyoutubelink,txtinstagram;
        ImageView imgqr,imglogodialog,imgbg,imgph,imgwhts,imgmail,imgloc,imgcompany;
        LinearLayout layout_imgdialog, layout_socialmedia, layout_fblink,layout_youtubelink,layout_instagramlink, layout_website,layout_phone,layout_whatsapp,layout_imglogo,layout_email,layout_address,layout_company;
        Button btnSave;
        CardView card;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(VisitCardFillActivity.this);
        dialog.setContentView(R.layout.layout_visitcard);
        String languagedata = LocaleHelper.getPersistedData(VisitCardFillActivity.this, "en");
        Context context= LocaleHelper.setLocale(VisitCardFillActivity.this, languagedata);

        Resources resources=context.getResources();



        dialog.setTitle(resources.getString(R.string.visitingcard));
        dialog.getWindow().setLayout(width ,height );
        layout_socialmedia=dialog.findViewById(R.id.layout_socialmedia);
        layout_fblink=dialog.findViewById(R.id.layout_fblink);
        layout_youtubelink=dialog.findViewById(R.id.layout_youtubelink);
        layout_instagramlink=dialog.findViewById(R.id.layout_instagramlink);

        txtfblink=dialog.findViewById(R.id.txtfblink);
        txtyoutubelink=dialog.findViewById(R.id.txtyoutubelink);
        txtinstagram=dialog.findViewById(R.id.txtinstagram);



        txtWebsite=dialog.findViewById(R.id.txtWebsite);
        txtAddress=dialog.findViewById(R.id.txtAddress);
        txtphone=dialog.findViewById(R.id.txtphone);
        txtEmail=dialog.findViewById(R.id.txtEmail);
        txtname=dialog.findViewById(R.id.txtname);
        btnSave=dialog.findViewById(R.id.btnSave);
        txtDesignation=dialog.findViewById(R.id.txtDesignation);
        txtwhatsapp=dialog.findViewById(R.id.txtwhatsapp);
        txtCompany=dialog.findViewById(R.id.txtCompany);
        layout_phone=dialog.findViewById(R.id.layout_phone);
        layout_whatsapp=dialog.findViewById(R.id.layout_whatsapp);
        layout_imglogo=dialog.findViewById(R.id.layout_imglogo);
        layout_imgdialog=dialog.findViewById(R.id.layout_imgdialog);
        layout_email=dialog.findViewById(R.id.layout_email);
        layout_address=dialog.findViewById(R.id.layout_address);
        layout_company=dialog.findViewById(R.id.layout_company);
        layout_website=dialog.findViewById(R.id.layout_website);
        imgqr=dialog.findViewById(R.id.imgqr);
        imgbg=dialog.findViewById(R.id.imgbg);
        card=dialog.findViewById(R.id.card);

        imglogodialog=dialog.findViewById(R.id.imglogodialog);
        imgph=dialog.findViewById(R.id.imgph);
        imgwhts=dialog.findViewById(R.id.imgwhts);
        imgmail=dialog.findViewById(R.id.imgmail);
        imgloc=dialog.findViewById(R.id.imgloc);
        imgcompany=dialog.findViewById(R.id.imgcompany);

        txtSaveApplink=dialog.findViewById(R.id.txtSaveApplink);
        txtCouponCode=dialog.findViewById(R.id.txtCouponCode);
       LinearLayout layout_landph=dialog.findViewById(R.id.layout_landph);
        TextView txtlandph=dialog.findViewById(R.id.txtlandph);

        if(!edtLandPhone.getText().toString().equalsIgnoreCase(""))
        {
            txtlandph.setText(edtLandPhone.getText().toString());

        }
        else{

            layout_landph.setVisibility(View.GONE);
        }

        if(!edtCouponcode.getText().toString().equalsIgnoreCase(""))
        {

            txtCouponCode.setText(edtCouponcode.getText().toString());
        }

        if(!edtSaveApplink.getText().toString().equalsIgnoreCase(""))
        {

            txtSaveApplink.setText(edtSaveApplink.getText().toString());
        }




       // int draw=Utils.arrimgs[viewpager.getCurrentItem()];


        List<VisitCardImg>vcardimg=new DatabaseHelper(VisitCardFillActivity.this).getImgData();

        byte[] arrcard=null;

        for(int i=0;i<vcardimg.size();i++)
        {
            if(i==viewpager.getCurrentItem()) {
                arrcard = vcardimg.get(i).getArrdata();
                Drawable image = new BitmapDrawable(BitmapFactory.decodeByteArray(arrcard, 0, arrcard.length));
                imgbg.setImageDrawable(image);
            }


        }







        if(cropped!=null)
        {
            imglogodialog.setImageBitmap(cropped);
        }
        else {

            layout_imgdialog.setVisibility(View.GONE);
        }


        if(edtYoutubelink.getText().toString().equalsIgnoreCase("")&&edtfacebook.getText().toString().equalsIgnoreCase("")&&edtInstagram.getText().toString().equalsIgnoreCase(""))
        {

            layout_socialmedia.setVisibility(View.GONE);
        }
        else {

            if(!edtYoutubelink.getText().toString().equalsIgnoreCase(""))
            {
                txtyoutubelink.setText(edtYoutubelink.getText().toString());
            }
            else{

                layout_youtubelink.setVisibility(View.GONE);
            }

            if(!edtfacebook.getText().toString().equalsIgnoreCase(""))
            {
                txtfblink.setText(edtfacebook.getText().toString());
            }
            else{

                layout_fblink.setVisibility(View.GONE);
            }


            if(!edtInstagram.getText().toString().equalsIgnoreCase(""))
            {
                txtinstagram.setText(edtInstagram.getText().toString());
            }
            else{

                layout_instagramlink.setVisibility(View.GONE);
            }


        }




        if(!edtName.getText().toString().equalsIgnoreCase(""))
        {

            txtname.setText(edtName.getText().toString());
        }
        else {
            txtname.setVisibility(View.GONE);
        }


        if(!edtPhone.getText().toString().equalsIgnoreCase(""))
        {

            txtphone.setText(" "+edtPhone.getText().toString());
        }
        else {

            layout_phone.setVisibility(View.GONE);
        }



        if(!edtemail.getText().toString().equalsIgnoreCase(""))
        {
            txtEmail.setText(edtemail.getText().toString());
        }
        else {

            layout_email.setVisibility(View.GONE);
        }

        if(!edtAddress.getText().toString().equalsIgnoreCase(""))
        {

            txtAddress.setText(edtAddress.getText().toString());
        }
        else {

            layout_address.setVisibility(View.GONE);
        }



        if(!edtCompany.getText().toString().equalsIgnoreCase(""))
        {

            txtCompany.setText(edtCompany.getText().toString());
        }
        else {

            layout_company.setVisibility(View.GONE);
        }




        if(!edtwhatsapp.getText().toString().equalsIgnoreCase(""))
        {

            txtwhatsapp.setText(" "+edtwhatsapp.getText().toString());
        }
        else {

            layout_whatsapp.setVisibility(View.GONE);
        }





        if(!edtDesignation.getText().toString().equalsIgnoreCase(""))
        {
            txtDesignation.setText(edtDesignation.getText().toString());
        }
        else {

            txtDesignation.setVisibility(View.GONE);
        }







        String text="";

        if(!edtWebsite.getText().toString().equalsIgnoreCase("")) {

             text = edtWebsite.getText().toString();

             txtWebsite.setText(text);

        }
        else {

            layout_website.setVisibility(View.GONE);
            text=getString(R.string.visitsite);

        }





        // Whatever you need to encode in the QR code
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imgqr.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        btnSave.setText(resources.getString(R.string.share));

        TextPaint textPaint=new TextPaint();
        textPaint.setStrokeWidth(10);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setColor(Color.parseColor("#ffffff"));


//        txtname.setLayerPaint(textPaint);
        txtEmail.setLayerPaint(textPaint);
        txtphone.setLayerPaint(textPaint);
        txtAddress.setLayerPaint(textPaint);

        txtDesignation.setLayerPaint(textPaint);
        txtwhatsapp.setLayerPaint(textPaint);
        txtCompany.setLayerPaint(textPaint);

        txtWebsite.setLayerPaint(textPaint);
        txtSaveApplink.setLayerPaint(textPaint);
        txtCouponCode.setLayerPaint(textPaint);

        txtfblink.setLayerPaint(textPaint);
        txtyoutubelink.setLayerPaint(textPaint);
        txtinstagram.setLayerPaint(textPaint);










        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();

                String arr[]={"share as Text","Share as Image"};



                new AlertDialog.Builder(VisitCardFillActivity.this)
                        .setSingleChoiceItems(arr, 0, null)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                                // Do something useful withe the position of the selected radio button

                                if(selectedPosition==0)
                                {
                                    shareVisitCardAsText(card);
                                }
                                else  if(selectedPosition==1){
                                    sharevisitCardimage(card);
                                }
//                                else  if(selectedPosition==2){
//                                    shareVisitCardAsPdf(card);
//
//                                }

                            }
                        })
                        .show();







            }
        });




        dialog.show();
    }


    public void sharevisitCardimage(CardView card)
    {

        String permission="";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

            permission=Manifest.permission.READ_MEDIA_IMAGES;

        }
        else{

            permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
        }


        if(ContextCompat.checkSelfPermission(VisitCardFillActivity.this, permission)== PackageManager.PERMISSION_GRANTED) {


            try {



                Bitmap bitmap = getBitmapFromView(card);

                /*
                 *
                 *
                 * portion for uploading bitmap data to database*/

                byte[] bArray =null;

                if(cropped!=null) {

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    cropped.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    bArray = bos.toByteArray();
                }



//                        int arrimgs[]={R.drawable.cone,R.drawable.ctwo,R.drawable.cthree,R.drawable.cfour,R.drawable.cfive,R.drawable.csix,R.drawable.cseven,R.drawable.ceight};

                int imagedata=Utils.arrimgs[viewpager.getCurrentItem()];


//                JSONObject jsonObject1=new JSONObject();
//                jsonObject1.put("name",edtName.getText().toString());
//                jsonObject1.put("phone",edtPhone.getText().toString());
//                jsonObject1.put("email",edtemail.getText().toString());
//
//                jsonObject1.put("address",edtAddress.getText().toString());
//                jsonObject1.put("whatsapp",edtwhatsapp.getText().toString());
//                jsonObject1.put("designation",edtDesignation.getText().toString());
//                jsonObject1.put("website",edtWebsite.getText().toString());
//                jsonObject1.put("company",edtCompany.getText().toString());
//
//                jsonObject1.put("cardbg",viewpager.getCurrentItem());
//
//
//                jsonObject1.put("fb",edtfacebook.getText().toString());
//                jsonObject1.put("youtube",edtYoutubelink.getText().toString());
//                jsonObject1.put("instagram",edtInstagram.getText().toString());
//
//
//                addVisitCardtoDatabase(jsonObject1.toString(),bArray);


                /* uploading visitcard data to database
                 *
                 *
                 *
                 *
                 *
                 *
                 *
                 * */



                Long tsLong = System.currentTimeMillis() / 1000;
                String ts = tsLong.toString();

                File fp = new File(VisitCardFillActivity.this.getExternalCacheDir() + "/Save/Visitingcard");


                if (!fp.exists()) {
                    fp.mkdirs();
                }

                File f = new File(fp.getAbsolutePath() + "/visitcard_" + ts + ".png");
                if (!f.exists()) {
                    f.createNewFile();

                } else {

                    f.delete();
                    f.createNewFile();
                }


                FileOutputStream output = new FileOutputStream(f);


                bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                output.close();


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    Uri photoURI = FileProvider.getUriForFile(VisitCardFillActivity.this, getApplicationContext().getPackageName() + ".provider", f);


                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                    startActivity(Intent.createChooser(intent, "Share Image"));


                } else {


                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                    startActivity(Intent.createChooser(intent, "Share Image"));
                }

            } catch (Exception e) {

            }
        }
        else {


            ActivityCompat.requestPermissions(VisitCardFillActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},11);


        }
    }

    public void shareVisitCardAsPdf(CardView card)
    {


        try {

            Bitmap bitmap = getBitmapFromView(card);

            /*
             *
             *
             * portion for uploading bitmap data to database*/

            byte[] bArray = null;

            if (cropped != null) {

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                cropped.compress(Bitmap.CompressFormat.PNG, 100, bos);
                bArray = bos.toByteArray();
            }


//                        int arrimgs[]={R.drawable.cone,R.drawable.ctwo,R.drawable.cthree,R.drawable.cfour,R.drawable.cfive,R.drawable.csix,R.drawable.cseven,R.drawable.ceight};

            int imagedata = Utils.arrimgs[viewpager.getCurrentItem()];


//            JSONObject jsonObject1 = new JSONObject();
//            jsonObject1.put("name", edtName.getText().toString());
//            jsonObject1.put("phone", edtPhone.getText().toString());
//            jsonObject1.put("email", edtemail.getText().toString());
//
//            jsonObject1.put("address", edtAddress.getText().toString());
//            jsonObject1.put("whatsapp", edtwhatsapp.getText().toString());
//            jsonObject1.put("designation", edtDesignation.getText().toString());
//            jsonObject1.put("website", edtWebsite.getText().toString());
//            jsonObject1.put("company", edtCompany.getText().toString());
//
//            jsonObject1.put("cardbg", viewpager.getCurrentItem());
//
//
//            jsonObject1.put("fb", edtfacebook.getText().toString());
//            jsonObject1.put("youtube", edtYoutubelink.getText().toString());
//            jsonObject1.put("instagram", edtInstagram.getText().toString());
//
//
//            addVisitCardtoDatabase(jsonObject1.toString(), bArray);
        }catch (Exception e)
        {

        }

        StringBuilder sb=new StringBuilder();


        if(!edtName.getText().toString().equalsIgnoreCase(""))
        {

            // txtname.setText(edtName.getText().toString());
            sb.append("Name : "+edtName.getText().toString()+"\n");
        }
        else {
            // txtname.setVisibility(View.GONE);
        }


        if(!edtPhone.getText().toString().equalsIgnoreCase(""))
        {

            // txtphone.setText("+91 "+edtPhone.getText().toString());

            sb.append("Phone number : "+"+91 "+edtPhone.getText().toString()+"\n");
        }
        else {

            // layout_phone.setVisibility(View.GONE);
        }



        if(!edtemail.getText().toString().equalsIgnoreCase(""))
        {
            // txtEmail.setText(edtemail.getText().toString());

            sb.append("Email : "+edtemail.getText().toString()+"\n");
        }
        else {

            // layout_email.setVisibility(View.GONE);
        }

        if(!edtAddress.getText().toString().equalsIgnoreCase(""))
        {

            sb.append("Address : "+edtAddress.getText().toString()+"\n");
        }
        else {

            //  layout_address.setVisibility(View.GONE);
        }



        if(!edtCompany.getText().toString().equalsIgnoreCase(""))
        {

            sb.append("Company : "+edtCompany.getText().toString()+"\n");
        }
        else {


        }




        if(!edtwhatsapp.getText().toString().equalsIgnoreCase(""))
        {

            // txtwhatsapp.setText("+91 "+edtwhatsapp.getText().toString());

            sb.append("Whatsapp Number : "+"+91 "+edtwhatsapp.getText().toString()+"\n");
        }
        else {

            //layout_whatsapp.setVisibility(View.GONE);
        }





        if(!edtDesignation.getText().toString().equalsIgnoreCase(""))
        {
            //  txtDesignation.setText(edtDesignation.getText().toString());

            sb.append("Designation : "+edtDesignation.getText().toString()+"\n");

        }
        else {

            // txtDesignation.setVisibility(View.GONE);
        }







        String text="";

        if(!edtWebsite.getText().toString().equalsIgnoreCase("")) {

            text = edtWebsite.getText().toString();

            //  txtWebsite.setText(text);

            sb.append("Website : "+text+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }


        if(!edtCouponcode.getText().toString().equalsIgnoreCase("")) {

            ;

            //  txtWebsite.setText(text);

            sb.append("Coupon code : "+edtCouponcode.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }


        if(!edtLandPhone.getText().toString().equalsIgnoreCase("")) {

            ;

            //  txtWebsite.setText(text);

            sb.append("Land phone : "+edtLandPhone.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }

        if(!edtSaveApplink.getText().toString().equalsIgnoreCase("")) {

            ;

            //  txtWebsite.setText(text);

            sb.append("Save app link : "+edtSaveApplink.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }





        if(!edtYoutubelink.getText().toString().equalsIgnoreCase(""))
        {
            // txtyoutubelink.setText(edtYoutubelink.getText().toString());

            sb.append("Youtube link : "+edtYoutubelink.getText().toString()+"\n");
        }
        else{

            //layout_youtubelink.setVisibility(View.GONE);
        }

        if(!edtfacebook.getText().toString().equalsIgnoreCase(""))
        {
            sb.append("Facebook link : "+edtfacebook.getText().toString()+"\n");
        }
        else{

            //  layout_fblink.setVisibility(View.GONE);
        }


        if(!edtInstagram.getText().toString().equalsIgnoreCase(""))
        {
            // txtinstagram.setText(edtInstagram.getText().toString());

            sb.append("Instagram link : "+edtInstagram.getText().toString()+"\n");
        }
        else{

            //  layout_instagramlink.setVisibility(View.GONE);
        }









        String sharetext="";

        sharetext=sb.toString();
        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "IntegraAccounts");

            if (!folder.exists()) {
                folder.mkdir();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }

            PdfWriter pdfWriter = new PdfWriter(file);
            PdfDocument doc = new PdfDocument(pdfWriter);
//
//        PdfFont fontArial
//                = PdfFont.create("C:\\WINDOWS\\Fonts\\ARIAL.TTF", // font file
//                10,  // font size
//                PdfEncodings.UTF_16BE, // font encoding
//                PdfFont.EMBED_SUBSET);


            Document document = new Document(doc);
            Paragraph englishText = new Paragraph(sharetext.toString());


            document.add(englishText);
//
            document.close();




            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(VisitCardFillActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }

        }catch (Exception e)
        {

        }

//        Intent shareIntent = new Intent(Intent.ACTION_SEND);
//
//        shareIntent.setType("text/plain");
//
//        shareIntent.putExtra(Intent.EXTRA_TEXT, sharetext);
//
//        startActivity(Intent.createChooser(shareIntent, "Share visit card data"));
    }


    public void shareVisitCardAsText(CardView card)
    {


        try {

            Bitmap bitmap = getBitmapFromView(card);

            /*
             *
             *
             * portion for uploading bitmap data to database*/

            byte[] bArray = null;

            if (cropped != null) {

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                cropped.compress(Bitmap.CompressFormat.PNG, 100, bos);
                bArray = bos.toByteArray();
            }


//                        int arrimgs[]={R.drawable.cone,R.drawable.ctwo,R.drawable.cthree,R.drawable.cfour,R.drawable.cfive,R.drawable.csix,R.drawable.cseven,R.drawable.ceight};

            int imagedata = Utils.arrimgs[viewpager.getCurrentItem()];


//            JSONObject jsonObject1 = new JSONObject();
//            jsonObject1.put("name", edtName.getText().toString());
//            jsonObject1.put("phone", edtPhone.getText().toString());
//            jsonObject1.put("email", edtemail.getText().toString());
//
//            jsonObject1.put("address", edtAddress.getText().toString());
//            jsonObject1.put("whatsapp", edtwhatsapp.getText().toString());
//            jsonObject1.put("designation", edtDesignation.getText().toString());
//            jsonObject1.put("website", edtWebsite.getText().toString());
//            jsonObject1.put("company", edtCompany.getText().toString());
//
//            jsonObject1.put("cardbg", viewpager.getCurrentItem());
//
//
//            jsonObject1.put("fb", edtfacebook.getText().toString());
//            jsonObject1.put("youtube", edtYoutubelink.getText().toString());
//            jsonObject1.put("instagram", edtInstagram.getText().toString());
//
//
//            addVisitCardtoDatabase(jsonObject1.toString(), bArray);
        }catch (Exception e)
        {

        }

        StringBuilder sb=new StringBuilder();


        if(!edtName.getText().toString().equalsIgnoreCase(""))
        {

           // txtname.setText(edtName.getText().toString());
            sb.append("Name : "+edtName.getText().toString()+"\n");
        }
        else {
           // txtname.setVisibility(View.GONE);
        }


        if(!edtPhone.getText().toString().equalsIgnoreCase(""))
        {

           // txtphone.setText("+91 "+edtPhone.getText().toString());

            sb.append("Phone number : "+"+91 "+edtPhone.getText().toString()+"\n");
        }
        else {

           // layout_phone.setVisibility(View.GONE);
        }



        if(!edtemail.getText().toString().equalsIgnoreCase(""))
        {
           // txtEmail.setText(edtemail.getText().toString());

            sb.append("Email : "+edtemail.getText().toString()+"\n");
        }
        else {

           // layout_email.setVisibility(View.GONE);
        }

        if(!edtAddress.getText().toString().equalsIgnoreCase(""))
        {

            sb.append("Address : "+edtAddress.getText().toString()+"\n");
        }
        else {

          //  layout_address.setVisibility(View.GONE);
        }



        if(!edtCompany.getText().toString().equalsIgnoreCase(""))
        {

            sb.append("Company : "+edtCompany.getText().toString()+"\n");
        }
        else {


        }




        if(!edtwhatsapp.getText().toString().equalsIgnoreCase(""))
        {

           // txtwhatsapp.setText("+91 "+edtwhatsapp.getText().toString());

            sb.append("Whatsapp Number : "+"+91 "+edtwhatsapp.getText().toString()+"\n");
        }
        else {

            //layout_whatsapp.setVisibility(View.GONE);
        }





        if(!edtDesignation.getText().toString().equalsIgnoreCase(""))
        {
          //  txtDesignation.setText(edtDesignation.getText().toString());

            sb.append("Designation : "+edtDesignation.getText().toString()+"\n");

        }
        else {

           // txtDesignation.setVisibility(View.GONE);
        }







        String text="";

        if(!edtWebsite.getText().toString().equalsIgnoreCase("")) {

            text = edtWebsite.getText().toString();

          //  txtWebsite.setText(text);

            sb.append("Website : "+text+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
           // text=getString(R.string.visitsite);

        }


        if(!edtCouponcode.getText().toString().equalsIgnoreCase("")) {

          ;

            //  txtWebsite.setText(text);

            sb.append("Coupon code : "+edtCouponcode.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }


        if(!edtLandPhone.getText().toString().equalsIgnoreCase("")) {

            ;

            //  txtWebsite.setText(text);

            sb.append("Land phone : "+edtLandPhone.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }

        if(!edtSaveApplink.getText().toString().equalsIgnoreCase("")) {

            ;

            //  txtWebsite.setText(text);

            sb.append("Save app link : "+edtSaveApplink.getText().toString()+"\n");

        }
        else {

            //layout_website.setVisibility(View.GONE);
            // text=getString(R.string.visitsite);

        }





        if(!edtYoutubelink.getText().toString().equalsIgnoreCase(""))
        {
           // txtyoutubelink.setText(edtYoutubelink.getText().toString());

            sb.append("Youtube link : "+edtYoutubelink.getText().toString()+"\n");
        }
        else{

            //layout_youtubelink.setVisibility(View.GONE);
        }

        if(!edtfacebook.getText().toString().equalsIgnoreCase(""))
        {
            sb.append("Facebook link : "+edtfacebook.getText().toString()+"\n");
        }
        else{

          //  layout_fblink.setVisibility(View.GONE);
        }


        if(!edtInstagram.getText().toString().equalsIgnoreCase(""))
        {
           // txtinstagram.setText(edtInstagram.getText().toString());

            sb.append("Instagram link : "+edtInstagram.getText().toString()+"\n");
        }
        else{

          //  layout_instagramlink.setVisibility(View.GONE);
        }









        String sharetext="";

        sharetext=sb.toString();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);

        shareIntent.setType("text/plain");

        shareIntent.putExtra(Intent.EXTRA_TEXT, sharetext);

        startActivity(Intent.createChooser(shareIntent, "Share visit card data"));
    }




    public void addVisitCardtoDatabase(String data,byte[]arr,byte[]cardimg)
    {
        if(visitCard!=null)
        {
            new DatabaseHelper(VisitCardFillActivity.this).updateVisitCardData(visitCard.getId()+"",data,arr,cardimg);
            Utils.playSimpleTone(VisitCardFillActivity.this);


        }
        else {

            new DatabaseHelper(VisitCardFillActivity.this).addVisitCardData(data, arr,cardimg);
            Utils.playSimpleTone(VisitCardFillActivity.this);



        }


    }







    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
}
