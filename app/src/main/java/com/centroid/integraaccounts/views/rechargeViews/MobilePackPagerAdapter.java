package com.centroid.integraaccounts.views.rechargeViews;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.RechargeRecordAdapter;
import com.centroid.integraaccounts.data.rechargedomain.RechargeRecords;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.rechargeViews.domain.mobile.Category;
import com.centroid.integraaccounts.views.rechargeViews.domain.mobile.Plan;

import java.util.ArrayList;
import java.util.List;

public class MobilePackPagerAdapter extends PagerAdapter {
     Context mContext;
    List<Category> categories=new ArrayList<>();

    public MobilePackPagerAdapter(Context context,List<Category> categories) {
       this. mContext = context;
       this.categories=categories;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        Category modelObject = categories.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout =  inflater.inflate(R.layout.layout_rechargepacks, collection, false);

        EditText edtSearchAmount=layout.findViewById(R.id.edtSearchAmount);
        RecyclerView recyclerview=layout.findViewById(R.id.recyclerview);
        RechargeRecordAdapter rechargeRecordAdapter=new RechargeRecordAdapter(mContext,categories.get(position).getPlans());
        recyclerview.setLayoutManager(new GridLayoutManager(mContext,2));
        recyclerview.setAdapter(rechargeRecordAdapter);

        List<Plan>plans=categories.get(position).getPlans();

        edtSearchAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try{

                    String a=charSequence.toString();
                    List<Plan> searchrecorddata=new ArrayList<>();

                    if(!a.equalsIgnoreCase(""))
                    {
//                        double searchAmount=Double.parseDouble(a);
                        searchrecorddata.clear();

                        for(int j=0;j<plans.size();j++)
                        {
                            Plan rechargeRecords=plans.get(j);

                            if(rechargeRecords.getAmount().toString().contains(a))
                            {

                                searchrecorddata.add(rechargeRecords);
                            }

                        }

                        recyclerview.setLayoutManager(new GridLayoutManager(mContext,2));
                        recyclerview.setAdapter(new RechargeRecordAdapter(mContext, searchrecorddata));


                    }
                    else{

                        recyclerview.setLayoutManager(new GridLayoutManager(mContext,2));
                        recyclerview.setAdapter(new RechargeRecordAdapter(mContext, plans));

                    }



                }
                catch (Exception e)
                {

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }




    @Override
    public int getCount() {
        return categories.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).getName();
    }
}
