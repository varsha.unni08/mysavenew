
package com.centroid.integraaccounts.views.rechargeViews.domain.mobile;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Plan {

    @SerializedName("id")
    @Expose
    private Integer id=0;
    @SerializedName("amount")
    @Expose
    private Integer amount=0;
    @SerializedName("validity")
    @Expose
    private String validity="";
    @SerializedName("talktime")
    @Expose
    private Double talktime=0.0;
    @SerializedName("validityDays")
    @Expose
    private Double validityDays=0.0;
    @SerializedName("benefit")
    @Expose
    private String benefit="";
    @SerializedName("calls")
    @Expose
    private String calls="";
    @SerializedName("sms")
    @Expose
    private String sms="";
    @SerializedName("data")
    @Expose
    private String data="";
    @SerializedName("subscriptions")
    @Expose
    private List<Subscription> subscriptions=new ArrayList<>();
    @SerializedName("remark")
    @Expose
    private String remark="";
    @SerializedName("rechargeable")
    @Expose
    private Boolean rechargeable=false;
    @SerializedName("dailyCost")
    @Expose
    private Double dailyCost=0.0;
    @SerializedName("addedAt")
    @Expose
    private String addedAt="";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public Double getTalktime() {
        return talktime;
    }

    public void setTalktime(Double talktime) {
        this.talktime = talktime;
    }

    public Double getValidityDays() {
        return validityDays;
    }

    public void setValidityDays(Double validityDays) {
        this.validityDays = validityDays;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getCalls() {
        return calls;
    }

    public void setCalls(String calls) {
        this.calls = calls;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getRechargeable() {
        return rechargeable;
    }

    public void setRechargeable(Boolean rechargeable) {
        this.rechargeable = rechargeable;
    }

    public Double getDailyCost() {
        return dailyCost;
    }

    public void setDailyCost(Double dailyCost) {
        this.dailyCost = dailyCost;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(String addedAt) {
        this.addedAt = addedAt;
    }

}
