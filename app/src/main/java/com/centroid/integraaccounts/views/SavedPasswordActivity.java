package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.SavedPasswordAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.internal.$Gson$Preconditions;
import com.itextpdf.text.pdf.parser.Line;

import org.json.JSONObject;

import java.util.List;

public class SavedPasswordActivity extends AppCompatActivity {

    ImageView imgback;

    TextView txtHead;

    RecyclerView recycler;

    FloatingActionButton fab_addtask;

    CommonData commonData_selected=null;
    Resources resources=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_password);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        txtHead=findViewById(R.id.txtHead);
        recycler=findViewById(R.id.recycler);
        fab_addtask=findViewById(R.id.fab_addtask);

        String languagedata = LocaleHelper.getPersistedData(SavedPasswordActivity.this, "en");
        Context context= LocaleHelper.setLocale(SavedPasswordActivity.this, languagedata);

         resources=context.getResources();

         txtHead.setText(resources.getString(R.string.passwordmanager));

        getPasswords();

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   layout_addpassword


                showPasswordDialog();

            }
        });

    }

    public void setPasswordData(CommonData commonData_selected)
    {
        this.commonData_selected=commonData_selected;
        showPasswordDialog();
    }


    public void getPasswords()
    {
        List<CommonData>commonData=new DatabaseHelper(SavedPasswordActivity.this).getData(Utils.DBtables.TABLE_PASSWORD);

        recycler.setLayoutManager(new LinearLayoutManager(SavedPasswordActivity.this));
        recycler.setAdapter(new SavedPasswordAdapter(SavedPasswordActivity.this,commonData));

        Log.e("TAAG",commonData.size()+"");
    }


    public void showPasswordDialog()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels-10;
        int width = displayMetrics.widthPixels-10;


        final Dialog dialog=new Dialog(SavedPasswordActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_addpassword);

        TextView savepassword=dialog.findViewById(R.id.savepassword);

        EditText edtTitle=dialog.findViewById(R.id.edtTitle);
        EditText edtCategory=dialog.findViewById(R.id.edtCategory);
        EditText edtUsername=dialog.findViewById(R.id.edtUsername);
        EditText edtPassword=dialog.findViewById(R.id.edtPassword);
        EditText edtWebsite=dialog.findViewById(R.id.edtWebsite);
        EditText edtRemarks=dialog.findViewById(R.id.edtRemarks);
        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

        edtTitle.setHint(resources.getString(R.string.title));
        edtUsername.setHint(resources.getString(R.string.uname));
        edtPassword.setHint(resources.getString(R.string.password));
        edtWebsite.setHint(resources.getString(R.string.web));
        btnSubmit.setText(resources.getString(R.string.submit));
        savepassword.setText(resources.getString(R.string.passwordmanager));
        edtRemarks.setText(resources.getString(R.string.remarks));



        if(commonData_selected!=null)
        {
            try {

                JSONObject jsonObject = new JSONObject(commonData_selected.getData());


                edtTitle.setText(jsonObject.getString("title"));
                edtCategory.setText(jsonObject.getString("category"));

                edtUsername.setText(jsonObject.getString("username"));
                edtPassword.setText(Utils.decodeBase64Password(jsonObject.getString("password")));
                edtWebsite.setText(jsonObject.getString("website"));

                if(jsonObject.has("remarks"))
                {
                    edtRemarks.setText(jsonObject.getString("remarks"));
                }


            }catch ( Exception e)
            {

            }



        }







        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!edtTitle.getText().toString().equalsIgnoreCase(""))
                {
                    if(!edtUsername.getText().toString().equalsIgnoreCase(""))
                    {

                        if(!edtPassword.getText().toString().equalsIgnoreCase(""))
                        {

                            dialog.dismiss();

                            try{

                                JSONObject jsonObject=new JSONObject();
                                jsonObject.put("title",edtTitle.getText().toString());
                                jsonObject.put("category",edtCategory.getText().toString());
                                jsonObject.put("username",edtUsername.getText().toString());
                                jsonObject.put("password",Utils.getBase64Password(edtPassword.getText().toString()));
                                jsonObject.put("website",edtWebsite.getText().toString());
                                jsonObject.put("remarks",edtRemarks.getText().toString());

                                if(commonData_selected!=null)
                                {
                                    new DatabaseHelper(SavedPasswordActivity.this).updateData(commonData_selected.getId(),jsonObject.toString(),Utils.DBtables.TABLE_PASSWORD);

                                    Utils.playSimpleTone(SavedPasswordActivity.this);
                                    commonData_selected=null;
                                }
                                else {

                                    new DatabaseHelper(SavedPasswordActivity.this).addData(Utils.DBtables.TABLE_PASSWORD, jsonObject.toString());
                                    Utils.playSimpleTone(SavedPasswordActivity.this);


                                }

                                getPasswords();



                            }catch (Exception e)
                            {

                            }




                        }
                        else {


                            Utils.showAlertWithSingle(SavedPasswordActivity.this, "Enter password", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });
                        }

                    }
                    else {


                        Utils.showAlertWithSingle(SavedPasswordActivity.this, "Enter username", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }


                }
                else {


                    Utils.showAlertWithSingle(SavedPasswordActivity.this, "Enter title", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }
            }
        });






        dialog.getWindow().setLayout(width, height);


        dialog.show();
    }
}