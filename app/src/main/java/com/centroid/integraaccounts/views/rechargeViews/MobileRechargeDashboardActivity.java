package com.centroid.integraaccounts.views.rechargeViews;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.InitiatedRechargeAdapter;
import com.centroid.integraaccounts.adapter.RechargeHistoryDataAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.RechargeHistoryData;
import com.centroid.integraaccounts.data.domain.RechargeHistoryStatus;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.DTHRechargeActivity;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.RechargeHistoryActivity;
import com.centroid.integraaccounts.views.initiated.InitRechargeStatus;
import com.centroid.integraaccounts.views.initiated.InitiatedRecharge;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MobileRechargeDashboardActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    String fullString="",fullurl="";
    EditText edittextMob;
    TextView txtAirtel,txtbsnl,txtjio,txtvi,txthead;

    ImageView imgairtel,imgbsnl,imgvi,imgjio,imgback;

    CardView card_airtel,card_bsnl,card_vi,card_jio;
    public static final int PICK_CONTACT=11;


    String arroperators[] = {"Airtel", "Vi", "Jio", "BSNL"};
    String arroperator_code[] = {"AT", "ID", "RJ", "CG"};
    String arrspkey[] = {"3", "VIL", "116", "4"};

    String operator_url = "https://mysaveapp.com/rechargeAPI/newrecharge/mobileCircle.php?";
    String recharge_url = "https://mysaveapp.com/rechargeAPI/RechargePhone.php?";
    FloatingActionButton fab_pickcontact;
    String operatorcircle="";
    Spinner sp_circler;

    String operator1="";
    String spk1="";
    String operatorcode1="";

    Button btnrechargehistory_toggle,btn_initiated_toggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_recharge_dashboard);
        getSupportActionBar().hide();
        recyclerView=findViewById(R.id.recycler);
        edittextMob=findViewById(R.id.edittextMob);
        imgback=findViewById(R.id.imgback);
        fab_pickcontact=findViewById(R.id.fab_pickcontact);
        txtAirtel=findViewById(R.id.txtAirtel);
        txtbsnl=findViewById(R.id.txtbsnl);
        txtjio=findViewById(R.id.txtjio);
        txtvi=findViewById(R.id.txtvi);
        imgairtel=findViewById(R.id.imgairtel);
        imgbsnl=findViewById(R.id.imgbsnl);
        imgvi=findViewById(R.id.imgvi);
        imgjio=findViewById(R.id.imgjio);
        btnrechargehistory_toggle=findViewById(R.id.btnrechargehistory_toggle);
        btn_initiated_toggle=findViewById(R.id.btn_initiated_toggle);
        card_airtel=findViewById(R.id.card_airtel);
        card_bsnl=findViewById(R.id.card_bsnl);
        card_vi=findViewById(R.id.card_vi);
        card_jio=findViewById(R.id.card_jio);
        sp_circler=findViewById(R.id.sp_circler);
        txthead=findViewById(R.id.txthead);

        txtAirtel.setOnClickListener(MobileRechargeDashboardActivity.this);
        txtbsnl.setOnClickListener(MobileRechargeDashboardActivity.this);
        txtjio.setOnClickListener(MobileRechargeDashboardActivity.this);
        txtvi.setOnClickListener(MobileRechargeDashboardActivity.this);
        imgairtel.setOnClickListener(MobileRechargeDashboardActivity.this);
        imgbsnl.setOnClickListener(MobileRechargeDashboardActivity.this);
        imgvi.setOnClickListener(MobileRechargeDashboardActivity.this);
        imgjio.setOnClickListener(MobileRechargeDashboardActivity.this);

        card_airtel.setOnClickListener(MobileRechargeDashboardActivity.this);
        card_bsnl.setOnClickListener(MobileRechargeDashboardActivity.this);
        card_vi.setOnClickListener(MobileRechargeDashboardActivity.this);
        card_jio.setOnClickListener(MobileRechargeDashboardActivity.this);
        imgback.setOnClickListener(MobileRechargeDashboardActivity.this);
        fab_pickcontact.setOnClickListener(MobileRechargeDashboardActivity.this);





        getRechargeHistory();



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MobileRechargeDashboardActivity.this,
                android.R.layout.simple_spinner_item, RechargePlans.MobilePlans.operator_circle);
        sp_circler.setAdapter(adapter);


        sp_circler.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                operatorcircle=RechargePlans.MobilePlans.operator_circlecode[i];

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        edittextMob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equalsIgnoreCase(""))
                {
                    if(charSequence.toString().length()==10) {


                        findOperator();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getRechargeHistory();





    }


    public void retryRechargeInit(InitiatedRecharge rechargeHistoryData)
    {

    }




    public void updateStatus(String id,int status,int code,String paymentstatus,String rpid,String agentid)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
         params.put("payment_status",paymentstatus);
        params.put("rpid", rpid+"");
        params.put("agentid",agentid);

        new RequestHandler(MobileRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();
                getRechargeHistory();
                if(code==0)
                {

                    if(status==1&&paymentstatus.equalsIgnoreCase("5")) {

                        updateGenStatus(id);

                        updategenStatus(id, 1, 0);


                    }


                }
                else{

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeDashboardActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateRechargeTransactionStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }

    public void updategenStatus(String id,int status,int code)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
        // params.put("device_id",token);

        new RequestHandler(MobileRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(code==0)
                {




                }
                else{

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeDashboardActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateTransactionStatusRecharge+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }

    public void updateGenStatus(String transaction_id)
    {




        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());

        params.put("id", transaction_id);





        new RequestHandler(MobileRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                try {

                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        String msg = jsonObject.getString("message");


                    } else {


                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(String err) {


                Toast.makeText(MobileRechargeDashboardActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.updateGenStatus + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getRechargeHistory();
    }

    public void getRechargeHistory()
    {

//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        // params.put("device_id",token);
        recyclerView.setVisibility(View.GONE);

        new RequestHandler(MobileRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                try{

                    RechargeHistoryStatus rechargeHistoryStatus=new GsonBuilder().create().fromJson(data,RechargeHistoryStatus.class);
                    if(rechargeHistoryStatus.getStatus().equals(1))
                    {

                        recyclerView.setVisibility(View.VISIBLE);

                        List<RechargeHistoryData> rechargeHistoryData=rechargeHistoryStatus.getData();

                        if(rechargeHistoryData.size()>0)
                        {
                            txthead.setVisibility(View.VISIBLE);
//                            txtHead.setVisibility(View.VISIBLE);
                            List<RechargeHistoryData>redata=new ArrayList<>();

                            for(int j=0;j<rechargeHistoryData.size();j++){
                                if(rechargeHistoryData.get(j).getRechargeType().equalsIgnoreCase("1"))
                                {
                                    redata.add(rechargeHistoryData.get(j));

                                }


                            }


                            RechargeHistoryDataAdapter rechargeHistoryDataAdapter=new RechargeHistoryDataAdapter(MobileRechargeDashboardActivity.this,redata);
                            recyclerView.setLayoutManager(new GridLayoutManager(MobileRechargeDashboardActivity.this,2));
                            recyclerView.setAdapter(rechargeHistoryDataAdapter);


                        }
                        else{



                        }



                    }
                    else{

Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"No data found",null);
                    }







                }

                catch (Exception e)
                {
                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"No data found",null);
                }






            }

            @Override
            public void onFailure(String err) {


            }
        },Utils.WebServiceMethodes.getRechargeHistory+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }

    public void getInitiatedRechargeHistory()
    {

        recyclerView.setVisibility(View.GONE);

        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(MobileRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                try{
                    InitRechargeStatus initRechargeStatus=new GsonBuilder().create().fromJson(data,InitRechargeStatus.class);

                    if(initRechargeStatus.getStatus().equals(1))
                    {
                        recyclerView.setVisibility(View.VISIBLE);
                        InitiatedRechargeAdapter initiatedRechargeAdapter=new InitiatedRechargeAdapter(MobileRechargeDashboardActivity.this,initRechargeStatus.getData());
                        recyclerView.setLayoutManager(new GridLayoutManager(MobileRechargeDashboardActivity.this,2));
                        recyclerView.setAdapter(initiatedRechargeAdapter);

                    }
                    else{

                        Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"No data found",null);
                    }









                }

                catch (Exception e)
                {
                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"No data found",null);
                }






            }

            @Override
            public void onFailure(String err) {


            }
        },Utils.WebServiceMethodes.getInitiatedRecharges+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }


    public void retryRechargeFromHistory(RechargeHistoryData rechargeHistoryData)
    {
        if(rechargeHistoryData.getPaymentstatus().equalsIgnoreCase("5")) {
            Utils.showAlert(MobileRechargeDashboardActivity.this, "Do you want to recharge Again ?", new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {



                        if (rechargeHistoryData.getRefundTransactionid() != null && rechargeHistoryData.getStatus().equalsIgnoreCase("3") ) {

                            if (!rechargeHistoryData.getRefundTransactionid().isEmpty()) {


                                for (int j = 0; j < arroperators.length; j++) {

                                    String operator = arroperators[j];

                                    if (rechargeHistoryData.getOperator().contains(operator)) {


                                        spk1 = arrspkey[j];

                                        Intent intent = new Intent(MobileRechargeDashboardActivity.this, MobileRechargeActivity.class);
                                        intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                                        intent.putExtra("operator", operator);
                                        intent.putExtra("spkey", arrspkey[j]);
                                        intent.putExtra("recharge", rechargeHistoryData.getAmount());
                                        intent.putExtra("operator_code", arroperator_code[j]);
                                        intent.putExtra("operatorcircle", rechargeHistoryData.getOperatorCode());

                                        startActivity(intent);
                                        break;
                                    }


                                }

                            }

                        } else if (rechargeHistoryData.getStatus().equalsIgnoreCase("1") ) {
                            for (int j = 0; j < arroperators.length; j++) {

                                String operator = arroperators[j];

                                if (rechargeHistoryData.getOperator().contains(operator)) {


                                    spk1 = arrspkey[j];

                                    Intent intent = new Intent(MobileRechargeDashboardActivity.this, MobileRechargeActivity.class);
                                    intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                                    intent.putExtra("operator", operator);
                                    intent.putExtra("spkey", arrspkey[j]);
                                    intent.putExtra("recharge", rechargeHistoryData.getAmount());
                                    intent.putExtra("operator_code", arroperator_code[j]);
                                    intent.putExtra("operatorcircle", rechargeHistoryData.getOperatorCode());

                                    startActivity(intent);
                                    break;
                                }


                            }


                        } else if (rechargeHistoryData.getStatus().equalsIgnoreCase("0") ) {


                            for (int j = 0; j < arroperators.length; j++) {

                                String operator = arroperators[j];

                                if (rechargeHistoryData.getOperator().contains(operator)) {


                                    spk1 = arrspkey[j];

                                }
                            }


                            int min = 1;
                            int max = 10000;
                            int random = new Random().nextInt((max - min) + 1) + min;

//        String spkey = "";
                            String phone = rechargeHistoryData.getMobileNumber();

                            double amount = Double.parseDouble(rechargeHistoryData.getAmount());
//        spkey = arrspkey[selectedposition];

                            String apireqid = random + "";
                            String customerno = rechargeHistoryData.getMobileNumber();

                            String urldata = recharge_url + "phone=" + rechargeHistoryData.getMobileNumber() + "&amount=" + rechargeHistoryData.getAmount() + "&spkey=" + spk1 + "&apireqid=" + apireqid + "&customerno=" + rechargeHistoryData.getMobileNumber();


                            ExecutorService executorService = Executors.newSingleThreadExecutor();
                            Handler handler = new Handler(Looper.getMainLooper());
                            executorService.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        final ProgressFragment progressFragment = new ProgressFragment();
                                        progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                        Map<String, String> params = new HashMap<>();
                                        params.put("timestamp", Utils.getTimestamp());
                                        String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                        fullString = "";
                                        URL url = new URL(fullurl);
                                        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                        String line;
                                        while ((line = reader.readLine()) != null) {
                                            fullString += line;
                                        }
                                        reader.close();

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {

                                                progressFragment.dismiss();

                                                if (!fullString.isEmpty()) {

                                                    try {

                                                        JSONObject jsonObject = new JSONObject(fullString);

                                                        if (jsonObject.getInt("status") == 2) {

                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            updateStatus(rechargeHistoryData.getId(), 1, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                            Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge completed successfully", new DialogEventListener() {
                                                                @Override
                                                                public void onPositiveButtonClicked() {

                                                                }

                                                                @Override
                                                                public void onNegativeButtonClicked() {

                                                                }
                                                            });
                                                        } else if (jsonObject.getInt("status") == 1) {

                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            updateStatus(rechargeHistoryData.getId(), 2, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                            Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge is pending", new DialogEventListener() {
                                                                @Override
                                                                public void onPositiveButtonClicked() {

                                                                }

                                                                @Override
                                                                public void onNegativeButtonClicked() {

                                                                }
                                                            });

                                                        } else {
                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);

                                                            Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later...", new DialogEventListener() {
                                                                @Override
                                                                public void onPositiveButtonClicked() {

                                                                }

                                                                @Override
                                                                public void onNegativeButtonClicked() {

                                                                }
                                                            });

                                                        }
                                                    } catch (Exception e) {


                                                    }


                                                    //update Transaction


                                                } else {

                                                    //update Transaction
//                                               postTransactionData(mobileRechargeResponse, 0);
                                                    updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),"","");


                                                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later..No Response from Server", new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });


                                                }


                                            }


                                        });


                                    } catch (Exception e) {


                                    }
                                }
                            });


                        }


                        else if(rechargeHistoryData.getStatus().equalsIgnoreCase("2"))
                        {
                            if(!rechargeHistoryData.getRpId().isEmpty()&&!rechargeHistoryData.getAgentId().isEmpty()) {

                                try {

                                    String date_recharge = rechargeHistoryData.getRechargeDate();
                                    DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    Date rechargedt=dateFormat.parse(date_recharge);
                                    DateFormat dateFormat1=new SimpleDateFormat("dd MMM yyyy");
                                    String serversubmitdate=dateFormat1.format(rechargedt);

                                    // 2024-01-01 12:50:56

                                    String recharge_retry_url = "https://mysaveapp.com/rechargeAPI/retryRechargeFromHistory.php?timestamp=" + Utils.getTimestamp() + "&agentid=" + rechargeHistoryData.getAgentId()+"&date="+serversubmitdate;
                                    ExecutorService executorService = Executors.newSingleThreadExecutor();
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    executorService.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                final ProgressFragment progressFragment = new ProgressFragment();
                                                progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                                Map<String, String> params = new HashMap<>();
                                                params.put("timestamp", Utils.getTimestamp());
                                                String fullurl = recharge_retry_url;

                                                fullString = "";
                                                URL url = new URL(fullurl);
                                                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                                String line;
                                                while ((line = reader.readLine()) != null) {
                                                    fullString += line;
                                                }
                                                reader.close();

                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {

                                                        progressFragment.dismiss();

                                                        try {

                                                            if (!fullString.isEmpty()) {

                                                                JSONObject jsonObject = new JSONObject(fullString);

                                                                if (jsonObject.getInt("status") == 2) {
                                                                    updateStatus(rechargeHistoryData.getId(), 1, 0, rechargeHistoryData.getPaymentstatus(), rechargeHistoryData.getRpId(), rechargeHistoryData.getAgentId());

                                                                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Recharge Successful", new DialogEventListener() {
                                                                        @Override
                                                                        public void onPositiveButtonClicked() {

                                                                        }

                                                                        @Override
                                                                        public void onNegativeButtonClicked() {

                                                                        }
                                                                    });


                                                                } else if (jsonObject.getInt("status") == 1) {

                                                                    updateStatus(rechargeHistoryData.getId(), 2, 0, rechargeHistoryData.getPaymentstatus(), rechargeHistoryData.getRpId(), rechargeHistoryData.getAgentId());

                                                                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Recharge is still pending....", new DialogEventListener() {
                                                                        @Override
                                                                        public void onPositiveButtonClicked() {

                                                                        }

                                                                        @Override
                                                                        public void onNegativeButtonClicked() {

                                                                        }
                                                                    });


                                                                } else {

                                                                    updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(), rechargeHistoryData.getRpId(), rechargeHistoryData.getAgentId());
                                                                }


                                                            }

                                                        } catch (Exception e) {

                                                        }


                                                    }
                                                });


                                            } catch (Exception e) {


                                            }
                                        }
                                    });
                                }catch (Exception e)
                                {

                                }
                            }
                            else{

                                for (int j = 0; j < arroperators.length; j++) {

                                    String operator = arroperators[j];

                                    if (rechargeHistoryData.getOperator().contains(operator)) {


                                        spk1 = arrspkey[j];

                                    }
                                }


                                int min = 1;
                                int max = 10000;
                                int random = new Random().nextInt((max - min) + 1) + min;
                                String phone = rechargeHistoryData.getMobileNumber();

                                double amount = Double.parseDouble(rechargeHistoryData.getAmount());

                                String apireqid = random + "";
                                String customerno = rechargeHistoryData.getMobileNumber();

                                String urldata = recharge_url + "phone=" + rechargeHistoryData.getMobileNumber() + "&amount=" + rechargeHistoryData.getAmount() + "&spkey=" + spk1 + "&apireqid=" + apireqid + "&customerno=" + rechargeHistoryData.getMobileNumber();


                                ExecutorService executorService = Executors.newSingleThreadExecutor();
                                Handler handler = new Handler(Looper.getMainLooper());
                                executorService.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            final ProgressFragment progressFragment = new ProgressFragment();
                                            progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                            Map<String, String> params = new HashMap<>();
                                            params.put("timestamp", Utils.getTimestamp());
                                            String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                            fullString = "";
                                            URL url = new URL(fullurl);
                                            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                            String line;
                                            while ((line = reader.readLine()) != null) {
                                                fullString += line;
                                            }
                                            reader.close();

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {

                                                    progressFragment.dismiss();

                                                    if (!fullString.isEmpty()) {

                                                        try {

                                                            JSONObject jsonObject = new JSONObject(fullString);
                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            if (jsonObject.getInt("status") == 2) {

                                                                updateStatus(rechargeHistoryData.getId(), 1, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge completed successfully", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });
                                                            } else if (jsonObject.getInt("status") == 1) {
                                                                updateStatus(rechargeHistoryData.getId(), 2, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge is pending", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });

                                                            } else {
                                                                updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);

                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later...", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });

                                                            }
                                                        } catch (Exception e) {


                                                        }


                                                        //update Transaction


                                                    } else {

                                                        //update Transaction
//                                               postTransactionData(mobileRechargeResponse, 0);
                                                        updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),"","");


                                                        Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later..No Response From Server", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });


                                                    }


                                                }


                                            });


                                        } catch (Exception e) {


                                        }
                                    }
                                });
                            }
                        }
                        else{
                            if(!rechargeHistoryData.getRpId().isEmpty()&&!rechargeHistoryData.getAgentId().isEmpty()) {

                                String recharge_retry_url = "https://mysaveapp.com/rechargeAPI/retryRecharge.php?timestamp=" + Utils.getTimestamp() + "&rpid=" + rechargeHistoryData.getRpId() + "&agentid=" + rechargeHistoryData.getAgentId();
                                ExecutorService executorService = Executors.newSingleThreadExecutor();
                                Handler handler = new Handler(Looper.getMainLooper());
                                executorService.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            final ProgressFragment progressFragment = new ProgressFragment();
                                            progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                            Map<String, String> params = new HashMap<>();
                                            params.put("timestamp", Utils.getTimestamp());
                                            String fullurl = recharge_retry_url;

                                            fullString = "";
                                            URL url = new URL(fullurl);
                                            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                            String line;
                                            while ((line = reader.readLine()) != null) {
                                                fullString += line;
                                            }
                                            reader.close();

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {

                                                    progressFragment.dismiss();

                                                    try {

                                                        if (!fullString.isEmpty()) {

                                                            JSONObject jsonObject = new JSONObject(fullString);
                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            if (jsonObject.getInt("status") == 2) {
                                                                updateStatus(rechargeHistoryData.getId(), 1, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);

                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Recharge Successful", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });


                                                            } else if (jsonObject.getInt("status") == 1) {

                                                                updateStatus(rechargeHistoryData.getId(), 2, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);

                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Recharge is still pending....", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });


                                                            } else {

                                                                updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                            }


                                                        }

                                                    } catch (Exception e) {

                                                    }


                                                }
                                            });


                                        } catch (Exception e) {


                                        }
                                    }
                                });
                            }
                            else{

                                for (int j = 0; j < arroperators.length; j++) {

                                    String operator = arroperators[j];

                                    if (rechargeHistoryData.getOperator().contains(operator)) {


                                        spk1 = arrspkey[j];

                                    }
                                }


                                int min = 1;
                                int max = 10000;
                                int random = new Random().nextInt((max - min) + 1) + min;

//        String spkey = "";
                                String phone = rechargeHistoryData.getMobileNumber();

                                double amount = Double.parseDouble(rechargeHistoryData.getAmount());
//        spkey = arrspkey[selectedposition];

                                String apireqid = random + "";
                                String customerno = rechargeHistoryData.getMobileNumber();

                                String urldata = recharge_url + "phone=" + rechargeHistoryData.getMobileNumber() + "&amount=" + rechargeHistoryData.getAmount() + "&spkey=" + spk1 + "&apireqid=" + apireqid + "&customerno=" + rechargeHistoryData.getMobileNumber();


                                ExecutorService executorService = Executors.newSingleThreadExecutor();
                                Handler handler = new Handler(Looper.getMainLooper());
                                executorService.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            final ProgressFragment progressFragment = new ProgressFragment();
                                            progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                            Map<String, String> params = new HashMap<>();
                                            params.put("timestamp", Utils.getTimestamp());
                                            String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                            fullString = "";
                                            URL url = new URL(fullurl);
                                            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                            String line;
                                            while ((line = reader.readLine()) != null) {
                                                fullString += line;
                                            }
                                            reader.close();

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {

                                                    progressFragment.dismiss();

                                                    if (!fullString.isEmpty()) {

                                                        try {

                                                            JSONObject jsonObject = new JSONObject(fullString);
                                                            String rpid=jsonObject.getString("rpid");
                                                            String agentid=jsonObject.getString("agentid");

                                                            if (jsonObject.getInt("status") == 2) {

                                                                updateStatus(rechargeHistoryData.getId(), 1, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge completed successfully", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });
                                                            } else if (jsonObject.getInt("status") == 1) {
                                                                updateStatus(rechargeHistoryData.getId(), 2, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);
                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge is pending", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });

                                                            } else {
                                                                updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),rpid,agentid);

                                                                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later...", new DialogEventListener() {
                                                                    @Override
                                                                    public void onPositiveButtonClicked() {

                                                                    }

                                                                    @Override
                                                                    public void onNegativeButtonClicked() {

                                                                    }
                                                                });

                                                            }
                                                        } catch (Exception e) {


                                                        }


                                                        //update Transaction


                                                    } else {

                                                        //update Transaction
//                                               postTransactionData(mobileRechargeResponse, 0);
                                                        updateStatus(rechargeHistoryData.getId(), 0, 0, rechargeHistoryData.getPaymentstatus(),"","");


                                                        Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this, "Your recharge failed. Please try again later.No Response From Server", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });


                                                    }


                                                }


                                            });


                                        } catch (Exception e) {


                                        }
                                    }
                                });
                            }

                        }

//


                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });
        }
        else{

            for (int j = 0; j < arroperators.length; j++) {

                String operator = arroperators[j];

                if (rechargeHistoryData.getOperator().contains(operator)) {


                    spk1 = arrspkey[j];

                    Intent intent = new Intent(MobileRechargeDashboardActivity.this, MobileRechargeActivity.class);
                    intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                    intent.putExtra("operator", operator);
                    intent.putExtra("spkey", arrspkey[j]);
                    intent.putExtra("recharge", rechargeHistoryData.getAmount());
                    intent.putExtra("operator_code", arroperator_code[j]);
                    intent.putExtra("operatorcircle", rechargeHistoryData.getOperatorCode());
                    intent.putExtra("transaction_id",rechargeHistoryData.getId());

                    startActivity(intent);
                    break;
                }


            }
        }
    }

    public void retryRechargeFromInitiatedHistory(InitiatedRecharge rechargeHistoryData)
    {


    }










    private void redirectToPlans(String operator,int index)
    {
        if(!edittextMob.getText().toString().equalsIgnoreCase(""))
        {
            if(edittextMob.getText().toString().length()==10)
            {
                if(!operatorcircle.isEmpty()) {


                    Intent intent = new Intent(MobileRechargeDashboardActivity.this, MobileRechargeActivity.class);
                    intent.putExtra("mobile", edittextMob.getText().toString());
                    intent.putExtra("operator", operator);
                    intent.putExtra("spkey", arrspkey[index]);
                    intent.putExtra("operator_code", arroperator_code[index]);
                    intent.putExtra("operatorcircle", operatorcircle);

                    intent.putExtra("recharge", "");
                    startActivity(intent);
                }
                else{
                    Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"Operator Circle is not found",null);
                }

            }
            else{


                Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"Invalid Mobile Number",null);
            }


        }
        else{


            Utils.showAlertWithSingle(MobileRechargeDashboardActivity.this,"Enter Mobile Number",null);
        }

    }


    @Override
    public void onClick(View view) {



        switch (view.getId())
        {

            case R.id.fab_pickcontact:

                if(ContextCompat.checkSelfPermission(MobileRechargeDashboardActivity.this, Manifest.permission.READ_CONTACTS)== PackageManager.PERMISSION_GRANTED)
                {

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    someActivityResultLauncher.launch(intent);
                }
                else{

                    ActivityCompat.requestPermissions(MobileRechargeDashboardActivity.this,new String[]{Manifest.permission.READ_CONTACTS},110);

                }




                break;

            case R.id.imgback:
                getOnBackPressedDispatcher().onBackPressed();

                break;

            case R.id.card_airtel:
                redirectToPlans(arroperators[0],0);

                break;
            case R.id.card_bsnl:
                redirectToPlans(arroperators[3],3);
                break;
            case R.id.card_vi:
                redirectToPlans(arroperators[1],1);
                break;
            case R.id.card_jio:
                redirectToPlans(arroperators[2],2);
                break;



            case R.id.txtAirtel:
                redirectToPlans(arroperators[0],0);
                break;
            case R.id.txtbsnl:
                redirectToPlans(arroperators[3],3);
                break;
            case R.id.txtjio:
                redirectToPlans(arroperators[2],2);
                break;
            case R.id.txtvi:
                redirectToPlans(arroperators[1],1);
                break;




            case R.id.imgairtel:
                redirectToPlans(arroperators[0],0);
                break;
            case R.id.imgbsnl:
                redirectToPlans(arroperators[3],3);
                break;
            case R.id.imgvi:
                redirectToPlans(arroperators[1],1);
                break;
            case R.id.imgjio:
                redirectToPlans(arroperators[2],2);
                break;







        }

    }

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {

                        try {
                            // There are no request codes
                            Intent data = result.getData();
                            if (data != null) {

                                Uri contactUri = data.getData();
                                Cursor c = getContentResolver().query(contactUri, null, null, null, null);
                                if (c.moveToFirst()) {
                                    String name = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                    String selectedPhone = "";
                                    String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));

                                    if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                                        while (phones.moveToNext()) {
                                            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                            Log.e("Number", phoneNumber);

                                            String p="";

                                            String a=phoneNumber.replace(" ","");
                                            if(a.length()>10) {

                                                p = a.substring(a.length() - 10, a.length());

                                                edittextMob.setText(p);
                                            }
                                            else{
                                                p=a;
                                                edittextMob.setText(p);
                                            }



                                        }
                                        phones.close();
                                    }

//                                    String message = String.format("Name %s, selected phone %s", name, selectedPhone);
//                                    Toast.makeText(MobileRechargeDashboardActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                                c.close();
                            }

                        }catch (Exception e)
                        {

                        }

                    } else {

                    }

                }
            });

    public void findOperator()
    {
        ExecutorService executorService= Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {


                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
//                    String fullurl=urldata+"&timestamp="+Utils.getTimestamp();
//                    fullurl=operator_url+"timestamp="+ Utils.getTimestamp()+"&mobile="+edittextMob.getText().toString().trim();

                    fullurl=operator_url+"timestamp="+ Utils.getTimestamp()+"&mobile="+edittextMob.getText().toString().trim();

                    fullString = "";
                    URL url = new URL(fullurl);
                    URLConnection connection  = url.openConnection();
                    connection.setConnectTimeout(1000);
                    connection.setReadTimeout(2000);
                    connection.connect();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            progressFragment.dismiss();


                            try{

                                Log.e("DTHOperator Response",fullString);
                                JSONObject js=new JSONObject(fullString);




//                                {"tel":"70474619916","records":{"Operator":"SunDirect","status":1}}



//                                JSONObject js=jsonObject.getJSONObject("records");



                                if(js.has("status"))
                                {

                                    String status=js.getString("status");

                                    if(status.compareTo("OK")==0)
                                    {

                                        JSONObject js1=js.getJSONObject("data");

                                       String  op=js1.getString("circle");

                                       if(!op.isEmpty()) {

                                           for (int j = 0; j < RechargePlans.MobilePlans.operator_circlecode.length; j++) {
                                               if (op.compareTo(RechargePlans.MobilePlans.operator_circlecode[j]) == 0) {


                                                   operatorcircle = RechargePlans.MobilePlans.operator_circlecode[j];

                                                   sp_circler.setSelection(j);

                                                   break;

                                               }
                                           }

//                                           Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Operator Circle found Successfully.Please Select the Operator", Snackbar.LENGTH_LONG);
//
//                                           View view = snackbar.getView();
//                                           FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
//                                           params.gravity = Gravity.CENTER;
//                                           view.setLayoutParams(params);
//                                           snackbar.show();



                                       }
                                       else{

//                                           Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Operator Circle not found .Please Select the Operator circle", Snackbar.LENGTH_LONG);
//                                           View view = snackbar.getView();
//                                           FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
//                                           params.gravity = Gravity.CENTER;
//                                           view.setLayoutParams(params);
//                                           snackbar.show();
                                       }


                                    }
                                    else{

//                                            Utils.showAlertWithSingle(DTHPlansActivity.this,"No plans available",null);

                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Customer not found", Snackbar.LENGTH_LONG);


                                        snackbar.show();
                                    }





                                }
                                else{

//                                        Utils.showAlertWithSingle(DTHPlansActivity.this,"No plans available",null);

                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "No plans available", Snackbar.LENGTH_LONG);


                                    snackbar.show();
                                }











                            }catch (Exception e)
                            {
                                // Utils.showAlertWithSingle(DTHPlansActivity.this,"Unable to fetch plans",null);
                                progressFragment.dismiss();

                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Unable to fetch plans", Snackbar.LENGTH_LONG);


                                snackbar.show();
                            }

//                            Utils.showAlertWithSingle(DTHPlansActivity.this,fullString,null);





                        }
                    });



                } catch (Exception e) {

                    progressFragment.dismiss();

                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Unable to fetch plans", Snackbar.LENGTH_LONG);


                    snackbar.show();
                }
            }
        });

    }
}