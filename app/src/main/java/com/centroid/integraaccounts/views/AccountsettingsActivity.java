package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class AccountsettingsActivity extends AppCompatActivity {

    ImageView imgback;

    EditText edtAmount, edtAccountname;

    Button btnSave;
    Spinner spinnerAccountName, spinnerAccounttype, spinnerType,spinnerYear;

    CommonData commonData;

    TextView txtHead;

    int customer=0,incomeaccount=0;

    List<String>yeardata;
    LinearLayout layout_year;


    int code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountsettings);
        getSupportActionBar().hide();

        commonData = (CommonData) getIntent().getSerializableExtra("Commondata");

        customer=getIntent().getIntExtra("customer",0);
        incomeaccount=getIntent().getIntExtra("incomeaccount",0);

        code=getIntent().getIntExtra(Utils.Requestcode.AccVoucherAll,0);

        txtHead = findViewById(R.id.txtHead);

        imgback = findViewById(R.id.imgback);
        spinnerType = findViewById(R.id.spinnerType);

        spinnerAccounttype = findViewById(R.id.spinnerAccounttype);
        spinnerAccountName = findViewById(R.id.spinnerAccountName);
        spinnerYear=findViewById(R.id.spinnerYear);
        layout_year=findViewById(R.id.layout_year);

        edtAccountname = findViewById(R.id.edtAccountname);

        btnSave = findViewById(R.id.btnSave);
        edtAmount = findViewById(R.id.edtAmount);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.accountcategory, R.layout.spinner_item);

        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerAccounttype.setAdapter(adapter);




        ArrayAdapter adapteraccname = ArrayAdapter.createFromResource(this,
                R.array.type, R.layout.spinner_item);

        adapteraccname.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerType.setAdapter(adapteraccname);


        yeardata=new ArrayList<>();

        Calendar c=Calendar.getInstance();
        int y=c.get(Calendar.YEAR)-1;
        yeardata.add(String.valueOf(y));

        for(int i=0;i<5;i++)
        {
            y++;

            yeardata.add(String.valueOf(y));

        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, yeardata.toArray(new String[yeardata.size()]));

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYear.setAdapter(spinnerArrayAdapter);



        if(customer==1)
        {
            spinnerAccounttype.setSelection(5);
        }

        if(incomeaccount==1)
        {
            spinnerAccounttype.setSelection(7);
        }


        if (code==Utils.Requestcode.ForAccountSettingsBankRequestcode)
        {
            spinnerAccounttype.setSelection(2);
        }
        else if(code==Utils.Requestcode.ForAccountSettingsCashRequestcode)
        {
            spinnerAccounttype.setSelection(3);
        }
        else if(code==Utils.Requestcode.ForAccountSettingsInvestmentRequestcode)
        {

            spinnerAccounttype.setSelection(9);
        }
        else if (code==Utils.Requestcode.ForAccountSettingsAssetRequestcode)
        {

            spinnerAccounttype.setSelection(1);
        }
        else if(code==Utils.Requestcode.ForAccountSettingsInsuranceRequestcode)
        {

            spinnerAccounttype.setSelection(8);
        }
        else if(code==Utils.Requestcode.ForAccountSettingsLiabilityRequestcode)
        {
            spinnerAccounttype.setSelection(10);
        }

        else if(code==Utils.Requestcode.ForAccountSettingsinvestmentRequestcode)
        {
            spinnerAccounttype.setSelection(9);
        }



        spinnerAccounttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String data=spinnerAccounttype.getSelectedItem().toString();

                layout_year.setVisibility(View.GONE);

                if(data.equalsIgnoreCase("Expense account"))
                {
                    layout_year.setVisibility(View.VISIBLE);

                    spinnerType.setSelection(1);
                }
                else if(data.equalsIgnoreCase("Income account"))
                {
                    layout_year.setVisibility(View.VISIBLE);
                    spinnerType.setSelection(2);
                }

                else if(data.equalsIgnoreCase("Asset account"))
                {

                    spinnerType.setSelection(1);
                }

                else if(data.equalsIgnoreCase("Bank"))
                {

                    spinnerType.setSelection(1);
                }
                else if(data.equalsIgnoreCase("Cash"))
                {

                    spinnerType.setSelection(1);
                }
                else   if(data.equalsIgnoreCase("Customers"))
                {

                    spinnerType.setSelection(1);
                }

                else   if(data.equalsIgnoreCase("Insurance"))
                {

                    spinnerType.setSelection(1);
                }


                else   if(data.equalsIgnoreCase("credit card"))
                {

                    spinnerType.setSelection(2);
                }

                else  if(data.equalsIgnoreCase("Liability account"))
                {

                    spinnerType.setSelection(2);
                }

                else  if(data.equalsIgnoreCase("Investment"))
                {

                    spinnerType.setSelection(1);
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        if (commonData != null) {

            try {


                btnSave.setText("Update");

                txtHead.setText("Edit account setup");

                JSONObject jsonObject = new JSONObject(commonData.getData());

                edtAmount.setText(jsonObject.getString("Amount"));

                edtAccountname.setText(jsonObject.getString("Accountname"));




                String accountcategory[] = this.getResources().getStringArray(R.array.accountcategory);

                for (int i = 0; i < accountcategory.length; i++) {
                    if (accountcategory[i].equalsIgnoreCase(jsonObject.getString("Accounttype"))) {

                        spinnerAccounttype.setSelection(i);
                        break;
                    }


                }


                String type[] = this.getResources().getStringArray(R.array.type);

                for (int i = 0; i < type.length; i++) {
                    if (type[i].equalsIgnoreCase(jsonObject.getString("Type"))) {

                        spinnerType.setSelection(i);
                        break;
                    }


                }


            } catch (Exception e) {

            }

        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (!edtAccountname.getText().toString().equalsIgnoreCase("")) {



//                    if(!checkAccountSettings(edtAccountname.getText().toString()))
//                    {


                        if (!spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Select account category")) {




                        try {


                            if (commonData == null) {



                                if(!checkAccountSettings(edtAccountname.getText().toString())) {


                                    String amount = "0";

                                    if (edtAmount.getText().toString().equalsIgnoreCase("")) {

                                        amount = "0";
                                    } else {
                                        amount = edtAmount.getText().toString();
                                    }

                                    double damount = Double.parseDouble(amount);

                                    if (damount > 0) {


                                        if (!spinnerType.getSelectedItem().toString().equalsIgnoreCase("Select Type")) {

                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("Accountname", edtAccountname.getText().toString());
                                            jsonObject.put("Amount", amount);
                                            jsonObject.put("Accounttype", spinnerAccounttype.getSelectedItem().toString());
                                            jsonObject.put("Type", spinnerType.getSelectedItem().toString());
                                            if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Expense account"))
                                            {
                                                jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                            }
                                            else if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Income account"))
                                            {
                                                jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                            }

                                            new DatabaseHelper(AccountsettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS, jsonObject.toString());
                                            edtAmount.setText("");
                                            spinnerAccounttype.setSelection(0);
                                            spinnerAccountName.setSelection(0);
                                            spinnerType.setSelection(0);
                                            edtAccountname.setText("");

                                        } else {

                                       //     Toast.makeText(AccountsettingsActivity.this, "Select Type", Toast.LENGTH_SHORT).show();
                                            Utils.showAlertWithSingle(AccountsettingsActivity.this, "Select Type", new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });

                                        }


                                    } else {

                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("Accountname", edtAccountname.getText().toString());
                                        jsonObject.put("Amount", amount);
                                        jsonObject.put("Accounttype", spinnerAccounttype.getSelectedItem().toString());
                                        jsonObject.put("Type", spinnerType.getSelectedItem().toString());
                                        if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Expense account"))
                                        {
                                            jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                        }
                                        else if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Income account"))
                                        {
                                            jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                        }
                                        new DatabaseHelper(AccountsettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS, jsonObject.toString());
                                        edtAmount.setText("");
                                        spinnerAccounttype.setSelection(0);
                                        spinnerAccountName.setSelection(0);
                                        spinnerType.setSelection(0);
                                       // edtAccountname.setText("");
                                    }


                                    if(code== Utils.Requestcode.ForAccountSettingswithoutRequestcode)
                                    {

                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }
                                    else
                                    if(code== Utils.Requestcode.ForAccountSettingsBankRequestcode)
                                    {

                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }

                                    else if(code== Utils.Requestcode.ForAccountSettingsCashRequestcode)
                                    {

                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }
                                    else if(customer==1)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }
                                    else if(incomeaccount==1)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }

                                    else if(code==Utils.Requestcode.ForAccountSettingsInvestmentRequestcode)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }

                                    else if(code==Utils.Requestcode.ForAccountSettingsAssetRequestcode)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }

                                    else if(code==Utils.Requestcode.ForAccountSettingsInsuranceRequestcode)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();
                                    }
                                    else if(code==Utils.Requestcode.ForAccountSettingsLiabilityRequestcode)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();

                                    }

                                   else if(code==Utils.Requestcode.ForAccountSettingsinvestmentRequestcode)
                                    {
                                        String data=edtAccountname.getText().toString();

                                        Intent intent=new Intent();
                                        intent.putExtra("AccountAdded",data);
                                        setResult(RESULT_OK,intent);

                                        finish();

                                    }



                                }
                                else {

                                 //   Toast.makeText(AccountsettingsActivity.this, "Account name is already added", Toast.LENGTH_SHORT).show();
                                    Utils.showAlertWithSingle(AccountsettingsActivity.this, "Account name is already added", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                }








                            } else {




                                String amount = "0";

                                if (edtAmount.getText().toString().equalsIgnoreCase("")) {

                                    amount = "0";
                                } else {
                                    amount = edtAmount.getText().toString();
                                }

                                double damount = Double.parseDouble(amount);

                                if (damount > 0) {


                                    if (!spinnerType.getSelectedItem().toString().equalsIgnoreCase("Select Type")) {


                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("Accountname", edtAccountname.getText().toString());
                                        jsonObject.put("Amount", amount);
                                        jsonObject.put("Accounttype", spinnerAccounttype.getSelectedItem().toString());
                                        jsonObject.put("Type", spinnerType.getSelectedItem().toString());
                                        if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Expense account"))
                                        {
                                            jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                        }
                                        else if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Income account"))
                                        {
                                            jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                        }

                                        new DatabaseHelper(AccountsettingsActivity.this).updateData(commonData.getId(), jsonObject.toString(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                                        edtAmount.setText("");
                                        edtAccountname.setText("");
                                        spinnerAccounttype.setSelection(0);
                                        spinnerAccountName.setSelection(0);
                                        spinnerType.setSelection(0);


                                    } else {


                                     //   Toast.makeText(AccountsettingsActivity.this, "Select Type", Toast.LENGTH_SHORT).show();

                                        Utils.showAlertWithSingle(AccountsettingsActivity.this, "Select Type", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    }


                                } else {

                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("Accountname", edtAccountname.getText().toString());
                                    jsonObject.put("Amount", amount);
                                    jsonObject.put("Accounttype", spinnerAccounttype.getSelectedItem().toString());
                                    jsonObject.put("Type", spinnerType.getSelectedItem().toString());
                                    if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Expense account"))
                                    {
                                        jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                    }
                                    else if(spinnerAccounttype.getSelectedItem().toString().equalsIgnoreCase("Income account"))
                                    {
                                        jsonObject.put("year", spinnerYear.getSelectedItem().toString());
                                    }

                                    new DatabaseHelper(AccountsettingsActivity.this).updateData(commonData.getId(), jsonObject.toString(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                                    edtAmount.setText("");
                                    edtAccountname.setText("");
                                    spinnerAccounttype.setSelection(0);
                                    spinnerAccountName.setSelection(0);
                                    spinnerType.setSelection(0);

                                }

                                onBackPressed();

                            }


                        } catch (Exception e) {


                        }





                    } else {

                      //  Toast.makeText(AccountsettingsActivity.this, "Select account category", Toast.LENGTH_SHORT).show();
                            Utils.showAlertWithSingle(AccountsettingsActivity.this, "Select account category", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                    }


//                    } else {
//
//                        Toast.makeText(AccountsettingsActivity.this, "Account name is already added", Toast.LENGTH_SHORT).show();
//                    }


                } else {

                   // Toast.makeText(AccountsettingsActivity.this, "Select account name", Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(AccountsettingsActivity.this, "Select account name", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }


            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }




    public boolean checkAccountSettings(String name)
    {

        boolean isAlreadyAdded=false;

      List<CommonData> commonData=new DatabaseHelper(AccountsettingsActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        Collections.reverse(commonData);

        if(commonData.size()>0)
        {


            for (CommonData cm:commonData)
            {

                try {


                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String accountname=jsonObject.getString("Accountname");

                    if (accountname.equalsIgnoreCase(name))

                    {

                        isAlreadyAdded=true;
                        break;
                    }





                }catch (Exception e)
                {

                }




            }



        }




        return isAlreadyAdded;

    }





}
