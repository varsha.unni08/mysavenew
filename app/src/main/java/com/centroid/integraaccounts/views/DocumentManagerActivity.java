package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.FileUtils;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.PathUtil;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.DocumentAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.gson.GsonBuilder;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class DocumentManagerActivity extends AppCompatActivity {

    ImageView imgback;

    TextView txtHead;

    RecyclerView recycler;

    FloatingActionButton fab_addtask;

    String filePath="";
    TextView txtfilepath;
    EditText edtDocname;

    String mimetype="";

    public static final int PICKFILE_RESULT_CODE=11;

    public static final int PICKIOmage_RESULT_CODE=112;

    GoogleSignInAccount signInAccount = null ;
    public static final int REQUEST_CODE_SIGN_IN=110;

    public static final int REQUEST_CODE_SIGN_INFORDELETION=111;

    String fileid="";
    byte[] b=null;
    File fileuploadToDrive=null;

    String folderid="";
     ProgressFragment progressFragment=null;
     Dialog dialog;
    GoogleSignInClient googleSignInClient=null;

    boolean istodownload=false,isToUpdate=false;
    String filenametoDownload="";

    String filenameToupdate="";

    Resources resources;

    String dbid="0";

    CommonData cm_updating=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_manager);
        getSupportActionBar().hide();

        String languagedata = LocaleHelper.getPersistedData(DocumentManagerActivity.this, "en");
        Context context = LocaleHelper.setLocale(DocumentManagerActivity.this, languagedata);

        resources = context.getResources();

        imgback=findViewById(R.id.imgback);
        txtHead=findViewById(R.id.txtHead);
        recycler=findViewById(R.id.recycler);
        fab_addtask=findViewById(R.id.fab_addtask);

        txtHead.setText(resources.getString(R.string.documentmanager));

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   layout_addpassword


                showDocumentDialog();

            }
        });
        showDocuments();
    }

    public void updateDocuments(String dbid,String id,String filename,CommonData commonData)
    {
        fileid=id;
        isToUpdate=true;
        filenameToupdate=filename;
        cm_updating=commonData;

        this.dbid=dbid;

        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        googleSignInClient = GoogleSignIn.getClient(DocumentManagerActivity.this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);

//        Toast.makeText(DocumentManagerActivity.this,dbid+"",Toast.LENGTH_SHORT).show();
//
//        if(ContextCompat.checkSelfPermission(DocumentManagerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
//        {
//
//            ActivityCompat.requestPermissions(DocumentManagerActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},111);
//
//        }
//        else {
//
//
//           // showFileOptionDialog();
//
//
//
//        }
    }



    public void downloadDocuments(String id,String filename)
    {
        fileid=id;
        istodownload=true;
        filenametoDownload=filename;

        Toast.makeText(DocumentManagerActivity.this,filenametoDownload,Toast.LENGTH_SHORT).show();
        Toast.makeText(DocumentManagerActivity.this,"file id : "+id,Toast.LENGTH_SHORT).show();

        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        googleSignInClient = GoogleSignIn.getClient(DocumentManagerActivity.this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }


    public void showDocuments()
    {
        try {
            List<CommonData> commonData = new DatabaseHelper(DocumentManagerActivity.this).getData(Utils.DBtables.TABLE_DOCUMENT);

            for(int p=0;p<commonData.size();p++)
            {
                try{

                    JSONObject jsonObject=new JSONObject(commonData.get(p).getData());

          // folderid=  jsonObject.getString("folderid");
                   // holder.txtUsername.setText(jsonObject.getString("fileid"));



                }catch (Exception e)
                {

                }
            }


            if (commonData.size() > 0) {
                recycler.setVisibility(View.VISIBLE);

                DocumentAdapter documentAdapter = new DocumentAdapter(DocumentManagerActivity.this, commonData);
                recycler.setLayoutManager(new LinearLayoutManager(DocumentManagerActivity.this));
                recycler.setAdapter(documentAdapter);

            } else {
                recycler.setVisibility(View.GONE);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void showDocumentDialog()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels-10;
        int width = displayMetrics.widthPixels-10;
        double h=height/1.4;
        int hi =(int)h;


         dialog=new Dialog(DocumentManagerActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_adddoc);

         edtDocname=dialog.findViewById(R.id.edtDocname);
         txtfilepath=dialog.findViewById(R.id.txtfilepath);
        ImageView btnpickfile=dialog.findViewById(R.id.btnpickfile);
        Button btnSave=dialog.findViewById(R.id.btnSave);

        btnSave.setText(resources.getString(R.string.save));
        edtDocname.setHint(resources.getString(R.string.name));


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!edtDocname.getText().toString().equalsIgnoreCase("")) {

                    if(fileuploadToDrive!=null) {


                        GoogleSignInOptions signInOptions =
                            new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                    .requestEmail()
                                    .requestScopes(Drive.SCOPE_FILE)
                                    .build();
                    googleSignInClient = GoogleSignIn.getClient(DocumentManagerActivity.this, signInOptions);

                    // The result of the sign-in Intent is handled in onActivityResult.
                    startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);

                    }
                    else {

                        Toast.makeText(DocumentManagerActivity.this,"Please select a file",Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    Toast.makeText(DocumentManagerActivity.this,"Enter document name",Toast.LENGTH_SHORT).show();
                }

            }
        });


        btnpickfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
//
//                // Optionally, specify a URI for the directory that should be opened in
//                // the system file picker when it loads.
//                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uriToLoad);
//
//                startActivityForResult(intent, your-request-code);

                String permission="";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                    permission=Manifest.permission.READ_MEDIA_IMAGES;

                }
                else{

                    permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                }




                if(ContextCompat.checkSelfPermission(DocumentManagerActivity.this, permission)!= PackageManager.PERMISSION_GRANTED)
                {

                    ActivityCompat.requestPermissions(DocumentManagerActivity.this,new String[]{permission},111);

                }
                else {

                    showFileOptionDialog();




                }
            }
        });














        dialog.getWindow().setLayout(width, hi);


        dialog.show();
    }



    public void showFileOptionDialog()
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels-10;
        int width = displayMetrics.widthPixels-10;
        double h=height/1.9;
        int hi =(int)h;
        Dialog dialog=new Dialog(DocumentManagerActivity.this);


        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_fileoptiondialog);

        ImageView imgfromgallery =dialog.findViewById(R.id.imgfromgallery);
        TextView txtFromgallery =dialog.findViewById(R.id.txtFromgallery);

        ImageView imgfolder=dialog.findViewById(R.id.imgfolder);
        TextView txtfolder=dialog.findViewById(R.id.txtfolder);

        imgfolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType("*/*");
                startActivityForResult(
                        Intent.createChooser(chooseFile, "Choose a file"),
                        PICKFILE_RESULT_CODE
                );


            }
        });

        txtfolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType("*/*");
                startActivityForResult(
                        Intent.createChooser(chooseFile, "Choose a file"),
                        PICKFILE_RESULT_CODE
                );


            }
        });


        imgfromgallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent i = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, PICKIOmage_RESULT_CODE);


            }
        });

        txtFromgallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent i = new Intent(
                        Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, PICKIOmage_RESULT_CODE);

            }
        });

        dialog.getWindow().setLayout(width, hi);
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK) {
            switch (requestCode) {

                case PICKFILE_RESULT_CODE:

                    if(data!=null) {

                        String result;

                        Uri uri = data.getData();
                        File file = new File(uri.getPath());//create path from uri
//                    final String[] split = file.getPath().split(":");//split the path.
//                    String p = split[1];//assign it to a string(your choice).
                        // String filePath= null;
                        try {
                            result = PathUtil.getPath(DocumentManagerActivity.this, uri);

                          //  Log.e("Filepath", result);
                            fileuploadToDrive = new File(result);

                            if (fileuploadToDrive.exists()) {
                                Log.e("uploadedfileexist", "true");
                            } else {

                                Log.e("uploadedfileexist", "false");
                            }

                            filePath = fileuploadToDrive.getAbsolutePath();
                            if (txtfilepath != null) {
                                txtfilepath.setText(filePath);
                            }

                            Uri returnUri = data.getData();
                            mimetype = getContentResolver().getType(returnUri);
                            Toast.makeText(DocumentManagerActivity.this, mimetype, Toast.LENGTH_SHORT).show();


                            InputStream fis = null;
                            try {
                                fis = getContentResolver().openInputStream(uri);
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                byte[] bi = new byte[1024];
                                for (int readNum; (readNum = fis.read(bi)) != -1; ) {
                                    bos.write(bi, 0, readNum);
                                }
                                b = bos.toByteArray();
                                Toast.makeText(DocumentManagerActivity.this, b.toString(), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Log.d("mylog", e.toString());

                                //Toast.makeText(DocumentManagerActivity.this,"Error on byte array :"+e.toString(),Toast.LENGTH_SHORT).show();
                            }

                            if (isToUpdate) {

                                GoogleSignInOptions signInOptions =
                                        new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                                .requestEmail()
                                                .requestScopes(Drive.SCOPE_FILE)
                                                .build();
                                googleSignInClient = GoogleSignIn.getClient(DocumentManagerActivity.this, signInOptions);

                                // The result of the sign-in Intent is handled in onActivityResult.
                                startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
                            }


                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }

                    }
                    break;


                case PICKIOmage_RESULT_CODE:

                    if (resultCode == -1) {


                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;

                        final Dialog d = new Dialog(DocumentManagerActivity.this);
                        d.setContentView(R.layout.layout_cropdialog);


                        ImageView imgback = d.findViewById(R.id.imgback);

                        ImageView imgcrop = d.findViewById(R.id.imgcrop);

                        final CropImageView cropImageView = d.findViewById(R.id.cropImageView);

                        cropImageView.setImageUriAsync(data.getData());

                        imgback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                d.dismiss();
                            }
                        });

                        imgcrop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                d.dismiss();

                                try {
                                    Bitmap cropped = cropImageView.getCroppedImage();

                                    Long tsLong = System.currentTimeMillis() / 1000;
                                    String ts = tsLong.toString();
                                    if (isToUpdate) {
                                        fileuploadToDrive = new File(getExternalCacheDir() + "/" + filenameToupdate + ".jpg");
                                    } else {

                                        fileuploadToDrive = new File(getExternalCacheDir() + "/" + edtDocname.getText().toString() + "_" + ts + ".jpg");


                                    }
                                    filePath = fileuploadToDrive.getAbsolutePath();
                                    if (txtfilepath != null) {
                                        txtfilepath.setText(filePath);
                                    }


                                    if (!fileuploadToDrive.exists()) {

                                        fileuploadToDrive.createNewFile();
                                    }
                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    cropped.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

                                    FileOutputStream fo = new FileOutputStream(fileuploadToDrive);
                                    fo.write(bytes.toByteArray());
                                    fo.close();


                                    Bitmap bm = BitmapFactory.decodeFile(fileuploadToDrive.getAbsolutePath());
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                                    b = baos.toByteArray();

                                    mimetype = "image/*";

//                                Uri returnUri = Uri.fromFile(fileuploadToDrive);
//                                String mimeType = getContentResolver().getType(returnUri);
//                                Toast.makeText(DocumentManagerActivity.this,mimeType,Toast.LENGTH_SHORT).show();

                                    if (isToUpdate) {
                                        GoogleSignInOptions signInOptions =
                                                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                                        .requestEmail()
                                                        .requestScopes(Drive.SCOPE_FILE)
                                                        .build();
                                        googleSignInClient = GoogleSignIn.getClient(DocumentManagerActivity.this, signInOptions);

                                        // The result of the sign-in Intent is handled in onActivityResult.
                                        startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);

                                    }


                                } catch (Exception e) {

                                }


                            }
                        });

                        d.getWindow().setLayout(width, height);
                        d.show();

                    }


                    break;

                case REQUEST_CODE_SIGN_IN:
                    if (data != null) {
                        if (isToUpdate) {
                            handleResultToUpdate(data);
                        } else if (istodownload) {
                            handleResultToDownload(data);
                        } else {
                            handleSignInResult(data);
                        }


                    }

                    break;
            }
        }
    }

    public void handleResultToUpdate(Intent result)
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");



        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        DocumentManagerActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();
                        if(!fileid.equalsIgnoreCase("")) {

                          //  Toast.makeText(DocumentManagerActivity.this,"file id not null",Toast.LENGTH_SHORT).show();

                            Executor mExecutor = Executors.newSingleThreadExecutor();
                            Tasks.call(mExecutor, () -> {
                                try {




                                  //  com.google.api.services.drive.model.File metadata1 = new com.google.api.services.drive.model.File().setName(fileuploadToDrive.getName());

                                    // Convert content to an AbstractInputStreamContent instance.
                                   // ByteArrayContent contentStream = new ByteArrayContent("*/*",b);

                                    // Update the metadata and contents.


                                    googleDriveService.files().delete(fileid).execute();


                                   // progressFragment.dismiss();


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                           // progressFragment.dismiss();

                                            updateFileData(dbid);

                                            Toast.makeText(DocumentManagerActivity.this,"file deleted successfully",Toast.LENGTH_SHORT).show();

                                            Utils.showAlertWithSingle(DocumentManagerActivity.this, "File updated successfully", new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });








                                        }
                                    });





                                    fileid="";
                                }catch (Exception e)
                                {


                                    File f=new File(Environment.getExternalStorageDirectory()+"/errordelete.txt");
                                    if(f.exists())
                                    {
                                        f.delete();
                                        f.createNewFile();
                                    }
                                    else {
                                        f.createNewFile();
                                    }

                                    try {
                                      OutputStream fileout=new FileOutputStream(f);
                                        OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);
                                        outputWriter.write(e.toString());
                                        outputWriter.close();
                                        //display file saved message
//                                        Toast.makeText(getBaseContext(), "File saved successfully!",
//                                                Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }



                                    //Toast.makeText(DocumentManagerActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                                }

                                return null;

                            });




                        }
                        else {


                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Log.e("TAAG",e.toString());

            }
        });
    }


    public void updateFileData(String databaseid)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{

//                    JSONObject js1=new JSONObject(cm_updating.getData());
//
////                    holder.txtTitle.setText(js1.getString("name"));
////                    holder.txtUsername.setText(jsonObject.getString("filename"));
//
//
//                    JSONObject jsonObject=new JSONObject();
//                    jsonObject.put("name",js1.getString("name"));
//                    jsonObject.put("filename",fileuploadToDrive.getName());
//                    jsonObject.put("fileid",fileid);
//                    jsonObject.put("folderid",folderid);

                    new DatabaseHelper(DocumentManagerActivity.this).deleteData(cm_updating.getId(),Utils.DBtables.TABLE_DOCUMENT);

                    Utils.playSimpleTone(DocumentManagerActivity.this);

                    Toast.makeText(DocumentManagerActivity.this,"file updated successfully",Toast.LENGTH_SHORT).show();
                    isToUpdate=false;
                    filenameToupdate="";
                    fileid="";
                    if(progressFragment!=null) {
                        progressFragment.dismiss();
                    }
                    if(dialog!=null) {
                        dialog.dismiss();
                    }
                    fileid="";
                    fileuploadToDrive=null;
                    Toast.makeText(DocumentManagerActivity.this,"file deleted successfully",Toast.LENGTH_SHORT).show();
                    showDocuments();
                }catch (Exception e)
                {
                    Toast.makeText(DocumentManagerActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    public void handleResultToDownload(Intent result)
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");



        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        DocumentManagerActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();

                        Toast.makeText(DocumentManagerActivity.this,"Created google service id",Toast.LENGTH_SHORT).show();

                        if(!fileid.equalsIgnoreCase("")) {


                            try {

                               // Toast.makeText(DocumentManagerActivity.this, filenametoDownload, Toast.LENGTH_SHORT).show();



                               // Toast.makeText(DocumentManagerActivity.this, "file created", Toast.LENGTH_SHORT).show();


//                                Executor executor=new DirectExecutor();
//
//                                executor.execute(new Runnable() {
//                                    @Override
//                                    public void run() {
//
//                                    }
//                                });
//
//
//                                   // OutputStream outputStream = new ByteArrayOutputStream();




                            }catch (Exception e)
                            {

                            }


                            Executor mExecutor = Executors.newSingleThreadExecutor();
                            Tasks.call(mExecutor, () -> {
                                try {

                                    File downloadfile = new File(getExternalCacheDir() + "/" + filenametoDownload);

                                    if (!downloadfile.exists()) {

                                        downloadfile.createNewFile();
                                    } else {

                                        downloadfile.delete();
                                        downloadfile.createNewFile();
                                    }

                                    OutputStream outputStream =new  FileOutputStream(downloadfile);

                                    // com.google.api.services.drive.Drive.Files

                                    try {

                                      //  Toast.makeText(DocumentManagerActivity.this, "file is downloading", Toast.LENGTH_SHORT).show();

                                        googleDriveService.files().get(fileid)
                                                .executeMediaAndDownloadTo(outputStream);

                                        // Toast.makeText(DocumentManagerActivity.this, "file is downloading", Toast.LENGTH_SHORT).show();


                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(DocumentManagerActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                                            }
                                        });

                                        // Toast.makeText(DocumentManagerActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                                    }


                                    istodownload=false;
                                    fileid="";

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utils.showAlertWithSingle(DocumentManagerActivity.this, "File downloaded successfully", new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });
                                            progressFragment.dismiss();

//                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//
//                                                Uri photoURI = FileProvider.getUriForFile(DocumentManagerActivity.this, getApplicationContext().getPackageName() + ".provider", downloadfile);
//
//                                                Intent intent = new Intent();
//                                                intent.setAction(Intent.ACTION_VIEW);
//                                                intent.setDataAndType(photoURI, "image/*");
//                                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                                startActivity(intent);
//
//                                            } else {
//
//                                                Intent intent = new Intent();
//                                                intent.setAction(Intent.ACTION_VIEW);
//                                                intent.setDataAndType(Uri.fromFile(downloadfile), "image/*");
//                                                startActivity(intent);
//                                            }

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                                Uri photoURI = FileProvider.getUriForFile(DocumentManagerActivity.this, getApplicationContext().getPackageName() + ".provider", downloadfile);

                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setType("*/*");
                                                intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                                                startActivity(Intent.createChooser(intent, "Share Bill"));

                                            } else {

                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setType("*/*");
                                                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(downloadfile));
                                                startActivity(Intent.createChooser(intent, "Share Image"));
                                            }



                                        }
                                    });





                                    fileid="";
                                }catch (Exception e)
                                {
                                    Toast.makeText(DocumentManagerActivity.this,"error:"+e.toString(),Toast.LENGTH_SHORT).show();

                                }

                                        return null;

                                    });




                        }
                        else {


                            Toast.makeText(DocumentManagerActivity.this,"file id is missing",Toast.LENGTH_SHORT).show();


                            progressFragment.dismiss();
                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Log.e("TAAG",e.toString());

            }
        });
    }





    private void handleSignInResult(Intent result) {

         progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");






        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        Toast.makeText(DocumentManagerActivity.this,"started",Toast.LENGTH_SHORT).show();

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        DocumentManagerActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();
                        if(!filePath.equalsIgnoreCase("")) {

                            Executor mExecutor = Executors.newSingleThreadExecutor();
                          //  Toast.makeText(DocumentManagerActivity.this,"filepath is not null",Toast.LENGTH_SHORT).show();


                            Tasks.call(mExecutor, () -> {

                                //   progressFragment.dismiss();

                               // googleDriveService.files().delete(fileid).execute();

                                Tasks.call(mExecutor, () -> {
                                    com.google.api.services.drive.model.File metadata = new com.google.api.services.drive.model.File()
                                            .setParents(Collections.singletonList("root"))

                                            .setName(fileuploadToDrive.getName());

                                    com.google.api.services.drive.model.File googleFile = googleDriveService.files().create(metadata).execute();
                                    if (googleFile == null) {
                                        throw new IOException("Null result when requesting file creation.");
                                    }
                                    fileid = googleFile.getId();


                                    Tasks.call(mExecutor, () -> {

                                        //progressFragment.dismiss();
                                        // Create a File containing any metadata changes.
                                        com.google.api.services.drive.model.File metadata1 = new com.google.api.services.drive.model.File().setName(fileuploadToDrive.getName());

                                        // Convert content to an AbstractInputStreamContent instance.
                                       // ByteArrayContent contentStream = ByteArrayContent.fromString("*/*", uploadgdrivedata);

                                        // Update the metadata and contents.
                                        ByteArrayContent contentStream = new ByteArrayContent(mimetype, b);

                                        googleDriveService.files().update(fileid, metadata1, contentStream).execute();
                                        saveFileDb();
                                        //updateGoogleDriveFileId(fileid);
                                        return null;
                                    });


                                    return googleFile.getId();

                                });



                                return null;
                            });
















                        }
                        else {


                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Utils.showAlertWithSingle(DocumentManagerActivity.this,e.toString(),null);

                Log.e("TAAG",e.toString());

            }
        });
    }




public void saveFileDb()
{


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("name",edtDocname.getText().toString());
                jsonObject.put("filename",fileuploadToDrive.getName());
                    jsonObject.put("fileid",fileid);
                jsonObject.put("folderid",folderid);

                new DatabaseHelper(DocumentManagerActivity.this).addData(Utils.DBtables.TABLE_DOCUMENT,jsonObject.toString());
                   // Toast.makeText(DocumentManagerActivity.this,"file updated successfully",Toast.LENGTH_SHORT).show();
                    Utils.playSimpleTone(DocumentManagerActivity.this);
                if(progressFragment!=null) {
                    progressFragment.dismiss();
                }
                if(dialog!=null) {
                    dialog.dismiss();
                }
                fileid="";
                fileuploadToDrive=null;
                    Toast.makeText(DocumentManagerActivity.this,"file uploaded successfully",Toast.LENGTH_SHORT).show();
                showDocuments();
                }catch (Exception e)
                {

                }
            }
        });










}




public void show()
{
    showDocuments();
}





}