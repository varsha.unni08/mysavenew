package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MileStoneManipulateAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.data.domain.MileStoneTarget;
import com.centroid.integraaccounts.dialogs.MileStoneEditFragment;
import com.centroid.integraaccounts.dialogs.MileStoneListFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.MileStoneListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MileStoneEditActivity extends AppCompatActivity {
    
    ImageView imgback,imgcheck;

  
    RecyclerView recycler;
    TextView txtNodata;

    String id="";

    CommonData data=null;

    List<MileStone>mileStones=new ArrayList<>();

    FloatingActionButton fabadd;
    MileStoneManipulateAdapter mileStoneManipulateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mile_stone_edit);
        getSupportActionBar().hide();
        
        imgback=findViewById(R.id.imgback);
        imgcheck=findViewById(R.id.imgcheck);
        recycler=findViewById(R.id.recycler);

        txtNodata=findViewById(R.id.txtNodata);
        fabadd=findViewById(R.id.fabadd);


        Intent i=getIntent();
        id=i.getStringExtra("data");

        if(id!=null) {

            if (!id.equalsIgnoreCase("")) {

                List<CommonData> cmd = new DatabaseHelper(MileStoneEditActivity.this).getDataByID(id, Utils.DBtables.TABLE_TARGET);
                if (cmd.size() > 0) {
                    data = cmd.get(0);
                } else {

                    data = null;
                }

            } else {

                data = null;
            }
        }
        else{

            data = null;

        }

        imgcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MileStoneTarget mileStoneTarget=new MileStoneTarget();
//                mileStoneTarget.setTargetamount(targetamount);

                if(mileStoneManipulateAdapter!=null) {


                    List<MileStone>ml=  mileStones;

                    if(ml.size()>0)
                    {

                        boolean isvalid=true;
                        Date stdate=null;
                        Date eddate=null;
                        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");


                        for(int i=0;i<ml.size();i++)
                        {

                            MileStone m=ml.get(i);

                            if(!m.getStart_date().equalsIgnoreCase("")&&!m.getEnd_date().equalsIgnoreCase(""))
                            {

                                if(i>0)
                                {
                                    try {
                                        Date stdate_compare = myFormat.parse(m.getStart_date());

                                        if(stdate_compare.after(eddate))
                                        {


                                        }
                                        else{


                                            isvalid=false;

                                            break;
                                        }

                                    }catch (Exception e)
                                    {
                                        isvalid=false;

                                        break;
                                    }

                                }


                                if(!m.getAmount().equalsIgnoreCase(""))
                                {
                                    double b=Double.parseDouble(m.getAmount());

                                    if(b!=0)
                                    {


                                        try{



                                            stdate=myFormat.parse(m.getStart_date());
                                            eddate=myFormat.parse(m.getEnd_date());

                                            if(stdate.before(eddate))
                                            {

                                                if(i>0)
                                                {


                                                }







                                            }
                                            else{

                                                isvalid=false;

                                                break;

                                            }





                                        }catch (Exception e)
                                        {

                                            isvalid=false;
                                            break;

                                        }




                                    }
                                    else{

                                        isvalid=false;
                                        break;
                                    }

                                }
                                else{

                                    isvalid=false;
                                    break;
                                }
                            }
                            else {

                                isvalid=false;
                                break;
                            }


                        }



                        if(isvalid)
                        {

                            mileStoneTarget.setMls(mileStones);
                            Intent intent = new Intent();
                            intent.putExtra("MileStone",mileStoneTarget);
                            setResult(Activity.RESULT_OK, intent);
                            finish();

                        }
                        else{

                            Utils.showAlert(MileStoneEditActivity.this, "Please enter the date properly.Amount should not be zero", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });

                        }







                    }





                }


            }
        });






        
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                onBackPressed();
            }
        });

        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<CommonData> commondata_target=new DatabaseHelper(MileStoneEditActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);

                List<MileStone>mls=new ArrayList<>();


                if(commondata_target.size()>0)
                {

                    for(CommonData cmd:commondata_target)
                    {

                        String jsondata=cmd.getData();

                        try{

                            JSONObject js3=new JSONObject(jsondata);

                            String amount=js3.getString("amount");
                            String start_date=js3.getString("start_date");
                            String end_date=js3.getString("end_date");
                            String targetid=js3.getString("targetid");

                            if(targetid.equalsIgnoreCase(id))
                            {
                                MileStone mileStone=new MileStone();
                                mileStone.setId(cmd.getId());
                                mileStone.setSelected(0);
                                mileStone.setEnd_date(end_date);
                                mileStone.setStart_date(start_date);
                                mileStone.setAmount(amount);
                                mileStone.setTargetid(targetid);
                                mls.add(mileStone);
                            }




                        }catch (Exception e)
                        {

                        }


                    }







                }

                MileStoneEditFragment mf=new MileStoneEditFragment(new MileStoneListener() {
                    @Override
                    public void onMileStoneCompleted(MileStone mileStones) {

                        setupData();
                    }
                },mls);

                Bundle b=new Bundle();
                b.putSerializable("milestone",new MileStone());
                b.putString("targetid",id);
                b.putInt("foredit",0);
                b.putInt("position",mls.size());
                mf.setArguments(b);
                mf.show(getSupportFragmentManager(),"");


            }
        });

        setupData();


    }



    public void setupData()
    {

        mileStones.clear();
        List<CommonData> commondata_target=new DatabaseHelper(MileStoneEditActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);



        if(commondata_target.size()>0)
        {

            for(CommonData cmd:commondata_target)
            {

                String jsondata=cmd.getData();

                try{

                    JSONObject js3=new JSONObject(jsondata);

                    String amount=js3.getString("amount");
                    String start_date=js3.getString("start_date");
                    String end_date=js3.getString("end_date");
                    String targetid=js3.getString("targetid");

                    if(targetid.equalsIgnoreCase(id))
                    {
                        MileStone mileStone=new MileStone();
                        mileStone.setId(cmd.getId());
                        mileStone.setSelected(0);
                        mileStone.setEnd_date(end_date);
                        mileStone.setStart_date(start_date);
                        mileStone.setAmount(amount);
                        mileStone.setTargetid(targetid);
                        mileStones.add(mileStone);
                    }




                }catch (Exception e)
                {

                }


            }







        }


        if(mileStones.size()>0)
        {
            txtNodata.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            mileStoneManipulateAdapter=    new MileStoneManipulateAdapter(MileStoneEditActivity.this,mileStones);

            recycler.setLayoutManager(new LinearLayoutManager(MileStoneEditActivity.this));
            recycler.setAdapter(mileStoneManipulateAdapter);


        }
        else{

            txtNodata.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);
        }
    }


    public void setupDataForEditing(int position)
    {

        MileStone ml=mileStones.get(position);

        String id1=ml.getId();

        List<CommonData> commondata_target=new DatabaseHelper(MileStoneEditActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);

        List<MileStone>mls=new ArrayList<>();


        if(commondata_target.size()>0)
        {

            for(CommonData cmd:commondata_target)
            {

                String jsondata=cmd.getData();

                try{

                    JSONObject js3=new JSONObject(jsondata);

                    String amount=js3.getString("amount");
                    String start_date=js3.getString("start_date");
                    String end_date=js3.getString("end_date");
                    String targetid=js3.getString("targetid");

                    if(targetid.equalsIgnoreCase(id))
                    {
                        MileStone mileStone=new MileStone();
                        mileStone.setId(cmd.getId());
                        mileStone.setSelected(0);
                        mileStone.setEnd_date(end_date);
                        mileStone.setStart_date(start_date);
                        mileStone.setAmount(amount);
                        mileStone.setTargetid(targetid);
                        mls.add(mileStone);
                    }




                }catch (Exception e)
                {

                }


            }







        }


        MileStoneEditFragment mf=new MileStoneEditFragment(new MileStoneListener() {
            @Override
            public void onMileStoneCompleted(MileStone mileStones) {

                setupData();


            }
        },mls);
        Bundle b=new Bundle();
        b.putSerializable("milestone",ml);
        b.putString("targetid",id);
        b.putInt("foredit",1);
        b.putInt("position",position);
        mf.setArguments(b);
        mf.show(getSupportFragmentManager(),"");








    }
}