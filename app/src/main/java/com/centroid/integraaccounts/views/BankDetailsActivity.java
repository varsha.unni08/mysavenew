package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankCashAdapter;
import com.centroid.integraaccounts.adapter.IncExpFullAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class BankDetailsActivity extends AppCompatActivity {


    String setupid="0",month="",year="";

    TextView txtMonthyear,txtdata,txtHead,txtclosebalance;

    LinearLayout layout_head;
    ImageView imgback,imgdownload;

    RecyclerView recyclerincome;

    String arrmonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    int fromledger=0,cashbank=0;

    String openingbalance="0",Type="Debit",close_balance="0";
    List<Accounts> IncExp=new ArrayList<>();

    String accname="";

    int bydate;

    String startdate="",enddate="";

    String debitcredit="";

    String closebalacebeforedate="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        getSupportActionBar().hide();
        txtHead=findViewById(R.id.txtHead);
        imgdownload=findViewById(R.id.imgdownload);
        txtclosebalance=findViewById(R.id.txtclosebalance);
        setupid= getIntent().getStringExtra("accountsetupid");
        month=getIntent().getStringExtra("month");
        debitcredit=getIntent().getStringExtra("debitcredit");
        year= getIntent().getStringExtra("year");
        close_balance=getIntent().getStringExtra("close_balance");
        fromledger=getIntent().getIntExtra("fromledger",0);
        cashbank=getIntent().getIntExtra("cashbank",0);
        closebalacebeforedate=getIntent().getStringExtra("closebalacebeforedate");

        bydate=getIntent().getIntExtra("bydate",0);


        double clba=Double.parseDouble(close_balance);

        if(clba<0) {
            double t = -1 * clba;

            txtclosebalance.setText("Closing balance : " + t + " " + getString(R.string.rs));
        }
        else {
            txtclosebalance.setText("Closing balance : " + close_balance + " " + getString(R.string.rs));
        }

//
//        if(cashbank==1)
//        {
//
//            txtHead.setText("Cash/Bank balance");
//
//        }








        try {




            // holder.txtClosingbalance.setText(ledgerAccounts.get(position).getClosingbalance() + " " + BankDetailsActivity.this.getString(R.string.rs));
            if (!setupid.equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(BankDetailsActivity.this).getAccountSettingsByID(setupid);

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    openingbalance=jsonObject.getString("Amount");
                    Type=jsonObject.getString("Type");

                    // holder.txtAccount.setText();

                    accname=jsonObject.getString("Accountname");

                    if(fromledger==1) {

                        txtHead.setText("Ledger : " + jsonObject.getString("Accountname"));

                    }
                    else {

                        txtHead.setText("" + jsonObject.getString("Accountname"));
                    }
                }
            }
        }catch (Exception e)
        {

        }






        txtMonthyear=findViewById(R.id.txtMonthyear);
        imgback=findViewById(R.id.imgback);
        txtdata=findViewById(R.id.txtdata);

        layout_head=findViewById(R.id.layout_head);
        recyclerincome=findViewById(R.id.recyclerincome);



        if(bydate==1)
        {

            startdate=getIntent().getStringExtra("startdate");
            enddate=getIntent().getStringExtra("enddate");



            txtMonthyear.setText(" From date : "+startdate + "\n\n To date : " + enddate);
        }
        else {

            int m=Integer.parseInt(month)-1;

            txtMonthyear.setText(arrmonth[m] + "/" + year);
        }





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });



        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.showAlert(BankDetailsActivity.this, "Do you want to download pdf ? ", new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                        if(ContextCompat.checkSelfPermission(BankDetailsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {


                            ActivityCompat.requestPermissions(BankDetailsActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
                        }
                        else {

                            downloadPdf();
                        }

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });



            }
        });










        getAccounts();
    }


    private void downloadPdf()
    {

        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "Save/" + "LedgersBook");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/Ledger" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();


            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk(accname+" \n\n", mOrderDetailsTitleFont);// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);


            document.add(mOrderDetailsTitleParagraph);

            document.add(new Paragraph(" \n\n"));

            if(bydate==1) {

                document.add(new Paragraph("Start date : " + startdate + "\n\n End date : " + enddate + "\n\n"));
            }
            else {

                int m=Integer.parseInt(month)-1;

                //txtMonthyear.setText(arrmonth[m] + "/" + year);

                document.add(new Paragraph(arrmonth[m] + "/" + year+"\n\n"));

            }


            PdfPTable table = new PdfPTable(4);
            table.addCell("Date");
            table.addCell("Account");
            table.addCell("Amount");
            table.addCell("Debit/Credit");


            for (int i=0;i<IncExp.size();i++)
            {
                table.addCell(IncExp.get(i).getACCOUNTS_date());

                if(IncExp.get(i).getACCOUNTS_setupid().equalsIgnoreCase("-1")) {

                    table.addCell("Opening balance");
                }
                else {


                    List<Accounts> accbyEntry = new DatabaseHelper(BankDetailsActivity.this).getAccountsDataByEntryId(IncExp.get(i).getACCOUNTS_id() + "");

                    List<Accounts>accbyID=new DatabaseHelper(BankDetailsActivity.this).getAccountsDataBYid(IncExp.get(i).getACCOUNTS_entryid() + "");


                    if (accbyEntry.size() > 0) {
                        // IncExp.add(accbyEntry.get(0));

                        List<CommonData> cm = new DatabaseHelper(BankDetailsActivity.this).getDataByID(accbyEntry.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                        if (cm.size() > 0) {

                            try {

                                JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                                // holder.txtaccount.setText(jsonObject.getString("Accountname"));

                                table.addCell(jsonObject.getString("Accountname"));


                            } catch (Exception e) {

                            }

                        } else {

                            table.addCell("Cash");
                        }


                    }

                    if (accbyID.size() > 0) {
                        // IncExp.add(accbyEntry.get(0));

                        List<CommonData> cm = new DatabaseHelper(BankDetailsActivity.this).getDataByID(accbyID.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                        if (cm.size() > 0) {

                            try {

                                JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                                // holder.txtaccount.setText(jsonObject.getString("Accountname"));

                                table.addCell(jsonObject.getString("Accountname"));


                            } catch (Exception e) {

                            }

                        } else {

                            table.addCell("Cash");
                        }


                    }

                }

                double d=Double.parseDouble(IncExp.get(i).getACCOUNTS_amount());
                if(d<0||String.valueOf(d).contains("-"))
                {
                    d=d*-1;
                }


                table.addCell(d+"");
                if(debitcredit.equalsIgnoreCase("Debit"))
                {

                    table.addCell("Debit");


                }

                else {

                    table.addCell("Credit");
                }




            }




            double d=Double.parseDouble(close_balance);

            if(d<0||String.valueOf(d).contains("-"))
            {
                d=d*-1;
            }


            document.add(table);
            document.add(new Paragraph(" \n\n"));
            document.add(new Paragraph(" Closing balance : "+d));

            document.close();
            Utils.playSimpleTone(BankDetailsActivity.this);

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(BankDetailsActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }


        }catch (Exception e)
        {

        }

    }





    private void getAccounts()
    {


        try {

            IncExp.clear();

            List<Accounts> accounts = new DatabaseHelper(BankDetailsActivity.this).getAccountsDataBYsetupid(setupid);


            for (Accounts acc : accounts) {
                String monh = acc.getACCOUNTS_month();
                String year1 = acc.getACCOUNTS_year();

//                if (debitcredit.equalsIgnoreCase("Debit")) {
//
//                    acc.setACCOUNTS_type(Utils.Cashtype.debit + "");
//                } else {
//                    acc.setACCOUNTS_type(Utils.Cashtype.credit + "");
//
//                }

                if (bydate == 1) {


                    String accounts_date = acc.getACCOUNTS_date();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                    Date accdate=sdf.parse(accounts_date);


                    Date startdat=sdf.parse(startdate);
                    Date endd=sdf.parse(enddate);



                    if(accdate.equals(startdat)&&accdate.before(endd)) {

                        IncExp.add(acc);

                    }
                    else  if(accdate.after(startdat) && accdate.equals(endd))
                    {
                        IncExp.add(acc);
                    }
                    else if(accdate.equals(startdat) && accdate.equals(endd))
                    {

                        IncExp.add(acc);
                    }
                    else if (accdate.after(startdat) && accdate.before(endd)) {

                        IncExp.add(acc);
                    }





                } else {

                    if (String.valueOf(month).equalsIgnoreCase(monh) && String.valueOf(year).equalsIgnoreCase(year1)) {

                        IncExp.add(acc);
                    }
                }


            }


            Collections.sort(IncExp, new Comparator<Accounts>() {
                @Override
                public int compare(Accounts accounts, Accounts t1) {

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate = null;

                    Date endDate = null;
                    try {


                        strDate = sdf.parse(accounts.getACCOUNTS_date());
                        endDate = sdf.parse(t1.getACCOUNTS_date());


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return endDate.compareTo(strDate);


                    // return 0;
                }
            });


            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);

            int m=0;

            if(bydate==1)
            {
                String arr[]= startdate.split("-");

                m=Integer.parseInt(arr[1]);


            }
            else {

                m = Integer.parseInt(month);

            }


            double openingbal = Double.parseDouble(openingbalance);

            double closingbalancebeforedate = getClosingbalancebeforedate(openingbal, setupid, "1-" + m + "-" + year, "");


            //txtMonthyear.setText(arrmonth[m] + "/" + year);

            Accounts acc = new Accounts();

            acc.setACCOUNTS_entryid("0");
            acc.setACCOUNTS_date("1-"+m+"-" + year);
            acc.setACCOUNTS_setupid("-1");
            acc.setACCOUNTS_amount(closingbalancebeforedate + "");

            if (debitcredit.equalsIgnoreCase("Debit")) {

                acc.setACCOUNTS_type(Utils.Cashtype.debit + "");
            } else {
                acc.setACCOUNTS_type(Utils.Cashtype.credit + "");

            }
            acc.setACCOUNTS_remarks("");
            acc.setACCOUNTS_month("1");
            acc.setACCOUNTS_year(year + "");

            IncExp.add(0, acc);

//        acc.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
//        acc.setACCOUNTS_cashbanktype(Utils.CashBanktype.account+"");


//       if(accounts.size()>0)
//       {
//
//           for (Accounts acs:accounts
//                ) {
//
//
//               IncExp.add(acs);
//
//               List<Accounts> accbyEntry = new DatabaseHelper(BankDetailsActivity.this).getAccountsDataByEntryId(acs.getACCOUNTS_id() + "");
//
//
//               if (accbyEntry.size() > 0) {
//                   IncExp.add(accbyEntry.get(0));
//               }
//
//           }
//       }


            if (IncExp.size() > 0) {
                layout_head.setVisibility(View.VISIBLE);
                recyclerincome.setVisibility(View.VISIBLE);

                BankCashAdapter incExpFullAdapter = new BankCashAdapter(BankDetailsActivity.this, IncExp);
                recyclerincome.setLayoutManager(new LinearLayoutManager(BankDetailsActivity.this));
                recyclerincome.setAdapter(incExpFullAdapter);
            } else {
                recyclerincome.setVisibility(View.GONE);

                layout_head.setVisibility(View.GONE);
            }


        }catch (Exception e)
        {

        }

    }



    private double getClosingbalancebeforedate(double openingbalance, String id, String selected_date,String type) {

        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeDate(BankDetailsActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());


                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());


                        }


                    }
                }
                else if(id.equalsIgnoreCase("0")&&acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {


                    if(type.equalsIgnoreCase("Income"))
                    {
                        openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                    }
                    else {

                        openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());

                    }




                }


            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;
        }

        return closingbalancebeforemonth;
    }
}
