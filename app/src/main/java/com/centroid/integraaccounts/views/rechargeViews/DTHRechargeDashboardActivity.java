package com.centroid.integraaccounts.views.rechargeViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.DTHoperatorAdapter;
import com.centroid.integraaccounts.adapter.RechargeHistoryDataAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.DTHPlansFromServer;
import com.centroid.integraaccounts.data.domain.RechargeHistoryData;
import com.centroid.integraaccounts.data.domain.RechargeHistoryStatus;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.services.OperatorSelectionListener;
import com.centroid.integraaccounts.views.DTHPlansActivity;
import com.centroid.integraaccounts.views.DTHRechargeActivity;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.initiated.InitiatedRecharge;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DTHRechargeDashboardActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edittextMob;
    FloatingActionButton fab_submit;
    RecyclerView recycler,recycler_recharge;

  String  spkey = "";
    String  Selected_operator_to_server = "";
    String Selected_operator = "", Selected_operator_code="";
    String fullurl="",fullString="";

    String dthopfind_base_url="https://mysaveapp.com/rechargeAPI/getDTHOperator.php";
    String recharge_url="https://mysaveapp.com/rechargeAPI/RechargePhone.php?";
    TextView txtHead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dthrecharge_dashboard);
        getSupportActionBar().hide();
        recycler=findViewById(R.id.recycler);
        edittextMob=findViewById(R.id.edittextMob);
        fab_submit=findViewById(R.id.fab_submit);
        txtHead=findViewById(R.id.txtHead);
        recycler_recharge=findViewById(R.id.recycler_recharge);

        fab_submit.setOnClickListener(DTHRechargeDashboardActivity.this);

        recycler.setLayoutManager(new GridLayoutManager(DTHRechargeDashboardActivity.this,3));
        DTHoperatorAdapter mobileOperatorAdapter = new DTHoperatorAdapter(DTHRechargeDashboardActivity.this, new OperatorSelectionListener() {
            @Override
            public void onOperatorSelected(int position, String selectedoperator) {


                    spkey = RechargePlans.DTHPlans.spkeys[position];
                    Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[position];
                    Selected_operator = RechargePlans.DTHPlans.arr_plans[position];
                    Selected_operator_code=RechargePlans.DTHPlans.arr_plans_opcode[position];

                    redirectToPlans();




            }
        });
        recycler.setAdapter(mobileOperatorAdapter);
       // RechargePlans.DTHPlans.arr_plans_img.length
        getRechargeHistory();

    }




    @Override
    public void onClick(View view) {



//        switch (view.getId())
//        {
//
//        case R.id.fab_submit :
//          //  validateCard();
//
//            break;
//
//
//
//
//
//
//        }

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getRechargeHistory();
    }

    public void retryRecharge(RechargeHistoryData rechargeHistoryData)
    {

        if(rechargeHistoryData.getPaymentstatus().equalsIgnoreCase("5")) {


            if(rechargeHistoryData.getStatus().equalsIgnoreCase("3")&&rechargeHistoryData.getRefundTransactionid()!=null)
            {

                for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                    String operator = RechargePlans.DTHPlans.arr_plans[j];

                    if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                        spkey = RechargePlans.DTHPlans.spkeys[j];
                        Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                        Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                        Selected_operator_code=RechargePlans.DTHPlans.arr_plans_opcode[j];
                        Intent intent=new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                        intent.putExtra("amount",rechargeHistoryData.getAmount());
                        intent.putExtra("spkey",spkey);
                        intent.putExtra("operator",Selected_operator);
                        intent.putExtra("cardnumber",rechargeHistoryData.getAccountNumber());
                        intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                        startActivity(intent);


                        break;
                    }


                }
            }

            else {
                if (rechargeHistoryData.getStatus().equalsIgnoreCase("2")) {

                    try {
                        String date_recharge = rechargeHistoryData.getRechargeDate();
                        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date rechargedt=dateFormat.parse(date_recharge);
                        DateFormat dateFormat1=new SimpleDateFormat("dd MMM yyyy");
                        String serversubmitdate=dateFormat1.format(rechargedt);

                        String recharge_retry_url = "https://mysaveapp.com/rechargeAPI/retryRechargeFromHistory.php?timestamp=" + Utils.getTimestamp() + "&agentid=" + rechargeHistoryData.getAgentId()+"&date="+serversubmitdate;


                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        Handler handler = new Handler(Looper.getMainLooper());
                        executorService.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    final ProgressFragment progressFragment = new ProgressFragment();
                                    progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                    Map<String, String> params = new HashMap<>();
                                    params.put("timestamp", Utils.getTimestamp());
                                    String fullurl = recharge_retry_url;

                                    fullString = "";
                                    URL url = new URL(fullurl);
                                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                    String line;
                                    while ((line = reader.readLine()) != null) {
                                        fullString += line;
                                    }
                                    reader.close();

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressFragment.dismiss();

                                            try {

                                                if (!fullString.isEmpty()) {

                                                    JSONObject jsonObject = new JSONObject(fullString);
                                                    String rpid = jsonObject.getString("rpid");
                                                    String agentid = jsonObject.getString("agentid");

                                                    if (jsonObject.getInt("status") == 2) {
                                                        updateStatus(rechargeHistoryData.getId(), 1, rechargeHistoryData.getPaymentstatus(), rpid, agentid);

                                                        Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, "Recharge Successful", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });


                                                    } else if (jsonObject.getInt("status") == 1) {

                                                        updateStatus(rechargeHistoryData.getId(), 2, rechargeHistoryData.getPaymentstatus(), rpid, agentid);

                                                        Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, "Recharge is still pending....", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });


                                                    } else {

                                                        updateStatus(rechargeHistoryData.getId(), 0, rechargeHistoryData.getPaymentstatus(), rpid, agentid);
                                                    }


                                                }

                                            } catch (Exception e) {

                                            }


                                        }
                                    });


                                } catch (Exception e) {


                                }
                            }
                        });

                    }
                    catch (Exception e)
                    {

                    }

                } else if (rechargeHistoryData.getStatus().equalsIgnoreCase("0")) {

                    int min = 1;
                    int max = 10000;
                    int random = new Random().nextInt((max - min) + 1) + min;


                    String phone = rechargeHistoryData.getAccountNumber();
//       String amount=Amount;
                    double amount1 = Double.parseDouble(rechargeHistoryData.getAmount());
                    String apireqid = random + "";
                    String customerno = rechargeHistoryData.getMobileNumber();

                    String urldata = recharge_url + "phone=" + phone + "&amount=" + amount1 + "&spkey=" + spkey + "&apireqid=" + apireqid + "&customerno=" + customerno;

                    ExecutorService executorService = Executors.newSingleThreadExecutor();
                    Handler handler = new Handler(Looper.getMainLooper());
                    executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final ProgressFragment progressFragment = new ProgressFragment();
                                progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                Map<String, String> params = new HashMap<>();
                                params.put("timestamp", Utils.getTimestamp());
                                String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                fullString = "";
                                URL url = new URL(fullurl);
                                BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                String line;
                                while ((line = reader.readLine()) != null) {
                                    fullString += line;
                                }
                                reader.close();

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        progressFragment.dismiss();

                                        if (!fullString.isEmpty()) {
                                            MobileRechargeResponse mobileRechargeResponse = new GsonBuilder().create().fromJson(fullString, MobileRechargeResponse.class);

                                            String msg = mobileRechargeResponse.getMsg();
                                            int status = 0;
                                            if (mobileRechargeResponse.getStatus().equals(2)) {
                                                status = 1;
                                                updateStatus(rechargeHistoryData.getId(), 1, rechargeHistoryData.getPaymentstatus(), mobileRechargeResponse.getRpid(), mobileRechargeResponse.getAgentid());
                                                Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });

                                            }
                                            if (mobileRechargeResponse.getStatus().equals(1)) {
                                                status = 2;
                                                updateStatus(rechargeHistoryData.getId(), 2, rechargeHistoryData.getPaymentstatus(), mobileRechargeResponse.getRpid(), mobileRechargeResponse.getAgentid());
                                                Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, "Your recharge is in pending", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });
                                            } else {
                                                status = 0;
                                                updateStatus(rechargeHistoryData.getId(), 0, rechargeHistoryData.getPaymentstatus(), mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid());
                                                Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });

                                            }


                                        }


                                    }
                                });


                            } catch (Exception e) {


                            }
                        }
                    });

                } else if (rechargeHistoryData.getStatus().equalsIgnoreCase("1")) {

                    for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                        String operator = RechargePlans.DTHPlans.arr_plans[j];

                        if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                            spkey = RechargePlans.DTHPlans.spkeys[j];
                            Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                            Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                            Selected_operator_code = RechargePlans.DTHPlans.arr_plans_opcode[j];

                            Intent intent = new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                            intent.putExtra("amount", rechargeHistoryData.getAmount());
                            intent.putExtra("spkey", spkey);
                            intent.putExtra("operator", Selected_operator);
                            intent.putExtra("cardnumber", rechargeHistoryData.getAccountNumber());
                            intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                            startActivity(intent);


                            break;
                        }


                    }

                }

            }

        }
        else{

            for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                String operator = RechargePlans.DTHPlans.arr_plans[j];

                if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                    spkey = RechargePlans.DTHPlans.spkeys[j];
                    Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                    Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                    Selected_operator_code=RechargePlans.DTHPlans.arr_plans_opcode[j];
                    Intent intent=new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                    intent.putExtra("amount",rechargeHistoryData.getAmount());
                    intent.putExtra("spkey",spkey);
                    intent.putExtra("operator",Selected_operator);
                    intent.putExtra("cardnumber",rechargeHistoryData.getAccountNumber());
                    intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                    startActivity(intent);


                    break;
                }


            }
        }


    }




    public void updateGenStatus(String transaction_id)
    {




        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());

        params.put("id", transaction_id);





        new RequestHandler(DTHRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                try {

                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        String msg = jsonObject.getString("message");


                    } else {


                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(String err) {


                Toast.makeText(DTHRechargeDashboardActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.updateGenStatus + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }



    public void updateStatus(String id,int status,String paymentstatus,String rpid,String agentid)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
        params.put("payment_status",paymentstatus);
        params.put("rpid", rpid+"");
        params.put("agentid",agentid);
        // params.put("device_id",token);

        new RequestHandler(DTHRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(paymentstatus.equalsIgnoreCase("5")&&status==1)
                {
                    updateGenStatus(id);
                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(DTHRechargeDashboardActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateRechargeTransactionStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }





    private void redirectToPlans()
    {
        Intent intent=new Intent(DTHRechargeDashboardActivity.this,DTHPlansActivity.class);
        intent.putExtra("spkey",spkey);
//        intent.putExtra("cardnumber",edittextMob.getText().toString());
        intent.putExtra("selectedoperator",Selected_operator);
        intent.putExtra("Selected_operator_code",Selected_operator_code);
        intent.putExtra("selectedoperator_to_SERVER",Selected_operator_to_server);
        startActivity(intent);
    }


    public void getRechargeHistory()
    {

//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(DTHRechargeDashboardActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

              //  progressFragment.dismiss();


                try{

                    RechargeHistoryStatus rechargeHistoryStatus=new GsonBuilder().create().fromJson(data,RechargeHistoryStatus.class);
                    if(rechargeHistoryStatus.getStatus().equals(1))
                    {

                        List<RechargeHistoryData> rechargeHistoryData=rechargeHistoryStatus.getData();

                        if(rechargeHistoryData.size()>0)
                        {

                            List<RechargeHistoryData>redata=new ArrayList<>();

                            for(int j=0;j<rechargeHistoryData.size();j++){
                                if(rechargeHistoryData.get(j).getRechargeType().equalsIgnoreCase("2"))
                                {
                                    redata.add(rechargeHistoryData.get(j));

                                }


                            }

                            if(redata.size()>0)
                            {
                                txtHead.setVisibility(View.VISIBLE);
                            }

//                            Collections.reverse(redata);

                            RechargeHistoryDataAdapter rechargeHistoryDataAdapter=new RechargeHistoryDataAdapter(DTHRechargeDashboardActivity.this,redata);
                            recycler_recharge.setLayoutManager(new GridLayoutManager(DTHRechargeDashboardActivity.this,2));
                            recycler_recharge.setAdapter(rechargeHistoryDataAdapter);


                        }
                        else{
                            txtHead.setVisibility(View.GONE);


                        }



                    }
                    else{
                        txtHead.setVisibility(View.GONE);

                    }







                }

                catch (Exception e)
                {

                }






            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();
                txtHead.setVisibility(View.GONE);
                //  Toast.makeText(MobileRechargeDashboardActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getRechargeHistory+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


    }


    public void retryRechargeFromInitiatedHistory(InitiatedRecharge rechargeHistoryData)
    {

    }

    public void retryRechargeInit(InitiatedRecharge rechargeHistoryData)
    {




    }



    private void getOperator(String cardnumber)
    {
        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");
        ExecutorService executorService= Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {


                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
//                    String fullurl=urldata+"&timestamp="+Utils.getTimestamp();
                    fullurl=dthopfind_base_url+"?timestamp="+ Utils.getTimestamp()+"&cardnumber="+cardnumber;
                    fullString = "";
                    URL url = new URL(fullurl);
                    URLConnection connection  = url.openConnection();
                    connection.setConnectTimeout(4000);
                    connection.setReadTimeout(5000);
                    connection.connect();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            progressFragment.dismiss();

                            try{

                                Log.e("DTHOperator Response",fullString);
                                JSONObject jsonObject=new JSONObject(fullString);

                                JSONObject js=jsonObject.getJSONObject("records");

                                if(js.has("status"))
                                {

                                    int status=js.getInt("status");

                                    if(status==1)
                                    {

                                        String operator=js.getString("Operator");

                                        for(int i=0;i<RechargePlans.DTHPlans.arr_plans.length;i++)
                                        {

                                            double d=Utils.similarity(RechargePlans.DTHPlans.arr_plans[i].trim(),operator);
                                            Log.e("Similarity Response",d+","+RechargePlans.DTHPlans.arr_plans[i]);
                                            if(d>=0.500)
                                            {

                                                spkey=RechargePlans.DTHPlans.spkeys[i];
                                                Selected_operator_to_server=RechargePlans.DTHPlans.arr_plans_toServer[i];
                                                Selected_operator=RechargePlans.DTHPlans.arr_plans[i];
                                                redirectToPlans();


                                                break;
                                            }


                                        }


                                    }
                                    else{

                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Customer not found", Snackbar.LENGTH_LONG);


                                        snackbar.show();
                                    }





                                }
                                else{

//                                        Utils.showAlertWithSingle(DTHPlansActivity.this,"No plans available",null);

                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "No plans available", Snackbar.LENGTH_LONG);


                                    snackbar.show();
                                }


                            }catch (Exception e)
                            {

                                progressFragment.dismiss();

                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Unable to fetch plans", Snackbar.LENGTH_LONG);


                                snackbar.show();
                            }







                        }
                    });



                } catch (Exception e) {


                }
            }
        });
    }

    public void retryRechargeFromHistory(RechargeHistoryData rechargeHistoryData)
    {

        if(rechargeHistoryData.getPaymentstatus().equalsIgnoreCase("5")) {
            Utils.showAlert(DTHRechargeDashboardActivity.this, "Do you want to recharge Again ?", new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                    if (rechargeHistoryData.getRefundTransactionid() != null && rechargeHistoryData.getStatus().equalsIgnoreCase("3")) {

                        if (!rechargeHistoryData.getRefundTransactionid().isEmpty()) {


                            for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                                String operator = RechargePlans.DTHPlans.arr_plans[j];

                                if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                                    spkey = RechargePlans.DTHPlans.spkeys[j];
                                    Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                                    Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                                    Selected_operator_code = RechargePlans.DTHPlans.arr_plans_opcode[j];


                                    Intent intent = new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                                    intent.putExtra("amount", rechargeHistoryData.getAmount());
                                    intent.putExtra("spkey", spkey);
                                    intent.putExtra("operator", Selected_operator);
                                    intent.putExtra("cardnumber", rechargeHistoryData.getAccountNumber());
                                    intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                                    startActivity(intent);


                                    break;
                                }


                            }

                        }

                    } else if (rechargeHistoryData.getStatus().equalsIgnoreCase("1")) {
                        for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                            String operator = RechargePlans.DTHPlans.arr_plans[j];

                            if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                                spkey = RechargePlans.DTHPlans.spkeys[j];
                                Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                                Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                                Selected_operator_code = RechargePlans.DTHPlans.arr_plans_opcode[j];

//
//                            Intent intent=new Intent(DTHRechargeDashboardActivity.this,DTHPlansActivity.class);
//                            intent.putExtra("spkey",spkey);
//                            intent.putExtra("selectedoperator",Selected_operator);
//                            intent.putExtra("Selected_operator_code",Selected_operator_code);
//                            intent.putExtra("selectedoperator_to_SERVER",Selected_operator_to_server);
//                            startActivity(intent);

                                Intent intent = new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                                intent.putExtra("amount", rechargeHistoryData.getAmount());
                                intent.putExtra("spkey", spkey);
                                intent.putExtra("operator", Selected_operator);
                                intent.putExtra("cardnumber", rechargeHistoryData.getAccountNumber());
                                intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
//        intent.putExtra("cardnumber",cardnumber);
                                startActivity(intent);


                                break;
                            }


                        }


                    }
                    else if(rechargeHistoryData.getStatus().equalsIgnoreCase("0")&& rechargeHistoryData.getRefundTransactionid() == null)
                    {


                        int min = 1;
                        int max = 10000;
                        int random = new Random().nextInt((max - min) + 1) + min;
                        String phone = rechargeHistoryData.getAccountNumber();
                        double amount1 = Double.parseDouble(rechargeHistoryData.getAmount());
                        String apireqid = random + "";
                        String customerno = rechargeHistoryData.getMobileNumber();
                        String urldata = recharge_url + "phone=" + phone + "&amount=" + amount1 + "&spkey=" + spkey + "&apireqid=" + apireqid + "&customerno=" + customerno;
                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        Handler handler = new Handler(Looper.getMainLooper());
                        executorService.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    final ProgressFragment progressFragment = new ProgressFragment();
                                    progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                    Map<String, String> params = new HashMap<>();
                                    params.put("timestamp", Utils.getTimestamp());
                                    String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                    fullString = "";
                                    URL url = new URL(fullurl);
                                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                    String line;
                                    while ((line = reader.readLine()) != null) {
                                        fullString += line;
                                    }
                                    reader.close();

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressFragment.dismiss();

                                            if (!fullString.isEmpty()) {
                                                MobileRechargeResponse mobileRechargeResponse = new GsonBuilder().create().fromJson(fullString, MobileRechargeResponse.class);

                                                String msg = mobileRechargeResponse.getMsg();
                                                int status = 0;
                                                if (mobileRechargeResponse.getStatus().equals(2)) {
                                                    status = 1;
                                                    updateStatus(rechargeHistoryData.getId(), 1, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(), mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });

                                                }
                                                if (mobileRechargeResponse.getStatus().equals(1)) {
                                                    status = 2;
                                                    updateStatus(rechargeHistoryData.getId(), 2, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(), mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, "Your recharge is in pending", new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });
                                                } else {
                                                    status = 0;
                                                    updateStatus(rechargeHistoryData.getId(), 0, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(), mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });

                                                }


                                            }


                                        }
                                    });


                                } catch (Exception e) {


                                }
                            }
                        });

                    }


                   else if (rechargeHistoryData.getRefundTransactionid() == null && rechargeHistoryData.getStatus().equalsIgnoreCase("0")) {


                        int min = 1;
                        int max = 10000;
                        int random = new Random().nextInt((max - min) + 1) + min;


                        String phone = rechargeHistoryData.getAccountNumber();
//       String amount=Amount;
                        double amount1 = Double.parseDouble(rechargeHistoryData.getAmount());
                        String apireqid = random + "";
                        String customerno = rechargeHistoryData.getMobileNumber();

                        String urldata = recharge_url + "phone=" + phone + "&amount=" + amount1 + "&spkey=" + spkey + "&apireqid=" + apireqid + "&customerno=" + customerno;

                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        Handler handler = new Handler(Looper.getMainLooper());
                        executorService.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    final ProgressFragment progressFragment = new ProgressFragment();
                                    progressFragment.show(getSupportFragmentManager(), "fkjfk");

                                    Map<String, String> params = new HashMap<>();
                                    params.put("timestamp", Utils.getTimestamp());
                                    String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();

                                    fullString = "";
                                    URL url = new URL(fullurl);
                                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                                    String line;
                                    while ((line = reader.readLine()) != null) {
                                        fullString += line;
                                    }
                                    reader.close();

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            progressFragment.dismiss();

                                            if (!fullString.isEmpty()) {
                                                MobileRechargeResponse mobileRechargeResponse = new GsonBuilder().create().fromJson(fullString, MobileRechargeResponse.class);

                                                String msg = mobileRechargeResponse.getMsg();
                                                int status = 0;
                                                if (mobileRechargeResponse.getStatus().equals(2)) {
                                                    status = 1;
                                                    updateStatus(rechargeHistoryData.getId(), 1, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });

                                                }
                                                if (mobileRechargeResponse.getStatus().equals(1)) {
                                                    status = 2;
                                                    updateStatus(rechargeHistoryData.getId(), 2, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, "Your recharge is in pending", new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });
                                                } else {
                                                    status = 0;
                                                    updateStatus(rechargeHistoryData.getId(), 0, rechargeHistoryData.getPaymentstatus(),mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid());
                                                    Utils.showAlertWithSingle(DTHRechargeDashboardActivity.this, msg, new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });

                                                }


                                            }


                                        }
                                    });


                                } catch (Exception e) {


                                }
                            }
                        });


                    }

                    else if (rechargeHistoryData.getRefundTransactionid() != null && rechargeHistoryData.getStatus().equalsIgnoreCase("0"))
                    {
                        for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                            String operator = RechargePlans.DTHPlans.arr_plans[j];

                            if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                                spkey = RechargePlans.DTHPlans.spkeys[j];
                                Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                                Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                                Selected_operator_code=RechargePlans.DTHPlans.arr_plans_opcode[j];
                                Intent intent=new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                                intent.putExtra("amount",rechargeHistoryData.getAmount());
                                intent.putExtra("spkey",spkey);
                                intent.putExtra("operator",Selected_operator);
                                intent.putExtra("cardnumber",rechargeHistoryData.getAccountNumber());
                                intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                                intent.putExtra("transaction_id",rechargeHistoryData.getId());
                                startActivity(intent);


                                break;
                            }


                        }
                    }


                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });
        }
        else{

            for (int j = 0; j < RechargePlans.DTHPlans.arr_plans.length; j++) {

                String operator = RechargePlans.DTHPlans.arr_plans[j];

                if (rechargeHistoryData.getOperator().contains(operator)) {


//                                spk1 = arrspkey[j];

                    spkey = RechargePlans.DTHPlans.spkeys[j];
                    Selected_operator_to_server = RechargePlans.DTHPlans.arr_plans_toServer[j];
                    Selected_operator = RechargePlans.DTHPlans.arr_plans[j];
                    Selected_operator_code=RechargePlans.DTHPlans.arr_plans_opcode[j];
                    Intent intent=new Intent(DTHRechargeDashboardActivity.this, DTHRechargeActivity.class);
                    intent.putExtra("amount",rechargeHistoryData.getAmount());
                    intent.putExtra("spkey",spkey);
                    intent.putExtra("operator",Selected_operator);
                    intent.putExtra("cardnumber",rechargeHistoryData.getAccountNumber());
                    intent.putExtra("mobile", rechargeHistoryData.getMobileNumber());
                    intent.putExtra("transaction_id",rechargeHistoryData.getId());
                    startActivity(intent);


                    break;
                }


            }
        }
    }


}