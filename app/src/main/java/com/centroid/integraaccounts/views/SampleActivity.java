package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;

import com.centroid.integraaccounts.data.domain.PaymentTokenData;

import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;







import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SampleActivity extends AppCompatActivity {

    Button button;


    String TAG="Transactionsts",txnTokenString="";

    int ActivityRequestCode=10;

    String orderIdString;

    String apikey="015217c0-3400-11ed-a401-434aee0a3bd9";
    String secretkey="ae096fef4b7cad57d3c7c4a1f5346da1108dcc2e";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        button=findViewById(R.id.button);

        // Service = PaytmPGService.getStagingService();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
                String date = df.format(c.getTime());
                Random rand = new Random();
                int min =1000, max= 9999;
// nextInt as provided by Random is exclusive of the top value so you need to add 1
                int randomNum = rand.nextInt((max - min) + 1) + min;
                orderIdString =  date+String.valueOf(randomNum);

                String ur="https://sandbox-icp-api.bankopen.co/api/payment_token";


                String dateorderid=Utils.getTimestamp();

                orderIdString=dateorderid;
                getProfile();




            }
        });
    }



public void getProfile()
{
    final ProgressFragment progressFragment=new ProgressFragment();
    progressFragment.show(getSupportFragmentManager(),"fkjfk");


    Map<String,String> params=new HashMap<>();
    params.put("timestamp",Utils.getTimestamp());
    // params.put("device_id",token);

    new RequestHandler(SampleActivity.this, params, new ResponseHandler() {
        @Override
        public void onSuccess(String data) {
            //progressFragment.dismiss();
            // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
            Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


            progressFragment.dismiss();
            if(profiledata!=null)
            {

                try{



                    if(profiledata.getStatus()==1)
                    {


                        if(profiledata.getData()!=null)
                        {

//
                         String   stateid=profiledata.getData().getStateId();
                            String  countryid=profiledata.getData().getCountryId();
                            String  mobile=profiledata.getData().getMobile();
                            String email=profiledata.getData().getEmailId();
                            String  name=profiledata.getData().getFullName();

                           // startActivity(new Intent(SampleActivity.this, PaymentWebViewActivity.class));

                            startActivity(new Intent(SampleActivity.this, PaymentWebViewActivity.class));


                        }





                    }
                    else {

                        Toast.makeText(SampleActivity.this," failed",Toast.LENGTH_SHORT).show();
                    }



                }catch (Exception e)
                {

                }



            }
        }

        @Override
        public void onFailure(String err) {
            progressFragment.dismiss();

            Toast.makeText(SampleActivity.this,err,Toast.LENGTH_SHORT).show();

        }
    },Utils.WebServiceMethodes.getUserDetails+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();

}



//    https://mysaving.in/IntegraAccount/openpay/response.php


}
