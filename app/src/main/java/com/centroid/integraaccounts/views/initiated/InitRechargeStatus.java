package com.centroid.integraaccounts.views.initiated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class InitRechargeStatus {




    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<InitiatedRecharge> data=new ArrayList<>();

    public InitRechargeStatus() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InitiatedRecharge> getData() {
        return data;
    }

    public void setData(List<InitiatedRecharge> data) {
        this.data = data;
    }
}
