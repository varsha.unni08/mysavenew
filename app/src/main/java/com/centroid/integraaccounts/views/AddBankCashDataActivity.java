package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.util.Calendar;

public class AddBankCashDataActivity extends AppCompatActivity {

    ImageView imgback, imgmonthyear;

    Button btnAdd;

    Spinner spPaymentAcc;

    TextView txtMonthyear,txtprofile;
    EditText edtAmount, edtBankdata;

    String monthyear = "";

    String arrmonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    CommonData commonData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_cash_data);
        getSupportActionBar().hide();

        commonData=(CommonData)getIntent().getSerializableExtra("Commondata");


        txtprofile=findViewById(R.id.txtprofile);
        imgback = findViewById(R.id.imgback);
        imgmonthyear = findViewById(R.id.imgmonthyear);
        btnAdd = findViewById(R.id.btnAdd);
        spPaymentAcc = findViewById(R.id.spPaymentAcc);
        txtMonthyear = findViewById(R.id.txtMonthyear);
        edtAmount = findViewById(R.id.edtAmount);
        edtBankdata = findViewById(R.id.edtBankdata);

        if(commonData!=null)
        {
            try{

                txtprofile.setText("Edit cash / Bank");

                JSONObject jsonObject=new JSONObject(commonData.getData());
                String Type=jsonObject.getString("Type");
                String month_year=jsonObject.getString("monthyear");

                String bankdata=jsonObject.getString("bankdata");
                String amount=jsonObject.getString("amount");

                monthyear=month_year;
                edtBankdata.setText(bankdata);
                edtAmount.setText(amount);

                String data[]=monthyear.split("/");
                int monthindex=Integer.parseInt(data[0]);

                txtMonthyear.setText(arrmonth[monthindex]+"/"+data[1]);

                String arr[]=this.getResources().getStringArray(R.array.paymentacc);

                for (int i=0;i<arr.length;i++)
                {
                    if(arr[i].equalsIgnoreCase(Type))
                    {
                        spPaymentAcc.setSelection(i);
                        break;
                    }
                }

                btnAdd.setText("Update");





            }catch (Exception e)
            {

            }


        }



        txtMonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthYear();
            }
        });

        imgmonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthYear();
            }
        });


        spPaymentAcc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (spPaymentAcc.getSelectedItem().toString().equalsIgnoreCase("Cash")) {

                    edtBankdata.setVisibility(View.GONE);
                } else if (spPaymentAcc.getSelectedItem().toString().equalsIgnoreCase("Bank")) {

                    edtBankdata.setVisibility(View.VISIBLE);

                } else {

                    edtBankdata.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!spPaymentAcc.getSelectedItem().toString().equalsIgnoreCase("Select payment account")) {
                    if (!monthyear.equalsIgnoreCase("")) {

                        if (spPaymentAcc.getSelectedItem().toString().equalsIgnoreCase("Bank")) {





                            if (!edtBankdata.getText().toString().equalsIgnoreCase("")) {

                                if (!edtAmount.getText().toString().equalsIgnoreCase("")) {


                                    //skdnfjsdwn

                                    try{

                                        JSONObject jsonObject=new JSONObject();
                                        jsonObject.put("Type",spPaymentAcc.getSelectedItem().toString());
                                        jsonObject.put("monthyear",monthyear);
                                        jsonObject.put("bankdata",edtBankdata.getText().toString());
                                        jsonObject.put("amount",edtAmount.getText().toString());

                                        uploadData(jsonObject.toString());


                                    }
                                    catch (Exception e)
                                    {

                                    }





                                } else {

                                    Toast.makeText(AddBankCashDataActivity.this, "Enter amount", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                Toast.makeText(AddBankCashDataActivity.this, "Enter bank data", Toast.LENGTH_SHORT).show();
                            }


                        } else {

                            //Toast.makeText(AddBankCashDataActivity.this, "Enter bank data", Toast.LENGTH_SHORT).show();


                            if (!edtAmount.getText().toString().equalsIgnoreCase("")) {


                                //sidfkjdnsw

                                try{

                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("Type",spPaymentAcc.getSelectedItem().toString());
                                    jsonObject.put("monthyear",monthyear);
                                    jsonObject.put("bankdata","");
                                    jsonObject.put("amount",edtAmount.getText().toString());

                                    uploadData(jsonObject.toString());

                                }
                                catch (Exception e)
                                {

                                }





                            } else {

                                Toast.makeText(AddBankCashDataActivity.this, "Enter amount", Toast.LENGTH_SHORT).show();
                            }




                        }










                    } else {

                        Toast.makeText(AddBankCashDataActivity.this, "Select month and year", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(AddBankCashDataActivity.this, "Select payment account", Toast.LENGTH_SHORT).show();
                }


            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void showMonthYear() {

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels / 2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(AddBankCashDataActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set = dialog.findViewById(R.id.date_time_set);


        final NumberPicker npmonth = dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);

        npmonth.setDisplayedValues(arrmonth);


        final NumberPicker npyear = dialog.findViewById(R.id.npyear);

        Calendar calendar1=Calendar.getInstance();
        int year1=calendar1.get(Calendar.YEAR);

        npyear.setMinValue(1990);
        npyear.setMaxValue(year1);




        dialog.getWindow().setLayout(width, height);


        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                monthyear = npmonth.getValue() + "/" + npyear.getValue();

                txtMonthyear.setText(arrmonth[npmonth.getValue()] + "/" +npyear.getValue());


            }
        });


        dialog.show();

    }


    public void uploadData(String data)
    {

        if(commonData!=null)
        {

            new DatabaseHelper(AddBankCashDataActivity.this).updateCashBankData(commonData.getId(),data);
            Utils.playSimpleTone(AddBankCashDataActivity.this);

        }
        else {

            new DatabaseHelper(AddBankCashDataActivity.this).addCashbankData(data);
            Utils.playSimpleTone(AddBankCashDataActivity.this);


        }

        spPaymentAcc.setSelection(0);
        edtAmount.setText("");
        edtBankdata.setText("");
        txtMonthyear.setText("Select Month and year");
        monthyear="";



    }
}
