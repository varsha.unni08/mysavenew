package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.CountrySpinnerAdapter;
import com.centroid.integraaccounts.adapter.StateListAdapter;
import com.centroid.integraaccounts.data.domain.BillAmount;
import com.centroid.integraaccounts.data.domain.BillData;
import com.centroid.integraaccounts.data.domain.CountryData;
import com.centroid.integraaccounts.data.domain.CountryList;
import com.centroid.integraaccounts.data.domain.Profile;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.State;
import com.centroid.integraaccounts.data.domain.StateData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class ProfileActivity extends AppCompatActivity {

    ImageView imgback,profile,imgedit,imgbill;

    EditText edtname,edtEmail;

    TextView edtMobile,txtph,txtprofile;

    Button update;

    Spinner spLanguage,spcountry;

    int PICK_IMAGE=122;

    Spinner spState;

    String mobile="",country="",name="",email="",id="";

    String stateid="0",countryid="0";
    String result = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        profile=findViewById(R.id.profile);
        imgedit=findViewById(R.id.imgedit);
        imgbill=findViewById(R.id.imgbill);


        edtname=findViewById(R.id.edtname);
        edtMobile=findViewById(R.id.edtMobile);
        spState=findViewById(R.id.spState);
        spcountry=findViewById(R.id.spcountry);

        txtph=findViewById(R.id.txtph);
        edtEmail=findViewById(R.id.edtEmail);
        txtprofile=findViewById(R.id.txtprofile);
    //    txtvisitlogin=findViewById(R.id.txtvisitlogin);

        update=findViewById(R.id.update);
        spLanguage=findViewById(R.id.spLanguage);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        imgbill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

getBillData();

            }
        });

        String languagedata= LocaleHelper.getPersistedData(ProfileActivity.this,"en");

        setupLanguages(languagedata);
        Context context= LocaleHelper.setLocale(ProfileActivity.this,languagedata);
        Resources resources=context.getResources();
        updateUI(resources);

        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                State state=(State) spState.getSelectedItem();
                stateid=state.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String item=spLanguage.getSelectedItem().toString();

                String languagedata=LocaleHelper.getPersistedData(ProfileActivity.this,"en");

                if(item.equals("తెలుగు"))
                {
                    languagedata="te";
                }
               else if(item.equals("English"))
                {
                    languagedata="en";
                }

               else if(item.equals("मराठी"))
                {
                    languagedata="mr";
                }
               else if(item.equals("தமிழ்"))
                {
                    languagedata="ta";
                }

             else   if(item.equals("ಕನ್ನಡ"))
                {
                    languagedata="kn";
                }

               else if(item.equals("हिंदी"))
                {
                    languagedata="hi";
                }


               else if(item.equals("മലയാളം"))
                {
                    languagedata="ml";
                }

               else
                {
                    languagedata="ar";
                }

               Context context= LocaleHelper.setLocale(ProfileActivity.this,languagedata);

                Resources resources=context.getResources();
                updateUI(resources);



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(!edtname.getText().toString().equals(""))
                {

                    if(Utils.emailValidator(edtEmail.getText().toString()))
                    {



                        CountryData countryData=(CountryData)spcountry.getSelectedItem();

                        if(!countryData.getId().equalsIgnoreCase("1"))
                        {
                            stateid="0";
                        }

                        String lan=spLanguage.getSelectedItem().toString();

                        if(!lan.equalsIgnoreCase("Select your language"))
                        {

                            final ProgressFragment progressFragment=new ProgressFragment();
                            progressFragment.show(getSupportFragmentManager(),"fkjfk");


                            Map<String,String> params=new HashMap<>();
                            params.put("name",edtname.getText().toString());
                            params.put("user_email",edtEmail.getText().toString());
                            params.put("stateid",stateid);
                            params.put("language",lan);
                            params.put("country_id",countryData.getId());
                            params.put("timestamp",Utils.getTimestamp());

                            new RequestHandler(ProfileActivity.this, params, new ResponseHandler() {
                                @Override
                                public void onSuccess(String data) {
                                    progressFragment.dismiss();
                                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                                    if(data!=null)
                                    {

                                        try {

                                            JSONObject jsonObject = new JSONObject(data);

                                            if (jsonObject.getInt("status") == 1) {

                                                // getProfileData();

                                               // Toast.makeText(ProfileActivity.this,"Profile uploaded successfully",Toast.LENGTH_SHORT).show();
                                                Utils.showAlertWithSingle(ProfileActivity.this,"Profile uploaded successfully", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });

                                            } else {

                                             //   Toast.makeText(ProfileActivity.this,"failed",Toast.LENGTH_SHORT).show();


                                                Utils.showAlertWithSingle(ProfileActivity.this,"failed", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });


                                            }

                                        }catch ( Exception e)
                                        {

                                        }

                                    }

                                }

                                @Override
                                public void onFailure(String err) {
                                     progressFragment.dismiss();

                                   // Toast.makeText(ProfileActivity.this,err,Toast.LENGTH_SHORT).show();
                                    Utils.showAlertWithSingle(ProfileActivity.this,err, new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                }
                            },Utils.WebServiceMethodes.UserProfileUpdate, Request.Method.POST).submitRequest();








                        }
                        else {

                        //    Toast.makeText(ProfileActivity.this,"Select language",Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(ProfileActivity.this,"Select language", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }











                    }
                    else {

                      //  Toast.makeText(ProfileActivity.this,"Enter a valid email id",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(ProfileActivity.this,"Enter a valid email id", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }

                }
                else {

                    Utils.showAlertWithSingle(ProfileActivity.this,"Enter name", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                    // Toast.makeText(ProfileActivity.this,"Enter name",Toast.LENGTH_SHORT).show();
                }

            }
        });

        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(ContextCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {

                    ActivityCompat.requestPermissions(ProfileActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},111);

                }
                else {

                    pickImage();

                }

            }
        });

       // getCountry();


        getProfileData();


        spcountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                getState();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    public void getCountry()
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
//        params.put("uuid",uuid);
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(ProfileActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {


                    CountryList countryList=new GsonBuilder().create().fromJson(data,CountryList.class);
                    if(countryList.getStatus()==1)
                    {


                        if(countryList.getData().size()>0)
                        {

                            Collections.sort(countryList.getData(), new Comparator<CountryData>() {
                                @Override
                                public int compare(CountryData countryData, CountryData t1) {
                                    return countryData.getCountryName().compareToIgnoreCase(t1.getCountryName());
                                }
                            });


                            CountrySpinnerAdapter countrySpinnerAdapter=new CountrySpinnerAdapter(ProfileActivity.this,countryList.getData());
                            spcountry.setAdapter(countrySpinnerAdapter);


                            for (int i=0;i<countryList.getData().size();i++)
                            {

                                if(countryList.getData().get(i).getId().equalsIgnoreCase(countryid))
                                {

                                    spcountry.setSelection(i);
                                    break;
                                }


                            }


                        }


                    }
                    else {

//                        Toast.makeText(ProfileActivity.this,"No Data Found",Toast.LENGTH_SHORT).
//                                show();

                        Utils.showAlertWithSingle(ProfileActivity.this,"No Data Found", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }


                    // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

           //     Toast.makeText(ProfileActivity.this,err,Toast.LENGTH_SHORT).show();
                Utils.showAlertWithSingle(ProfileActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.getCountry+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();




//
//
//        Call<CountryList>countryListCall=client.getCountry();
//
//        countryListCall.enqueue(new Callback<CountryList>() {
//            @Override
//            public void onResponse(Call<CountryList> call, Response<CountryList> response) {
//                progressFragment.dismiss();
//
//                if (response.body() != null) {
//
//                    try {
//
//                        CountryList countryList=response.body();
//                        if(countryList.getStatus()==1)
//                        {
//
//
//                            if(countryList.getData().size()>0)
//                            {
//
//                                Collections.sort(countryList.getData(), new Comparator<CountryData>() {
//                                    @Override
//                                    public int compare(CountryData countryData, CountryData t1) {
//                                        return countryData.getCountryName().compareToIgnoreCase(t1.getCountryName());
//                                    }
//                                });
//
//
//                                CountrySpinnerAdapter countrySpinnerAdapter=new CountrySpinnerAdapter(ProfileActivity.this,countryList.getData());
//                                spcountry.setAdapter(countrySpinnerAdapter);
//
//
//                                for (int i=0;i<countryList.getData().size();i++)
//                                {
//
//                                    if(countryList.getData().get(i).getId().equalsIgnoreCase(countryid))
//                                    {
//
//                                        spcountry.setSelection(i);
//                                        break;
//                                    }
//
//
//                                }
//
//
//                            }
//
//
//                        }
//                        else {
//
//                            Toast.makeText(ProfileActivity.this,"No Data Found",Toast.LENGTH_SHORT).
//                                    show();
//
//                        }
//
//
//
//
//
//
//                    } catch (Exception e) {
//
//                    }
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CountryList> call, Throwable t) {
//
//                progressFragment.dismiss();
//
//            }
//        });
    }



    public void getState()
    {




        CountryData cid=(CountryData)spcountry.getSelectedItem();


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");



        Map<String,String> params=new HashMap<>();
        //params.put("countryid",countryid+"");
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(ProfileActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {



                    StateData stateData=new GsonBuilder().create().fromJson(data,StateData.class);

                    //StateData stateData=response.body();

                    if(stateData.getStatus()==1)
                    {
                        spState.setVisibility(View.VISIBLE);

                        Collections.sort(stateData.getData(), new Comparator<State>() {
                            @Override
                            public int compare(State state, State t1) {
                                int a=0;






                                return state.getStateName().compareTo(t1.getStateName());
                            }
                        });









                        // Toast.makeText(RegistrationActivity.this,"Your mobile number does not already exists",Toast.LENGTH_SHORT).show();

                        spState.setAdapter(new StateListAdapter(ProfileActivity.this,stateData.getData()));



                        if(stateData.getData().size()>0)
                        {


                            List<State>states=stateData.getData();

                            for (int i=0;i<states.size();i++)
                            {
                                if(stateid.equalsIgnoreCase(states.get(i).getId()))
                                {


                                    spState.setSelection(i);

                                    break;
                                }
                            }



                        }

//                                Intent i=  new Intent(RegistrationActivity.this,OTPActivity.class);
//
//                                i.putExtra("Data",usr);
//
//
//                                startActivity(i);



                    }
                    else {
                        stateid="0";

                        spState.setVisibility(View.GONE);

                        //  Toast.makeText(RegistrationActivity.this,"",Toast.LENGTH_SHORT).show();
                    }



                    // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(ProfileActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getState+"?countryid="+countryid+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();






//
//
//        Call<StateData> jsonObjectCall=client.getState(cid.getId()
//
//        );
//        jsonObjectCall.enqueue(new Callback<StateData>() {
//            @Override
//            public void onResponse(Call<StateData> call, Response<StateData> response) {
//
//               // progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
////                            JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        StateData stateData=response.body();
//
//                        if(stateData.getStatus()==1)
//                        {
//                            spState.setVisibility(View.VISIBLE);
//
//                            Collections.sort(stateData.getData(), new Comparator<State>() {
//                                @Override
//                                public int compare(State state, State t1) {
//                                    int a=0;
//
//
//
//
//
//
//                                    return state.getStateName().compareTo(t1.getStateName());
//                                }
//                            });
//
//
//
//
//
//
//
//
//
//                            // Toast.makeText(RegistrationActivity.this,"Your mobile number does not already exists",Toast.LENGTH_SHORT).show();
//
//                            spState.setAdapter(new StateListAdapter(ProfileActivity.this,stateData.getData()));
//
//
//
//                            if(stateData.getData().size()>0)
//                            {
//
//
//                                List<State>states=stateData.getData();
//
//                                for (int i=0;i<states.size();i++)
//                                {
//                                    if(stateid.equalsIgnoreCase(states.get(i).getId()))
//                                    {
//
//
//                                        spState.setSelection(i);
//
//                                        break;
//                                    }
//                                }
//
//
//
//                            }
//
////                                Intent i=  new Intent(RegistrationActivity.this,OTPActivity.class);
////
////                                i.putExtra("Data",usr);
////
////
////                                startActivity(i);
//
//
//
//                        }
//                        else {
//                            stateid="0";
//
//                            spState.setVisibility(View.GONE);
//
//                            //  Toast.makeText(RegistrationActivity.this,"",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<StateData> call, Throwable t) {
//
//               // progressFragment.dismiss();
//            }
//        });


    }


    public void updateUI(Resources resources)
    {

        edtname.setHint(resources.getText(R.string.name));

        edtEmail.setHint(resources.getString(R.string.email));

        txtph.setText(resources.getString(R.string.phonenumber));

        update.setText(resources.getString(R.string.update));

        txtprofile.setText(resources.getString(R.string.profile));

     //   txtvisitlogin.setText(resources.getString(R.string.visitlogin));

       // spLanguage.setAdapter(new ArrayAdapter<String>(ProfileActivity.this,android.R.layout.simple_spinner_item,resources.getStringArray(R.array.languages)));
    }




public void setupLanguages(String lan)
{



    if(lan.equals("te"))
    {
        //languagedata="te";

        spLanguage.setSelection(6);
    }
   else if(lan.equals("en"))
    {
        //languagedata="en";

        spLanguage.setSelection(2);
    }

    else  if(lan.equals("mr"))
    {
        //languagedata="mr";
        spLanguage.setSelection(7);
    }
    else if(lan.equals("ta"))
    {
       // languagedata="ta";
        spLanguage.setSelection(4);
    }

    else if(lan.equals("kn"))
    {
        //languagedata="kn";
        spLanguage.setSelection(5);
    }

    else if(lan.equals("hi"))
    {
       // languagedata="hi";
        spLanguage.setSelection(1);
    }

    else  if(lan.equals("en"))
    {
        //languagedata="en";
        spLanguage.setSelection(2);
    }

    else  if(lan.equals("ml"))
    {
        //languagedata="ml";
        spLanguage.setSelection(3);
    }
    else{
        spLanguage.setSelection(8);
    }


   // spLanguage

}





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        pickImage();
    }

    public void pickImage()
    {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode==PICK_IMAGE&&resultCode==RESULT_OK&&data!=null)
        {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

           final Dialog d=new Dialog(ProfileActivity.this);
            d.setContentView(R.layout.layout_cropdialog);



            ImageView imgback=d.findViewById(R.id.imgback);

            ImageView imgcrop=d.findViewById(R.id.imgcrop);

            final CropImageView cropImageView=d.findViewById(R.id.cropImageView);

            cropImageView.setImageUriAsync(data.getData());

            imgback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    d.dismiss();
                }
            });

            imgcrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    d.dismiss();

                    try {
                        Bitmap cropped = cropImageView.getCroppedImage();

                      final   File f = new File(Utils.getTempstorageFilePath(ProfileActivity.this));

                        if (!f.exists()) {

                            f.createNewFile();
                        }
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        cropped.compress(Bitmap.CompressFormat.PNG, 60, bytes);

                        FileOutputStream fo =new  FileOutputStream(f);
                        fo.write(bytes.toByteArray());
                        fo.close();




                        AlertDialog.Builder builder=new AlertDialog.Builder(ProfileActivity.this);
                        builder.setMessage("Do you want to set as profile image ? ");
                        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                                uploadImage(f,cropped);
                            }
                        });
                        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();

                            }
                        });

                        builder.show();






                    }catch (Exception e)
                    {

                    }



                }
            });

            d.getWindow().setLayout(width,height);
            d.show();




        }


//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            final CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//
//
//                Glide.with(ProfileActivity.this).load(result.getUri()).apply(RequestOptions.circleCropTransform()).into(profile);
//
//
//
//
//                //Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
//            }
//        }


    }



    public void uploadImage( File file,Bitmap bmp)
    {

//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");
//
//
//        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
//
//        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);
//
//
//
//        Call<JsonObject> jsonArrayCall=client.uploadUserProfile(new PreferenceHelper(ProfileActivity.this).getData(Utils.userkey),part);
//
//
//        jsonArrayCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//
//                if(response.body()!=null)
//                {
//
//                    try {
//
//                        JSONObject jsonObject = new JSONObject(response.body().toString());
//
//                        if (jsonObject.getInt("status") == 1) {
//
//                            getProfileData();
//
//                            Toast.makeText(ProfileActivity.this,"Profile image uploaded successfully",Toast.LENGTH_SHORT).show();
//
//
//                        } else {
//
//                            Toast.makeText(ProfileActivity.this,"failed",Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }catch ( Exception e)
//                    {
//
//                    }
//
//                }
//
//
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//
//            }
//        });



        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");

//        RequestBody fileReqBody = RequestBody.create(MediaType.parse("application/json"), file);
//
//        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {

                try {

                    String attachmentName = "file";
                    String attachmentFileName = file.getName();
                    String crlf = "\r\n";
                    String twoHyphens = "--";
                    String boundary =  "*****";

                    HttpURLConnection httpUrlConnection = null;
                    URL url = new URL(Utils.baseurl+Utils.WebServiceMethodes.uploadUserProfile);
                    httpUrlConnection = (HttpURLConnection) url.openConnection();
                    httpUrlConnection.setUseCaches(false);
                    httpUrlConnection.setDoOutput(true);

                    httpUrlConnection.setRequestMethod("POST");
                    httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
                    httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
                    httpUrlConnection.setRequestProperty("timestamp", Utils.getTimestamp());
                    httpUrlConnection.setRequestProperty("Authorization", new PreferenceHelper(ProfileActivity.this).getData(Utils.userkey));
                    httpUrlConnection.setRequestProperty(
                            "Content-Type", "multipart/form-data;boundary=" + boundary);

                    DataOutputStream request = new DataOutputStream(
                            httpUrlConnection.getOutputStream());

                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"" +
                            attachmentName + "\";filename=\"" +
                            attachmentFileName + "\"" + crlf);
                    request.writeBytes(crlf);



                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(file);
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        byte[] b = new byte[1024];
                        for (int readNum; (readNum = fis.read(b)) != -1; ) {
                            bos.write(b, 0, readNum);
                        }
                        //return bos.toByteArray();

                        request.write(bos.toByteArray());
                    } catch (Exception e) {
                        Log.d("mylog", e.toString());
                    }

                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary +
                            twoHyphens + crlf);

                    request.flush();
                    request.close();

                    InputStream responseStream = new
                            BufferedInputStream(httpUrlConnection.getInputStream());

                    BufferedReader responseStreamReader =
                            new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    responseStreamReader.close();

                     result = stringBuilder.toString();

                    responseStream.close();

                    httpUrlConnection.disconnect();

                }catch (Exception e)
                {

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                       // Toast.makeText(ProfileActivity.this,result,Toast.LENGTH_SHORT).show();

                        if(result!=null)
                        {
                            try {

                                JSONObject jsonObject = new JSONObject(result);

                                if (jsonObject.getInt("status") == 1) {

                                    getProfileData();

                                    Toast.makeText(ProfileActivity.this,"Profile image uploaded successfully",Toast.LENGTH_SHORT).show();


                                } else {

                                    Toast.makeText(ProfileActivity.this,"failed",Toast.LENGTH_SHORT).show();

                                }

                            }catch ( Exception e)
                            {

                            }
                        }

                        progressFragment.dismiss();
                    }
                });


            }
        });


    }



    public void getBillData()
    {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(ProfileActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
             //   Toast.makeText(ProfileActivity.this,data,Toast.LENGTH_SHORT).show();
               try{


                   BillAmount billAmount=new GsonBuilder().create().fromJson(data,BillAmount.class);

                   if(billAmount.getStatus()==1)
                   {

                       BillData billData=billAmount.getData();

                       Intent intent=new Intent(ProfileActivity.this, InvoiceActivity.class);
                       intent.putExtra("settingsdata",billAmount.getSettingsdata());
                       intent.putExtra("bill",billData.getDisplay_bill_no());
                       intent.putExtra("currency",billData.getCurrency());
                       intent.putExtra("actualamount",billData.getAmt());
                       intent.putExtra("countryid",countryid);
                       intent.putExtra("stateid",stateid);
                       intent.putExtra("email",email);
                       intent.putExtra("name",name);

                       intent.putExtra("amount",billAmount);




                       intent.putExtra("tid",billData.getCashTransactionId());
                     startActivity(intent);











                   }
                   else{

                       Toast.makeText(ProfileActivity.this," Sorry you have to purchase the app first ",Toast.LENGTH_SHORT).show();

                   }









                    }catch (Exception e)
                    {

                    }




            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(ProfileActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getbill+"?timestamp="+Utils.getTimestamp()+"&regid="+id, Request.Method.GET).submitRequest();



    }







    public void getProfileData()
    {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(ProfileActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {
                                id=profiledata.getData().getId();

                                edtEmail.setText(profiledata.getData().getEmailId());
                                edtname.setText(profiledata.getData().getFullName());
                                edtMobile.setText(""+profiledata.getData().getMobile());

                                stateid=profiledata.getData().getStateId();
                                countryid=profiledata.getData().getCountryId();
                                mobile=profiledata.getData().getMobile();
                                email=profiledata.getData().getEmailId();
                                name=profiledata.getData().getFullName();
                                getCountry();

                                getState();


                                if(!TextUtils.isEmpty(profiledata.getData().getProfileImage()))
                                {
                                    Glide.with(ProfileActivity.this).load(Utils.imgbaseurl+profiledata.getData().getProfileImage()).apply(RequestOptions.circleCropTransform()).into(profile);

                                }

                            }





                        }
                        else {

                            Toast.makeText(ProfileActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                 progressFragment.dismiss();

                Toast.makeText(ProfileActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();


//
//
//        Call<Profiledata> jsonObjectCall=client.getUserProfile(new PreferenceHelper(ProfileActivity.this).getData(Utils.userkey)
//
//        );
//        jsonObjectCall.enqueue(new Callback<Profiledata>() {
//            @Override
//            public void onResponse(Call<Profiledata> call, Response<Profiledata> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//
//                            if(response.body().getData()!=null)
//                            {
//
//                                edtEmail.setText(response.body().getData().getEmailId());
//                                edtname.setText(response.body().getData().getFullName());
//                                edtMobile.setText(""+response.body().getData().getMobile());
//
//                                stateid=response.body().getData().getStateId();
//                                countryid=response.body().getData().getCountryId();
//                                getCountry();
//
//                                getState();
//
//
//                                if(!TextUtils.isEmpty(response.body().getData().getProfileImage()))
//                                {
//                                    Glide.with(ProfileActivity.this).load(Utils.imgbaseurl+response.body().getData().getProfileImage()).apply(RequestOptions.circleCropTransform()).into(profile);
//
//                                }
//
//                            }
//
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(ProfileActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Profiledata> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });
    }
}
