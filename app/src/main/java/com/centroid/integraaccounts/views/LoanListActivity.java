package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.LoanAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.List;

public class LoanListActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    ImageView imgback;

    TextView txtTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_list);
        getSupportActionBar().hide();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        txtTotal=findViewById(R.id.txtTotal);



getLoanData();






        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

                double h=displayMetrics.heightPixels/1.2;
                int height = (int)h;
                int width = displayMetrics.widthPixels;

                final Dialog dialog = new Dialog(LoanListActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_addloan);

                final EditText edtloanname=dialog.findViewById(R.id.edtloanname);

                final EditText edtloanamount=dialog.findViewById(R.id.edtloanamount);

                Button btnAdd=dialog.findViewById(R.id.btnAdd);

                dialog.getWindow().setLayout(width,height);

                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!edtloanname.getText().toString().equalsIgnoreCase(""))
                        {

                            if(!edtloanamount.getText().toString().equalsIgnoreCase(""))
                            {

                                dialog.dismiss();


                                try{

                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("name",edtloanname.getText().toString());
                                    jsonObject.put("amount",edtloanamount.getText().toString());

                                    new DatabaseHelper(LoanListActivity.this).addloanData(jsonObject.toString());


                                    getLoanData();

                                }catch (Exception e)
                                {

                                }




                            }
                            else {

//                                Toast.makeText(LoanListActivity.this,"Enter loan amount",Toast.LENGTH_SHORT).show();

                                Utils.showAlertWithSingle(LoanListActivity.this, "Enter loan amount", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

                            }


                        }
                        else {


                          //  Toast.makeText(LoanListActivity.this,"Enter loan name",Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(LoanListActivity.this, "Enter loan name", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }



                    }
                });



                dialog.show();




                //


               // startActivity(new Intent(LiabilitiesActivity.this, AddLiabilitiesActivity.class));







            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getLoanData();
    }


    public void getLoanData()
    {

        List<CommonData>commonData=new DatabaseHelper(LoanListActivity.this).getLoanData();

        if(commonData.size()>0)
        {

            LoanAdapter loanAdapter=new LoanAdapter(LoanListActivity.this,commonData);
            recycler.setLayoutManager(new LinearLayoutManager(LoanListActivity.this));
            recycler.setAdapter(loanAdapter);

            double total=0;


            for (CommonData cm:commonData)
            {

                try{

                    JSONObject jsonObject=new JSONObject(cm.getData());


                    double amount=Double.parseDouble(jsonObject.getString("amount"));
                    total=total+amount;


                }catch (Exception e)
                {


                }


            }

            txtTotal.setText("Total : "+total+" Rs");


        }
        else {

            txtTotal.setText("Total : 0"+" Rs");
        }


    }
}
