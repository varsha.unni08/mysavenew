package com.centroid.integraaccounts.views.initiated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitiatedRecharge {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("recharge_amount")
    @Expose
    private String rechargeAmount;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("init_date")
    @Expose
    private String initDate;
    @SerializedName("operator_circle")
    @Expose
    private String operatorCircle;
    @SerializedName("operator_code")
    @Expose
    private String operatorCode;
    @SerializedName("recharge_type")
    @Expose
    private String rechargeType;
    @SerializedName("Payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("refund_date")
    @Expose
    private Object refundDate;
    @SerializedName("refund_transaction_id")
    @Expose
    private String refundTransactionId;
    @SerializedName("recharge_status")
    @Expose
    private String recharge_status;

    public InitiatedRecharge() {
    }

    public String getRecharge_status() {
        return recharge_status;
    }

    public void setRecharge_status(String recharge_status) {
        this.recharge_status = recharge_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }

    public String getOperatorCircle() {
        return operatorCircle;
    }

    public void setOperatorCircle(String operatorCircle) {
        this.operatorCircle = operatorCircle;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Object getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(Object refundDate) {
        this.refundDate = refundDate;
    }

    public String getRefundTransactionId() {
        return refundTransactionId;
    }

    public void setRefundTransactionId(String refundTransactionId) {
        this.refundTransactionId = refundTransactionId;
    }
}
