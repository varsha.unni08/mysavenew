package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.data.domain.MileStoneTarget;
import com.centroid.integraaccounts.dialogs.CalculatorDialog;
import com.centroid.integraaccounts.dialogs.MileStoneListFragment;
import com.centroid.integraaccounts.interfaces.CalculatorService;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.google.api.client.util.Data;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TargetDetailsActivity extends AppCompatActivity {

    ImageView imgback,imgedit,imgicon;

    TextView txtTargetName,txtTargetCategory,txtTargetdate,txtsavedamount,txtpercent,txtTargetAmount,txtNote,txtInvestmentName,txtClosingbalance,totalAddedAmount;
    ProgressBar progressBar;

    CommonData data;

    Button btnAddAmount,btntargetReached,btntGoalReached;

    double savedamount=0,savedamount_already=0,targetamount_already=0;

    LinearLayout layout_nodata,layout_targetdetails,layout_investmentaccount,layout_closingbalance;
    double closingbalance=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target_details);
        getSupportActionBar().hide();

        data= (CommonData)getIntent().getSerializableExtra("data");

        layout_targetdetails=findViewById(R.id.layout_targetdetails);
        layout_nodata=findViewById(R.id.layout_nodata);
        imgback=findViewById(R.id.imgback);
        imgedit=findViewById(R.id.imgedit);
        txtTargetdate=findViewById(R.id.txtTargetdate);
        txtTargetCategory=findViewById(R.id.txtTargetCategory);
        txtsavedamount=findViewById(R.id.txtsavedamount);
        imgicon=findViewById(R.id.imgicon);
        txtpercent=findViewById(R.id.txtpercent);
        progressBar=findViewById(R.id.progressBar);
        txtTargetName=findViewById(R.id.txtTargetName);

        btnAddAmount=findViewById(R.id.btnAddAmount);
        imgedit=findViewById(R.id.imgedit);
        txtTargetAmount=findViewById(R.id.txtTargetAmount);
        btntargetReached=findViewById(R.id.btntargetReached);
        txtNote=findViewById(R.id.txtNote);
        txtInvestmentName=findViewById(R.id.txtInvestmentName);
        txtClosingbalance=findViewById(R.id.txtClosingbalance);
        totalAddedAmount=findViewById(R.id.totalAddedAmount);

        layout_investmentaccount=findViewById(R.id.layout_investmentaccount);
        layout_closingbalance=findViewById(R.id.layout_closingbalance);
        btntGoalReached=findViewById(R.id.btntGoalReached);

        btntGoalReached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    if(data!=null) {

                        JSONObject js = new JSONObject(data.getData());

                        if(js.has("goalreached"))
                        {

                        }
                        else {


                            js.put("goalreached", "1");

                            new DatabaseHelper(TargetDetailsActivity.this).updateData(data.getId(), js.toString(), Utils.DBtables.TABLE_TARGET);
onBackPressed();
                        }
                    }



                }catch (Exception e)
                {


                }



            }
        });



        showTargetDetails();


            imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btnAddAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CalculatorDialog dialogFragment=new CalculatorDialog(new CalculatorService() {
                    @Override
                    public void setAmount(double amount) {

                        DecimalFormat df = new DecimalFormat("0.00");

                        String a=  df.format(amount);

                      //  savedamount=Double.parseDouble(a);

                       // savedamount=savedamount_already+savedamount;


                      //  showTargetDetails();




                        try{

                            Calendar c=Calendar.getInstance();
                            int y=c.get(Calendar.YEAR);
                            int m=c.get(Calendar.MONTH);
                            int d=c.get(Calendar.DAY_OF_MONTH);

                            int m1=m+1;

                            String date=d+"-"+m1+"-"+y;

                            JSONObject jsn=new JSONObject();
                            jsn.put("date",date);
                            jsn.put("savedamount",a);
                            jsn.put("targetid",data.getId());

                            new DatabaseHelper(TargetDetailsActivity.this).addData( Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE,jsn.toString());

                            showTargetDetails();

//                            List<CommonData>cmd=new DatabaseHelper(TargetDetailsActivity.this).getData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);
//
//                            for(CommonData cm:cmd)
//                            {
//
//                                JSONObject j=new JSONObject(cm.getData());
//                                String am=j.getString("savedamount");
//                                String tid=j.getString("targetid");
//
//                                if(tid.equalsIgnoreCase(data.getId())) {
//
//                                    savedamount = Double.parseDouble(am);
//
//                                    savedamount=savedamount_already+savedamount;
//
//                                }
//
//
//
//                            }
//
//                            txtsavedamount.setText("Saved amount : "+savedamount + "");
//
//
//                            double ta=targetamount_already;
//
//                            double p=savedamount/ta;
//
//                            double percentage=p*100;
//                            DecimalFormat df1 = new DecimalFormat("0.00");
//
//                            String a1=  df1.format(percentage);
//
//                            int per=(int)percentage;
//
//                            progressBar.setProgress(per);
//
//
//                            txtpercent.setText(a1+" % \n "+savedamount +" / "+targetamount_already);








                        }catch (Exception e)
                        {

                        }




                    }
                });
                dialogFragment.show(getSupportFragmentManager(),"My  Fragment");



            }
        });


        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i=new Intent(TargetDetailsActivity.this, NewTargetActivity.class);
                i.putExtra("data",data.getId());
                startActivity(i);

            }
        });

        btntargetReached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i=new Intent(TargetDetailsActivity.this, MileStoneEditActivity.class);
                i.putExtra("data",data.getId());

                startActivity(i);



            }
        });



    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showTargetDetails();
    }

    public void showTargetDetails()
    {
        try {

          //  txtInvestmentName



            List<CommonData> cmd=new DatabaseHelper(TargetDetailsActivity.this).getDataByID(data.getId(), Utils.DBtables.TABLE_TARGET);


            if(cmd!=null) {

                if (cmd.size() > 0) {

                    layout_nodata.setVisibility(View.GONE);
                    layout_targetdetails.setVisibility(View.VISIBLE);
                    imgedit.setVisibility(View.VISIBLE);

                    data = cmd.get(0);


                    JSONObject js = new JSONObject(data.getData());

                    if(   js.has("goalreached"))
                    {
                        String goalreached=js.getString("goalreached");

                        if(goalreached.equalsIgnoreCase("1"))
                        {
                            btntargetReached.setVisibility(View.GONE);
                            btnAddAmount.setVisibility(View.GONE);
                            btntGoalReached.setText("Goal Reached");
                            imgedit.setVisibility(View.GONE);

                        }

                    }

                    String targetname=js.getString("targetname");
                    String targetamount = js.getString("targetamount");
                    String savedamount = js.getString("savedamount");
                    String target_date = js.getString("target_date");
                    String note = js.getString("note");

                    String target_categoryid = js.getString("target_categoryid");
                    txtTargetName.setText(targetname);

                    if(js.has("investment_id")) {

                        String investment_id = js.getString("investment_id");

//                        List<CommonData> cmd_invest = new DatabaseHelper(TargetDetailsActivity.this).getDataByID(investment_id, Utils.DBtables.INVESTMENT_table);
//
//                        if (cmd_invest.size() > 0) {
//
//                            CommonData cmd1 = cmd_invest.get(0);
//
//                            JSONObject jsonObject=new JSONObject(cmd1.getData());
//
//
//                            String month= jsonObject.getString("name");

                            List<CommonData> data=new DatabaseHelper(TargetDetailsActivity.this).getAccountSettingsByID(investment_id);

                            if(data.size()>0)
                            {

                                JSONObject jso=new JSONObject(data.get(0).getData());
                                String amount= jso.getString("Amount");

                                txtInvestmentName.setText(jso.getString("Accountname"));

                                 closingbalance=   getClosingBalance(Double.parseDouble(amount),data.get(0).getId(),"","");


                                txtClosingbalance.setText(closingbalance+"");

                                // holder.txtvoucher.setText();

                            }

                     //   }

                    }
                    else{

                        closingbalance=0;
                        txtClosingbalance.setVisibility(View.GONE);

                        txtInvestmentName.setVisibility(View.GONE);
                        layout_closingbalance.setVisibility(View.GONE);
                        layout_investmentaccount.setVisibility(View.GONE);
                    }








                    List<CommonData> cmd1 = new DatabaseHelper(TargetDetailsActivity.this).getData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);

                    double dsaved = Double.parseDouble(savedamount);
                    //  double saved_amount=0;

                    txtNote.setText(note);

                    for (CommonData cm1 : cmd1) {

                        JSONObject j = new JSONObject(cm1.getData());
                        String am = j.getString("savedamount");
                        String tid = j.getString("targetid");

                        if (tid.equalsIgnoreCase(data.getId())) {

                            double t = Double.parseDouble(am);

                            dsaved = dsaved + t;

                        }


                    }
                    totalAddedAmount.setText(dsaved+"");

                    double t=dsaved+closingbalance;


                    txtsavedamount.setText( t+"");

                    txtTargetdate.setText( target_date+"");

                    txtTargetAmount.setText(targetamount+"");

                    TargetCategory tg = new DatabaseHelper(TargetDetailsActivity.this).getTargetCategoryById(target_categoryid);

                    if (tg != null) {
                        byte[] b = tg.getImage();

                        Drawable image = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(b, 0, b.length));


                        imgicon.setImageDrawable(image);
                        txtTargetCategory.setText(tg.getTask_category());

                    }

                    double sa = dsaved;

                    targetamount_already = Double.parseDouble(targetamount);

                    savedamount_already = sa;
                    double ta = Double.parseDouble(targetamount);

                    double p = t / ta;

                    double percentage = p * 100;
                    DecimalFormat df = new DecimalFormat("0.00");

                    String a = df.format(percentage);

                    int per = (int) percentage;

                    progressBar.setProgress(per);


                    txtpercent.setText(a + " % \n " + t + " / " + targetamount);
                } else {

                    layout_nodata.setVisibility(View.VISIBLE);
                    layout_targetdetails.setVisibility(View.GONE);
                    imgedit.setVisibility(View.GONE);
                }
            }
            else{

                layout_nodata.setVisibility(View.VISIBLE);
                layout_targetdetails.setVisibility(View.GONE);
                imgedit.setVisibility(View.GONE);

            }


        }
        catch (Exception e)
        {

        }
    }


    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = new DatabaseHelper(TargetDetailsActivity.this).getAllAccounts();

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }
}