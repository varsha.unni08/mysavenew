package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AddedAmountAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.AddedAmount;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.github.mikephil.charting.charts.BarChart;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MileStoneAddedActivity extends AppCompatActivity {

    ImageView imgback;
    TextView txtDetails,txtstartdate,txtenddate,txtAmount;

    MileStone mileStone;
    RecyclerView recycler;

    String amount = "";
    String start_date = "";
    String end_date = "";
    String targetid = "";

ProgressBar progressBar;
TextView txtpercent;

double totalamount_addedinmilestone=0;

List<AddedAmount>adm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mile_stone_added);
        getSupportActionBar().hide();
        adm=new ArrayList<>();

        Intent i=getIntent();
        mileStone=(MileStone)i.getSerializableExtra("mileStone");




        imgback=findViewById(R.id.imgback);
        txtDetails=findViewById(R.id.txtDetails);
        txtpercent=findViewById(R.id.txtpercent);
        progressBar=findViewById(R.id.progressBar);
        recycler=findViewById(R.id.recycler);


        txtAmount=findViewById(R.id.txtAmount);
        txtstartdate=findViewById(R.id.txtstartdate);
        txtenddate=findViewById(R.id.txtenddate);


        try {



             amount = mileStone.getAmount();
             start_date = mileStone.getStart_date();
             end_date = mileStone.getEnd_date();
             targetid = mileStone.getTargetid();

//            txtDetails.setText("Start date : "+start_date+"\nEnd date : "+end_date+"\nMileStone target amount : "+amount);

            txtAmount.setText(amount);

            txtstartdate.setText(start_date);

            txtenddate.setText(end_date);


            List<CommonData>cmd=new DatabaseHelper(MileStoneAddedActivity.this).getData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);

            Date addeddate=null;
            Date milestonestartdate=null,milestoneedndate=null;
            SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

            for(CommonData cm:cmd)
            {



                JSONObject j=new JSONObject(cm.getData());
                String date= j.getString("date");
                String am=j.getString("savedamount");
                String tid=j.getString("targetid");




                addeddate=myFormat.parse(date);
                milestonestartdate=myFormat.parse(start_date);
                milestoneedndate=myFormat.parse(end_date);

                double d =0;


                if(tid.equalsIgnoreCase(mileStone.getTargetid())) {
                    d = Double.parseDouble(am);


                    if(addeddate.equals(milestonestartdate))
                    {
                        totalamount_addedinmilestone=totalamount_addedinmilestone+d;

                        AddedAmount admt=new AddedAmount();
                        admt.setAmount(am);
                        admt.setTid(tid);
                        admt.setDate(date);
                        admt.setId(cm.getId());
                        adm.add(admt);

                    }

                    if(addeddate.after(milestonestartdate))
                    {
                        totalamount_addedinmilestone=totalamount_addedinmilestone+d;
                        AddedAmount admt=new AddedAmount();
                        admt.setAmount(am);
                        admt.setTid(tid);
                        admt.setDate(date);
                        admt.setId(cm.getId());
                        adm.add(admt);

                    }

                    if(milestoneedndate.before(addeddate))
                    {
                        totalamount_addedinmilestone=totalamount_addedinmilestone+d;
                        AddedAmount admt=new AddedAmount();
                        admt.setAmount(am);
                        admt.setTid(tid);
                        admt.setDate(date);
                        admt.setId(cm.getId());
                        adm.add(admt);

                    }

                    if(addeddate.equals(milestoneedndate))
                    {
                        totalamount_addedinmilestone=totalamount_addedinmilestone+d;
                        AddedAmount admt=new AddedAmount();
                        admt.setAmount(am);
                        admt.setTid(tid);
                        admt.setDate(date);
                        admt.setId(cm.getId());
                        adm.add(admt);

                    }









                }



            }


            double targetamount=Double.parseDouble(amount);

            double d=totalamount_addedinmilestone/targetamount;

            double percent=d*100;

            int p=(int)percent;

            txtpercent.setText(totalamount_addedinmilestone+"/"+targetamount+"\n"+p+" %");
            progressBar.setProgress(p);

            recycler.setLayoutManager(new LinearLayoutManager(MileStoneAddedActivity.this));
            recycler.setAdapter(new AddedAmountAdapter(MileStoneAddedActivity.this,adm));




        }
        catch (Exception e)
        {


        }



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }
}