package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.CashBankDataAdapter;
import com.centroid.integraaccounts.adapter.LedgerHeadAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BankBalanceActivity extends AppCompatActivity {

    ImageView imgback,imgmonthyear,imgdownload,imgstartDatepick,imgendDatepick;

    FloatingActionButton fab_addtask;

    RecyclerView recycler;
    TextView txtMonthyear,txtstartdatepick,txtenddatepick;

    LinearLayout layout_inchead;

    String monthyear = "";

    String startdate="",endate="";

    int m=0,yea=0;
    List<CommonData>commonDataSelected;

    String month_selected = "", yearselected = "";
    
    List<LedgerAccount>ledgerAccounts=new ArrayList<>();

    Button btnSearch;

    String arrmonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    List<Accounts>accountsbydate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_balance);
        getSupportActionBar().hide();
        commonDataSelected=new ArrayList<>();
        imgback=findViewById(R.id.imgback);
        layout_inchead=findViewById(R.id.layout_inchead);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgdownload=findViewById(R.id.imgdownload);
        imgmonthyear=findViewById(R.id.imgmonthyear);
        txtMonthyear=findViewById(R.id.txtMonthyear);

        imgendDatepick=findViewById(R.id.imgendDatepick);
        imgstartDatepick=findViewById(R.id.imgstartDatepick);

        txtstartdatepick=findViewById(R.id.txtstartdatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);

        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

                //showMonthYear();
            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });



        txtMonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthYear();
            }
        });


        imgmonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthYear();
            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (!startdate.equalsIgnoreCase("")) {

                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate)||endDate.equals(strDate))
                            {

                                accountsbydate= Utils.getAccountsBetweenDates(BankBalanceActivity.this,strDate,endDate);



                             getBankBalancedata();

                                // showTransactions(accountsbydate);

                            }

                            else {

                             //   Toast.makeText(BankBalanceActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();

                                Utils.showAlertWithSingle(BankBalanceActivity.this,"Select date properly", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });


                            }


                        } else {

                          //  Toast.makeText(BankBalanceActivity.this, "Select end date", Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(BankBalanceActivity.this,"Select end date", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }


                    } else {

                      //  Toast.makeText(BankBalanceActivity.this, "Select start date", Toast.LENGTH_SHORT).show();


                        Utils.showAlertWithSingle(BankBalanceActivity.this,"Select start date", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }

                }catch (Exception e)
                {

                }
            }
        });



        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(ledgerAccounts.size()>0)
                {


                    Utils.showAlert(BankBalanceActivity.this, "Do you want to download pdf ? ", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            if(ContextCompat.checkSelfPermission(BankBalanceActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {


                                ActivityCompat.requestPermissions(BankBalanceActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
                            }
                            else {



                                downloadPdf();


                            }

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }




            }
        });




getBillListByDate();
    }



    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(BankBalanceActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else {

                    endate = i2 + "-" + m + "-" + i;

                    txtenddatepick.setText(i2 + "-" + m + "-" + i);
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }



    private void downloadPdf()
    {

        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "Save/" + "CashBankBalance");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/CashBankBalance" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();





            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk("Cash or Bank balance \n\n", mOrderDetailsTitleFont);// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph);

            document.add(new Paragraph(" \n\n"));


            PdfPTable table = new PdfPTable(2);
            table.addCell("Account");
            table.addCell("Closing balance");


            int aw=0;

            int a= ledgerAccounts.size();

            for (int i=0;i<a;i++){

                List<CommonData> commonData = new DatabaseHelper(BankBalanceActivity.this).getAccountSettingsByID(ledgerAccounts.get(i).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table.addCell(jsonObject.getString("Accountname"));


                }
                else {
                    table.addCell("Cash");

                }

                table.addCell(ledgerAccounts.get(i).getClosingbalance());


            }


            document.add(table);









            document.close();
            Utils.playSimpleTone(BankBalanceActivity.this);
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(BankBalanceActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }




        }catch (Exception e)
        {

        }
    }















    public void showMonthYear() {

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR)-1;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels / 2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(BankBalanceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set = dialog.findViewById(R.id.date_time_set);


        final NumberPicker npmonth = dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);

        npmonth.setDisplayedValues(arrmonth);


        final NumberPicker npyear = dialog.findViewById(R.id.npyear);

        Calendar calendar1=Calendar.getInstance();
        int year1=calendar1.get(Calendar.YEAR)-1;
        int mm=calendar1.get(Calendar.MONTH);

        npyear.setMinValue(year1);
        npyear.setMaxValue(year1+5);
        npyear.setValue(year1);
        npmonth.setValue(mm);




        dialog.getWindow().setLayout(width, height);


        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                int m = npmonth.getValue() + 1;
                month_selected = m + "";
                yearselected = npyear.getValue() + "";

                monthyear = npmonth.getValue() + "/" + npyear.getValue();

                txtMonthyear.setText(arrmonth[npmonth.getValue()] + "/" +npyear.getValue() );

                getBankBalancedata();


            }
        });


        dialog.show();

    }

    public void showCurrentMonthYear()
    {
        Calendar calendar=Calendar.getInstance();
        int m = calendar.get(Calendar.MONTH) + 1;
        month_selected = m + "";
        yearselected = calendar.get(Calendar.YEAR) + "";

        monthyear = m-1 + "/" + calendar.get(Calendar.YEAR);

        txtMonthyear.setText(arrmonth[m-1] + "/" +yearselected );

    }


    public void getBillListByDate()
    {
        try {
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH) + 1;
            month_selected = m + "";
            yearselected = calendar.get(Calendar.YEAR) + "";
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);


            startdate = "1" + "-" + m + "-" + yearselected;

            endate = dayOfMonth + "-" + m + "-" + yearselected;

            txtstartdatepick.setText(startdate);
            txtenddatepick.setText(endate);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(endate);

            if (endDate.after(strDate) || endDate.equals(strDate)) {

                accountsbydate = Utils.getAccountsBetweenDates(BankBalanceActivity.this, strDate, endDate);


                getBankBalancedata();

                // showTransactions(accountsbydate);

            }


            // getAccounthead();

        }catch (Exception e)
        {

        }




    }



    @Override
    protected void onRestart() {
        super.onRestart();
      //  getBillListByDate();
    }

    public void getBankBalancedata()
    {
        try{


            commonDataSelected.clear();

        List<CommonData>commonData=new DatabaseHelper(BankBalanceActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if(commonData.size()>0)
        {


            for (int i=0;i<commonData.size();i++) {


                JSONObject jsonObject = new JSONObject(commonData.get(i).getData());


                String accounttype = jsonObject.getString("Accounttype");

                if(accounttype.equalsIgnoreCase("Bank"))
                {
                    commonDataSelected.add(commonData.get(i));
                }
                else if(accounttype.equalsIgnoreCase("Cash"))
                {
                    commonDataSelected.add(commonData.get(i));
                }


            }

            JSONObject jobj=new JSONObject();

          jobj.put("Amount","0");
            jobj.put("Accountname", "Cash");
            jobj.put("Accounttype", "Cash");


            CommonData data=new CommonData();
            data.setId("0");
            data.setData(jobj.toString());

            commonDataSelected.add(data);

        }


            getBankBalanceData();



        }catch (Exception e)
        {

            Log.e("TAGGG",e.toString());
        }







    }


    public void getBankBalanceData()
    {


            ledgerAccounts.clear();
            List<Accounts> accountsSorted = accountsbydate;

            List<Accounts>accstring=new ArrayList<>();

            for (Accounts acc:accountsSorted) {

                if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
                {
                    accstring.add(acc);
                }

            }


            String selected_date = startdate;


            try {

                for (CommonData cm : commonDataSelected) {


                    JSONObject jobj=new JSONObject(cm.getData());

                    String amount=jobj.getString("Amount");
                    double d=Double.parseDouble(amount);


                    if(d>0)
                    {
                        double openingbalance = 0;


                        if (!cm.getData().equalsIgnoreCase("")) {

                            JSONObject jsonObject = new JSONObject(cm.getData());

                            openingbalance = Double.parseDouble(jsonObject.getString("Amount"));

                        } else {

                            openingbalance = 0;
                        }


                        double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), selected_date, "Income");

                        double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");


                        finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                        LedgerAccount ledgerAccount = new LedgerAccount();
                        if(closingbalancebeforedate<0)
                        {

                            ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                        }
                        else {

                            ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                        }


                        if(finalclosingbalance<0) {

                            ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                        }
                        else {
                            ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                        }


                        ledgerAccount.setAccountheadid(cm.getId());
                        ledgerAccount.setAccountheadname("");
                        ledgerAccount.setClosingbalance(finalclosingbalance + "");

                        ledgerAccounts.add(ledgerAccount);


                    }




                    for (Accounts acc:accstring) {



                      //  if(acc.getACCOUNTS_year().equalsIgnoreCase(yearselected)&&acc.getACCOUNTS_month().equalsIgnoreCase(month_selected)) {


                            if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                                double openingbalance = 0;


                                if (!cm.getData().equalsIgnoreCase("")) {

                                    JSONObject jsonObject = new JSONObject(cm.getData());

                                    openingbalance = Double.parseDouble(jsonObject.getString("Amount"));

                                } else {

                                    openingbalance = 0;
                                }


                                double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), selected_date, "Income");

                                double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");


                                finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                                if(!isAccountExist(ledgerAccounts,cm.getId())) {

                                    LedgerAccount ledgerAccount = new LedgerAccount();
                                    ledgerAccount.setAccountheadid(cm.getId());

                                    if(closingbalancebeforedate<0)
                                    {

                                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                                    }
                                    else {

                                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                                    }


                                    if(finalclosingbalance<0) {

                                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                                    }
                                    else {
                                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                                    }



                                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");
                                    ledgerAccount.setAccountheadname("");
                                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                                    ledgerAccounts.add(ledgerAccount);
                                }

                            }

                       // }
                    }
//
                }
//

            } catch (Exception e) {

            }


            if(ledgerAccounts.size()>0)
            {



                layout_inchead.setVisibility(View.VISIBLE);
                recycler.setVisibility(View.VISIBLE);

                recycler.setLayoutManager(new LinearLayoutManager(BankBalanceActivity.this));
                recycler.setAdapter(new CashBankDataAdapter(BankBalanceActivity.this, ledgerAccounts,startdate,endate));

            }
            else {


                layout_inchead.setVisibility(View.INVISIBLE);
                recycler.setVisibility(View.INVISIBLE);
            }


        }


    private boolean isAccountExist(List<LedgerAccount>accounts,String setupid)
    {

        boolean a=false;
        for (LedgerAccount acc:accounts) {

            if(acc.getAccountheadid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }


    private boolean isAccountAlreadyExist(List<Accounts>accounts,String setupid)
    {

        boolean a=false;
        for (Accounts acc:accounts) {

            if(acc.getACCOUNTS_setupid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }



    private double getFinalClosingBalance(String month,String year1,String id,String type)
    {
        double finalclosingbalance=0;

        List<Accounts> accountsSorted = accountsbydate;



        for (Accounts accounts : accountsSorted) {



            if (id.equalsIgnoreCase(accounts.getACCOUNTS_setupid())) {

              //  if (accounts.getACCOUNTS_year().equalsIgnoreCase(year1) && accounts.getACCOUNTS_month().equalsIgnoreCase(month)) {


                 //   if(!id.equalsIgnoreCase("0")) {


                        if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            finalclosingbalance = finalclosingbalance - Double.parseDouble(accounts.getACCOUNTS_amount());


                        } else if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());


                        }

                //    }
//                    else {
//
//
//                        if(type.equalsIgnoreCase("Income"))
//                        {
//                            finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());
//
//                        }
//                        else {
//
//                            finalclosingbalance = finalclosingbalance - Double.parseDouble(accounts.getACCOUNTS_amount());
//
//                        }
//                    }


                    //  finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());


                    // incomeAccount.add(accounts);

               // }


            }
        }






        return finalclosingbalance;


    }


















    private double getClosingbalancebeforedate(double openingbalance, String id, String selected_date,String type) {

        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeDate(BankBalanceActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


              //  if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());


                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());


                        }


                    }
              //  }
//                else if(id.equalsIgnoreCase("0")&&acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {
//
//
//                    if(type.equalsIgnoreCase("Income"))
//                    {
//                        openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//                    else {
//
//                        openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//
//
//
//
//                }


            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;
        }

        return closingbalancebeforemonth;
    }


}
