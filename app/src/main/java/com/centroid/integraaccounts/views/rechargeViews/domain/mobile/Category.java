
package com.centroid.integraaccounts.views.rechargeViews.domain.mobile;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Category {

    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("fullName")
    @Expose
    private String fullName="";
    @SerializedName("plans")
    @Expose
    private List<Plan> plans=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public void setPlans(List<Plan> plans) {
        this.plans = plans;
    }

}
