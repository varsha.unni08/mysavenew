package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.FeedbackMessageAdapter;
import com.centroid.integraaccounts.data.domain.FeedbackmessageData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class FeedbackMessageActivity extends AppCompatActivity {

    FloatingActionButton fab_add;

    RecyclerView recycler;
    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_message);
        getSupportActionBar().hide();
        fab_add=findViewById(R.id.fab_add);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);


        getFeedBacks();

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                double height = displayMetrics.heightPixels/1.6;

                int h=(int)height;
                int width = displayMetrics.widthPixels;

                final Dialog dialog=new Dialog(FeedbackMessageActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_feedbackform);

                EditText edtFeedback=dialog.findViewById(R.id.edtFeedback);
                Button btnAdd=dialog.findViewById(R.id.btnAdd);


                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if(!edtFeedback.getText().toString().equalsIgnoreCase(""))
                        {
                            dialog.dismiss();


uploadFeedback(edtFeedback.getText().toString());



                        }
                        else {

                            Toast.makeText(FeedbackMessageActivity.this,"Enter your feedback",Toast.LENGTH_SHORT).show();
                        }

                    }
                });



                dialog.getWindow().setLayout(width,h);


                dialog.show();
            }
        });
    }




    public void getFeedBacks()
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(FeedbackMessageActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    FeedbackmessageData feedbackmessageData=new GsonBuilder().create().fromJson(data,FeedbackmessageData.class);

                    try{



                        if(feedbackmessageData.getStatus()==1)
                        {

                            FeedbackMessageAdapter feedbackMessageAdapter=new FeedbackMessageAdapter(FeedbackMessageActivity.this,feedbackmessageData.getData());
                            recycler.setLayoutManager(new LinearLayoutManager(FeedbackMessageActivity.this));
                            recycler.setAdapter(feedbackMessageAdapter);

                            //  Toast.makeText(FeedbackMessageActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();


                        }
                        else {

                            Toast.makeText(FeedbackMessageActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getFeedback+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();







//        String key=new PreferenceHelper(FeedbackMessageActivity.this).getData(Utils.userkey);
//
//
//
//
//        Call<FeedbackmessageData> jsonObjectCall=client.getFeedback(key);
//        jsonObjectCall.enqueue(new Callback<FeedbackmessageData>() {
//            @Override
//            public void onResponse(Call<FeedbackmessageData> call, Response<FeedbackmessageData> response) {
//
//               // progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//                            FeedbackMessageAdapter feedbackMessageAdapter=new FeedbackMessageAdapter(FeedbackMessageActivity.this,response.body().getData());
//                            recycler.setLayoutManager(new LinearLayoutManager(FeedbackMessageActivity.this));
//                            recycler.setAdapter(feedbackMessageAdapter);
//
//                          //  Toast.makeText(FeedbackMessageActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();
//
//
//                        }
//                        else {
//
//                             Toast.makeText(FeedbackMessageActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<FeedbackmessageData> call, Throwable t) {
//
//                //progressFragment.dismiss();
//            }
//        });
    }



    public void uploadFeedback(String message)
    {

//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("addFeedback.php")
//        Call<JsonObject> addFeedback(@Header("Authorization") String Authorization,@Field("message") String message);


        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();
        params.put("message",message);
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(FeedbackMessageActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {
                    getFeedBacks();

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {


                            Toast.makeText(FeedbackMessageActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();


                        }
                        else {

                            // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.addFeedback, Request.Method.POST).submitRequest();






//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");
//
//
//        String key=new PreferenceHelper(FeedbackMessageActivity.this).getData(Utils.userkey);
//
//
//
//
//        Call<JsonObject> jsonObjectCall=client.addFeedback(key,message);
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//                    getFeedBacks();
//
//                    try{
//
//                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        if(jsonObject.getInt("status")==1)
//                        {
//
//
//                            Toast.makeText(FeedbackMessageActivity.this,"Your feedback posted successfully",Toast.LENGTH_SHORT).show();
//
//
//                        }
//                        else {
//
//                            // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });


    }
}
