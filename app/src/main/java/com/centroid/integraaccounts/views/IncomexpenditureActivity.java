package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.LedgerAccountAdapter;
import com.centroid.integraaccounts.adapter.LedgerHeadAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IncomexpenditureActivity extends AppCompatActivity {


    RecyclerView recyclerincome, recyclerExpense;

    TextView txtTotalIncome, txtTotalExpense,txtDate;


    ImageView imgback, imgmonthyear,imgdownload;

    FloatingActionButton fab_addtask;

    RecyclerView recycler;
    TextView txtMonthyear;

    String month_selected = "", yearselected = "";

    String monthyear = "";

    int m = 0, yea = 0;

    double totalexp=0,totalinc=0;

    List<CommonData> cmfiltered_income, cmfiltered_expense;

    String arrmonth[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    List<LedgerAccount> ledgerAccountsexpense = new ArrayList<>();

    List<LedgerAccount> ledgerAccountsincome = new ArrayList<>();

    Handler handler;

    LinearLayout layout_ledgerhead,layout_inchead,layout_headexpense;

    Resources resources;

    ImageView imgstartDatepick,imgendDatepick;

    TextView txtstartdatepick,txtenddatepick;

    String startdate="",endate="";

    Button btnSearch;

    List<Accounts>   accountsbydate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incomexpenditure);
        getSupportActionBar().hide();
        handler=new Handler();
        String languagedata = LocaleHelper.getPersistedData(IncomexpenditureActivity.this, "en");
        Context context= LocaleHelper.setLocale(IncomexpenditureActivity.this, languagedata);
        resources=context.getResources();
        cmfiltered_income = new ArrayList<>();
        cmfiltered_expense = new ArrayList<>();
        accountsbydate=new ArrayList<>();
        imgstartDatepick = findViewById(R.id.imgstartDatepick);
        imgendDatepick = findViewById(R.id.imgendDatepick);
        txtstartdatepick = findViewById(R.id.txtstartdatepick);
        txtenddatepick = findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);
        imgback = findViewById(R.id.imgback);
        fab_addtask = findViewById(R.id.fab_addtask);
        recycler = findViewById(R.id.recycler);
        layout_ledgerhead=findViewById(R.id.layout_ledgerhead);
        layout_inchead=findViewById(R.id.layout_inchead);
        layout_headexpense=findViewById(R.id.layout_headexpense);
        imgdownload=findViewById(R.id.imgdownload);
        imgmonthyear = findViewById(R.id.imgmonthyear);
        txtMonthyear = findViewById(R.id.txtMonthyear);
        recyclerincome = findViewById(R.id.recyclerincome);
        recyclerExpense = findViewById(R.id.recyclerExpense);
        txtTotalIncome = findViewById(R.id.txtTotalIncome);

        txtTotalExpense = findViewById(R.id.txtTotalExpense);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {

                    if (!startdate.equalsIgnoreCase("")) {

                        if (!endate.equalsIgnoreCase("")) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = sdf.parse(startdate);
                            Date endDate = sdf.parse(endate);

                            if(endDate.after(strDate)||endDate.equals(strDate))
                            {


                                String arr[]=startdate.split("-");





                   accountsbydate= new DatabaseHelper(IncomexpenditureActivity.this).getAllAccounts();



                                getAccounthead();



                            }

                            else {

                              //  Toast.makeText(IncomexpenditureActivity.this, "Select date properly", Toast.LENGTH_SHORT).show();

                                Utils.showAlertWithSingle(IncomexpenditureActivity.this,"Select date properly", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });


                            }


                        } else {

                          //  Toast.makeText(IncomexpenditureActivity.this, "Select end date", Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(IncomexpenditureActivity.this,"Select end date", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });


                        }


                    } else {

                     //   Toast.makeText(IncomexpenditureActivity.this, "Select start date", Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(IncomexpenditureActivity.this,"Select start date", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });



                    }

                }catch (Exception e)
                {

                }


            }
        });






        txtstartdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);
            }
        });

        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        imgstartDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);

            }
        });

        imgendDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });





        txtMonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthYear();
            }
        });


        imgmonthyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMonthYear();
            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ledgerAccountsincome.size()>0||ledgerAccountsexpense.size()>0)
                {


                    Utils.showAlert(IncomexpenditureActivity.this, "Do you want to download pdf ? ", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            if(ContextCompat.checkSelfPermission(IncomexpenditureActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {


                                ActivityCompat.requestPermissions(IncomexpenditureActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);
                            }
                            else {

                                downloadPdf();
                            }

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }


            }
        });

        showCurrentMonthYear();
    }



    public void showCurrentMonthYear()
    {


        try {
            Calendar calendar = Calendar.getInstance();
            int m = calendar.get(Calendar.MONTH) + 1;
            month_selected = m + "";
            yearselected = calendar.get(Calendar.YEAR) + "";
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);


            startdate = "1" + "-" + m + "-" + yearselected;

            endate = dayOfMonth + "-" + m + "-" + yearselected;

            txtstartdatepick.setText(startdate);
            txtenddatepick.setText(endate);

            String arr[]=startdate.split("-");


            String currentYearStartdate="1-1-"+arr[2];




            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date strDate = sdf.parse(startdate);
            Date endDate = sdf.parse(endate);

            //  Date currystdate=sdf.parse(currentYearStartdate);

            Date curr_ystdate = sdf.parse(currentYearStartdate);

            if (endDate.after(strDate) || endDate.equals(strDate)) {

                // accountsbydate = Utils.getAccountsBetweenDates(LedgerActivity.this, curr_ystdate, endDate);

                accountsbydate=new DatabaseHelper(IncomexpenditureActivity.this).getAllAccounts();
                getAccounthead();

                // showTransactions(accountsbydate);

            }


            // getAccounthead();

        }catch (Exception e)
        {

        }

    }


    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(IncomexpenditureActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                if (code==0) {

                    startdate = i2 + "-" + m + "-" + i;

                    txtstartdatepick.setText(i2 + "-" + m + "-" + i);
                }
                else {

                    endate = i2 + "-" + m + "-" + i;

                    txtenddatepick.setText(i2 + "-" + m + "-" + i);
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }







    private void downloadPdf()
    {

        try {

            File folder = new File(this.getExternalCacheDir() + "/" + "Save/" + "IncomeExpenditure");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/Save" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();





            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk("Income \n\n", mOrderDetailsTitleFont);// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph);

            document.add(new Paragraph(" \n\n"));


            PdfPTable table = new PdfPTable(3);
            table.addCell("Account");
            table.addCell("Debit");
            table.addCell("Credit");



            int aw=0;

            int a= ledgerAccountsincome.size();

            while (aw<a){

                List<CommonData> commonData = new DatabaseHelper(IncomexpenditureActivity.this).getAccountSettingsByID(ledgerAccountsincome.get(aw).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table.addCell(jsonObject.getString("Accountname"));


                }




                if(ledgerAccountsincome.get(aw).getDebitcreditopening().equalsIgnoreCase(Utils.Cashtype.debit+""))
                {
                    table.addCell(ledgerAccountsincome.get(aw).getClosingbalance());
                    table.addCell("");
                }
                else {

                    table.addCell("");
                    table.addCell(ledgerAccountsincome.get(aw).getClosingbalance());

                }





                aw++;
            }


            document.add(table);

            document.add(new Paragraph("\nTotal Income : "+totalinc+"  \n"));


            Font mOrderDetailsTitleFont1 = new Font(Font.FontFamily.TIMES_ROMAN, 15.0f, Font.NORMAL, BaseColor.BLACK);// Creating Chunk
            Chunk mOrderDetailsTitleChunk1 = new Chunk("Expense \n", mOrderDetailsTitleFont1);// Creating Paragraph to add...


            Paragraph mOrderDetailsTitleParagraph1 = new Paragraph(mOrderDetailsTitleChunk1);
            mOrderDetailsTitleParagraph1.setAlignment(Element.ALIGN_CENTER);






            document.add(mOrderDetailsTitleParagraph1);






            document.add(new Paragraph(" \n\n"));



            PdfPTable table1 = new PdfPTable(3);
            table1.addCell("Account");
            table1.addCell("Debit");
            table1.addCell("Credit");


             aw=0;

             a= ledgerAccountsexpense.size();

            while (aw<a){

                List<CommonData> commonData = new DatabaseHelper(IncomexpenditureActivity.this).getAccountSettingsByID(ledgerAccountsexpense.get(aw).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    table1.addCell(jsonObject.getString("Accountname"));


                }

              //  table1.addCell(ledgerAccountsexpense.get(aw).getClosingbalance());


                if(ledgerAccountsexpense.get(aw).getDebitcreditopening().equalsIgnoreCase(Utils.Cashtype.debit+""))
                {
                    table1.addCell(ledgerAccountsexpense.get(aw).getClosingbalance());
                    table1.addCell("");
                }
                else {

                    table1.addCell("");
                    table1.addCell(ledgerAccountsexpense.get(aw).getClosingbalance());

                }

                aw++;
            }

            document.add(table1);

            document.add(new Paragraph("\nTotal Expense : "+totalexp+"  \n"));




            if(totalinc>totalexp)
            {
                if(totalexp<0)
                {
                    totalexp=totalexp*-1;
                }


                double d=totalinc-totalexp;
                document.add(new Paragraph("\n Income over expense "+d+" "+resources.getString(R.string.rs)+" \n"));

            }
            else if (totalexp>totalinc){

                double d=totalinc-totalexp;

                document.add(new Paragraph(("\n Expense over Income"+d+" "+resources.getString(R.string.rs)+" \n")));
            }
            else {

                document.add(new Paragraph("\n Expense equal to Income\n"));
            }









            document.close();
            Utils.playSimpleTone(IncomexpenditureActivity.this);
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(IncomexpenditureActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }




        }catch (Exception e)
        {

        }
    }










    public void getIncomeExpenseAccountHead() {


        totalexp=0;
        totalinc=0;
        getAccounthead();


    }


    public void showMonthYear() {

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels / 2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(IncomexpenditureActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set = dialog.findViewById(R.id.date_time_set);


        final NumberPicker npmonth = dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);

        npmonth.setDisplayedValues(arrmonth);


        final NumberPicker npyear = dialog.findViewById(R.id.npyear);

        Calendar calendar1 = Calendar.getInstance();
        int year1 = calendar1.get(Calendar.YEAR)-1;
        int mm = calendar1.get(Calendar.MONTH);

        npyear.setMinValue(year1);
        npyear.setMaxValue(year1 + 5);
        npyear.setValue(year1);
        npmonth.setValue(mm);


        dialog.getWindow().setLayout(width, height);


        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                int m = npmonth.getValue() + 1;
                month_selected = m + "";
                yearselected = npyear.getValue() + "";

                monthyear = npmonth.getValue() + "/" + npyear.getValue();

                txtMonthyear.setText(arrmonth[npmonth.getValue()] + "/" + npyear.getValue());

                getIncomeExpenseAccountHead();
                //   getBankBalancedata();


            }
        });


        dialog.show();

    }



    public void setCurrentMonthYear()
    {
        Calendar calendar=Calendar.getInstance();

        int m = calendar.get(Calendar.MONTH) + 1;
        month_selected = m + "";
        yearselected = calendar.get(Calendar.YEAR) + "";


        monthyear = calendar.get(Calendar.MONTH) + "/" + yearselected;

        txtMonthyear.setText(arrmonth[m-1] + "/" + yearselected);

        getAccounthead();
    }





    public void getAccounthead() {

        layout_ledgerhead.setVisibility(View.VISIBLE);

        cmfiltered_expense.clear();
        cmfiltered_income.clear();

        List<CommonData> commonData = new DatabaseHelper(IncomexpenditureActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            //Collections.reverse(commonData);


            for (CommonData cm : commonData) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Income account")) {
                        cmfiltered_income.add(cm);
                    }

                    if (acctype.equalsIgnoreCase("Expense account")) {
                        cmfiltered_expense.add(cm);
                    }


                } catch (Exception e) {

                }

            }




        }
        else {

            layout_inchead.setVisibility(View.GONE);
            recyclerincome.setVisibility(View.GONE);


        }

        checkIncomeHeads();
        // checkExpenseHeads();



    }


    private boolean isAccountAlreadyExist(List<Accounts>accounts,String setupid)
    {

        boolean a=false;
        for (Accounts acc:accounts) {

            if(acc.getACCOUNTS_setupid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }





    private void checkIncomeHeads()
    {

      //  List<LedgerAccount> ledgerAccounts = new ArrayList<>();

        ledgerAccountsincome.clear();
        List<Accounts> accountsSorted = accountsbydate;

        List<Accounts>accstring=new ArrayList<>();

        for (Accounts acc:accountsSorted) {

            if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
            {
                accstring.add(acc);
            }

        }

        String selected_date = startdate;

        try {

            for (CommonData cm : cmfiltered_income) {

                JSONObject jobj=new JSONObject(cm.getData());

                String amount=jobj.getString("Amount");
                double d=Double.parseDouble(amount);
                // Log.e("Amount",d+"");


                if(d>0)
                {


                    double openingbalance = 0;

                    String Type="";

                    if (!cm.getData().equalsIgnoreCase("")) {

                        JSONObject jsonObject = new JSONObject(cm.getData());

                        openingbalance = Double.parseDouble(jsonObject.getString("Amount"));

                        Type=jsonObject.getString("Type");


                    } else {

                        openingbalance = 0;
                    }

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        openingbalance=openingbalance*-1;
                    }





                    double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

//                    double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");

                    double finalclosingbalance = 0;

                    finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                    LedgerAccount ledgerAccount = new LedgerAccount();
                    ledgerAccount.setAccountheadid(cm.getId());
                    ledgerAccount.setAccountheadname("");
                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate+"");

                    if(closingbalancebeforedate<0)
                    {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                    }
                    else {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                    }





                    if(finalclosingbalance<0) {

                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                    }
                    else {
                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                    }
                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                    ledgerAccountsincome.add(ledgerAccount);

                }









                for (Accounts acc:accstring) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                        double openingbalance = 0;

                        String Type="";


                        if (!cm.getData().equalsIgnoreCase("")) {

                            JSONObject jsonObject = new JSONObject(cm.getData());

                            openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                            Type=jsonObject.getString("Type");

                        } else {

                            openingbalance = 0;
                        }

                        if(Type.equalsIgnoreCase("Credit"))
                        {
                            if(openingbalance>0) {
                                openingbalance = openingbalance * -1;
                            }
                        }






                        double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

                        // double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");
                        double finalclosingbalance = 0;


                        finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                        if(!isAccountExist(ledgerAccountsincome,cm.getId())) {


                            LedgerAccount ledgerAccount = new LedgerAccount();
                            ledgerAccount.setAccountheadid(cm.getId());
                            ledgerAccount.setAccountheadname("");
                            ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");

                            if(closingbalancebeforedate<0)
                            {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                            }
                            else {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                            }

                            if (finalclosingbalance<0) {

                                ledgerAccount.setDebitorcredit(Utils.Cashtype.credit + "");

                            } else {
                                ledgerAccount.setDebitorcredit(Utils.Cashtype.debit + "");
                            }
                            ledgerAccount.setClosingbalance(finalclosingbalance + "");

                            ledgerAccountsincome.add(ledgerAccount);
                        }

                    }

                    // }
                }
//
            }
//

        } catch (Exception e) {

        }


        if(ledgerAccountsincome.size()>0)
        {

            for (LedgerAccount ledgerAccount:ledgerAccountsincome) {

                totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());



            }
            txtTotalIncome.setText("Total Income : "+totalinc+" "+resources.getString(R.string.rs));

            layout_inchead.setVisibility(View.VISIBLE);
            recyclerincome.setVisibility(View.VISIBLE);
            imgdownload.setVisibility(View.VISIBLE);
            //txtStatement.setVisibility(View.VISIBLE);

            recyclerincome.setLayoutManager(new LinearLayoutManager(IncomexpenditureActivity.this));
            recyclerincome.setAdapter(new LedgerAccountAdapter(IncomexpenditureActivity.this, ledgerAccountsincome,startdate,endate));

        }
        else {

            txtTotalIncome.setText("Total Income : "+0+" "+resources.getString(R.string.rs));
            txtTotalIncome.setVisibility(View.INVISIBLE);

            layout_inchead.setVisibility(View.INVISIBLE);
            recyclerincome.setVisibility(View.INVISIBLE);
            imgdownload.setVisibility(View.INVISIBLE);
        }

        checkExpenseHeads();

      }


    private void checkExpenseHeads() {


        ledgerAccountsexpense.clear();

        List<Accounts> accountsSorted = accountsbydate;

        List<Accounts>accstring=new ArrayList<>();

        for (Accounts acc:accountsSorted) {

            if(!isAccountAlreadyExist(accstring,acc.getACCOUNTS_setupid()))
            {
                accstring.add(acc);
            }

        }

        String selected_date = startdate;

        try {

            for (CommonData cm : cmfiltered_expense) {

                JSONObject jobj=new JSONObject(cm.getData());

                String amount=jobj.getString("Amount");
                double d=Double.parseDouble(amount);
                // Log.e("Amount",d+"");


                if(d>0)
                {


                    double openingbalance = 0;

                    String Type="";

                    if (!cm.getData().equalsIgnoreCase("")) {

                        JSONObject jsonObject = new JSONObject(cm.getData());

                        openingbalance = Double.parseDouble(jsonObject.getString("Amount"));

                        Type=jsonObject.getString("Type");


                    } else {

                        openingbalance = 0;
                    }

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        openingbalance=openingbalance*-1;
                    }





                    double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

//                    double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");

                    double finalclosingbalance = 0;

                    finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                    LedgerAccount ledgerAccount = new LedgerAccount();
                    ledgerAccount.setAccountheadid(cm.getId());
                    ledgerAccount.setAccountheadname("");
                    ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate+"");

                    if(closingbalancebeforedate<0)
                    {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                    }
                    else {

                        ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                    }





                    if(finalclosingbalance<0) {

                        ledgerAccount.setDebitorcredit(Utils.Cashtype.credit+"");

                    }
                    else {
                        ledgerAccount.setDebitorcredit(Utils.Cashtype.debit+"");
                    }
                    ledgerAccount.setClosingbalance(finalclosingbalance + "");

                    ledgerAccountsexpense.add(ledgerAccount);

                }









                for (Accounts acc:accstring) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(cm.getId())) {


                        double openingbalance = 0;

                        String Type="";


                        if (!cm.getData().equalsIgnoreCase("")) {

                            JSONObject jsonObject = new JSONObject(cm.getData());

                            openingbalance = Double.parseDouble(jsonObject.getString("Amount"));
                            Type=jsonObject.getString("Type");

                        } else {

                            openingbalance = 0;
                        }

                        if(Type.equalsIgnoreCase("Credit"))
                        {
                            openingbalance=openingbalance*-1;
                        }






                        double closingbalancebeforedate = getClosingbalancebeforedate(openingbalance, cm.getId(), endate, "Income");

                        // double finalclosingbalance = getFinalClosingBalance(month_selected, yearselected, cm.getId(), "Income");
                        double finalclosingbalance = 0;


                        finalclosingbalance = finalclosingbalance + closingbalancebeforedate;

                        if(!isAccountExist(ledgerAccountsexpense,cm.getId())) {


                            LedgerAccount ledgerAccount = new LedgerAccount();
                            ledgerAccount.setAccountheadid(cm.getId());
                            ledgerAccount.setAccountheadname("");
                            ledgerAccount.setClosingbalancebeforedate(closingbalancebeforedate + "");

                            if(closingbalancebeforedate<0)
                            {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.credit+"");
                            }
                            else {

                                ledgerAccount.setDebitcreditopening(Utils.Cashtype.debit+"");

                            }

                            if (finalclosingbalance<0) {

                                ledgerAccount.setDebitorcredit(Utils.Cashtype.credit + "");

                            } else {
                                ledgerAccount.setDebitorcredit(Utils.Cashtype.debit + "");
                            }
                            ledgerAccount.setClosingbalance(finalclosingbalance + "");

                            ledgerAccountsexpense.add(ledgerAccount);
                        }

                    }

                    // }
                }
//
            }
//

        } catch (Exception e) {

        }


        if(ledgerAccountsexpense.size()>0)
        {

            for (LedgerAccount ledgerAccount:ledgerAccountsexpense) {

                totalexp=totalexp+Double.parseDouble(ledgerAccount.getClosingbalance());



            }
            txtTotalExpense.setText("Total Expense : "+totalexp+" "+resources.getString(R.string.rs));

            layout_headexpense.setVisibility(View.VISIBLE);
            recyclerExpense.setVisibility(View.VISIBLE);
            imgdownload.setVisibility(View.VISIBLE);

           // txtStatement.setVisibility(View.VISIBLE);

            recyclerExpense.setLayoutManager(new LinearLayoutManager(IncomexpenditureActivity.this));
            recyclerExpense.setAdapter(new LedgerAccountAdapter(IncomexpenditureActivity.this, ledgerAccountsexpense,startdate,endate));

        }
        else {

            txtTotalExpense.setText("Total Expense : "+0+" "+resources.getString(R.string.rs));

            layout_headexpense.setVisibility(View.INVISIBLE);
            txtTotalExpense.setVisibility(View.INVISIBLE);
            recyclerExpense.setVisibility(View.INVISIBLE);
            imgdownload.setVisibility(View.INVISIBLE);
        }



    }


    private boolean isAccountExist(List<LedgerAccount>accounts,String setupid)
    {

        boolean a=false;
        for (LedgerAccount acc:accounts) {

            if(acc.getAccountheadid().equalsIgnoreCase(setupid))
            {

                a=true;

                break;
            }


        }

        return a;


    }



//    private double getFinalClosingBalance(String month,String year1,String id,String type)
//    {
//        double finalclosingbalance=0;
//
//        List<Accounts> accountsSorted = Utils.sortAccountByDate(IncomexpenditureActivity.this);
//
//
//
//        for (Accounts accounts : accountsSorted) {
//
//
//
//            if (id.equalsIgnoreCase(accounts.getACCOUNTS_setupid())) {
//
//            if (accounts.getACCOUNTS_year().equalsIgnoreCase(year1) && accounts.getACCOUNTS_month().equalsIgnoreCase(month)) {
//
//
//                if(!id.equalsIgnoreCase("0")) {
//
//
//                    if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {
//
//                        finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());
//
//
//                    } else if (accounts.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {
//
//                        finalclosingbalance = finalclosingbalance - Double.parseDouble(accounts.getACCOUNTS_amount());
//
//
//                    }
//
//                }
//                else {
//
//
//                    if(type.equalsIgnoreCase("Income"))
//                    {
//                        finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());
//
//                    }
//                    else {
//
//                        finalclosingbalance = finalclosingbalance - Double.parseDouble(accounts.getACCOUNTS_amount());
//
//                    }
//                }
//
//
//              //  finalclosingbalance = finalclosingbalance + Double.parseDouble(accounts.getACCOUNTS_amount());
//
//
//                    // incomeAccount.add(accounts);
//
//                }
//
//
//            }
//        }
//
//
//
//
//
//
//        return finalclosingbalance;
//
//
//    }


















    private double getClosingbalancebeforedate(double openingbalance, String id, String selected_date,String type) {

        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(IncomexpenditureActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


               if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());


                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());


                        }


                    }
                }
//                else if(id.equalsIgnoreCase("0")&&acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {
//
//
//                    if(type.equalsIgnoreCase("Income"))
//                    {
//                        openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//                    else {
//
//                        openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
//
//                    }
//
//
//
//
//                }


            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;
        }

        return closingbalancebeforemonth;
    }


}
