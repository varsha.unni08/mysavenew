package com.centroid.integraaccounts.views;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.adapter.UPIListAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.data.domain.UPIapps;
import com.centroid.integraaccounts.dialogs.SpinnerDialogInvestmentFragment;
import com.centroid.integraaccounts.fragments.SpinnerDialogFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

public class AddPaymentVoucherActivity extends AppCompatActivity {

    ImageView imgback, imgDatepick, imgdelete;
    TextView txtdatepick,txtHead,txtSpinner;
    Spinner spinnerAccountName, spinnerAccounttype, spinnerBankdata;
    EditText edtAmount, edtvoucher, edtremarks;
    Button btnSave,btndelete;

    String date = "", month_selected = "", yearselected = "";

    Accounts paymentVoucher;

    LinearLayout layout_bankdata;
    List<CommonData> cmfiltered = new ArrayList<>();
    List<CommonData> commonDataListfiltered = new ArrayList<>();
    FloatingActionButton fabadd, fabaddbank;
    String bankid = "0", bankname = "", debitaccountid = "0";


    Thread thread=null;

    Handler handler;

    Resources resources;
    TextView txtwarning;

    Button btnbudget;

    int index=0;

    double total = 0;
    double totalbudget = 0;
    double paidamount=0;

    int generatebill=0;

    RadioButton rbCash,rbBank;

    String newAccount="";
    Dialog dialog=null;

    CommonData commonData_selected=null;

    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    String BHIM_UPI = "in.org.npci.upiapp";
    String GOOGLE_PAY = "com.google.android.apps.nbu.paisa.user";
    String PHONE_PE = "com.phonepe.app";
    String PAYTM = "net.one97.paytm";
    String FREECHARGE="com.freecharge.android";
//    String FREECHARGE="com.freecharge.android";

    String upiapps[]={BHIM_UPI,GOOGLE_PAY,PHONE_PE,PAYTM,FREECHARGE};
    String upiappsname[]={"Bhim","Google pay","Phone pe","Pay TM","Free Charge"};
    int icons[]={R.drawable.bhim,R.drawable.gpay,R.drawable.phonepe,R.drawable.paytm,R.drawable.freecharge};

    int fromReceipt=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_voucher);
        getSupportActionBar().hide();
        handler=new Handler();

        cmfiltered = new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        rbBank=findViewById(R.id.rbBank);
        rbCash=findViewById(R.id.rbCash);

        paymentVoucher = (Accounts) getIntent().getSerializableExtra("paymentVouchers");

        fromReceipt=getIntent().getIntExtra("fromReceipt",0);


        imgback = findViewById(R.id.imgback);
        txtSpinner=findViewById(R.id.txtSpinner);
        imgDatepick = findViewById(R.id.imgDatepick);
        fabadd = findViewById(R.id.fabadd);
        fabaddbank = findViewById(R.id.fabaddbank);
        imgdelete = findViewById(R.id.imgdelete);
        txtdatepick = findViewById(R.id.txtdatepick);
        layout_bankdata = findViewById(R.id.layout_bankdata);
        txtwarning=findViewById(R.id.txtwarning);

        spinnerAccountName = findViewById(R.id.spinnerAccountName);
        spinnerAccounttype = findViewById(R.id.spinnerAccounttype);
        spinnerBankdata = findViewById(R.id.spinnerBankdata);
        edtAmount = findViewById(R.id.edtAmount);
        btndelete=findViewById(R.id.btndelete);

        edtvoucher = findViewById(R.id.edtvoucher);
        edtremarks = findViewById(R.id.edtremarks);
        btnSave = findViewById(R.id.btnSave);
        btnbudget=findViewById(R.id.btnbudget);


        String languagedata = LocaleHelper.getPersistedData(AddPaymentVoucherActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddPaymentVoucherActivity.this, languagedata);

        resources=context.getResources();
        edtAmount.setHint(resources.getString(R.string.amount));
        edtremarks.setHint(resources.getString(R.string.enterremarks));
        txtdatepick.setText(resources.getString(R.string.selectdate));
        btnSave.setText(resources.getString(R.string.save));
        txtHead.setText(resources.getString(R.string.addpaymentvoucher));
        txtwarning.setText(resources.getString(R.string.totalexceedbudget));
        txtSpinner.setText(resources.getString(R.string.selectanaccount));
        rbBank.setText(resources.getString(R.string.bank));
        rbCash.setText(resources.getString(R.string.cashreal));
        btnbudget.setText(resources.getString(R.string.setbudget));

        String arr[]=resources.getStringArray(R.array.paymentacc);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerAccounttype.setAdapter(spinnerArrayAdapter);



        edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                compareWithBudget();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });










        addAccountSettings();
        addBankData();

        fabaddbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(AddPaymentVoucherActivity.this, "Add account setup in Bank account category", Toast.LENGTH_SHORT).show();
//
//                startActivity(new Intent(AddPaymentVoucherActivity.this, AccountsettingsActivity.class));
                Intent i=new Intent(AddPaymentVoucherActivity.this, AccountsettingsActivity.class);

                if(index==1) {
                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsBankRequestcode);

                }
                else if(index==0) {

                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsCashRequestcode);

                }

                startActivityForResult(i,Utils.Requestcode.ForAccountSettingsBankRequestcode);

            }
        });

        btnbudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(AddPaymentVoucherActivity.this, BudgetListActivity.class));


            }
        });


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             //   Toast.makeText(AddPaymentVoucherActivity.this, "Add account setup in Expense account category", Toast.LENGTH_SHORT).show();

                if(fromReceipt==1)
                {
                    Intent i = new Intent(AddPaymentVoucherActivity.this, AccountsettingsActivity.class);
                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingsinvestmentRequestcode);
                    startActivityForResult(i, Utils.Requestcode.ForAccountSettingsinvestmentRequestcode);
                }
                else {

                    Intent i = new Intent(AddPaymentVoucherActivity.this, AccountsettingsActivity.class);
                    i.putExtra(Utils.Requestcode.AccVoucherAll, Utils.Requestcode.ForAccountSettingswithoutRequestcode);
                    startActivityForResult(i, Utils.Requestcode.ForAccountSettingswithoutRequestcode);
                }

            }
        });


        rbBank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b) {
                    index = 1;

                    addBankData();
                }
            }
        });


        rbCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                index = 0;
                addBankData();
            }
            }
        });
        layout_bankdata.setVisibility(View.VISIBLE);


//        spinnerAccounttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                if (i==0) {
//                    layout_bankdata.setVisibility(View.VISIBLE);
//                    index=0;
//                } else if (i==1) {
//                    layout_bankdata.setVisibility(View.VISIBLE);
//                    index=1;
//
//                  //  addBankData();
//                }
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fromReceipt==1)
                {

                    SpinnerDialogInvestmentFragment spinnerDialogFragment = new SpinnerDialogInvestmentFragment(true, new AccountSetupEventListener() {
                        @Override
                        public void getSelectedAccountSetup(CommonData commonData) {

                            try {

                                commonData_selected = commonData;

                                JSONObject jcmn1 = new JSONObject(commonData.getData());
                                String acctype = jcmn1.getString("Accountname");

                                txtSpinner.setText(Utils.getCapsSentences(context, acctype));

                                txtwarning.setVisibility(View.GONE);


                            } catch (Exception e) {

                            }


                        }
                    });


                    spinnerDialogFragment.show(getSupportFragmentManager(), "skjdf");


                }
                else {

                    SpinnerDialogFragment spinnerDialogFragment = new SpinnerDialogFragment(false, new AccountSetupEventListener() {
                        @Override
                        public void getSelectedAccountSetup(CommonData commonData) {

                            try {

                                commonData_selected = commonData;

                                JSONObject jcmn1 = new JSONObject(commonData.getData());
                                String acctype = jcmn1.getString("Accountname");

                                txtSpinner.setText(Utils.getCapsSentences(context, acctype));

                                txtwarning.setVisibility(View.GONE);


                            } catch (Exception e) {

                            }


                        }
                    });


                    spinnerDialogFragment.show(getSupportFragmentManager(), "skjdf");
                }


            }
        });


        spinnerAccountName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                compareWithBudget();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });







        addBankData();







        Executor executor=new DirectExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {

                setEditMode();

            }
        });





        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (paymentVoucher != null) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(AddPaymentVoucherActivity.this);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                            dialogInterface.dismiss();


                            Executor executor=new DirectExecutor();

                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                  //  updateData();


                                    try {


                                        if(paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash+"")) {

                                            new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(debitaccountid + "");
                                            Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                                            //  new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                                        }

                                        else  if(paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.bank+"")) {


                                            //  new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                                            new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(debitaccountid + "");

                                            Utils.playSimpleTone(AddPaymentVoucherActivity.this);

                                        }
                                        else {

                                            // new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                            new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");
                                            Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                                            List<Accounts>accounts2=new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id()+"");


                                            if(accounts2.size()>0) {

                                                new DatabaseHelper(AddPaymentVoucherActivity.this).deleteAccountDataById(accounts2.get(0).getACCOUNTS_id()+"");
                                                Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                                            }

                                        }



                                        onBackPressed();

                                    }catch (Exception e)
                                    {

                                    }







                                }
                            });




















                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();
                }

            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();

            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!date.equalsIgnoreCase("")) {


                    if (!edtAmount.getText().toString().equalsIgnoreCase("")) {


                            try {

                                if (paymentVoucher != null) {


                                    Executor executor=new DirectExecutor();

                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateData();
                                        }
                                    });



                                } else {


                                    Executor executor=new DirectExecutor();

                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            addData();
                                        }
                                    });

                                }


                            } catch (Exception e) {


                            }




                    } else {

                       // Toast.makeText(AddPaymentVoucherActivity.this, resources.getString(R.string.amount), Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(AddPaymentVoucherActivity.this, resources.getString(R.string.amount), new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }


                } else {

                  //  Toast.makeText(AddPaymentVoucherActivity.this, resources.getString(R.string.selectdate), Toast.LENGTH_SHORT).show();

                    Utils.showAlertWithSingle(AddPaymentVoucherActivity.this, resources.getString(R.string.selectdate), new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }


            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==Utils.Requestcode.ForAccountSettingswithoutRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

             newAccount=data.getStringExtra("AccountAdded");

            addAccountSettings();

        }
        else   if(requestCode==Utils.Requestcode.ForAccountSettingsBankRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addBankData();

        }
        else   if(requestCode==Utils.Requestcode.ForAccountSettingsCashRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addBankData();

        }

        if(requestCode==Utils.Requestcode.ForAccountSettingsinvestmentRequestcode&&data!=null&&resultCode==RESULT_OK)
        {

            newAccount=data.getStringExtra("AccountAdded");

            addAccountSettings();

        }



    }

    public boolean compareWithBudget()
    {

       boolean isover=false;

        try {

             total = 0;
             totalbudget = 0;

            int m=Integer.parseInt(month_selected)-1;

            String monthinarray=arrmonth[m];
            CommonData data = commonData_selected;


            List<Accounts> paymentVouchers = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataByVouchertypeAndSetupid(Utils.VoucherType.paymentvoucher + "", data.getId());

            // Log.e("CompareTAG",paymentVouchers.size()+"");

            if (paymentVouchers.size() > 0) {


                for (Accounts paymentVoucher : paymentVouchers
                ) {

                    if (paymentVoucher.getACCOUNTS_month().equalsIgnoreCase(month_selected) && paymentVoucher.getACCOUNTS_year().equalsIgnoreCase(yearselected)) {

                        String amount = paymentVoucher.getACCOUNTS_amount();

                        total = total + Double.parseDouble(amount);
                    }


                }


            }


            if(!edtAmount.getText().toString().equalsIgnoreCase(""))
            {
                total=total+Double.parseDouble(edtAmount.getText().toString());
            }
            else {
                total=total+0;
            }







            List<Budget> budgets = new DatabaseHelper(AddPaymentVoucherActivity.this).getBudgetData();

            if (budgets.size() > 0) {


                for (Budget budget : budgets) {

                    JSONObject jsonObject = new JSONObject(budget.getData());


                    String year1 = jsonObject.getString("year");
                    String amount = jsonObject.getString("amount");
                    String month = jsonObject.getString("month");
                    String accountname = jsonObject.getString("accountname");

                  //  CommonData cm = (CommonData) spinnerAccountName.getSelectedItem();

                    if(accountname.trim().equalsIgnoreCase(data.getId().trim())) {


                        if (year1.equalsIgnoreCase(String.valueOf(yearselected)) && monthinarray.equalsIgnoreCase(month)) {

                            btnbudget.setVisibility(View.GONE);
                            totalbudget = Double.parseDouble(amount);

                           // break;


                        }
                    }


                }

                if(totalbudget==0)
                {
                    txtwarning.setVisibility(View.GONE);
                    isover=false;
                }


               else if(total>totalbudget)
                {

                    txtwarning.setVisibility(View.VISIBLE);
                    isover=true;
                }
                else {

                    txtwarning.setVisibility(View.GONE);
                    isover=false;
                }


            }


        }catch (Exception e)
        {

        }


        return isover;
    }




    public void addData()
    {

       // if(compareWithBudget()) {

        CommonData cm = commonData_selected;

        CommonData cm_bank = (CommonData) spinnerBankdata.getSelectedItem();

        if(cm_bank!=null) {


            Accounts accounts = new Accounts();

            //entry id is equal to zero in the base of accounts section from account setup
            accounts.setACCOUNTS_entryid("0");
            accounts.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
            accounts.setACCOUNTS_date(date);
            accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
            accounts.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
            accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts.setACCOUNTS_month(month_selected);
            accounts.setACCOUNTS_year(yearselected);
            accounts.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
            accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account + "");


            long insertid = new DatabaseHelper(AddPaymentVoucherActivity.this).addAccountsData(accounts);
            Utils.playSimpleTone(AddPaymentVoucherActivity.this);

            if (index == 0) {


                Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
                accounts1.setACCOUNTS_entryid(insertid + "");
                accounts1.setACCOUNTS_date(date);
                //setupid must be zero in the base of cash
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.cash + "");

                new DatabaseHelper(AddPaymentVoucherActivity.this).addAccountsData(accounts1);
                Utils.playSimpleTone(AddPaymentVoucherActivity.this);

            } else {

////bank section
                Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
                accounts1.setACCOUNTS_entryid(insertid + "");
                accounts1.setACCOUNTS_date(date);
                //setupid must be bank account id
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
                accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank + "");

                paidamount=Double.parseDouble(edtAmount.getText().toString());

                new DatabaseHelper(AddPaymentVoucherActivity.this).addAccountsData(accounts1);
                Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                listUPIApps();

            }


            date = "";
            month_selected = "";
            yearselected = "";
            spinnerAccountName.setSelection(0);
            edtAmount.setText("");
            edtvoucher.setText("");
            edtremarks.setText("");
            txtdatepick.setText("Select date");
            spinnerAccounttype.setSelection(0);
            rbCash.setChecked(true);
            setDate();

        }
        else {

           // Toast.makeText(AddPaymentVoucherActivity.this,"Please select any cash/bank account head",Toast.LENGTH_SHORT).show();

            Utils.showAlertWithSingle(AddPaymentVoucherActivity.this,"Please select any cash/bank account head", new DialogEventListener() {
                @Override
                public void onPositiveButtonClicked() {

                }

                @Override
                public void onNegativeButtonClicked() {

                }
            });




        }
       // }
//        Uri uri = Uri.parse("upi://pay").buildUpon()
//
//                .build();
//
//
//        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
//        upiPayIntent.setData(uri);
//        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");
//
//         check if intent resolves
//        if(null != chooser.resolveActivity(getPackageManager())) {
//            startActivityForResult(chooser, 10);
//        } else {
//            Toast.makeText(this,"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
//        }
    }


    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Here, no request code
                        Intent data = result.getData();
                        //doSomeOperations();
                    }
                }
            });

public void listUPIApps()
{

//    Uri uri = Uri.parse("upi://pay").buildUpon()
//            .appendQueryParameter("pa", "")
//            .appendQueryParameter("pn", "dhh")
//            .appendQueryParameter("tn", edtremarks.getText().toString())
//            .appendQueryParameter("am", edtAmount.getText().toString())
//
//
//            .build();
//
//
//    Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
//    upiPayIntent.setData(uri);
//    Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");
//
//    if(null != chooser.resolveActivity(getPackageManager())) {
////        registerForActivityResult(chooser, 10);
//
//        someActivityResultLauncher.launch(chooser);
//    } else {
//        Toast.makeText(this,"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
//    }






    List<UPIapps>upIapps=new ArrayList<>();
    for (int i=0;i<upiapps.length;i++)
    {
        UPIapps upIapps1=new UPIapps();
        upIapps1.setName(upiappsname[i]);
        upIapps1.setPackagename(upiapps[i]);
        upIapps1.setIcon(icons[i]);

        upIapps.add(upIapps1);
    }
    DisplayMetrics displayMetrics = new DisplayMetrics();
   getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    int height = displayMetrics.heightPixels-100;
    int width = displayMetrics.widthPixels-100;


   dialog=new Dialog(AddPaymentVoucherActivity.this);
    dialog.setTitle("Pay with");
    dialog.setContentView(R.layout.layout_listupi);

    RecyclerView recycler=dialog.findViewById(R.id.recycler);
    recycler.setLayoutManager(new LinearLayoutManager(AddPaymentVoucherActivity.this));
    recycler.setAdapter(new UPIListAdapter(AddPaymentVoucherActivity.this,upIapps));

    dialog.getWindow().setLayout(width, height);


    dialog.show();


}


public void selectUpiApp(UPIapps upIapps)
{
    if(dialog!=null)
    {
        dialog.dismiss();
    }

    try {

        PackageManager manager = getPackageManager();

       // Intent intent = manager.getLaunchIntentForPackage(upIapps.getPackagename());

        Intent intent = getPackageManager().getLaunchIntentForPackage(upIapps.getPackagename());
        if (intent != null) {
            // We found the activity now start the activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + upIapps.getPackagename()));
            startActivity(intent);
        }

//        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//        startActivity(intent);
//        Uri uri =
//                new Uri.Builder()
//                        .scheme("upi")
//                        .authority("pay")
//                        .appendQueryParameter("tr", "24111321221")
//                        .appendQueryParameter("tn", "payment voucher")
//                        .appendQueryParameter("am", paidamount+"")
//                        .appendQueryParameter("cu", "INR")
//
//                        .build();
//
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//
//        intent.setPackage(upIapps.getPackagename());
//  startActivityForResult(intent, 789);

    }catch (Exception e)
    {
//        Utils.showAlertWithSingle(AddPaymentVoucherActivity.this, "The selected application not found", new DialogEventListener() {
//            @Override
//            public void onPositiveButtonClicked() {
//
//            }
//
//            @Override
//            public void onNegativeButtonClicked() {
//
//            }
//        });

        Utils.showAlertWithSingle(AddPaymentVoucherActivity.this, e.toString()+","+e.getLocalizedMessage(), new DialogEventListener() {
            @Override
            public void onPositiveButtonClicked() {

            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
    }

}









public void updateData()
{
    long insertid=0l;

    CommonData cm = commonData_selected;

    CommonData cm_bank = (CommonData) spinnerBankdata.getSelectedItem();

    if(cm_bank!=null) {

        Accounts accounts = new Accounts();

        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());
        accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
        accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account + "");


        insertid = new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(debitaccountid + "", accounts);


        if (index == 0) {

            Accounts accounts1 = new Accounts();

            accounts1.setACCOUNTS_entryid(debitaccountid + "");
            accounts1.setACCOUNTS_date(date);

            accounts1.setACCOUNTS_setupid(cm_bank.getId());
            accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
            accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts1.setACCOUNTS_month(month_selected);
            accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
            accounts1.setACCOUNTS_year(yearselected);
            accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.cash + "");

            if (paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash + "")) {


                new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                Utils.playSimpleTone(AddPaymentVoucherActivity.this);


            } else if (paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.bank + "")) {


                new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                Utils.playSimpleTone(AddPaymentVoucherActivity.this);

            } else {


                List<Accounts> accounts2 = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");


                if (accounts2.size() > 0) {

                    new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(accounts2.get(0).getACCOUNTS_id() + "", accounts1);
                    Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                }

            }


        } else {

            Accounts accounts1 = new Accounts();

            accounts1.setACCOUNTS_entryid(debitaccountid + "");
            accounts1.setACCOUNTS_date(date);
            accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.paymentvoucher);
            accounts1.setACCOUNTS_setupid(cm_bank.getId());
            accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
            accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts1.setACCOUNTS_month(month_selected);
            accounts1.setACCOUNTS_year(yearselected);
            accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank + "");


            if (paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash + "")) {


                new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);
                Utils.playSimpleTone(AddPaymentVoucherActivity.this);
            } else if (paymentVoucher.getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.bank + "")) {


                new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);
                Utils.playSimpleTone(AddPaymentVoucherActivity.this);
            } else {
                List<Accounts> accounts2 = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");


                if (accounts2.size() > 0) {

                    new DatabaseHelper(AddPaymentVoucherActivity.this).updateAccountsData(accounts2.get(0).getACCOUNTS_id() + "", accounts1);
                    Utils.playSimpleTone(AddPaymentVoucherActivity.this);
                }
            }


        }


        date = "";
        month_selected = "";
        yearselected = "";
        spinnerAccountName.setSelection(0);
        edtAmount.setText("");
        edtvoucher.setText("");
        edtremarks.setText("");
        spinnerAccounttype.setSelection(0);
        rbCash.setChecked(true);

        listUPIApps();

//        Uri uri = Uri.parse("upi://pay").buildUpon()
//
//                .build();
//
//
//        Intent upiPayIntent = new Intent(Intent.ACTION_VIEW);
//        upiPayIntent.setData(uri);
//        Intent chooser = Intent.createChooser(upiPayIntent, "Pay with");
//
//        // check if intent resolves
//        if(null != chooser.resolveActivity(getPackageManager())) {
//            startActivityForResult(chooser, 10);
//        } else {
//            Toast.makeText(this,"No UPI app found, please install one to continue",Toast.LENGTH_SHORT).show();
//        }
       // onBackPressed();
    }
    else {

      //  Toast.makeText(AddPaymentVoucherActivity.this,"Please select any cash/bank account head",Toast.LENGTH_SHORT).show();


        Utils.showAlertWithSingle(AddPaymentVoucherActivity.this,"Please select any cash/bank account head", new DialogEventListener() {
            @Override
            public void onPositiveButtonClicked() {

            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });



    }
}










    public void setEditMode() {

        if (paymentVoucher != null) {

            try {


                txtHead.setText(resources.getString(R.string.editpaymentvoucher));
                btnSave.setText(resources.getString(R.string.update));
                btndelete.setText(resources.getString(R.string.delete));

                String month = paymentVoucher.getACCOUNTS_month();
                String year1 = paymentVoucher.getACCOUNTS_year();
                String dat = paymentVoucher.getACCOUNTS_date();
                String amount = paymentVoucher.getACCOUNTS_amount();
                String accountname = paymentVoucher.getACCOUNTS_setupid();
                String accounttype = paymentVoucher.getACCOUNTS_type();
                String remarks = paymentVoucher.getACCOUNTS_remarks();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                String cashbank=paymentVoucher.getACCOUNTS_cashbanktype();



                month_selected = month;
                yearselected = year1;
                date = dat;
                btndelete.setVisibility(View.VISIBLE);
                txtdatepick.setText(date);
                edtAmount.setText(amount);

                edtremarks.setText(remarks);



                if (cashbank.equalsIgnoreCase(Utils.CashBanktype.bank+"")) {

                    List<Accounts> cmd = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataBYid(paymentVoucher.getACCOUNTS_entryid());
                    if (cmd.size() > 0) {
                        accountname = cmd.get(0).getACCOUNTS_setupid();
                        debitaccountid = cmd.get(0).getACCOUNTS_id() + "";
                    }

                   spinnerAccounttype.setSelection(1);
                    rbBank.setChecked(true);
                    rbCash.setChecked(false);

                    bankid = paymentVoucher.getACCOUNTS_setupid();


                }
                else if(cashbank.equalsIgnoreCase(Utils.CashBanktype.account+"")) {

                    List<Accounts> bankdata = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");
                    debitaccountid = paymentVoucher.getACCOUNTS_id() + "";
                    accountname = paymentVoucher.getACCOUNTS_setupid();

                    if (bankdata.size() > 0) {
                        accountname = paymentVoucher.getACCOUNTS_setupid();
                        debitaccountid = paymentVoucher.getACCOUNTS_id() + "";

                       // spinnerAccounttype.setSelection(0);

                        bankid = bankdata.get(0).getACCOUNTS_setupid();

                        if(bankdata.get(0).getACCOUNTS_cashbanktype().equalsIgnoreCase(Utils.CashBanktype.cash+""))
                        {
                            spinnerAccounttype.setSelection(0);

                            rbBank.setChecked(false);
                            rbCash.setChecked(true);
                        }
                        else {

                            spinnerAccounttype.setSelection(1);

                            rbBank.setChecked(true);
                            rbCash.setChecked(false);
                        }


                    }

                    else {

                        spinnerAccounttype.setSelection(0);

                        rbBank.setChecked(false);
                        rbCash.setChecked(true);
                    }
                }

                else if(cashbank.equalsIgnoreCase(Utils.CashBanktype.cash+"")) {

                    List<Accounts> cmd = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountsDataBYid(paymentVoucher.getACCOUNTS_entryid());
                    if (cmd.size() > 0) {
                        accountname = cmd.get(0).getACCOUNTS_setupid();
                        debitaccountid = cmd.get(0).getACCOUNTS_id() + "";
                    }

                    spinnerAccounttype.setSelection(0);
                    rbBank.setChecked(false);
                    rbCash.setChecked(true);

                    bankid = paymentVoucher.getACCOUNTS_setupid();

                }



                List<CommonData> cm = new DatabaseHelper(AddPaymentVoucherActivity.this).getDataByID(accountname, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                CommonData cmselected = cm.get(0);





                Log.e("Paymentvoucherid",paymentVoucher.getACCOUNTS_id()+"");
                Log.e("debitaccountid",debitaccountid+"");






                for (int i = 0; i < cmfiltered.size(); i++) {
                    if (cmfiltered.get(i).getId().equalsIgnoreCase(cmselected.getId())) {

                        commonData_selected=cmfiltered.get(i);

                        try {


                            JSONObject jcmn1 = new JSONObject(commonData_selected.getData());
                            String acctype = jcmn1.getString("Accountname");
                            txtSpinner.setText(acctype);

                        }catch (Exception e)
                        {

                        }

                       // spinnerAccountName.setSelection(i);
                        break;
                    }
                }











                for (int i = 0; i < commonDataListfiltered.size(); i++) {

                    if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                        spinnerBankdata.setSelection(i);
                        break;
                    }

                }

















            } catch (Exception e) {

            }


        }
        else {

            setDate();
        }
    }

    public void showDatePicker() {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(AddPaymentVoucherActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m = i1 + 1;
                month_selected = m + "";
                yearselected = i + "";

                date = i2 + "-" + m + "-" + i;

                txtdatepick.setText(i2 + "-" + m + "-" + i);


            }
        }, year, month, dayOfMonth);

        datePickerDialog.show();
    }


    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);
    }








    @Override
    protected void onRestart() {
        super.onRestart();

        addAccountSettings();
        addBankData();
    }

    public void addAccountSettings() {

        cmfiltered.clear();

        List<CommonData> commonData = new DatabaseHelper(AddPaymentVoucherActivity.this).getAccountSettingsData();


        if(fromReceipt==1)
        {

            List<CommonData> commonData1=new ArrayList<>();
            for(int i=0;i<commonData.size();i++)
            {
                CommonData cm=commonData.get(i);
                String Accounttype="";
                try{
                    JSONObject jsonObject=new JSONObject(cm.getData());

                    Accounttype=jsonObject.getString("Accounttype");

                    if(Accounttype.compareTo("Investment")==0)
                    {
                        commonData1.add(cm);
                    }



                }catch (Exception e)
                {

                }

            }


            Collections.sort(commonData1, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a = 0;

                    try {


                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype = jcmn1.getString("Accountname");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1 = j1.getString("Accountname");


                        a = acctype.compareToIgnoreCase(acctypej1);

                    } catch (Exception e) {

                    }


                    return a;
                }
            });

            cmfiltered.addAll(commonData1);

            for(int i=0;i<cmfiltered.size();i++)
            {

                if(i==0) {

                    CommonData cd1 = cmfiltered.get(i);

                    commonData_selected = cd1;

                    try {


                        JSONObject jcmn1 = new JSONObject(cd1.getData());
                        String acctype = jcmn1.getString("Accountname");
                        txtSpinner.setText(acctype);





                    } catch (Exception e) {

                    }


                }






            }



            AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddPaymentVoucherActivity.this, cmfiltered);
            spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);
        }
        else {

            if (commonData.size() > 0) {

                Collections.sort(commonData, new Comparator<CommonData>() {
                    @Override
                    public int compare(CommonData commonData, CommonData t1) {

                        int a = 0;

                        try {


                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = jcmn1.getString("Accountname");
                            JSONObject j1 = new JSONObject(t1.getData());
                            String acctypej1 = j1.getString("Accountname");

                            a = acctype.compareToIgnoreCase(acctypej1);

                        } catch (Exception e) {

                        }


                        return a;
                    }
                });

                cmfiltered.addAll(commonData);


                AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddPaymentVoucherActivity.this, cmfiltered);
                spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);


                if (newAccount != null) {
                    if (!newAccount.equalsIgnoreCase("")) {

                        for (int i = 0; i < cmfiltered.size(); i++) {
                            try {
                                JSONObject jcmn1 = new JSONObject(cmfiltered.get(i).getData());
                                String acctype = jcmn1.getString("Accountname");

                                if (acctype.trim().equalsIgnoreCase(newAccount.trim())) {

                                    spinnerAccountName.setSelection(i);
                                    break;
                                }


                            } catch (Exception e) {

                            }


                        }

                    }
                }


            }

        }


    }


    public void addBankData() {


        if(index==1) {


            commonDataListfiltered.clear();
            List<CommonData> commonDataList = new DatabaseHelper(AddPaymentVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


            try {

                if (commonDataList.size() > 0) {

                    for (CommonData commonData : commonDataList) {


                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        if (jsonObject.getString("Accounttype").equalsIgnoreCase("Bank")) {
                            // holder.txtmonth.setText(jsonObject.getString("Type"));

                            commonDataListfiltered.add(commonData);
                        }

                    }

                }


               // if (commonDataListfiltered.size() > 0) {

                    BankSpinnerAdapter bankSpinnerAdapter = new BankSpinnerAdapter(AddPaymentVoucherActivity.this, commonDataListfiltered);
                    spinnerBankdata.setAdapter(bankSpinnerAdapter);


                    if (paymentVoucher != null) {


                        for (int i = 0; i < commonDataListfiltered.size(); i++) {

                            if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                                spinnerBankdata.setSelection(i);
                                break;
                            }

                        }


                    }

                if(!newAccount.equalsIgnoreCase("")) {

                    for (int i = 0; i < commonDataListfiltered.size(); i++) {
                        try {
                            JSONObject jcmn1 = new JSONObject(commonDataListfiltered.get(i).getData());
                            String acctype= jcmn1.getString("Accountname");

                            if(acctype.trim().equalsIgnoreCase(newAccount.trim()))
                            {

                                spinnerBankdata.setSelection(i);
                                break;
                            }



                        } catch (Exception e) {

                        }


                    }

                }


//                } else {
//
//                    //  layout_bankdata.setVisibility(View.GONE);
//
//
//                    // Toast.makeText(AddPaymentVoucherActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
//
//
//                }

            } catch (Exception e) {

            }
        }
        else {


            commonDataListfiltered.clear();
            List<CommonData> commonDataList = new DatabaseHelper(AddPaymentVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


            try {

                if (commonDataList.size() > 0) {

                    for (CommonData commonData : commonDataList) {


                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        if (jsonObject.getString("Accounttype").equalsIgnoreCase("Cash")) {
                            // holder.txtmonth.setText(jsonObject.getString("Type"));

                            commonDataListfiltered.add(commonData);
                        }

                    }

                }


              //  if (commonDataListfiltered.size() > 0) {

                    BankSpinnerAdapter bankSpinnerAdapter = new BankSpinnerAdapter(AddPaymentVoucherActivity.this, commonDataListfiltered);
                    spinnerBankdata.setAdapter(bankSpinnerAdapter);


                    if (paymentVoucher != null) {


                        for (int i = 0; i < commonDataListfiltered.size(); i++) {

                            if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(bankid)) {

                                spinnerBankdata.setSelection(i);
                                break;
                            }

                        }


                    }


                if(!newAccount.equalsIgnoreCase("")) {

                    for (int i = 0; i < commonDataListfiltered.size(); i++) {
                        try {
                            JSONObject jcmn1 = new JSONObject(commonDataListfiltered.get(i).getData());
                            String acctype= jcmn1.getString("Accountname");

                            if(acctype.trim().equalsIgnoreCase(newAccount.trim()))
                            {

                                spinnerBankdata.setSelection(i);
                                break;
                            }



                        } catch (Exception e) {

                        }


                    }

                }


             //   }
            } catch (Exception e) {

            }



        }
    }
}
