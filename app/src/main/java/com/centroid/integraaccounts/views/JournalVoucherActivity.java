package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.fragments.SpinnerDialogFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executor;

public class JournalVoucherActivity extends AppCompatActivity {


    ImageView imgback, imgDatepick, imgdelete;
    TextView txtdatepick,txtHead,txtSpinnerDebit,txtSpinnerCredit;
    Spinner spinnerAccountName, spinnerBankdata;
    EditText edtAmount, edtvoucher, edtremarks;
    Button btnSave,btndelete;

    String date = "", month_selected = "", yearselected = "";

    Accounts paymentVoucher;

    LinearLayout layout_bankdata;
    List<CommonData> cmfiltered = new ArrayList<>();
    List<CommonData> commonDataListfiltered = new ArrayList<>();
    FloatingActionButton fabadd, fabaddbank;
    String bankid = "0", bankname = "", debitaccountid = "0";


    Thread thread=null;

    Handler handler;

    Resources resources;

    Accounts accountselected;

    int secondEntry=0;

    CommonData commonData_selected_debit,commonData_selected_credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_voucher);
        getSupportActionBar().hide();

        accountselected=(Accounts) getIntent().getSerializableExtra("Account");

        cmfiltered=new ArrayList<>();

      //  paymentVoucher=(Accounts) getIntent().getSerializableExtra("receipt");
        handler=new Handler();

        cmfiltered = new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        txtSpinnerCredit=findViewById(R.id.txtSpinnerCredit);
        txtSpinnerDebit=findViewById(R.id.txtSpinnerDebit);

        // paymentVoucher = (Accounts) getIntent().getSerializableExtra("paymentVouchers");
        imgback = findViewById(R.id.imgback);
        imgDatepick = findViewById(R.id.imgDatepick);
        fabadd = findViewById(R.id.fabadd);
        fabaddbank = findViewById(R.id.fabaddbank);
        imgdelete = findViewById(R.id.imgdelete);
        txtdatepick = findViewById(R.id.txtdatepick);
        layout_bankdata = findViewById(R.id.layout_bankdata);

        spinnerAccountName = findViewById(R.id.spinnerAccountName);
       // spinnerAccounttype = findViewById(R.id.spinnerAccounttype);
        spinnerBankdata = findViewById(R.id.spinnerBankdata);
        edtAmount = findViewById(R.id.edtAmount);
        btndelete=findViewById(R.id.btndelete);

        edtvoucher = findViewById(R.id.edtvoucher);
        edtremarks = findViewById(R.id.edtremarks);
        btnSave = findViewById(R.id.btnSave);


        String languagedata = LocaleHelper.getPersistedData(JournalVoucherActivity.this, "en");
        Context context= LocaleHelper.setLocale(JournalVoucherActivity.this, languagedata);

        resources=context.getResources();
        edtAmount.setHint( Utils.getCapsSentences(JournalVoucherActivity.this, resources.getString(R.string.amount)));
        edtremarks.setHint(Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.enterremarks)));
        txtdatepick.setText(Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.selectdate)));
        btnSave.setText(Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.save)));

        txtHead.setText(Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.journalvoucher)));

        addAccountSettings();
        setDate();
       // addAccountSettings(1);

        Executor executor=new DirectExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                setEditMode();
            }
        });



        txtSpinnerCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SpinnerDialogFragment spinnerDialogFragment=new SpinnerDialogFragment(false,new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {

                        try {

                            commonData_selected_credit=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = jcmn1.getString("Accountname");

                            txtSpinnerCredit.setText(Utils.getCapsSentences(JournalVoucherActivity.this,acctype));


                        }catch (Exception e)
                        {

                        }


                    }
                });


                spinnerDialogFragment.show(getSupportFragmentManager(),"skjdf");

            }
        });


        txtSpinnerDebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SpinnerDialogFragment spinnerDialogFragment=new SpinnerDialogFragment(false,new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {

                        try {

                            commonData_selected_debit=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = jcmn1.getString("Accountname");

                            txtSpinnerDebit.setText(Utils.getCapsSentences(JournalVoucherActivity.this,acctype));


                        }catch (Exception e)
                        {

                        }


                    }
                });


                spinnerDialogFragment.show(getSupportFragmentManager(),"skjdf");
            }
        });





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showDatePicker();
            }
        });


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(JournalVoucherActivity.this, AccountsettingsActivity.class));



            }
        });

        fabaddbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(JournalVoucherActivity.this, AccountsettingsActivity.class));


            }
        });



        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (accountselected != null) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(JournalVoucherActivity.this);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                            dialogInterface.dismiss();


                            Executor executor=new DirectExecutor();

                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    //  updateData();


                                    try {




                                            //  new DatabaseHelper(JournalVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts1);

                                            new DatabaseHelper(JournalVoucherActivity.this).deleteAccountDataById(accountselected.getACCOUNTS_id() + "");

                                            new DatabaseHelper(JournalVoucherActivity.this).deleteAccountDataById(secondEntry + "");

                                        Utils.playSimpleTone(JournalVoucherActivity.this);

                                        onBackPressed();

                                    }catch (Exception e)
                                    {

                                    }







                                }
                            });




















                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();
                }

            }
        });
















        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonData dbit=(CommonData) spinnerAccountName.getSelectedItem();

                CommonData crdit=(CommonData) spinnerBankdata.getSelectedItem();


                if(!date.equalsIgnoreCase(""))
                {
                    if(commonData_selected_debit!=null)
                    {

                        if(!edtAmount.getText().toString().equalsIgnoreCase(""))
                        {

                            if(commonData_selected_credit!=null) {

                                if(accountselected!=null)
                                {

                                    Executor executor = new DirectExecutor();
                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateData();
                                        }
                                    });

                                }
                                else {


                                    Executor executor = new DirectExecutor();
                                    executor.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            addData();
                                        }
                                    });

                                }





                            }

                            else {

                             //   Toast.makeText(JournalVoucherActivity.this,resources.getString(R.string.selectcreditaccount),Toast.LENGTH_SHORT).show();


                                Utils.showAlertWithSingle(JournalVoucherActivity.this,Utils.getCapsSentences(JournalVoucherActivity.this, resources.getString(R.string.selectcreditaccount)), new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

                            }



                            }

                        else {

                         //   Toast.makeText(JournalVoucherActivity.this,resources.getString(R.string.amount),Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(JournalVoucherActivity.this, resources.getString(R.string.amount), new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });



                        }



                    }

                    else {

                      //  Toast.makeText(JournalVoucherActivity.this,resources.getString(R.string.selectdebitaccount),Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(JournalVoucherActivity.this, Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.selectdebitaccount)), new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }



                }
                else {

                   // Toast.makeText(JournalVoucherActivity.this,resources.getString(R.string.selectdate),Toast.LENGTH_SHORT).show();


                    Utils.showAlertWithSingle(JournalVoucherActivity.this, Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.selectdate)), new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }






            }
        });


    }


    public void updateData()
    {
        CommonData cm = commonData_selected_debit;

        CommonData cm_bank = commonData_selected_credit;

        Accounts accounts = new Accounts();


        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.journalvoucher);
        accounts.setACCOUNTS_setupid(cm.getId());
        //setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());
        accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);
        //  accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account+"");


       new DatabaseHelper(JournalVoucherActivity.this).updateAccountsData(accountselected.getACCOUNTS_id()+"",accounts);

        Utils.playSimpleTone(JournalVoucherActivity.this);




        Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
        accounts1.setACCOUNTS_entryid(accountselected.getACCOUNTS_id() + "");
        accounts1.setACCOUNTS_date(date);
        accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.journalvoucher);
        //setupid must be bank account id
        accounts1.setACCOUNTS_setupid(cm_bank.getId());
        accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
        accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
        accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts1.setACCOUNTS_month(month_selected);
        accounts1.setACCOUNTS_year(yearselected);
        // accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank+"");

        new DatabaseHelper(JournalVoucherActivity.this).updateAccountsData(secondEntry+"",accounts1);
        Utils.playSimpleTone(JournalVoucherActivity.this);



        date = "";
        month_selected = "";
        yearselected = "";
        spinnerAccountName.setSelection(0);
        edtAmount.setText("");
        edtvoucher.setText("");
        edtremarks.setText("");
        txtdatepick.setText(resources.getString(R.string.selectdate));
        spinnerBankdata.setSelection(0);
        setDate();
        onBackPressed();
    }




    public void setEditMode()
    {
        try{


           if(accountselected!=null)
           {

               btndelete.setVisibility(View.VISIBLE);

               date = accountselected.getACCOUNTS_date();
               month_selected = accountselected.getACCOUNTS_month();
               yearselected = accountselected.getACCOUNTS_year();

               edtAmount.setText(accountselected.getACCOUNTS_amount());

               edtremarks.setText(accountselected.getACCOUNTS_remarks());
               txtdatepick.setText(date);


               for (int i=0;i<cmfiltered.size();i++)
               {

                   if(cmfiltered.get(i).getId().equalsIgnoreCase(accountselected.getACCOUNTS_setupid()))
                   {

                       commonData_selected_debit=cmfiltered.get(i);

                       try {

                           JSONObject jsonObject=new JSONObject(commonData_selected_debit.getData());


                           String accname=jsonObject.getString("Accountname");


                           txtSpinnerDebit.setText(accname);
                       }catch (Exception e)
                       {

                       }
                       spinnerAccountName.setSelection(i);
                       break;


                   }

               }

               List<Accounts>acc=new DatabaseHelper(JournalVoucherActivity.this).getAccountsDataByEntryId(accountselected.getACCOUNTS_id()+"");

               if(acc.size()>0) {

                   Accounts accounts_second=acc.get(0);
                   secondEntry=accounts_second.getACCOUNTS_id();


                   for (int i = 0; i < commonDataListfiltered.size(); i++) {

                       if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(accounts_second.getACCOUNTS_setupid())) {

                           spinnerBankdata.setSelection(i);

                           commonData_selected_credit=commonDataListfiltered.get(i);

                           try {

                               JSONObject jsonObject=new JSONObject(commonData_selected_credit.getData());


                               String accname=jsonObject.getString("Accountname");


                               txtSpinnerCredit.setText(accname);
                           }catch (Exception e)
                           {

                           }

                           break;


                       }

                   }

               }



           }













        }catch (Exception e)
        {

        }















    }















    public void addData()
    {



            CommonData cm = commonData_selected_debit;

            CommonData cm_bank = commonData_selected_credit;

            Accounts accounts = new Accounts();

            //entry id is equal to zero in the base of accounts section from account setup
            accounts.setACCOUNTS_entryid("0");
            accounts.setACCOUNTS_date(date);
            accounts.setACCOUNTS_vouchertype(Utils.VoucherType.journalvoucher);
            accounts.setACCOUNTS_setupid(cm.getId());//setup id is the account setup id from the dropdown
            accounts.setACCOUNTS_amount(edtAmount.getText().toString());
            accounts.setACCOUNTS_type(Utils.Cashtype.debit + "");
            accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
            accounts.setACCOUNTS_month(month_selected);
            accounts.setACCOUNTS_year(yearselected);
          //  accounts.setACCOUNTS_cashbanktype(Utils.CashBanktype.account+"");


            long insertid = new DatabaseHelper(JournalVoucherActivity.this).addAccountsData(accounts);

        Utils.playSimpleTone(JournalVoucherActivity.this);




        Accounts accounts1 = new Accounts();
//entry id is must equal to accountsid in accounts section
                accounts1.setACCOUNTS_entryid(insertid + "");
                accounts1.setACCOUNTS_date(date);
                accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.journalvoucher);
                //setupid must be bank account id
                accounts1.setACCOUNTS_setupid(cm_bank.getId());
                accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
                accounts1.setACCOUNTS_type(Utils.Cashtype.credit + "");
                accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
                accounts1.setACCOUNTS_month(month_selected);
                accounts1.setACCOUNTS_year(yearselected);
               // accounts1.setACCOUNTS_cashbanktype(Utils.CashBanktype.bank+"");

                new DatabaseHelper(JournalVoucherActivity.this).addAccountsData(accounts1);

        Utils.playSimpleTone(JournalVoucherActivity.this);


        date = "";
            month_selected = "";
            yearselected = "";
            spinnerAccountName.setSelection(0);
            edtAmount.setText("");
            edtvoucher.setText("");
            edtremarks.setText("");
            txtdatepick.setText(resources.getString(R.string.selectdate));
            spinnerBankdata.setSelection(0);
        setDate();


    }





    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(JournalVoucherActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;
                month_selected=m+"";
                yearselected=i+"";

                date=i2+"-"+m+"-"+i;

                txtdatepick.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }







    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        addAccountSettings();
      //  addAccountSettings(1);
    }

    public void addAccountSettings()
    {

        try {
            cmfiltered.clear();
            commonDataListfiltered.clear();


            List<CommonData> commonData = new DatabaseHelper(JournalVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if (commonData.size() > 0) {



                Collections.sort(commonData, new Comparator<CommonData>() {
                    @Override
                    public int compare(CommonData commonData, CommonData t1) {

                        int a=0;

                        try{



                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype= jcmn1.getString("Accountname");
                            JSONObject j1 = new JSONObject(t1.getData());
                            String acctypej1= j1.getString("Accountname");

                            a=     acctype.compareToIgnoreCase(acctypej1);

                        }
                        catch (Exception e)
                        {

                        }





                        return a;
                    }
                });






                CommonData cm=new CommonData();
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("Accountname",Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.selectdebitaccount)));

                cm.setData(jsonObject.toString());
                cm.setId("0");





                    cmfiltered.addAll(commonData);
                    cmfiltered.add(0,cm);















                CommonData cm1=new CommonData();
                JSONObject jsonObject1=new JSONObject();
                jsonObject1.put("Accountname",Utils.getCapsSentences(JournalVoucherActivity.this,resources.getString(R.string.selectcreditaccount)));

                cm1.setData(jsonObject1.toString());
                cm1.setId("0");





                commonDataListfiltered.addAll(commonData);
                commonDataListfiltered.add(0,cm1);









                commonDataListfiltered.addAll(commonData);


                    AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(JournalVoucherActivity.this, cmfiltered);
                    spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);






                    AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter1 = new AccountSettingsSpinnerAdapter(JournalVoucherActivity.this, commonDataListfiltered);
                    spinnerBankdata.setAdapter(accountSettingsSpinnerAdapter1);


                //}








            }

        }catch (Exception e)
        {


        }


    }
}
