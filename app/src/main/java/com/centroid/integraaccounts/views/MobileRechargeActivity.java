package com.centroid.integraaccounts.views;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.OpenPay.PaymentWebViewActivity;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.MobileOperatorAdapter;
import com.centroid.integraaccounts.adapter.PaymentTypeAdapter;
import com.centroid.integraaccounts.adapter.RechargeRecordAdapter;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.rechargedomain.RPlans;
import com.centroid.integraaccounts.data.rechargedomain.RechargeRecords;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.services.OperatorSelectionListener;
import com.centroid.integraaccounts.views.rechargeViews.CommonRechargeViewActivity;
import com.centroid.integraaccounts.views.rechargeViews.MobilePackPagerAdapter;
import com.centroid.integraaccounts.views.rechargeViews.MobileRechargeDashboardActivity;
import com.centroid.integraaccounts.views.rechargeViews.domain.mobile.Category;
import com.centroid.integraaccounts.views.rechargeViews.domain.mobile.MobileRechargePack;
import com.centroid.integraaccounts.webserviceHelper.RechargeRequestHandler;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MobileRechargeActivity extends AppCompatActivity {

    ImageView imgback, imgoperator,imagoperator;
    TextView txtSpinner,txtprofile;
    String fullString = "";
    RecyclerView recyclerview;
    Spinner Spinner_Plans;

    Button btnsubmit,btnsubmitfind;
    LinearLayout layout_operator;
    double totalamountToSubmit=0;
    String email = "";
    String name = "";
    String mobile ="";

    String id_transaction="";

    EditText edtPhone;
    //    String arrmonth[] = {"Airtel", "Idea", "Jio", "BSNL"};
    String arrmonth[] = {"Airtel", "VI", "Jio", "BSNL"};
    String arrspkey[] = {"3", "VIL", "116", "4"};
    int drawable_key[] = {R.drawable.airtel, R.drawable.vi, R.drawable.jio, R.drawable.bsnl};

    String selected_operator = "";
    String amount = "";
    String recharge_url = "https://mysaveapp.com/rechargeAPI/RechargePhone.php?";
    String operator_url = "https://mysaveapp.com/rechargeAPI/getMobileOperator.php?";
    int selectedposition = 0;
    String transactionid = "", userid = "",paymentmode="upi";
    String fullurl="";
    String rechargeamount="0",mobilenumber="",spkey="",operator_code="";
    EditText edtSearchAmount;
    List<RechargeRecords> recordes=new ArrayList<>();
    String recharge="",operatorcircle="";

    TabLayout tabs;
    ViewPager viewPager;
    int code1=0;

    String paymentdata_id="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_recharge);
        getSupportActionBar().hide();

        imgback = findViewById(R.id.imgback);
        edtSearchAmount=findViewById(R.id.edtSearchAmount);
        imagoperator=findViewById(R.id.imagoperator);
        txtprofile=findViewById(R.id.txtprofile);
        txtSpinner = findViewById(R.id.txtSpinner);
        btnsubmit = findViewById(R.id.btnsubmit);
        edtPhone = findViewById(R.id.edtPhone);
        Spinner_Plans = findViewById(R.id.Spinner_Plans);
        recyclerview = findViewById(R.id.recyclerview);
        imgoperator = findViewById(R.id.imgoperator);
        btnsubmitfind=findViewById(R.id.btnsubmitfind);
        layout_operator=findViewById(R.id.layout_operator);
        tabs=findViewById(R.id.tabs);
        viewPager=findViewById(R.id.viewPager);

        Intent intent=getIntent();
        selected_operator=intent.getStringExtra("operator");
        mobilenumber=intent.getStringExtra("mobile");
        spkey=intent.getStringExtra("spkey");
        recharge=intent.getStringExtra("recharge");
        operator_code=intent.getStringExtra("operator_code");
        operatorcircle=intent.getStringExtra("operatorcircle");
        txtprofile.setText(mobilenumber);
        recordes=new ArrayList<>();
        if (selected_operator.compareTo("VI") == 0) {

            selected_operator = "Vodafone";
            imagoperator.setImageResource(R.drawable.vi);
//            txtSpinner.setText("VI");
        }
        else if(selected_operator.compareTo("Airtel") == 0)
        {
            imagoperator.setImageResource(R.drawable.airtel);
        }
        else if(selected_operator.compareTo("Jio") == 0)
        {
            imagoperator.setImageResource(R.drawable.jio);
        }
        else if(selected_operator.compareTo("BSNL") == 0)
        {
            imagoperator.setImageResource(R.drawable.bsnl);
        }




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });



        getRechargePlans();




        if(!recharge.isEmpty())
        {
            rechargeamount=recharge;
           getProfile(recharge);
        }




    }

    public void getRechargePlans()
    {
        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                   // https://mysaveapp.com/rechargeAPI/newrecharge/mobilePlans.php?q=283486532145554&circle=KL&operatorcode=AT

                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
                    String urldata = Utils.rechargebaseurl+ "q=" + Utils.getTimestamp() + "&circle=" + operatorcircle + "&operatorcode=" + operator_code+"&timestamp="+Utils.getTimestamp();

                    String fullString = "";
                    URL url = new URL(urldata);
//                    URLConnection connection  = url.openConnection();
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setConnectTimeout(10000);
                    connection.setReadTimeout(10000);
                    connection.connect();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

//                                        progressFragment.dismiss();

                    MobileRechargePack mobileRechargePack=new GsonBuilder().create().fromJson(fullString,MobileRechargePack.class);


                    if(mobileRechargePack.getStatus().compareTo("OK")==0)
                    {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressFragment.dismiss();
                                List<Category>categories=mobileRechargePack.getCategories();

                                if(categories.size()>0)
                                {
                                    if(tabs.getTabCount()>0) {
                                        tabs.removeAllTabs();
                                    }

                                    for(int i=0;i<categories.size();i++)
                                    {
                                        tabs.addTab(tabs.newTab());

                                    }


                                    MobilePackPagerAdapter mobilePackPagerAdapter=new MobilePackPagerAdapter(MobileRechargeActivity.this,categories);
                                    viewPager.setAdapter(mobilePackPagerAdapter);


                                    tabs.setupWithViewPager(viewPager);

                                }

                            }
                        });




                    }



                } catch (Exception e) {

                    if(e.toString().contains("TimeoutException")) {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressFragment.dismiss();
                                Utils.showAlertWithSingle(MobileRechargeActivity.this, "Something went wrong. Please try again later...", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });
                            }
                        });


                    }

                }
            }
        });
    }


    private void updateTransactionStatus(String id)
    {

        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dajbb");
        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        params.put("id",id);


        new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();

                try{

                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.getInt("status")==1)
                    {
                        String msg=jsonObject.getString("message");
//                        String id=jsonObject.getString("id");

                        Utils.showAlertWithSingle(MobileRechargeActivity.this, msg, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }
                    else{


                        String msg=jsonObject.getString("message");

                        Utils.showAlertWithSingle(MobileRechargeActivity.this, msg, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }




                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateTransactionStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }





    public void getProfile(String amountdata) {
        this.rechargeamount=amountdata;
        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "fkjfk");


        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata = new GsonBuilder().create().fromJson(data, Profiledata.class);


                progressFragment.dismiss();
                if (profiledata != null) {

                    try {


                        if (profiledata.getStatus() == 1) {


                            if (profiledata.getData() != null) {

//                                name=i.getStringExtra("name");
//                                email=i.getStringExtra("email");
//                                phone=i.getStringExtra("phone");
//                                amount=i.getStringExtra("amount");
                                int arricons[]={R.drawable.upi,R.drawable.netbanking,
                                        R.drawable.debitcard,
                                        R.drawable.creditcard};


                                String arr[]={"UPI (no convenience charges)","NET Banking (payment gateway charge @1.5% application)",
                                "Debit Card (payment gateway charge @o.4% application)",
                                "Credit card (payment gateway charge @2.1% application)"};

                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int height = displayMetrics.heightPixels ;
                                int width = displayMetrics.widthPixels;

                                final Dialog dialog = new Dialog(MobileRechargeActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.layout_servicechargecalculation);

                                Spinner spinner_dth=dialog.findViewById(R.id.spinner_dth);
                                TextView txtAmount=dialog.findViewById(R.id.txtAmount);
                                TextView txtTotalAmount=dialog.findViewById(R.id.txtTotalAmount);

                                ImageView imgicon=dialog.findViewById(R.id.imgicon);
                                TextView txtPaymentSpinner=dialog.findViewById(R.id.txtPaymentSpinner);


                                Button save=dialog.findViewById(R.id.save);

                                txtAmount.setText(amountdata);

                                txtTotalAmount.setText(amountdata);
                                double t=Double.parseDouble(amountdata);
                                totalamountToSubmit=t;
                                amount=totalamountToSubmit+"";

                                txtPaymentSpinner.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        int a=(int)height/2;

                                        final Dialog dialog1 = new Dialog(MobileRechargeActivity.this);
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.layout_paymenttype);
                                        RecyclerView recycler=dialog1.findViewById(R.id.recycler);
                                        PaymentTypeAdapter paymentTypeAdapter=new PaymentTypeAdapter(MobileRechargeActivity.this, new OperatorSelectionListener() {
                                            @Override
                                            public void onOperatorSelected(int i, String selectedoperator) {
                                                imgicon.setImageResource(arricons[i]);
                                                txtPaymentSpinner.setText(arr[i]);
                                                if(i==0)
                                                {
                                                    paymentmode="upi";
                                                    txtTotalAmount.setText(amountdata);
                                                    double t=Double.parseDouble(amountdata);
                                                    totalamountToSubmit=t;
                                                    amount=totalamountToSubmit+"";
                                                }
                                                else if(i==1)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");

                                                    paymentmode="Net Banking";
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(1.5/100);
                                                    totalamountToSubmit= am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    txtTotalAmount.setText(a+"");
                                                    amount=a;

                                                }
                                                else if(i==2)
                                                {
                                                    paymentmode="Debit card";
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(0.4/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                else if(i==3)
                                                {
                                                    paymentmode="Credit card";
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(2.1/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                dialog1.dismiss();
                                            }
                                        });

                                        recycler.setLayoutManager(new GridLayoutManager(MobileRechargeActivity.this,2));
                                        recycler.setAdapter(paymentTypeAdapter);

                                        dialog1.getWindow().setLayout(width,a);
                                        dialog1.show();

                                    }
                                });

                                imgicon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        int a=(int)height/2;
                                        final Dialog dialog1 = new Dialog(MobileRechargeActivity.this);
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.layout_paymenttype);
                                        RecyclerView recycler=dialog1.findViewById(R.id.recycler);
                                        PaymentTypeAdapter paymentTypeAdapter=new PaymentTypeAdapter(MobileRechargeActivity.this, new OperatorSelectionListener() {
                                            @Override
                                            public void onOperatorSelected(int i, String selectedoperator) {

                                                imgicon.setImageResource(arricons[i]);
                                                txtPaymentSpinner.setText(arr[i]);

                                                if(i==0)
                                                {
                                                    txtTotalAmount.setText(amountdata);
                                                    double t=Double.parseDouble(amountdata);
                                                    totalamountToSubmit=t;
                                                    amount=totalamountToSubmit+"";
                                                }
                                                else if(i==1)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");


                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(1.5/100);
                                                    totalamountToSubmit= am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    txtTotalAmount.setText(a+"");
                                                    amount=a;

                                                }
                                                else if(i==2)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(0.4/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                else if(i==3)
                                                {
                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                    double am=Double.parseDouble(amountdata);

                                                    double servicecharge=am*(2.1/100);
                                                    totalamountToSubmit=am+servicecharge;
                                                    String a=  df.format(totalamountToSubmit);
                                                    amount=a;
                                                    txtTotalAmount.setText(a+"");

                                                }

                                                dialog1.dismiss();
                                            }
                                        });

                                        recycler.setLayoutManager(new GridLayoutManager(MobileRechargeActivity.this,2));
                                        recycler.setAdapter(paymentTypeAdapter);
                                        dialog1.getWindow().setLayout(width,a);
                                        dialog1.show();
                                    }
                                });



                                save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        Utils.showAlertWithSingle(MobileRechargeActivity.this, "Please do not press back button or refresh", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {
                                                String stateid = profiledata.getData().getStateId();
                                                String countryid = profiledata.getData().getCountryId();
                                                 mobile = mobilenumber;
                                                 email = profiledata.getData().getEmailId();
                                                 name = profiledata.getData().getFullName();
                                                userid = profiledata.getData().getId();

                                                int random = new Random().nextInt((99999 - 1000) + 1) + 1000;
                                                String apireqid=random+"";

                                                code1=0;



                                                    postPGData(amount);


                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });



                                    }
                                });

                                dialog.getWindow().setLayout(width,height);

                                dialog.show();

//



                            }


                        } else {

                            Toast.makeText(MobileRechargeActivity.this, " failed", Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {

                    }


                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.getUserDetails + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        if (data != null) {

                            if (data.hasExtra("result")) {

                                transactionid = data.getStringExtra("result");

                                    updatePaymentStatus(id_transaction,5,code1,"2");

                            }
                        }

                    }
                    else {
                        transactionid = "";

                        updatePaymentStatus(id_transaction,6,code1,"0");
//

                        }



                }
            });


    public void postPGData(String amountdata)
    {

        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dajbb");
        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("user_id", userid);
        params.put("mobile_number", mobilenumber);
        params.put("account_number", mobilenumber);
        params.put("transaction_id", transactionid);
        params.put("operatorcircle", operatorcircle);
        params.put("amount", amountdata);
        params.put("rechargeamount", rechargeamount);
        params.put("operator", selected_operator);
        params.put("rp_id", "");
        params.put("agent_id", "");
        params.put("status",  "2");
        params.put("recharge_type", "1");
        params.put("spkey",spkey);
        params.put("payment_status", "4");
        params.put("Payment_Mode",paymentmode);



        if(id_transaction.isEmpty()) {

            new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();

                    try {

                        JSONObject jsonObject = new JSONObject(data);
                        if (jsonObject.getInt("status") == 1) {
                            String msg = jsonObject.getString("message");
                            id_transaction = jsonObject.getString("id");

                            int random = new Random().nextInt((99999 - 1000) + 1) + 1000;
                            String apireqid = random + "";

                            code1 = 0;


                            Intent i = new Intent(MobileRechargeActivity.this, PaymentWebViewActivity.class);
                            i.putExtra("name", name);
                            i.putExtra("email", email);
                            i.putExtra("phone", mobile);
                            i.putExtra("amount", amount + "");
                            i.putExtra("requestid", apireqid);
                            i.putExtra("spkey", spkey);
                            i.putExtra("operator", selected_operator);
                            i.putExtra("rechargeamount", rechargeamount);
                            i.putExtra("id_transaction", id_transaction);

                            someActivityResultLauncher.launch(i);


                        } else {


                        }


                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    Toast.makeText(MobileRechargeActivity.this, err, Toast.LENGTH_SHORT).show();

                }
            }, Utils.WebServiceMethodes.PostTransactionata + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();


        }
        else{
            params.put("id",id_transaction);

            new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();

                    try {

                        JSONObject jsonObject = new JSONObject(data);
                        if (jsonObject.getInt("status") == 1) {
                            String msg = jsonObject.getString("message");
                           // id_transaction = jsonObject.getString("id");

                            int random = new Random().nextInt((99999 - 1000) + 1) + 1000;
                            String apireqid = random + "";

                            code1 = 0;


                            Intent i = new Intent(MobileRechargeActivity.this, PaymentWebViewActivity.class);
                            i.putExtra("name", name);
                            i.putExtra("email", email);
                            i.putExtra("phone", mobile);
                            i.putExtra("amount", amount + "");
                            i.putExtra("requestid", apireqid);
                            i.putExtra("spkey", spkey);
                            i.putExtra("operator", selected_operator);
                            i.putExtra("rechargeamount", rechargeamount);
                            i.putExtra("id_transaction", id_transaction);

                            someActivityResultLauncher.launch(i);


                        } else {


                        }


                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    Toast.makeText(MobileRechargeActivity.this, err, Toast.LENGTH_SHORT).show();

                }
            }, Utils.WebServiceMethodes.updatePostTransaction + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();


        }


    }


    public void updateRechargePaymentStatus(String id,int status,int code)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
        // params.put("device_id",token);

        new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(code==0)
                {




                }
                else{

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateRechargeStatusInPayment+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }


    public void updatePaymentStatus(String id,int status,int code,String rechargestatus)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("id", id);
        params.put("status", status+"");
         params.put("transaction_id",transactionid);
        params.put("rechargestatus",rechargestatus);

        new RequestHandler(MobileRechargeActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                try{
//                    JSONObject jsonObject=new JSONObject(data);

                    if(status==5)
                    {
                        rechargePhone(rechargeamount);
                    }



                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(MobileRechargeActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updatePaymentStatus+"?timestamp="+Utils.getTimestamp(), Request.Method.POST).submitRequest();



    }



    public void rechargePhone(String Amount) {
        int min = 1;
        int max = 10000;
        int random = new Random().nextInt((max - min) + 1) + min;

//        String spkey = "";
        String phone = mobilenumber;

        double amount = Double.parseDouble(Amount);
//        spkey = arrspkey[selectedposition];

        String apireqid = random + "";
        String customerno = mobilenumber;

        String urldata = recharge_url + "phone=" + phone + "&amount=" + amount + "&spkey=" + spkey + "&apireqid=" + apireqid + "&customerno=" + customerno;



        Intent intent=new Intent(MobileRechargeActivity.this, CommonRechargeViewActivity.class);
        intent.putExtra("url",urldata);
        intent.putExtra("phone",phone);
        intent.putExtra("operator",selected_operator);
        intent.putExtra("transaction_id",id_transaction);
        startActivity(intent);
        finish();


    }





}