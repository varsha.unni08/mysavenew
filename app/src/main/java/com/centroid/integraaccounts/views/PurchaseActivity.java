package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.RazorPayOrder;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
//import com.razorpay.Checkout;
//import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseActivity extends AppCompatActivity  {

//    Checkout checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        getSupportActionBar().hide();

//        checkout=new Checkout();
//
//        checkout.setKeyID("rzp_test_znHSTgNEqEyAID");
//        {"receipt":"sldjfs",
//                "amount":500,
//                "currency":"INR"
//
//        }

        try {

            Map<String,Object> jsonObject1 = new HashMap<>();
            jsonObject1.put("receipt", "recipt1234");
            jsonObject1.put("amount", 500);
            jsonObject1.put("currency", "INR");



//            RestService.RestApiInterface client = RestService.getRazorPayClientClient();
//
//            Call<JsonObject> jsonObjectCall = client.postOrders(jsonObject1);
//
//            jsonObjectCall.enqueue(new Callback<JsonObject>() {
//                @Override
//                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                    if(response!=null)
//                    {
//
//                        if(response.body()!=null)
//                        {
//
//                            Toast.makeText(PurchaseActivity.this,response.body().toString(),Toast.LENGTH_SHORT).show();
//
//
//                            RazorPayOrder razorPayOrder=new GsonBuilder().create().fromJson(response.body().toString(),RazorPayOrder.class);
//
//
//                            startPayment(razorPayOrder);
//
//
//
//
//                        }
//                        else {
//
//                            try {
//
//                                byte[] inputStream = response.errorBody().bytes();
//
//
//                                String str = new String(inputStream, StandardCharsets.UTF_8);
//
//                                //Toast.makeText(PurchaseActivity.this,str,Toast.LENGTH_SHORT).show();
//
//                            }catch (Exception e)
//                            {
//
//                            }
//
//
//
//                        }
//
//
//                    }
//
//
//
//                }
//
//                @Override
//                public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                    Toast.makeText(PurchaseActivity.this,t.toString(),Toast.LENGTH_SHORT).show();
//
//
//                }
//            });

            //secret id : lvhUGQCw5xOkb7w70DKHkVOd

            // {"id":"order_GsPkjrBYI0URJq","entity":"order","amount":500,"amount_paid":0,"amount_due":500,"currency":"INR","receipt":"sldjfs","offer_id":null,"status":"created","attempts":0,"notes":[],"created_at":1617008845}


        }catch (Exception e)
        {


        }
    }



    public void startPayment(RazorPayOrder razorPayOrder)
    {

        try {
            JSONObject options = new JSONObject();

            options.put("name", "Merchant Name");
            options.put("description", "Reference No. #123456");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", razorPayOrder.getId());//from response of step 3.
            options.put("theme.color", "#3399cc");
            options.put("currency", "INR");
            options.put("amount", razorPayOrder.getAmount());//pass amount in currency subunits
            options.put("prefill.email", "gaurav.kumar@example.com");
            options.put("prefill.contact","9988776655");
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

          //  checkout.open(PurchaseActivity.this, options);

        } catch(Exception e) {
            Log.e("TAG", "Error in starting Razorpay Checkout", e);
        }
    }





//
//    @Override
//    public void onPaymentSuccess(String s) {
//
//        Toast.makeText(PurchaseActivity.this,"Success : "+s,Toast.LENGTH_SHORT).show();
//
//
//    }
//
//    @Override
//    public void onPaymentError(int i, String s) {
//        Toast.makeText(PurchaseActivity.this,"Failed : "+s,Toast.LENGTH_SHORT).show();
//    }
}
