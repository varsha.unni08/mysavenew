package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.EmergencyLinksAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class EmergencyLinkActivity extends AppCompatActivity {

    ImageView imgback;
    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtNodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_link);
        getSupportActionBar().hide();
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);
        txtNodata=findViewById(R.id.txtNodata);



        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                final Dialog dialog = new Dialog(
                        EmergencyLinkActivity.this);
                dialog.setContentView(R.layout.layout_myweblinks);
                dialog.setTitle("My Save");
                int b=(int)(height/1.3);
                dialog.getWindow().setLayout(width - 30,b );

                EditText edtWeblink=dialog.findViewById(R.id.edtWeblink);
                EditText edtPassword=dialog.findViewById(R.id.edtPassword);
                EditText edtUsername=dialog.findViewById(R.id.edtUsername);
                EditText edtDescription=dialog.findViewById(R.id.edtDescription);
                Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

                edtUsername.setVisibility(View.GONE);
                edtDescription.setVisibility(View.GONE);

                edtWeblink.setHint("Name");
                edtPassword.setHint("Phone Number");
                edtPassword.setInputType(InputType.TYPE_CLASS_PHONE);

                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!edtWeblink.getText().toString().equalsIgnoreCase(""))
                        {
                            if(!edtPassword.getText().toString().equalsIgnoreCase(""))
                            {


                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("emergencydata", edtWeblink.getText().toString());
                                    jsonObject.put("emergencyno", edtPassword.getText().toString());
                                    jsonObject.put("isAllowDelete", "1");


                                    new DatabaseHelper(EmergencyLinkActivity.this).addData(Utils.DBtables.TABLE_EMERGENCY,jsonObject.toString());

                                    dialog.dismiss();

                                    getEmergencyData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }






                            }
                            else{


                                Utils.showAlertWithSingle(EmergencyLinkActivity.this, "Enter the Website password", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });
                            }


                        }
                        else{


                            Utils.showAlertWithSingle(EmergencyLinkActivity.this, "Enter the Website link", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });
                        }





                    }
                });






                dialog.show();






            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getEmergencyData();
    }


    public void showEditDialog(CommonData commonData)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(
                EmergencyLinkActivity.this);
        dialog.setContentView(R.layout.layout_myweblinks);
        dialog.setTitle("My Save");
        int b=(int)(height/1.3);
        dialog.getWindow().setLayout(width - 30,b );

        EditText edtWeblink=dialog.findViewById(R.id.edtWeblink);
        EditText edtPassword=dialog.findViewById(R.id.edtPassword);
        EditText edtUsername=dialog.findViewById(R.id.edtUsername);
        EditText edtDescription=dialog.findViewById(R.id.edtDescription);
        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

        edtUsername.setVisibility(View.GONE);
        edtDescription.setVisibility(View.GONE);
        btnSubmit.setText("Update");

        edtWeblink.setHint("Name");
        edtPassword.setHint("Phone Number");
        edtPassword.setInputType(InputType.TYPE_CLASS_PHONE);


        try {

            CommonData cmd=commonData;


            JSONObject jsonObject = new JSONObject(cmd.getData());


            String emergencydata=   jsonObject.getString("emergencydata");
            String emergencyno=   jsonObject.getString("emergencyno");

            edtWeblink.setText(emergencydata);
            edtPassword.setText(emergencyno);







        } catch (JSONException e) {
            e.printStackTrace();
        }






        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtWeblink.getText().toString().equalsIgnoreCase(""))
                {
                    if(!edtPassword.getText().toString().equalsIgnoreCase(""))
                    {


                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("emergencydata", edtWeblink.getText().toString());
                            jsonObject.put("emergencyno", edtPassword.getText().toString());
                            jsonObject.put("isAllowDelete", "1");


                            new DatabaseHelper(EmergencyLinkActivity.this).updateData(commonData.getId(),jsonObject.toString(), Utils.DBtables.TABLE_EMERGENCY);

                            dialog.dismiss();

                            getEmergencyData();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }






                    }
                    else{


                        Utils.showAlertWithSingle(EmergencyLinkActivity.this, "Enter the Website password", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }


                }
                else{


                    Utils.showAlertWithSingle(EmergencyLinkActivity.this, "Enter the Website link", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }





            }
        });






        dialog.show();
    }


    public void getEmergencyData()
    {

        List<CommonData> commondata= new DatabaseHelper(EmergencyLinkActivity.this).getData(Utils.DBtables.TABLE_EMERGENCY);


        if (commondata.size()>0)
        {
            txtNodata.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);

            recycler.setLayoutManager(new LinearLayoutManager(EmergencyLinkActivity.this));
            recycler.setAdapter(new EmergencyLinksAdapter(EmergencyLinkActivity.this,commondata));

        }
        else{
            txtNodata.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);

        }
    }
}