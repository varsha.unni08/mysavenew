package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AssetLedgerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AssetLedgerActivity extends AppCompatActivity {

    LinearLayout layout_head;

    RecyclerView recycler;
    List<CommonData> cmfiltered_;

    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_ledger);
        getSupportActionBar().hide();
        cmfiltered_=new ArrayList<>();
        imgback=findViewById(R.id.imgback);
        recycler=findViewById(R.id.recycler);
        layout_head=findViewById(R.id.layout_head);

        String languagedata = LocaleHelper.getPersistedData(AssetLedgerActivity.this, "en");
        Context context= LocaleHelper.setLocale(AssetLedgerActivity.this, languagedata);

        Resources resources=context.getResources();

        Utils.showTutorial(resources.getString(R.string.assettutorial),AssetLedgerActivity.this,Utils.Tutorials.assettutorial);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getAccounthead();
    }


    public void getAccounthead() {

        layout_head.setVisibility(View.VISIBLE);
        recycler.setVisibility(View.VISIBLE);

        cmfiltered_.clear();


        List<CommonData> commonData = new DatabaseHelper(AssetLedgerActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            //Collections.reverse(commonData);


            for (CommonData cm : commonData) {

                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String Accounttype=jsonObject.getString("Accounttype");






                        if(Accounttype.equalsIgnoreCase("Asset account"))
                        {
                            cmfiltered_.add(cm);
                        }







                }catch (Exception e)
                {


                }

            }

            try {



            }catch (Exception e)
            {

            }



        }
        else {

            layout_head.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);


        }

        if(cmfiltered_.size()>0)
        {
            layout_head.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.VISIBLE);
        }
        else {
            layout_head.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);

        }

        checkHeads();




    }

    private void checkHeads() {

        List<LedgerAccount>ledgerAccounts=new ArrayList<>();
        try {

            if (cmfiltered_.size() > 0) {

                Calendar calendar = Calendar.getInstance();
                int y = calendar.get(Calendar.YEAR);
                int m = calendar.get(Calendar.MONTH) + 1;
                int d = calendar.get(Calendar.DAY_OF_MONTH);

                String enddate = d + "-" + m + "-" + y;


                List<Accounts> accounts = new DatabaseHelper(AssetLedgerActivity.this).getAllAccounts();

               // if (accounts.size() > 0) {

                    for (CommonData data : cmfiltered_) {

                        JSONObject jsonObject = new JSONObject(data.getData());
                        String Amount = jsonObject.getString("Amount");

                      //  double amt = Double.parseDouble(Amount);

                        double amt =0;


                        amt = Double.parseDouble(Amount);

                        String Type=jsonObject.getString("Type");


                        //  amt = Double.parseDouble(Amount);

                        if(Type.equalsIgnoreCase("Credit"))
                        {
                            amt=amt*-1;
                        }
                        double closingbalance = getClosingBalance(amt, data.getId(), enddate, "");

                        DecimalFormat decimalFormatter = new DecimalFormat("###################################################");

                                LedgerAccount ledgerAccount = new LedgerAccount();
                                ledgerAccount.setAccountheadid(data.getId());
                                ledgerAccount.setClosingbalance(decimalFormatter.format(closingbalance)+"");
                                ledgerAccounts.add(ledgerAccount);



                    }


               // }


            }


        }catch (Exception e)
        {

        }



        recycler.setLayoutManager(new LinearLayoutManager(AssetLedgerActivity.this));
        recycler.setAdapter(new AssetLedgerAdapter(AssetLedgerActivity.this,ledgerAccounts));



    }



    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = new DatabaseHelper(AssetLedgerActivity.this).getAllAccounts();

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }
}
