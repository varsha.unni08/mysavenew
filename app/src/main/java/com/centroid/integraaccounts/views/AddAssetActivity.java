package com.centroid.integraaccounts.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.PathUtil;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AssetFileListAdapter;
import com.centroid.integraaccounts.adapter.AssetListAdapter;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.adapter.RemindDateAdapter;
import com.centroid.integraaccounts.adapter.ReminddateSelectAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.RemindData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.receivers.DateTimeReceiver;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsAssetRequestcode;
import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsInvestmentRequestcode;

public class AddAssetActivity extends AppCompatActivity {

    ImageView imgback,imgRemind,imgDatepurchase,imgClose,imgRemindDate;
    TextView txtReminddate,txtdatepurchase,txtRemindedDate;

    CardView card_remind;
    Button btnAdd,btnSubmitRemindDate,btnDocuments;
    EditText edtremarks,edtassetamount,edtDescription;

    CommonData commonData;

    String purchasedate="",remind_date="";
    TextView txtprofile;
    String fname="";

    String assetid="",newaccount="";

    Spinner spassetname;

    FloatingActionButton fabadd;
    RecyclerView recyclerRemindDate,recyclerDocuments;

    List<String>remindDates;

    List<RemindData>remindData;

    ReminddateSelectAdapter reminddateSelectAdapter=null;

    Set<String>rmdate;

    int updatestatus=-1;


    List<String>taskid;


    GoogleSignInClient googleSignInClient;
    Dialog uploaddoc=null;


    public static final int PICKFILE_RESULT_CODE=11;

    public static final int PICKIOmage_RESULT_CODE=112;
    public static final int REQUEST_CODE_SIGN_IN=110;
    public static final int REQUEST_CODE_SIGN_INFORDELETION=111;
    public static final int REQUEST_CODE_SIGN_IN_DOWNLOAD=113;

    String fileid="";
    byte[] b=null;
    File fileuploadToDrive=null;
    String filePath="",mimetype="";


    ImageView imgphoto;

    LinearLayout layout_imagepick;
    LinearLayout layout_filepick;
    TextView txtFilepath;
    EditText edtfileTitle;

    List<String>assetfilelist=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset);
        getSupportActionBar().hide();

        commonData=(CommonData) getIntent().getSerializableExtra("data");
        remindDates=new ArrayList<>();
        rmdate=new HashSet<>();
        remindData=new ArrayList<>();
        taskid=new ArrayList<>();

        edtDescription=findViewById(R.id.edtDescription);
        btnSubmitRemindDate=findViewById(R.id.btnSubmitRemindDate);

        txtRemindedDate=findViewById(R.id.txtRemindedDate);
        imgRemindDate=findViewById(R.id.imgRemindDate);


        imgClose=findViewById(R.id.imgClose);
        fabadd=findViewById(R.id.fabadd);
        card_remind=findViewById(R.id.card_remind);
        recyclerRemindDate=findViewById(R.id.recyclerRemindDate);
        imgback=findViewById(R.id.imgback);
        imgRemind=findViewById(R.id.imgRemind);
        imgDatepurchase=findViewById(R.id.imgDatepurchase);
        txtReminddate=findViewById(R.id.txtReminddate);
        txtdatepurchase=findViewById(R.id.txtdatepurchase);
        txtprofile=findViewById(R.id.txtprofile);
        btnAdd=findViewById(R.id.btnAdd);
        edtremarks=findViewById(R.id.edtremarks);
        edtassetamount=findViewById(R.id.edtassetamount);
        spassetname=findViewById(R.id.spassetname);
      //  txtvisitlogin=findViewById(R.id.txtvisitlogin);
        btnDocuments=findViewById(R.id.btnDocuments);
        recyclerDocuments=findViewById(R.id.recyclerDocuments);



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });




        String languagedata = LocaleHelper.getPersistedData(AddAssetActivity.this, "en");
        Context conte= LocaleHelper.setLocale(AddAssetActivity.this, languagedata);

        final Resources resources=conte.getResources();


        txtprofile.setText(resources.getString(R.string.assetsetup));
        btnAdd.setText(resources.getString(R.string.save));
     //   txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        edtassetamount.setHint(resources.getString(R.string.openingbalance));
        txtdatepurchase.setText(Utils.getCapsSentences(AddAssetActivity.this,resources.getString(R.string.purchasedate)));

        txtReminddate.setText(Utils.getCapsSentences(AddAssetActivity.this,resources.getString(R.string.reminddate)));
        edtremarks.setHint(resources.getString(R.string.enterremarks));


        Utils.showTutorial(resources.getString(R.string.listofmyassetstutorial),AddAssetActivity.this,Utils.Tutorials.addassettutorial);


        if(commonData!=null)
        {

            try{

                JSONObject jsonObject=new JSONObject(commonData.getData());


                if(jsonObject.has("assetfiles")) {

                    String assetfiledata = jsonObject.getString("assetfiles");

                    JSONArray jsonArrayfiles = new JSONArray(assetfiledata);
                    for (int i = 0; i < jsonArrayfiles.length(); i++) {
                        JSONObject jsonObject1 = jsonArrayfiles.getJSONObject(i);

                        assetfilelist.add(jsonObject1.toString());

                    }

                    if (assetfilelist.size() > 0) {

                        showFileAdapter();
                    }
                }


              purchasedate=jsonObject.getString("purchase_date");
                JSONArray jsonArray=jsonObject.getJSONArray("remind_date");
                remindData.clear();

//                if(!jsondate.equalsIgnoreCase(""))
//                {

                    for (int i=0;i<jsonArray.length();i++)
                    {

                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        String id=jsonObject1.getString("Task");


                        List<CommonData>commonData_task=new DatabaseHelper(AddAssetActivity.this).getDataByID(id,Utils.DBtables.TABLE_TASK);


                        if(commonData_task.size()>0) {


                            JSONObject jsonObject2=new JSONObject(commonData_task.get(0).getData());


                            RemindData remindData1 = new RemindData();
                            remindData1.setId(id+"");
                            remindData1.setDate(jsonObject2.getString("date"));
                            remindData1.setDescription(jsonObject2.getString("name"));

                            remindData.add(remindData1);
                        }
//                        remindDates.add(remindDate);




                    }



                    if(remindData.size()>0)
                    {


                        recyclerRemindDate.setVisibility(View.VISIBLE);
                        recyclerRemindDate.setLayoutManager(new LinearLayoutManager(AddAssetActivity.this));

                        reminddateSelectAdapter=new ReminddateSelectAdapter(AddAssetActivity.this,remindData);
                        recyclerRemindDate.setAdapter(reminddateSelectAdapter);




                    }


               // }



               // txtReminddate.setText(remind_date);
                txtdatepurchase.setText(purchasedate);


                assetid=jsonObject.getString("name");
                edtremarks.setText(jsonObject.getString("remarks"));
                edtassetamount.setText(jsonObject.getString("amount"));

                btnAdd.setText(resources.getString(R.string.update));
               // txtprofile.setText("Update asset");


            }catch (Exception e)
            {

            }






        }

        btnDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //uploaddoc=new Dialog(AddAssetActivity.this);
                DisplayMetrics displayMetrics = new DisplayMetrics();
             getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

             double a=displayMetrics.heightPixels/1.2;
                int height =(int)a ;
                int width = displayMetrics.widthPixels;

                uploaddoc = new Dialog(AddAssetActivity.this);
                uploaddoc.requestWindowFeature(Window.FEATURE_NO_TITLE);
                uploaddoc.setContentView(R.layout.layout_uploaddoc);

                uploaddoc.getWindow().setLayout(width,height);

                Spinner spdoc=uploaddoc.findViewById(R.id.spdoc);
                 edtfileTitle=uploaddoc.findViewById(R.id.edtassetamount);
                 imgphoto=uploaddoc.findViewById(R.id.imgphoto);
                Button btnDocuments=uploaddoc.findViewById(R.id.btnDocuments);
                 layout_imagepick=uploaddoc.findViewById(R.id.layout_imagepick);
                 layout_filepick=uploaddoc.findViewById(R.id.layout_filepick);
                txtFilepath=uploaddoc.findViewById(R.id.txtFilepath);

                Button btnFile=uploaddoc.findViewById(R.id.btnFile);

                btnFile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                        chooseFile.setType("*/*");
                        startActivityForResult(
                                Intent.createChooser(chooseFile, "Choose a file"),
                                PICKFILE_RESULT_CODE
                        );
                    }
                });

                spdoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if(position==0)
                        {
                            layout_imagepick.setVisibility(View.VISIBLE);
                            layout_filepick.setVisibility(View.GONE);
                        }
                        else {

                            layout_imagepick.setVisibility(View.GONE);
                            layout_filepick.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                imgphoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(ContextCompat.checkSelfPermission(AddAssetActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                        {

                            ActivityCompat.requestPermissions(AddAssetActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},111);

                        }
                        else {

                          //  showFileOptionDialog();

                            Intent i = new Intent(
                                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, PICKIOmage_RESULT_CODE);


                        }
                    }
                });

                btnDocuments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(!edtfileTitle.getText().toString().equalsIgnoreCase(""))
                        {
                            
                            if(b!=null) {

                                GoogleSignInOptions signInOptions =
                                        new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                                .requestEmail()
                                                .requestScopes(Drive.SCOPE_FILE)
                                                .build();
                                googleSignInClient = GoogleSignIn.getClient(AddAssetActivity.this, signInOptions);

                                startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
                            }
                            else{
                                Utils.showAlertWithSingle(AddAssetActivity.this, "Select a file", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });
                            }

                        }
                        else {

                            Utils.showAlertWithSingle(AddAssetActivity.this, "Enter title", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });
                        }


                    }
                });






uploaddoc.show();
            }
        });


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // Toast.makeText(AddAssetActivity.this,resources.getString(R.string.addassetsettings),Toast.LENGTH_SHORT).show();


              //  startActivity(new Intent(AddAssetActivity.this, AccountsettingsActivity.class));


                Intent i= new Intent(AddAssetActivity.this, AccountsettingsActivity.class);
                i.putExtra(Utils.Requestcode.AccVoucherAll,ForAccountSettingsAssetRequestcode);
                startActivityForResult(i,ForAccountSettingsAssetRequestcode);






            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    if(!edtassetamount.getText().toString().equalsIgnoreCase(""))
                    {

                            if(reminddateSelectAdapter!=null&&reminddateSelectAdapter.getRemindDataList().size()>0)
                            {

                                try{



                                    String id=((CommonData)spassetname.getSelectedItem()).getId();

                                    CommonData cm=(CommonData)spassetname.getSelectedItem();

                                    String Accountname=new JSONObject(cm.getData()).getString("Accountname");


                                    JSONArray jsonArray=new JSONArray();
                                    JSONArray jsonArray_doc=new JSONArray();

                                    for (RemindData rmdate:reminddateSelectAdapter.getRemindDataList())
                                    {

                                        if(!rmdate.getDescription().equalsIgnoreCase("")&&!rmdate.getDate().equalsIgnoreCase("")) {



                                            if(!rmdate.getId().equalsIgnoreCase("0"))
                                            {

                                                JSONObject jsonObject_task = new JSONObject();
                                                jsonObject_task.put("name", rmdate.getDescription());

                                                jsonObject_task.put("date", rmdate.getDate());
                                                jsonObject_task.put("time", "");
                                                jsonObject_task.put("status", 0);



                                                List<CommonData>commonData_task=new DatabaseHelper(AddAssetActivity.this).getDataByID(rmdate.getId(),Utils.DBtables.TABLE_TASK);


                                                if(commonData_task.size()>0) {


                                                    taskid.add(rmdate.getId()+"");

                                                    new DatabaseHelper(AddAssetActivity.this).updateData(rmdate.getId(),jsonObject_task.toString(),Utils.DBtables.TABLE_TASK);
                                                }

                                                try {

                                                    Calendar calendar = Calendar.getInstance();

                                                    DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                                                    Date dtc=dateFormat.parse(rmdate.getDate());
                                                    calendar.setTime(dtc);


                                                    Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                                                    myIntent.putExtra("Task", rmdate.getDescription());
                                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                                                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                                                    alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

                                                }catch (Exception e)

                                                {

                                                }

                                            }
                                            else {

                                                JSONObject jsonObject_task = new JSONObject();
                                                jsonObject_task.put("name", rmdate.getDescription());

                                                jsonObject_task.put("date", rmdate.getDate());
                                                jsonObject_task.put("time", "");
                                                jsonObject_task.put("status", 0);

                                                long rowid=    new DatabaseHelper(AddAssetActivity.this).addData(Utils.DBtables.TABLE_TASK, jsonObject_task.toString());


                                                taskid.add(rowid+"");




                                                try {

                                                    Calendar calendar = Calendar.getInstance();

                                                    DateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                                                    Date dtc=dateFormat.parse(rmdate.getDate());
                                                    calendar.setTime(dtc);


                                                    Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                                                    myIntent.putExtra("Task", rmdate.getDescription());
                                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                                                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                                                    alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

                                                }catch (Exception e)

                                                {

                                                }

                                            }





                                        }



                                    }

                                    if(assetfilelist.size()>0)
                                    {
                                        for (String a : assetfilelist)
                                        {

                                            JSONObject js=new JSONObject(a);
                                            jsonArray_doc.put(js);


                                        }

                                    }



                                    for (String taskrow:taskid)
                                    {
                                      JSONObject jsonObject=new JSONObject();
                                      jsonObject.put("Task",taskrow);
                                      jsonArray.put(jsonObject);
                                    }


                                    JSONObject jsonObject=new JSONObject();
                                    jsonObject.put("name",id);
                                    jsonObject.put("amount",edtassetamount.getText().toString());
                                    jsonObject.put("purchase_date",purchasedate);
                                    jsonObject.put("remind_date",jsonArray);
                                    jsonObject.put("remarks",edtremarks.getText().toString());
                                    jsonObject.put("assetfiles",jsonArray_doc.toString());

                                    uploadData(jsonObject.toString());




                                }catch (Exception e)
                                {


                                }



//

                            }
                            else {

                                Toast.makeText(AddAssetActivity.this,resources.getString(R.string.reminddate),Toast.LENGTH_SHORT).show();
                            }

//
                    }
                    else {

                        Toast.makeText(AddAssetActivity.this,resources.getString(R.string.openingbalance),Toast.LENGTH_SHORT).show();
                    }



            }
        });

        txtdatepurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        txtReminddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                recyclerRemindDate.setVisibility(View.VISIBLE);

                RemindData remindData1=new RemindData();

                remindData1.setId("0");
                    remindData1.setDate("");
                    remindData1.setDescription("");

                 remindData.add(remindData1);

                reminddateSelectAdapter= new ReminddateSelectAdapter(AddAssetActivity.this,remindData);

                recyclerRemindDate.setLayoutManager(new LinearLayoutManager(AddAssetActivity.this));
                recyclerRemindDate.setAdapter(reminddateSelectAdapter);

                //showDatePicker(1);

               // card_remind.setVisibility(View.VISIBLE);
            }
        });

      //  edtDescription=findViewById(R.id.edtDescription);



        imgRemindDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        txtRemindedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);
            }
        });

        btnSubmitRemindDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!remind_date.equalsIgnoreCase("")) {

                    if(updatestatus!=-1)
                    {
                        remindData.get(updatestatus).setDate(remind_date);
                        remindData.get(updatestatus).setDescription(edtDescription.getText().toString());

                    }
                    else {


                        RemindData remindData1 = new RemindData();
                        remindData1.setDate(remind_date);
                        remindData1.setDescription(edtDescription.getText().toString());
                        remindData.add(remindData1);
                    }


                    card_remind.setVisibility(View.GONE);


                    remindDates.clear();

                    rmdate.add(remind_date);

                    Iterator<String> it = rmdate.iterator();
                    while (it.hasNext()) {
                        remindDates.add(it.next());
                    }


                    recyclerRemindDate.setVisibility(View.VISIBLE);
                    recyclerRemindDate.setLayoutManager(new GridLayoutManager(AddAssetActivity.this, 2));

                    recyclerRemindDate.setAdapter(new RemindDateAdapter(AddAssetActivity.this, remindData));

                    remind_date="";

                    edtDescription.setText("");
                    txtRemindedDate.setText("Select a remind date");

                    recyclerRemindDate.getAdapter().notifyDataSetChanged();
                    updatestatus=-1;
                }
                else {

                    Toast.makeText(AddAssetActivity.this,"Select a remind date",Toast.LENGTH_SHORT).show();
                }
            }
        });


        imgDatepurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                card_remind.setVisibility(View.VISIBLE);

               // showDatePicker(1);
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                card_remind.setVisibility(View.GONE);
            }
        });

        getAssetData();

        spassetname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {

                    CommonData cm = (CommonData) spassetname.getSelectedItem();

                    JSONObject jsonObject = new JSONObject(cm.getData());
                    String Amount = jsonObject.getString("Amount");
                    double amt =0;


                     amt = Double.parseDouble(Amount);

                    String Type=jsonObject.getString("Type");


                  //  amt = Double.parseDouble(Amount);

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        amt=amt*-1;
                    }

                    String id = cm.getId();

                    Calendar calendar = Calendar.getInstance();
                    int m = calendar.get(Calendar.MONTH) + 1;
                    int d = calendar.get(Calendar.DAY_OF_MONTH);
                    int y = calendar.get(Calendar.YEAR);
                    String enddate = d + "-" + m + "-" + y;

                    if(commonData==null) {
                        double closingbalance = getClosingBalance(amt, id, enddate, "");

                        DecimalFormat decimalFormatter = new DecimalFormat("############");


                        edtassetamount.setText(decimalFormatter.format(closingbalance) );

                    }
                }catch (Exception e)
                {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            switch (requestCode) {

                case PICKFILE_RESULT_CODE:

                    if (data != null) {

                        String result;

                        Uri uri = data.getData();
                        File file = new File(uri.getPath());//create path from uri
//                    final String[] split = file.getPath().split(":");//split the path.
//                    String p = split[1];//assign it to a string(your choice).
                        // String filePath= null;
                        try {
                            result = PathUtil.getPath(AddAssetActivity.this, uri);

                            //  Log.e("Filepath", result);
                            fileuploadToDrive = new File(result);

                            if (fileuploadToDrive.exists()) {
                                Log.e("uploadedfileexist", "true");
                            } else {

                                Log.e("uploadedfileexist", "false");
                            }

                            filePath = fileuploadToDrive.getAbsolutePath();
                            if (txtFilepath != null) {
                                txtFilepath.setText(filePath);
                            }

                            Uri returnUri = data.getData();
                            mimetype = getContentResolver().getType(returnUri);
                            Toast.makeText(AddAssetActivity.this, mimetype, Toast.LENGTH_SHORT).show();


                            InputStream fis = null;
                            try {
                                fis = getContentResolver().openInputStream(uri);
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                byte[] bi = new byte[1024];
                                for (int readNum; (readNum = fis.read(bi)) != -1; ) {
                                    bos.write(bi, 0, readNum);
                                }
                                b = bos.toByteArray();
                                Toast.makeText(AddAssetActivity.this, b.toString(), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Log.d("mylog", e.toString());

                                //Toast.makeText(DocumentManagerActivity.this,"Error on byte array :"+e.toString(),Toast.LENGTH_SHORT).show();
                            }

//                            if (isToUpdate) {
//
//                                GoogleSignInOptions signInOptions =
//                                        new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                                                .requestEmail()
//                                                .requestScopes(Drive.SCOPE_FILE)
//                                                .build();
//                                googleSignInClient = GoogleSignIn.getClient(AddAssetActivity.this, signInOptions);
//
//                                // The result of the sign-in Intent is handled in onActivityResult.
//                                startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
//                            }


                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }

                    }
                    break;


                case PICKIOmage_RESULT_CODE:

                    if (resultCode == -1) {


                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;

                        final Dialog d = new Dialog(AddAssetActivity.this);
                        d.setContentView(R.layout.layout_cropdialog);


                        ImageView imgback = d.findViewById(R.id.imgback);

                        ImageView imgcrop = d.findViewById(R.id.imgcrop);

                        final CropImageView cropImageView = d.findViewById(R.id.cropImageView);

                        cropImageView.setImageUriAsync(data.getData());

                        imgback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                d.dismiss();
                            }
                        });

                        imgcrop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                d.dismiss();

                                try {
                                    Bitmap cropped = cropImageView.getCroppedImage();

                                    Long tsLong = System.currentTimeMillis() / 1000;
                                    String ts = tsLong.toString();
//                                    if (isToUpdate) {
//                                        fileuploadToDrive = new File(getExternalCacheDir() + "/" + filenameToupdate + ".jpg");
//                                    } else {

                                        fileuploadToDrive = new File(getExternalCacheDir() + "/" + new Date().getTime() + "_" + ts + ".jpg");


                                   // }
                                    filePath = fileuploadToDrive.getAbsolutePath();
//                                    if (txtfilepath != null) {
//                                        txtfilepath.setText(filePath);
//                                    }


                                    if (!fileuploadToDrive.exists()) {

                                        fileuploadToDrive.createNewFile();
                                    }
                                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                    cropped.compress(Bitmap.CompressFormat.JPEG, 60, bytes);

                                    FileOutputStream fo = new FileOutputStream(fileuploadToDrive);
                                    fo.write(bytes.toByteArray());
                                    fo.close();


                                    Bitmap bm = BitmapFactory.decodeFile(fileuploadToDrive.getAbsolutePath());
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                                    imgphoto.setImageBitmap(cropped);

                                    b = baos.toByteArray();

                                    mimetype = "image/*";

//                                Uri returnUri = Uri.fromFile(fileuploadToDrive);
//                                String mimeType = getContentResolver().getType(returnUri);
//                                Toast.makeText(DocumentManagerActivity.this,mimeType,Toast.LENGTH_SHORT).show();

//                                    if (isToUpdate) {

//
//                                    }


                                } catch (Exception e) {

                                }


                            }
                        });

                        d.getWindow().setLayout(width, height);
                        d.show();

                    }


                    break;

                case REQUEST_CODE_SIGN_IN:
                    if (data != null) {
//                        if (isToUpdate) {
//                            handleResultToUpdate(data);
//                        } else if (istodownload) {
//                            handleResultToDownload(data);
//                        } else {
                            handleSignInResult(data);
//                        }


                    }

                    break;

                case ForAccountSettingsAssetRequestcode:

                    newaccount=data.getStringExtra("AccountAdded");
                    getAssetData();

                    break;

                case REQUEST_CODE_SIGN_INFORDELETION:

                    handleSignInResultForDeletion(data);

                    break;

                case REQUEST_CODE_SIGN_IN_DOWNLOAD:

                    handleResultToDownload(data);

                    break;
            }

        }





    }

    public void handleResultToDownload(Intent result)
    {

        ProgressFragment  progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");



        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        GoogleSignInAccount   signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        AddAssetActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();

                        Toast.makeText(AddAssetActivity.this,"Created google service id",Toast.LENGTH_SHORT).show();

                        if(!fileid.equalsIgnoreCase("")) {


                            try {

                                // Toast.makeText(DocumentManagerActivity.this, filenametoDownload, Toast.LENGTH_SHORT).show();



                                // Toast.makeText(DocumentManagerActivity.this, "file created", Toast.LENGTH_SHORT).show();


//                                Executor executor=new DirectExecutor();
//
//                                executor.execute(new Runnable() {
//                                    @Override
//                                    public void run() {
//
//                                    }
//                                });
//
//
//                                   // OutputStream outputStream = new ByteArrayOutputStream();




                            }catch (Exception e)
                            {

                            }


                            Executor mExecutor = Executors.newSingleThreadExecutor();
                            Tasks.call(mExecutor, () -> {
                                try {

                                    File downloadfile = new File(getExternalCacheDir() + "/" + fname);

                                    if (!downloadfile.exists()) {

                                        downloadfile.createNewFile();
                                    } else {

                                        downloadfile.delete();
                                        downloadfile.createNewFile();
                                    }

                                    OutputStream outputStream =new  FileOutputStream(downloadfile);

                                    // com.google.api.services.drive.Drive.Files

                                    try {

                                        //  Toast.makeText(DocumentManagerActivity.this, "file is downloading", Toast.LENGTH_SHORT).show();

                                        googleDriveService.files().get(fileid)
                                                .executeMediaAndDownloadTo(outputStream);

                                        // Toast.makeText(DocumentManagerActivity.this, "file is downloading", Toast.LENGTH_SHORT).show();


                                    } catch (Exception e) {
                                        e.printStackTrace();

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(AddAssetActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                                            }
                                        });

                                        // Toast.makeText(DocumentManagerActivity.this,e.toString(),Toast.LENGTH_SHORT).show();

                                    }


                                  // istodownload=false;
                                    fileid="";

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utils.showAlertWithSingle(AddAssetActivity.this, "File downloaded successfully", new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });
                                            progressFragment.dismiss();

//                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//
//                                                Uri photoURI = FileProvider.getUriForFile(DocumentManagerActivity.this, getApplicationContext().getPackageName() + ".provider", downloadfile);
//
//                                                Intent intent = new Intent();
//                                                intent.setAction(Intent.ACTION_VIEW);
//                                                intent.setDataAndType(photoURI, "image/*");
//                                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                                startActivity(intent);
//
//                                            } else {
//
//                                                Intent intent = new Intent();
//                                                intent.setAction(Intent.ACTION_VIEW);
//                                                intent.setDataAndType(Uri.fromFile(downloadfile), "image/*");
//                                                startActivity(intent);
//                                            }

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                                Uri photoURI = FileProvider.getUriForFile(AddAssetActivity.this, getApplicationContext().getPackageName() + ".provider", downloadfile);

                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setType("*/*");
                                                intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                                                startActivity(Intent.createChooser(intent, "Share Bill"));

                                            } else {

                                                Intent intent = new Intent(Intent.ACTION_SEND);
                                                intent.setType("*/*");
                                                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(downloadfile));
                                                startActivity(Intent.createChooser(intent, "Share Image"));
                                            }



                                        }
                                    });





                                    fileid="";
                                }catch (Exception e)
                                {
                                    Toast.makeText(AddAssetActivity.this,"error:"+e.toString(),Toast.LENGTH_SHORT).show();

                                }

                                return null;

                            });




                        }
                        else {


                            Toast.makeText(AddAssetActivity.this,"file id is missing",Toast.LENGTH_SHORT).show();


                            progressFragment.dismiss();
                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Log.e("TAAG",e.toString());

            }
        });
    }


    private void handleSignInResultForDeletion(Intent result) {

        ProgressFragment  progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");






        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        GoogleSignInAccount    signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        Toast.makeText(AddAssetActivity.this,"started",Toast.LENGTH_SHORT).show();

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        AddAssetActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();


                            Executor mExecutor = Executors.newSingleThreadExecutor();
                            //  Toast.makeText(DocumentManagerActivity.this,"filepath is not null",Toast.LENGTH_SHORT).show();


                            Tasks.call(mExecutor, () -> {

                                //   progressFragment.dismiss();

                                // googleDriveService.files().delete(fileid).execute();

                                Tasks.call(mExecutor, () -> {
                                    com.google.api.services.drive.model.File metadata = new com.google.api.services.drive.model.File()
                                            .setParents(Collections.singletonList("root"))

                                            .setName(fileuploadToDrive.getName());

                                    com.google.api.services.drive.model.File googleFile = googleDriveService.files().create(metadata).execute();
                                    if (googleFile == null) {
                                        throw new IOException("Null result when requesting file creation.");
                                    }
                                    fileid = googleFile.getId();


                                    Tasks.call(mExecutor, () -> {

                                        //progressFragment.dismiss();
                                        // Create a File containing any metadata changes.
//                                        com.google.api.services.drive.model.File metadata1 = new com.google.api.services.drive.model.File().setName(fileuploadToDrive.getName());
//
//                                        // Convert content to an AbstractInputStreamContent instance.
//                                        // ByteArrayContent contentStream = ByteArrayContent.fromString("*/*", uploadgdrivedata);
//
//                                        // Update the metadata and contents.
//                                        ByteArrayContent contentStream = new ByteArrayContent(mimetype, b);

                                        googleDriveService.files().delete(fileid).execute();
                                        //saveFileDb();
                                        //updateGoogleDriveFileId(fileid);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                try {


                                                    if(progressFragment!=null)
                                                    {
                                                        progressFragment.dismiss();
                                                    }



                                                    //showFileAdapter();


                                                }catch (Exception e)
                                                {

                                                }


                                            }
                                        });



                                        return null;
                                    });


                                    return googleFile.getId();

                                });



                                return null;
                            });




















                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Utils.showAlertWithSingle(AddAssetActivity.this,e.toString(),null);

                Log.e("TAAG",e.toString());

            }
        });
    }

    private void handleSignInResult(Intent result) {

      ProgressFragment  progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");






        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        GoogleSignInAccount    signInAccount=googleSignInAccount;
                        String token=   googleSignInAccount.getId();
                        Log.e("TAAG",token);

                        Toast.makeText(AddAssetActivity.this,"started",Toast.LENGTH_SHORT).show();

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        AddAssetActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();
                        if(!filePath.equalsIgnoreCase("")) {

                            Executor mExecutor = Executors.newSingleThreadExecutor();
                            //  Toast.makeText(DocumentManagerActivity.this,"filepath is not null",Toast.LENGTH_SHORT).show();


                            Tasks.call(mExecutor, () -> {

                                //   progressFragment.dismiss();

                                // googleDriveService.files().delete(fileid).execute();

                                Tasks.call(mExecutor, () -> {
                                    com.google.api.services.drive.model.File metadata = new com.google.api.services.drive.model.File()
                                            .setParents(Collections.singletonList("root"))

                                            .setName(fileuploadToDrive.getName());

                                    com.google.api.services.drive.model.File googleFile = googleDriveService.files().create(metadata).execute();
                                    if (googleFile == null) {
                                        throw new IOException("Null result when requesting file creation.");
                                    }
                                    fileid = googleFile.getId();


                                    Tasks.call(mExecutor, () -> {

                                        //progressFragment.dismiss();
                                        // Create a File containing any metadata changes.
                                        com.google.api.services.drive.model.File metadata1 = new com.google.api.services.drive.model.File().setName(fileuploadToDrive.getName());

                                        // Convert content to an AbstractInputStreamContent instance.
                                        // ByteArrayContent contentStream = ByteArrayContent.fromString("*/*", uploadgdrivedata);

                                        // Update the metadata and contents.
                                        ByteArrayContent contentStream = new ByteArrayContent(mimetype, b);

                                        googleDriveService.files().update(fileid, metadata1, contentStream).execute();
                                        //saveFileDb();
                                        //updateGoogleDriveFileId(fileid);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                try {

                                                    JSONObject js=new JSONObject();
                                                    js.put("name",edtfileTitle.getText().toString());
                                                    js.put("fileid",fileid);
                                                    js.put("filename",fileuploadToDrive.getName());
                                                    assetfilelist.add(js.toString());
                                                    b=null;
                                                    fileid="";
                                                    filePath="";
                                                    if(progressFragment!=null)
                                                    {
                                                        progressFragment.dismiss();
                                                    }

                                                    if(uploaddoc!=null) {

                                                        uploaddoc.dismiss();
                                                    }

                                                    showFileAdapter();


                                                }catch (Exception e)
                                                {

                                                }


                                            }
                                        });



                                        return null;
                                    });


                                    return googleFile.getId();

                                });



                                return null;
                            });



                        }
                        else {


                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Utils.showAlertWithSingle(AddAssetActivity.this,e.toString(),null);

                Log.e("TAAG",e.toString());

            }
        });
    }



    public void showFileAdapter()
    {

        recyclerDocuments.setLayoutManager(new LinearLayoutManager(AddAssetActivity.this));

        recyclerDocuments.setAdapter(new AssetFileListAdapter(AddAssetActivity.this,assetfilelist));
    }


    public void  downloadFile(String fileid,String fname)
    {
        this.fileid=fileid;
        this.fname=fname;
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        googleSignInClient = GoogleSignIn.getClient(AddAssetActivity.this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN_DOWNLOAD);
    }





    public void deleteAndRefreshFile(int position)
    {

        String languagedata = LocaleHelper.getPersistedData(AddAssetActivity.this, "en");
        Context conte= LocaleHelper.setLocale(AddAssetActivity.this, languagedata);

        final Resources resources=conte.getResources();

        Utils.showAlert(AddAssetActivity.this, resources.getString(R.string.deleteconfirm), new DialogEventListener() {
            @Override
            public void onPositiveButtonClicked() {

                try {
                    JSONObject js = new JSONObject(assetfilelist.get(position));




                    fileid = js.getString("fileid");


                    assetfilelist.remove(position);
                    showFileAdapter();

//            GoogleSignInOptions signInOptions =
//                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                            .requestEmail()
//                            .requestScopes(Drive.SCOPE_FILE)
//                            .build();
//            googleSignInClient = GoogleSignIn.getClient(AddAssetActivity.this, signInOptions);
//
//            // The result of the sign-in Intent is handled in onActivityResult.
//            startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_INFORDELETION);
                }catch (Exception e)
                {

                }



            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });





    }




    public void showDatePicker(final int code)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddAssetActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;
                if(code==0) {

                    purchasedate = i2 + "-" + m + "-" + i;
                    txtdatepurchase.setText(i2+"-"+m+"-"+i);
                }
                else {

                    remind_date = i2 + "-" + m + "-" + i;
                    txtRemindedDate.setText(remind_date);
                   // txtReminddate.setText(i2+"-"+m+"-"+i);




                }




            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

    public void removeDate(int position)
    {

        if(!remindData.get(position).getId().equalsIgnoreCase("0"))
        {

            List<CommonData>commonData_task=new DatabaseHelper(AddAssetActivity.this).getDataByID(remindData.get(position).getId(),Utils.DBtables.TABLE_TASK);


            if(commonData_task.size()>0) {

                Utils.playSimpleTone(AddAssetActivity.this);
                new DatabaseHelper(AddAssetActivity.this).deleteData(remindData.get(position).getId(),Utils.DBtables.TABLE_TASK);
            }
            remindData.remove(position);

        }
        else {

            remindData.remove(position);
        }

        if(remindData.size()>0) {
            recyclerRemindDate.setVisibility(View.VISIBLE);

            recyclerRemindDate.setLayoutManager(new LinearLayoutManager(AddAssetActivity.this));

            reminddateSelectAdapter=new ReminddateSelectAdapter(AddAssetActivity.this, remindData);

            recyclerRemindDate.setAdapter(reminddateSelectAdapter);
        }
        else {

            recyclerRemindDate.setVisibility(View.GONE);
            remind_date="";
        }


    }

    public void updateData(int position)
    {

        updatestatus=position;
        RemindData remiData=remindData.get(position);

        card_remind.setVisibility(View.VISIBLE);

        txtRemindedDate.setText(remiData.getDate());
        remind_date=remiData.getDate();

        edtDescription.setText(remiData.getDescription());


    }



    public void uploadData(String data)
    {

        if(commonData!=null)
        {
            new DatabaseHelper(AddAssetActivity.this).updateData(commonData.getId(),data,Utils.DBtables.TABLE_ASSET);

            Utils.playSimpleTone(AddAssetActivity.this);

        }
        else {

            new DatabaseHelper(AddAssetActivity.this).addData(Utils.DBtables.TABLE_ASSET,data);
        }


        addreminder();

        edtassetamount.setText("");

        edtremarks.setText("");
//        txtdatepurchase.setText("Select date of purchase");
//        txtReminddate.setText("Select remind date");
        purchasedate="";
        remind_date="";



        String languagedata = LocaleHelper.getPersistedData(AddAssetActivity.this, "en");
        Context conte= LocaleHelper.setLocale(AddAssetActivity.this, languagedata);

        Resources resources=conte.getResources();


        txtprofile.setText(resources.getString(R.string.assetsetup));
        btnAdd.setText(resources.getString(R.string.save));
      //  txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        edtassetamount.setHint(resources.getString(R.string.openingbalance));
        txtdatepurchase.setText(resources.getString(R.string.purchasedate));

        txtReminddate.setText(resources.getString(R.string.reminddate));
        edtremarks.setHint(resources.getString(R.string.enterremarks));

        onBackPressed();


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getAssetData();
    }



    public void addreminder()
    {


        try {


            String subject = "";


            String id = ((CommonData) spassetname.getSelectedItem()).getId();


            List<CommonData> cms = new DatabaseHelper(AddAssetActivity.this).getDataByID(id, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if (cms.size() > 0) {
                //subject=


                JSONObject jsonObject = new JSONObject(cms.get(0).getData());


                subject = jsonObject.getString("Accountname");

            }



            String rmdate[] = remind_date.split("-");

            int rminddate = Integer.parseInt(rmdate[0]);
            int rmindmonth = Integer.parseInt(rmdate[1]);
            int rmindYear = Integer.parseInt(rmdate[2]);


            Calendar calendar = Calendar.getInstance();

            int currentyear = calendar.get(Calendar.YEAR);
            int currentmonth = calendar.get(Calendar.MONTH);
            int currentday = calendar.get(Calendar.DAY_OF_MONTH);

            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);


            String time = hour + ":" + minute;

            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
            Date dat = null;
            try {
                dat = fmt.parse(time);
            } catch (ParseException e) {

                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");

            String timeselected = fmtOut.format(dat);

            JSONObject js = new JSONObject();
            js.put("name", subject);
            js.put("date", currentday + "-" + currentmonth + "-" + currentyear);
            js.put("time", timeselected);
            js.put("status", 0);


            new DatabaseHelper(AddAssetActivity.this).addData(Utils.DBtables.TABLE_TASK, js.toString());
            Utils.playSimpleTone(AddAssetActivity.this);

            Calendar c = Calendar.getInstance();

            Log.e("HOUR OF DAY",calendar.get(Calendar.HOUR)+"");

            c.set(Calendar.MONTH, rmindmonth-1);
            c.set(Calendar.YEAR, rmindYear);
            c.set(Calendar.DAY_OF_MONTH, rminddate);

            c.set(Calendar.HOUR, hour);
            c.set(Calendar.MINUTE, minute);
            c.set(Calendar.SECOND, 0);



            c.set(Calendar.AM_PM, Calendar.AM);


            Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
            myIntent.putExtra("Task",subject);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent,0);

            AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC, c.getTimeInMillis(), pendingIntent);






        }catch (Exception e)
        {

        }


    }

















    public void getAssetData()
    {

        // spinvestname


        List<CommonData> commonData_filtered=new ArrayList<>();

        List<CommonData>commonData=new DatabaseHelper(AddAssetActivity.this).getAccountSettingsData();

        for (CommonData cm:commonData)
        {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accounttype");

                if(acctype.equalsIgnoreCase("Asset account"))
                {



                    commonData_filtered.add(cm);
                }


                //




            }catch (Exception e)
            {

            }

        }


        BankSpinnerAdapter bankSpinnerAdapter=new BankSpinnerAdapter(AddAssetActivity.this,commonData_filtered);
        spassetname.setAdapter(bankSpinnerAdapter);



        for (int i=0;i<commonData_filtered.size();i++)

        {

            CommonData cm=commonData_filtered.get(i);

            if(cm.getId().equalsIgnoreCase(assetid))
            {
                try {

//                    JSONObject jsonObject = new JSONObject(cm.getData());
//                    String Amount = jsonObject.getString("Amount");
//
//                    double amt = Double.parseDouble(Amount);
//
//                    String id = cm.getId();
//
//                    Calendar calendar = Calendar.getInstance();
//                    int m = calendar.get(Calendar.MONTH) + 1;
//                    int d = calendar.get(Calendar.DAY_OF_MONTH);
//                    int y = calendar.get(Calendar.YEAR);
//                    String enddate = d + "-" + m + "-" + y;
//                    double closingbalance=getClosingBalance(amt,id,enddate,"");
//                    edtassetamount.setText(closingbalance+"");

                    spassetname.setSelection(i);
                    break;
                }catch (Exception e)
                {

                }
            }





        }


        if(!newaccount.equalsIgnoreCase(""))
        {
            try {

                for (int i = 0; i < commonData_filtered.size(); i++) {
                    CommonData cm = commonData_filtered.get(i);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    String data=jsonObject.getString("Accountname");

                    String Amount=jsonObject.getString("Amount");

                   // double amt=Double.parseDouble(Amount);

                    double amt =0;


                    amt = Double.parseDouble(Amount);

                    String Type=jsonObject.getString("Type");


                    //  amt = Double.parseDouble(Amount);

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        amt=amt*-1;
                    }

                    String id=cm.getId();

                    Calendar calendar=Calendar.getInstance();
                    int m=calendar.get(Calendar.MONTH)+1;
                    int d=calendar.get(Calendar.DAY_OF_MONTH);
                    int y=calendar.get(Calendar.YEAR);
                    String enddate=d+"-"+m+"-"+y;





                    if(data.equalsIgnoreCase(newaccount))
                    {
                        spassetname.setSelection(i);
                        newaccount="";

                        double closingbalance=getClosingBalance(amt,id,enddate,"");

                       // edtassetamount.setText(String.valueOf(closingbalance));

                        DecimalFormat decimalFormatter = new DecimalFormat("###################################################");



                        edtassetamount.setText(decimalFormatter.format(closingbalance) );

                        break;
                    }


                }

            }catch (Exception e)
            {

            }


        }










    }



    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(AddAssetActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }


}
