package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WalletActivity extends AppCompatActivity {

    TextView txtWalletbalance,txtdatepick,txtnetbalance,txtHead,txtnetbalancehead,txtWalletbalancehead;

    ImageView imgDatepick,imgback;

    Spinner spinnerAccountName;

    FloatingActionButton fabadd;

    EditText edtAmount;

    Button btnSave,btndelete;

    Resources resources;

    String cashbankid="0",walletbalance="0",netbalance="0";

    String month_selected="",yearselected="",date="";

    double balance=0;

   // long insetedid;

    Accounts acs=null;

    int paymentvoucher=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        getSupportActionBar().hide();

        balance=getIntent().getDoubleExtra("balance",0);

        acs=(Accounts)getIntent().getSerializableExtra("data");

        paymentvoucher=getIntent().getIntExtra("paymentvoucher",0);

        walletbalance=String.valueOf(walletbalance);

        String languagedata = LocaleHelper.getPersistedData(WalletActivity.this, "en");
        Context context= LocaleHelper.setLocale(WalletActivity.this, languagedata);

        resources=context.getResources();



        txtWalletbalance=findViewById(R.id.txtWalletbalance);
        txtdatepick=findViewById(R.id.txtdatepick);
        txtnetbalance=findViewById(R.id.txtnetbalance);
//        txtvisitlogin=findViewById(R.id.txtvisitlogin);
        txtHead=findViewById(R.id.txtHead);
        txtnetbalancehead=findViewById(R.id.txtnetbalancehead);
        txtWalletbalancehead=findViewById(R.id.txtWalletbalancehead);


        imgDatepick=findViewById(R.id.imgDatepick);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        fabadd=findViewById(R.id.fabadd);
        imgback=findViewById(R.id.imgback);


        edtAmount=findViewById(R.id.edtAmount);
        btnSave=findViewById(R.id.btnSave);
        btndelete=findViewById(R.id.btndelete);

        txtWalletbalance.setText("0 "+resources.getString(R.string.rs));
        edtAmount.setHint(resources.getString(R.string.amount));
        btnSave.setText(resources.getString(R.string.save));
        btndelete.setText(resources.getString(R.string.delete));
        txtdatepick.setText(resources.getString(R.string.date));




      //  txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        txtHead.setText(resources.getString(R.string.addmoneytowallet));
        txtWalletbalancehead.setText(resources.getString(R.string.walletbalance));
        txtnetbalancehead.setText(resources.getString(R.string.netbalance));






        spinnerAccountName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                cashbankid=((CommonData)spinnerAccountName.getSelectedItem()).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WalletActivity.this, AccountsettingsActivity.class));



            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {



            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equalsIgnoreCase(""))
                {
                    double walletba=Double.parseDouble(walletbalance);

                    double enteramount=Double.parseDouble(charSequence.toString());

                    double a=walletba+enteramount;

                    netbalance=String.valueOf(a);



                }
                else {

                    netbalance=walletbalance;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if(acs!=null)
        {

            date=acs.ACCOUNTS_date;
            month_selected=acs.ACCOUNTS_month;
            yearselected=acs.getACCOUNTS_year();
            edtAmount.setText(acs.ACCOUNTS_amount);

            btndelete.setVisibility(View.VISIBLE);

            txtdatepick.setText(date);

            txtHead.setText(resources.getString(R.string.editmoneytowallet));



        }
        else{

            setDate();

            btndelete.setVisibility(View.GONE);

        }

        if(paymentvoucher==1)
        {
            btnSave.setVisibility(View.GONE);
            btndelete.setVisibility(View.GONE);
        }
        else{

            btnSave.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
        }



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(!cashbankid.equalsIgnoreCase("0"))
//                {


                    if(!edtAmount.getText().toString().equalsIgnoreCase(""))

                    {

                        try {





                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("Walletbalance",netbalance);
                            jsonObject.put("date",date);
                            jsonObject.put("cashbank","");
                            jsonObject.put("month",month_selected);
                            jsonObject.put("year",yearselected);
                            jsonObject.put("amount",edtAmount.getText().toString().trim());

                            if(acs!=null)
                            {
                                new DatabaseHelper(WalletActivity.this).updateData(acs.getACCOUNTS_id()+"",jsonObject.toString(),Utils.DBtables.TABLE_WALLET);

                            }
                            else {


                                  new DatabaseHelper(WalletActivity.this).addData(Utils.DBtables.TABLE_WALLET,jsonObject.toString());

                            }

                            onBackPressed();


//                            List<CommonData>commonData=new DatabaseHelper(WalletActivity.this).getData(Utils.DBtables.TABLE_WALLET);
//
//                            double openingbal=0,totalw=0;
//                            if(commonData.size()>0) {
//
//
//                                for (CommonData cm : commonData) {
//
//
//                                    JSONObject jsonObject = new JSONObject(cm.getData());
//
//                                    String Walletbalance = jsonObject.getString("Walletbalance");
//
//
//                                    String month_wallet = jsonObject.getString("month");
//                                    String year_wallet = jsonObject.getString("year");
//                                    String amount_wallet = jsonObject.getString("amount");
//
//                                    String date = jsonObject.getString("date");
//
//                                    if (String.valueOf(selected_month).equalsIgnoreCase(month_wallet) && String.valueOf(yea).equalsIgnoreCase(year_wallet)) {
//
//
//                                        totalw = totalw + Double.parseDouble(amount_wallet);
//
//
//                                        if (date_opening.equalsIgnoreCase("")) {
//
//                                            openingbal = Double.parseDouble(amount_wallet);
//
//
//                                            date_opening = date;
//                                        }
//
//                                    }
//
//                                }
//
//
//                            }

//                            double a=Double.parseDouble(edtAmount.getText().toString());
//
//                            balance=balance+a;
//
//                            walletbalance=String.valueOf(walletbalance);
//
//                            txtWalletbalance.setText(balance+" "+resources.getString(R.string.rs));
//
//                            txtnetbalance.setText(balance+" "+resources.getString(R.string.rs));
//                            edtAmount.setText("");

                           // getWalletBalance();

                        }catch (Exception e)
                        {

                        }





                    }
                    else {


                        Toast.makeText(WalletActivity.this,"Enter amount",Toast.LENGTH_SHORT).show();
                    }


//
//                }
//                else {
//
//
//                    Toast.makeText(WalletActivity.this,"Select cash/bank account",Toast.LENGTH_SHORT).show();
//                }
//
//




            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setMessage("Do you want to delete now ? ");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();


                        if(paymentvoucher==0) {

                            new DatabaseHelper(WalletActivity.this).deleteData(acs.getACCOUNTS_id() + "", Utils.DBtables.TABLE_WALLET);

                        }
                        else{

                            new DatabaseHelper(WalletActivity.this).deleteAccountDataById(acs.getACCOUNTS_id() + "");

                        }

                        onBackPressed();



                    }
                });
                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });

                builder.show();



            }
        });




        addBankAccountData();

        //getWalletBalance();
    }


    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        final int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(WalletActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                date=i2+"-"+m+"-"+i;

                month_selected=m+"";
                yearselected=year+"";

                txtdatepick.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }


    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        addBankAccountData();
    }

    public void addBankAccountData()
    {

        List<CommonData> commonData_filtered=new ArrayList<>();

        List<CommonData>commonData=new DatabaseHelper(WalletActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        for (CommonData cm:commonData)
        {

            try {

                JSONObject jsonObject = new JSONObject(cm.getData());

                String acctype= jsonObject.getString("Accounttype");

                if(acctype.equalsIgnoreCase("Bank"))
                {
                    commonData_filtered.add(cm);
                }

                if(acctype.equalsIgnoreCase("Cash"))
                {
                    commonData_filtered.add(cm);
                }



                //




            }catch (Exception e)
            {

            }

        }


        BankSpinnerAdapter bankSpinnerAdapter=new BankSpinnerAdapter(WalletActivity.this,commonData_filtered);
        spinnerAccountName.setAdapter(bankSpinnerAdapter);


        for (int i=0;i<commonData_filtered.size();i++)
        {

            if(commonData_filtered.get(i).getId().equalsIgnoreCase(cashbankid))
            {

                spinnerAccountName.setSelection(i);
                break;
            }
        }





    }





}
