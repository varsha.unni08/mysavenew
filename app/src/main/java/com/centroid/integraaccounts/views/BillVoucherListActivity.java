package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BillVoucherAdapter;
import com.centroid.integraaccounts.adapter.PaymentVoucherAdapter;
import com.centroid.integraaccounts.adapter.ReceiptAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class BillVoucherListActivity extends AppCompatActivity {


    TextView txtHead;

    ImageView imgback,imgaacsettings,imgbudget;

    LinearLayout layout_head;



    RecyclerView recycler;
    FloatingActionButton fab_addtask;

    TextView txtdatepick;
    ImageView imgDatepick;

    Button submit;

    int m=0,yea=0;

    Spinner spinnerAccountName;
    TextView txtTotal;
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    PaymentVoucherAdapter paymentVoucherAdapter;

    List<PaymentVoucher> paymentVouchers_selected=new ArrayList<>();

//    Button btnsetBudget;


    List<CommonData>cmfiltered;


    Resources resources;

    TextView txtdate,txtaccount,txtamount,txttype,txtaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_voucher_list);
        getSupportActionBar().hide();

        cmfiltered=new ArrayList<>();
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgaacsettings=findViewById(R.id.imgaacsettings);
        imgbudget=findViewById(R.id.imgbudget);
        layout_head=findViewById(R.id.layout_head);




        paymentVouchers_selected=new ArrayList<>();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);

        txtdatepick=findViewById(R.id.txtdatepick);
        imgDatepick=findViewById(R.id.imgDatepick);
        submit=findViewById(R.id.submit);
        spinnerAccountName=findViewById(R.id.spinnerAccountName);
        txtTotal=findViewById(R.id.txtTotal);


        txtdate=findViewById(R.id.txtdate);
        txtaccount=findViewById(R.id.txtaccount);
        txtamount=findViewById(R.id.txtamount);
        txttype=findViewById(R.id.txttype);
        txtaction=findViewById(R.id.txtaction);


        String languagedata = LocaleHelper.getPersistedData(BillVoucherListActivity.this, "en");
        Context context= LocaleHelper.setLocale(BillVoucherListActivity.this, languagedata);

        resources=context.getResources();
        submit.setText(resources.getString(R.string.search));
        txtHead.setText(resources.getString(R.string.billing));
        //  txtbudget.setText(resources.getString(R.string.setbudget));

        txtdate.setText(resources.getString(R.string.date));
        txtaccount.setText(Utils.getCapsSentences(BillVoucherListActivity.this,resources.getString(R.string.nameofparty)));
        txtamount.setText(resources.getString(R.string.amount));
        txttype.setText(Utils.getCapsSentences(BillVoucherListActivity.this,resources.getString(R.string.creditaccount)));
        txtaction.setText(resources.getString(R.string.action));


        Utils.showTutorial(resources.getString(R.string.billingtutorial),BillVoucherListActivity.this,Utils.Tutorials.billingtutorial);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

//        addAccountSettings();
        getBillListByDate();


        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthDialog();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               showMonthDialog();
            }
        });


        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(new PreferenceHelper(BillVoucherListActivity.this).getIntData(Utils.trialwarning)==1)
                {
                    Toast.makeText(BillVoucherListActivity.this,Utils.Messages.warning,Toast.LENGTH_SHORT).show();
                }

                if(new PreferenceHelper(BillVoucherListActivity.this).getIntData(Utils.expirywarning)==1)
                {
                    Toast.makeText(BillVoucherListActivity.this,Utils.Messages.expirywarning,Toast.LENGTH_SHORT).show();
                }

                Intent intent=new Intent(BillVoucherListActivity.this, AddBillVoucherActivity.class);
                startActivity(intent);
            }
        });


        if(new PreferenceHelper(BillVoucherListActivity.this).getIntData(Utils.trialcompleted)==1)
        {
            Toast.makeText(BillVoucherListActivity.this,Utils.Messages.error,Toast.LENGTH_SHORT).show();

            fab_addtask.hide();
        }

        if(new PreferenceHelper(BillVoucherListActivity.this).getIntData(Utils.expirycompleted)==1)
        {
            Toast.makeText(BillVoucherListActivity.this,Utils.Messages.expiryerror,Toast.LENGTH_SHORT).show();

            fab_addtask.hide();
        }




    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getBillListByDate();
    }



    private void showMonthDialog() {

        String month="",yearselected="";


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR)-1;
        int m_calender=calendar.get(Calendar.MONTH);

        DisplayMetrics displayMetrics = new DisplayMetrics();
       getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels/2;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(BillVoucherListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);
        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(0);
        npmonth.setMaxValue(11);
        npmonth.setValue(m_calender);


        npmonth.setDisplayedValues(arrmonth);
        final NumberPicker npyear=dialog.findViewById(R.id.npyear);
        npyear.setMinValue(year);
        npyear.setMaxValue(year+5);


        npyear.setValue(year);
        npyear.setWrapSelectorWheel(true);
        dialog.getWindow().setLayout(width,height);
        npmonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {


            }
        });

        npyear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });





        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                m =  npmonth.getValue();

                yea = npyear.getValue();

                String month=arrmonth[m];

                txtdatepick.setText(month+"/"+yea);
                getBillVoucherData();

                // getVoucherData();

                //getVoucherData(m,yea);


            }
        });


        dialog.show();





    }














    public void getBillListByDate()
    {
        Calendar calendar=Calendar.getInstance();

        yea=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);

        m=month;


        txtdatepick.setText(arrmonth[m]+"/"+yea);
        getBillVoucherData();



    }



    public void showReceiptVoucherPdf(Accounts paymentVoucher)
    {

        try {

            LayoutInflater layoutInflater=(LayoutInflater) BillVoucherListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View card=layoutInflater.inflate(R.layout.layout_amount,null);

            LinearLayout layout=card.findViewById(R.id.layoutpdf);




            TextView txtheade=card.findViewById(R.id.txtheade);
            TextView  txtDatehead=card.findViewById(R.id.txtDatehead);
            TextView txtDate=card.findViewById(R.id.txtDate);
            TextView txtcustomerhead=card.findViewById(R.id.txtcustomerhead);
            TextView txtcustomer=card.findViewById(R.id.txtcustomer);
            TextView txtamounthead=card.findViewById(R.id.txtamounthead);
            TextView txtAmount=card.findViewById(R.id.txtAmount);
            TextView txtremarkshead=card.findViewById(R.id.txtremarkshead);
            TextView txtremarks=card.findViewById(R.id.txtremarks);

            txtheade.setText(resources.getString(R.string.receipt));
            txtDatehead.setText(resources.getString(R.string.date));
            txtremarkshead.setText(resources.getString(R.string.remarks));

            txtDate.setText(paymentVoucher.getACCOUNTS_date()+"");
            txtcustomerhead.setText(resources.getString(R.string.customer));
            txtamounthead.setText(resources.getString(R.string.amount));
            txtAmount.setText(resources.getString(R.string.rs)+" "+paymentVoucher.getACCOUNTS_amount());

            txtremarks.setText(paymentVoucher.getACCOUNTS_remarks());


            List<Accounts>accounts=new DatabaseHelper(BillVoucherListActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id()+"");

            String debitsetupid="";
            if(accounts.size()>0)
            {

               // debitaccountid=accounts.get(0).getACCOUNTS_id()+"";
                debitsetupid=accounts.get(0).getACCOUNTS_setupid();
            }


            List<CommonData> commonDatalist = new DatabaseHelper(BillVoucherListActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);


            for (int i=0;i<commonDatalist.size();i++)
            {


                if(commonDatalist.get(i).getId().equalsIgnoreCase(debitsetupid))
                {


                    JSONObject jsonObject=new JSONObject(commonDatalist.get(i).getData());

                    txtcustomer.setText(jsonObject.getString("Accountname"));

                    break;

                }

            }





            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            Bitmap bitmap = Bitmap.createBitmap(layout.getMeasuredWidth(), layout.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            layout.layout(0, 0, layout.getMeasuredWidth(), layout.getMeasuredHeight());
            layout.draw(canvas);




            File folder = new File(this.getExternalCacheDir() + "/" + "Save/"+"Receipt");

            if (!folder.exists()) {
                folder.mkdirs();
            }


            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File file = new File(folder.getAbsolutePath() + "/recipt" + ts + ".pdf");

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document();// Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));

// Open to write
            document.open();

            document.setPageSize(PageSize.A4);
            document.addCreationDate();



            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();


            //need to write code

            Image image = null;
            try
            {
                image = Image.getInstance(byteArray);
            }
            catch (BadElementException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (MalformedURLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // image.scaleAbsolute(150f, 150f);
            try
            {
                document.add(image);
            } catch (DocumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



            document.close();

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
            {

                Uri photoURI = FileProvider.getUriForFile(BillVoucherListActivity.this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(photoURI, "application/pdf");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            }
            else {

                Intent intent =new  Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                startActivity(intent);
            }




        }catch (Exception e)
        {

        }

    }







    public void getBillVoucherData()
    {


        String monthinarray=arrmonth[m];

        List<Accounts>commonData_selected=new ArrayList<>();

        List<Accounts>commonData=new DatabaseHelper(BillVoucherListActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.billvoucher+"");

        Log.e("RECEIPTTAGG",commonData.size()+"");




        try {

            int selected_month = m + 1;

            // List<PaymentVoucher> paymentVouchers = new DatabaseHelper(BillVoucherListActivity.this).getPaymentVoucherData();


            double total = 0;

            for (Accounts paymentVoucher : commonData
            ) {

                // JSONObject jsonObject = new JSONObject(paymentVoucher.getData());

                String month = paymentVoucher.getACCOUNTS_month();
                String year1 = paymentVoucher.getACCOUNTS_year();
//                String accountname = jsonObject.getString("accountname");
                String amount = paymentVoucher.getACCOUNTS_amount();
                String entryid=paymentVoucher.getACCOUNTS_entryid();
                ;


                if (String.valueOf(selected_month).equalsIgnoreCase(month) && String.valueOf(yea).equalsIgnoreCase(year1)) {

                    // CommonData cm = (CommonData) spinnerAccountName.getSelectedItem();

                    //  if (accountname.equalsIgnoreCase(cm.getId())) {

                    if(entryid.equalsIgnoreCase("0")) {

                        total = total + Double.parseDouble(amount);

                        commonData_selected.add(paymentVoucher);
                    }

                }


            }


            if (commonData_selected.size() > 0) {


                Collections.sort(commonData_selected, new Comparator<Accounts>() {
                    @Override
                    public int compare(Accounts accounts, Accounts t1) {

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date strDate=null;

                        Date endDate=null;
                        try {
                            strDate = sdf.parse(accounts.getACCOUNTS_date());
                            endDate = sdf.parse(t1.getACCOUNTS_date());




                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        return strDate.compareTo(endDate);
                    }
                });



                Collections.reverse(commonData_selected);


                recycler.setVisibility(View.VISIBLE);
                layout_head.setVisibility(View.VISIBLE);

                txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

                recycler.setLayoutManager(new LinearLayoutManager(BillVoucherListActivity.this));
                recycler.setAdapter(new BillVoucherAdapter(BillVoucherListActivity.this, commonData_selected));


            }
            else {

                txtTotal.setText(resources.getString(R.string.total)+" : " + total + " "+getResources().getString(R.string.rs));

                recycler.setVisibility(View.INVISIBLE);
                layout_head.setVisibility(View.GONE);
            }


        }catch (Exception e)
        {

        }



    }

//    public void createReceiptoPdf(Bitmap bitmap)
//    {
//
//        try {
//
//            File folder = new File(this.getExternalCacheDir() + "/" + "IntegraAccounts/"+"Receipt");
//
//            if (!folder.exists()) {
//                folder.mkdirs();
//            }
//
//
//            Long tsLong = System.currentTimeMillis() / 1000;
//            String ts = tsLong.toString();
//
//            File file = new File(folder.getAbsolutePath() + "/recipt" + ts + ".pdf");
//
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//
//
//            Document document = new Document();// Location to save
//            PdfWriter.getInstance(document, new FileOutputStream(file));
//
//// Open to write
//            document.open();
//
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//
//
//            //need to write code
//
//            Image image = null;
//            try
//            {
//                image = Image.getInstance(byteArray);
//            }
//            catch (BadElementException e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            catch (MalformedURLException e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            catch (IOException e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            // image.scaleAbsolute(150f, 150f);
//            try
//            {
//                document.add(image);
//            } catch (DocumentException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//
//
//
//
//
//
//            document.close();
//
//            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
//            {
//
//                Uri photoURI = FileProvider.getUriForFile(BillVoucherListActivity.this, getApplicationContext().getPackageName() + ".provider", file);
//
//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_VIEW);
//                intent.setDataAndType(photoURI, "application/pdf");
//                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                startActivity(intent);
//
//            }
//            else {
//
//                Intent intent =new  Intent();
//                intent.setAction(Intent.ACTION_VIEW);
//                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
//                startActivity(intent);
//            }
//
//
//
//
//        }catch (Exception e)
//        {
//
//        }
//
//
//    }
}
