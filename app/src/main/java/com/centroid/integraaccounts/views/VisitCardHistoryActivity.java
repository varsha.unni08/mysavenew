package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.VisitCardHistoryAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.VisitCard;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class VisitCardHistoryActivity extends AppCompatActivity {

    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    ImageView imgback;

    TextView txtprofile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_card_history);
        getSupportActionBar().hide();

        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        imgback=findViewById(R.id.imgback);

        txtprofile=findViewById(R.id.txtprofile);
        String languagedata = LocaleHelper.getPersistedData(VisitCardHistoryActivity.this, "en");
        Context context= LocaleHelper.setLocale(VisitCardHistoryActivity.this, languagedata);

        Resources resources=context.getResources();
        txtprofile.setText(resources.getString(R.string.visitingcardhistory));





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(VisitCardHistoryActivity.this, VisitCardFillActivity.class));
            }
        });

        getVisitCardHistory();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getVisitCardHistory();
    }

    public void getVisitCardHistory()
    {
        List<VisitCard> commonData=new DatabaseHelper(VisitCardHistoryActivity.this).getVisitCardData();

        if(commonData.size()>0)
        {

            VisitCardHistoryAdapter visitCardHistoryAdapter=new VisitCardHistoryAdapter(VisitCardHistoryActivity.this,commonData);

            recycler.setLayoutManager(new LinearLayoutManager(VisitCardHistoryActivity.this));
            recycler.setAdapter(visitCardHistoryAdapter);
        }
    }
}
