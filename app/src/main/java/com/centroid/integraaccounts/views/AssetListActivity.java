package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AssetListAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class AssetListActivity extends AppCompatActivity {

    ImageView imgback;
    FloatingActionButton fab_addtask;

    RecyclerView recycler;

    TextView txtprofile;

    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_list);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        fab_addtask=findViewById(R.id.fab_addtask);
        recycler=findViewById(R.id.recycler);
        txtprofile=findViewById(R.id.txtprofile);


        String languagedata = LocaleHelper.getPersistedData(AssetListActivity.this, "en");
        Context context= LocaleHelper.setLocale(AssetListActivity.this, languagedata);

        resources=context.getResources();
        txtprofile.setText(resources.getString(R.string.asset));


        Utils.showTutorial(resources.getString(R.string.accsetuptutorial),AssetListActivity.this,Utils.Tutorials.assettutorial);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        fab_addtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(AssetListActivity.this,AddAssetActivity.class));

            }
        });

        getAsset();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getAsset();
    }

    public void getAsset()
    {
        List<CommonData>commonData=new DatabaseHelper(AssetListActivity.this).getData(Utils.DBtables.TABLE_ASSET);

        if(commonData.size()>0)
        {

            recycler.setLayoutManager(new LinearLayoutManager(AssetListActivity.this));
            recycler.setAdapter(new AssetListAdapter(AssetListActivity.this,commonData));


        }



    }
}
