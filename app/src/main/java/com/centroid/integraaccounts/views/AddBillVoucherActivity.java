package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.AccountSettingsSpinnerAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.fragments.SpinnerDialogFragment;
import com.centroid.integraaccounts.interfaces.AccountSetupEventListener;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class AddBillVoucherActivity extends AppCompatActivity {

    ImageView imgback, imgDatepick, imgdelete, imgdownload;
    TextView txtdatepick, txtHead,txtSpinner,txtBillVoucher,txtbno;
    Spinner spinnerAccountName, spinnerAccounttype, spinnerBankdata;
    EditText edtAmount, edtvoucher, edtremarks;
    Button btnSave, btndelete, btndownload;

    String date = "", month_selected = "", yearselected = "";

    Accounts paymentVoucher;

    LinearLayout layout_bankdata;
    List<CommonData> cmfiltered = new ArrayList<>();
    List<CommonData> commonDataListfiltered = new ArrayList<>();
    FloatingActionButton fabadd, fabaddbank, fabaddincome;
    String bankid = "0", bankname = "", debitaccountid = "0";


    Thread thread = null;

    Handler handler;

    Resources resources;

    int index = 0;

    String creditsetupid = "0", debitsetupid = "0";

    Accounts accounts = null, accounts1 = null;

    String customer="",income="";

    CommonData commonData_selected;

    int trid=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bill_voucher);
        getSupportActionBar().hide();
        cmfiltered = new ArrayList<>();

        paymentVoucher = (Accounts) getIntent().getSerializableExtra("Account");
        handler = new Handler();
        trid=new PreferenceHelper(AddBillVoucherActivity.this).getIntData(Utils.billvoucherkey);
        trid=trid+1;
        cmfiltered = new ArrayList<>();
        txtHead = findViewById(R.id.txtHead);
        imgdownload = findViewById(R.id.imgdownload);
        txtSpinner=findViewById(R.id.txtSpinner);
        txtBillVoucher=findViewById(R.id.txtBillVoucher);
        txtbno=findViewById(R.id.txtbno);

        // paymentVoucher = (Accounts) getIntent().getSerializableExtra("paymentVouchers");
        imgback = findViewById(R.id.imgback);
        imgDatepick = findViewById(R.id.imgDatepick);
        fabadd = findViewById(R.id.fabadd);
        fabaddbank = findViewById(R.id.fabaddbank);
        imgdelete = findViewById(R.id.imgdelete);
        txtdatepick = findViewById(R.id.txtdatepick);
        layout_bankdata = findViewById(R.id.layout_bankdata);

        fabaddincome = findViewById(R.id.fabaddincome);

        spinnerAccountName = findViewById(R.id.spinnerAccountName);
        spinnerAccounttype = findViewById(R.id.spinnerAccounttype);
        spinnerBankdata = findViewById(R.id.spinnerBankdata);
        edtAmount = findViewById(R.id.edtAmount);
        btndelete = findViewById(R.id.btndelete);
        btndownload = findViewById(R.id.btndownload);

        edtvoucher = findViewById(R.id.edtvoucher);
        edtremarks = findViewById(R.id.edtremarks);
        btnSave = findViewById(R.id.btnSave);


//        List<Accounts>accountsbill= new DatabaseHelper(AddBillVoucherActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.billvoucher+"");
//
//        int a=0;
//        if(accountsbill.size()>0)
//        {
//            for (Accounts account:accountsbill
//            ) {
//
//                if(account.getACCOUNTS_entryid().equalsIgnoreCase("0"))
//                {
//                    a++;
//                }
//
//            }
//
//
//        }

       // trid=a;





       // trid=trid+1;

        txtBillVoucher.setText(Utils.BillVoucherDetails.billvoucherNumber+trid);

        String languagedata = LocaleHelper.getPersistedData(AddBillVoucherActivity.this, "en");
        Context context = LocaleHelper.setLocale(AddBillVoucherActivity.this, languagedata);

        resources = context.getResources();
        edtAmount.setHint(resources.getString(R.string.amount));
        edtremarks.setHint(resources.getString(R.string.enterremarks));
        txtdatepick.setText(resources.getString(R.string.selectdate));
        btnSave.setText(resources.getString(R.string.save));

        txtHead.setText(resources.getString(R.string.billing));
        txtbno.setText(resources.getString(R.string.billvouchernumber));
        txtSpinner.setText(Utils.getCapsSentences(context,resources.getString(R.string.selectincomeaccount)));

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SpinnerDialogFragment spinnerDialogFragment=new SpinnerDialogFragment(true,new AccountSetupEventListener() {
                    @Override
                    public void getSelectedAccountSetup(CommonData commonData) {

                        try {

                            commonData_selected=commonData;

                            JSONObject jcmn1 = new JSONObject(commonData.getData());
                            String acctype = Utils.getCapsSentences(context,jcmn1.getString("Accountname"));

                            txtSpinner.setText(acctype);


                        }catch (Exception e)
                        {

                        }


                    }
                });


                spinnerDialogFragment.show(getSupportFragmentManager(),"skjdf");



            }
        });


        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });
        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker();
            }
        });


        Executor executor = new DirectExecutor();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                setEditMode();
            }
        });


        imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String permission="";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                    permission=Manifest.permission.READ_MEDIA_IMAGES;

                }
                else{

                    permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                }


                if (ContextCompat.checkSelfPermission(AddBillVoucherActivity.this, permission) == PackageManager.PERMISSION_GRANTED) {


                    Utils.showAlert(AddBillVoucherActivity.this, "Do you want to download bill voucher now ? ", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {


                            if (paymentVoucher != null) {
                                if (accounts != null) {

                                    paymentVoucher = accounts;
                                }

                                showReceiptVoucherPdf();
                            } else {
                                paymentVoucher = accounts;


                                setEditMode();

                                showReceiptVoucherPdf();
                            }
                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                } else {


                    ActivityCompat.requestPermissions(AddBillVoucherActivity.this, new String[]{permission}, 111);

                }


            }
        });


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Toast.makeText(AddBillVoucherActivity.this, "Add account setup in Bank account category", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AddBillVoucherActivity.this, AccountsettingsActivity.class);
                intent.putExtra("customer", 1);


                startActivityForResult(intent,Utils.Requestcode.ForAccountSettingsCustomerRequestcode);


            }
        });

        fabaddincome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(AddBillVoucherActivity.this, "Add account setup in Income account category", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(AddBillVoucherActivity.this, AccountsettingsActivity.class);
                intent.putExtra("incomeaccount", 1);
                startActivityForResult(intent,Utils.Requestcode.ForAccountSettingsIncomeRequestcode);

            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (paymentVoucher != null) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddBillVoucherActivity.this);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                            dialogInterface.dismiss();


                            Executor executor = new DirectExecutor();

                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    //  updateData();


                                    try {

                                        int accid = paymentVoucher.getACCOUNTS_id();

                                        int billid=0;
                                        try{

                                            billid=Integer.parseInt(paymentVoucher.getBillvouchernumber());
                                        }catch (Exception e)
                                        {

                                        }

                                        if(billid!=0) {

                                            if (billid==new PreferenceHelper(AddBillVoucherActivity.this).getIntData(Utils.billvoucherkey))
                                            {
                                                int t=billid-1;

                                                new PreferenceHelper(AddBillVoucherActivity.this).putIntData(Utils.billvoucherkey,t);

                                            }
                                        }


                                        new DatabaseHelper(AddBillVoucherActivity.this).deleteAccountDataById(paymentVoucher.getACCOUNTS_id() + "");

                                        List<Accounts> accounts = new DatabaseHelper(AddBillVoucherActivity.this).getAccountsDataByEntryId(accid + "");

                                        if (accounts.size() > 0) {

                                            Accounts accounts1 = accounts.get(0);


                                            new DatabaseHelper(AddBillVoucherActivity.this).deleteAccountDataById(accounts1.getACCOUNTS_id() + "");


                                        }


                                        onBackPressed();

                                    } catch (Exception e) {

                                    }


                                }
                            });


                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();


                }

            }
        });


        addAccountSettings();
        addIncomeAccountSettings();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!date.equalsIgnoreCase("")) {


                    if (!edtAmount.getText().toString().equalsIgnoreCase("")) {


//                        if(!edtremarks.getText().toString().equalsIgnoreCase(""))
//                        {

                        try {

                            if (paymentVoucher != null) {

//
                                Executor executor = new DirectExecutor();

                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateData();
                                    }
                                });
//
//
                            } else {

                                Executor executor = new DirectExecutor();

                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        addData();
                                    }
                                });


                            }


                        } catch (Exception e) {


                        }




                    } else {

                        Toast.makeText(AddBillVoucherActivity.this, resources.getString(R.string.amount), Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(AddBillVoucherActivity.this, resources.getString(R.string.selectdate), Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode==Utils.Requestcode.ForAccountSettingsCustomerRequestcode&&resultCode==RESULT_OK&&data!=null)
        {

          customer=  data.getStringExtra("AccountAdded");

          addAccountSettings();




        }


        if(requestCode==Utils.Requestcode.ForAccountSettingsIncomeRequestcode&&resultCode==RESULT_OK&&data!=null)
        {

            income=  data.getStringExtra("AccountAdded");

            addIncomeAccountSettings();




        }






    }

    public void showReceiptVoucherPdf() {

        try {



            final ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"fkjfk");


            Map<String,String> params=new HashMap<>();
            params.put("timestamp",Utils.getTimestamp());
            // params.put("device_id",token);

            new RequestHandler(AddBillVoucherActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                    Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                    progressFragment.dismiss();
                    if(profiledata!=null)
                    {

                        try{



                            if(profiledata.getStatus()==1)
                            {


                                if(profiledata.getData()!=null)
                                {


                                    String phonenumber=profiledata.getData().getMobile();
                                    String name=profiledata.getData().getFullName();

                                    String email =profiledata.getData().getEmailId();


                                    final Dialog card=new Dialog(AddBillVoucherActivity.this);
                                    card.setContentView(R.layout.layout_billdialog);


                                    // View card = layoutInflater.inflate(R.layout.layout_amount, null);

                                    final    LinearLayout view1 = card.findViewById(R.id.layoutimg);




                                    TextView txtMyname=card.findViewById(R.id.txtMyname);
                                    TextView txtph=card.findViewById(R.id.txtph);
                                    TextView txtemail=card.findViewById(R.id.txtemail);

                                    txtMyname.setText(name);
                                    txtph.setText(phonenumber);
                                    txtemail.setText(email);

                                    TextView txtheade = card.findViewById(R.id.txtheade);
                                    TextView txtBillno=card.findViewById(R.id.txtBillno);
                                    TextView txtDatehead = card.findViewById(R.id.txtDatehead);
                                    TextView txtDate = card.findViewById(R.id.txtDate);
                                    TextView txtcustomerhead = card.findViewById(R.id.txtcustomerhead);
                                    TextView txtcustomer = card.findViewById(R.id.txtcustomer);
                                    TextView txtamounthead = card.findViewById(R.id.txtamounthead);
                                    TextView txtAmount = card.findViewById(R.id.txtAmount);
                                    TextView txtremarkshead = card.findViewById(R.id.txtremarkshead);
                                    TextView txtremarks = card.findViewById(R.id.txtremarks);
                                    TextView txtBillnohead=card.findViewById(R.id.txtBillnohead);

                                    final Button btnDownload=card.findViewById(R.id.btnDownload);

                                    txtheade.setText(resources.getString(R.string.bill));
                                    txtDatehead.setText(resources.getString(R.string.date));
                                    txtremarkshead.setText(resources.getString(R.string.remarks));

                                    txtDate.setText(paymentVoucher.getACCOUNTS_date() + "");
                                    txtcustomerhead.setText(resources.getString(R.string.customer));
                                    txtamounthead.setText(resources.getString(R.string.amount));
                                    txtAmount.setText(resources.getString(R.string.rs) + " " + paymentVoucher.getACCOUNTS_amount());
                                    txtBillno.setText(trid+"");
                                    txtBillnohead.setText(resources.getString(R.string.billvouchernumber));
                                    btnDownload.setText(resources.getString(R.string.download));

                                    txtremarks.setText(paymentVoucher.getACCOUNTS_remarks());
                                    for (int i = 0; i < cmfiltered.size(); i++) {


                                        if (cmfiltered.get(i).getId().equalsIgnoreCase(debitsetupid)) {
                                            //spinnerAccountName.setSelection(i);
                                            // txtcustomer.setText(cmfiltered.get(i).getData());

                                            JSONObject jsonObject = new JSONObject(cmfiltered.get(i).getData());

                                            txtcustomer.setText(jsonObject.getString("Accountname"));

                                            break;

                                        }

                                    }



                                    btnDownload.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            btnDownload.setVisibility(View.GONE);

                                            card.dismiss();

                                            try {

                                                String permission="";

                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                                                    permission=Manifest.permission.READ_MEDIA_IMAGES;

                                                }
                                                else{

                                                    permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                                                }
                                                if (ContextCompat.checkSelfPermission(AddBillVoucherActivity.this,permission) == PackageManager.PERMISSION_GRANTED) {


                                                    DisplayMetrics metrics = new DisplayMetrics();
                                                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                                                    int heightPixels = metrics.heightPixels;
                                                    int widthPixels = metrics.widthPixels;

                                                    View v1 = card.getWindow().getDecorView().getRootView();


                                                    v1.setDrawingCacheEnabled(true);
                                                    Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                                                    v1.setDrawingCacheEnabled(false);


                                                    Long tsLong = System.currentTimeMillis() / 1000;
                                                    String ts = tsLong.toString();

                                                    File fp = new File(AddBillVoucherActivity.this.getExternalCacheDir() + "/Save/Bill");


                                                    if (!fp.exists()) {
                                                        fp.mkdirs();
                                                    }

                                                    File f = new File(fp.getAbsolutePath(), "bill" + ts + ".png");
                                                    if (!f.exists()) {
                                                        f.createNewFile();

                                                    } else {

                                                        f.delete();
                                                        f.createNewFile();
                                                    }


                                                    FileOutputStream output = new FileOutputStream(f);


                                                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, output);
                                                    output.close();


                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                                        Uri photoURI = FileProvider.getUriForFile(AddBillVoucherActivity.this, getApplicationContext().getPackageName() + ".provider", f);

                                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                                        intent.setType("image/*");
                                                        intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                                                        startActivity(Intent.createChooser(intent, "Share Bill"));

                                                    } else {

                                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                                        intent.setType("image/*");
                                                        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
                                                        startActivity(Intent.createChooser(intent, "Share Image"));
                                                    }

                                                }
                                            }

                                            catch (Exception e)
                                            {

                                                Log.e("TAG",e.toString());
                                            }


                                        }
                                    });


                                    DisplayMetrics metrics = new DisplayMetrics();
                                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                                    int heightPixels = metrics.heightPixels;
                                    int widthPixels = metrics.widthPixels;

                                    card.getWindow().setLayout(widthPixels,heightPixels);



                                    card.show();






















                                }





                            }
                            else {

                                //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();

                                Utils.showAlertWithSingle(AddBillVoucherActivity.this, "failed", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });


                            }



                        }catch (Exception e)
                        {

                        }



                    }
                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

//                    Utils.showAlertWithSingle(AppRenewalActivity.this, err, new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });


                }
            },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();






































        } catch (Exception e) {


            Log.e("ERRR", e.toString());
        }

    }


      private Bitmap getBitmapFromView(View view) {
  //  Define a bitmap with the same size as the view


          Bitmap returnedBitmap = Bitmap.createBitmap(1000, 1000,Bitmap.Config.ARGB_8888);
          //Bind a canvas to it
          Canvas canvas = new Canvas(returnedBitmap);
          //Get the view's background
          Drawable bgDrawable =view.getBackground();
          if (bgDrawable!=null) {
              //has background drawable, then draw it on the canvas
              bgDrawable.draw(canvas);
          }   else{
              //does not have background drawable, then draw white background on the canvas
              canvas.drawColor(Color.WHITE);
          }
          // draw the view on the canvas
          view.draw(canvas);
          //return the bitmap
          return returnedBitmap;
      }


    public void setEditMode() {

        if (paymentVoucher != null) {


            try {

                txtHead.setText(resources.getString(R.string.editbillvoucher));
                btnSave.setText(resources.getString(R.string.update));
                btndelete.setText(resources.getString(R.string.delete));
                // btndownload.setText("Download");

                imgdownload.setVisibility(View.VISIBLE);

                String month = paymentVoucher.getACCOUNTS_month();
                String year1 = paymentVoucher.getACCOUNTS_year();
                String dat = paymentVoucher.getACCOUNTS_date();
                String amount = paymentVoucher.getACCOUNTS_amount();
                creditsetupid = paymentVoucher.getACCOUNTS_setupid();
                String accounttype = paymentVoucher.getACCOUNTS_type();
                String remarks = paymentVoucher.getACCOUNTS_remarks();
                String entryid = paymentVoucher.getACCOUNTS_entryid();
                String cashbank = paymentVoucher.getACCOUNTS_cashbanktype();

trid=Integer.parseInt(paymentVoucher.getBillvouchernumber());

     ;
                txtBillVoucher.setText(Utils.BillVoucherDetails.billvoucherNumber+paymentVoucher.getBillvouchernumber());

                month_selected = month;
                yearselected = year1;
                date = dat;
                btndelete.setVisibility(View.VISIBLE);
                txtdatepick.setText(date);
                edtAmount.setText(amount);

                edtremarks.setText(remarks);


                List<Accounts> accounts = new DatabaseHelper(AddBillVoucherActivity.this).getAccountsDataByEntryId(paymentVoucher.getACCOUNTS_id() + "");

                if (accounts.size() > 0) {

                    debitaccountid = accounts.get(0).getACCOUNTS_id() + "";
                    debitsetupid = accounts.get(0).getACCOUNTS_setupid();
                }

               // commonDataListfiltered.clear();

                List<CommonData> commonData = new DatabaseHelper(AddBillVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                for (CommonData cm : commonData) {

                    if(cm.getId().equalsIgnoreCase(creditsetupid))
                    {

                        commonData_selected=cm;

                        try{

                            JSONObject jcmn1 = new JSONObject(cm.getData());
                            String acctype = jcmn1.getString("Accountname");

                            txtSpinner.setText(acctype);


                        }catch (Exception e)
                        {


                        }

break;

                    }




                }



//                CommonData cmincome=(CommonData) spinnerAccounttype.getSelectedItem();


//
//                CommonData cmincustomer=(CommonData) spinnerAccountName.getSelectedItem();


            } catch (Exception e) {

            }
        } else {

            setDate();
        }
    }


    private void updateData() {

        CommonData cmincome = commonData_selected;

        CommonData cmincustomer = (CommonData) spinnerAccountName.getSelectedItem();


        accounts = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.billvoucher);
        accounts.setACCOUNTS_setupid(cmincome.getId());//setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());

        accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");
        accounts.setBillvouchernumber(trid+"");

        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);
        accounts.setACCOUNTS_id(paymentVoucher.getACCOUNTS_id());

        new DatabaseHelper(AddBillVoucherActivity.this).updateAccountsData(paymentVoucher.getACCOUNTS_id() + "", accounts);
        Utils.playSimpleTone(AddBillVoucherActivity.this);

        Accounts accounts1 = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts1.setACCOUNTS_entryid(paymentVoucher.getACCOUNTS_id() + "");
        accounts1.setACCOUNTS_date(date);
        accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.billvoucher);
        accounts1.setACCOUNTS_setupid(cmincustomer.getId());//setup id is the account setup id from the dropdown
        accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
        accounts.setBillvouchernumber(trid+"");
        accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");

        accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts1.setACCOUNTS_month(month_selected);
        accounts1.setACCOUNTS_year(yearselected);

        new DatabaseHelper(AddBillVoucherActivity.this).updateAccountsData(debitaccountid, accounts1);
        Utils.playSimpleTone(AddBillVoucherActivity.this);

        date = "";
        month_selected = "";
        yearselected = "";
        spinnerAccountName.setSelection(0);
        edtAmount.setText("");
        edtvoucher.setText("");
        edtremarks.setText("");
        txtdatepick.setText(resources.getString(R.string.selectdate));
        spinnerAccounttype.setSelection(0);
        setDate();

        onBackPressed();

    }


    private void addData() {


        CommonData cmincome = commonData_selected;

        CommonData cmincustomer = (CommonData) spinnerAccountName.getSelectedItem();


        accounts = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts.setACCOUNTS_entryid("0");
        accounts.setACCOUNTS_date(date);
        accounts.setACCOUNTS_vouchertype(Utils.VoucherType.billvoucher);
        accounts.setACCOUNTS_setupid(cmincome.getId());//setup id is the account setup id from the dropdown
        accounts.setACCOUNTS_amount(edtAmount.getText().toString());

        accounts.setBillvouchernumber(trid+"");
        accounts.setACCOUNTS_type(Utils.Cashtype.credit + "");

        accounts.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts.setACCOUNTS_month(month_selected);
        accounts.setACCOUNTS_year(yearselected);

        long insertid = new DatabaseHelper(AddBillVoucherActivity.this).addAccountsData(accounts);
        Utils.playSimpleTone(AddBillVoucherActivity.this);
        accounts.setACCOUNTS_id(Integer.parseInt(insertid + ""));

        accounts1 = new Accounts();

        //entry id is equal to zero in the base of accounts section from account setup
        accounts1.setACCOUNTS_entryid(insertid + "");
        accounts1.setACCOUNTS_date(date);
        accounts1.setACCOUNTS_vouchertype(Utils.VoucherType.billvoucher);
        accounts1.setACCOUNTS_setupid(cmincustomer.getId());//setup id is the account setup id from the dropdown
        accounts1.setACCOUNTS_amount(edtAmount.getText().toString());
        accounts.setBillvouchernumber(trid+"");
        accounts1.setACCOUNTS_type(Utils.Cashtype.debit + "");

        accounts1.setACCOUNTS_remarks(edtremarks.getText().toString());
        accounts1.setACCOUNTS_month(month_selected);
        accounts1.setACCOUNTS_year(yearselected);

        new DatabaseHelper(AddBillVoucherActivity.this).addAccountsData(accounts1);
        new PreferenceHelper(AddBillVoucherActivity.this).putIntData(Utils.billvoucherkey,trid);
        Utils.playSimpleTone(AddBillVoucherActivity.this);

        date = "";
        month_selected = "";
        yearselected = "";
        spinnerAccountName.setSelection(0);
        edtAmount.setText("");
        edtvoucher.setText("");
        edtremarks.setText("");
        txtdatepick.setText(resources.getString(R.string.selectdate));
        spinnerAccounttype.setSelection(0);
        setDate();
        // onBackPressed();

        imgdownload.setVisibility(View.VISIBLE);


    }


    public void setDate() {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        date = dayOfMonth + "-" + m + "-" + year;

        txtdatepick.setText(dayOfMonth + "-" + m + "-" + year);

       // trid=new DatabaseHelper(AddBillVoucherActivity.this).getLastBillVoucherID();

//       List<Accounts>accounts= new DatabaseHelper(AddBillVoucherActivity.this).getAccountsDataByVouchertype(Utils.VoucherType.billvoucher+"");
//
//       int a=0;
//       if(accounts.size()>0)
//       {
//           for (Accounts account:accounts
//                ) {
//
//               if(account.getACCOUNTS_entryid().equalsIgnoreCase("0"))
//               {
//                   a++;
//               }
//
//           }
//
//
//       }
//
//       trid=a;

        trid=new PreferenceHelper(AddBillVoucherActivity.this).getIntData(Utils.billvoucherkey);

        trid=trid+1;
        txtBillVoucher.setText(Utils.BillVoucherDetails.billvoucherNumber+trid);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        addAccountSettings();
        addIncomeAccountSettings();
        //addAccountSettings();
    }

    public void addAccountSettings() {


        cmfiltered.clear();
        commonDataListfiltered.clear();

        List<CommonData> commonData = new DatabaseHelper(AddBillVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {

            //Collections.reverse(commonData);

            Collections.sort(commonData, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a = 0;

                    try {


                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype = jcmn1.getString("Accountname");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1 = j1.getString("Accountname");

                        a = acctype.compareToIgnoreCase(acctypej1);

                    } catch (Exception e) {

                    }


                    return a;
                }
            });


            for (CommonData cm : commonData) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Customers")) {
                        cmfiltered.add(cm);
                    }


                } catch (Exception e) {

                }

            }

            Collections.sort(cmfiltered, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int b = 0;
                    try {

                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        JSONObject jsonObject1 = new JSONObject(t1.getData());

                        String acctype1 = jsonObject.getString("Accountname");
                        String acctype2 = jsonObject1.getString("Accountname");

                        b = acctype1.compareToIgnoreCase(acctype2);
                    } catch (Exception e) {

                    }

                    return b;
                }
            });


            if (cmfiltered.size() > 0) {

                AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddBillVoucherActivity.this, cmfiltered);
                spinnerAccountName.setAdapter(accountSettingsSpinnerAdapter);

            }

            if (!debitsetupid.equalsIgnoreCase("0")) {

                for (int i = 0; i < cmfiltered.size(); i++) {




                    if (cmfiltered.get(i).getId().equalsIgnoreCase(debitsetupid)) {
                        spinnerAccountName.setSelection(i);

                        break;

                    }

                }

            }


            if (!customer.equalsIgnoreCase("")) {

                for (int i = 0; i < cmfiltered.size(); i++) {




                    try {

                        JSONObject jsonObject = new JSONObject(cmfiltered.get(i).getData());

                       // JSONObject jsonObject1 = new JSONObject(t1.getData());

                        String acctype1 = jsonObject.getString("Accountname");
                        if (acctype1.equalsIgnoreCase(customer)) {

                            spinnerAccountName.setSelection(i);

                            break;

                        }

                    }catch (Exception e)
                    {

                    }

                }

            }


        }


    }


    public void addIncomeAccountSettings() {


        // cmfiltered.clear();
        commonDataListfiltered.clear();

        List<CommonData> commonData = new DatabaseHelper(AddBillVoucherActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if (commonData.size() > 0) {


            Collections.sort(commonData, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    int a = 0;

                    try {


                        JSONObject jcmn1 = new JSONObject(commonData.getData());
                        String acctype = jcmn1.getString("Accountname");
                        JSONObject j1 = new JSONObject(t1.getData());
                        String acctypej1 = j1.getString("Accountname");

                        a = acctype.compareTo(acctypej1);

                    } catch (Exception e) {

                    }


                    return a;
                }
            });


            for (CommonData cm : commonData) {


                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype = jsonObject.getString("Accounttype");


                    if (acctype.equalsIgnoreCase("Income account")) {
                        commonDataListfiltered.add(cm);
                    }


                } catch (Exception e) {

                }

            }

            Collections.sort(commonDataListfiltered, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {
                    int a = 0;
                    try {

                        JSONObject jsonObject = new JSONObject(commonData.getData());

                        JSONObject jsonObject1 = new JSONObject(t1.getData());

                        String acctype1 = jsonObject.getString("Accountname");
                        String acctype2 = jsonObject1.getString("Accountname");

                        a = acctype1.compareToIgnoreCase(acctype2);
                    } catch (Exception e) {

                    }

                    return a;
                }
            });


            if (commonDataListfiltered.size() > 0) {

                AccountSettingsSpinnerAdapter accountSettingsSpinnerAdapter = new AccountSettingsSpinnerAdapter(AddBillVoucherActivity.this, commonDataListfiltered);
                spinnerAccounttype.setAdapter(accountSettingsSpinnerAdapter);

            }


            if (!creditsetupid.equalsIgnoreCase("0")) {

                for (int i = 0; i < commonDataListfiltered.size(); i++) {


                    if (commonDataListfiltered.get(i).getId().equalsIgnoreCase(creditsetupid)) {
                        spinnerAccounttype.setSelection(i);

                        break;

                    }

                }

            }


            if(!income.equalsIgnoreCase(""))
            {

                for (int i = 0; i < commonDataListfiltered.size(); i++) {

                    try {

                        JSONObject jsonObject = new JSONObject(commonDataListfiltered.get(i).getData());

                        String acctype2 = jsonObject.getString("Accountname");
                        if (acctype2.equalsIgnoreCase(income)) {
                            spinnerAccounttype.setSelection(i);

                            commonData_selected=commonDataListfiltered.get(i);
                            txtSpinner.setText(income);

                            break;

                        }

                    }catch (Exception e)
                    {

                    }

                }


            }


        }


    }

    public void showDatePicker() {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(AddBillVoucherActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m = i1 + 1;
                month_selected = m + "";
                yearselected = i + "";

                date = i2 + "-" + m + "-" + i;

                txtdatepick.setText(i2 + "-" + m + "-" + i);


            }
        }, year, month, dayOfMonth);

        datePickerDialog.show();
    }

}
