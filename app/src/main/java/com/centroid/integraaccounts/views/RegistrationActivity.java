package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.CountrySpinnerAdapter;
import com.centroid.integraaccounts.adapter.StateListAdapter;
import com.centroid.integraaccounts.data.domain.CountryData;
import com.centroid.integraaccounts.data.domain.CountryList;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.Sponsor;
import com.centroid.integraaccounts.data.domain.State;
import com.centroid.integraaccounts.data.domain.StateData;
import com.centroid.integraaccounts.data.domain.User;
import com.centroid.integraaccounts.data.domain.UserDetails;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.centroid.integraaccounts.webserviceHelper.SponsorService;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public class RegistrationActivity extends AppCompatActivity {

    Button btnsubmit,btnLogin,btnSubmitCoupon;

    ImageView imgback;

    Spinner spstate,spLanguage,spCountry;

    boolean isValidCouponcode=false;

    RadioButton exist_radio_button,Notexist_radio_button;

    EditText edtName,edtEmail,edtPhone,edtPassword,edtConfirmPassword,edtspPh,edtcouponcode;

    LinearLayout layout_sponsor,layout_sponsordata,layout_spsor,layout_couponcode;

    TextView txtSponserName,txtSponserCode,txtSponserNamedata,txtSponserCodedata,txtWebLinkTerms;
     Dialog dialog=null;

    public static final String FILTER_ACTION_KEY = "any_key";

//    SponsorReceiver myReceiver;

    Profiledata sponsor_exist=null;

    CheckBox cbAutolink;

    Button btnSubmit;
    Button btnSubmitNumber;

    int c=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().hide();

        layout_sponsordata=findViewById(R.id.layout_sponsordata);
        txtSponserNamedata=findViewById(R.id.txtSponserNamedata);
        txtSponserCodedata=findViewById(R.id.txtSponserCodedata);
        txtWebLinkTerms=findViewById(R.id.txtWebLinkTerms);
        cbAutolink=findViewById(R.id.cbAutolink);
        layout_couponcode=findViewById(R.id.layout_couponcode);
        edtcouponcode=findViewById(R.id.edtcouponcode);



        edtspPh=findViewById(R.id.edtspPh);
        layout_sponsor=findViewById(R.id.layout_sponsor);
        btnSubmitNumber=findViewById(R.id.btnSubmitNumber);
        txtSponserName=findViewById(R.id.txtSponserName);
        txtSponserCode=findViewById(R.id.txtSponserCode);




        btnsubmit=findViewById(R.id.btnsubmit);
        btnSubmitCoupon=findViewById(R.id.btnSubmitCoupon);
        btnLogin=findViewById(R.id.btnLogin);
        imgback=findViewById(R.id.imgback);
        spstate=findViewById(R.id.spState);
        spCountry=findViewById(R.id.spCountry);
        spLanguage=findViewById(R.id.spLanguage);

        exist_radio_button=findViewById(R.id.exist_radio_button);
        Notexist_radio_button=findViewById(R.id.Notexist_radio_button);


        edtName=findViewById(R.id.edtName);
        edtEmail=findViewById(R.id.edtEmail);
        edtPhone=findViewById(R.id.edtPhone);
        edtPassword=findViewById(R.id.edtPassword);
        edtConfirmPassword=findViewById(R.id.edtConfirmPassword);

        edtspPh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                layout_sponsor.setVisibility(View.GONE);
                sponsor_exist=null;


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        btnSubmitNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtspPh.getText().toString().equalsIgnoreCase(""))
                {

                    String data=edtspPh.getText().toString();

//
                 //   getSponsor(data);
                }
                else {

                    Toast.makeText(RegistrationActivity.this,"Enter your sponser's mobile number",Toast.LENGTH_SHORT).show();




                }
            }
        });


        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(c==0)
                {
                    Toast.makeText(RegistrationActivity.this, "Password must have atleast 8 characters", Toast.LENGTH_SHORT).show();

                    c++;
                }
                

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        btnSubmitCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtcouponcode.getText().toString().equalsIgnoreCase(""))
                {


                    getCouponCode(edtcouponcode.getText().toString());

                }
                else{


                    Utils.showAlertWithSingle(RegistrationActivity.this,"Enter coupon code",null);
                }



            }
        });


        txtWebLinkTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browse = new Intent(Intent. ACTION_VIEW, Uri. parse(Utils.domain+"index.php/web/termsCondition")); startActivity(browse);
            }
        });


        exist_radio_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b)
                {
                    exist_radio_button.setTextColor(Color.BLACK);

                    layout_couponcode.setVisibility(View.VISIBLE);
                }
                else {

                    exist_radio_button.setTextColor(Color.WHITE);

                    layout_couponcode.setVisibility(View.GONE);
                }



            }
        });


        exist_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(exist_radio_button.isChecked())
                {
//                    showSponserAlert();

                    layout_couponcode.setVisibility(View.VISIBLE);
                }
                else{

                    layout_couponcode.setVisibility(View.GONE);
                }
            }
        });



        Notexist_radio_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                if(b)
                {
                    Notexist_radio_button.setTextColor(Color.BLACK);
                    layout_couponcode.setVisibility(View.GONE);
                }
                else {

                    Notexist_radio_button.setTextColor(Color.WHITE);

                    layout_couponcode.setVisibility(View.VISIBLE);
                }





            }
        });

        Notexist_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Notexist_radio_button.isChecked())
                {

                    layout_sponsordata.setVisibility(View.GONE);
                    sponsor_exist=null;

                }

            }
        });






        String arrlang[]=getResources().getStringArray(R.array.languages);


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegistrationActivity.this,   R.layout.spinneritemwhite,arrlang );
//
        spinnerArrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_white);
        spLanguage.setAdapter(spinnerArrayAdapter);





        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String item=spLanguage.getSelectedItem().toString();

                String languagedata= LocaleHelper.getPersistedData(RegistrationActivity.this,"en");






                if(item.equals("తెలుగు"))
                {
                    languagedata="te";
                }
                if(item.equals("English"))
                {
                    languagedata="en";
                }

                if(item.equals("मराठी"))
                {
                    languagedata="mr";
                }
                if(item.equals("தமிழ்"))
                {
                    languagedata="ta";
                }

                if(item.equals("ಕನ್ನಡ"))
                {
                    languagedata="kn";
                }

                if(item.equals("हिंदी"))
                {
                    languagedata="hi";
                }


                if(item.equals("മലയാളം"))
                {
                    languagedata="ml";
                }

                Context context= LocaleHelper.setLocale(RegistrationActivity.this,languagedata);

                Resources resources=context.getResources();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spLanguage.setSelection(2);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent i=  new Intent(RegistrationActivity.this,OTPActivity.class);
//
//                i.putExtra("Data",usr);
//
//
//                startActivity(i);

                CountryData countryData=(CountryData)spCountry.getSelectedItem();

                State state=(State)spstate.getSelectedItem();



                if(!edtName.getText().toString().equals(""))
                {

                    if(Utils.emailValidator(edtEmail.getText().toString()))
                    {

                        if(countryData!=null)

                        {

//                            if(state!=null) {


                                if (!edtPhone.getText().toString().equals("")) {
                                    boolean iscountrycheckmobile=true;

                                    if(countryData.getId().equalsIgnoreCase("1"))
                                    {
                                        if(edtPhone.getText().toString().length()==10)
                                        {

                                            iscountrycheckmobile=true;
                                        }
                                        else{

                                            iscountrycheckmobile=false;
                                        }


                                    }
                                    else{

                                        iscountrycheckmobile=true;

                                    }

                                    boolean isvalid_couponcode=false;

                                    if(exist_radio_button.isChecked())
                                    {
                                        if(isValidCouponcode)
                                        {

                                            isvalid_couponcode=true;
                                        }
                                        else{

                                            isvalid_couponcode=false;

                                        }




                                    }
                                    else{

                                        isvalid_couponcode=true;

                                    }







                                    if(iscountrycheckmobile) {


                                        if (isvalid_couponcode){

                                        if (!edtPassword.getText().toString().equals("")) {

                                            if (edtPassword.getText().toString().length() >= 8) {

                                                if (edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {

                                                    String lan = spLanguage.getSelectedItem().toString();

                                                    if (!lan.equalsIgnoreCase("Select your language")) {

                                                        if (cbAutolink.isChecked()) {


                                                            String lang = spLanguage.getSelectedItem().toString();

                                                            User user = new User();
                                                            user.setEmail(edtEmail.getText().toString().trim());
                                                            user.setName(edtName.getText().toString().trim());
                                                            user.setPassword(edtPassword.getText().toString().trim());
                                                            user.setPhone(edtPhone.getText().toString().trim());

                                                            if (state != null) {
                                                                user.setStateid(state.getId());
                                                            } else {
                                                                user.setStateid("0");
                                                            }
                                                            user.setCountryid(countryData.getId());
                                                            user.setLanguage(lang);
//                                                            if (sponsor_exist != null) {

                                                            user.setSponsor(sponsor_exist);
                                                            //   }


                                                            checkMobileExist(user);

                                                        } else {

                                                            // Toast.makeText(RegistrationActivity.this, "Please agree the terms and conditions", Toast.LENGTH_SHORT).show();
                                                            Utils.showAlertWithSingle(RegistrationActivity.this, "Please agree the terms and conditions", new DialogEventListener() {
                                                                @Override
                                                                public void onPositiveButtonClicked() {

                                                                }

                                                                @Override
                                                                public void onNegativeButtonClicked() {

                                                                }
                                                            });
                                                        }


                                                    } else {

                                                        //  Toast.makeText(RegistrationActivity.this, "Select your language", Toast.LENGTH_SHORT).show();
                                                        Utils.showAlertWithSingle(RegistrationActivity.this, "Select your language", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });
                                                    }


                                                } else {

                                                    //   Toast.makeText(RegistrationActivity.this, "password confirmation failed", Toast.LENGTH_SHORT).show();
                                                    Utils.showAlertWithSingle(RegistrationActivity.this, "password confirmation failed", new DialogEventListener() {
                                                        @Override
                                                        public void onPositiveButtonClicked() {

                                                        }

                                                        @Override
                                                        public void onNegativeButtonClicked() {

                                                        }
                                                    });
                                                }


                                            } else {

                                                //    Toast.makeText(RegistrationActivity.this, "Password must have atleast 8 characters", Toast.LENGTH_SHORT).show();
                                                Utils.showAlertWithSingle(RegistrationActivity.this, "Password must have atleast 8 characters", new DialogEventListener() {
                                                    @Override
                                                    public void onPositiveButtonClicked() {

                                                    }

                                                    @Override
                                                    public void onNegativeButtonClicked() {

                                                    }
                                                });
                                            }


                                        } else {

                                            //  Toast.makeText(RegistrationActivity.this, "Enter a password", Toast.LENGTH_SHORT).show();
                                            Utils.showAlertWithSingle(RegistrationActivity.this, "Enter password", new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });
                                        }

                                    }
                                        else{

                                            Utils.showAlertWithSingle(RegistrationActivity.this,"Click validate button to check if coupon code is valid , or click 'No' if you do not have coupon code",null);

                                        }

//                                        } else {
//
//                                            //  Toast.makeText(RegistrationActivity.this, "Enter phone number", Toast.LENGTH_SHORT).show();
//                                            Utils.showAlertWithSingle(RegistrationActivity.this, "Select your sponser", new DialogEventListener() {
//                                                @Override
//                                                public void onPositiveButtonClicked() {
//
//                                                }
//
//                                                @Override
//                                                public void onNegativeButtonClicked() {
//
//                                                }
//                                            });
//                                        }
                                    }
                                    else{

                                        Utils.showAlertWithSingle(RegistrationActivity.this, "Enter 10 digit mobile number for country india", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });

                                    }







                                    } else {

                                        //  Toast.makeText(RegistrationActivity.this, "Enter phone number", Toast.LENGTH_SHORT).show();
                                        Utils.showAlertWithSingle(RegistrationActivity.this, "Enter phone number", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    }




//                            }
//                            else{
//
//                                Toast.makeText(RegistrationActivity.this,"Select state",Toast.LENGTH_SHORT).show();
//
//                            }




                        }
                        else{

                         //   Toast.makeText(RegistrationActivity.this,"Select country",Toast.LENGTH_SHORT).show();
                            Utils.showAlertWithSingle(RegistrationActivity.this,"Select country", new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });
                        }


                    }
                    else{

                     //   Toast.makeText(RegistrationActivity.this,"Enter a valid email",Toast.LENGTH_SHORT).show();
                        Utils.showAlertWithSingle(RegistrationActivity.this,"Enter a valid email", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }


                }
                else{

                  //  Toast.makeText(RegistrationActivity.this,"Enter name",Toast.LENGTH_SHORT).show();
                    Utils.showAlertWithSingle(RegistrationActivity.this,"Enter name", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }




            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(RegistrationActivity.this,LoginActivity.class));

            }
        });

//        getState();
        getCountry();

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                CountryData countryData=(CountryData) spCountry.getSelectedItem();
                getState(Integer.parseInt(countryData.getId()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


//    public void showSponserAlert()
//    {
//
//
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//    dialog = new Dialog(RegistrationActivity.this);
//        dialog.setContentView(R.layout.layout_sponserverify);
//        dialog.setTitle(R.string.app_name);
//        int b=(int)(height/1.1);
//        dialog.getWindow().setLayout(width - 50,b );
//
//
//        EditText edtspPh=dialog.findViewById(R.id.edtspPh);
//
//         btnSubmit=dialog.findViewById(R.id.btnSubmit);
//          btnSubmitNumber=dialog.findViewById(R.id.btnSubmitNumber);
//         layout_sponsor=dialog.findViewById(R.id.layout_sponsor);
//
//        txtSponserName=dialog.findViewById(R.id.txtSponserName);
//        txtSponserCode=dialog.findViewById(R.id.txtSponserCode);
//
//
//
//        btnSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                dialog.dismiss();
//
//
//                if(sponsor_exist!=null&&sponsor_exist.getStatus()==1) {
//
//                    layout_sponsordata.setVisibility(View.VISIBLE);
//                    txtSponserCodedata.setText(sponsor_exist.getData().getRegCode());
//                    txtSponserNamedata.setText(sponsor_exist.getData().getFullName());
//                }
//                else {
//
//                    layout_sponsordata.setVisibility(View.GONE);
//                }
//
//            }
//        });
//
//        btnSubmitNumber.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(!edtspPh.getText().toString().equalsIgnoreCase(""))
//                {
//
//                    dialog.dismiss();
//
//                    String data=edtspPh.getText().toString();
//                    getCouponCode(data);
//
//                  //  getSponsor(data);
//                }
//                else {
//
//                    Toast.makeText(RegistrationActivity.this,"Enter coupon code",Toast.LENGTH_SHORT).show();
//
////                    Utils.showAlertWithSingle(RegistrationActivity.this,"Enter name", new DialogEventListener() {
////                        @Override
////                        public void onPositiveButtonClicked() {
////
////                        }
////
////                        @Override
////                        public void onNegativeButtonClicked() {
////
////                        }
////                    });
//
//
//                }
//
//
//            }
//        });
//
//        edtspPh.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                layout_sponsordata.setVisibility(View.GONE);
//                btnSubmit.setVisibility(View.GONE);
//               // btnSubmitNumber.setVisibility(View.VISIBLE);
//
//                sponsor_exist=null;
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
////call webservice
//
//
//        dialog.show();
//
//    }

    private void getCouponCode(String datacoupon)
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdof");





        Map<String,String> params=new HashMap<>();
//        params.put("uuid",uuid);
        //  params.put("mobile",data);
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(RegistrationActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {


                    Profiledata sponsor=new GsonBuilder().create().fromJson(data,Profiledata.class);

                    if(sponsor.getStatus()==1)
                    {
                        sponsor_exist=sponsor;
                        layout_sponsordata.setVisibility(View.VISIBLE);
                        //btnSubmit.setVisibility(View.VISIBLE);
                        // btnSubmitNumber.setVisibility(View.GONE);

                        txtSponserCodedata.setText(datacoupon);
                        txtSponserNamedata.setText(sponsor.getData().getFullName());

                        isValidCouponcode=true;
                    }
                    else {
                        layout_sponsor.setVisibility(View.GONE);
                        // btnSubmit.setVisibility(View.GONE);
                        // btnSubmitNumber.setVisibility(View.VISIBLE);
                        isValidCouponcode=false;
                        sponsor_exist=null;
                    }


                    // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                // Toast.makeText(RegistrationActivity.this,err,Toast.LENGTH_SHORT).show();
                Utils.showAlertWithSingle(RegistrationActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.getCouponcodeData+"?coupon="+datacoupon+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();







    }


    private void getSponsor(String data)
    {



//        ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"sdof");
//
//
//
//
//
//        Map<String,String> params=new HashMap<>();
////        params.put("uuid",uuid);
//     //  params.put("mobile",data);
////        params.put("password",edtPassword.getText().toString().trim());
//
//
//
//
//
//        new RequestHandler(RegistrationActivity.this, params, new ResponseHandler() {
//            @Override
//            public void onSuccess(String data) {
//                progressFragment.dismiss();
//                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
//                try {
//
//
//                    Sponsor sponsor=new GsonBuilder().create().fromJson(data,Sponsor.class);
//
//                    if(sponsor.getStatus()==1)
//                    {
//                        sponsor_exist=sponsor;
//                        layout_sponsordata.setVisibility(View.VISIBLE);
//                        //btnSubmit.setVisibility(View.VISIBLE);
//                       // btnSubmitNumber.setVisibility(View.GONE);
//
//                        txtSponserCodedata.setText(sponsor.getData().getRegCode());
//                        txtSponserNamedata.setText(sponsor.getData().getFullName());
//                    }
//                    else {
//                        layout_sponsor.setVisibility(View.GONE);
//                       // btnSubmit.setVisibility(View.GONE);
//                       // btnSubmitNumber.setVisibility(View.VISIBLE);
//
//                        sponsor_exist=null;
//                    }
//
//
//                    // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();
//
//
//
//                }catch (Exception e)
//                {
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(String err) {
//                progressFragment.dismiss();
//
//               // Toast.makeText(RegistrationActivity.this,err,Toast.LENGTH_SHORT).show();
//                Utils.showAlertWithSingle(RegistrationActivity.this,err, new DialogEventListener() {
//                    @Override
//                    public void onPositiveButtonClicked() {
//
//                    }
//
//                    @Override
//                    public void onNegativeButtonClicked() {
//
//                    }
//                });
//            }
//        },Utils.WebServiceMethodes.checkSponsor+"?mobile="+data+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();



//
//
//        Call<Sponsor> countryListCall = client.checkSponsor(data);
//
//        countryListCall.enqueue(new Callback<Sponsor>() {
//            @Override
//            public void onResponse(Call<Sponsor> call, Response<Sponsor> response) {
//                progressFragment.dismiss();
//
//                if(response!=null) {
//
//                    if(response.body()!=null) {
//
//
//
//                        Sponsor sponsor=response.body();
//
//                        if(sponsor.getStatus()==1)
//                        {
//                            sponsor_exist=sponsor;
//                            layout_sponsor.setVisibility(View.VISIBLE);
//                            btnSubmit.setVisibility(View.VISIBLE);
//
//                            txtSponserCode.setText(sponsor.getData().getRegCode());
//                            txtSponserName.setText(sponsor.getData().getFullName());
//                        }
//                        else {
//                            layout_sponsor.setVisibility(View.GONE);
//                            btnSubmit.setVisibility(View.GONE);
//
//                            sponsor_exist=null;
//                        }
//
//
//                                          }
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<Sponsor> call, Throwable t) {
//                progressFragment.dismiss();
//            }
//        });



    }



//    public class SponsorReceiver extends BroadcastReceiver
//    {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            Sponsor sponsor=(Sponsor) intent.getSerializableExtra("broadcastMessage");
//
//            if(sponsor.getStatus()==1)
//            {
//                sponsor_exist=sponsor;
//                layout_sponsor.setVisibility(View.VISIBLE);
//                btnSubmit.setVisibility(View.VISIBLE);
//
//                txtSponserCode.setText(sponsor.getData().getRegCode());
//                txtSponserName.setText(sponsor.getData().getFullName());
//            }
//            else {
//                layout_sponsor.setVisibility(View.GONE);
//                btnSubmit.setVisibility(View.GONE);
//
//                sponsor_exist=null;
//            }
//
//            //Log.e("TAAG",sponsor.getMessage()+"sdk");
//
//           // Toast.makeText(RegistrationActivity.this,sponsor.getMessage()+"",Toast.LENGTH_SHORT).show();
//
//
//        }
//    }



    private void setReceiver() {
//        myReceiver = new SponsorReceiver();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(FILTER_ACTION_KEY);
//
//
//        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, intentFilter);
    }


    @Override
    protected void onStart() {
        //setReceiver();
        super.onStart();
    }

    @Override
    protected void onStop() {

//        if(myReceiver!=null) {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
//        }
        super.onStop();
    }



    public void getCountry()
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
//        params.put("uuid",uuid);
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(RegistrationActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {


                    CountryList countryList=new GsonBuilder().create().fromJson(data,CountryList.class);

                    if(countryList.getStatus()==1) {


                        if (countryList.getData().size() > 0) {

                            Collections.sort(countryList.getData(), new Comparator<CountryData>() {
                                @Override
                                public int compare(CountryData countryData, CountryData t1) {
                                    return countryData.getCountryName().compareToIgnoreCase(t1.getCountryName());
                                }
                            });


                            CountrySpinnerAdapter countrySpinnerAdapter = new CountrySpinnerAdapter(RegistrationActivity.this, countryList.getData());
                            spCountry.setAdapter(countrySpinnerAdapter);

                            for(int i=0;i<countryList.getData().size();i++)
                            {
                                if(countryList.getData().get(i).getId().equalsIgnoreCase("1"))
                                {

                                    spCountry.setSelection(i);


                                }


                            }


                        }
                    }



                        // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

              //  Toast.makeText(RegistrationActivity.this,err,Toast.LENGTH_SHORT).show();
                Utils.showAlertWithSingle(RegistrationActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.getCountry, Request.Method.GET).submitRequest();




//
//
//        Call<CountryList>countryListCall=client.getCountry();
//
//        countryListCall.enqueue(new Callback<CountryList>() {
//            @Override
//            public void onResponse(Call<CountryList> call, Response<CountryList> response) {
//                progressFragment.dismiss();
//
//                if (response.body() != null) {
//
//                    try {
//
//                        CountryList countryList=response.body();
//                        if(countryList.getStatus()==1)
//                        {
//
//
//                            if(countryList.getData().size()>0)
//                            {
//
//                                Collections.sort(countryList.getData(), new Comparator<CountryData>() {
//                                    @Override
//                                    public int compare(CountryData countryData, CountryData t1) {
//                                        return countryData.getCountryName().compareToIgnoreCase(t1.getCountryName());
//                                    }
//                                });
//
//
//                                CountrySpinnerAdapter countrySpinnerAdapter=new CountrySpinnerAdapter(RegistrationActivity.this,countryList.getData());
//                                spCountry.setAdapter(countrySpinnerAdapter);
//
//
//
//                            }
//
//
//                        }
//                        else {
//
//                            Toast.makeText(RegistrationActivity.this,"No Data Found",Toast.LENGTH_SHORT).
//                                    show();
//
//                        }
//
//
//
//
//
//
//                    } catch (Exception e) {
//
//                    }
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CountryList> call, Throwable t) {
//
//                progressFragment.dismiss();
//
//            }
//        });

    }


    public void getState(int countryid)
        {

//            @Headers({"Accept: application/json"})
//
//            @GET("getState.php")
//            Call<StateData> getState(@Query("countryid")String countryid);

            final ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"fkjfk");



            Map<String,String> params=new HashMap<>();
       //params.put("countryid",countryid+"");
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





            new RequestHandler(RegistrationActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                    try {



                        StateData stateData=new GsonBuilder().create().fromJson(data,StateData.class);

                        if(stateData.getStatus()==1)
                        {
                            spstate.setVisibility(View.VISIBLE);

                            Collections.sort(stateData.getData(), new Comparator<State>() {
                                @Override
                                public int compare(State state, State t1) {
                                    int a=0;






                                    return state.getStateName().compareTo(t1.getStateName());
                                }
                            });



                            spstate.setAdapter(new StateListAdapter(RegistrationActivity.this,stateData.getData()));

                        }
                        else {
                            spstate.setVisibility(View.GONE);
                            spstate.setAdapter(new StateListAdapter(RegistrationActivity.this,stateData.getData()));

//                            Toast.makeText(RegistrationActivity.this,"No Data Found",Toast.LENGTH_SHORT).
//                                    show();

//                            Utils.showAlertWithSingle(RegistrationActivity.this,"No Data Found", new DialogEventListener() {
//                                @Override
//                                public void onPositiveButtonClicked() {
//
//                                }
//
//                                @Override
//                                public void onNegativeButtonClicked() {
//
//                                }
//                            });

                        }



                        // Toast.makeText(LoginActivity.this,"Sorry, You cannot login to another device.Your data will lost",Toast.LENGTH_SHORT).show();



                    }catch (Exception e)
                    {

                    }

                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                   // Toast.makeText(RegistrationActivity.this,err,Toast.LENGTH_SHORT).show();
                    Utils.showAlertWithSingle(RegistrationActivity.this,err, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }
            },Utils.WebServiceMethodes.getState+"?countryid="+countryid+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();

















//
//            Call<StateData> jsonObjectCall=client.getState(countryid+""
//
//            );
//            jsonObjectCall.enqueue(new Callback<StateData>() {
//                @Override
//                public void onResponse(Call<StateData> call, Response<StateData> response) {
//
//                    progressFragment.dismiss();
//                    if(response.body()!=null)
//                    {
//
//                        try{
//
////                            JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                            StateData stateData=response.body();
//
//                            if(stateData.getStatus()==1)
//                            {
//                                spstate.setVisibility(View.VISIBLE);
//
//                                Collections.sort(stateData.getData(), new Comparator<State>() {
//                                    @Override
//                                    public int compare(State state, State t1) {
//                                        int a=0;
//
//
//
//
//
//
//                                        return state.getStateName().compareTo(t1.getStateName());
//                                    }
//                                });
//
//
//
//spstate.setAdapter(new StateListAdapter(RegistrationActivity.this,stateData.getData()));
//
//                            }
//                            else {
//                                spstate.setVisibility(View.GONE);
//                                spstate.setAdapter(new StateListAdapter(RegistrationActivity.this,stateData.getData()));
//
//                                Toast.makeText(RegistrationActivity.this,"No Data Found",Toast.LENGTH_SHORT).
//                                        show();
//
//                            }
//
//
//
//                        }catch (Exception e)
//                        {
//
//                        }



                    //}


//                }
//
//                @Override
//                public void onFailure(Call<StateData> call, Throwable t) {
//
//                    progressFragment.dismiss();
//                }
//            });


        }


    public void checkMobileExist(final User usr)
    {

        Utils.usr=usr;

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");




        Map<String,String> params=new HashMap<>();
        //params.put("countryid",countryid+"");
//        params.put("mobile",edtPhone.getText().toString().trim());
//        params.put("password",edtPassword.getText().toString().trim());





        new RequestHandler(RegistrationActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try{

                    //  JSONObject jsonObject=new JSONObject(response.body().toString());\

                    UserDetails userData=new GsonBuilder().create().fromJson(data,UserDetails.class);


                    if(userData.getStatus()==0)
                    {

                        // Toast.makeText(RegistrationActivity.this,"Your mobile number does not already exists",Toast.LENGTH_SHORT).show();





                        Intent i=  new Intent(RegistrationActivity.this,OTPActivity.class);

//                        i.putExtra("Data",usr);


                        startActivity(i);



                    }
                    else {

                      //  Toast.makeText(RegistrationActivity.this,"Your mobile number is already exists",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(RegistrationActivity.this,"Your mobile number is already exists", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });


                    }



                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

             //   Toast.makeText(RegistrationActivity.this,err,Toast.LENGTH_SHORT).show();
                Utils.showAlertWithSingle(RegistrationActivity.this,err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });
            }
        },Utils.WebServiceMethodes.getUserByMobile+"?mobile="+usr.getPhone()+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();






















//
//
//        Call<JsonObject> jsonObjectCall=client.getUserByMobile(
//                usr.getPhone()
//        );
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//                      //  JSONObject jsonObject=new JSONObject(response.body().toString());\
//
//                        UserDetails userData=new GsonBuilder().create().fromJson(response.body().toString(),UserDetails.class);
//
//
//                        if(userData.getStatus()==0)
//                        {
//
//                           // Toast.makeText(RegistrationActivity.this,"Your mobile number does not already exists",Toast.LENGTH_SHORT).show();
//
//
//
//
//
//                            Intent i=  new Intent(RegistrationActivity.this,OTPActivity.class);
//
//                            i.putExtra("Data",usr);
//
//
//                            startActivity(i);
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(RegistrationActivity.this,"Your mobile number is already exists",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });




    }
}
