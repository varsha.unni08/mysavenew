package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.NotificationAdapter;
import com.centroid.integraaccounts.data.domain.NotificationList;
import com.centroid.integraaccounts.data.domain.Notificationdata;
import com.centroid.integraaccounts.data.domain.Notificationmessage;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity {

    ImageView imgback;
    TextView txtNodata;

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recycler=findViewById(R.id.recycler);
        txtNodata=findViewById(R.id.txtNodata);



        getNotifications();




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getNotifications();
    }

    public void getNotifications()
    {



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();


        new RequestHandler(NotificationsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{

NotificationList notificationList=new GsonBuilder().create().fromJson(data,NotificationList.class);

                        if(notificationList.getStatus()==1)
                        {

                            recycler.setVisibility(View.VISIBLE);
                            txtNodata.setVisibility(View.GONE);


                            if(notificationList.getData().size()>0)
                            {

                                List<Notificationmessage>notificationdata=new ArrayList<>();

                                for (Notificationmessage notifi:notificationList.getData())
                                {

                                    notificationdata.add(notifi);

                                }






                                NotificationAdapter notificationAdapter=new NotificationAdapter(NotificationsActivity.this,notificationdata);
                                recycler.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this));
                                recycler.setAdapter(notificationAdapter);







                            }





                        }
                        else {

                            recycler.setVisibility(View.GONE);
                            txtNodata.setVisibility(View.VISIBLE);

                            // Toast.makeText(NotificationsActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getNotifications+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();


//
//
//        Call<NotificationList> jsonObjectCall=client.getNotifications(new PreferenceHelper(NotificationsActivity.this).getData(Utils.userkey)
//
//        );
//        jsonObjectCall.enqueue(new Callback<NotificationList>() {
//            @Override
//            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
//
//
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//                            recycler.setVisibility(View.VISIBLE);
//                            txtNodata.setVisibility(View.GONE);
//
//
//                            if(response.body().getData().size()>0)
//                            {
//
//                                List<Notificationdata>notificationdata=new ArrayList<>();
//
//                                for (Notificationdata notifi:response.body().getData())
//                                {
//
//                                    notificationdata.add(notifi);
//
//                                }
//
//                                Collections.reverse(notificationdata);
//
//
//
//
//                                NotificationAdapter notificationAdapter=new NotificationAdapter(NotificationsActivity.this,notificationdata);
//                                recycler.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this));
//                                recycler.setAdapter(notificationAdapter);
//
//
//
//
//
//
//
//                            }
//
//
//
//
//
//                        }
//                        else {
//
//                            recycler.setVisibility(View.GONE);
//                            txtNodata.setVisibility(View.VISIBLE);
//
//                           // Toast.makeText(NotificationsActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<NotificationList> call, Throwable t) {
//
//
//            }
//        });
    }
}
