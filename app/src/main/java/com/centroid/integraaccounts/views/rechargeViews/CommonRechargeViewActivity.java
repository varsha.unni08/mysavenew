package com.centroid.integraaccounts.views.rechargeViews;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.MobileRechargeResponse;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.messageservice.MessageService;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.SplashActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommonRechargeViewActivity extends AppCompatActivity {

    String urldata = "",phone="",operator="";
    VideoView simpleVideoView;
    LinearLayout layout_rpid,layout_agentid;
    TextView txtRpid,txtAgentid,txtHead;
    ImageView imgoperator,imgback;
    Button btnbkhome;
    String fullString="",transaction_id="0";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_recharge_view);
        getSupportActionBar().hide();
        simpleVideoView=findViewById(R.id.simpleVideoView);
        btnbkhome=findViewById(R.id.btnbkhome);
        layout_rpid=findViewById(R.id.layout_rpid);
        layout_agentid=findViewById(R.id.layout_agentid);
        txtRpid=findViewById(R.id.txtRpid);
        txtAgentid=findViewById(R.id.txtAgentid);
        txtHead=findViewById(R.id.txtHead);
        imgback=findViewById(R.id.imgback);
        imgoperator=findViewById(R.id.imgoperator);
//        btnbkhome=findViewById(R.id.btnbkhome);

        Intent intent=getIntent();
        if(intent!=null)
        {

            urldata=intent.getStringExtra("url");
            transaction_id=intent.getStringExtra("transaction_id");
            phone=   intent.getStringExtra("phone");
            operator=  intent.getStringExtra("operator");

            if (operator.compareTo("VI") == 0) {


                imgoperator.setImageResource(R.drawable.vi);
//            txtSpinner.setText("VI");
            }
            else if(operator.compareTo("Airtel") == 0)
            {
                imgoperator.setImageResource(R.drawable.airtel);
            }
            else if(operator.compareTo("Jio") == 0)
            {
                imgoperator.setImageResource(R.drawable.jio);
            }
            else if(operator.compareTo("BSNL") == 0)
            {
                imgoperator.setImageResource(R.drawable.bsnl);
            }

           else if(operator.compareTo("Dish TV")==0)
            {

                imgoperator.setImageResource(R.drawable.dishtv);
            }
            else if(operator.compareTo("Airtel Digital TV")==0)
            {
                imgoperator.setImageResource(R.drawable.airteldigitaltv);

            }
            else if(operator.compareTo("Sun Direct")==0)
            {

                imgoperator.setImageResource(R.drawable.sundirect);
            }
            else if(operator.compareTo("Tata Sky")==0)
            {

                imgoperator.setImageResource(R.drawable.ttsky);
            }
            else{

                imgoperator.setImageResource(R.drawable.d2h);
            }


        }


        simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.loader));
        simpleVideoView.start();

        simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.loader));
                simpleVideoView.start();
            }
        });
        rechargePhone();

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnbkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    public void rechargePhone()
    {

        int min = 1;
        int max = 10000;
        int random = new Random().nextInt((max - min) + 1) + min;

//        String spkey = "";
//        String phone = mobilenumber;

//        double amount = Double.parseDouble(Amount);
//        spkey = arrspkey[selectedposition];

//        String apireqid = random + "";
//        String customerno = mobilenumber;

       // String urldata = recharge_url + "phone=" + phone + "&amount=" + amount + "&spkey=" + spkey + "&apireqid=" + apireqid + "&customerno=" + customerno;


        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("timestamp", Utils.getTimestamp());
                    String fullurl = urldata + "&timestamp=" + Utils.getTimestamp();
                    fullString = "";
                    URL url = new URL(fullurl);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fullString += line;
                    }
                    reader.close();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {



                            if (!fullString.isEmpty()) {
                                MobileRechargeResponse mobileRechargeResponse = new GsonBuilder().create().fromJson(fullString, MobileRechargeResponse.class);

                                String msg = mobileRechargeResponse.getMsg();
                              //  int status = 0;
                                if (mobileRechargeResponse.getStatus().toString().equalsIgnoreCase("2")) {
                                   // status = 1;
//                                    updateRechargePaymentStatus(paymentdata_id,1,0);
//                                    postTransactionData(mobileRechargeResponse, 1);



                                    simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tick));
                                            simpleVideoView.start();
                                        }
                                    });


                                    Utils.showAlertWithSingle(CommonRechargeViewActivity.this, "Successfully completed your recharge..", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                    showNotification("Successfully completed your recharge..","Recharge");

                                    layout_agentid.setVisibility(View.VISIBLE);
                                    layout_rpid.setVisibility(View.VISIBLE);
                                    btnbkhome.setVisibility(View.VISIBLE);
                                    txtAgentid.setText("Agent ID : "+mobileRechargeResponse.getAgentid());
                                    txtRpid.setText("RP ID : "+mobileRechargeResponse.getRpid());
                                    txtHead.setText("Recharge Success");
                                    updateRechargeStatus(mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid(),"1");

                                }
                                else   if(mobileRechargeResponse.getStatus().toString().equalsIgnoreCase("1"))
                                {
//                                    status=2;
//                                    updateRechargePaymentStatus(paymentdata_id,2,0);
//                                    postTransactionData(mobileRechargeResponse,2);

                                    showNotification("Recharge process is pending.Please click back button to recharge dashboard and retry again","Recharge");

                                    updateRechargeStatus(mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid(),"2");
                                    simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.error));
                                            simpleVideoView.start();
                                        }
                                    });
                                    txtHead.setText("Recharge process is pending.Please click back button to recharge dashboard and retry again");
                                    btnbkhome.setVisibility(View.VISIBLE);
                                    Utils.showAlertWithSingle(CommonRechargeViewActivity.this, "Recharge process is pending.Please click back button to recharge dashboard and retry again", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                }





                                else {


                                    updateRechargeStatus(mobileRechargeResponse.getRpid(),mobileRechargeResponse.getAgentid(),"0");
//                                    updateRechargePaymentStatus(paymentdata_id,0,0);
//                                    postTransactionData(mobileRechargeResponse, 0);
                                    txtHead.setText("Recharge process failed.Please click back button to recharge dashboard and retry again");

                                    showNotification("Recharge process failed.Please click back button to recharge dashboard and retry again","Recharge");

                                    btnbkhome.setVisibility(View.VISIBLE);

                                    simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.error));
                                            simpleVideoView.start();
                                        }
                                    });

                                    Utils.showAlertWithSingle(CommonRechargeViewActivity.this, "Recharge process failed.Please click back button to recharge dashboard and retry again", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });



                                }





                            }


                        }
                    });


                } catch (Exception e) {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {


                            txtHead.setText("Recharge process failed.Please click back button to recharge dashboard and retry again");

                            updateRechargeStatus("","","0");
                            simpleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mediaPlayer) {
                                    simpleVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.error));
                                    simpleVideoView.start();
                                }
                            });


//                            Utils.showAlertWithSingle(CommonRechargeViewActivity.this, "Your recharge failed. Please try again later...", new DialogEventListener() {
//                                @Override
//                                public void onPositiveButtonClicked() {
//
//                                }
//
//                                @Override
//                                public void onNegativeButtonClicked() {
//
//                                }
//                            });

                        }
                    });


                }
            }
        });

    }


    public void updateRechargeStatus(String rpid,String agentid,String status)
    {




        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());
        params.put("status", status);
        params.put("id", transaction_id);
        params.put("rp_id", rpid);
        params.put("agent_id", agentid);
        if(status.equalsIgnoreCase("1")) {

            params.put("genstatus", "1");
        }
        else{

            params.put("genstatus", "0");
        }



        new RequestHandler(CommonRechargeViewActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                try {

                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        String msg = jsonObject.getString("message");
                        updateGenStatus();


                    } else {


                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(String err) {


                Toast.makeText(CommonRechargeViewActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.updateRechargeStatus + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }




    public void updateGenStatus()
    {




        Map<String, String> params = new HashMap<>();
        params.put("timestamp", Utils.getTimestamp());

        params.put("id", transaction_id);





        new RequestHandler(CommonRechargeViewActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                try {

                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getInt("status") == 1) {
                        String msg = jsonObject.getString("message");


                    } else {


                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(String err) {


                Toast.makeText(CommonRechargeViewActivity.this, err, Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.updateGenStatus + "?timestamp=" + Utils.getTimestamp(), Request.Method.POST).submitRequest();

    }




private void showNotification(String message,String title)
{


    int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);


        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "mychannel";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel;
// Create a notification and set the notification channel.

        Notification notification = null;

        NotificationManager mNotificationManager;

        Intent notificationIntent = new Intent(CommonRechargeViewActivity.this, SplashActivity.class);
//                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        PendingIntent pendingIntent;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                        pendingIntent = PendingIntent.getActivity(this,
//                                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE);
//
//                    }else {
//                        pendingIntent = PendingIntent.getActivity(this,
//                                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                    }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            pendingIntent = PendingIntent.getActivity(this, 1251, notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
            notification = new Notification.Builder(CommonRechargeViewActivity.this, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.logo).setColor(getColor(R.color.appbg))
                    .setContentIntent(pendingIntent)

                    .setChannelId(CHANNEL_ID)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .build();

            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);

        } else {
            pendingIntent = PendingIntent.getActivity(this, 1251, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notification = new Notification.Builder(CommonRechargeViewActivity.this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.logo)
                    .setContentIntent(pendingIntent)
                    .setStyle(new Notification.BigTextStyle().bigText(message))

                    .build();
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

//                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        notification.setSmallIcon(R.drawable.icon_transperent);
//                        notification.setColor(getResources().getColor(R.color.notification_color));
//                    } else {
//                        notification.setSmallIcon(R.drawable.icon);
//                    }


        MediaPlayer mp;
        mp = MediaPlayer.create(CommonRechargeViewActivity.this, R.raw.notifysound);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();


// Issue the notification.
        mNotificationManager.notify(m, notification);



}

}