package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.Notificationdata;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public class NotificationdetailsActivity extends AppCompatActivity {

    ImageView imgback;
    TextView txtHead,txtMessage;

    Notificationdata notificationdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificationdetails);
        getSupportActionBar().hide();
        notificationdata=(Notificationdata)getIntent().getSerializableExtra("Data");
        imgback=findViewById(R.id.imgback);
        txtHead=findViewById(R.id.txtHead);
        txtMessage=findViewById(R.id.txtMessage);

        if(notificationdata!=null) {
            if(notificationdata.getMessage()!=null) {

            txtHead.setText(notificationdata.getMessage().getTitle());
            txtMessage.setText(notificationdata.getMessage().getMessage());


            updateNotification();
            }
        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void updateNotification()
    {

//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("updateNotificationStatus.php")
//        Call<JsonObject> updateNotificationStatus(@Header("Authorization") String Authorization,@Field("messageid") String messageid);



        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(NotificationdetailsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {





                        }
                        else {

                            // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.updateNotificationStatus, Request.Method.POST).submitRequest();








//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"fkjfk");
//
//
//String key=new PreferenceHelper(NotificationdetailsActivity.this).getData(Utils.userkey);
//
//
//
//
//        Call<JsonObject> jsonObjectCall=client.updateNotificationStatus(key,notificationdata.getId());
//        jsonObjectCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        if(jsonObject.getInt("status")==1)
//                        {
//
//
//
//
//
//                        }
//                        else {
//
//                           // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });


    }
}
