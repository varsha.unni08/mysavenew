package com.centroid.integraaccounts.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.BankSpinnerAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.receivers.DateTimeReceiver;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.centroid.integraaccounts.Constants.Utils.Requestcode.ForAccountSettingsInvestmentRequestcode;

public class AddinvestActivity extends AppCompatActivity {


    Spinner spinvestname;
    EditText edtinvestamount,edtmonthlyamount,edtremarks,edtnoOfinstallment;

    Spinner spStatus;

    TextView txtdatepick,txtclosingdate,txtprofile;

    RelativeLayout reldatepick;

    ImageView imgDatepick,imgclosing,imgback;
    Button btnAdd;

    String closingdate="";
    int date=0;

    CommonData commonData;

    String investid="",type="";

    PendingIntent pendingIntent;

    FloatingActionButton fabadd;

    Resources resources;

    List<String>taskid;
    String newaccount="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getSupportActionBar().hide();

        taskid=new ArrayList<>();
        commonData=(CommonData) getIntent().getSerializableExtra("invest");

        imgback=findViewById(R.id.imgback);
        imgclosing=findViewById(R.id.imgclosing);
        imgDatepick=findViewById(R.id.imgDatepick);
        reldatepick=findViewById(R.id.reldatepick);
        txtdatepick=findViewById(R.id.txtdatepick);
        txtclosingdate=findViewById(R.id.txtclosingdate);
        txtprofile=findViewById(R.id.txtprofile);
        edtinvestamount=findViewById(R.id.edtinvestamount);
        edtmonthlyamount=findViewById(R.id.edtmonthlyamount);
        spinvestname=findViewById(R.id.spinvestname);
        edtremarks=findViewById(R.id.edtremarks);
      //  txtvisitlogin=findViewById(R.id.txtvisitlogin);

        spStatus=findViewById(R.id.spStatus);
        btnAdd=findViewById(R.id.btnAdd);
        fabadd=findViewById(R.id.fabadd);
        edtnoOfinstallment=findViewById(R.id.edtnoOfinstallment);



        String languagedata = LocaleHelper.getPersistedData(AddinvestActivity.this, "en");
        Context context= LocaleHelper.setLocale(AddinvestActivity.this, languagedata);

        resources=context.getResources();

        edtinvestamount.setHint(resources.getString(R.string.openingbalance));
        txtclosingdate.setText(resources.getString(R.string.selectclosingdate));
        edtremarks.setHint(resources.getString(R.string.enterremarks));
        edtmonthlyamount.setHint(resources.getString(R.string.monthlyamount));
        btnAdd.setText(resources.getString(R.string.save));
        txtprofile.setText(resources.getString(R.string.investment));
      //  txtvisitlogin.setText(resources.getString(R.string.visitlogin));
        txtdatepick.setText(resources.getString(R.string.dtofpayment));
        edtnoOfinstallment.setHint(Utils.getCapsSentences(context,resources.getString(R.string.noofinstallment)));


        String arr[]=resources.getStringArray(R.array.investtype);



        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spStatus.setAdapter(spinnerArrayAdapter);






        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // Toast.makeText(AddinvestActivity.this,resources.getString(R.string.accsettinginvest),Toast.LENGTH_SHORT).show();


               Intent i= new Intent(AddinvestActivity.this, AccountsettingsActivity.class);
               i.putExtra(Utils.Requestcode.AccVoucherAll,ForAccountSettingsInvestmentRequestcode);
                startActivityForResult(i,ForAccountSettingsInvestmentRequestcode);
            }
        });

        edtnoOfinstallment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(date!=0)
                {
                   calculateClosingDate();
                }

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        if(commonData!=null)
        {

            btnAdd.setText(resources.getString(R.string.update));

                try {

                JSONObject jsonObject = new JSONObject(commonData.getData());

                investid=jsonObject.getString("name");
                type=jsonObject.getString("type");
                edtinvestamount.setText(jsonObject.getString("amount"));

                edtmonthlyamount.setText(jsonObject.getString("month_amount"));
                edtremarks.setText(jsonObject.getString("remarks"));



                txtdatepick.setText(jsonObject.getInt("monthly_date")+"");
                txtclosingdate.setText(jsonObject.getString("closing_date"));


                date=jsonObject.getInt("monthly_date");
                closingdate=jsonObject.getString("closing_date");

                if(jsonObject.has("numberOfinstallment"))
                {
                    edtnoOfinstallment.setText(jsonObject.getString("numberOfinstallment"));
                }


                String taskarray=jsonObject.getString("Task");

                if(!taskarray.equalsIgnoreCase(""))
                {
                    JSONArray jsonArray=new JSONArray(taskarray);

                    for (int i=0;i<jsonArray.length();i++)
                    {

                        JSONObject jsonObj=jsonArray.getJSONObject(i);

                        String Taskid=jsonObj.getString("Taskid");

                        taskid.add(Taskid);

                    }



                }


//                    JSONArray jsonArray=new JSONArray();
//
//                    for (String tid:taskid)
//                    {
//                        JSONObject jsonObject1=new JSONObject();
//
//                        jsonObject1.put("Taskid", tid);
//                        jsonArray.put(jsonObject1);
//
//                    }
//
//                    jsonObject.put("Task", jsonArray.toString());




                String arrsp[]=resources.getStringArray(R.array.investtype);

                for(int i=0;i<arrsp.length;i++)
                {
                    if(arrsp[i].equalsIgnoreCase(jsonObject.getString("type")))
                    {
                        spStatus.setSelection(i);
                        break;
                    }
                }




            } catch (Exception e) {

            }



        }






        getInvestmentData();









        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate();
            }
        });

        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate();
            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtclosingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             //   if(spStatus.getSelectedItemPosition()==2) {

                    showDatePicker();
              //  }

            }
        });

        imgclosing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // if(spStatus.getSelectedItemPosition()==2) {

                    showDatePicker();
             //   }

            }
        });

        spinvestname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {

                    CommonData cm = (CommonData) spinvestname.getSelectedItem();

                    JSONObject jsonObject = new JSONObject(cm.getData());
                    String Amount = jsonObject.getString("Amount");

                   // double amt = Double.parseDouble(Amount);

                    double amt =0;


                    amt = Double.parseDouble(Amount);

                    String Type=jsonObject.getString("Type");


                    //  amt = Double.parseDouble(Amount);

                    if(Type.equalsIgnoreCase("Credit"))
                    {
                        amt=amt*-1;
                    }

                    String id = cm.getId();

                    Calendar calendar = Calendar.getInstance();
                    int m = calendar.get(Calendar.MONTH) + 1;
                    int d = calendar.get(Calendar.DAY_OF_MONTH);
                    int y = calendar.get(Calendar.YEAR);
                    String enddate = d + "-" + m + "-" + y;

                    if(commonData==null) {
                        double closingbalance = getClosingBalance(amt, id, enddate, "");
                        edtinvestamount.setText(closingbalance + "");
                    }
                }catch (Exception e)
                {

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(i==1)
                {

                    edtmonthlyamount.setVisibility(View.VISIBLE);
                    edtnoOfinstallment.setVisibility(View.VISIBLE);
                    reldatepick.setVisibility(View.VISIBLE);

                }
                else if(i==2)
                {
                    edtmonthlyamount.setVisibility(View.GONE);
                    reldatepick.setVisibility(View.GONE);
                    edtnoOfinstallment.setVisibility(View.GONE);

                }
                else{

                    edtmonthlyamount.setVisibility(View.GONE);
                    reldatepick.setVisibility(View.GONE);
                    edtnoOfinstallment.setVisibility(View.GONE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (String tid:taskid)
                {

                   new DatabaseHelper(AddinvestActivity.this).deleteData(tid,Utils.DBtables.TABLE_TASK);
                    Utils.playSimpleTone(AddinvestActivity.this);
                }


                    if(!edtinvestamount.getText().toString().equalsIgnoreCase(""))
                    {


                        if(!spStatus.getSelectedItem().toString().equalsIgnoreCase("Select investment type"))
                        {

                            if(spStatus.getSelectedItem().toString().equalsIgnoreCase("Monthly"))
                            {

                                if(!edtmonthlyamount.getText().toString().equalsIgnoreCase(""))
                                {

                                    if(!edtnoOfinstallment.getText().toString().equalsIgnoreCase("")) {

                                        if(!edtnoOfinstallment.getText().toString().equalsIgnoreCase("0")) {


                                        if (date != 0) {


                                            //if(!edtremarks.getText().toString().equalsIgnoreCase("")) {


                                            if (!closingdate.equalsIgnoreCase("")) {


                                                try {

                                                    JSONObject jsonObject = new JSONObject();

                                                    String data = ((CommonData) spinvestname.getSelectedItem()).getId();


                                                    jsonObject.put("name", data);
                                                    jsonObject.put("amount", edtinvestamount.getText().toString());
                                                    jsonObject.put("type", spStatus.getSelectedItem().toString());
                                                    jsonObject.put("month_amount", edtmonthlyamount.getText().toString());
                                                    jsonObject.put("closing_date", closingdate);
                                                    jsonObject.put("monthly_date", date);
                                                    jsonObject.put("numberOfinstallment", edtnoOfinstallment.getText().toString());
                                                    jsonObject.put("remarks", edtremarks.getText().toString());


                                                    addTaskstoReminder(jsonObject.toString());

                                                    JSONArray jsonArray=new JSONArray();

                                                    for (String tid:taskid)
                                                    {
                                                        JSONObject jsonObject1=new JSONObject();

                                                        jsonObject1.put("Taskid", tid);
                                                        jsonArray.put(jsonObject1);

                                                    }

                                                    jsonObject.put("Task", jsonArray.toString());

                                                    Utils.playSimpleTone(AddinvestActivity.this);
                                                    addInvestment(jsonObject.toString());


                                                } catch (Exception e) {

                                                }


                                            } else {

                                                Toast.makeText(AddinvestActivity.this, resources.getString(R.string.selectclosingdate), Toast.LENGTH_SHORT).show();

                                            }

//                                        }
//
//                                        else {
//
//
//                                            Toast.makeText(AddinvestActivity.this, resources.getString(R.string.enterremarks), Toast.LENGTH_SHORT).show();
//
//                                        }

                                        } else {

                                            Toast.makeText(AddinvestActivity.this, resources.getString(R.string.dtofpayment), Toast.LENGTH_SHORT).show();

                                        }

                                        }
                                        else {

                                            Toast.makeText(AddinvestActivity.this, "Number of installment is not to be zero", Toast.LENGTH_SHORT).show();


                                        }

                                    }
                                    else {

                                        Toast.makeText(AddinvestActivity.this, "Enter number of installment", Toast.LENGTH_SHORT).show();


                                    }



                                }
                                else {

                                    Toast.makeText(AddinvestActivity.this,resources.getString(R.string.amount),Toast.LENGTH_SHORT).show();

                                }



                            }
                            else {





                                if(!closingdate.equalsIgnoreCase(""))
                                {

                                  //  if(!edtremarks.getText().toString().equalsIgnoreCase("")) {



                                        try {

                                            String data=((CommonData)spinvestname.getSelectedItem()).getId();


                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("name", data);
                                            jsonObject.put("amount", edtinvestamount.getText().toString());
                                            jsonObject.put("type", spStatus.getSelectedItem().toString());
                                            jsonObject.put("month_amount", "");
                                            jsonObject.put("closing_date", closingdate);
                                            jsonObject.put("numberOfinstallment", "");
                                            jsonObject.put("monthly_date", "1");
                                            jsonObject.put("remarks",edtremarks.getText().toString());

                                            addTaskstoReminder(jsonObject.toString());


                                            JSONArray jsonArray=new JSONArray();

                                            for (String tid:taskid)
                                            {
                                                JSONObject jsonObject1=new JSONObject();

                                                jsonObject1.put("Taskid", tid);
                                                jsonArray.put(jsonObject1);

                                            }

                                            jsonObject.put("Task", jsonArray.toString());







                                            addInvestment(jsonObject.toString());


                                        } catch (Exception e) {

                                        }
//                                    }
//
//                                    else {
//
//                                        Toast.makeText(AddinvestActivity.this,resources.getString(R.string.enterremarks),Toast.LENGTH_SHORT).show();
//
//                                    }


                                }
                                else {

                                    Toast.makeText(AddinvestActivity.this,resources.getString(R.string.selectclosingdate),Toast.LENGTH_SHORT).show();

                                }




                            }





                        }
                        else {


                            Toast.makeText(AddinvestActivity.this,resources.getString(R.string.selectinvesttype),Toast.LENGTH_SHORT).show();
                        }

                    }
                    else {


                        Toast.makeText(AddinvestActivity.this,resources.getString(R.string.amount),Toast.LENGTH_SHORT).show();
                    }




            }
        });


    }

    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(AddinvestActivity.this, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode== ForAccountSettingsInvestmentRequestcode&&resultCode==RESULT_OK&&data!=null)
        {

            newaccount=data.getStringExtra("AccountAdded");


            getInvestmentData();



        }



    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }


    public void addInvestment(String data)
    {
        if(commonData!=null) {

            new DatabaseHelper(AddinvestActivity.this).updateData(commonData.getId(),data,Utils.DBtables.INVESTMENT_table);
            Utils.playSimpleTone(AddinvestActivity.this);
            Toast.makeText(AddinvestActivity.this,resources.getString(R.string.datasaved),Toast.LENGTH_SHORT).show();


            onBackPressed();

        }
        else {





            new DatabaseHelper(AddinvestActivity.this).addData(Utils.DBtables.INVESTMENT_table,data);
            Utils.playSimpleTone(AddinvestActivity.this);
            Toast.makeText(AddinvestActivity.this,resources.getString(R.string.datasaved),Toast.LENGTH_SHORT).show();

        }

       // addTaskstoReminder(data);



        edtinvestamount.setText("");
        //edtinvestname.setText("");
        spStatus.setSelection(0);
        edtmonthlyamount.setText("");
        txtdatepick.setText(resources.getString(R.string.dtofpayment));
        date=0;
        txtclosingdate.setText(resources.getString(R.string.selectclosingdate));
        edtremarks.setText("");



    }





    public void showDate()
    {

        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        double h=displayMetrics.heightPixels/1.2;
        int height = (int)h;
        int width = displayMetrics.widthPixels;

        final Dialog dialog = new Dialog(AddinvestActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_date_picker);

        TextView txtHead=dialog.findViewById(R.id.txtHead);
        txtHead.setVisibility(View.VISIBLE);

        txtHead.setText("Select a date in which you can invest in all months");

        Button date_time_set=dialog.findViewById(R.id.date_time_set);




        final NumberPicker npmonth=dialog.findViewById(R.id.npmonth);


        npmonth.setWrapSelectorWheel(true);
        npmonth.setMinValue(1);
        npmonth.setMaxValue(31);



        final NumberPicker npyear=dialog.findViewById(R.id.npyear);


        npyear.setVisibility(View.GONE);



        dialog.getWindow().setLayout(width,height);







        date_time_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                date=npmonth.getValue();

                txtdatepick.setText(date+"" );

                if(!edtnoOfinstallment.getText().toString().equalsIgnoreCase(""))
                {

                    calculateClosingDate();
                }


            }
        });


        dialog.show();

    }





    public void getInvestmentData()
    {

       // spinvestname


            List<CommonData> commonData_filtered=new ArrayList<>();

            List<CommonData>commonData=new DatabaseHelper(AddinvestActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            for (CommonData cm:commonData)
            {

                try {

                    JSONObject jsonObject = new JSONObject(cm.getData());

                    String acctype= jsonObject.getString("Accounttype");

                    if(acctype.equalsIgnoreCase("Investment"))
                    {



                        commonData_filtered.add(cm);
                    }


                    //




                }catch (Exception e)
                {

                }

            }


            BankSpinnerAdapter bankSpinnerAdapter=new BankSpinnerAdapter(AddinvestActivity.this,commonData_filtered);
            spinvestname.setAdapter(bankSpinnerAdapter);



        for (int i=0;i<commonData_filtered.size();i++)

        {

            CommonData cm=commonData_filtered.get(i);

            if(cm.getId().equalsIgnoreCase(investid))
            {

                spinvestname.setSelection(i);
                break;
            }





        }

       if(!newaccount.equalsIgnoreCase("")) {

           try {

               for (int i = 0; i < commonData_filtered.size(); i++) {

                   CommonData cm = commonData_filtered.get(i);
                   JSONObject jsonObject = new JSONObject(cm.getData());

                   String acctype = jsonObject.getString("Accountname");

                   if (acctype.equalsIgnoreCase(newaccount)) {

                       spinvestname.setSelection(i);
                       break;
                   }


               }

           } catch (Exception e) {

           }

       }


        }

    public void calculateClosingDate()
    {
        try {

            int numberofEmi = Integer.parseInt(edtnoOfinstallment.getText().toString());

            Calendar calendar = Calendar.getInstance();
            int y = calendar.get(Calendar.YEAR);
            int m = calendar.get(Calendar.MONTH);
            String strt_date = date + "-" + m + "-" + y;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date startdate = sdf.parse(strt_date);

            calendar.setTime(startdate);

            for (int i=0;i<numberofEmi;i++)
            {
                calendar.add(Calendar.MONTH,1);

            }

            String clo=sdf.format(calendar.getTime());

            txtclosingdate.setText(clo);
            closingdate=clo;








        }catch (Exception e)
        {

        }

    }


    public void showDatePicker()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(AddinvestActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;

                closingdate=i2+"-"+m+"-"+i;

                txtclosingdate.setText(i2+"-"+m+"-"+i);


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }



    public void addTaskstoReminder(String data)
    {

        try{
        Log.e("TAGG",data);


        String subject="";


        String id=((CommonData)spinvestname.getSelectedItem()).getId();


        List<CommonData> cms=new DatabaseHelper(AddinvestActivity.this).getDataByID(id,Utils.DBtables.TABLE_ACCOUNTSETTINGS);

        if(cms.size()>0)
        {
            //subject=


            JSONObject jsonObject=new JSONObject(cms.get(0).getData());


            subject=jsonObject.getString("Accountname");

        }






            JSONObject jsonObject=new JSONObject(data);
            String type=jsonObject.getString("type");

            String closing_date=jsonObject.getString("closing_date");

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date clo_date = sdf.parse(closing_date);
//
            if(type.equalsIgnoreCase("Monthly"))
            {


                int monthly_date=Integer.parseInt(jsonObject.getString("monthly_date"));
                int  numberOfinstallment=Integer.parseInt(jsonObject.getString("numberOfinstallment"));

                Calendar calendar1=Calendar.getInstance();

                int curr_year=calendar1.get(Calendar.YEAR);
                int m=calendar1.get(Calendar.MONTH)+1;

                calendar1.set(Calendar.YEAR,curr_year);
                calendar1.set(Calendar.MONTH,m);
                calendar1.set(Calendar.DAY_OF_MONTH,monthly_date);

                //Date month_date=sdf.parse(monthly_date+"-"+m+"-"+curr_year);

               // calendar1.setTime(month_date);


                for (int i=0;i<numberOfinstallment;i++)
                {
                    calendar1.add(Calendar.MONTH, 1);

                    if(calendar1.getTime().before(clo_date))
                    {
                        String date=  sdf.format(calendar1.getTime());
                        JSONObject js = new JSONObject();
                        js.put("name", " "+subject);
                        js.put("date", date);
                        js.put("time", "");
                        js.put("status", 0);
                    long rowid=    new DatabaseHelper(AddinvestActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());
                        Utils.playSimpleTone(AddinvestActivity.this);

                    taskid.add(rowid+"");




                        try {

                            Intent myIntent = new Intent(getApplicationContext(), DateTimeReceiver.class);
                            myIntent.putExtra("Task", subject);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, 0);

                            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                            alarmManager.set(AlarmManager.RTC, calendar1.getTimeInMillis(), pendingIntent);

                        }catch (Exception e)

                        {

                        }






                    }



                }



            }
            else {
                String closingdate=jsonObject.getString("closing_date");

                JSONObject js = new JSONObject();
                js.put("name", ""+subject);
                js.put("date", closingdate);
                js.put("time", "");
                js.put("status", 0);


              long rowid=  new DatabaseHelper(AddinvestActivity.this).addData(Utils.DBtables.TABLE_TASK,js.toString());
                Utils.playSimpleTone(AddinvestActivity.this);
                taskid.add(rowid+"");

            }








        }catch (Exception e)
        {


        }




    }
}
