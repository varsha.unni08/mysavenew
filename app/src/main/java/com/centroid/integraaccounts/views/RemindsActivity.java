package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.TaskDataAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class RemindsActivity extends AppCompatActivity {

    ImageView imgback,imgDatepick,imgEndDatepick;

    TextView txtdatepick,txtenddatepick;

    RecyclerView recycler;

    Button btnSearch;

    String date = "", month_selected = "", yearselected = "",enddate="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminds);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        imgDatepick=findViewById(R.id.imgDatepick);
        recycler=findViewById(R.id.recycler);

        txtdatepick=findViewById(R.id.txtdatepick);

        imgEndDatepick=findViewById(R.id.imgEndDatepick);
        txtenddatepick=findViewById(R.id.txtenddatepick);
        btnSearch=findViewById(R.id.btnSearch);




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtdatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);
            }
        });


        imgDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(0);
            }
        });


        txtenddatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);
            }
        });


        imgEndDatepick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);
            }
        });

       // getTaskData();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!date.equalsIgnoreCase(""))
                {

                    if(!enddate.equalsIgnoreCase(""))
                    {



                        getTaskData();
                    }
                    else {

                        Utils.showAlertWithSingle(RemindsActivity.this,"Select end date",null);
                    }
                }
                else {

                    Utils.showAlertWithSingle(RemindsActivity.this,"Select start date",null);
                }

            }
        });

        setDate();
    }

    public void setDate()
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        month_selected = m + "";
        yearselected = year + "";

        String m1="";

        if(m/10<1)
        {
            m1="0"+m;
        }
        else {
            m1=""+m;
        }

        String day="";

        if(dayOfMonth/10<1)
        {
            day="0"+dayOfMonth;
        }
        else {

            day=dayOfMonth+"";
        }

        date = day + "-" + m1 + "-" + year;

        enddate=day + "-" + m1 + "-" + year;

        txtdatepick.setText(date);
        txtenddatepick.setText(enddate);

        getTaskData();
    }

    public void showDatePicker(int code) {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(RemindsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m = i1 + 1;
                month_selected = m + "";
                yearselected = i + "";

                String month="";
                if(m/10<1)
                {
                    month="0"+m;
                }
                else {
                    month=""+m;
                }

                String day="";

                if(i2/10<1)
                {
                    day="0"+i2;
                }
                else {

                    day=i2+"";
                }


                if(code==0) {

                    date = day + "-" + month + "-" + i;

                    txtdatepick.setText(day + "-" + month + "-" + i);
                }
                else {
                    enddate = day + "-" + month + "-" + i;

                    txtenddatepick.setText(day + "-" + month + "-" + i);

                }

               // getTaskData();


            }
        }, year, month, dayOfMonth);

        datePickerDialog.show();
    }



    public void getTaskData()
    {

        List<CommonData> taskData=new DatabaseHelper(RemindsActivity.this).getData(Utils.DBtables.TABLE_TASK);

        Collections.reverse(taskData);

        List<CommonData>commonData_selected=new ArrayList<>();

        try {





            for (CommonData cm:taskData) {

                JSONObject jsonObject = new JSONObject(cm.getData());

                //String name = jsonObject.getString("name");
               // String date1 = jsonObject.getString("date");

//                if(!date.equalsIgnoreCase("")) {
//                    if (date.equalsIgnoreCase(date1)) {
//
//                        commonData_selected.add(cm);
//                    }
//                }else {
                    try {

                        JSONObject jso=new JSONObject(cm.getData());


                        String date11=jso.getString("date");


                        String status = jsonObject.getString("status");

                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date currentDate = sdf.parse(date);
                        Date end_date=sdf.parse(enddate);
                        Date cmdate=sdf.parse(date11);
                        if(status.equalsIgnoreCase("0")) {

                            if(end_date.after(currentDate)||end_date.equals(currentDate)) {
//                                if (cmdate.after(currentDate)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }
//                               else if (cmdate.equals(currentDate)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }
//
//                             else   if (cmdate.before(end_date)) {
//                                    commonData_selected.add(cm);
//                                }
//
//                             else   if (cmdate.equals(end_date)) {
//
//
//                                    commonData_selected.add(cm);
//
//
//                                }

                                if(cmdate.equals(currentDate)&&cmdate.before(end_date)) {

                                    commonData_selected.add(cm);

                                }
                                else  if(cmdate.after(currentDate) && cmdate.equals(end_date))
                                {
                                    commonData_selected.add(cm);
                                }
                                else if(cmdate.equals(currentDate) && cmdate.equals(end_date))
                                {

                                    commonData_selected.add(cm);
                                }
                                else if (cmdate.after(currentDate) && cmdate.before(end_date)) {

                                    commonData_selected.add(cm);
                                }
                            }
                            else {

                                Utils.showAlertWithSingle(RemindsActivity.this,"Select the date properly",null);
                            }

                        }


                        }catch (Exception e)
                    {

                    }

                   // commonData_selected.add(cm);
              //  }



            }

            Collections.sort(commonData_selected, new Comparator<CommonData>() {
                @Override
                public int compare(CommonData commonData, CommonData t1) {

                    Date datecommonData=null,dateT1=null;
                    try {

                        JSONObject jso = new JSONObject(commonData.getData());

                        JSONObject jso1 = new JSONObject(t1.getData());


                        String date = jso.getString("date");

                        String date1=jso1.getString("date");


                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                         datecommonData = sdf.parse(date);
                        dateT1 = sdf.parse(date1);






                    }catch (Exception e)
                    {

                    }



                    return datecommonData.compareTo(dateT1);
                }
            });






            TaskDataAdapter taskDataAdapter = new TaskDataAdapter(RemindsActivity.this, commonData_selected);
            recycler.setLayoutManager(new LinearLayoutManager(RemindsActivity.this));
            recycler.setAdapter(taskDataAdapter);

        }catch (Exception e)
        {

        }

        //Log.e("Tasksize",taskData.size()+"");




    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getTaskData();
    }
}
