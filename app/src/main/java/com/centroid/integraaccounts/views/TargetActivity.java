package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.TargetListAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class TargetActivity extends AppCompatActivity {

    FloatingActionButton fab_addtarget;
    RecyclerView recycler;

    TextView txtNodata;

    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        fab_addtarget=findViewById(R.id.fab_addtarget);
        recycler=findViewById(R.id.recycler);
        txtNodata=findViewById(R.id.txtNodata);





        fab_addtarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent5=new Intent(TargetActivity.this,NewTargetActivity.class);
                startActivity(intent5);
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getTargets();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getTargets();
    }


    public void getTargets()
    {
        List<CommonData>cmd=new DatabaseHelper(TargetActivity.this).getData(Utils.DBtables.TABLE_TARGET);

        if(cmd.size()>0)
        {

            recycler.setVisibility(View.VISIBLE);
            txtNodata.setVisibility(View.GONE);
            recycler.setLayoutManager(new LinearLayoutManager(TargetActivity.this));
            recycler.setAdapter(new TargetListAdapter(TargetActivity.this,cmd));


        }
        else{

            recycler.setVisibility(View.GONE);
            txtNodata.setVisibility(View.VISIBLE);

        }

    }
}