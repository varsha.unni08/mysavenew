package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class ForgotPasswordActivity extends AppCompatActivity {


    TextView txtphone;


    EditText edtPassword;



    EditText edtRePassword;


    TextView btn_login;

    String phone="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().hide();

        txtphone=findViewById(R.id.txtphone);
        edtPassword=findViewById(R.id.edtPassword);
        edtRePassword=findViewById(R.id.edtRePassword);
        btn_login=findViewById(R.id.btn_login);

        phone=getIntent().getStringExtra("phone");

        txtphone.setText(phone);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(edtPassword.getText().toString().length()>=8)
                {


                    if(edtPassword.getText().toString().trim().equals(edtRePassword.getText().toString().trim()))
                    {


                        updatepassword(phone,edtPassword.getText().toString());


                    }
                    else {

                      //  Toast.makeText(ForgotPasswordActivity.this,"your password must have atleast 8 characters",Toast.LENGTH_SHORT).show();

                        Utils.showAlertWithSingle(ForgotPasswordActivity.this,"your password must have atleast 8 characters", new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }


                }
                else {

                  //  Toast.makeText(ForgotPasswordActivity.this,"your password must have atleast 8 characters",Toast.LENGTH_SHORT).show();


                    Utils.showAlertWithSingle(ForgotPasswordActivity.this,"your password must have atleast 8 characters", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });

                }

            }
        });

    }


    public void updatepassword(final String phone, final String password)
    {

        //@Field("mobile") String mobile,@Field("password") String password)


        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();
        params.put("mobile",phone);
        params.put("password",password);
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(ForgotPasswordActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                if(data!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {

                            String base64=Utils.getBase64Password(password);

                            new PreferenceHelper(ForgotPasswordActivity.this).putData(Utils.firstpassword,base64);

                          //  Toast.makeText(ForgotPasswordActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(ForgotPasswordActivity.this,jsonObject.getString("message"), new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });





                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();



                        }
                        else {

                          //  Toast.makeText(ForgotPasswordActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                            Utils.showAlertWithSingle(ForgotPasswordActivity.this,jsonObject.getString("message"), new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });


                        }




                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.changePassword, Request.Method.POST).submitRequest();






//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"kk");
//
//
//
//
//        Call<JsonObject> callback = client.changePassword(phone,password);
//        callback.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//
//                if(response.body()!=null)
//                {
//
//                    try{
//
//                        JSONObject jsonObject=new JSONObject(response.body().toString());
//
//                        if(jsonObject.getInt("status")==1)
//                        {
//
//                            Toast.makeText(ForgotPasswordActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
//
//                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(ForgotPasswordActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
//
//
//                        }
//
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//
//            }
//        });




    }
}
