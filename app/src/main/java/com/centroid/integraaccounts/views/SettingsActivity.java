package com.centroid.integraaccounts.views;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.GDConfigs.ServiceListener;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.SettingsAdapter;
import com.centroid.integraaccounts.app.DirectExecutor;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.BackFileStatus;
import com.centroid.integraaccounts.data.domain.BackupFileListData;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.ExecutionOptions;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.events.ChangeListener;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.drive.Drive.SCOPE_APPFOLDER;

public class SettingsActivity extends AppCompatActivity {

    RecyclerView recycler;

    ImageView img;
    TextView txtTitle;
     String firstpin="",secondpin="";

     boolean isfirstpinsubmitted=false;

     String fileid="",email="";

     int uploaded_data=11;

     Handler handler;

     String uploadgdrivedata="";
    String result = null;
     public static int REQUEST_CODE_SIGN_IN=11;

      ServiceListener serviceListener = null ;//1
    DriveClient driveClient = null; //2
    DriveResourceClient driveResourceClient = null ;//3
    GoogleSignInAccount signInAccount = null ;

    File fileuploadToDrive=null;

    GoogleSignInClient googleSignInClient=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().hide();
        handler=new Handler();
        recycler=findViewById(R.id.recycler);
        img=findViewById(R.id.img);
        txtTitle=findViewById(R.id.txtTitle);


        String languagedata = LocaleHelper.getPersistedData(SettingsActivity.this, "en");
        Context context= LocaleHelper.setLocale(SettingsActivity.this, languagedata);

        Resources resources=context.getResources();


        txtTitle.setText(resources.getString(R.string.settings));

        recycler.setLayoutManager(new LinearLayoutManager(SettingsActivity.this));
        recycler.setAdapter(new SettingsAdapter(SettingsActivity.this));


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if( googleSignInClient!=null)
        {
             googleSignInClient.signOut();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        String languagedata = LocaleHelper.getPersistedData(SettingsActivity.this, "en");
        Context context= LocaleHelper.setLocale(SettingsActivity.this, languagedata);

        Resources resources=context.getResources();


        txtTitle.setText(resources.getString(R.string.settings));

        recycler.setLayoutManager(new LinearLayoutManager(SettingsActivity.this));
        recycler.setAdapter(new SettingsAdapter(SettingsActivity.this));

    }

    public void requestWritePermission(String permission)
    {
        ActivityCompat.requestPermissions(SettingsActivity.this,new String[]{permission},111);
    }


    public void uploadDataToServer()
    {
        try {

            List<Accounts> accounts = new DatabaseHelper(SettingsActivity.this).getAllAccounts();

            JSONArray jsonArray = new JSONArray();

            JSONArray jsonArray_account=new JSONArray();
            JSONArray jsonArray_diarysub=new JSONArray();
            JSONArray jsonArray_diary=new JSONArray();

            JSONArray jsonArray_budget=new JSONArray();
            JSONArray jsonArray_investment=new JSONArray();
            JSONArray jsonArray_task=new JSONArray();
            JSONArray jsonArray_accsettings=new JSONArray();
            JSONArray jsonArray_insurance=new JSONArray();
            JSONArray jsonArray_liability=new JSONArray();
            JSONArray jsonArray_asset=new JSONArray();
            JSONArray jsonArray_accountsrecipt=new JSONArray();
            JSONArray jsonArray_apppin=new JSONArray();
            JSONArray jsonArray_wallet=new JSONArray();
            JSONArray jsonArray_document=new JSONArray();
            JSONArray jsonArray_password=new JSONArray();
            JSONArray jsonArray_visitcard=new JSONArray();
            JSONArray jsonArray_target=new JSONArray();
            JSONArray jsonArray_milestone=new JSONArray();
            JSONArray jsonArray_targetcategory=new JSONArray();
            JSONArray jsonArray_visitcardimage=new JSONArray();
            JSONArray jsonArray_weblinks=new JSONArray();
            JSONArray jsonArray_emergencycontacts=new JSONArray();




            for (int i = 0; i < accounts.size(); i++) {

                String jsonObject=new Gson().toJson(accounts.get(i));
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_account.put(i,jsonObject1);

            }

            jsonArray.put(0,jsonArray_account.toString());

            List<CommonData>commonData_diarysubject=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.DIARYSUBJECT_table);
            for (int i = 0; i < commonData_diarysubject.size(); i++) {

                String jsonObject=commonData_diarysubject.get(i).getData();

                JSONObject jsonObject1=new JSONObject();
                jsonObject1.put("subject",jsonObject);




//                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_diarysub.put(i,jsonObject1);

            }
            jsonArray.put(1,jsonArray_diarysub.toString());

            List<CommonData>commonData_diary=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.DIARY_table);
            for (int i = 0; i < commonData_diary.size(); i++) {

                String jsonObject=commonData_diary.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_diary.put(i,jsonObject1);

            }
            jsonArray.put(2,jsonArray_diary.toString());

            List<CommonData>commonData_budget=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_BUDGET);
            for (int i = 0; i < commonData_budget.size(); i++) {

                String jsonObject=commonData_budget.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_budget.put(i,jsonObject1);

            }
            jsonArray.put(3,jsonArray_budget.toString());

            List<CommonData>commonData_invest=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.INVESTMENT_table);
            for (int i = 0; i < commonData_invest.size(); i++) {

                String jsonObject=commonData_invest.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_investment.put(i,jsonObject1);

            }
            jsonArray.put(4,jsonArray_investment.toString());

            List<CommonData>commonData_task=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_TASK);
            for (int i = 0; i < commonData_task.size(); i++) {

                String jsonObject=commonData_task.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_task.put(i,jsonObject1);

            }
            jsonArray.put(5,jsonArray_task.toString());


            List<CommonData>commonData_acc=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);
            for (int i = 0; i < commonData_acc.size(); i++) {

                String jsonObject=commonData_acc.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_accsettings.put(i,jsonObject1);

            }
            jsonArray.put(6,jsonArray_accsettings.toString());

            List<CommonData>commonData_insu=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_INSURANCE);
            for (int i = 0; i < commonData_insu.size(); i++) {

                String jsonObject=commonData_insu.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_insurance.put(i,jsonObject1);

            }
            jsonArray.put(7,jsonArray_insurance.toString());

            List<CommonData>commonData_iliab=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_LIABILITY);
            for (int i = 0; i < commonData_iliab.size(); i++) {

                String jsonObject=commonData_iliab.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_liability.put(i,jsonObject1);

            }
            jsonArray.put(8,jsonArray_liability.toString());


            List<CommonData>commonData_asset=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ASSET);
            for (int i = 0; i < commonData_asset.size(); i++) {

                String jsonObject=commonData_asset.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_asset.put(i,jsonObject1);

            }
            jsonArray.put(9,jsonArray_asset.toString());



            List<CommonData>commonData_accrecipt=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT);
            for (int i = 0; i < commonData_accrecipt.size(); i++) {

                String jsonObject=commonData_accrecipt.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_accountsrecipt.put(i,jsonObject1);

            }
            jsonArray.put(10,jsonArray_accountsrecipt.toString());


            List<CommonData>commonData_appin=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_APP_PIN);
            for (int i = 0; i < commonData_appin.size(); i++) {

                String jsonObject=commonData_appin.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_apppin.put(i,jsonObject1);

            }
            jsonArray.put(11,jsonArray_apppin.toString());


            List<CommonData>commonData_wallet=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_WALLET);
            for (int i = 0; i < commonData_wallet.size(); i++) {

                String jsonObject=commonData_wallet.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_wallet.put(i,jsonObject1);

            }
            jsonArray.put(12,jsonArray_wallet.toString());


            List<CommonData>commonData_doc=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_DOCUMENT);
            for (int i = 0; i < commonData_doc.size(); i++) {

                String jsonObject=commonData_doc.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_document.put(i,jsonObject1);

            }
            jsonArray.put(13,jsonArray_document.toString());


            List<CommonData>commonData_pass=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_PASSWORD);
            for (int i = 0; i < commonData_pass.size(); i++) {

                String jsonObject=commonData_pass.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_password.put(i,jsonObject1);

            }
            jsonArray.put(14,jsonArray_password.toString());


            List<CommonData>commonData_visit=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_VISITCARD);
            for (int i = 0; i < commonData_visit.size(); i++) {

                String jsonObject=commonData_visit.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_visitcard.put(i,jsonObject1);

            }
            jsonArray.put(15,jsonArray_visitcard.toString());


            List<CommonData>commonData_target=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_TARGET);
            for (int i = 0; i < commonData_target.size(); i++) {

                String jsonObject=commonData_target.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_target.put(i,jsonObject1);

            }
            jsonArray.put(16,jsonArray_target.toString());


            List<CommonData>commonData_milestone=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);
            for (int i = 0; i < commonData_milestone.size(); i++) {

                String jsonObject=commonData_milestone.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_milestone.put(i,jsonObject1);

            }
            jsonArray.put(17,jsonArray_milestone.toString());


            List<TargetCategory>commonData_targetcategory=new DatabaseHelper(SettingsActivity.this).getTargetCategory();
            for (int i = 0; i < commonData_targetcategory.size(); i++) {

                byte dataimage[]=commonData_targetcategory.get(i).getImage();
                //String  encImage = Base64.encodeToString(dataimage, Base64.DEFAULT);


                String id=commonData_targetcategory.get(i).getId();
                String name=commonData_targetcategory.get(i).getTask_category();

                JSONObject jsonObject=new JSONObject();
                jsonObject.put("name",name);
                jsonObject.put("id",id);
              //  jsonObject.put("image",encImage);

                jsonArray_targetcategory.put(i,jsonObject);

            }
            jsonArray.put(18,jsonArray_targetcategory.toString());


            List<CommonData>commonData_visitcardimage=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_VISITCARD_IMAGE);
            for (int i = 0; i < commonData_visitcardimage.size(); i++) {

                String jsonObject=commonData_visitcardimage.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_visitcardimage.put(i,jsonObject1);

            }
            jsonArray.put(19,jsonArray_visitcardimage.toString());

            List<CommonData>commonData_weblinks=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_WEBLINKS);
            for (int i = 0; i < commonData_weblinks.size(); i++) {

                String jsonObject=commonData_weblinks.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_weblinks.put(i,jsonObject1);

            }
            jsonArray.put(20,jsonArray_weblinks.toString());



            List<CommonData>commonData_emergency=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_EMERGENCY);
            for (int i = 0; i < commonData_emergency.size(); i++) {

                String jsonObject=commonData_emergency.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_emergencycontacts.put(i,jsonObject1);

            }
            jsonArray.put(21,jsonArray_emergencycontacts.toString());






            uploadJson(jsonArray.toString());



        }catch (Exception e)
        {

            e.printStackTrace();
        }




    }


    public void uploadFileToDrive()
    {
        try{

            fileid="";


            final ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"fkjfk");


            Map<String,String> params=new HashMap<>();
            params.put("timestamp",Utils.getTimestamp());
            // params.put("device_id",token);

            new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    //progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                    Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                    progressFragment.dismiss();
                    if(profiledata!=null)
                    {

                        try{



                            if(profiledata.getStatus()==1)
                            {


                                if(profiledata.getData()!=null)
                                {








                                }





                            }
                            else {

                             //   Toast.makeText(SettingsActivity.this," failed",Toast.LENGTH_SHORT).show();

                                Utils.showAlertWithSingle(SettingsActivity.this," failed", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });

                            }



                        }catch (Exception e)
                        {
e.printStackTrace();


//                            Utils.showAlertWithSingle(SettingsActivity.this,e.toString()+","+e.getLocalizedMessage(), new DialogEventListener() {
//                                @Override
//                                public void onPositiveButtonClicked() {
//
//                                }
//
//                                @Override
//                                public void onNegativeButtonClicked() {
//
//                                }
//                            });

                        }



                    }
                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                  //  Toast.makeText(SettingsActivity.this,err,Toast.LENGTH_SHORT).show();
                    Utils.showAlertWithSingle(SettingsActivity.this,err, new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }
            },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();































        }catch (Exception e)
        {
            Log.e("Exception",e.toString());
        }
    }




  public  void prepareDataToUpload()
    {
//        String id=profiledata.getData().getId();
//
//        if(profiledata.getData().getGdrive_fileid()!=null) {
//
//            fileid = profiledata.getData().getGdrive_fileid();
//        }

        try {

            List<Accounts> accounts = new DatabaseHelper(SettingsActivity.this).getAllAccounts();

            JSONArray jsonArray = new JSONArray();

            JSONArray jsonArray_account=new JSONArray();
            JSONArray jsonArray_diarysub=new JSONArray();
            JSONArray jsonArray_diary=new JSONArray();

            JSONArray jsonArray_budget=new JSONArray();
            JSONArray jsonArray_investment=new JSONArray();
            JSONArray jsonArray_task=new JSONArray();
            JSONArray jsonArray_accsettings=new JSONArray();
            JSONArray jsonArray_insurance=new JSONArray();
            JSONArray jsonArray_liability=new JSONArray();
            JSONArray jsonArray_asset=new JSONArray();
            JSONArray jsonArray_accountsrecipt=new JSONArray();
            JSONArray jsonArray_apppin=new JSONArray();
            JSONArray jsonArray_wallet=new JSONArray();
            JSONArray jsonArray_document=new JSONArray();
            JSONArray jsonArray_password=new JSONArray();
            JSONArray jsonArray_visitcard=new JSONArray();
            JSONArray jsonArray_target=new JSONArray();
            JSONArray jsonArray_milestone=new JSONArray();
            JSONArray jsonArray_targetcategory=new JSONArray();
            JSONArray jsonArray_visitcardimage=new JSONArray();
            JSONArray jsonArray_weblinks=new JSONArray();
            JSONArray jsonArray_emergencycontacts=new JSONArray();




            for (int i = 0; i < accounts.size(); i++) {

                String jsonObject=new Gson().toJson(accounts.get(i));
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_account.put(i,jsonObject1);

            }

            jsonArray.put(0,jsonArray_account.toString());

            List<CommonData>commonData_diarysubject=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.DIARYSUBJECT_table);
            for (int i = 0; i < commonData_diarysubject.size(); i++) {

                String jsonObject=commonData_diarysubject.get(i).getData();

                JSONObject jsonObject1=new JSONObject();
                jsonObject1.put("subject",jsonObject);




//                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_diarysub.put(i,jsonObject1);

            }
            jsonArray.put(1,jsonArray_diarysub.toString());

            List<CommonData>commonData_diary=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.DIARY_table);
            for (int i = 0; i < commonData_diary.size(); i++) {

                String jsonObject=commonData_diary.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_diary.put(i,jsonObject1);

            }
            jsonArray.put(2,jsonArray_diary.toString());

            List<CommonData>commonData_budget=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_BUDGET);
            for (int i = 0; i < commonData_budget.size(); i++) {

                String jsonObject=commonData_budget.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_budget.put(i,jsonObject1);

            }
            jsonArray.put(3,jsonArray_budget.toString());

            List<CommonData>commonData_invest=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.INVESTMENT_table);
            for (int i = 0; i < commonData_invest.size(); i++) {

                String jsonObject=commonData_invest.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_investment.put(i,jsonObject1);

            }
            jsonArray.put(4,jsonArray_investment.toString());

            List<CommonData>commonData_task=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_TASK);
            for (int i = 0; i < commonData_task.size(); i++) {

                String jsonObject=commonData_task.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_task.put(i,jsonObject1);

            }
            jsonArray.put(5,jsonArray_task.toString());


            List<CommonData>commonData_acc=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);
            for (int i = 0; i < commonData_acc.size(); i++) {

                String jsonObject=commonData_acc.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_accsettings.put(i,jsonObject1);

            }
            jsonArray.put(6,jsonArray_accsettings.toString());

            List<CommonData>commonData_insu=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_INSURANCE);
            for (int i = 0; i < commonData_insu.size(); i++) {

                String jsonObject=commonData_insu.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_insurance.put(i,jsonObject1);

            }
            jsonArray.put(7,jsonArray_insurance.toString());

            List<CommonData>commonData_iliab=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_LIABILITY);
            for (int i = 0; i < commonData_iliab.size(); i++) {

                String jsonObject=commonData_iliab.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_liability.put(i,jsonObject1);

            }
            jsonArray.put(8,jsonArray_liability.toString());


            List<CommonData>commonData_asset=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ASSET);
            for (int i = 0; i < commonData_asset.size(); i++) {

                String jsonObject=commonData_asset.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_asset.put(i,jsonObject1);

            }
            jsonArray.put(9,jsonArray_asset.toString());



            List<CommonData>commonData_accrecipt=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT);
            for (int i = 0; i < commonData_accrecipt.size(); i++) {

                String jsonObject=commonData_accrecipt.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_accountsrecipt.put(i,jsonObject1);

            }
            jsonArray.put(10,jsonArray_accountsrecipt.toString());


            List<CommonData>commonData_appin=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_APP_PIN);
            for (int i = 0; i < commonData_appin.size(); i++) {

                String jsonObject=commonData_appin.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_apppin.put(i,jsonObject1);

            }
            jsonArray.put(11,jsonArray_apppin.toString());


            List<CommonData>commonData_wallet=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_WALLET);
            for (int i = 0; i < commonData_wallet.size(); i++) {

                String jsonObject=commonData_wallet.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_wallet.put(i,jsonObject1);

            }
            jsonArray.put(12,jsonArray_wallet.toString());


            List<CommonData>commonData_doc=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_DOCUMENT);
            for (int i = 0; i < commonData_doc.size(); i++) {

                String jsonObject=commonData_doc.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_document.put(i,jsonObject1);

            }
            jsonArray.put(13,jsonArray_document.toString());


            List<CommonData>commonData_pass=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_PASSWORD);
            for (int i = 0; i < commonData_pass.size(); i++) {

                String jsonObject=commonData_pass.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_password.put(i,jsonObject1);

            }
            jsonArray.put(14,jsonArray_password.toString());


            List<CommonData>commonData_visit=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_VISITCARD);
            for (int i = 0; i < commonData_visit.size(); i++) {

                String jsonObject=commonData_visit.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_visitcard.put(i,jsonObject1);

            }
            jsonArray.put(15,jsonArray_visitcard.toString());


            List<CommonData>commonData_target=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_TARGET);
            for (int i = 0; i < commonData_target.size(); i++) {

                String jsonObject=commonData_target.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_target.put(i,jsonObject1);

            }
            jsonArray.put(16,jsonArray_target.toString());


            List<CommonData>commonData_milestone=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_MILESTONE);
            for (int i = 0; i < commonData_milestone.size(); i++) {

                String jsonObject=commonData_milestone.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_milestone.put(i,jsonObject1);

            }
            jsonArray.put(17,jsonArray_milestone.toString());


            List<TargetCategory>commonData_targetcategory=new DatabaseHelper(SettingsActivity.this).getTargetCategory();
            for (int i = 0; i < commonData_targetcategory.size(); i++) {

                byte dataimage[]=commonData_targetcategory.get(i).getImage();
                //String  encImage = Base64.encodeToString(dataimage, Base64.DEFAULT);


                String id=commonData_targetcategory.get(i).getId();
                String name=commonData_targetcategory.get(i).getTask_category();

                JSONObject jsonObject=new JSONObject();
                jsonObject.put("name",name);
                jsonObject.put("id",id);
                //  jsonObject.put("image",encImage);

                jsonArray_targetcategory.put(i,jsonObject);

            }
            jsonArray.put(18,jsonArray_targetcategory.toString());


            List<CommonData>commonData_visitcardimage=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_VISITCARD_IMAGE);
            for (int i = 0; i < commonData_visitcardimage.size(); i++) {

                String jsonObject=commonData_visitcardimage.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_visitcardimage.put(i,jsonObject1);

            }
            jsonArray.put(19,jsonArray_visitcardimage.toString());

            List<CommonData>commonData_weblinks=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_WEBLINKS);
            for (int i = 0; i < commonData_weblinks.size(); i++) {

                String jsonObject=commonData_weblinks.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_weblinks.put(i,jsonObject1);

            }
            jsonArray.put(20,jsonArray_weblinks.toString());



            List<CommonData>commonData_emergency=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_EMERGENCY);
            for (int i = 0; i < commonData_emergency.size(); i++) {

                String jsonObject=commonData_emergency.get(i).getData();
                JSONObject jsonObject1=new JSONObject(jsonObject);
                jsonArray_emergencycontacts.put(i,jsonObject1);

            }
            jsonArray.put(21,jsonArray_emergencycontacts.toString());



            uploadgdrivedata=jsonArray.toString();

         //   Toast.makeText(SettingsActivity.this,uploadgdrivedata,Toast.LENGTH_SHORT).show();


        //    uploadJson(jsonArray.toString());

            GoogleSignInOptions signInOptions =
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestEmail()
                            .requestScopes(Drive.SCOPE_FILE)
                            .build();
            googleSignInClient = GoogleSignIn.getClient(SettingsActivity.this, signInOptions);

            // The result of the sign-in Intent is handled in onActivityResult.
            //   startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);

            someActivityResultLauncher.launch(googleSignInClient.getSignInIntent());


        }catch (Exception e)
        {

            e.printStackTrace();
        }
    }


    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        handleSignInResult(data);
                    }
                }
            });



//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//       if(requestCode==REQUEST_CODE_SIGN_IN)
//        if ( data != null) {
//            handleSignInResult(data);
//        }
//
//
//
//    }


    private void handleSignInResult(Intent result) {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");





        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        signInAccount=googleSignInAccount;
                     String token=   googleSignInAccount.getId();
                    email= googleSignInAccount.getEmail();
                     Log.e("TAAG",token);

                        GoogleAccountCredential credential =
                                GoogleAccountCredential.usingOAuth2(
                                        SettingsActivity.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                        credential.setSelectedAccount(signInAccount.getAccount());
                        com.google.api.services.drive.Drive googleDriveService =
                                new com.google.api.services.drive.Drive.Builder(
                                        AndroidHttp.newCompatibleTransport(),
                                        new GsonFactory(),
                                        credential)
                                        .setApplicationName("Drive API Migration")
                                        .build();



                        if(!uploadgdrivedata.isEmpty()) {

                            Long tsLong = System.currentTimeMillis()/1000;
                            String ts = tsLong.toString();

                            Toast.makeText(SettingsActivity.this,"Going to upload",Toast.LENGTH_SHORT).show();

                           // Toast.makeText(SettingsActivity.this,ts,Toast.LENGTH_SHORT).show();
                            Executor mExecutor = Executors.newSingleThreadExecutor();

                                Tasks.call(mExecutor, () -> {
                                    com.google.api.services.drive.model.File metadata = new com.google.api.services.drive.model.File()
                                            .setParents(Collections.singletonList("root"))
                                            .setMimeType("application/json")
                                            .setName(ts+".json");

                                    com.google.api.services.drive.model.File googleFile = googleDriveService.files().create(metadata).execute();
                                    if (googleFile == null) {
                                        throw new IOException("Null result when requesting file creation.");
                                    }
                                    fileid = googleFile.getId();


                                    Tasks.call(mExecutor, () -> {

                                        progressFragment.dismiss();
                                        // Create a File containing any metadata changes.
                                        com.google.api.services.drive.model.File metadata1 = new com.google.api.services.drive.model.File().setName(ts+".json");

                                        // Convert content to an AbstractInputStreamContent instance.
                                        ByteArrayContent contentStream = ByteArrayContent.fromString("text/plain", uploadgdrivedata);
                                        googleDriveService.files().update(fileid, metadata1, contentStream).execute();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(SettingsActivity.this,"Data uploaded to google drive successfully",Toast.LENGTH_SHORT).show();
                                                updateGoogleDriveFileId(fileid,email);
                                            }
                                        });


                                        return null;
                                    });


                                    return googleFile.getId();

                                });

                           // }
                        }
                        else {

                            Executor mExecutor = Executors.newSingleThreadExecutor();

                             Tasks.call(mExecutor, () -> {
                                // Retrieve the metadata as a File object.
                                com.google.api.services.drive.model.File metadata = googleDriveService.files().get(fileid).execute();
                                String name = metadata.getName();

                                // Stream the file contents to a String.
                                try (InputStream is = googleDriveService.files().get(fileid).executeMediaAsInputStream();
                                     BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    String line;



                                    while ((line = reader.readLine()) != null) {
                                        stringBuilder.append(line);
                                    }
                                    String contents = stringBuilder.toString();

                                    try{
                                        progressFragment.dismiss();

                                        try{

                                            if(!contents.equalsIgnoreCase(""))
                                            {

                                                JSONArray jsonArray=new JSONArray(contents);

                                                // JSONArray jsonArray=new JSONArray(contents);

                                                JSONArray jsonArray_account=new JSONArray();
                                                JSONArray jsonArray_diarysub=new JSONArray();
                                                JSONArray jsonArray_diary=new JSONArray();

                                                JSONArray jsonArray_budget=new JSONArray();
                                                JSONArray jsonArray_investment=new JSONArray();
                                                JSONArray jsonArray_task=new JSONArray();
                                                JSONArray jsonArray_accsettings=new JSONArray();
                                                JSONArray jsonArray_insurance=new JSONArray();
                                                JSONArray jsonArray_liability=new JSONArray();
                                                JSONArray jsonArray_asset=new JSONArray();
                                                JSONArray jsonArray_accountsrecipt=new JSONArray();
                                                JSONArray jsonArray_apppin=new JSONArray();
                                                JSONArray jsonArray_wallet=new JSONArray();
                                                JSONArray jsonArray_document=new JSONArray();
                                                JSONArray jsonArray_password=new JSONArray();
                                                JSONArray jsonArray_visitcard=new JSONArray();


                                                JSONArray jsonArray_target=new JSONArray();
                                                JSONArray jsonArray_milestone=new JSONArray();
                                                JSONArray jsonArray_targetcategory=new JSONArray();
                                                JSONArray jsonArray_visitcardimage=new JSONArray();
                                                JSONArray jsonArray_weblinks=new JSONArray();
                                                JSONArray jsonArray_emergency=new JSONArray();

                                                for (int i=0;i<jsonArray.length();i++)
                                                {

                                                    if(i==0) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_account=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_account.length();k++) {

                                                            Accounts accounts = new GsonBuilder().create().fromJson(jsonArray_account.get(k).toString(), Accounts.class);


                                                            new DatabaseHelper(SettingsActivity.this).addAccountsData(accounts);
                                                        }
                                                    }

                                                    if(i==1) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_diarysub=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_diarysub.length();k++) {

                                                          //  subject

                                                            JSONObject js1=new JSONObject(jsonArray_diarysub.get(k).toString());

                                                            String a=js1.getString("subject");


                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diarysub.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARYSUBJECT_table,a);
                                                        }
                                                    }

                                                    if(i==2) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_diary=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_diary.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diary.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARY_table,jsonArray_diary.get(k).toString());
                                                        }
                                                    }

                                                    if(i==3) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_budget=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_budget.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_budget.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_BUDGET,jsonArray_budget.get(k).toString());
                                                        }
                                                    }
                                                    if(i==4) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_investment=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_investment.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_investment.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.INVESTMENT_table,jsonArray_investment.get(k).toString());
                                                        }
                                                    }


                                                    if(i==5) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_task=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_task.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_task.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonArray_task.get(k).toString());
                                                        }
                                                    }


                                                    if(i==6) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_accsettings=new JSONArray(jsonObject);
                                                        new DatabaseHelper(SettingsActivity.this).dropAllData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);
                                                        for(int k=0;k<jsonArray_accsettings.length();k++) {

                                                            CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accsettings.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonArray_accsettings.get(k).toString());
                                                        }
                                                    }


                                                    if(i==7) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_insurance=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_insurance.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_insurance.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_INSURANCE,jsonArray_insurance.get(k).toString());
                                                        }
                                                    }


                                                    if(i==8) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_liability=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_liability.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_liability.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_LIABILITY,jsonArray_liability.get(k).toString());
                                                        }
                                                    }

                                                    if(i==9) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_asset=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_asset.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_asset.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ASSET,jsonArray_asset.get(k).toString());
                                                        }
                                                    }

                                                    if(i==10) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_accountsrecipt=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_accountsrecipt.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accountsrecipt.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT,jsonArray_accountsrecipt.get(k).toString());
                                                        }
                                                    }

                                                    if(i==11) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_apppin=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_apppin.length();k++) {

                                                            //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_apppin.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_APP_PIN,jsonArray_apppin.get(k).toString());
                                                        }
                                                    }

                                                    if(i==12) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_wallet=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_wallet.length();k++) {

                                                            //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_wallet.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_WALLET,jsonArray_wallet.get(k).toString());
                                                        }
                                                    }

                                                    if(i==13) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_document=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_document.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_document.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_DOCUMENT,jsonArray_document.get(k).toString());
                                                        }
                                                    }

                                                    if(i==14) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_password=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_password.length();k++) {

                                                            //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_password.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_PASSWORD,jsonArray_password.get(k).toString());
                                                        }
                                                    }

                                                    if(i==15) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_visitcard=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_visitcard.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_visitcard.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_VISITCARD,jsonArray_visitcard.get(k).toString());
                                                        }
                                                    }

                                                    if(i==16) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_target=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_target.length();k++) {

                                                          //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_target.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_TARGET,jsonArray_target.get(k).toString());
                                                        }
                                                    }


                                                    if(i==17) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_milestone=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_milestone.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_milestone.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_MILESTONE,jsonArray_milestone.get(k).toString());
                                                        }
                                                    }


                                                    if(i==18) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_targetcategory=new JSONArray(jsonObject);
                                                        new DatabaseHelper(SettingsActivity.this).dropAllDataFromTarget(Utils.DBtables.TABLE_TARGETCATEGORY);
                                                        for(int k=0;k<jsonArray_targetcategory.length();k++) {
                                                            byte[] bitMapData =null;

                                                            JSONObject jsonObject1=new JSONObject(jsonArray_targetcategory.get(k).toString());
                                                            String name1=jsonObject1.getString("name");
                                                            String id=jsonObject1.getString("id");
                                                            for (int i1 = 0; i1 < Utils.arr_target_iconimgs.length; i1++) {


                                                                if(i1==k) {

                                                                    try {


                                                                        Drawable drawable = ContextCompat.getDrawable(SettingsActivity.this, Utils.arr_target_iconimgs[i1]);
                                                                        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                                                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                                                        bitMapData = stream.toByteArray();

                                                                        //  new DatabaseHelper(MainActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,Utils.arr_target[i],bitMapData);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }

                                                                }


                                                            }
                                                            TargetCategory accounts = new TargetCategory();
                                                            accounts.setId(id);
                                                            if(bitMapData!=null) {
                                                                accounts.setImage(bitMapData);
                                                            }
                                                            else{
                                                                accounts.setImage(new byte[10]);
                                                            }
                                                            accounts.setTask_category(name1);
                                                            new DatabaseHelper(SettingsActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,accounts.getTask_category(),accounts.getImage());
                                                        }
                                                    }




                                                    if(i==19) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_visitcardimage=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_visitcardimage.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_visitcardimage.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_VISITCARD_IMAGE,jsonArray_visitcardimage.get(k).toString());
                                                        }
                                                    }


                                                    if(i==20) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_weblinks=new JSONArray(jsonObject);

                                                        for(int k=0;k<jsonArray_weblinks.length();k++) {

                                                            //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_apppin.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_WEBLINKS,jsonArray_weblinks.get(k).toString());
                                                        }
                                                    }

                                                    if(i==21) {
                                                        String jsonObject = jsonArray.getString(i);
                                                        jsonArray_emergency=new JSONArray(jsonObject);
                                                        new DatabaseHelper(SettingsActivity.this).dropAllDataofEmergency();
                                                        for(int k=0;k<jsonArray_emergency.length();k++) {

                                                           // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accsettings.get(k).toString(), CommonData.class);

                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_EMERGENCY,jsonArray_emergency.get(k).toString());
                                                        }
                                                    }

//                                                    if(i==20) {
//                                                        new DatabaseHelper(SettingsActivity.this).deleteAllData(Utils.DBtables.TABLE_TARGETCATEGORY);
//                                                        String jsonObject = jsonArray.getString(i);
//                                                        jsonArray_accountsettings=new JSONArray(jsonObject);
//
//                                                        for(int k=0;k<jsonArray_accountsettings.length();k++) {
//
//                                                            CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accountsettings.get(k).toString(), CommonData.class);
//
//                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,accounts.getData());
//                                                        }
//                                                    }



                                                }




                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(SettingsActivity.this," Data restored ",Toast.LENGTH_SHORT).show();

                                                    }
                                                });






                                            }




                                        }catch (Exception e)
                                        {

                                        }

//                                        if(!contents.equalsIgnoreCase(""))
//                                        {
//
//                                            JSONArray jsonArray=new JSONArray(contents);
//
//                                            JSONArray jsonArray_account=new JSONArray();
//                                            JSONArray jsonArray_diarysub=new JSONArray();
//                                            JSONArray jsonArray_diary=new JSONArray();
//
//                                            JSONArray jsonArray_budget=new JSONArray();
//                                            JSONArray jsonArray_investment=new JSONArray();
//                                            JSONArray jsonArray_task=new JSONArray();
//                                            JSONArray jsonArray_accsettings=new JSONArray();
//                                            JSONArray jsonArray_insurance=new JSONArray();
//                                            JSONArray jsonArray_liability=new JSONArray();
//                                            JSONArray jsonArray_asset=new JSONArray();
//                                            JSONArray jsonArray_accountsrecipt=new JSONArray();
//                                            JSONArray jsonArray_apppin=new JSONArray();
//                                            JSONArray jsonArray_wallet=new JSONArray();
//                                            JSONArray jsonArray_document=new JSONArray();
//                                            JSONArray jsonArray_password=new JSONArray();
//                                            JSONArray jsonArray_visitcard=new JSONArray();
//
//                                            for (int i=0;i<jsonArray.length();i++)
//                                            {
//
//                                                if(i==0) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_account=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_account.length();k++) {
//
//                                                        Accounts accounts = new GsonBuilder().create().fromJson(jsonArray_account.get(k).toString(), Accounts.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addAccountsData(accounts);
//                                                    }
//                                                }
//
//                                                if(i==1) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_diarysub=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_diarysub.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diarysub.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARYSUBJECT_table,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==2) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_diary=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_diary.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diary.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARY_table,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==3) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_budget=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_budget.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_budget.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_BUDGET,accounts.getData());
//                                                    }
//                                                }
//                                                if(i==4) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_investment=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_investment.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_investment.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.INVESTMENT_table,accounts.getData());
//                                                    }
//                                                }
//
//
//                                                if(i==5) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_task=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_task.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_task.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_TASK,accounts.getData());
//                                                    }
//                                                }
//
//
//                                                if(i==6) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_accsettings=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_accsettings.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accsettings.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,accounts.getData());
//                                                    }
//                                                }
//
//
//                                                if(i==7) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_insurance=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_insurance.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_insurance.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_INSURANCE,accounts.getData());
//                                                    }
//                                                }
//
//
//                                                if(i==8) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_liability=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_liability.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_liability.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_LIABILITY,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==9) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_asset=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_asset.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_asset.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ASSET,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==10) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_accountsrecipt=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_accountsrecipt.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accountsrecipt.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==11) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_apppin=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_apppin.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_apppin.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_APP_PIN,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==12) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_wallet=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_wallet.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_wallet.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_WALLET,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==13) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_document=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_document.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_document.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_DOCUMENT,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==14) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_password=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_password.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_password.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_PASSWORD,accounts.getData());
//                                                    }
//                                                }
//
//                                                if(i==15) {
//                                                    String jsonObject = jsonArray.getString(i);
//                                                    jsonArray_visitcard=new JSONArray(jsonObject);
//
//                                                    for(int k=0;k<jsonArray_visitcard.length();k++) {
//
//                                                        CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_visitcard.get(k).toString(), CommonData.class);
//
//                                                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_VISITCARD,accounts.getData());
//                                                    }
//                                                }
//
//
//
//                                            }
//
//
//
//
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    Toast.makeText(SettingsActivity.this," Data restored from Google drive",Toast.LENGTH_SHORT).show();
//
//                                                }
//                                            });
//
//
//
//                                        }




                                    }catch (Exception e)
                                    {
                                        Toast.makeText(SettingsActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
                                    }






                                  //  Toast.makeText(SettingsActivity.this," Data restored from Google drive",Toast.LENGTH_SHORT).show();



                                    return Pair.create(name, contents);
                                }
                            });




                        }


                        }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressFragment.dismiss();

                Utils.showAlertWithSingle(SettingsActivity.this,e.toString(),null);

                Log.e("TAAG",e.toString());

            }
        });
    }





    public void updateGoogleDriveFileId(String fileId,String email)
    {
        try {

//            Date currentTime = Calendar.getInstance().getTime();

            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm ss");

            String backupdate= sdf1.format(new Date());




            final ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"fkjfk");
            Map<String,String> params=new HashMap<>();
             params.put("gdrive_fileid",fileId);
            params.put("email",email);
            params.put("date",backupdate);
             params.put("timestamp",Utils.getTimestamp());
             new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {
                    progressFragment.dismiss();
                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();

                    try {
                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date date=new Date();
                            String dt= sdf.format(date);

                            new PreferenceHelper(SettingsActivity.this).putData(Utils.databackuptime,dt);

                            fileuploadToDrive=null;

                            uploadgdrivedata="";

                            Toast.makeText(SettingsActivity.this,"Data uploaded to google drive successfully",Toast.LENGTH_SHORT).show();
                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    Toast.makeText(SettingsActivity.this,err,Toast.LENGTH_SHORT).show();

                }
            },Utils.WebServiceMethodes.addGdriveBackupinfo, Request.Method.POST).submitRequest();
            Calendar c=Calendar.getInstance();
            int day=c.get(Calendar.DAY_OF_MONTH);
            int month=c.get(Calendar.MONTH);
            int year=c.get(Calendar.YEAR);
            int m=month+1;
            String mnth=m+"";
            if(m>10)
            {
                mnth=m+"";
            }
            else {

                mnth="0"+m;
            }
            String currentdate=day+"-"+mnth+"-"+year;
            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_BACKUP,currentdate);
            Utils.playSimpleTone(SettingsActivity.this);




            //            Utils.showAlertWithSingle(SettingsActivity.this, "Remember the Email ID Used for Backup and Restore", new DialogEventListener() {
//                @Override
//                public void onPositiveButtonClicked() {
//
//                }
//
//                @Override
//                public void onNegativeButtonClicked() {
//
//                }
//            });

        }catch (Exception e)
        {

        }
    }



    public void uploadJson(String filedata) {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        params.put("data",filedata);

        new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();



                 try{

                     JSONObject jsonObject=new JSONObject(data);

                     if(jsonObject.getInt("status")==1)
                     {
                         Toast.makeText(SettingsActivity.this,"Backup completed to server",Toast.LENGTH_SHORT).show();
                         Calendar c=Calendar.getInstance();
                         int day=c.get(Calendar.DAY_OF_MONTH);
                         int month=c.get(Calendar.MONTH);
                         int year=c.get(Calendar.YEAR);
                         int m=month+1;
                         String mnth=m+"";
                         if(m>10)
                         {
                             mnth=m+"";
                         }
                         else {

                             mnth="0"+m;
                         }
                         String currentdate=day+"-"+mnth+"-"+year;
                         new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_BACKUP,currentdate);

                     }




                 }catch (Exception e)
                 {

                 }



            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

                Utils.showAlertWithSingle(SettingsActivity.this, err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });


            }
        },Utils.WebServiceMethodes.uploadBackupFile, Request.Method.POST).submitRequest();






//        RequestBody fileReqBody = RequestBody.create(MediaType.parse("application/json"), file);
//
//        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

//        ExecutorService executor = Executors.newSingleThreadExecutor();
//
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
//                    String attachmentName = "file";
//                    String attachmentFileName = file.getName();
//                    String crlf = "\r\n";
//                    String twoHyphens = "--";
//                    String boundary =  "*****";
//
//                    HttpURLConnection httpUrlConnection = null;
//                    URL url = new URL(Utils.baseurl+Utils.WebServiceMethodes.uploadBackupFile);
//                    httpUrlConnection = (HttpURLConnection) url.openConnection();
//                    httpUrlConnection.setUseCaches(false);
//                    httpUrlConnection.setDoOutput(true);
//
//                    httpUrlConnection.setRequestMethod("POST");
//                    httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
//                    httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
//                    httpUrlConnection.setRequestProperty("Content-Type", "application/json; utf-8");
//                    httpUrlConnection.setRequestProperty("timestamp", Utils.getTimestamp());
//                    httpUrlConnection.setRequestProperty("Authorization", new PreferenceHelper(SettingsActivity.this).getData(Utils.userkey));
//                    httpUrlConnection.setRequestProperty(
//                            "Content-Type", "multipart/form-data;boundary=" + boundary);
//
//                    DataOutputStream request = new DataOutputStream(
//                            httpUrlConnection.getOutputStream());
//
//                    request.writeBytes(twoHyphens + boundary + crlf);
//                    request.writeBytes("Content-Disposition: form-data; name=\"" +
//                            attachmentName + "\";filename=\"" +
//                            attachmentFileName + "\"" + crlf);
//                    request.writeBytes(crlf);
//
//                    FileInputStream fis = null;
//                    try {
//                        fis = new FileInputStream(file);
//                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                        byte[] b = new byte[1024];
//                        for (int readNum; (readNum = fis.read(b)) != -1; ) {
//                            bos.write(b, 0, readNum);
//                        }
//                        //return bos.toByteArray();
//
//                        request.write(bos.toByteArray());
//                    } catch (Exception e) {
//                        Log.d("mylog", e.toString());
//                    }
//
//                    request.writeBytes(crlf);
//                    request.writeBytes(twoHyphens + boundary +
//                            twoHyphens + crlf);
//
//                    request.flush();
//                    request.close();
//
////                    InputStream responseStream = new
////                            BufferedInputStream(httpUrlConnection.getInputStream());
////
////                    BufferedReader responseStreamReader =
////                            new BufferedReader(new InputStreamReader(responseStream));
////
////                    String line = "";
////                    StringBuilder stringBuilder = new StringBuilder();
////
////                    while ((line = responseStreamReader.readLine()) != null) {
////                        stringBuilder.append(line).append("\n");
////                    }
////                    responseStreamReader.close();
//
////                    result = stringBuilder.toString();
////
////                    responseStream.close();
//
//                    httpUrlConnection.disconnect();
//
//
//
//
//
//                }catch (Exception e)
//                {
//
//                    e.printStackTrace();
//                }
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                      //  Toast.makeText(SettingsActivity.this,result,Toast.LENGTH_SHORT).show();
//
//                        try {
//
//                            JSONObject jsonObject = new JSONObject(result);
//
//                            if (jsonObject.getInt("status") == 1) {
//
//                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//                                Date date=new Date();
//                                String dt= sdf.format(date);
//
//                                new PreferenceHelper(SettingsActivity.this).putData(Utils.databackuptime,dt);
//
//                                Toast.makeText(SettingsActivity.this,"Data uploaded to server successfully",Toast.LENGTH_SHORT).show();
//
//
//                            } else {
//
//                                Toast.makeText(SettingsActivity.this,"failed",Toast.LENGTH_SHORT).show();
//
//                            }
//
//                        }catch ( Exception e)
//                        {
//
//                        }
//
//                        progressFragment.dismiss();
//                    }
//                });
//
//
//            }
//        });




//
//
//        Call<JsonObject> jsonArrayCall=client.uploadBackupFile(new PreferenceHelper(SettingsActivity.this).getData(Utils.userkey),part);
//
//        jsonArrayCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//
//                progressFragment.dismiss();
//
//                if(response.body()!=null)
//                {
//
//                    try {
//
//                        JSONObject jsonObject = new JSONObject(response.body().toString());
//
//                        if (jsonObject.getInt("status") == 1) {
//
//
//
//                            Toast.makeText(SettingsActivity.this,"Data uploaded to server successfully",Toast.LENGTH_SHORT).show();
//
//
//                        } else {
//
//                            Toast.makeText(SettingsActivity.this,"failed",Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }catch ( Exception e)
//                    {
//
//                    }
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//progressFragment.dismiss();
//            }
//        });

    }

    public void downloadDataFromServer()
    {

        getProfileData();

    }


    public void downloadDataFromGoogleDrive()
    {

//       getUserFileId();
        showAllBackUpFiles();
    }




    private void showAllBackUpFiles()
    {



        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                progressFragment.dismiss();
               //  Toast.makeText(SettingsActivity.this,data,Toast.LENGTH_SHORT).show();
                BackFileStatus backFileStatus=new GsonBuilder().create().fromJson(data,BackFileStatus.class);



                if(backFileStatus.getStatus()==1)
                {

                    List<BackupFileListData>bkuplist=backFileStatus.getData();

                    Collections.reverse(bkuplist);

                    List<String>bk=new ArrayList<>();

                    for (BackupFileListData bkf:bkuplist
                         ) {

                        String a=bkf.getEmailId()+"\n"+bkf.getUpdatedDate()+"\n";

                        bk.add(a);


                    }





                    new androidx.appcompat.app.AlertDialog.Builder(SettingsActivity.this)
                            .setTitle("Select a Back up File ")
                            .setSingleChoiceItems(bk.toArray(new String[bk.size()]), 0, null)
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();

                                    int selectedPosition = ((androidx.appcompat.app.AlertDialog)dialog).getListView().getCheckedItemPosition();
//                                    String item= bk.get(selectedPosition);

                                    BackupFileListData bkupfiledata=bkuplist.get(selectedPosition);

                                    getDataFromGoogleDrive(bkupfiledata.getFileId());





                                }
                            })
                            .show();








                }
                else{

                    getUserFileId();
                }




            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(SettingsActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getBackupInfoByuserId+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();

    }





    public void getUserFileId()
    {







        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {

                                String gdrivefileid=profiledata.getData().getGdrive_fileid();
                                String emailid=profiledata.getData().getDrive_mailId();

                                if(emailid!=null&& !emailid.equalsIgnoreCase("")) {

                                    Utils.showAlertWithSingle(SettingsActivity.this, "Your last backup drive is " + emailid + ". Please select the same email id for restoring the backup.", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {
                                            getDataFromGoogleDrive(gdrivefileid);
                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });

                                }

                                else{

                                    if(Utils.checkDBTablempty(SettingsActivity.this))
                                    {
                                        getDataFromGoogleDrive(gdrivefileid);
                                    }
                                    else {

                                        Utils.showAlertWithSingle(SettingsActivity.this, "Please backup your data first", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });
                                    }

                                }






                            }





                        }
                        else {

                            Toast.makeText(SettingsActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(SettingsActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();


    }



    public void getDataFromGoogleDrive(String fileid)
    {

        this.fileid=fileid;


        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        googleSignInClient = GoogleSignIn.getClient(SettingsActivity.this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
     //   startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);

        someActivityResultLauncher.launch(googleSignInClient.getSignInIntent());



    }



    public void getProfileData()
    {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());
        // params.put("device_id",token);

        new RequestHandler(SettingsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                progressFragment.dismiss();
                if(profiledata!=null)
                {

                    try{



                        if(profiledata.getStatus()==1)
                        {


                            if(profiledata.getData()!=null)
                            {

                                if(profiledata.getData().getServerbackup_fileid()!=null&&!profiledata.getData().getServerbackup_fileid().equalsIgnoreCase("")) {

                                    String downloadurl = Utils.backupbaseurl + profiledata.getData().getServerbackup_fileid();


                                    getDataFromUrl(downloadurl);
                                }
                                else{


                                    Utils.showAlertWithSingle(SettingsActivity.this, "Please  backup your data on server first", new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                }

                            }





                        }
                        else {

                            Toast.makeText(SettingsActivity.this," failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }



                }
            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                Toast.makeText(SettingsActivity.this,err,Toast.LENGTH_SHORT).show();

            }
        },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();




//
//
//        Call<Profiledata> jsonObjectCall=client.getUserProfile(new PreferenceHelper(SettingsActivity.this).getData(Utils.userkey)
//
//        );
//        jsonObjectCall.enqueue(new Callback<Profiledata>() {
//            @Override
//            public void onResponse(Call<Profiledata> call, Response<Profiledata> response) {
//
//                progressFragment.dismiss();
//                if(response.body()!=null)
//                {
//
//                    try{
//
//
//
//                        if(response.body().getStatus()==1)
//                        {
//
//
//                            if(response.body().getData()!=null)
//                            {
//
//                                String downloadurl=Utils.backupbaseurl+response.body().getData().getId()+".json";
//
//
//                                getDataFromUrl(downloadurl);
//
//                            }
//
//
//
//
//
//                        }
//                        else {
//
//                            Toast.makeText(SettingsActivity.this," failed",Toast.LENGTH_SHORT).show();
//                        }
//
//
//
//                    }catch (Exception e)
//                    {
//
//                    }
//
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<Profiledata> call, Throwable t) {
//
//                progressFragment.dismiss();
//            }
//        });



    }




    public void getDataFromUrl(final String url)
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"fkjfk");


        ExecutorService executors = Executors.newFixedThreadPool(1);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // your async code goes here.
                progressFragment.dismiss();
                String line="";
                try {
                URL Url = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
                InputStream is = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                line = sb.toString();
                connection.disconnect();
                is.close();
                sb.delete(0, sb.length());


                    Thread.sleep(10_000);

                    // create message and pass any object here doesn't matter
                    // for a simple example I have used a simple string
                   //String msg = "My Message!";

                } catch (Exception e) {
                    e.printStackTrace();
                }


                Log.e("TAGjson",line);



                try{




                    if(!line.equalsIgnoreCase(""))
                    {

                        JSONArray jsonArray=new JSONArray(line);

                        // JSONArray jsonArray=new JSONArray(contents);

                        JSONArray jsonArray_account=new JSONArray();
                        JSONArray jsonArray_diarysub=new JSONArray();
                        JSONArray jsonArray_diary=new JSONArray();

                        JSONArray jsonArray_budget=new JSONArray();
                        JSONArray jsonArray_investment=new JSONArray();
                        JSONArray jsonArray_task=new JSONArray();
                        JSONArray jsonArray_accsettings=new JSONArray();
                        JSONArray jsonArray_insurance=new JSONArray();
                        JSONArray jsonArray_liability=new JSONArray();
                        JSONArray jsonArray_asset=new JSONArray();
                        JSONArray jsonArray_accountsrecipt=new JSONArray();
                        JSONArray jsonArray_apppin=new JSONArray();
                        JSONArray jsonArray_wallet=new JSONArray();
                        JSONArray jsonArray_document=new JSONArray();
                        JSONArray jsonArray_password=new JSONArray();
                        JSONArray jsonArray_visitcard=new JSONArray();


                        JSONArray jsonArray_target=new JSONArray();
                        JSONArray jsonArray_milestone=new JSONArray();
                        JSONArray jsonArray_targetcategory=new JSONArray();
                        JSONArray jsonArray_visitcardimage=new JSONArray();
                        JSONArray jsonArray_weblinks=new JSONArray();
                        JSONArray jsonArray_emergency=new JSONArray();

                        for (int i=0;i<jsonArray.length();i++)
                        {

                            if(i==0) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_account=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_account.length();k++) {

                                    Accounts accounts = new GsonBuilder().create().fromJson(jsonArray_account.get(k).toString(), Accounts.class);


                                    new DatabaseHelper(SettingsActivity.this).addAccountsData(accounts);
                                }
                            }

                            if(i==1) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_diarysub=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_diarysub.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diarysub.get(k).toString(), CommonData.class);

//                                    subject

                                    JSONObject jsonObject1=new JSONObject(jsonArray_diarysub.get(k).toString());

                                    String subject=jsonObject1.getString("subject");



                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARYSUBJECT_table,subject);
                                }
                            }

                            if(i==2) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_diary=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_diary.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_diary.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.DIARY_table,jsonArray_diary.get(k).toString());
                                }
                            }

                            if(i==3) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_budget=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_budget.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_budget.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_BUDGET,jsonArray_budget.get(k).toString());
                                }
                            }
                            if(i==4) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_investment=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_investment.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_investment.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.INVESTMENT_table,jsonArray_investment.get(k).toString());
                                }
                            }


                            if(i==5) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_task=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_task.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_task.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_TASK,jsonArray_task.get(k).toString());
                                }
                            }


                            if(i==6) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_accsettings=new JSONArray(jsonObject);
                                new DatabaseHelper(SettingsActivity.this).dropAllData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);
                                for(int k=0;k<jsonArray_accsettings.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accsettings.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,jsonArray_accsettings.get(k).toString());
                                }
                            }


                            if(i==7) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_insurance=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_insurance.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_insurance.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_INSURANCE,jsonArray_insurance.get(k).toString());
                                }
                            }


                            if(i==8) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_liability=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_liability.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_liability.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_LIABILITY,jsonArray_liability.get(k).toString());
                                }
                            }

                            if(i==9) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_asset=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_asset.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_asset.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ASSET,jsonArray_asset.get(k).toString());
                                }
                            }

                            if(i==10) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_accountsrecipt=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_accountsrecipt.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accountsrecipt.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT,jsonArray_accountsrecipt.get(k).toString());
                                }
                            }

                            if(i==11) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_apppin=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_apppin.length();k++) {

                                    //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_apppin.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_APP_PIN,jsonArray_apppin.get(k).toString());
                                }
                            }

                            if(i==12) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_wallet=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_wallet.length();k++) {

                                    //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_wallet.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_WALLET,jsonArray_wallet.get(k).toString());
                                }
                            }

                            if(i==13) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_document=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_document.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_document.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_DOCUMENT,jsonArray_document.get(k).toString());
                                }
                            }

                            if(i==14) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_password=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_password.length();k++) {

                                    //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_password.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_PASSWORD,jsonArray_password.get(k).toString());
                                }
                            }

                            if(i==15) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_visitcard=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_visitcard.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_visitcard.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_VISITCARD,jsonArray_visitcard.get(k).toString());
                                }
                            }

                            if(i==16) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_target=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_target.length();k++) {

                                    //  CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_target.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_TARGET,jsonArray_target.get(k).toString());
                                }
                            }


                            if(i==17) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_milestone=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_milestone.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_milestone.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_MILESTONE,jsonArray_milestone.get(k).toString());
                                }
                            }


                            if(i==18) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_targetcategory=new JSONArray(jsonObject);
                                new DatabaseHelper(SettingsActivity.this).dropAllDataFromTarget(Utils.DBtables.TABLE_TARGETCATEGORY);
                                for(int k=0;k<jsonArray_targetcategory.length();k++) {
                                    byte[] bitMapData =null;

                                    JSONObject jsonObject1=new JSONObject(jsonArray_targetcategory.get(k).toString());
                                    String name=jsonObject1.getString("name");
                                    String id=jsonObject1.getString("id");
                                    for (int i1 = 0; i1 < Utils.arr_target_iconimgs.length; i1++) {


                                        if(i1==k) {

                                            try {


                                                Drawable drawable = ContextCompat.getDrawable(SettingsActivity.this, Utils.arr_target_iconimgs[i1]);
                                                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                                bitMapData = stream.toByteArray();

                                                //  new DatabaseHelper(MainActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,Utils.arr_target[i],bitMapData);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }


                                    }
                                    TargetCategory accounts = new TargetCategory();
                                    accounts.setId(id);
                                    if(bitMapData!=null) {
                                        accounts.setImage(bitMapData);
                                    }
                                    else{
                                        accounts.setImage(new byte[10]);
                                    }
                                    accounts.setTask_category(name);
                                    new DatabaseHelper(SettingsActivity.this).addTargetData(Utils.DBtables.TABLE_TARGETCATEGORY,accounts.getTask_category(),accounts.getImage());
                                }
                            }



                            if(i==19) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_visitcardimage=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_visitcardimage.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_visitcardimage.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_VISITCARD_IMAGE,jsonArray_visitcardimage.get(k).toString());
                                }
                            }

                            if(i==20) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_weblinks=new JSONArray(jsonObject);

                                for(int k=0;k<jsonArray_weblinks.length();k++) {

                                    //CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_apppin.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_WEBLINKS,jsonArray_weblinks.get(k).toString());
                                }
                            }

                            if(i==21) {
                                String jsonObject = jsonArray.getString(i);
                                jsonArray_emergency=new JSONArray(jsonObject);
                                new DatabaseHelper(SettingsActivity.this).dropAllDataofEmergency();
                                for(int k=0;k<jsonArray_emergency.length();k++) {

                                    // CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accsettings.get(k).toString(), CommonData.class);

                                    new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_EMERGENCY,jsonArray_emergency.get(k).toString());
                                }
                            }









//                                                    if(i==20) {
//                                                        new DatabaseHelper(SettingsActivity.this).deleteAllData(Utils.DBtables.TABLE_TARGETCATEGORY);
//                                                        String jsonObject = jsonArray.getString(i);
//                                                        jsonArray_accountsettings=new JSONArray(jsonObject);
//
//                                                        for(int k=0;k<jsonArray_accountsettings.length();k++) {
//
//                                                            CommonData accounts = new GsonBuilder().create().fromJson(jsonArray_accountsettings.get(k).toString(), CommonData.class);
//
//                                                            new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_ACCOUNTSETTINGS,accounts.getData());
//                                                        }
//                                                    }



                        }




                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SettingsActivity.this," Data restored ",Toast.LENGTH_SHORT).show();

                            }
                        });






                    }



                }catch (Exception e)
                {

                }




            }
        };
        executors.submit(runnable);

        Toast.makeText(SettingsActivity.this,"Data fetched from server successfully",Toast.LENGTH_SHORT).show();




    }







        public void showPin()
    {

        AlertDialog.Builder builder=new AlertDialog.Builder(SettingsActivity.this);
        builder.setMessage("Do you want to set new pin ? ");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();






                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

                double h=displayMetrics.heightPixels/1.2;
                int height = (int)h;
                int width = displayMetrics.widthPixels;

                final Dialog dialog = new Dialog(SettingsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_pindialog);

                final EditText edtName=dialog.findViewById(R.id.edtName);
                ImageView imgclear=dialog.findViewById(R.id.imgclear);


                isfirstpinsubmitted=false;
              final   List<CommonData>cm=new DatabaseHelper(SettingsActivity.this).getData(Utils.DBtables.TABLE_APP_PIN);
                if(cm.size()>0)
                {

                    edtName.setHint("Enter your current PIN");
                }
                else {
                    edtName.setHint("Enter your 6 digit PIN");
                }

                Button btn1=dialog.findViewById(R.id.btn1);
                Button btn2=dialog.findViewById(R.id.btn2);
                Button btn3=dialog.findViewById(R.id.btn3);
                Button btn4=dialog.findViewById(R.id.btn4);
                Button btn5=dialog.findViewById(R.id.btn5);
                Button btn6=dialog.findViewById(R.id.btn6);
                Button btn7=dialog.findViewById(R.id.btn7);
                Button btn8=dialog.findViewById(R.id.btn8);
                Button btn9=dialog.findViewById(R.id.btn9);
                Button btn0=dialog.findViewById(R.id.btn0);
                Button btnsubmit=dialog.findViewById(R.id.btnsubmit);

                edtName.setInputType(InputType.TYPE_NULL);

                btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String p=edtName.getText().toString();

                        if(!isfirstpinsubmitted) {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "1";

                            }




                            edtName.setText(getDotedtextbyLength(firstpin.length()));

                        }
                        else {

                            if(secondpin.length()<6) {
                                secondpin = secondpin + "1";
                            }
                            //edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }

                    }
                });
                btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "2";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {
                            if(firstpin.length()<6) {
                                firstpin = firstpin + "2";
                            }

                           // edtName.setText(firstpin);

                            edtName.setText(getDotedtextbyLength(firstpin.length()));
                        }
                    }
                });

                btn3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();
                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "3";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {
                            if(firstpin.length()<6) {

                                firstpin = firstpin + "3";
                            }

                           // edtName.setText(firstpin);
                            edtName.setText(getDotedtextbyLength(firstpin.length()));
                        }

                    }
                });
                btn4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String p = edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "4";
                            }
                            //edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "4";
                            }

                            edtName.setText(getDotedtextbyLength(firstpin.length()));

                           // edtName.setText(firstpin);
                        }

                    }
                });

                btn5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "5";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "5";
                            }

                           // edtName.setText(firstpin);

                            edtName.setText(getDotedtextbyLength(firstpin.length()));
                        }
                    }
                });

                btn6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "6";
                            }
                            //edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "6";
                            }

                           // edtName.setText(firstpin);

                            edtName.setText(getDotedtextbyLength(firstpin.length()));
                        }

                    }
                });

                btn7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();


                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "7";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "7";
                            }

                            //edtName.setText(firstpin);

                            edtName.setText(getDotedtextbyLength(firstpin.length()));
                        }

                    }
                });

                btn8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "8";
                            }
                          //  edtName.setText(secondpin);
                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "8";
                            }
                            edtName.setText(getDotedtextbyLength(firstpin.length()));

                           // edtName.setText(firstpin);
                        }

                    }
                });

                btn9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "9";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {
                            if(firstpin.length()<6) {
                                firstpin = firstpin + "9";
                            }

                            edtName.setText(getDotedtextbyLength(firstpin.length()));

                           // edtName.setText(firstpin);
                        }

                    }
                });

                btn0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String p=edtName.getText().toString();

                        if(isfirstpinsubmitted)
                        {
                            if(secondpin.length()<6) {
                                secondpin = secondpin + "0";
                            }
                           // edtName.setText(secondpin);

                            edtName.setText(getDotedtextbyLength(secondpin.length()));

                        }
                        else {

                            if(firstpin.length()<6) {
                                firstpin = firstpin + "0";
                            }

                            edtName.setText(getDotedtextbyLength(firstpin.length()));

                            //edtName.setText(firstpin);
                        }

                    }
                });

                imgclear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

edtName.setText("");

                        if(isfirstpinsubmitted)
                        {
                            secondpin = "";

                        }
                        else {
                            firstpin = "";

                        }

                    }
                });



btnsubmit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {







            if (isfirstpinsubmitted) {

                if (!secondpin.equalsIgnoreCase("")) {

                    if (secondpin.equalsIgnoreCase(firstpin)) {



                        dialog.dismiss();
                        new DatabaseHelper(SettingsActivity.this).deleteAllData(Utils.DBtables.TABLE_APP_PIN);

                        new DatabaseHelper(SettingsActivity.this).addData(Utils.DBtables.TABLE_APP_PIN, firstpin);

                        firstpin="";
                        secondpin="";

                    } else {

                        Toast.makeText(SettingsActivity.this, "PIN confirmation failed", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    Toast.makeText(SettingsActivity.this, "Please confirm you PIN", Toast.LENGTH_SHORT).show();
                }


            }
            else   if(cm.size()>0) {

                String data = cm.get(0).getData();

                if (data.equalsIgnoreCase(firstpin)) {
                    firstpin="";
                    edtName.setText("");
                    edtName.setHint("Enter PIN");
                    cm.clear();

                }
                else {

                    Toast.makeText(SettingsActivity.this, "Sorry your current PIN is wrong", Toast.LENGTH_SHORT).show();
                }
            }




            else {

                isfirstpinsubmitted = true;

                edtName.setText("");
                edtName.setHint("Confirm PIN");
            }






    }
});








                dialog.getWindow().setLayout(width,height);

                dialog.show();




            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });

        builder.show();



    }



    public String  getDotedtextbyLength(int l)
    {
        String data="";
        for (int i=0;i<l;i++)
        {

            data=data+"\u2022";


        }

        return data;
    }
}
