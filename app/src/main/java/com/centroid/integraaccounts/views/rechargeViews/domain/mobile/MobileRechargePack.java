
package com.centroid.integraaccounts.views.rechargeViews.domain.mobile;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MobileRechargePack {

    @SerializedName("status")
    @Expose
    private String status="";
    @SerializedName("categories")
    @Expose
    private List<Category> categories=new ArrayList<>();
    @SerializedName("requestId")
    @Expose
    private String requestId="";
    @SerializedName("processingTime")
    @Expose
    private Double processingTime=0.0;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Double getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(Double processingTime) {
        this.processingTime = processingTime;
    }

}
