package com.centroid.integraaccounts.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.adapter.IncExpFullAdapter;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class AssetAccountsActivity extends AppCompatActivity {

    TextView txtHead,txtTotal;

    ImageView imgback;

    String accountsetupid="",closebalacebeforedate="",Type="";

    RecyclerView recycler;

    List<Accounts>IncExp=new ArrayList<>();

    String openingbalance="";
    LinearLayout layout_head;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_accounts);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        txtHead=findViewById(R.id.txtHead);
        txtTotal=findViewById(R.id.txtTotal);
        recycler=findViewById(R.id.recycler);
        layout_head=findViewById(R.id.layout_head);

        accountsetupid=getIntent().getStringExtra("accountsetupid");
        closebalacebeforedate=getIntent().getStringExtra("closebalacebeforedate");

        double clo=Double.parseDouble(closebalacebeforedate);

        if (clo<0||String.valueOf(clo).contains("-"))
        {
            clo=clo*-1;
        }

        txtTotal.setText("Closing Balance : "+clo+"");



        List<CommonData> commonData=new DatabaseHelper(AssetAccountsActivity.this).getAccountSettingsByID(accountsetupid);

        if(commonData.size()>0)
        {
            CommonData cm=commonData.get(0);

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(cm.getData());
                String Accounttype=jsonObject.getString("Accountname");
                openingbalance=jsonObject.getString("Amount");

                Type=jsonObject.getString("Type");
                txtHead.setText("Ledger : "+Utils.getCapsSentences(AssetAccountsActivity.this,Accounttype));
            } catch (JSONException e) {
                e.printStackTrace();
            }





        }


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getAccounts();
    }




    private void getAccounts()
    {


        try {

          //  IncExp.clear();

            List<Accounts> accounts = new DatabaseHelper(AssetAccountsActivity.this).getAccountsDataBYsetupid(accountsetupid);


            for (Accounts acc : accounts) {

                if(acc.getACCOUNTS_setupid().equalsIgnoreCase(accountsetupid))
                {
                    IncExp.add(acc);

                }


            }





            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);



            double openingbal = Double.parseDouble(openingbalance);

            if(Type.equalsIgnoreCase("Credit"))
            {
                openingbal=openingbal*-1;
            }




            Accounts acc = new Accounts();

            acc.setACCOUNTS_entryid("0");


                acc.setACCOUNTS_date("1-" + "1" + "-" + year);
           // }
            acc.setACCOUNTS_setupid("-1");
            acc.setACCOUNTS_amount(openingbal + "");

            if (openingbal>=0) {

                acc.setACCOUNTS_type(Utils.Cashtype.debit + "");
            } else {
                acc.setACCOUNTS_type(Utils.Cashtype.credit + "");

            }
            acc.setACCOUNTS_remarks("");
            acc.setACCOUNTS_month("1");
            acc.setACCOUNTS_year(year + "");

            IncExp.add(0, acc);




            if (IncExp.size() > 0) {
                layout_head.setVisibility(View.VISIBLE);
                recycler.setVisibility(View.VISIBLE);

                IncExpFullAdapter incExpFullAdapter = new IncExpFullAdapter(AssetAccountsActivity.this, IncExp);
                recycler.setLayoutManager(new LinearLayoutManager(AssetAccountsActivity.this));
                recycler.setAdapter(incExpFullAdapter);
            } else {
                recycler.setVisibility(View.GONE);

                layout_head.setVisibility(View.GONE);
            }


        }catch (Exception e)
        {

        }

    }

}
