package com.centroid.integraaccounts.messageservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.centroid.integraaccounts.Constants.Utils;

public class TaskRemindReceiver  extends BroadcastReceiver {

    public TaskRemindReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Utils.scheduleJob(context);
    }
}
