package com.centroid.integraaccounts.messageservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.widget.Toast;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.util.Date;

public class MessageService extends FirebaseMessagingService {


    public MessageService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String a=remoteMessage.getFrom();
        remoteMessage.getNotification().getBody();

        try {


            if (remoteMessage != null) {

                if (remoteMessage.getNotification() != null) {

                    int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);


                    String title = remoteMessage.getNotification().getTitle();

                    String message = remoteMessage.getNotification().getBody();


                    int notifyID = 1;
                    String CHANNEL_ID = "my_channel_01";// The id of the channel.
                    CharSequence name = "mychannel";// The user-visible name of the channel.
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel;
// Create a notification and set the notification channel.

                    Notification notification = null;

                    NotificationManager mNotificationManager;

                    Intent notificationIntent = new Intent(MessageService.this, SplashActivity.class);
//                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
                    PendingIntent pendingIntent;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                        pendingIntent = PendingIntent.getActivity(this,
//                                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE);
//
//                    }else {
//                        pendingIntent = PendingIntent.getActivity(this,
//                                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                    }



                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        pendingIntent = PendingIntent.getActivity(this, 1251, notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
                        notification = new Notification.Builder(MessageService.this, CHANNEL_ID)
                                .setContentTitle(title)
                                .setContentText(message)
                                .setSmallIcon(R.drawable.logo).setColor(getColor(R.color.appbg))
                                .setContentIntent(pendingIntent)

                                .setChannelId(CHANNEL_ID)
                                .setStyle(new Notification.BigTextStyle().bigText(message))
                                .build();

                        mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                        mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.createNotificationChannel(mChannel);

                    } else {
                        pendingIntent = PendingIntent.getActivity(this, 1251, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        notification = new Notification.Builder(MessageService.this)
                                .setContentTitle(title)
                                .setContentText(message)
                                .setSmallIcon(R.drawable.logo)
                                .setContentIntent(pendingIntent)
                                .setStyle(new Notification.BigTextStyle().bigText(message))

                                .build();
                        mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    }

//                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        notification.setSmallIcon(R.drawable.icon_transperent);
//                        notification.setColor(getResources().getColor(R.color.notification_color));
//                    } else {
//                        notification.setSmallIcon(R.drawable.icon);
//                    }


                    MediaPlayer mp;
                    mp = MediaPlayer.create(MessageService.this, R.raw.notifysound);
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            mp.reset();
                            mp.release();
                            mp = null;
                        }
                    });
                    mp.start();


// Issue the notification.
                    mNotificationManager.notify(m, notification);

                }


            }

        }catch (Exception e)
        {
e.printStackTrace();

        }

    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

      //  Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();


    }
}
