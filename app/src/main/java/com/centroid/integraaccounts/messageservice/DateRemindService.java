package com.centroid.integraaccounts.messageservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddnewTaskActivity;
import com.centroid.integraaccounts.views.SplashActivity;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DateRemindService extends JobService {
    public DateRemindService() {
    }


    @Override
    public boolean onStartJob(JobParameters jobParameters) {

//        List<CommonData>commonData=new DatabaseHelper(getApplicationContext()).getData(Utils.DBtables.TABLE_TASK);
//
//       // Log.e("Task data",commonData.size()+"");
//
//
//        try{
//
//    for (CommonData cm:commonData)
//    {
//        int notificationcompleted =0;
//
//        JSONObject jsonObject=new JSONObject(cm.getData());
//
//        String date=jsonObject.getString("date");
//
//        if(jsonObject.has("notificationcompleted")) {
//
//             notificationcompleted = jsonObject.getInt("notificationcompleted");
//        }
//        //Log.e("Task date",date);
//
//        if(jsonObject.has("reminddate"))
//        {
//
//            String reminddate=jsonObject.getString("reminddate");
//            Log.e("Remind date",reminddate);
//        }
//
//        String time=jsonObject.getString("time");
//        SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
//        String currenttime=fmtOut.format(new Date());
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//        String currentdate=sdf.format(new Date());
//
//        if(notificationcompleted!=1)
//        {
//
//        if(date.equalsIgnoreCase(currentdate)) {
//
//            if (time.equalsIgnoreCase(currenttime)) {
//
//
//                String Task = jsonObject.getString("name");
//
//
//                // Toast.makeText(getApplicationContext(),"Time starts now",Toast.LENGTH_SHORT).show();
//
//                String title = "Reminder";
//
//                String message = Task;
//
//
//                int notifyID = 1;
//                String CHANNEL_ID = "my_channel_01";// The id of the channel.
//                CharSequence name = "mychannel";// The user-visible name of the channel.
//                int importance = NotificationManager.IMPORTANCE_HIGH;
//                NotificationChannel mChannel;
//// Create a notification and set the notification channel.
//
//                Notification notification = null;
//
//                NotificationManager mNotificationManager;
//
//                Intent in = new Intent(getApplicationContext(), SplashActivity.class);
//                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, in, 0);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
//                            .setContentTitle(title)
//                            .setContentText(message)
//                            .setSmallIcon(R.mipmap.ic_launcher)
//                            .setContentIntent(pendingIntent)
//
//                            .setChannelId(CHANNEL_ID)
//                            .setStyle(new Notification.BigTextStyle().bigText(message))
//                            .build();
//
//                    mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//                    mNotificationManager =
//                            (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                    mNotificationManager.createNotificationChannel(mChannel);
//
//                } else {
//
//                    notification = new Notification.Builder(getApplicationContext())
//                            .setContentTitle(title)
//                            .setContentText(message)
//                            .setSmallIcon(R.mipmap.ic_launcher)
//                            .setContentIntent(pendingIntent)
//                            .setStyle(new Notification.BigTextStyle().bigText(message))
//
//                            .build();
//                    mNotificationManager =
//                            (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
//                }
//
////                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                        notification.setSmallIcon(R.drawable.icon_transperent);
////                        notification.setColor(getResources().getColor(R.color.notification_color));
////                    } else {
////                        notification.setSmallIcon(R.drawable.icon);
////                    }
//
//
//                MediaPlayer mp;
//                mp = MediaPlayer.create(getApplicationContext(), R.raw.notifysound);
//                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                    @Override
//                    public void onCompletion(MediaPlayer mp) {
//                        // TODO Auto-generated method stub
//                        mp.reset();
//                        mp.release();
//                        mp = null;
//                    }
//                });
//                mp.start();
//
//
//// Issue the notification.
//                mNotificationManager.notify(notifyID++, notification);
//
//
//                jsonObject.put("notificationcompleted", 1);
//
//                new DatabaseHelper(getApplicationContext()).updateData(cm.getId(), jsonObject.toString(), Utils.DBtables.TABLE_TASK);
//
//
//            }
//
//        }
//        }
//
//
//
//
//
//
//        //  Log.e("Task time",time+"\n Task date : "+date+"\n: Current time : "+timeselected);
//
//    }
//
//
//
//        }catch (Exception e)
//        {
//
//        }
//
//
//
//        Utils.scheduleJob(getApplicationContext());

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}