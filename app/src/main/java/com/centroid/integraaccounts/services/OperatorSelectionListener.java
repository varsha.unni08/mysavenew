package com.centroid.integraaccounts.services;

public interface OperatorSelectionListener {


   public void onOperatorSelected(int position,String selectedoperator);
}
