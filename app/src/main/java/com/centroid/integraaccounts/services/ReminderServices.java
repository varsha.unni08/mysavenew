package com.centroid.integraaccounts.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;
import android.os.IBinder;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.SplashActivity;
import com.centroid.integraaccounts.views.TasksActivity;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ReminderServices  extends Service {
    int notifyID = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        onTaskRemoved(intent);
       // Toast.makeText(ReminderServices.this,"This is a Service running in Background",
               // Toast.LENGTH_SHORT).show();
        try {

            List<CommonData> taskData = new DatabaseHelper(ReminderServices.this).getData(Utils.DBtables.TABLE_TASK);

         //   Collections.reverse(taskData);

            for (CommonData cm : taskData) {

                JSONObject jsonObject = new JSONObject(cm.getData());




                String status = jsonObject.getString("status");
                String date1 ="";
                if(jsonObject.has("reminddate"))
                {
                    date1 =   jsonObject.getString("reminddate");
                }
                else {
                     date1 = jsonObject.getString("date");
                }
                String tname=jsonObject.getString("name");

                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Calendar calendar = Calendar.getInstance();
                int y = calendar.get(Calendar.YEAR);
                int m = calendar.get(Calendar.MONTH)+1;
                int d = calendar.get(Calendar.DAY_OF_MONTH);
                String cu_date = d + "-" + m + "-" + y;
                Date currentdate = dateFormat.parse(cu_date);

                Date cmdate = dateFormat.parse(date1);






                if(cmdate.equals(currentdate)) {


                    String title = "Reminder";

                    String message = tname;


                    String CHANNEL_ID = "my_channel_01";// The id of the channel.
                    CharSequence name = "mychannel";// The user-visible name of the channel.
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel;
// Create a notification and set the notification channel.

                    Notification notification = null;

                    NotificationManager mNotificationManager;

                    Intent in = new Intent(ReminderServices.this, SplashActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(ReminderServices.this, 0, in, 0);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notification = new Notification.Builder(ReminderServices.this, CHANNEL_ID)
                                .setContentTitle(title)
                                .setContentText(message)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentIntent(pendingIntent)

                                .setChannelId(CHANNEL_ID)
                                .setStyle(new Notification.BigTextStyle().bigText(message))
                                .build();

                        mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                        mNotificationManager =
                                (NotificationManager) ReminderServices.this.getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.createNotificationChannel(mChannel);

                    } else {

                        notification = new Notification.Builder(ReminderServices.this)
                                .setContentTitle(title)
                                .setContentText(message)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentIntent(pendingIntent)
                                .setStyle(new Notification.BigTextStyle().bigText(message))

                                .build();
                        mNotificationManager =
                                (NotificationManager) ReminderServices.this.getSystemService(Context.NOTIFICATION_SERVICE);
                    }
                    if(!jsonObject.has("NotificationRead")) {
                        mNotificationManager.notify(notifyID++, notification);
                        jsonObject.put("NotificationRead", 1);

                        new DatabaseHelper(ReminderServices.this).updateData(cm.getId(), jsonObject.toString(), Utils.DBtables.TABLE_TASK);


                    }
                    else{

                }

                    break;
                }
            }
        }catch (Exception e)
        {


        }


        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(ReminderServices.this,this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

}
