package com.centroid.integraaccounts.Constants;

import com.centroid.integraaccounts.R;

public class RechargePlans {

    public static class Airtelplans{


        public static  String arr_plans[]={"Airtel Prepaid","Airtel Live ( Non Gst )","Airtel Money Prepaid","Airtel Money ( Non Gst ) Prepaid","Airtel Non complain (Prepaid)"};

        public static  String spkeys[]={"3","ALN","AMM","AMN","ANC"};

    }


    public static class Viplans{


        public static  String arr_plans[]={"VI Prepaid","Vi ( Non Gst)"};

        public static  String spkeys[]={"VIL","VIN"};

    }


    public static class Jioplans{


        public static  String arr_plans[]={"Reliance Jio Prepaid","Reliance Jio ( Non Gst ) Prepaid","Realiance Jio Postpaid"};

        public static  String spkeys[]={"116","RJN","103"};

    }


    public static class BSNLplans{


        public static  String arr_plans[]={"BSNL Postpaid","BSNL - Special Tariff-Prepaid",
                "BSNL - Special Tariff ( Non Gst ) Prepaid","BSNL J&k Special Tarrif-Prepaid",
        "BSNL J&k Talktime","BSNL Talktime prepaid","BSNL Talktime ( Non Gst) Prepaid"};

        public static  String spkeys[]={"104","5","BSN","7","6","4","BTN"};

    }



    public static class DTHPlans{

        public static String arr_plans[]={"Airtel Digital TV",
                "Dish TV",
        "Sun Direct","Tata Sky",
                "Videocon D2H"};

        public static String arr_plans_toServer[]={"Airtel dth",
                "Dish TV",
                "Sun Direct","Tata Sky",
                "Videocon"};

        public static String arr_plans_opcode[]={"AD",
                "DT",
                "SD","TS",
                "VD"};

        public static  String spkeys[]={"51","53","54","55","56"};

        public static int arr_plans_img[]={R.drawable.airteldigitaltv,R.drawable.dishtv,R.drawable.sundirect,R.drawable.ttsky,R.drawable.d2h};

    }


    public static class MobilePlans{






        public static  String operator_circlecode[]={"AP","AS","BR","DL","GJ","HP","HR","JK","KL","KA","KO","MH","MP","MU","NE","OR",
        "PB","RJ","TN","UE","UW","WB"};

        public static  String operator_circle[]={"Andhra Pradesh","Assam","Bihar and Jharkhand","Delhi Metro","Gujarat","Himachal Pradesh",
        "Haryana","Jammu and kashmir","Kerala","Karnataka","Kolkata Metro","Maharashtra","Madhya Pradesh and Chhattisgarh",
        "Mumbai Metro","North East India","Odisha","Punjab","Rajasthan","Tamil Nadu","Uttar Pradesh(East)","Uttar Pradesh (West) and Uttarakhand",
        "West Bangal"};
    }
}
