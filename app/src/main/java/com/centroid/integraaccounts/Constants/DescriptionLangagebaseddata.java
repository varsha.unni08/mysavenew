package com.centroid.integraaccounts.Constants;

public class DescriptionLangagebaseddata {


    public static String hindidata="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> खाता सेटअप </ b> <br>\n" +
            "                 \n" +
            "                   खाता सेटअप वित्तीय लेनदेन के लिए खाता शीर्षों (बही) के प्रबंधन के लिए है।\n" +
            "यह सभी मौजूदा खाता शीर्षों को वर्णानुक्रम में प्रदर्शित करेगा और हम कोई भी संशोधन करने के लिए प्रत्येक का चयन कर सकते हैं। हम + बटन को स्पर्श करके भी एक नया बना सकते हैं।\n" +
            "हम निम्नलिखित विवरण दर्ज कर सकते हैं:\n" +
            "खाता नाम - इस फ़ील्ड का उपयोग खाता नाम/लेज़र नाम को सहेजने के लिए किया जाता है। इस क्षेत्र में डुप्लिकेट और रिक्त मानों की अनुमति नहीं है।\n" +
            "खाता श्रेणी - ड्रॉप डाउन सूची से दिए गए खाते की श्रेणी का चयन करने के लिए प्रयुक्त होता है।\n" +
            "ओपनिंग बैलेंस - ऐप से शुरू करते समय उस अकाउंट में ओपनिंग बैलेंस डालने के लिए इस्तेमाल किया जाता है। यह अनिवार्य नहीं है।\n" +
            "प्रकार - इसका उपयोग दर्ज की गई प्रारंभिक शेष राशि का उल्लेख करने के लिए किया जाता है चाहे वह डेबिट हो या क्रेडिट। यदि कोई प्रारंभिक शेष राशि नहीं है, तो इसे चुनने की कोई आवश्यकता नहीं है।" +
            "                        <b> भुगतान वाउचर </ b> <br>\n" +
            "                        \n" +
            "                       भुगतान वाउचर\n" +
            "\n" +
            "इस वाउचर का उपयोग दैनिक भुगतान/खर्चों को रिकॉर्ड करने के लिए किया जाता है।\n" +
            "\n" +
            "पहली स्क्रीन चालू माह के सभी वाउचर प्रदर्शित करती है।\n" +
            "हम आवश्यक लोगों को संपादित करना या हटाना चुन सकते हैं।\n" +
            "नया वाउचर बनाने के लिए + बटन स्पर्श करें।\n" +
            "वर्तमान तिथि प्रदर्शित की जाएगी और इसे आवश्यकतानुसार बदला जा सकता है।\n" +
            "आप ड्रॉपडाउन सूची से खाता शीर्ष का चयन कर सकते हैं।\n" +
            "आप इसके आगे + चिह्न को स्पर्श करके एक नया बना सकते हैं।\n" +
            "आप राशि के बगल में ड्रॉपडाउन सूची का उपयोग यह इंगित करने के लिए कर सकते हैं कि दर्ज की गई राशि नकद या बैंक लेनदेन में है या नहीं।\n" +
            "बैंक का चयन करने के बाद, बैंक का नाम चुनने के लिए एक ड्रॉपडाउन सूची तुरंत दिखाई देगी।\n" +
            "आप इसके आगे + चिह्न को स्पर्श करके एक नया बैंक खाता बना सकते हैं।\n" +
            "आप टिप्पणी के कॉलम में भुगतान के बारे में कुछ टिप्पणी, यदि कोई हो, दर्ज कर सकते हैं।" +
            "                 \n" +
            "                        <b> रसीद वाउचर </ b> <br>\n" +
            "                  \n" +
            "                        इस वाउचर का उपयोग प्राप्तियों / आय को रिकॉर्ड करने के लिए किया जाता है\n" +
            "                 \n" +
            "                       पहली स्क्रीन चालू माह के सभी वाउचर प्रदर्शित करती है।\n" +
            "                      हम आवश्यक लोगों को संपादित या हटाना चुन सकते हैं।\n" +
            "                   नया वाउचर बनाने के लिए + बटन स्पर्श करें।\n" +
            "                     वर्तमान तिथि प्रदर्शित की जाएगी और आवश्यकतानुसार इसे बदला जा सकता है।\n" +
            "                 आप ड्रॉपडाउन सूची से खाता प्रमुख का चयन कर सकते हैं।\n" +
            "                      आप इसके बगल में स्थित + चिन्ह को स्पर्श करके एक नया बना सकते हैं।\n" +
            "                     आप यह इंगित करने के लिए राशि के आगे ड्रॉपडाउन सूची का उपयोग कर सकते हैं कि क्या दर्ज की गई राशि नकद या बैंक लेनदेन में है।\n" +
            "                बैंक का चयन करने के बाद, बैंक नाम चुनने की एक ड्रॉपडाउन सूची तुरंत दिखाई देगी।\n" +
            "                      आप इसके बगल में स्थित + चिन्ह को स्पर्श करके एक नया बैंक खाता बना सकते हैं।\n" +
            "                       आप टिप्पणी कॉलम में किसी भी रसीद के बारे में कुछ टिप्पणी दर्ज कर सकते हैं। <br>\n" +
            "                      \n" +
            "                        <b> जर्नल वाउचर </ b> <br>\n" +
            "                     \n" +
            "                      जर्नल वाउचर का उपयोग दो खातों के बीच डेबिट / क्रेडिट लेनदेन के लिए किया जाता है।\n" +
            "                      डेबिट खाते का चयन करने के बाद, आप राशि दर्ज कर सकते हैं और फिर क्रेडिट खाते का चयन कर सकते हैं। यदि आवश्यक हो तो टिप्पणी दर्ज की जा सकती है <b> बैंक वाउचर </ b>\n" +
            "<br> इस वाउचर का उपयोग बैंक से नकदी जमा और निकासी को रिकॉर्ड करने के लिए किया जाता है।\n" +
            "\n" +
            "यहां ड्रॉपडाउन सूची खाता सेटअप में श्रेणी बैंक में शामिल खातों को दिखाती है।\n" +
            "आप इसके आगे + चिन्ह दबाकर एक नया खाता भी बना सकते हैं।\n" +
            "उसके बाद आपको यह रिकॉर्ड करना होगा कि आप राशि जमा कर रहे हैं या निकाल रहे हैं और दर्ज कर रहे हैं";


    public static String Engdata="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> Account Setup </b> <br>  \n" +
            "                 \n" +
            "                   Account Setup is for managing the account heads (ledgers) for financial transactions.\n" +
            "It will display all existing Account heads in alphabetical order and we can select each one to do any modifications. We can also create a new one by touching + button.\n" +
            "We can enter the following details:\n" +
            "               Account Name - This field is used to save the Account Name/Ledger Name. Duplicate and blank values are not allowed in this field.\n" +
            "               Account Category - Used to select the category of the given account from a drop down list.\n" +
            "               Opening Balance - Used to enter the opening balance in that account when starting with the app. This is not mandatory.\n" +
            "               Type - This is used to mention the entered opening balance whether it is debit or credit. If there is no opening balance, no need to select it. <br>\n" +
            "                     \n" +
            "                        <b> Payment Voucher </b> <br> \n" +
            "                        \n" +
            "                       Payment Voucher\n" +
            " \n" +
            "This voucher is used to record daily payments/expenses.\n" +
            " \n" +
            "The first screen displays all the vouchers of the current month. \n" +
            "We can choose to edit or delete the required ones. \n" +
            "Touch the + button to create a new voucher. \n" +
            "The current date will be displayed and it can be changed as required. \n" +
            "You can select the Account Head from the dropdown list. \n" +
            "You can create a new one by touching the + sign next to it. \n" +
            "You can use the dropdown list next to the amount to indicate whether the amount entered is in cash or bank transaction. \n" +
            "Once the bank is selected, a dropdown list for selecting the bank name will appear immediately. \n" +
            "You can create a new bank account by touching the + sign next to it. \n" +
            "You can enter some remarks, if any, about the payment in the remark’s column.<br>" +
            "                 \n" +
            "                        <b> Receipt Voucher </b> <br> \n" +
            "                  \n" +
            "                        This voucher is used to record receipts/incomes \n" +
            " \n" +
            "The first screen displays all the vouchers of the current month. \n" +
            "We can choose to edit or delete the required ones.\n" +
            "Touch the + button to create a new voucher. \n" +
            "The current date will be displayed and it can be changed as required. \n" +
            "You can select the Account Head from the dropdown list. \n" +
            "You can create a new one by touching the + sign next to it. \n" +
            "You can use the dropdown list next to the amount to indicate whether the amount entered is in cash or bank transaction. \n" +
            "Once the bank is selected, a dropdown list for selecting the bank name will appear immediately. \n" +
            "You can create a new bank account by touching the + sign next to it. \n" +
            "You can enter some remarks if any about the receipt in the remarks column.. <br> \n" +
            "                      \n" +
            "                        <b> Journal Voucher </b> <br>\n" +
            "                     \n" +
            "                      Journal vouchers are used for debit / credit transactions between two accounts. \n" +
            "                      After selecting the debit account, you can enter the amount and then select the credit account. Comment can be entered if needed<b> Bank Voucher </b>\n" +
            "<br>This voucher is used to record cash deposits and withdrawals from the bank. \n" +
            "\n" +
            "Here the dropdown list shows the accounts included in the category bank in the account setup. \n" +
            "You can also create a new account by pressing the + sign next to it. \n" +
            "After that you have to record whether you are depositing or withdrawing and entering the amount";


    public static String mldata="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> അക്കൗണ്ട് സജ്ജീകരണം </ b> <br>\n" +
            "                 \n" +
            "                  സാമ്പത്തിക ഇടപാടുകൾക്കായി അക്കൗണ്ട് ഹെഡ്സ് (ലെഡ്ജറുകൾ) കൈകാര്യം ചെയ്യുന്നതിനാണ് അക്കൗണ്ട് സജ്ജീകരണം.\n" +
            "ഇത് നിലവിലുള്ള എല്ലാ അക്ക head ണ്ട് ഹെഡുകളും അക്ഷരമാലാക്രമത്തിൽ പ്രദർശിപ്പിക്കും, കൂടാതെ എന്തെങ്കിലും പരിഷ്കാരങ്ങൾ നടത്താൻ നമുക്ക് ഓരോരുത്തരെയും തിരഞ്ഞെടുക്കാം. + ബട്ടൺ സ്\u200Cപർശിച്ചുകൊണ്ട് നമുക്ക് പുതിയതൊന്ന് സൃഷ്\u200Cടിക്കാനും കഴിയും.\n" +
            "ഞങ്ങൾക്ക് ഇനിപ്പറയുന്ന വിശദാംശങ്ങൾ നൽകാം:\n" +
            "അക്ക name ണ്ട് നാമം - അക്ക name ണ്ട് നാമം / ലെഡ്ജർ നാമം സംരക്ഷിക്കാൻ ഈ ഫീൽഡ് ഉപയോഗിക്കുന്നു. ഈ ഫീൽഡിൽ തനിപ്പകർപ്പും ശൂന്യവുമായ മൂല്യങ്ങൾ അനുവദനീയമല്ല.\n" +
            "അക്ക Category ണ്ട് വിഭാഗം - ഒരു ഡ്രോപ്പ് ഡ down ൺ ലിസ്റ്റിൽ നിന്ന് തന്നിരിക്കുന്ന അക്ക of ണ്ടിന്റെ വിഭാഗം തിരഞ്ഞെടുക്കാൻ ഉപയോഗിക്കുന്നു.\n" +
            "ഓപ്പണിംഗ് ബാലൻസ് - ആപ്ലിക്കേഷൻ ആരംഭിക്കുമ്പോൾ ആ അക്കൗണ്ടിലെ ഓപ്പണിംഗ് ബാലൻസ് നൽകാൻ ഉപയോഗിക്കുന്നു. ഇത് നിർബന്ധമല്ല.\n" +
            "തരം - നൽകിയ ഓപ്പണിംഗ് ബാലൻസ് ഡെബിറ്റ് അല്ലെങ്കിൽ ക്രെഡിറ്റ് ആണോ എന്ന് സൂചിപ്പിക്കാൻ ഇത് ഉപയോഗിക്കുന്നു. ഓപ്പണിംഗ് ബാലൻസ് ഇല്ലെങ്കിൽ, അത് തിരഞ്ഞെടുക്കേണ്ടതില്ല." +
            "                        <b> പേയ്\u200Cമെന്റ് വൗച്ചർ </ b> <br>\n" +
            "                        \n" +
            "                       പണം അടക്കുന്ന രസീത്\n" +
            "\n" +
            "ദൈനംദിന പേയ്\u200Cമെന്റുകൾ / ചെലവുകൾ രേഖപ്പെടുത്താൻ ഈ വൗച്ചർ ഉപയോഗിക്കുന്നു.\n" +
            "\n" +
            "ആദ്യ സ്\u200Cക്രീൻ നിലവിലെ മാസത്തിലെ എല്ലാ വൗച്ചറുകളും പ്രദർശിപ്പിക്കുന്നു.\n" +
            "ആവശ്യമായവ എഡിറ്റുചെയ്യാനോ ഇല്ലാതാക്കാനോ നമുക്ക് തിരഞ്ഞെടുക്കാം.\n" +
            "ഒരു പുതിയ വൗച്ചർ സൃഷ്ടിക്കാൻ + ബട്ടൺ സ്\u200Cപർശിക്കുക.\n" +
            "നിലവിലെ തീയതി പ്രദർശിപ്പിക്കും, ആവശ്യാനുസരണം ഇത് മാറ്റാം.\n" +
            "ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റിൽ നിന്ന് നിങ്ങൾക്ക് അക്കൗണ്ട് ഹെഡ് തിരഞ്ഞെടുക്കാം.\n" +
            "അതിനടുത്തുള്ള + ചിഹ്നം സ്\u200Cപർശിച്ചുകൊണ്ട് നിങ്ങൾക്ക് പുതിയൊരെണ്ണം സൃഷ്\u200Cടിക്കാൻ കഴിയും.\n" +
            "നൽകിയ തുക പണത്തിലാണോ ബാങ്ക് ഇടപാടിലാണോ എന്ന് സൂചിപ്പിക്കുന്നതിന് നിങ്ങൾക്ക് തുകയുടെ അടുത്തുള്ള ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റ് ഉപയോഗിക്കാം.\n" +
            "ബാങ്ക് തിരഞ്ഞെടുത്തുകഴിഞ്ഞാൽ, ബാങ്ക് നാമം തിരഞ്ഞെടുക്കുന്നതിനുള്ള ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റ് ഉടനടി ദൃശ്യമാകും.\n" +
            "അതിനടുത്തുള്ള + ചിഹ്നത്തിൽ സ്പർശിച്ചുകൊണ്ട് നിങ്ങൾക്ക് ഒരു പുതിയ ബാങ്ക് അക്ക create ണ്ട് സൃഷ്ടിക്കാൻ കഴിയും.\n" +
            "അഭിപ്രായത്തിന്റെ നിരയിലെ പേയ്\u200Cമെന്റിനെക്കുറിച്ച് നിങ്ങൾക്ക് എന്തെങ്കിലും അഭിപ്രായങ്ങൾ നൽകാം." +
            "                        <b> രസീത് വൗച്ചർ </ b> <br>\n" +
            "                  \n" +
            "                        രസീതുകൾ / വരുമാനം രേഖപ്പെടുത്താൻ ഈ വൗച്ചർ ഉപയോഗിക്കുന്നു\n" +
            "                 \n" +
            "                       ആദ്യ മാസത്തെ നിലവിലെ എല്ലാ വൗച്ചറുകളും ആദ്യ സ്ക്രീൻ പ്രദർശിപ്പിക്കുന്നു.\n" +
            "                      ആവശ്യമായവ എഡിറ്റുചെയ്യാനോ ഇല്ലാതാക്കാനോ നമുക്ക് തിരഞ്ഞെടുക്കാം.\n" +
            "                   ഒരു പുതിയ വൗച്ചർ സൃഷ്ടിക്കാൻ + ബട്ടൺ സ്\u200Cപർശിക്കുക.\n" +
            "                     നിലവിലെ തീയതി പ്രദർശിപ്പിക്കും, ആവശ്യാനുസരണം മാറ്റാനും കഴിയും.\n" +
            "                 ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റിൽ നിന്ന് നിങ്ങൾക്ക് അക്കൗണ്ട് ഹെഡ് തിരഞ്ഞെടുക്കാം.\n" +
            "                      അതിനടുത്തുള്ള + ചിഹ്നം സ്\u200Cപർശിച്ചുകൊണ്ട് നിങ്ങൾക്ക് പുതിയൊരെണ്ണം സൃഷ്\u200Cടിക്കാൻ കഴിയും.\n" +
            "                     നൽകിയ തുക പണത്തിലാണോ ബാങ്ക് ഇടപാടുകളിലാണോ എന്ന് സൂചിപ്പിക്കുന്നതിന് നിങ്ങൾക്ക് തുകയുടെ അടുത്തുള്ള ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റ് ഉപയോഗിക്കാം.\n" +
            "                ബാങ്ക് തിരഞ്ഞെടുത്ത ശേഷം, ബാങ്ക് നാമം തിരഞ്ഞെടുക്കുന്നതിനുള്ള ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റ് ഉടനടി ദൃശ്യമാകും.\n" +
            "                      അതിനടുത്തുള്ള + ചിഹ്നത്തിൽ സ്പർശിച്ചുകൊണ്ട് നിങ്ങൾക്ക് ഒരു പുതിയ ബാങ്ക് അക്ക create ണ്ട് സൃഷ്ടിക്കാൻ കഴിയും.\n" +
            "                       അഭിപ്രായ രസത്തിൽ ഏതെങ്കിലും രസീതിനെക്കുറിച്ച് നിങ്ങൾക്ക് ചില അഭിപ്രായങ്ങൾ നൽകാം. <br>\n" +
            "                      \n" +
            "                        <b> ജേണൽ വൗച്ചർ </ b> <br>\n" +
            "                     \n" +
            "                      രണ്ട് അക്ക between ണ്ടുകൾ തമ്മിലുള്ള ഡെബിറ്റ് / ക്രെഡിറ്റ് ഇടപാടുകൾക്ക് ജേണൽ വൗച്ചറുകൾ ഉപയോഗിക്കുന്നു.\n" +
            "                      ഡെബിറ്റ് അക്കൗണ്ട് തിരഞ്ഞെടുത്ത ശേഷം, നിങ്ങൾക്ക് തുക നൽകി ക്രെഡിറ്റ് അക്കൗണ്ട് തിരഞ്ഞെടുക്കാം. ആവശ്യമെങ്കിൽ അഭിപ്രായം നൽകാം <b> ബാങ്ക് വൗച്ചർ </ b>\n" +
            "<br> ബാങ്കിൽ നിന്ന് പണ നിക്ഷേപങ്ങളും പിൻവലിക്കലുകളും രേഖപ്പെടുത്താൻ ഈ വൗച്ചർ ഉപയോഗിക്കുന്നു.\n" +
            "\n" +
            "അക്കൗണ്ട് സജ്ജീകരണത്തിലെ കാറ്റഗറി ബാങ്കിൽ ഉൾപ്പെടുത്തിയിരിക്കുന്ന അക്കൗണ്ടുകൾ ഡ്രോപ്പ്ഡൗൺ ലിസ്റ്റ് ഇവിടെ കാണിക്കുന്നു.\n" +
            "അതിനടുത്തുള്ള + ചിഹ്നം അമർത്തിക്കൊണ്ട് നിങ്ങൾക്ക് ഒരു പുതിയ അക്ക create ണ്ട് സൃഷ്ടിക്കാനും കഴിയും.\n" +
            "അതിനുശേഷം നിങ്ങൾ തുക നിക്ഷേപിക്കുകയാണോ അല്ലെങ്കിൽ പിൻവലിക്കുകയാണോ എന്ന് രേഖപ്പെടുത്തണം";



    public static String tel="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> ఖాతా సెటప్ </ b> <br>\n" +
            "                 \n" +
            "                   ఆర్థిక లావాదేవీల కోసం ఖాతా హెడ్\u200Cలను (లెడ్జర్లు) నిర్వహించడం కోసం ఖాతా సెటప్.\n" +
            "ఇది ఇప్పటికే ఉన్న అన్ని ఖాతా హెడ్\u200Cలను అక్షర క్రమంలో ప్రదర్శిస్తుంది మరియు ఏదైనా మార్పులు చేయడానికి మేము ప్రతిదాన్ని ఎంచుకోవచ్చు. + బటన్\u200Cను తాకడం ద్వారా మనం క్రొత్తదాన్ని కూడా సృష్టించవచ్చు.\n" +
            "మేము ఈ క్రింది వివరాలను నమోదు చేయవచ్చు:\n" +
            "ఖాతా పేరు - ఖాతా పేరు / లెడ్జర్ పేరును సేవ్ చేయడానికి ఈ ఫీల్డ్ ఉపయోగించబడుతుంది. ఈ ఫీల్డ్\u200Cలో నకిలీ మరియు ఖాళీ విలువలు అనుమతించబడవు.\n" +
            "ఖాతా వర్గం - డ్రాప్ డౌన్ జాబితా నుండి ఇచ్చిన ఖాతా యొక్క వర్గాన్ని ఎంచుకోవడానికి ఉపయోగిస్తారు.\n" +
            "ఓపెనింగ్ బ్యాలెన్స్ - అనువర్తనంతో ప్రారంభించేటప్పుడు ఆ ఖాతాలో ఓపెనింగ్ బ్యాలెన్స్ నమోదు చేయడానికి ఉపయోగిస్తారు. ఇది తప్పనిసరి కాదు.\n" +
            "రకం - ఎంటర్ చేసిన ఓపెనింగ్ బ్యాలెన్స్ డెబిట్ లేదా క్రెడిట్ అని పేర్కొనడానికి ఇది ఉపయోగించబడుతుంది. ఓపెనింగ్ బ్యాలెన్స్ లేకపోతే, దాన్ని ఎంచుకోవలసిన అవసరం లేదు." +
            "                        <b> చెల్లింపు వోచర్ </ b> <br>\n" +
            "                        \n" +
            "                      చెల్లింపు వోచర్\n" +
            "\n" +
            "ఈ వోచర్ రోజువారీ చెల్లింపులు / ఖర్చులను రికార్డ్ చేయడానికి ఉపయోగించబడుతుంది.\n" +
            "\n" +
            "మొదటి స్క్రీన్ ప్రస్తుత నెల యొక్క అన్ని వోచర్\u200Cలను ప్రదర్శిస్తుంది.\n" +
            "అవసరమైన వాటిని సవరించడానికి లేదా తొలగించడానికి మేము ఎంచుకోవచ్చు.\n" +
            "క్రొత్త వోచర్\u200Cను సృష్టించడానికి + బటన్\u200Cను తాకండి.\n" +
            "ప్రస్తుత తేదీ ప్రదర్శించబడుతుంది మరియు అవసరమైన విధంగా మార్చవచ్చు.\n" +
            "డ్రాప్\u200Cడౌన్ జాబితా నుండి మీరు ఖాతా హెడ్\u200Cను ఎంచుకోవచ్చు.\n" +
            "దాని ప్రక్కన ఉన్న + గుర్తును తాకడం ద్వారా మీరు క్రొత్తదాన్ని సృష్టించవచ్చు.\n" +
            "ఎంటర్ చేసిన మొత్తం నగదు లేదా బ్యాంక్ లావాదేవీలో ఉందో లేదో సూచించడానికి మీరు మొత్తం పక్కన ఉన్న డ్రాప్\u200Cడౌన్ జాబితాను ఉపయోగించవచ్చు.\n" +
            "బ్యాంక్ ఎంచుకున్న తర్వాత, బ్యాంక్ పేరును ఎంచుకోవడానికి డ్రాప్\u200Cడౌన్ జాబితా వెంటనే కనిపిస్తుంది.\n" +
            "దాని ప్రక్కన ఉన్న + గుర్తును తాకడం ద్వారా మీరు క్రొత్త బ్యాంక్ ఖాతాను సృష్టించవచ్చు.\n" +
            "వ్యాఖ్య యొక్క కాలమ్\u200Cలో చెల్లింపు గురించి మీరు కొన్ని వ్యాఖ్యలను నమోదు చేయవచ్చు." +
            "                        <b> రసీదు వోచర్ </ b> <br>\n" +
            "                  \n" +
            "                        ఈ రసీదు రసీదులు / ఆదాయాన్ని రికార్డ్ చేయడానికి ఉపయోగించబడుతుంది\n" +
            "                 \n" +
            "                       మొదటి స్క్రీన్ ప్రస్తుత నెలలో అన్ని వోచర్\u200Cలను ప్రదర్శిస్తుంది.\n" +
            "                      అవసరమైన వాటిని సవరించడానికి లేదా తొలగించడానికి మేము ఎంచుకోవచ్చు.\n" +
            "                   క్రొత్త వోచర్\u200Cను సృష్టించడానికి + బటన్\u200Cను తాకండి.\n" +
            "                     ప్రస్తుత తేదీ ప్రదర్శించబడుతుంది మరియు అవసరమైన విధంగా మార్చవచ్చు.\n" +
            "                 డ్రాప్\u200Cడౌన్ జాబితా నుండి మీరు ఖాతా హెడ్\u200Cను ఎంచుకోవచ్చు.\n" +
            "                      దాని ప్రక్కన ఉన్న + గుర్తును తాకడం ద్వారా మీరు క్రొత్తదాన్ని సృష్టించవచ్చు.\n" +
            "                     నమోదు చేసిన మొత్తం నగదు లేదా బ్యాంక్ లావాదేవీల్లో ఉందో లేదో సూచించడానికి మీరు మొత్తానికి ప్రక్కన ఉన్న డ్రాప్\u200Cడౌన్ జాబితాను ఉపయోగించవచ్చు.\n" +
            "                బ్యాంకును ఎంచుకున్న తరువాత, బ్యాంక్ పేరును ఎంచుకోవడానికి డ్రాప్\u200Cడౌన్ జాబితా వెంటనే కనిపిస్తుంది.\n" +
            "                      దాని ప్రక్కన ఉన్న + గుర్తును తాకడం ద్వారా మీరు క్రొత్త బ్యాంక్ ఖాతాను సృష్టించవచ్చు.\n" +
            "                       వ్యాఖ్య కాలమ్\u200Cలో ఏదైనా రశీదు గురించి మీరు కొన్ని వ్యాఖ్యలను నమోదు చేయవచ్చు. <br>\n" +
            "                      \n" +
            "                        <b> జర్నల్ వోచర్ </ b> <br>\n" +
            "                     \n" +
            "                      రెండు ఖాతాల మధ్య డెబిట్ / క్రెడిట్ లావాదేవీల కోసం జర్నల్ వోచర్లు ఉపయోగించబడతాయి.\n" +
            "                      డెబిట్ ఖాతాను ఎంచుకున్న తరువాత, మీరు మొత్తాన్ని నమోదు చేసి, ఆపై క్రెడిట్ ఖాతాను ఎంచుకోవచ్చు. అవసరమైతే వ్యాఖ్యను నమోదు చేయవచ్చు <b> బ్యాంక్ వోచర్ </ b>\n" +
            "<br> ఈ వోచర్ బ్యాంకు నుండి నగదు డిపాజిట్లు మరియు ఉపసంహరణలను రికార్డ్ చేయడానికి ఉపయోగించబడుతుంది.\n" +
            "\n" +
            "ఇక్కడ డ్రాప్\u200Cడౌన్ జాబితా ఖాతా సెటప్\u200Cలో కేటగిరీ బ్యాంక్\u200Cలో చేర్చబడిన ఖాతాలను చూపుతుంది.\n" +
            "దాని ప్రక్కన ఉన్న + గుర్తును నొక్కడం ద్వారా మీరు క్రొత్త ఖాతాను కూడా సృష్టించవచ్చు.\n" +
            "ఆ తరువాత మీరు డిపాజిట్ చేస్తున్నారా లేదా ఉపసంహరించుకుంటున్నారా లేదా మొత్తాన్ని నమోదు చేస్తున్నారా అని రికార్డ్ చేయాలి";

    public static String tamil="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> கணக்கு அமைவு </ b> <br>\n" +
            "                 \n" +
            "                   கணக்கு அமைவு என்பது நிதி பரிவர்த்தனைகளுக்கான கணக்குத் தலைவர்களை (லெட்ஜர்கள்) நிர்வகிப்பதாகும்.\n" +
            "இது ஏற்கனவே உள்ள அனைத்து கணக்குத் தலைகளையும் அகர வரிசைப்படி காண்பிக்கும், மேலும் எந்த மாற்றங்களையும் செய்ய ஒவ்வொன்றையும் தேர்ந்தெடுக்கலாம். + பொத்தானைத் தொடுவதன் மூலமும் புதிய ஒன்றை உருவாக்கலாம்.\n" +
            "பின்வரும் விவரங்களை நாம் உள்ளிடலாம்:\n" +
            "கணக்கு பெயர் - கணக்கு பெயர் / லெட்ஜர் பெயரை சேமிக்க இந்த புலம் பயன்படுத்தப்படுகிறது. இந்த துறையில் நகல் மற்றும் வெற்று மதிப்புகள் அனுமதிக்கப்படாது.\n" +
            "கணக்கு வகை - கீழ்தோன்றும் பட்டியலில் இருந்து கொடுக்கப்பட்ட கணக்கின் வகையைத் தேர்ந்தெடுக்கப் பயன்படுகிறது.\n" +
            "திறப்பு இருப்பு - பயன்பாட்டைத் தொடங்கும்போது அந்தக் கணக்கில் தொடக்க இருப்பை உள்ளிடப் பயன்படுகிறது. இது கட்டாயமில்லை.\n" +
            "வகை - இது டெபிட் அல்லது கிரெடிட் என்பதை உள்ளிடப்பட்ட தொடக்க இருப்பைக் குறிப்பிட பயன்படுகிறது. தொடக்க இருப்பு இல்லை என்றால், அதைத் தேர்ந்தெடுக்க வேண்டிய அவசியமில்லை." +
            "                        <b> கட்டண வவுச்சர் </ b> <br>\n" +
            "                        \n" +
            "                      கட்டண வவுச்சர்\n" +
            "\n" +
            "இந்த வவுச்சர் தினசரி கொடுப்பனவுகள் / செலவுகளை பதிவு செய்ய பயன்படுத்தப்படுகிறது.\n" +
            "\n" +
            "முதல் திரை நடப்பு மாதத்தின் அனைத்து வவுச்சர்களையும் காட்டுகிறது.\n" +
            "தேவையானவற்றைத் திருத்த அல்லது நீக்க நாம் தேர்வு செய்யலாம்.\n" +
            "புதிய வவுச்சரை உருவாக்க + பொத்தானைத் தொடவும்.\n" +
            "தற்போதைய தேதி காண்பிக்கப்படும், மேலும் தேவைக்கேற்ப அதை மாற்றலாம்.\n" +
            "கீழ்தோன்றும் பட்டியலில் இருந்து கணக்குத் தலைவரை நீங்கள் தேர்ந்தெடுக்கலாம்.\n" +
            "அதற்கு அடுத்துள்ள + அடையாளத்தைத் தொட்டு புதிய ஒன்றை உருவாக்கலாம்.\n" +
            "உள்ளிடப்பட்ட தொகை ரொக்கமா அல்லது வங்கி பரிவர்த்தனையில் உள்ளதா என்பதைக் குறிக்க நீங்கள் தொகைக்கு அடுத்த கீழ்தோன்றும் பட்டியலைப் பயன்படுத்தலாம்.\n" +
            "வங்கி தேர்ந்தெடுக்கப்பட்டதும், வங்கியின் பெயரைத் தேர்ந்தெடுப்பதற்கான கீழ்தோன்றும் பட்டியல் உடனடியாக தோன்றும்.\n" +
            "அதற்கு அடுத்த + அடையாளத்தைத் தொட்டு புதிய வங்கிக் கணக்கை உருவாக்கலாம்.\n" +
            "குறிப்பின் பத்தியில் பணம் செலுத்துவது குறித்து ஏதேனும் கருத்துகளை நீங்கள் உள்ளிடலாம்." +
            "                        <b> ரசீது வவுச்சர் </ b> <br>\n" +
            "                  \n" +
            "                        ரசீதுகள் / வருமானத்தை பதிவு செய்ய இந்த வவுச்சர் பயன்படுத்தப்படுகிறது\n" +
            "                 \n" +
            "                       முதல் திரை நடப்பு மாதத்திற்கான அனைத்து வவுச்சர்களையும் காட்டுகிறது.\n" +
            "                      தேவையானவற்றைத் திருத்த அல்லது நீக்க நாம் தேர்வு செய்யலாம்.\n" +
            "                   புதிய வவுச்சரை உருவாக்க + பொத்தானைத் தொடவும்.\n" +
            "                     தற்போதைய தேதி காண்பிக்கப்படும் மற்றும் தேவைக்கேற்ப மாற்றலாம்.\n" +
            "                 கீழ்தோன்றும் பட்டியலில் இருந்து கணக்குத் தலைவரைத் தேர்ந்தெடுக்கலாம்.\n" +
            "                      அதற்கு அடுத்துள்ள + அடையாளத்தைத் தொட்டு புதிய ஒன்றை உருவாக்கலாம்.\n" +
            "                     உள்ளிடப்பட்ட தொகை ரொக்கமா அல்லது வங்கி பரிவர்த்தனைகளில் உள்ளதா என்பதைக் குறிக்க நீங்கள் தொகைக்கு அடுத்த கீழ்தோன்றும் பட்டியலைப் பயன்படுத்தலாம்.\n" +
            "                வங்கியைத் தேர்ந்தெடுத்த பிறகு, வங்கியின் பெயரைத் தேர்ந்தெடுப்பதற்கான கீழ்தோன்றும் பட்டியல் உடனடியாக தோன்றும்.\n" +
            "                      அதற்கு அடுத்த + அடையாளத்தைத் தொட்டு புதிய வங்கிக் கணக்கை உருவாக்கலாம்.\n" +
            "                       கருத்து ரசீதில் எந்த ரசீது பற்றியும் சில கருத்துகளை உள்ளிடலாம். <br>\n" +
            "                      \n" +
            "                        <b> ஜர்னல் வவுச்சர் </ b> <br>\n" +
            "                     \n" +
            "                      இரண்டு கணக்குகளுக்கு இடையிலான பற்று / கடன் பரிவர்த்தனைகளுக்கு ஜர்னல் வவுச்சர்கள் பயன்படுத்தப்படுகின்றன.\n" +
            "                      டெபிட் கணக்கைத் தேர்ந்தெடுத்த பிறகு, நீங்கள் தொகையை உள்ளிட்டு கடன் கணக்கைத் தேர்ந்தெடுக்கலாம். தேவைப்பட்டால் கருத்து உள்ளிடலாம் <b> வங்கி வவுச்சர் </ b>\n" +
            "<br> இந்த வவுச்சர் வங்கியில் இருந்து பண வைப்பு மற்றும் திரும்பப் பெறுதல் ஆகியவற்றை பதிவு செய்ய பயன்படுத்தப்படுகிறது.\n" +
            "\n" +
            "இங்கே கீழ்தோன்றும் பட்டியல் கணக்கு அமைப்பில் வகை வங்கியில் சேர்க்கப்பட்டுள்ள கணக்குகளைக் காட்டுகிறது.\n" +
            "அதற்கு அடுத்த + அடையாளத்தை அழுத்துவதன் மூலமும் புதிய கணக்கை உருவாக்கலாம்.\n" +
            "அதன்பிறகு நீங்கள் டெபாசிட் செய்கிறீர்களா அல்லது திரும்பப் பெறுகிறீர்களா மற்றும் தொகையை உள்ளிடுகிறீர்களா என்பதை பதிவு செய்ய வேண்டும்";


    public static String kn="<html> <body> <p align = \"justify\"> <br> \"\n" +
            "                     \n" +
            "                        <b> ಖಾತೆ ಸೆಟಪ್ </ b> <br>\n" +
            "                 \n" +
            "                  ಹಣಕಾಸಿನ ವಹಿವಾಟುಗಳಿಗಾಗಿ ಖಾತೆ ಮುಖ್ಯಸ್ಥರನ್ನು (ಲೆಡ್ಜರ್\u200Cಗಳನ್ನು) ನಿರ್ವಹಿಸುವುದಕ್ಕಾಗಿ ಖಾತೆ ಸೆಟಪ್ ಆಗಿದೆ.\n" +
            "ಇದು ಅಸ್ತಿತ್ವದಲ್ಲಿರುವ ಎಲ್ಲಾ ಖಾತೆ ಮುಖ್ಯಸ್ಥರನ್ನು ವರ್ಣಮಾಲೆಯಂತೆ ಪ್ರದರ್ಶಿಸುತ್ತದೆ ಮತ್ತು ಯಾವುದೇ ಮಾರ್ಪಾಡುಗಳನ್ನು ಮಾಡಲು ನಾವು ಪ್ರತಿಯೊಂದನ್ನು ಆಯ್ಕೆ ಮಾಡಬಹುದು. + ಬಟನ್ ಸ್ಪರ್ಶಿಸುವ ಮೂಲಕ ನಾವು ಹೊಸದನ್ನು ಸಹ ರಚಿಸಬಹುದು.\n" +
            "ನಾವು ಈ ಕೆಳಗಿನ ವಿವರಗಳನ್ನು ನಮೂದಿಸಬಹುದು:\n" +
            "ಖಾತೆಯ ಹೆಸರು - ಖಾತೆಯ ಹೆಸರು / ಲೆಡ್ಜರ್ ಹೆಸರನ್ನು ಉಳಿಸಲು ಈ ಕ್ಷೇತ್ರವನ್ನು ಬಳಸಲಾಗುತ್ತದೆ. ಈ ಕ್ಷೇತ್ರದಲ್ಲಿ ನಕಲಿ ಮತ್ತು ಖಾಲಿ ಮೌಲ್ಯಗಳನ್ನು ಅನುಮತಿಸಲಾಗುವುದಿಲ್ಲ.\n" +
            "ಖಾತೆ ವರ್ಗ - ಡ್ರಾಪ್ ಡೌನ್ ಪಟ್ಟಿಯಿಂದ ನಿರ್ದಿಷ್ಟ ಖಾತೆಯ ವರ್ಗವನ್ನು ಆಯ್ಕೆ ಮಾಡಲು ಬಳಸಲಾಗುತ್ತದೆ.\n" +
            "ಓಪನಿಂಗ್ ಬ್ಯಾಲೆನ್ಸ್ - ಅಪ್ಲಿಕೇಶನ್\u200Cನೊಂದಿಗೆ ಪ್ರಾರಂಭಿಸುವಾಗ ಆ ಖಾತೆಯಲ್ಲಿ ಆರಂಭಿಕ ಬ್ಯಾಲೆನ್ಸ್ ಅನ್ನು ನಮೂದಿಸಲು ಬಳಸಲಾಗುತ್ತದೆ. ಇದು ಕಡ್ಡಾಯವಲ್ಲ.\n" +
            "ಕೌಟುಂಬಿಕತೆ - ನಮೂದಿಸಿದ ಆರಂಭಿಕ ಸಮತೋಲನವನ್ನು ಡೆಬಿಟ್ ಅಥವಾ ಕ್ರೆಡಿಟ್ ಎಂದು ನಮೂದಿಸಲು ಇದನ್ನು ಬಳಸಲಾಗುತ್ತದೆ. ಆರಂಭಿಕ ಸಮತೋಲನ ಇಲ್ಲದಿದ್ದರೆ, ಅದನ್ನು ಆಯ್ಕೆ ಮಾಡುವ ಅಗತ್ಯವಿಲ್ಲ." +
            "                        <b> ಪಾವತಿ ಚೀಟಿ </ b> <br>\n" +
            "                        \n" +
            "                      ಪಾವತಿ ಚೀಟಿ\n" +
            "\n" +
            "ಈ ಚೀಟಿಯನ್ನು ದೈನಂದಿನ ಪಾವತಿ / ವೆಚ್ಚಗಳನ್ನು ದಾಖಲಿಸಲು ಬಳಸಲಾಗುತ್ತದೆ.\n" +
            "\n" +
            "ಮೊದಲ ಪರದೆಯು ಪ್ರಸ್ತುತ ತಿಂಗಳ ಎಲ್ಲಾ ಚೀಟಿಗಳನ್ನು ಪ್ರದರ್ಶಿಸುತ್ತದೆ.\n" +
            "ಅಗತ್ಯವಿರುವದನ್ನು ಸಂಪಾದಿಸಲು ಅಥವಾ ಅಳಿಸಲು ನಾವು ಆಯ್ಕೆ ಮಾಡಬಹುದು.\n" +
            "ಹೊಸ ಚೀಟಿ ರಚಿಸಲು + ಬಟನ್ ಸ್ಪರ್ಶಿಸಿ.\n" +
            "ಪ್ರಸ್ತುತ ದಿನಾಂಕವನ್ನು ಪ್ರದರ್ಶಿಸಲಾಗುತ್ತದೆ ಮತ್ತು ಅದನ್ನು ಅಗತ್ಯವಿರುವಂತೆ ಬದಲಾಯಿಸಬಹುದು.\n" +
            "ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿಯಿಂದ ನೀವು ಖಾತೆ ಮುಖ್ಯಸ್ಥರನ್ನು ಆಯ್ಕೆ ಮಾಡಬಹುದು.\n" +
            "ಅದರ ಪಕ್ಕದಲ್ಲಿರುವ + ಚಿಹ್ನೆಯನ್ನು ಸ್ಪರ್ಶಿಸುವ ಮೂಲಕ ನೀವು ಹೊಸದನ್ನು ರಚಿಸಬಹುದು.\n" +
            "ನಮೂದಿಸಿದ ಮೊತ್ತವು ನಗದು ಅಥವಾ ಬ್ಯಾಂಕ್ ವಹಿವಾಟಿನಲ್ಲಿದೆ ಎಂದು ಸೂಚಿಸಲು ನೀವು ಮೊತ್ತದ ಪಕ್ಕದಲ್ಲಿರುವ ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿಯನ್ನು ಬಳಸಬಹುದು.\n" +
            "ಬ್ಯಾಂಕ್ ಆಯ್ಕೆ ಮಾಡಿದ ನಂತರ, ಬ್ಯಾಂಕ್ ಹೆಸರನ್ನು ಆಯ್ಕೆ ಮಾಡಲು ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿ ತಕ್ಷಣ ಕಾಣಿಸುತ್ತದೆ.\n" +
            "ಅದರ ಪಕ್ಕದಲ್ಲಿರುವ + ಚಿಹ್ನೆಯನ್ನು ಸ್ಪರ್ಶಿಸುವ ಮೂಲಕ ನೀವು ಹೊಸ ಬ್ಯಾಂಕ್ ಖಾತೆಯನ್ನು ರಚಿಸಬಹುದು.\n" +
            "ಟಿಪ್ಪಣಿಯ ಅಂಕಣದಲ್ಲಿ ಪಾವತಿ ಕುರಿತು ನೀವು ಕೆಲವು ಟೀಕೆಗಳನ್ನು ನಮೂದಿಸಬಹುದು." +
            "                 \n" +
            "                        <b> ರಶೀದಿ ಚೀಟಿ </ b> <br>\n" +
            "                  \n" +
            "                        ರಶೀದಿ / ಆದಾಯವನ್ನು ದಾಖಲಿಸಲು ಈ ಚೀಟಿಯನ್ನು ಬಳಸಲಾಗುತ್ತದೆ\n" +
            "                 \n" +
            "                       ಮೊದಲ ಪರದೆಯು ಪ್ರಸ್ತುತ ತಿಂಗಳ ಎಲ್ಲಾ ಚೀಟಿಗಳನ್ನು ಪ್ರದರ್ಶಿಸುತ್ತದೆ.\n" +
            "                      ಅಗತ್ಯವಿರುವದನ್ನು ಸಂಪಾದಿಸಲು ಅಥವಾ ಅಳಿಸಲು ನಾವು ಆಯ್ಕೆ ಮಾಡಬಹುದು.\n" +
            "                   ಹೊಸ ಚೀಟಿ ರಚಿಸಲು + ಬಟನ್ ಸ್ಪರ್ಶಿಸಿ.\n" +
            "                     ಪ್ರಸ್ತುತ ದಿನಾಂಕವನ್ನು ಪ್ರದರ್ಶಿಸಲಾಗುತ್ತದೆ ಮತ್ತು ಅಗತ್ಯವಿರುವಂತೆ ಬದಲಾಯಿಸಬಹುದು.\n" +
            "                 ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿಯಿಂದ ನೀವು ಖಾತೆ ಮುಖ್ಯಸ್ಥರನ್ನು ಆಯ್ಕೆ ಮಾಡಬಹುದು.\n" +
            "                      ಅದರ ಪಕ್ಕದಲ್ಲಿರುವ + ಚಿಹ್ನೆಯನ್ನು ಸ್ಪರ್ಶಿಸುವ ಮೂಲಕ ನೀವು ಹೊಸದನ್ನು ರಚಿಸಬಹುದು.\n" +
            "                     ನಮೂದಿಸಿದ ಮೊತ್ತವು ನಗದು ಅಥವಾ ಬ್ಯಾಂಕ್ ವಹಿವಾಟಿನಲ್ಲಿದೆ ಎಂದು ಸೂಚಿಸಲು ನೀವು ಮೊತ್ತದ ಪಕ್ಕದಲ್ಲಿರುವ ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿಯನ್ನು ಬಳಸಬಹುದು.\n" +
            "                ಬ್ಯಾಂಕ್ ಆಯ್ಕೆ ಮಾಡಿದ ನಂತರ, ಬ್ಯಾಂಕ್ ಹೆಸರನ್ನು ಆಯ್ಕೆ ಮಾಡಲು ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿ ತಕ್ಷಣ ಕಾಣಿಸುತ್ತದೆ.\n" +
            "                      ಅದರ ಪಕ್ಕದಲ್ಲಿರುವ + ಚಿಹ್ನೆಯನ್ನು ಸ್ಪರ್ಶಿಸುವ ಮೂಲಕ ನೀವು ಹೊಸ ಬ್ಯಾಂಕ್ ಖಾತೆಯನ್ನು ರಚಿಸಬಹುದು.\n" +
            "                       ಕಾಮೆಂಟ್ ಅಂಕಣದಲ್ಲಿ ಯಾವುದೇ ರಶೀದಿಯ ಬಗ್ಗೆ ನೀವು ಕೆಲವು ಕಾಮೆಂಟ್\u200Cಗಳನ್ನು ನಮೂದಿಸಬಹುದು. <br>\n" +
            "                      \n" +
            "                        <b> ಜರ್ನಲ್ ಚೀಟಿ </ b> <br>\n" +
            "                     \n" +
            "                      ಎರಡು ಖಾತೆಗಳ ನಡುವಿನ ಡೆಬಿಟ್ / ಕ್ರೆಡಿಟ್ ವ್ಯವಹಾರಗಳಿಗೆ ಜರ್ನಲ್ ಚೀಟಿಗಳನ್ನು ಬಳಸಲಾಗುತ್ತದೆ.\n" +
            "                      ಡೆಬಿಟ್ ಖಾತೆಯನ್ನು ಆಯ್ಕೆ ಮಾಡಿದ ನಂತರ, ನೀವು ಮೊತ್ತವನ್ನು ನಮೂದಿಸಬಹುದು ಮತ್ತು ನಂತರ ಕ್ರೆಡಿಟ್ ಖಾತೆಯನ್ನು ಆಯ್ಕೆ ಮಾಡಬಹುದು. ಅಗತ್ಯವಿದ್ದರೆ ಕಾಮೆಂಟ್ ಅನ್ನು ನಮೂದಿಸಬಹುದು <b> ಬ್ಯಾಂಕ್ ಚೀಟಿ </ b>\n" +
            "<br> ಈ ಚೀಟಿಯನ್ನು ಬ್ಯಾಂಕಿನಿಂದ ನಗದು ಠೇವಣಿ ಮತ್ತು ಹಿಂಪಡೆಯುವಿಕೆಯನ್ನು ದಾಖಲಿಸಲು ಬಳಸಲಾಗುತ್ತದೆ.\n" +
            "\n" +
            "ಖಾತೆ ಸೆಟಪ್\u200Cನಲ್ಲಿ ವರ್ಗ ಬ್ಯಾಂಕಿನಲ್ಲಿ ಸೇರಿಸಲಾದ ಖಾತೆಗಳನ್ನು ಇಲ್ಲಿ ಡ್ರಾಪ್\u200Cಡೌನ್ ಪಟ್ಟಿ ತೋರಿಸುತ್ತದೆ.\n" +
            "ಅದರ ಪಕ್ಕದಲ್ಲಿರುವ + ಚಿಹ್ನೆಯನ್ನು ಒತ್ತುವ ಮೂಲಕ ನೀವು ಹೊಸ ಖಾತೆಯನ್ನು ಸಹ ರಚಿಸಬಹುದು.\n" +
            "ಅದರ ನಂತರ ನೀವು ಮೊತ್ತವನ್ನು ಠೇವಣಿ ಮಾಡುತ್ತಿದ್ದೀರಾ ಅಥವಾ ಹಿಂತೆಗೆದುಕೊಳ್ಳುತ್ತಿದ್ದೀರಾ ಎಂದು ದಾಖಲಿಸಬೇಕು";

    public static String marathi="<html> <body> <p संरेखित करा \"justify \"> <br>\"\n" +
            "                     \n" +
            "                        <b> खाते सेटअप </ b> <br>\n" +
            "                 \n" +
            "                    खाते व्यवहारासाठी खाते प्रमुख (लेजर) व्यवस्थापित करण्यासाठी खाते सेटअप आहे.\n" +
            "हे सर्व विद्यमान खाते प्रमुखांना वर्णक्रमानुसार प्रदर्शित करेल आणि आम्ही कोणतीही बदल करण्यासाठी प्रत्येकजण निवडू शकतो. + बटणावर स्पर्श करून आम्ही एक नवीन तयार करू शकतो.\n" +
            "आम्ही खालील तपशील प्रविष्ट करू शकता:\n" +
            "खात्याचे नाव - हे फील्ड खाते / लेजरचे नाव सेव्ह करण्यासाठी वापरले जाते. या क्षेत्रात डुप्लिकेट आणि रिक्त मूल्ये अनुमत नाहीत.\n" +
            "खाते वर्ग - दिलेल्या खात्याची श्रेणी ड्रॉप डाऊन सूचीतून निवडण्यासाठी वापरली जाते.\n" +
            "उघडणे शिल्लक - अ\u200Dॅपसह प्रारंभ करताना त्या खात्यात प्रारंभिक शिल्लक प्रविष्ट करण्यासाठी वापरले जाते. हे अनिवार्य नाही.\n" +
            "प्रकार - प्रवेश केलेल्या ओपनिंग शिल्लक ते डेबिट किंवा क्रेडिट असो याचा उल्लेख करण्यासाठी याचा उपयोग केला जातो. जर उद्घाटन शिल्लक नसेल तर ते निवडण्याची आवश्यकता नाही." +
            "                        <b> पेमेंट व्हाउचर </ b> <br>\n" +
            "                        \n" +
            "                      पेमेंट व्हाउचर\n" +
            "\n" +
            "हे व्हाउचर दररोज देयके / खर्च नोंदविण्यासाठी वापरला जातो.\n" +
            "\n" +
            "प्रथम स्क्रीन चालू महिन्यातील सर्व व्हाउचर प्रदर्शित करते.\n" +
            "आम्ही आवश्यक संपादन करणे किंवा हटविणे निवडू शकतो.\n" +
            "नवीन व्हाउचर तयार करण्यासाठी + बटणावर स्पर्श करा.\n" +
            "वर्तमान तारीख प्रदर्शित केली जाईल आणि आवश्यकतेनुसार ती बदलली जाऊ शकते.\n" +
            "आपण ड्रॉपडाउन सूचीमधून खाते प्रमुख निवडू शकता.\n" +
            "पुढील + चिन्हास स्पर्श करून आपण एक नवीन तयार करू शकता.\n" +
            "आपण दिलेली रक्कम रोख किंवा बँक व्यवहारात आहे की नाही हे दर्शविण्यासाठी आपण पुढील ड्रॉपडाऊन सूची वापरू शकता.\n" +
            "एकदा बँक निवडल्यानंतर, बँकेचे नाव निवडण्यासाठी ड्रॉपडाउन सूची त्वरित दिसून येईल.\n" +
            "पुढील + चिन्हास स्पर्श करून आपण नवीन बँक खाते तयार करू शकता.\n" +
            "आपण टिप्पणीच्या स्तंभातील देयकाबद्दल काही शेरा, काही असल्यास प्रविष्ट करू शकता." +
            "                        <b> पावती व्हाउचर </ b> <br>\n" +
            "                  \n" +
            "                        या व्हाउचरचा उपयोग पावत्या / उत्पन्नाची नोंद करण्यासाठी केला जातो\n" +
            "                 \n" +
            "                       प्रथम स्क्रीन चालू महिन्यासाठी सर्व व्हाउचर प्रदर्शित करते.\n" +
            "                      आम्ही आवश्यक संपादन करणे किंवा हटविणे निवडू शकतो.\n" +
            "                   नवीन व्हाउचर तयार करण्यासाठी + बटणावर स्पर्श करा.\n" +
            "                     वर्तमान तारीख प्रदर्शित केली जाईल आणि आवश्यकतेनुसार ते बदलले जाऊ शकतात.\n" +
            "                 आपण ड्रॉपडाउन सूचीमधून खाते प्रमुख निवडू शकता.\n" +
            "                      पुढील + चिन्हास स्पर्श करून आपण एक नवीन तयार करू शकता.\n" +
            "                     आपण दिलेली रक्कम रोख किंवा बँक व्यवहारात आहे की नाही हे दर्शविण्यासाठी आपण पुढील ड्रॉपडाऊन सूची वापरू शकता.\n" +
            "                बँक निवडल्यानंतर, बँकेचे नाव निवडण्यासाठी ड्रॉपडाउन सूची त्वरित दिसून येईल.\n" +
            "                      पुढील + चिन्हास स्पर्श करून आपण नवीन बँक खाते तयार करू शकता.\n" +
            "                       आपण टिप्पणी स्तंभात कोणत्याही पावतीबद्दल काही टिप्पण्या प्रविष्ट करू शकता. <br>\n" +
            "                      \n" +
            "                        <b> जर्नल व्हाउचर </ b> <br>\n" +
            "                     \n" +
            "                      जर्नल व्हाउचर दोन खात्यांमधील डेबिट / क्रेडिट व्यवहारांसाठी वापरले जातात.\n" +
            "                      डेबिट खाते निवडल्यानंतर आपण रक्कम प्रविष्ट करू शकता आणि नंतर क्रेडिट खाते निवडू शकता. <b> बँक व्हाउचर </ b> आवश्यक असल्यास टिप्पणी प्रविष्ट केली जाऊ शकते\n" +
            "<br> या व्हाउचरचा उपयोग रोख ठेव आणि बँकेतून पैसे काढण्याच्या रेकॉर्डसाठी केला जातो.\n" +
            "\n" +
            "येथे ड्रॉपडाउन यादी खाते सेटअपमध्ये श्रेणी बँकेत समाविष्ट खाती दर्शवते.\n" +
            "त्यापुढील + चिन्ह दाबून आपण नवीन खाते देखील तयार करू शकता.\n" +
            "त्यानंतर आपण रक्कम जमा करत आहात की काढत आहात आणि प्रविष्ट करीत आहात याची नोंद घ्यावी लागेल";
}
