package com.centroid.integraaccounts.Constants;

import android.accounts.Account;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.User;
import com.centroid.integraaccounts.data.domain.UserData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.messageservice.DateRemindService;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.views.AccountSettingsListActivity;
import com.centroid.integraaccounts.views.IncomexpenditureActivity;
import com.centroid.integraaccounts.views.ProfileActivity;
import com.google.android.material.snackbar.Snackbar;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils  {


    public static int  arrimgs[]={R.drawable.cone,R.drawable.ctwo,R.drawable.cthree,R.drawable.cfour,R.drawable.cfive,R.drawable.csix,R.drawable.cseven,R.drawable.ceight};

    public static int mymoneydatas[]={R.drawable.ic_payment,R.drawable.ic_receipt_black,R.drawable.ic_account_balance_wallet,R.drawable.ic_business_center_,R.drawable.ic_bank_building,R.drawable.ic_journal,0,0,0,R.drawable.ic_bill,R.drawable.ic_monetization,R.drawable.ic_accsettingss};
    public static String mymoneydetails[]={"Payments","Receipts","Wallet","Budget","Bank","Journal","","","","Billing","Cash / Bank Statement","Account Settings"};


    public static String mybelongings[]={"Assets","Liabilities","Insurances","Investments","Passwords","Documents"};

    public static int mybelongingsimg[]={R.drawable.ic_assets,R.drawable.ic_clipboard,R.drawable.ic_life_insurance,R.drawable.ic_profits,R.drawable.ic_password,R.drawable.ic_docsettings};

    public static String mylifes[]={"Task","Diary","Dream"};

    public static int mylifesimg[]={R.drawable.ic_assignment,R.drawable.ic_journal,R.drawable.ic_target};


    public static String myutilities[]={"Mobile Recharge","DTH Recharge","Visiting Card","Website Links","Emergency Numbers"};

    public static int utilitiesimg[]={R.drawable.ic_baseline_phone_recharge,R.drawable.satelitedish,R.drawable.idcard,R.drawable.link,R.drawable.alarm};



    public static String emergency_utilities[]={"National Emergency Number","Police","Fire","Ambulance","Disaster Management Services","Women Helpline","Women Helpline(Domestic Abuse)"};

    public static String emergency_utilitiesphone[]={"112","100","101","102","108","1091","181"};







    public static String whatsapp="919846290789";

    public static String billvoucherkey="Billvoucherkeyid";

    public static String merchantkey="merchantkey";

    public static String Needversionupdate="versionupdate";

    public static User usr=null;


    public static String target_first_key="target_first_key";

    public static String target_second_key="target_second_key";


    public static int  arr_target_iconimgs[]={R.drawable.car,R.drawable.home,R.drawable.education,R.drawable.emergency,R.drawable.healthcare,R.drawable.party,R.drawable.charity};

    public static String arr_target[]={"Vehicle","New home","Education","Emergency","Healthcare","Party","Charity"};



    public static class CCAvenueCredentials{

        public static String merchant_id="1858814";
        public static String access_code="AVBF79JL71CC24FBCC";
        public static String workingkey="A8B001DF332075173DA7425F3FEBCB1B";


        public static String jsonurl="https://secure.ccavenue.com/transaction/transaction.do";




    }







    public static String getTimestamp()
    {
        Calendar calendar=Calendar.getInstance();
        long ts=calendar.getTimeInMillis();
        String s=ts+"";
        return s;
    }


    public static class ServerMessage{

        public static String sender="CGSAVE";
        public static String forgotpasstemplateid="1007856104698741987";
        public static String registrationtemplateid="1007625690429475781";
        public static String registration_Confirm_templateid="1007134283594642980";

        public static String route="2";

        public static String type="1";

        public static String apikey="bf25917c3254cfe9f50694f24884f23a";

    }


    public static class ServerMessageType{

        public static int forgot_password=1;

        public static int registration_Confirm_password=2;


        public static int registration=3;
    }

    public static String buildServerMessage(int Type, String otp,String username,String password)
    {
        String message="";

        if(Type==ServerMessageType.forgot_password)
        {

            message="Your OTP for forgot Password is "+otp+" .CGSAVE";
        }

        if(Type==ServerMessageType.registration_Confirm_password)
        {

            message="Your registration is successful. Your Registration ID "+username+" Passowrd "+password+" .CGSAVE";
        }

        if(Type==ServerMessageType.registration)
        {

            message="Dear "+username+" Welcome to SAVE App - My Personal App. Your OTP is "+otp+" CGSAVE";
        }


        return message;
    }


    public static String getBase64Password(String text)
    {
        byte[] data = new byte[0];
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }

    public static String decodeBase64Password(String text)
    {
        String tex ="";
        byte[] data = Base64.decode(text,Base64.DEFAULT);
       try{
         tex = new String(data,"UTF-8");
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
        return tex;
    }


    public static class Messages{

        public static String warning="Your application's trial period will expired within a few days";

        public static String error="Your application's trial period is expired";


        public static String expirywarning="Please renew your application.It will expired within a few days";

        public static String expiryerror="Sorry , please renew the application";
    }



    public static String expirycompleted="EXPIRYCOMPLETED";
    public static String expirywarning="EXPIRYWARNING";

    public static String firstuser="FIRSTUSER";

    public static String firstpassword="FIRSTPassword";


    public static String trialcompleted="TRIALCOMPLETED";
    public static String trialwarning="TRIALWARNING";

    public static String applockedkey="APPLOCKEDKEY";


    public static String userkey="userkey";

    public static String userLoginFirst="userLoginFirst";

    public static String AccSettingsFirst="AccSettingsFirst";
    public static String VisitcardFirst="VisitcardFirst";
    public static String EmergencyFirst="EmergencyFirst";
    public static String firstloginedtime="firstloginedtime";
    public static String databackuptime="databackuptime";

    public static String backuptoken="BACKUPTOKEN";



    public static   String smsbaseurl="http://eapoluenterprise.in/httpapi/";


    public static String showTutorial="SHOWTUTORIAL";








    public static   String baseurl="https://mysaving.in/IntegraAccount/api/";

    public static   String rechargebaseurl="https://mysaveapp.com/rechargeAPI/newrecharge/mobilePlans.php?";

    public static   String imgbaseurl="https://mysaving.in/uploads/profile/";

    public static   String backupbaseurl="https://mysaving.in/IntegraAccount/backups/";

    public static String domain="https://mysaving.in/";

    public static String newdomain="http://mysaveapp.com/";

     public static String linkimg="https://mysaving.in/images/";

    public static String sliderimageurl="https://mysaving.in/images/";









//    public static   String baseurl="https://centroidsolutions.in/IntegraAccount/api/";
//
//    public static   String imgbaseurl="https://centroidsolutions.in/IntegraAccount/images/";
//
//    public static   String backupbaseurl="https://centroidsolutions.in/IntegraAccount/backups/";
//
//    public static String domain="https://centroidsolutions.in/";
//
//    public static String linkimg="https://centroidsolutions.in/images/";
//
//    public static String sliderimageurl="https://centroidsolutions.in/IntegraAccount/sliderimages/";




    public static String razorpayBaseUrl="https://api.razorpay.com/";

    public static String razorkeyid="rzp_test_znHSTgNEqEyAID";

    public static String razorSecretkey="lvhUGQCw5xOkb7w70DKHkVOd";



    public static class Tutorials{

        public static String paymentvouchertutorial="paymentvouchertutorial";
        public static String reciptvouchertutorial="reciptvouchertutorial";
        public static String journalvouchertutorial="journalvouchertutorial";
        public static String accsetuptutorial="accsetuptutorial";
        public static String bankvouchertutorial="bankvouchertutorial";
        public static String billingtutorial="billingtutorial";
        public static String wallettutorial="wallettutorial";
        public static String cashbankbalancetutorial="cashbankbalancetutorial";
        public static String assettutorial="assettutorial";
        public static String liabilitytutorial="liabilitytutorial";
        public static String insurancetutorial="insurancetutorial";
        public static String investmenttutorial="investmenttutorial";
        public static String mydiarytutorial="mydiarytutorial";
        public static String budgettutorial="budgettutorial";
        public static String transactiontutorial="transactiontutorial";
        public static String ledgertutorial="ledgertutorial";
        public static String cashbalanceledgertutorial="cashbalanceledgertutorial";
        public static String incomexptutorial="incomexptutorial";
        public static String listofmyassetstutorial="listofmyassetstutorial";
        public static String listofliabilitiestutorial="listofliabilitiestutorial";
        public static String listofmyinsurancetutorial="listofmyinsurancetutorial";
        public static String listofmyinvestmenttutorial="listofmyinvestmenttutorial";

        public static String addassettutorial="addassettutorial";



    }




    public static class WebServiceMethodes{

        public static String login="UserLogin.php";
        public static String getCountry="getCountry.php";
        public static String getState="getState.php";
        public static String checkSponsor="getSponserDetailsByMobile.php";
        public static String getCouponcodeData="getCouponcodeData.php";
        public static String getUserByMobile="getUserByMobile.php";
        public static String smsMethode="httpapi";
        public static String userAuthenticate="UserAuthenticate.php";
        public static String updateOSDeviceid="updateDeviceOSId.php";

        public static String updateDeviceId="updateDeviceId.php";

        public static String getUserDetails="getUserDetails.php";

        public static String UserProfileUpdate="UserProfileUpdate.php";

        public static String validateExpirydate="validateExpirydate.php";

        public static String validateTrialPeriod="validateTrialPeriod.php";

        public static String showMemberDetails="getDSTByPhoneNumber.php";

        public static String getSliderImages="getSlideBanner.php";

        public static String getSettingsValue="getSettingsValue.php";

        public static String getSettingsSlider="getSettingsSlider.php";

        public static String getPositionBeforePurchase="getPositionBeforePurchase.php";

        public static String updateRenewalStatus="updateRenewalStatus.php";

        public static String updateGoogleDriveFileId="updateGoogleDriveFileId.php";

        public static String updatePositionNext="updatePositionNext.php";

        public static String updatePosition="updatePosition.php";

        public static String addSalesInfo="addSalesInfo.php";

        public static String checkSalesInfo="getpaymentDetails.php";

        public static String getRenewalDetails="getRenewalDetails.php";

        public static String getCurrencyByCountryID="getCurrencyByCountryID.php";

        public static String getNotifications="getNotificationsData.php";

        public static String getMobileAppVersion="getMobileAppVersion.php";

        public static String deleteAccount="deleteAccount.php";

        public static String getFeedback="getFeedback.php";

        public static String changePassword="changePassword.php";

        public static String addFeedback="addFeedback.php";

        public static String updateNotificationStatus="updateNotificationStatus.php";

        public static String uploadBackupFile="uploadBackupFile.php";

        public static String uploadUserProfile="uploadUserProfile.php";

        public static String updateActivationdate="updateActivationdate.php";

        public static String getbill="getDSTSales.php";

        public static String getSalesData="getSalesData.php";

        public static String updateVersionPlatform="updateVersionPlatform.php";

        public static String getBackupInfoByuserId="getBackupInfoByuserId.php";

        public static String addGdriveBackupinfo="addGdriveBackupinfo.php";

        public static String checkApp="checkApp.php";

        public static String getRechargePlans="getRechargePlans.php";

        public static String PostTransactionata="PostTransactionata.php";
        public static String updatePostTransaction="updatePostTransaction.php";


        public static String updateRechargeStatus="updateRechargeStatus.php";
        public static String updateGenStatus="updateGenStatus.php";

        public static String updateTransactionStatus="updateTransactionStatus.php";

        public static String PostPgAmountData="PostPgAmountData.php";
        public static String getRechargeHistory="getRechargeHistory.php";
//        public static String updateRechargeTransactionStatus="updateRechargeTransactionStatus.php";
public static String updateRechargeTransactionStatus="updateRechargeStatusRetry.php";


        public static String updateTransactionStatusRecharge="updateTransactionStatusRecharge.php";
        public static String getInitiatedRecharges="getInitiatedRecharges.php";


        public static String updatePaymentStatus="updatePaymentStatus.php";
        public static String updateRechargeStatusInPayment="updateRechargeStatusInPayment.php";

    }





    public static boolean checkDBTablempty(Context context)
    {

        boolean isdbEmpty=false;
        List<Accounts> accounts = new DatabaseHelper(context).getAllAccounts();
        List<CommonData>commonData_diarysubject=new DatabaseHelper(context).getData(Utils.DBtables.DIARYSUBJECT_table);
        List<CommonData>commonData_diary=new DatabaseHelper(context).getData(Utils.DBtables.DIARY_table);
        List<CommonData>commonData_budget=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_BUDGET);
        List<CommonData>commonData_invest=new DatabaseHelper(context).getData(Utils.DBtables.INVESTMENT_table);
        List<CommonData>commonData_task=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_TASK);
       // List<CommonData>commonData_acc=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_ACCOUNTSETTINGS);
        List<CommonData>commonData_insu=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_INSURANCE);
        List<CommonData>commonData_iliab=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_LIABILITY);
        List<CommonData>commonData_asset=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_ASSET);
        List<CommonData>commonData_accrecipt=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT);
        List<CommonData>commonData_appin=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_APP_PIN);
        List<CommonData>commonData_wallet=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_WALLET);
        List<CommonData>commonData_doc=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_DOCUMENT);
        List<CommonData>commonData_pass=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_PASSWORD);
        List<CommonData>commonData_visit=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_VISITCARD);
        List<CommonData>commonData_target=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_TARGET);
        List<CommonData>commonData_milestone=new DatabaseHelper(context).getData(DBtables.TABLE_MILESTONE);
        List<CommonData>commonData_addedmilestone=new DatabaseHelper(context).getData(DBtables.TABLE_ADDEDAMOUNT_MILESTONE);

        List<CommonData>commonData_weblinks=new DatabaseHelper(context).getData(DBtables.TABLE_WEBLINKS);
//        List<CommonData>commonData_emergency=new DatabaseHelper(context).getData(DBtables.TABLE_EMERGENCY);


        if(commonData_weblinks.size()>0||     accounts.size()>0||commonData_diarysubject.size()>0||commonData_diary.size()>0||
                commonData_budget.size()>0||commonData_invest.size()>0||commonData_task.size()>0||
                commonData_insu.size()>0||commonData_iliab.size()>0||commonData_asset.size()>0
                ||commonData_accrecipt.size()>0||commonData_appin.size()>0||commonData_wallet.size()>0||commonData_doc.size()>0||
                commonData_pass.size()>0||commonData_visit.size()>0||commonData_target.size()>0||commonData_milestone.size()>0||commonData_addedmilestone.size()>0 )
        {



            isdbEmpty=false;



        }
        else {

            isdbEmpty=true;
        }

        return isdbEmpty;
    }











    public static class BillVoucherDetails{

        public static String billvoucherNumber="Save_Bill_000";

    }

    public static class CashFreeCredentials{

        public static  String appid=  "2110088e61c5abc8b72c74b033800112";

        public static  String cashfreesecretkey="6d61be6225fcc9f4ea900b1fd8fb3aa6cc83b7ea";

        public static String paymenturl="https://api.cashfree.com/pg/orders";
    }


    public static class PaytmCredentials{

        public static int ActivityRequestCode=11;
        //Test credentials

//       public static String TestMerchantID=
//                "eAoUUb21380278750445";
//        public static String TestMerchantKey=
//        "@q%YFqxqTmAsmuWi";
//        public static String Website=
//        "WEBSTAGING";
//        public static String IndustryType=
//        "Retail";
//
//        public static String ChannelID =
//        "WAP";





        //Live credentials

        public static String TestMerchantID=
                "GDaxgY06469545723565";
        public static String TestMerchantKey=
                "z2qhcZY4%W5uMgeK";
        public static String Website=
                "DEFAULT";
        public static String IndustryType=
                "Retail";

        public static String ChannelID =
                "WAP";

    }




    public static Stack<Fragment>fragmentStack=new Stack<>();



    public static class PaymentCredential{

//        public static String paymentaccount="ramachandrank10@ybl";

        public static String paymentaccount="9846290789@cnrb";
    }



    public static class Cashtype{

        public static int credit=1;
        public static int debit=0;





    }

    public static void showTutorial(String msg,Context context,String key)
    {
        if(new PreferenceHelper(context).getData(key).equalsIgnoreCase("1"))
        {

        }
        else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((AppCompatActivity)context). getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;


            final Dialog dialog=new Dialog(context);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
           // AlertDialog.Builder builder = new AlertDialog.Builder(context);
            dialog.setContentView(R.layout.layout_tutorial);
            dialog.setCancelable(true);
          //  dialog.getWindow().setLayout();


            TextView txtTutorial = dialog.findViewById(R.id.txtTutorial);
            CheckBox cbDontshow = dialog.findViewById(R.id.cbDontshow);

            Button btnSubmit=dialog.findViewById(R.id.btnSubmit);

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context cont= LocaleHelper.setLocale(context, languagedata);

            Resources resources=cont.getResources();
            btnSubmit.setText(resources.getString(R.string.ok));



            txtTutorial.setText(msg);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cbDontshow.isChecked())
                    {
                        new PreferenceHelper(context).putData(key,"1");
                    }


                    dialog.dismiss();
                }
            });



            dialog.getWindow().setLayout(width-5,height );


            dialog.show();
        }
    }

    public static String getCapsSentences(Context context,String tagName) {

        String data="";

        String languagedata=LocaleHelper.getPersistedData(context,"en");

        if(languagedata.equalsIgnoreCase("en")) {

            if (!tagName.equalsIgnoreCase("")) {
                String[] splits = tagName.toLowerCase().split(" ");
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < splits.length; i++) {
                    String eachWord = splits[i];


                    if (i > 0 && eachWord.length() > 0) {
                        sb.append(" ");
                    }

                    if (isWordExist(eachWord)) {
                        sb.append(eachWord);
                    } else {
                        String cap = eachWord.substring(0, 1).toUpperCase()
                                + eachWord.substring(1);
                        sb.append(cap);
                    }
                }

                data = sb.toString();
            } else {

                data = "";

            }
        }
        else{

            data=tagName;
        }
        return data;
    }

    public static boolean isWordExist(String word)
    {
        boolean iswordexist=false;

        String[] arraywords={"in","to","of","or","on","and","an"};
        for (int i = 0; i < arraywords.length; i++) {

            if(arraywords[i].equalsIgnoreCase(word.trim()))

            {
                iswordexist=true;

                break;


            }



        }

        return iswordexist;



    }

    public static boolean isConnectionAvailable(Context context) {
        if(context!=null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnected()
                        && netInfo.isConnectedOrConnecting()
                        && netInfo.isAvailable()) {
                    return true;
                }
            }
        }
        return false;
    }


    public static class Contents{

        public static String Content="Content";

        public static int howtouse=1;
        public static int termsandcondition=7;
        public static int privacypolicy=2;
        public static int aboutus=3;


    }






    public static class CashBanktype{

        public static int cash=1;
        public static int bank=2;

        public static int account=3;





    }

    public static class VoucherType{

        public static int paymentvoucher=1;
        public static int receiptvoucher=2;

        public static int billvoucher=3;

        public static int bankvoucher=5;

        public static int journalvoucher=4;

        public static int wallet=6;

    }

    public static class Requestcode{

        public static String AccVoucherAll="All";


        public static int ForAccountSettingswithoutRequestcode=11;

        public static int ForAccountSettingsBankRequestcode=110;

        public static int ForAccountSettingsCashRequestcode=10;

        public static int ForAccountSettingsCustomerRequestcode=101;

        public static int ForAccountSettingsIncomeRequestcode=100;

        public static int ForAccountSettingsInvestmentRequestcode=150;
        public static final int  ForAccountSettingsAssetRequestcode=153;

        public static int ForAccountSettingsInsuranceRequestcode=157;

        public static int ForAccountSettingsLiabilityRequestcode=160;

        public static int ForAccountSettingsinvestmentRequestcode=165;
    }




    public static class DBtables{


        public static String DIARYSUBJECT_table="DIARYSUBJECT_table";

        public static String DIARY_table="DIARY_table";

        public static String TABLE_BUDGET="TABLE_BUDGET";


        public static String INVESTMENT_table="INVESTMENT_table";

        public static String TABLE_TASK="TABLE_TASK";

        public static String TABLE_ACCOUNTSETTINGS="TABLE_ACCOUNTSETTINGS";


        public static String TABLE_INSURANCE="TABLE_INSURANCE";


        public static String TABLE_LIABILITY="TABLE_LIABILITY";


        public static String TABLE_ASSET="TABLE_ASSET";

        public static String TABLE_ACCOUNTS="TABLE_ACCOUNTS";
        public static String TABLE_ACCOUNTS_RECEIPT="TABLE_ACCOUNTS_RECEIPT";

        public static String TABLE_APP_PIN="TABLE_APP_PIN";

        public static String TABLE_WALLET="TABLE_WALLET";

        public static String TABLE_DOCUMENT="TABLE_DOCUMENT";

        public static String TABLE_PASSWORD="TABLE_PASSWORD";



        public static String TABLE_VISITCARD="TABLE_VISITCARD";

        public static String TABLE_VISITCARD_IMAGE="TABLE_VISITCARD_IMAGE";

        public static String TABLE_TARGETCATEGORY="TABLE_TARGETCATEGORY";

        public static String TABLE_TARGET="TABLE_TARGET";

        public static String TABLE_MILESTONE="TABLE_MILESTONE";

        public static String TABLE_ADDEDAMOUNT_MILESTONE="TABLE_ADDEDAMOUNT";

        public static String TABLE_BACKUP="TABLE_BACKUP";

        public static String TABLE_RENEWALMSG="TABLE_RENEWALMSG";

        public static String TABLE_WEBLINKS="TABLE_WEBLINKS";
        public static String TABLE_EMERGENCY="TABLE_EMERGENCY";


    }



    public static class TableAccounts{

        public static String ACCOUNTS_id="ACCOUNTS_id";
        public static String ACCOUNTS_entryid="ACCOUNTS_entryid";

        public static String ACCOUNTS_date="ACCOUNTS_date";


        public static String ACCOUNTS_setupid="ACCOUNTS_setupid";


        public static String ACCOUNTS_amount="ACCOUNTS_amount";


        public static String ACCOUNTS_type="ACCOUNTS_type";


        public static String ACCOUNTS_remarks="ACCOUNTS_remarks";

        public static String ACCOUNTS_month="ACCOUNTS_month";


        public static String ACCOUNTS_year="ACCOUNTS_year";

        public static String ACCOUNTS_cashbanktype="ACCOUNTS_cashbanktype";

        public static String ACCOUNTS_VoucherType="ACCOUNTS_VoucherType";

        public static String ACCOUNTS_billId="ACCOUNTS_billId";

        public static String ACCOUNTS_billVoucherNumber="ACCOUNTS_billVoucherNumber";

    }

    public static String getFormatedAmount(double val)
    {
        DecimalFormat formater = new DecimalFormat("#.##");

       String a= formater.format(val);

       return a;
    }


    public static void playSimpleTone(Context ct)
    {
        MediaPlayer mp;
        mp = MediaPlayer.create(ct, R.raw.smallbeep);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();

    }


    public static List<Accounts>getAccountsBetweenDates(Context context,Date startdate,Date enddate)
    {

        List<Accounts> accounts_noentry = new DatabaseHelper(context).getAllAccounts();

        List<Accounts>accounts_bydate=new ArrayList<>();

        try {

            if (accounts_noentry.size() > 0) {

                for (Accounts accounts : accounts_noentry) {


                    String accounts_date = accounts.getACCOUNTS_date();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                    Date accdate=sdf.parse(accounts_date);

                    if(accdate.equals(startdate)&&accdate.before(enddate)) {

                        accounts_bydate.add(accounts);

                    }
                    else  if(accdate.after(startdate) && accdate.equals(enddate))
                    {
                        accounts_bydate.add(accounts);
                    }
                    else if(accdate.equals(startdate) && accdate.equals(enddate))
                    {

                        accounts_bydate.add(accounts);
                    }
                    else if (accdate.after(startdate) && accdate.before(enddate)) {

                        accounts_bydate.add(accounts);
                    }



                    }


            }

        }catch (Exception e)
        {

        }

        return accounts_bydate;
    }



    public static List<Accounts>sortAccountByYear(Context context,String year) {
        List<Accounts> accounts_noentry = new DatabaseHelper(context).getAllAccounts();

        List<Accounts>accounts=new ArrayList<>();


        for (Accounts acc:accounts_noentry)
        {

            if(acc.getACCOUNTS_year().equalsIgnoreCase(year))
            {
                accounts.add(acc);
            }



        }

        return accounts;

    }








    public static List<Accounts>sortAccountByDate(Context context)
    {
        List<Accounts> accounts_noentry = new DatabaseHelper(context).getAllAccounts();


        if(accounts_noentry.size()>0)
        {


            Collections.sort(accounts_noentry, new Comparator<Accounts>() {
                @Override
                public int compare(Accounts accounts, Accounts t1) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate=null;

                    Date endDate=null;
                    try {


                        strDate = sdf.parse(accounts.getACCOUNTS_date());
                        endDate = sdf.parse(t1.getACCOUNTS_date());




                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return strDate.compareTo(endDate);
                }
            });
        }



        return accounts_noentry;
    }


    public static List<Accounts>getAllAccountBeforeDate(Context context,String date)
    {

        List<Accounts> accounts_noentry = new DatabaseHelper(context).getAllAccounts();

        List<Accounts>accountsbefordate=new ArrayList<>();


        if(accounts_noentry.size()>0) {



            for (int i=0;i<accounts_noentry.size();i++)
            {

                Accounts accounts=accounts_noentry.get(i);

                String date_acc=accounts.getACCOUNTS_date();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date dateAccount=null;

                Date selecteddate=null;
                try {

                    dateAccount = sdf.parse(accounts.getACCOUNTS_date());
                    selecteddate = sdf.parse(date);

                    if(dateAccount.before(selecteddate))
                    {


                        accountsbefordate.add(accounts);

                    }

//                    if(dateAccount.equals(selecteddate))
//                    {
//
//
//                        accountsbefordate.add(accounts);
//
//                    }


                }
                catch (Exception e)
                {

                }








            }


        }



        return accountsbefordate;


        }



    public static List<Accounts>getAllAccountBeforeIncludesDate(Context context,String date)
    {

        List<Accounts> accounts_noentry = new DatabaseHelper(context).getAllAccounts();

        List<Accounts>accountsbefordate=new ArrayList<>();


        if(accounts_noentry.size()>0) {



            for (int i=0;i<accounts_noentry.size();i++)
            {

                Accounts accounts=accounts_noentry.get(i);

                String date_acc=accounts.getACCOUNTS_date();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date dateAccount=null;

                Date selecteddate=null;
                try {

                    dateAccount = sdf.parse(accounts.getACCOUNTS_date());
                    selecteddate = sdf.parse(date);

                    if(dateAccount.before(selecteddate))
                    {


                        accountsbefordate.add(accounts);

                    }

                    if(dateAccount.equals(selecteddate))
                    {


                        accountsbefordate.add(accounts);

                    }


                }
                catch (Exception e)
                {

                }








            }


        }



        return accountsbefordate;


    }

    public static void showAlertWithSingle(Context context, String msg, final DialogEventListener dialogEventListener)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onPositiveButtonClicked();
                }



            }
        });


        builder.show();

//        Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
//
//
//        snackbar.show();

    }


    public static void showUpdateAlert(Context context, String msg, final DialogEventListener dialogEventListener)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setCancelable(false);

        builder.setMessage(msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onPositiveButtonClicked();
                }



            }
        });
        builder.setNegativeButton("Got it !", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onNegativeButtonClicked();
                }

            }
        });

        builder.show();
    }



    public static void showAlert(Context context, String msg, final DialogEventListener dialogEventListener)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setMessage(msg);
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onPositiveButtonClicked();
                }



            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onNegativeButtonClicked();
                }

            }
        });

        builder.show();
    }


    public static void showReciptAlert(Context context, String msg, final DialogEventListener dialogEventListener)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setMessage(msg);
        builder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onPositiveButtonClicked();
                }



            }
        });
        builder.setNegativeButton("SKIP", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(dialogEventListener!=null)
                {
                    dialogInterface.dismiss();

                    dialogEventListener.onNegativeButtonClicked();
                }

            }
        });

        builder.show();
    }




    public static boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String getTempstorageFilePath(Context context) {

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        String tempstorage = context.getExternalCacheDir()+"/"+ts+".png";

        return tempstorage;

    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
    /* // If you have Apache Commons Text, you can use it to calculate the edit distance:
    LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
    return (longerLength - levenshteinDistance.apply(longer, shorter)) / (double) longerLength; */
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;

    }

    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }



    public static void scheduleJob(Context context) {

        ComponentName serviceComponent = new ComponentName(context, DateRemindService.class);

        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);

        builder.setMinimumLatency(3 * 1000); // Wait at least 30s

        builder.setOverrideDeadline(6 * 1000); // Maximum delay 60s



        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(context.JOB_SCHEDULER_SERVICE);

        jobScheduler.schedule(builder.build());

    }

}
