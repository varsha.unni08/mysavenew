package com.centroid.integraaccounts.data.domain;

public class DTHValidity {

    String validity="";
    String amount="";

    public DTHValidity() {
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
