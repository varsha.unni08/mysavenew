package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class VisitCard implements Serializable {
    int id;
    String data;
    byte[] bimage;

    public VisitCard() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public byte[] getBimage() {
        return bimage;
    }

    public void setBimage(byte[] bimage) {
        this.bimage = bimage;
    }
}
