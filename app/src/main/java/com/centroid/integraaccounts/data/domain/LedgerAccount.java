package com.centroid.integraaccounts.data.domain;

public class LedgerAccount {


    String accountheadid="";
    String Closingbalance="0";
    String accountheadname="";
    String debitorcredit="";
    String debitcreditopening="";
    String closingbalancebeforedate="";

    public LedgerAccount() {
    }


    public String getDebitcreditopening() {
        return debitcreditopening;
    }

    public void setDebitcreditopening(String debitcreditopening) {
        this.debitcreditopening = debitcreditopening;
    }

    public String getClosingbalancebeforedate() {
        return closingbalancebeforedate;
    }

    public void setClosingbalancebeforedate(String closingbalancebeforedate) {
        this.closingbalancebeforedate = closingbalancebeforedate;
    }

    public String getDebitorcredit() {
        return debitorcredit;
    }

    public void setDebitorcredit(String debitorcredit) {
        this.debitorcredit = debitorcredit;
    }

    public String getAccountheadid() {
        return accountheadid;
    }

    public void setAccountheadid(String accountheadid) {
        this.accountheadid = accountheadid;
    }

    public String getClosingbalance() {
        return Closingbalance;
    }

    public void setClosingbalance(String closingbalance) {
        Closingbalance = closingbalance;
    }

    public String getAccountheadname() {
        return accountheadname;
    }

    public void setAccountheadname(String accountheadname) {
        this.accountheadname = accountheadname;
    }
}
