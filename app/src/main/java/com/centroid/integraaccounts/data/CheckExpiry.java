package com.centroid.integraaccounts.data;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.ExpirydateCheckListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.LoginActivity;
import com.centroid.integraaccounts.views.MainActivity;
import com.centroid.integraaccounts.views.ProfileActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckExpiry {

    Context context;

    ExpirydateCheckListener expirydateCheckListener;

    public CheckExpiry(Context context, ExpirydateCheckListener expirydateCheckListener) {
        this.context = context;
        this.expirydateCheckListener = expirydateCheckListener;
    }



    public void checkExpiryDate()
    {
//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(context, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
               // progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {

                    JSONObject jsonObject1=new JSONObject(data);


                    if(jsonObject1!=null)
                    {

                        if(jsonObject1!=null)
                        {

                            if(expirydateCheckListener!=null)
                            {
                                expirydateCheckListener.OnExpirydateChecked(jsonObject1);
                            }









                        }









                    }


                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
               // progressFragment.dismiss();

               // Toast.makeText(context,err,Toast.LENGTH_SHORT).show();


                Utils.showAlertWithSingle(context, err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });



            }
        },Utils.WebServiceMethodes.validateExpirydate+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();



















//
//
//        Call<JsonObject> jsonArrayCall=client.checkExpiryDate(new PreferenceHelper(context).getData(Utils.userkey));
//
//        jsonArrayCall.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                progressFragment.dismiss();
//
//                if(response!=null)
//                {
//
//                    if(response.body()!=null)
//                    {
//
//if(expirydateCheckListener!=null)
//{
//    expirydateCheckListener.OnExpirydateChecked(response.body());
//}
//
//
//
//
//
//
//
//
//
//                    }
//
//
//
//
//
//
//
//
//
//                }

//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//                progressFragment.dismiss();
//
//                if(expirydateCheckListener!=null)
//                {
//                    expirydateCheckListener.OnFailed();
//                }
//            }
//        });

    }
}
