package com.centroid.integraaccounts.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.MediaPlayer;

import androidx.annotation.Nullable;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Diarydata;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.data.domain.TaskData;
import com.centroid.integraaccounts.data.domain.VisitCard;
import com.centroid.integraaccounts.data.domain.VisitCardImg;
import com.centroid.integraaccounts.messageservice.MessageService;
import com.centroid.integraaccounts.paymentdata.domain.Target;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    Context ct;

    public DatabaseHelper(@Nullable Context context) {
        super(context, "IntegraDB", null, 8);
        this.ct=context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {







        String CREATE_PAYMENTVOUCHER_TABLE = "CREATE TABLE " + "TABLE_PAYMENTVOUCHER" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "voucherdata" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_PAYMENTVOUCHER_TABLE);


        String CREATE_INSURANCE_no = "CREATE TABLE " + "INSURANCE_NO" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "insuranceNO" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_INSURANCE_no);



        String CREATE_INVESTNAMES = "CREATE TABLE " + "INVESTNAMES_TABLE" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "investname" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_INVESTNAMES);



        String CREATE_CASHBALANCE = "CREATE TABLE " + "CASHBALANCE_table" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "cashbalancedata" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_CASHBALANCE);


        String CREATE_LOAN = "CREATE TABLE " + "LOAN_table" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "loan_data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_LOAN);

        String CREATE_RECEIPT = "CREATE TABLE " + "RECEIPT_table" + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "receipt_data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_RECEIPT);



        String CREATE_document = "CREATE TABLE " + Utils.DBtables.TABLE_DOCUMENT + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_document);



        String CREATE_WALLET = "CREATE TABLE " + Utils.DBtables.TABLE_WALLET + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
               + ")";

        sqLiteDatabase.execSQL(CREATE_WALLET);




        String CREATE_PASSWORD = "CREATE TABLE " + Utils.DBtables.TABLE_PASSWORD + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_PASSWORD);



        String CREATE_VISIT_CARD_IMAGE = "CREATE TABLE " + Utils.DBtables.TABLE_VISITCARD_IMAGE + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " BLOB "
                + ")";

        sqLiteDatabase.execSQL(CREATE_VISIT_CARD_IMAGE);



        String CREATE_TARGET = "CREATE TABLE " + Utils.DBtables.TABLE_TARGETCATEGORY + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT, "+ "iconimage"+" BLOB"
                + ")";

        sqLiteDatabase.execSQL(CREATE_TARGET);





        String CREATE_APPIN = "CREATE TABLE " + Utils.DBtables.TABLE_APP_PIN + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_APPIN);


        String CREATE_TABLE_MILESTONE = "CREATE TABLE " + Utils.DBtables.TABLE_MILESTONE + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_TABLE_MILESTONE);



        String CREATE_TABLE_ACCOUNTS = "CREATE TABLE " + Utils.DBtables.TABLE_ACCOUNTS + "("
                + Utils.TableAccounts.ACCOUNTS_id + " INTEGER PRIMARY KEY AUTOINCREMENT , "+Utils.TableAccounts.ACCOUNTS_VoucherType + " INTEGER ," + Utils.TableAccounts.ACCOUNTS_entryid + " TEXT , " +Utils.TableAccounts.ACCOUNTS_date + " TEXT , "+Utils.TableAccounts.ACCOUNTS_setupid + " TEXT , "+Utils.TableAccounts.ACCOUNTS_amount + " TEXT , "+Utils.TableAccounts.ACCOUNTS_type + " TEXT , "+Utils.TableAccounts.ACCOUNTS_remarks + " TEXT , "+Utils.TableAccounts.ACCOUNTS_year + " TEXT , "+Utils.TableAccounts.ACCOUNTS_month + " TEXT  , "
                +Utils.TableAccounts.ACCOUNTS_cashbanktype+" TEXT , "+Utils.TableAccounts.ACCOUNTS_billId+" TEXT , "+Utils.TableAccounts.ACCOUNTS_billVoucherNumber+" TEXT"+" )";

        sqLiteDatabase.execSQL(CREATE_TABLE_ACCOUNTS);



        String CREATE_TABLE_ACCOUNTS_RECEIPT = "CREATE TABLE " + Utils.DBtables.TABLE_ACCOUNTS_RECEIPT + "("
                + Utils.TableAccounts.ACCOUNTS_id + " INTEGER PRIMARY KEY AUTOINCREMENT," + Utils.TableAccounts.ACCOUNTS_entryid + " TEXT , " +Utils.TableAccounts.ACCOUNTS_date + " TEXT , "+Utils.TableAccounts.ACCOUNTS_setupid + " TEXT , "+Utils.TableAccounts.ACCOUNTS_amount + " TEXT , "+Utils.TableAccounts.ACCOUNTS_type + " TEXT , "+Utils.TableAccounts.ACCOUNTS_remarks + " TEXT , "+Utils.TableAccounts.ACCOUNTS_year + " TEXT , "+Utils.TableAccounts.ACCOUNTS_month + " TEXT  , "
                +Utils.TableAccounts.ACCOUNTS_cashbanktype+" TEXT "+ ")";

        sqLiteDatabase.execSQL(CREATE_TABLE_ACCOUNTS_RECEIPT);






        String CREATE_ASSET = "CREATE TABLE " + Utils.DBtables.TABLE_ASSET + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_ASSET);





        String CREATE_LIABILITIES = "CREATE TABLE " + Utils.DBtables.TABLE_LIABILITY + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_LIABILITIES);






        String CREATE_INSURANCE_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_INSURANCE + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_INSURANCE_TABLE);





        String CREATE_ACCOUNTSETTINGS_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_ACCOUNTSETTINGS + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_ACCOUNTSETTINGS_TABLE);








        String CREATE_DIARYSUBJ = "CREATE TABLE " + Utils.DBtables.DIARYSUBJECT_table + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_DIARYSUBJ);




        String CREATE_BUDGET_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_BUDGET + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_BUDGET_TABLE);



        String CREATE_DIARY_TABLE = "CREATE TABLE " + Utils.DBtables.DIARY_table + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_DIARY_TABLE);


        String CREATE_INVESTMENT = "CREATE TABLE " + Utils.DBtables.INVESTMENT_table + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_INVESTMENT);



        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_TASK + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                + ")";

        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);




        String CREATE_VISITCARD_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_VISITCARD + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT , " +"logoimage"+" BLOB ,"
                + "cardimg"+" BLOB"+")";

        sqLiteDatabase.execSQL(CREATE_VISITCARD_TABLE);




//        Target target=new Target();
//        target.setAmount(targetamount+"");
//        target.setInitialamount(savedamount+"");
//        target.setDate(date);
//        target.setNote(edtNotes.getText().toString());
//        target.setCategoryid(tgf.getId());


        String CREATE_TARGET_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_TARGET + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_TARGET_TABLE);


        String CREATE_ddedamount_INMILESTONE = "CREATE TABLE " + Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_ddedamount_INMILESTONE);

        String CREATE_BACKUPTABLE = "CREATE TABLE " + Utils.DBtables.TABLE_BACKUP + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_BACKUPTABLE);

        String CREATE_RENEWALMSGTABLE = "CREATE TABLE " + Utils.DBtables.TABLE_RENEWALMSG + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_RENEWALMSGTABLE);


        String CREATE_WEBLINKS = "CREATE TABLE " + Utils.DBtables.TABLE_WEBLINKS + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_WEBLINKS);


        String CREATE_EMERGENCY = "CREATE TABLE " + Utils.DBtables.TABLE_EMERGENCY + "("
                + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

        sqLiteDatabase.execSQL(CREATE_EMERGENCY);

    }




    public boolean checkDataBase(){

        SQLiteDatabase checkDB = null;
        boolean iscontent=false;


        File f=new File("/data/data/com.mysaving.integraaccounts/databases/IntegraDB.db");

//        try{
//            checkDB = SQLiteDatabase.openDatabase("", null, SQLiteDatabase.OPEN_READONLY);
//        } catch(SQLiteException e){
//            //database doesn't exist yet
//        }
//
//        if(checkDB != null){
//            checkDB.close();
//        }

        if(f.length()>0)
        {

            iscontent=true;
        }
        else {

            iscontent=false;
        }


        return iscontent;
    }




    public void deleteAllData(String tablename)
    {
        try {


            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from "+ tablename);

        }catch (Exception e)
        {

        }

    }

    public void dropAllData(String tablename)
    {
        try {


            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DROP TABLE IF EXISTS "+ tablename);
            String CREATE_ACCOUNTSETTINGS_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_ACCOUNTSETTINGS + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                    + ")";
            db.execSQL(CREATE_ACCOUNTSETTINGS_TABLE);

        }catch (Exception e)
        {

        }

    }


    public void dropAllDataofEmergency()
    {
        try {


            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DROP TABLE IF EXISTS "+ Utils.DBtables.TABLE_EMERGENCY);


            String CREATE_EMERGENCY = "CREATE TABLE " + Utils.DBtables.TABLE_EMERGENCY + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            db.execSQL(CREATE_EMERGENCY);


        }catch (Exception e)
        {

        }

    }

    public void dropAllDataFromTarget(String tablename)
    {
        try {


            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DROP TABLE IF EXISTS "+ tablename);

            String CREATE_TARGET = "CREATE TABLE " + Utils.DBtables.TABLE_TARGETCATEGORY + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT, "+ "iconimage"+" BLOB"
                    + ")";

            db.execSQL(CREATE_TARGET);


        }catch (Exception e)
        {

        }

    }



    public Long addAccountsData(Accounts accounts)
    {



        Long insertid=0l;
        try{


            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put(Utils.TableAccounts.ACCOUNTS_entryid,accounts.getACCOUNTS_entryid());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_date,accounts.getACCOUNTS_date());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_setupid,accounts.getACCOUNTS_setupid());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_amount,accounts.getACCOUNTS_amount());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_type,accounts.getACCOUNTS_type());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_billId,accounts.getACCOUNTS_billId());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_billVoucherNumber,accounts.getBillvouchernumber());


            contentValues.put(Utils.TableAccounts.ACCOUNTS_remarks,accounts.getACCOUNTS_remarks());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_year,accounts.getACCOUNTS_year());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_month,accounts.getACCOUNTS_month());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_VoucherType,accounts.getACCOUNTS_vouchertype());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_cashbanktype,accounts.getACCOUNTS_cashbanktype());

          insertid=  sqLiteDatabase.insert(Utils.DBtables.TABLE_ACCOUNTS,null,contentValues);


            sqLiteDatabase.close();









        }catch (Exception e)
        {

        }

        return insertid;
    }


    public int getLastBillVoucherID()
    {

        int trid=0;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery(query,null);


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {

                    trid=cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id));




                                   } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return trid;
    }




    @SuppressLint("Range")
    public List<Accounts>getAllAccounts()
    {



        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
              String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery(query,null);


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();

                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));


                    contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));

                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));

                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));


                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));

                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));

                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));
                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }





















    public List<Accounts>getAccountsDataByBillid(String id)
    {







        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_billId+"=?",new String[]{id});


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();
                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));



                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));


                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));

                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));

                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));

                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public List<Accounts>getAccountsDataByVouchertypeAndSetupid(String id,String setupid)
    {



        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            //  String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_VoucherType+"=?",new String[]{id});


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {


                    if(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)).equalsIgnoreCase(setupid)) {

                        if(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)).equalsIgnoreCase(Utils.CashBanktype.account+"")) {

                            Accounts contact = new Accounts();
                            contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                            contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                            contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                            contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                            contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));

                            contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));

                            contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                            contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));

                            contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));


                            contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                            contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));

                            contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));
                            contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                            // Adding contact to list
                            taskData.add(contact);
                        }
                    }
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }






    public List<Accounts>getAccountsDataByVouchertype(String id)
    {



        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            //  String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_VoucherType+"=?",new String[]{id});


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();
                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));


                    contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));

                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));

                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));


                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));

                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));
                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public List<Accounts>getAccountsDataByEntryId(String id)
    {



        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            //  String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_entryid+"=?",new String[]{id});


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();
                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));



                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));

                    contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));


                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));

                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));
                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));
                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }






    public List<Accounts>getAccountsDataBYsetupid(String id)
    {







        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            //  String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_setupid+"=?",new String[]{id});


            // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();
                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_vouchertype(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType));
                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));



                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));


                    contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));

                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));
                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));

                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));
                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }




    public List<Accounts>getAccountsDataBYid(String id)
    {







        List<Accounts>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
          //  String query="SELECT  * FROM  "+Utils.DBtables.TABLE_ACCOUNTS;

            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_ACCOUNTS+" WHERE "+Utils.TableAccounts.ACCOUNTS_id+"=?",new String[]{id});


           // Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Accounts contact = new Accounts();
                    contact.setACCOUNTS_id(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_id)));
                    contact.setACCOUNTS_entryid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_entryid)));


                    contact.setACCOUNTS_setupid(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_setupid)));

                    contact.setACCOUNTS_date(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_date)));


                    contact.setACCOUNTS_amount(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_amount)));



                    contact.setACCOUNTS_type(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_type)));


                    contact.setACCOUNTS_remarks(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_remarks)));


                    contact.setACCOUNTS_billId(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billId)));

                    contact.setACCOUNTS_year(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_year)));


                    contact.setACCOUNTS_month(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_month)));
                    contact.setACCOUNTS_cashbanktype(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_cashbanktype)));

                    contact.setACCOUNTS_vouchertype(cursor.getInt(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_VoucherType)));
                    contact.setBillvouchernumber(cursor.getString(cursor.getColumnIndex(Utils.TableAccounts.ACCOUNTS_billVoucherNumber)));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public void deleteAccountDataById(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete(Utils.DBtables.TABLE_ACCOUNTS,Utils.TableAccounts.ACCOUNTS_id   + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }


//    public int updateReceiptAccountsData(String id,Accounts accounts)
//    {
//
//
//
//        int insertid=0;
//        try{
//
//
//            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
//
//            ContentValues contentValues=new ContentValues();
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_entryid,accounts.getACCOUNTS_entryid());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_date,accounts.getACCOUNTS_date());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_setupid,accounts.getACCOUNTS_setupid());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_amount,accounts.getACCOUNTS_amount());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_type,accounts.getACCOUNTS_type());
//
//
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_remarks,accounts.getACCOUNTS_remarks());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_year,accounts.getACCOUNTS_year());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_month,accounts.getACCOUNTS_month());
//            contentValues.put(Utils.TableAccounts.ACCOUNTS_cashbanktype,accounts.getACCOUNTS_cashbanktype());
//
//            insertid=  sqLiteDatabase.update(Utils.DBtables.TABLE_ACCOUNTS_RECEIPT,contentValues,Utils.TableAccounts.ACCOUNTS_id  + " = ?",
//                    new String[] { String.valueOf(id) });
//
//
//            sqLiteDatabase.close();
//
//
//
//
//
//
//
//
//
//        }catch (Exception e)
//        {
//
//        }
//
//        return insertid;
//    }




    public int updateAccountsData(String id,Accounts accounts)
    {



        int insertid=0;
        try{


            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put(Utils.TableAccounts.ACCOUNTS_entryid,accounts.getACCOUNTS_entryid());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_date,accounts.getACCOUNTS_date());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_setupid,accounts.getACCOUNTS_setupid());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_amount,accounts.getACCOUNTS_amount());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_type,accounts.getACCOUNTS_type());


            contentValues.put(Utils.TableAccounts.ACCOUNTS_VoucherType,accounts.getACCOUNTS_vouchertype());

            contentValues.put(Utils.TableAccounts.ACCOUNTS_billId,accounts.getACCOUNTS_billId());


            contentValues.put(Utils.TableAccounts.ACCOUNTS_billVoucherNumber,accounts.getBillvouchernumber());

            contentValues.put(Utils.TableAccounts.ACCOUNTS_remarks,accounts.getACCOUNTS_remarks());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_year,accounts.getACCOUNTS_year());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_month,accounts.getACCOUNTS_month());
            contentValues.put(Utils.TableAccounts.ACCOUNTS_cashbanktype,accounts.getACCOUNTS_cashbanktype());

            insertid=  sqLiteDatabase.update(Utils.DBtables.TABLE_ACCOUNTS,contentValues,Utils.TableAccounts.ACCOUNTS_id  + " = ?",
                    new String[] { String.valueOf(id) });


            sqLiteDatabase.close();









        }catch (Exception e)
        {

        }

        return insertid;
    }




public long addVisitCardData(String data,byte[]arr,byte[]cardimg)
{
    long insertedid=0l;
    try{

        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

        ContentValues contentValues=new ContentValues();
        contentValues.put("data",data);
        contentValues.put("logoimage",arr);
        contentValues.put("cardimg",cardimg);
        insertedid=   sqLiteDatabase.insert(Utils.DBtables.TABLE_VISITCARD,null,contentValues);


        sqLiteDatabase.close();





    }catch (Exception e)
    {

    }

    return insertedid;
}






    public long updateVisitCardData(String id,String data,byte[]arr,byte[]cardimg)
    {
        long insertedid=0l;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);
            contentValues.put("logoimage",arr);
            contentValues.put("cardimg",cardimg);

            //insertedid=   sqLiteDatabase.insert(Utils.DBtables.TABLE_VISITCARD,null,contentValues);

            sqLiteDatabase.update(Utils.DBtables.TABLE_VISITCARD,contentValues, "keyid = ?",
                    new String[] { String.valueOf(id) });
            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }

        return insertedid;
    }


















    public void deleteVisitCardData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete(Utils.DBtables.TABLE_VISITCARD,"keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }


    public List<VisitCard>getVisitCardData()
    {

        List<VisitCard>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+Utils.DBtables.TABLE_VISITCARD;

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    VisitCard contact = new VisitCard();
                    contact.setId(cursor.getInt(0));
                    contact.setData(cursor.getString(1));
                    contact.setBimage(cursor.getBlob(2));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }



    public long addVisitcardImgData(byte[] data)
    {

        long insertedid=0l;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);
            insertedid=   sqLiteDatabase.insert(Utils.DBtables.TABLE_VISITCARD_IMAGE,null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }

        return insertedid;

    }


    public long updateVisitcardImgData(byte[] data,String id)
    {

        long insertedid=0l;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);
            sqLiteDatabase.update(Utils.DBtables.TABLE_VISITCARD_IMAGE, contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });



            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }

        return insertedid;

    }


    public long addData(String table,String data)
    {

        long insertedid=0;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);
         insertedid=   sqLiteDatabase.insert(table,null,contentValues);


            sqLiteDatabase.close();

            if(!table.equalsIgnoreCase(Utils.DBtables.TABLE_ACCOUNTSETTINGS)&&!table.equalsIgnoreCase(Utils.DBtables.TABLE_BACKUP)) {


            }


        }catch (Exception e)
        {

        }

        return insertedid;

    }

    public long addTargetData(String table,String data,byte[] icon)
    {

        long insertedid=0l;
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);
            contentValues.put("iconimage",icon);
            insertedid=   sqLiteDatabase.insert(table,null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }

        return insertedid;

    }


    public List<TargetCategory>getTargetCategory()
    {
        List<TargetCategory>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+Utils.DBtables.TABLE_TARGETCATEGORY;

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    TargetCategory contact = new TargetCategory();
                    contact.setId(cursor.getString(0));
                    contact.setTask_category(cursor.getString(1));
                    contact.setImage(cursor.getBlob(2));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }

            // Utils.showAlertWithSingle(ct,e.toString(),null);


        }catch (Exception e)
        {
            Utils.showAlertWithSingle(ct,e.toString(),null);
        }

        return taskData;


    }


    public TargetCategory getTargetCategoryById(String id)
    {
   TargetCategory taskData=new TargetCategory();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            Cursor cursor=  sqLiteDatabase.rawQuery("SELECT * FROM "+Utils.DBtables.TABLE_TARGETCATEGORY+" WHERE keyid=?",new String[]{id});




            if (cursor.moveToFirst()) {
                do {

                    taskData.setId(cursor.getString(0));
                    taskData.setTask_category(cursor.getString(1));
                    taskData.setImage(cursor.getBlob(2));

                    // Adding contact to list

                } while (cursor.moveToNext());
            }

            // Utils.showAlertWithSingle(ct,e.toString(),null);


        }catch (Exception e)
        {
            Utils.showAlertWithSingle(ct,e.toString(),null);
        }

        return taskData;


    }




    public List<VisitCardImg>getImgData()
    {
        List<VisitCardImg>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+Utils.DBtables.TABLE_VISITCARD_IMAGE;

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    VisitCardImg contact = new VisitCardImg();
                    contact.setId(cursor.getString(0));
                    contact.setArrdata(cursor.getBlob(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }

           // Utils.showAlertWithSingle(ct,e.toString(),null);


        }catch (Exception e)
        {
Utils.showAlertWithSingle(ct,e.toString(),null);
        }

        return taskData;

    }











    public List<CommonData>getData(String tablename)
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  "+tablename;

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public void updateData(String id,String data,String table)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            ContentValues contentValues=new ContentValues();
            contentValues.put("data",data);

            sqLiteDatabase.update(table, contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });



        }catch (Exception e)
        {

        }

    }


    public void deleteData(String id,String table)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete(table,"keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }


    public  List<CommonData> getDataByID(String id,String table)
    {
        List<CommonData>commonData=new ArrayList<>();

        try {
            SQLiteDatabase db=this.getWritableDatabase();

            Cursor cursor=  db.rawQuery("SELECT * FROM "+table+" WHERE keyid=?",new String[]{id});


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    commonData.add(contact);
                } while (cursor.moveToNext());
            }

        }catch (Exception e)
        {

        }

        return commonData;
    }





























    public void addwalletData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("wallet_data",data);
            sqLiteDatabase.insert("WALLET_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }




    public void updateWalletData(String id,String data)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            ContentValues contentValues=new ContentValues();
            contentValues.put("wallet_data",data);

            sqLiteDatabase.update("WALLET_table", contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });



        }catch (Exception e)
        {

        }

    }





    public List<CommonData>getBankData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  WALLET_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public void deleteWalletData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("WALLET_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }





















    public void deleteReceiptData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("RECEIPT_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }




    public List<CommonData>getReceiptData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  RECEIPT_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }



    public void addreceiptData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("receipt_data",data);
            sqLiteDatabase.insert("RECEIPT_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }


    public void updateReceiptData(String id,String data)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            ContentValues contentValues=new ContentValues();
            contentValues.put("receipt_data",data);

            sqLiteDatabase.update("RECEIPT_table", contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });



        }catch (Exception e)
        {

        }

    }


    public void deleteLiabilityData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("LIABILITY_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }


    public void addliabilityData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("liability_data",data);
            sqLiteDatabase.insert("LIABILITY_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }



    public List<CommonData>getLiabilityData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  LIABILITY_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }





















    public void deleteLoanData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("LOAN_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }






    public void addloanData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("loan_data",data);
            sqLiteDatabase.insert("LOAN_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }


    public List<CommonData>getLoanData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  LOAN_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }























    public List<CommonData>getAssetData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  ASSET_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public void addassetData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("asset_data",data);
            sqLiteDatabase.insert("ASSET_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }


    public void updateAssetData(String id,String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("asset_data",data);
            sqLiteDatabase.update("ASSET_table", contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }


    public void deleteAssetData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("ASSET_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }



    public void updateCashBankData(String id,String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("cashbalancedata",data);
            sqLiteDatabase.update("CASHBALANCE_table", contentValues, "keyid" + " = ?",
                    new String[] { String.valueOf(id) });


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }


    public void deleteCashbankData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("CASHBALANCE_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }

    public void addCashbankData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("cashbalancedata",data);
            sqLiteDatabase.insert("CASHBALANCE_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }



    public List<CommonData>getBankbalanceData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  CASHBALANCE_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }







    public void deleteInvestData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("INVESTMENT_table","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }









    public List<CommonData>getInvestmentData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  INVESTMENT_table";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }


    public void addInvestmentData(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("investmentdata",data);
            sqLiteDatabase.insert("INVESTMENT_table",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }
















    public List<String>getInsuranceNumbers()
    {

        List<String>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  INSURANCE_NO";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {


                    // Adding contact to list
                    taskData.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }





    public void addInsuranceNumber(String data)
    {
        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();

            ContentValues contentValues=new ContentValues();
            contentValues.put("insuranceNO",data);
            sqLiteDatabase.insert("INSURANCE_NO",null,contentValues);


            sqLiteDatabase.close();





        }catch (Exception e)
        {

        }


    }




    public List<CommonData> getAccountSettingsByID(String id)
    {

        List<CommonData>commonData=new ArrayList<>();

        try {
            SQLiteDatabase db=this.getWritableDatabase();

          Cursor cursor=  db.rawQuery("SELECT * FROM TABLE_ACCOUNTSETTINGS WHERE keyid=?",new String[]{id});


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    commonData.add(contact);
                } while (cursor.moveToNext());
            }

        }catch (Exception e)
        {

        }

        return commonData;
    }


    public void deleteAccountsettingData(String id)
    {

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();


            sqLiteDatabase.delete("TABLE_ACCOUNTSETTINGS","keyid" + " = ?",
                    new String[] { String.valueOf(id)});



        }catch (Exception e)
        {

        }

    }




    public List<CommonData>getAccountSettingsData()
    {

        List<CommonData>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  TABLE_ACCOUNTSETTINGS";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    CommonData contact = new CommonData();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }

//    public void addTarget(Target trg)
//    {
//
//        try{
//
//            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
//
//            ContentValues contentValues=new ContentValues();
//            contentValues.put("insuranceNO",data);
//            sqLiteDatabase.insert("INSURANCE_NO",null,contentValues);
//
//
//            sqLiteDatabase.close();
//
//
//
//
//
//        }catch (Exception e)
//        {
//
//        }
//
//    }










    public List<Budget>getBudgetData()
    {

        List<Budget>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  TABLE_BUDGET";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    Budget contact = new Budget();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }








    public List<PaymentVoucher>getPaymentVoucherData()
    {

        List<PaymentVoucher>taskData=new ArrayList<>();

        try{

            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            String query="SELECT  * FROM  TABLE_PAYMENTVOUCHER";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);


            if (cursor.moveToFirst()) {
                do {
                    PaymentVoucher contact = new PaymentVoucher();
                    contact.setId(cursor.getString(0));
                    contact.setData(cursor.getString(1));

                    // Adding contact to list
                    taskData.add(contact);
                } while (cursor.moveToNext());
            }


        }catch (Exception e)
        {

        }

        return taskData;
    }








    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {


        try{

            String CREATE_EMERGENCY = "CREATE TABLE " + Utils.DBtables.TABLE_EMERGENCY + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            sqLiteDatabase.execSQL(CREATE_EMERGENCY);


        }catch (Exception e)
        {

        }




        try{

            String CREATE_WEBLINKS = "CREATE TABLE " + Utils.DBtables.TABLE_WEBLINKS + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            sqLiteDatabase.execSQL(CREATE_WEBLINKS);


        }catch (Exception e)
        {

        }





        try{
            String CREATE_RENEWALMSGTABLE = "CREATE TABLE " + Utils.DBtables.TABLE_RENEWALMSG + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            sqLiteDatabase.execSQL(CREATE_RENEWALMSGTABLE);


        }catch (Exception e)
        {

        }

        try{

            String CREATE_BACKUPTABLE = "CREATE TABLE " + Utils.DBtables.TABLE_BACKUP + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            sqLiteDatabase.execSQL(CREATE_BACKUPTABLE);

        }catch (Exception e)
        {

        }

        try{

            String CREATE_ddedamount_INMILESTONE = "CREATE TABLE " + Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " +")";

            sqLiteDatabase.execSQL(CREATE_ddedamount_INMILESTONE);

        }catch (Exception e)
        {

        }

        try{

            String CREATE_TABLE_MILESTONE = "CREATE TABLE " + Utils.DBtables.TABLE_MILESTONE + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                    + ")";

            sqLiteDatabase.execSQL(CREATE_TABLE_MILESTONE);

        }catch (Exception e)
        {

        }




        try {

            String CREATE_TARGET_TABLE = "CREATE TABLE " + Utils.DBtables.TABLE_TARGET + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT " + ")";

            sqLiteDatabase.execSQL(CREATE_TARGET_TABLE);
        }catch (Exception e)
        {

        }

        try{

            String CREATE_TARGET = "CREATE TABLE " + Utils.DBtables.TABLE_TARGETCATEGORY + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT, "+ "iconimage"+" BLOB"
                    + ")";

            sqLiteDatabase.execSQL(CREATE_TARGET);

        }catch (Exception e)
        {

        }

        try {

            sqLiteDatabase.execSQL("ALTER TABLE "+Utils.DBtables.TABLE_VISITCARD+" ADD COLUMN cardimg BLOB");
        }
        catch (Exception e)
        {

        }

        try {
            String CREATE_VISIT_CARD_IMAGE = "CREATE TABLE " + Utils.DBtables.TABLE_VISITCARD_IMAGE + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " BLOB "
                    + ")";

            sqLiteDatabase.execSQL(CREATE_VISIT_CARD_IMAGE);
        }
        catch (Exception e)
        {

        }


        try {
            String CREATE_PASSWORD = "CREATE TABLE " + Utils.DBtables.TABLE_PASSWORD + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                    + ")";

            sqLiteDatabase.execSQL(CREATE_PASSWORD);
        }catch (Exception e)
        {

        }

        try {
            String CREATE_document = "CREATE TABLE " + Utils.DBtables.TABLE_DOCUMENT + "("
                    + "keyid" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "data" + " TEXT "
                    + ")";

            sqLiteDatabase.execSQL(CREATE_document);
        }
        catch (Exception e)
        {

        }


    }
}
