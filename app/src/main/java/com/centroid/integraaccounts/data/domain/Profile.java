package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("join_date")
    @Expose
    private Object joinDate;
    @SerializedName("activation_date")
    @Expose
    private Object activationDate;
    @SerializedName("activation_key")
    @Expose
    private String activationKey;
    @SerializedName("default_lang")
    @Expose
    private String defaultLang;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("gdrive_fileid")
    @Expose
    private String gdrive_fileid;
    @SerializedName("drive_mailId")
    @Expose
    private String drive_mailId;
    @SerializedName("serverbackup_fileid")
    @Expose
    private String serverbackup_fileid;

    public Profile() {
    }

    public String getServerbackup_fileid() {
        return serverbackup_fileid;
    }

    public void setServerbackup_fileid(String serverbackup_fileid) {
        this.serverbackup_fileid = serverbackup_fileid;
    }

    public String getDrive_mailId() {
        return drive_mailId;
    }

    public void setDrive_mailId(String drive_mailId) {
        this.drive_mailId = drive_mailId;
    }

    public String getGdrive_fileid() {
        return gdrive_fileid;
    }

    public void setGdrive_fileid(String gdrive_fileid) {
        this.gdrive_fileid = gdrive_fileid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Object getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Object joinDate) {
        this.joinDate = joinDate;
    }

    public Object getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Object activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
