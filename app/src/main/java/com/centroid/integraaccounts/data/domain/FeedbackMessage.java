package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackMessage {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("reg_id")
    @Expose
    private String regId;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("send_date")
    @Expose
    private String sendDate;
    @SerializedName("feedback_msg")
    @Expose
    private String feedbackMsg;
    @SerializedName("reply_msg")
    @Expose
    private Object replyMsg;
    @SerializedName("reply_date")
    @Expose
    private Object replyDate;
    @SerializedName("replied_userid")
    @Expose
    private Object repliedUserid;
    @SerializedName("commit_status")
    @Expose
    private String commitStatus;

    public FeedbackMessage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getFeedbackMsg() {
        return feedbackMsg;
    }

    public void setFeedbackMsg(String feedbackMsg) {
        this.feedbackMsg = feedbackMsg;
    }

    public Object getReplyMsg() {
        return replyMsg;
    }

    public void setReplyMsg(Object replyMsg) {
        this.replyMsg = replyMsg;
    }

    public Object getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(Object replyDate) {
        this.replyDate = replyDate;
    }

    public Object getRepliedUserid() {
        return repliedUserid;
    }

    public void setRepliedUserid(Object repliedUserid) {
        this.repliedUserid = repliedUserid;
    }

    public String getCommitStatus() {
        return commitStatus;
    }

    public void setCommitStatus(String commitStatus) {
        this.commitStatus = commitStatus;
    }
}
