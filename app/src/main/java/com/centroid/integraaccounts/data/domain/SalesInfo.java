package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesInfo {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("billno_prefix")
    @Expose
    private String billnoPrefix;
    @SerializedName("bill_no")
    @Expose
    private String billNo;
    @SerializedName("reg_id")
    @Expose
    private String regId;
    @SerializedName("state_id")
    @Expose
    private Object stateId;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("sales_type")
    @Expose
    private String salesType;
    @SerializedName("sales_date")
    @Expose
    private String salesDate;
    @SerializedName("expe_date")
    @Expose
    private String expeDate;
    @SerializedName("amt")
    @Expose
    private String amt;
    @SerializedName("binary_val")
    @Expose
    private String binaryVal;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("ex_rate ")
    @Expose
    private Object exRate;
    @SerializedName("rupee_convertion_val")
    @Expose
    private String rupeeConvertionVal;
    @SerializedName("sponser_reg_id")
    @Expose
    private String sponserRegId;
    @SerializedName("sponser_reg_code")
    @Expose
    private String sponserRegCode;
    @SerializedName("referal_commission_rupee")
    @Expose
    private String referalCommissionRupee;
    @SerializedName("sponser_currency")
    @Expose
    private String sponserCurrency;
    @SerializedName("sponser_cur_ex_rate")
    @Expose
    private String sponserCurExRate;
    @SerializedName("sponser_conversion_value")
    @Expose
    private String sponserConversionValue;
    @SerializedName("binary_gen_status")
    @Expose
    private String binaryGenStatus;

    public SalesInfo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillnoPrefix() {
        return billnoPrefix;
    }

    public void setBillnoPrefix(String billnoPrefix) {
        this.billnoPrefix = billnoPrefix;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public Object getStateId() {
        return stateId;
    }

    public void setStateId(Object stateId) {
        this.stateId = stateId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(String salesDate) {
        this.salesDate = salesDate;
    }

    public String getExpeDate() {
        return expeDate;
    }

    public void setExpeDate(String expeDate) {
        this.expeDate = expeDate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getBinaryVal() {
        return binaryVal;
    }

    public void setBinaryVal(String binaryVal) {
        this.binaryVal = binaryVal;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Object getExRate() {
        return exRate;
    }

    public void setExRate(Object exRate) {
        this.exRate = exRate;
    }

    public String getRupeeConvertionVal() {
        return rupeeConvertionVal;
    }

    public void setRupeeConvertionVal(String rupeeConvertionVal) {
        this.rupeeConvertionVal = rupeeConvertionVal;
    }

    public String getSponserRegId() {
        return sponserRegId;
    }

    public void setSponserRegId(String sponserRegId) {
        this.sponserRegId = sponserRegId;
    }

    public String getSponserRegCode() {
        return sponserRegCode;
    }

    public void setSponserRegCode(String sponserRegCode) {
        this.sponserRegCode = sponserRegCode;
    }

    public String getReferalCommissionRupee() {
        return referalCommissionRupee;
    }

    public void setReferalCommissionRupee(String referalCommissionRupee) {
        this.referalCommissionRupee = referalCommissionRupee;
    }

    public String getSponserCurrency() {
        return sponserCurrency;
    }

    public void setSponserCurrency(String sponserCurrency) {
        this.sponserCurrency = sponserCurrency;
    }

    public String getSponserCurExRate() {
        return sponserCurExRate;
    }

    public void setSponserCurExRate(String sponserCurExRate) {
        this.sponserCurExRate = sponserCurExRate;
    }

    public String getSponserConversionValue() {
        return sponserConversionValue;
    }

    public void setSponserConversionValue(String sponserConversionValue) {
        this.sponserConversionValue = sponserConversionValue;
    }

    public String getBinaryGenStatus() {
        return binaryGenStatus;
    }

    public void setBinaryGenStatus(String binaryGenStatus) {
        this.binaryGenStatus = binaryGenStatus;
    }
}
