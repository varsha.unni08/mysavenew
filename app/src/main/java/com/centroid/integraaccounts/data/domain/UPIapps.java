package com.centroid.integraaccounts.data.domain;

public class UPIapps {
    String name;
    String packagename;
    int icon;

    public UPIapps() {
    }

    public UPIapps(String name, String packagename, int icon) {
        this.name = name;
        this.packagename = packagename;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
