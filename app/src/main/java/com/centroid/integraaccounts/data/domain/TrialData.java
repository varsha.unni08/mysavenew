package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrialData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("activation_date")
    @Expose
    private String activationDate;
    @SerializedName("join_date")
    @Expose
    private String join_date;
    @SerializedName("trialstart_date")
    @Expose
    private String trialstart_date;

    public TrialData() {
    }

    public String getTrialstart_date() {
        return trialstart_date;
    }

    public void setTrialstart_date(String trialstart_date) {
        this.trialstart_date = trialstart_date;
    }

    public String getJoin_date() {
        return join_date;
    }

    public void setJoin_date(String join_date) {
        this.join_date = join_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }
}
