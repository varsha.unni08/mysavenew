package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Notificationmessage implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdby_id")
    @Expose
    private String createdbyId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("message_validity")
    @Expose
    private String messageValidity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("verified_status")
    @Expose
    private String verifiedStatus;
    @SerializedName("user_type")
    @Expose
    private Object userType;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("free_byuser")
    @Expose
    private Object freeByuser;
    @SerializedName("active_byuser")
    @Expose
    private Object activeByuser;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("full_name")
    @Expose
    private String fullName;

    public Notificationmessage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedbyId() {
        return createdbyId;
    }

    public void setCreatedbyId(String createdbyId) {
        this.createdbyId = createdbyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessageValidity() {
        return messageValidity;
    }

    public void setMessageValidity(String messageValidity) {
        this.messageValidity = messageValidity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public Object getUserType() {
        return userType;
    }

    public void setUserType(Object userType) {
        this.userType = userType;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public Object getFreeByuser() {
        return freeByuser;
    }

    public void setFreeByuser(Object freeByuser) {
        this.freeByuser = freeByuser;
    }

    public Object getActiveByuser() {
        return activeByuser;
    }

    public void setActiveByuser(Object activeByuser) {
        this.activeByuser = activeByuser;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
