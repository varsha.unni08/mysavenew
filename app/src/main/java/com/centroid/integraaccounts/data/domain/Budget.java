package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class Budget implements Serializable {

    String id;
    String data;

    public Budget() {
    }

    public Budget(String id, String data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
