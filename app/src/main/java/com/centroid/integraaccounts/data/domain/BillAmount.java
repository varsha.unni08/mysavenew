package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BillAmount implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private BillData data=new BillData();

    @SerializedName("settingsdata")
    @Expose
    private SettingsData settingsdata=new SettingsData();

    public BillAmount() {
    }

    public SettingsData getSettingsdata() {
        return settingsdata;
    }

    public void setSettingsdata(SettingsData settingsdata) {
        this.settingsdata = settingsdata;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BillData getData() {
        return data;
    }

    public void setData(BillData data) {
        this.data = data;
    }
}
