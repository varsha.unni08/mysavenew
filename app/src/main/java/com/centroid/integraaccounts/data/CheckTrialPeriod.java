package com.centroid.integraaccounts.data;

import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.interfaces.ExpirydateCheckListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckTrialPeriod {

    Context context;

    ExpirydateCheckListener expirydateCheckListener;

    public CheckTrialPeriod(Context context, ExpirydateCheckListener expirydateCheckListener) {
        this.context = context;
        this.expirydateCheckListener = expirydateCheckListener;
    }


    public void checkTrialPeriodData()
    {



//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"fkjfk");

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",Utils.getTimestamp());


        new RequestHandler(context, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //progressFragment.dismiss();
                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                try {

                    JSONObject jsonObject1=new JSONObject(data);


                    if(jsonObject1!=null)
                    {

                        if(jsonObject1!=null)
                        {

                            if(expirydateCheckListener!=null)
                            {
                                expirydateCheckListener.OnExpirydateChecked(jsonObject1);
                            }


                        }


                    }


                }catch (Exception e)
                {

                }

            }

            @Override
            public void onFailure(String err) {
             //   progressFragment.dismiss();

                Utils.showAlertWithSingle(context, err, new DialogEventListener() {
                    @Override
                    public void onPositiveButtonClicked() {

                    }

                    @Override
                    public void onNegativeButtonClicked() {

                    }
                });

            }
        },Utils.WebServiceMethodes.validateTrialPeriod+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();











    }
}
