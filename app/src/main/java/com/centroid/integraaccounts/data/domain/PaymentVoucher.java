package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class PaymentVoucher implements Serializable {

    String id;
    String data;

    public PaymentVoucher() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
