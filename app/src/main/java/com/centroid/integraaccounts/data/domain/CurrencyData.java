package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CurrencyData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("currency_code")
    @Expose
    private String currency_code;
    @SerializedName("conversion_value")
    @Expose
    private String conversion_value;

    public CurrencyData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getConversion_value() {
        return conversion_value;
    }

    public void setConversion_value(String conversion_value) {
        this.conversion_value = conversion_value;
    }
}
