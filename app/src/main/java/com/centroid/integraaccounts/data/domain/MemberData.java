package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("reg_id")
    @Expose
    private String regId;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("activation_date")
    @Expose
    private String activationDate;
    @SerializedName("activation_key")
    @Expose
    private String activationKey;
    @SerializedName("join_source")
    @Expose
    private String joinSource;
    @SerializedName("used_link_for_registration")
    @Expose
    private Object usedLinkForRegistration;
    @SerializedName("sponser_reg_id")
    @Expose
    private String sponserRegId;
    @SerializedName("sponser_reg_code")
    @Expose
    private String sponserRegCode;
    @SerializedName("parent_reg_id")
    @Expose
    private Object parentRegId;
    @SerializedName("parent_reg_code")
    @Expose
    private Object parentRegCode;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("encr_password")
    @Expose
    private String encrPassword;
    @SerializedName("default_lang")
    @Expose
    private String defaultLang;
    @SerializedName("sponsed_count_left")
    @Expose
    private String sponsedCountLeft;
    @SerializedName("sponsed_count_right")
    @Expose
    private String sponsedCountRight;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("position_downline_setup_default")
    @Expose
    private String positionDownlineSetupDefault;
    @SerializedName("position_next")
    @Expose
    private String positionNext;
    @SerializedName("bank_account_no")
    @Expose
    private Object bankAccountNo;
    @SerializedName("bank_account_name")
    @Expose
    private Object bankAccountName;
    @SerializedName("bank_name")
    @Expose
    private Object bankName;
    @SerializedName("branch_name")
    @Expose
    private Object branchName;
    @SerializedName("pan_card")
    @Expose
    private Object panCard;
    @SerializedName("binary_left")
    @Expose
    private String binaryLeft;
    @SerializedName("binary_right")
    @Expose
    private String binaryRight;
    @SerializedName("binary_matched")
    @Expose
    private String binaryMatched;
    @SerializedName("carry_left")
    @Expose
    private String carryLeft;
    @SerializedName("carry_right")
    @Expose
    private String carryRight;
    @SerializedName("binary_amt")
    @Expose
    private String binaryAmt;
    @SerializedName("binary_amt_pre")
    @Expose
    private String binaryAmtPre;
    @SerializedName("binary_lastgn_date")
    @Expose
    private String binaryLastgnDate;
    @SerializedName("referral_commission_amt")
    @Expose
    private String referralCommissionAmt;
    @SerializedName("referral_commission_amt_pre")
    @Expose
    private String referralCommissionAmtPre;
    @SerializedName("referral_commission_lastgn_date")
    @Expose
    private Object referralCommissionLastgnDate;
    @SerializedName("level_amt")
    @Expose
    private String levelAmt;
    @SerializedName("level_amt_pre")
    @Expose
    private String levelAmtPre;
    @SerializedName("level_lastgn_date")
    @Expose
    private Object levelLastgnDate;
    @SerializedName("achievement_amt")
    @Expose
    private String achievementAmt;
    @SerializedName("achievement_amt_pre")
    @Expose
    private String achievementAmtPre;
    @SerializedName("achievement_lastgn_date")
    @Expose
    private Object achievementLastgnDate;
    @SerializedName("member_cancel")
    @Expose
    private String memberCancel;
    @SerializedName("member_terminated")
    @Expose
    private String memberTerminated;
    @SerializedName("profile_photo")
    @Expose
    private Object profilePhoto;
    @SerializedName("pan_no")
    @Expose
    private Object panNo;
    @SerializedName("pan_photo")
    @Expose
    private Object panPhoto;
    @SerializedName("adhar_no")
    @Expose
    private Object adharNo;
    @SerializedName("adhar_photo")
    @Expose
    private Object adharPhoto;
    @SerializedName("bank_checkleaf_photo")
    @Expose
    private Object bankCheckleafPhoto;
    @SerializedName("branch")
    @Expose
    private Object branch;
    @SerializedName("ifsc")
    @Expose
    private Object ifsc;
    @SerializedName("updatestatus")
    @Expose
    private String updatestatus;
    @SerializedName("remark_varification")
    @Expose
    private String remarkVarification;
    @SerializedName("last_varified_by")
    @Expose
    private Object lastVarifiedBy;
    @SerializedName("last_varified_date")
    @Expose
    private Object lastVarifiedDate;


    public MemberData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getJoinSource() {
        return joinSource;
    }

    public void setJoinSource(String joinSource) {
        this.joinSource = joinSource;
    }

    public Object getUsedLinkForRegistration() {
        return usedLinkForRegistration;
    }

    public void setUsedLinkForRegistration(Object usedLinkForRegistration) {
        this.usedLinkForRegistration = usedLinkForRegistration;
    }

    public String getSponserRegId() {
        return sponserRegId;
    }

    public void setSponserRegId(String sponserRegId) {
        this.sponserRegId = sponserRegId;
    }

    public String getSponserRegCode() {
        return sponserRegCode;
    }

    public void setSponserRegCode(String sponserRegCode) {
        this.sponserRegCode = sponserRegCode;
    }

    public Object getParentRegId() {
        return parentRegId;
    }

    public void setParentRegId(Object parentRegId) {
        this.parentRegId = parentRegId;
    }

    public Object getParentRegCode() {
        return parentRegCode;
    }

    public void setParentRegCode(Object parentRegCode) {
        this.parentRegCode = parentRegCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncrPassword() {
        return encrPassword;
    }

    public void setEncrPassword(String encrPassword) {
        this.encrPassword = encrPassword;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getSponsedCountLeft() {
        return sponsedCountLeft;
    }

    public void setSponsedCountLeft(String sponsedCountLeft) {
        this.sponsedCountLeft = sponsedCountLeft;
    }

    public String getSponsedCountRight() {
        return sponsedCountRight;
    }

    public void setSponsedCountRight(String sponsedCountRight) {
        this.sponsedCountRight = sponsedCountRight;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionDownlineSetupDefault() {
        return positionDownlineSetupDefault;
    }

    public void setPositionDownlineSetupDefault(String positionDownlineSetupDefault) {
        this.positionDownlineSetupDefault = positionDownlineSetupDefault;
    }

    public String getPositionNext() {
        return positionNext;
    }

    public void setPositionNext(String positionNext) {
        this.positionNext = positionNext;
    }

    public Object getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(Object bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public Object getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(Object bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public Object getBranchName() {
        return branchName;
    }

    public void setBranchName(Object branchName) {
        this.branchName = branchName;
    }

    public Object getPanCard() {
        return panCard;
    }

    public void setPanCard(Object panCard) {
        this.panCard = panCard;
    }

    public String getBinaryLeft() {
        return binaryLeft;
    }

    public void setBinaryLeft(String binaryLeft) {
        this.binaryLeft = binaryLeft;
    }

    public String getBinaryRight() {
        return binaryRight;
    }

    public void setBinaryRight(String binaryRight) {
        this.binaryRight = binaryRight;
    }

    public String getBinaryMatched() {
        return binaryMatched;
    }

    public void setBinaryMatched(String binaryMatched) {
        this.binaryMatched = binaryMatched;
    }

    public String getCarryLeft() {
        return carryLeft;
    }

    public void setCarryLeft(String carryLeft) {
        this.carryLeft = carryLeft;
    }

    public String getCarryRight() {
        return carryRight;
    }

    public void setCarryRight(String carryRight) {
        this.carryRight = carryRight;
    }

    public String getBinaryAmt() {
        return binaryAmt;
    }

    public void setBinaryAmt(String binaryAmt) {
        this.binaryAmt = binaryAmt;
    }

    public String getBinaryAmtPre() {
        return binaryAmtPre;
    }

    public void setBinaryAmtPre(String binaryAmtPre) {
        this.binaryAmtPre = binaryAmtPre;
    }

    public String getBinaryLastgnDate() {
        return binaryLastgnDate;
    }

    public void setBinaryLastgnDate(String binaryLastgnDate) {
        this.binaryLastgnDate = binaryLastgnDate;
    }

    public String getReferralCommissionAmt() {
        return referralCommissionAmt;
    }

    public void setReferralCommissionAmt(String referralCommissionAmt) {
        this.referralCommissionAmt = referralCommissionAmt;
    }

    public String getReferralCommissionAmtPre() {
        return referralCommissionAmtPre;
    }

    public void setReferralCommissionAmtPre(String referralCommissionAmtPre) {
        this.referralCommissionAmtPre = referralCommissionAmtPre;
    }

    public Object getReferralCommissionLastgnDate() {
        return referralCommissionLastgnDate;
    }

    public void setReferralCommissionLastgnDate(Object referralCommissionLastgnDate) {
        this.referralCommissionLastgnDate = referralCommissionLastgnDate;
    }

    public String getLevelAmt() {
        return levelAmt;
    }

    public void setLevelAmt(String levelAmt) {
        this.levelAmt = levelAmt;
    }

    public String getLevelAmtPre() {
        return levelAmtPre;
    }

    public void setLevelAmtPre(String levelAmtPre) {
        this.levelAmtPre = levelAmtPre;
    }

    public Object getLevelLastgnDate() {
        return levelLastgnDate;
    }

    public void setLevelLastgnDate(Object levelLastgnDate) {
        this.levelLastgnDate = levelLastgnDate;
    }

    public String getAchievementAmt() {
        return achievementAmt;
    }

    public void setAchievementAmt(String achievementAmt) {
        this.achievementAmt = achievementAmt;
    }

    public String getAchievementAmtPre() {
        return achievementAmtPre;
    }

    public void setAchievementAmtPre(String achievementAmtPre) {
        this.achievementAmtPre = achievementAmtPre;
    }

    public Object getAchievementLastgnDate() {
        return achievementLastgnDate;
    }

    public void setAchievementLastgnDate(Object achievementLastgnDate) {
        this.achievementLastgnDate = achievementLastgnDate;
    }

    public String getMemberCancel() {
        return memberCancel;
    }

    public void setMemberCancel(String memberCancel) {
        this.memberCancel = memberCancel;
    }

    public String getMemberTerminated() {
        return memberTerminated;
    }

    public void setMemberTerminated(String memberTerminated) {
        this.memberTerminated = memberTerminated;
    }

    public Object getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(Object profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Object getPanNo() {
        return panNo;
    }

    public void setPanNo(Object panNo) {
        this.panNo = panNo;
    }

    public Object getPanPhoto() {
        return panPhoto;
    }

    public void setPanPhoto(Object panPhoto) {
        this.panPhoto = panPhoto;
    }

    public Object getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(Object adharNo) {
        this.adharNo = adharNo;
    }

    public Object getAdharPhoto() {
        return adharPhoto;
    }

    public void setAdharPhoto(Object adharPhoto) {
        this.adharPhoto = adharPhoto;
    }

    public Object getBankCheckleafPhoto() {
        return bankCheckleafPhoto;
    }

    public void setBankCheckleafPhoto(Object bankCheckleafPhoto) {
        this.bankCheckleafPhoto = bankCheckleafPhoto;
    }

    public Object getBranch() {
        return branch;
    }

    public void setBranch(Object branch) {
        this.branch = branch;
    }

    public Object getIfsc() {
        return ifsc;
    }

    public void setIfsc(Object ifsc) {
        this.ifsc = ifsc;
    }

    public String getUpdatestatus() {
        return updatestatus;
    }

    public void setUpdatestatus(String updatestatus) {
        this.updatestatus = updatestatus;
    }

    public String getRemarkVarification() {
        return remarkVarification;
    }

    public void setRemarkVarification(String remarkVarification) {
        this.remarkVarification = remarkVarification;
    }

    public Object getLastVarifiedBy() {
        return lastVarifiedBy;
    }

    public void setLastVarifiedBy(Object lastVarifiedBy) {
        this.lastVarifiedBy = lastVarifiedBy;
    }

    public Object getLastVarifiedDate() {
        return lastVarifiedDate;
    }

    public void setLastVarifiedDate(Object lastVarifiedDate) {
        this.lastVarifiedDate = lastVarifiedDate;
    }
}
