package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class CommonData implements Serializable {

    String id;
    String data;

    int isalreadyMapped=0;

    public CommonData(String id, String data, int isalreadyMapped) {
        this.id = id;
        this.data = data;
        this.isalreadyMapped=isalreadyMapped;
    }

    public CommonData() {
    }

    public int getIsalreadyMapped() {
        return isalreadyMapped;
    }

    public void setIsalreadyMapped(int isalreadyMapped) {
        this.isalreadyMapped = isalreadyMapped;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
