package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class Accounts implements Serializable {

    public  int ACCOUNTS_id;
    public  String ACCOUNTS_entryid="";

    public  String ACCOUNTS_date="";


    public  String ACCOUNTS_setupid="";


    public  String ACCOUNTS_amount="";


    public  String ACCOUNTS_type="";


    public  String ACCOUNTS_remarks="";

    public  String ACCOUNTS_month="";


    public  String ACCOUNTS_year="";

    String ACCOUNTS_cashbanktype="";

    int ACCOUNTS_vouchertype=0;

    int ACCOUNTS_billId=0;

    public String closingbalance="";

    public String billvouchernumber="";

    public Accounts() {
    }

    public String getBillvouchernumber() {
        return billvouchernumber;
    }

    public void setBillvouchernumber(String billvouchernumber) {
        this.billvouchernumber = billvouchernumber;
    }

    public String getClosingbalance() {
        return closingbalance;
    }

    public void setClosingbalance(String closingbalance) {
        this.closingbalance = closingbalance;
    }

    public int getACCOUNTS_billId() {
        return ACCOUNTS_billId;
    }

    public void setACCOUNTS_billId(int ACCOUNTS_billId) {
        this.ACCOUNTS_billId = ACCOUNTS_billId;
    }

    public int getACCOUNTS_vouchertype() {
        return ACCOUNTS_vouchertype;
    }

    public void setACCOUNTS_vouchertype(int ACCOUNTS_vouchertype) {
        this.ACCOUNTS_vouchertype = ACCOUNTS_vouchertype;
    }

    public String getACCOUNTS_cashbanktype() {
        return ACCOUNTS_cashbanktype;
    }

    public void setACCOUNTS_cashbanktype(String ACCOUNTS_cashbanktype) {
        this.ACCOUNTS_cashbanktype = ACCOUNTS_cashbanktype;
    }

    public String getACCOUNTS_month() {
        return ACCOUNTS_month;
    }

    public void setACCOUNTS_month(String ACCOUNTS_month) {
        this.ACCOUNTS_month = ACCOUNTS_month;
    }

    public String getACCOUNTS_year() {
        return ACCOUNTS_year;
    }

    public void setACCOUNTS_year(String ACCOUNTS_year) {
        this.ACCOUNTS_year = ACCOUNTS_year;
    }

    public int getACCOUNTS_id() {
        return ACCOUNTS_id;
    }

    public void setACCOUNTS_id(int ACCOUNTS_id) {
        this.ACCOUNTS_id = ACCOUNTS_id;
    }

    public String getACCOUNTS_entryid() {
        return ACCOUNTS_entryid;
    }

    public void setACCOUNTS_entryid(String ACCOUNTS_entryid) {
        this.ACCOUNTS_entryid = ACCOUNTS_entryid;
    }

    public String getACCOUNTS_date() {
        return ACCOUNTS_date;
    }

    public void setACCOUNTS_date(String ACCOUNTS_date) {
        this.ACCOUNTS_date = ACCOUNTS_date;
    }

    public String getACCOUNTS_setupid() {
        return ACCOUNTS_setupid;
    }

    public void setACCOUNTS_setupid(String ACCOUNTS_setupid) {
        this.ACCOUNTS_setupid = ACCOUNTS_setupid;
    }

    public String getACCOUNTS_amount() {
        return ACCOUNTS_amount;
    }

    public void setACCOUNTS_amount(String ACCOUNTS_amount) {
        this.ACCOUNTS_amount = ACCOUNTS_amount;
    }

    public String getACCOUNTS_type() {
        return ACCOUNTS_type;
    }

    public void setACCOUNTS_type(String ACCOUNTS_type) {
        this.ACCOUNTS_type = ACCOUNTS_type;
    }

    public String getACCOUNTS_remarks() {
        return ACCOUNTS_remarks;
    }

    public void setACCOUNTS_remarks(String ACCOUNTS_remarks) {
        this.ACCOUNTS_remarks = ACCOUNTS_remarks;
    }
}
