package com.centroid.integraaccounts.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileRechargeResponse {
    @SerializedName("account")
    @Expose
    private String account="";
    @SerializedName("amount")
    @Expose
    private Double amount=0.0;
    @SerializedName("rpid")
    @Expose
    private String rpid="";
    @SerializedName("agentid")
    @Expose
    private String agentid="";
    @SerializedName("opid")
    @Expose
    private String opid="";
    @SerializedName("isRefundStatusShow")
    @Expose
    private Boolean isRefundStatusShow=false;
    @SerializedName("status")
    @Expose
    private Integer status=0;
    @SerializedName("msg")
    @Expose
    private String msg="";
    @SerializedName("bal")
    @Expose
    private Double bal=0.0;
    @SerializedName("errorcode")
    @Expose
    private String errorcode="";

    public MobileRechargeResponse() {
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRpid() {
        return rpid;
    }

    public void setRpid(String rpid) {
        this.rpid = rpid;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getOpid() {
        return opid;
    }

    public void setOpid(String opid) {
        this.opid = opid;
    }

    public Boolean getRefundStatusShow() {
        return isRefundStatusShow;
    }

    public void setRefundStatusShow(Boolean refundStatusShow) {
        isRefundStatusShow = refundStatusShow;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Double getBal() {
        return bal;
    }

    public void setBal(Double bal) {
        this.bal = bal;
    }

    public String getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }
}
