package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NetworkData {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("position_downline_setup_default")
    @Expose
    private String positionDownlineSetupDefault;
    @SerializedName("position_next")
    @Expose
    private String positionNext;

    @SerializedName("binary_left")
    @Expose
    private String binaryLeft;
    @SerializedName("binary_right")
    @Expose
    private String binaryRight;
    @SerializedName("binary_matched")
    @Expose
    private String binaryMatched;
    @SerializedName("carry_left")
    @Expose
    private String carryLeft;
    @SerializedName("carry_right")
    @Expose
    private String carryRight;
    @SerializedName("binary_amt")
    @Expose
    private String binaryAmt;

    @SerializedName("referral_commission_amt")
    @Expose
    private String referralCommissionAmt;

    @SerializedName("level_amt")
    @Expose
    private String levelAmt;

    @SerializedName("achievement_amt")
    @Expose
    private String achievementAmt;
    @SerializedName("member_status")
    @Expose
    private String member_status;


    public NetworkData() {
    }

    public String getMember_status() {
        return member_status;
    }

    public void setMember_status(String member_status) {
        this.member_status = member_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionDownlineSetupDefault() {
        return positionDownlineSetupDefault;
    }

    public void setPositionDownlineSetupDefault(String positionDownlineSetupDefault) {
        this.positionDownlineSetupDefault = positionDownlineSetupDefault;
    }

    public String getPositionNext() {
        return positionNext;
    }

    public void setPositionNext(String positionNext) {
        this.positionNext = positionNext;
    }

    public String getBinaryLeft() {
        return binaryLeft;
    }

    public void setBinaryLeft(String binaryLeft) {
        this.binaryLeft = binaryLeft;
    }

    public String getBinaryRight() {
        return binaryRight;
    }

    public void setBinaryRight(String binaryRight) {
        this.binaryRight = binaryRight;
    }

    public String getBinaryMatched() {
        return binaryMatched;
    }

    public void setBinaryMatched(String binaryMatched) {
        this.binaryMatched = binaryMatched;
    }

    public String getCarryLeft() {
        return carryLeft;
    }

    public void setCarryLeft(String carryLeft) {
        this.carryLeft = carryLeft;
    }

    public String getCarryRight() {
        return carryRight;
    }

    public void setCarryRight(String carryRight) {
        this.carryRight = carryRight;
    }

    public String getBinaryAmt() {
        return binaryAmt;
    }

    public void setBinaryAmt(String binaryAmt) {
        this.binaryAmt = binaryAmt;
    }

    public String getReferralCommissionAmt() {
        return referralCommissionAmt;
    }

    public void setReferralCommissionAmt(String referralCommissionAmt) {
        this.referralCommissionAmt = referralCommissionAmt;
    }

    public String getLevelAmt() {
        return levelAmt;
    }

    public void setLevelAmt(String levelAmt) {
        this.levelAmt = levelAmt;
    }

    public String getAchievementAmt() {
        return achievementAmt;
    }

    public void setAchievementAmt(String achievementAmt) {
        this.achievementAmt = achievementAmt;
    }
}
