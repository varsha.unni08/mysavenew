package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentCustomer {

    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("id")
    @Expose
    private String id;



    @SerializedName("entity")
    @Expose
    private String entity;

    public PaymentCustomer() {
    }

    public PaymentCustomer(String contactNumber, String emailId, String id, String entity) {
        this.contactNumber = contactNumber;
        this.emailId = emailId;
        this.id = id;
        this.entity = entity;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }
}
