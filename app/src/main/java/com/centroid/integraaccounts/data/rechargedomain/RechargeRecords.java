package com.centroid.integraaccounts.data.rechargedomain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeRecords {

    @SerializedName("rs")
    @Expose
    private String rs;
    @SerializedName("desc")
    @Expose
    private String desc;

    public RechargeRecords() {
    }

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
