package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class User implements Serializable {
    String name;
    String phone;
    String email;
    String password;
    String status;
    String stateid;
    String language;
    String countryid;

    Profiledata sponsor=null;

    public User() {
    }


    public Profiledata getSponsor() {
        return sponsor;
    }

    public void setSponsor(Profiledata sponsor) {
        this.sponsor = sponsor;
    }

    public String getCountryid() {
        return countryid;
    }

    public void setCountryid(String countryid) {
        this.countryid = countryid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
