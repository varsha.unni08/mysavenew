package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;

public class MileStone implements Serializable {

    String id="";
    String amount="";
    String start_date="";
    String end_date="";
    int selected=0;
    String mileStoneid="";
    String targetid="";
    boolean iswarningexist=false;
    String warningmessage="";

    public String getMileStoneid() {
        return mileStoneid;
    }

    public void setMileStoneid(String mileStoneid) {
        this.mileStoneid = mileStoneid;
    }

    public MileStone() {
    }

    public boolean isIswarningexist() {
        return iswarningexist;
    }

    public void setIswarningexist(boolean iswarningexist) {
        this.iswarningexist = iswarningexist;
    }

    public String getWarningmessage() {
        return warningmessage;
    }

    public void setWarningmessage(String warningmessage) {
        this.warningmessage = warningmessage;
    }

    public String getTargetid() {
        return targetid;
    }

    public void setTargetid(String targetid) {
        this.targetid = targetid;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
