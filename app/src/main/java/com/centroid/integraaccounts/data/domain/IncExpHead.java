package com.centroid.integraaccounts.data.domain;

import java.util.ArrayList;
import java.util.List;

public class IncExpHead {

    int selected=0;

    List<LedgerAccount>ledgerAccounts=new ArrayList<>();

    double totalamount=0;

    public IncExpHead() {
    }

    public double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(double totalamount) {
        this.totalamount = totalamount;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public List<LedgerAccount> getLedgerAccounts() {
        return ledgerAccounts;
    }

    public void setLedgerAccounts(List<LedgerAccount> ledgerAccounts) {
        this.ledgerAccounts = ledgerAccounts;
    }
}
