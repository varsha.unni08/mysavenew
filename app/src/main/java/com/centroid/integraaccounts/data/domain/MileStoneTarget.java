package com.centroid.integraaccounts.data.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MileStoneTarget implements Serializable {

    double targetamount=0;
    List<MileStone>mls=new ArrayList<>();


    public MileStoneTarget() {
    }

    public double getTargetamount() {
        return targetamount;
    }

    public void setTargetamount(double targetamount) {
        this.targetamount = targetamount;
    }

    public List<MileStone> getMls() {
        return mls;
    }

    public void setMls(List<MileStone> mls) {
        this.mls = mls;
    }
}
