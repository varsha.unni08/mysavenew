package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileAppVersionData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("filepath")
    @Expose
    private String filepath;

    public MobileAppVersionData() {
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
