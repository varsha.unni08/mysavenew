package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("activation_date")
    @Expose
    private String activationDate;
    @SerializedName("activation_key")
    @Expose
    private String activationKey;
    @SerializedName("join_source")
    @Expose
    private String joinSource;
    @SerializedName("used_link_for_registration")
    @Expose
    private String usedLinkForRegistration;
    @SerializedName("sp_reg_id")
    @Expose
    private String spRegId;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("sp_reg_code")
    @Expose
    private String spRegCode;
    @SerializedName("default_lang")
    @Expose
    private String defaultLang;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("encr_password")
    @Expose
    private String encrPassword;
    @SerializedName("gdrive_fileid")
    @Expose
    private Object gdriveFileid;

    public UserData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getJoinSource() {
        return joinSource;
    }

    public void setJoinSource(String joinSource) {
        this.joinSource = joinSource;
    }

    public String getUsedLinkForRegistration() {
        return usedLinkForRegistration;
    }

    public void setUsedLinkForRegistration(String usedLinkForRegistration) {
        this.usedLinkForRegistration = usedLinkForRegistration;
    }

    public String getSpRegId() {
        return spRegId;
    }

    public void setSpRegId(String spRegId) {
        this.spRegId = spRegId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSpRegCode() {
        return spRegCode;
    }

    public void setSpRegCode(String spRegCode) {
        this.spRegCode = spRegCode;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncrPassword() {
        return encrPassword;
    }

    public void setEncrPassword(String encrPassword) {
        this.encrPassword = encrPassword;
    }

    public Object getGdriveFileid() {
        return gdriveFileid;
    }

    public void setGdriveFileid(Object gdriveFileid) {
        this.gdriveFileid = gdriveFileid;
    }
}
