package com.centroid.integraaccounts.data.domain;

public class DiarySubject {

    int selected=0;
    String data="";


    public DiarySubject() {
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
