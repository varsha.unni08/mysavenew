package com.centroid.integraaccounts.data.domain;

import android.graphics.drawable.Drawable;

public class VisitCardImg {

    String id;
    byte[] arrdata;
    Drawable dr;

    public VisitCardImg() {
    }

    public Drawable getDr() {
        return dr;
    }

    public void setDr(Drawable dr) {
        this.dr = dr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getArrdata() {
        return arrdata;
    }

    public void setArrdata(byte[] arrdata) {
        this.arrdata = arrdata;
    }
}
