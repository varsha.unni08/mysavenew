package com.centroid.integraaccounts.data;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

public class SecuredDataHelper {
    SharedPreferences sharedPreferences;

    public SecuredDataHelper(Context context) {
        try {

            sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

//            String masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
//
//             sharedPreferences = EncryptedSharedPreferences.create(
//                    "secret_shared_prefs",
//                    masterKeyAlias,
//                    context,
//                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
//                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
//            );
//
//            // use the shared preferences and editor as you normally would
//            SharedPreferences.Editor editor = sharedPreferences.edit();
        }catch (Exception e)
        {

        }
    }


    public void putData(String key,String data)
    {
        sharedPreferences.edit()
                .putString(key, data)
                .apply();
    }

    public String getData(String key)
    {

        String data="";

        data = sharedPreferences.getString(key, "");



        return data;

    }

    public void putBooleanData(String key, boolean data)
    {
        sharedPreferences.edit().putBoolean(key,data).commit();
    }

    public boolean getBoolData(String key)
    {
        return sharedPreferences.getBoolean(key,false);

    }

    public void putIntData(String key, int data)
    {
        sharedPreferences.edit().putInt(key,data).commit();
    }

    public Integer getIntData(String key)
    {
        return sharedPreferences.getInt(key,0);
    }
}
