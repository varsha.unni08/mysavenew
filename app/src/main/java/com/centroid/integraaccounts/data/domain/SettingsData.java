package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SettingsData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("bv")
    @Expose
    private String bv;
    @SerializedName("referal_commission")
    @Expose
    private String referalCommission;
    @SerializedName("one_bv_required_amount")
    @Expose
    private String oneBvRequiredAmount;
    @SerializedName("renewal_charge")
    @Expose
    private String renewalCharge;
    @SerializedName("one_bv_equalant_value")
    @Expose
    private String oneBvEqualantValue;
    @SerializedName("daily_ceiling")
    @Expose
    private String dailyCeiling;
    @SerializedName("Percent_tds")
    @Expose
    private String percentTds;
    @SerializedName("Percent_tds_nonpan")
    @Expose
    private String percentTdsNonpan;
    @SerializedName("percent_admin_charges")
    @Expose
    private String percentAdminCharges;
    @SerializedName("renewal_notification_days")
    @Expose
    private String renewalNotificationDays;
    @SerializedName("bill_prefix")
    @Expose
    private String billPrefix;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("other_tax")
    @Expose
    private String otherTax;
    @SerializedName("buffer_count")
    @Expose
    private String bufferCount;
    @SerializedName("link_image")
    @Expose
    private String linkImage;
    @SerializedName("network_image")
    @Expose
    private String network_image;


    public SettingsData() {
    }

    public String getNetwork_image() {
        return network_image;
    }

    public void setNetwork_image(String network_image) {
        this.network_image = network_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBv() {
        return bv;
    }

    public void setBv(String bv) {
        this.bv = bv;
    }

    public String getReferalCommission() {
        return referalCommission;
    }

    public void setReferalCommission(String referalCommission) {
        this.referalCommission = referalCommission;
    }

    public String getOneBvRequiredAmount() {
        return oneBvRequiredAmount;
    }

    public void setOneBvRequiredAmount(String oneBvRequiredAmount) {
        this.oneBvRequiredAmount = oneBvRequiredAmount;
    }

    public String getRenewalCharge() {
        return renewalCharge;
    }

    public void setRenewalCharge(String renewalCharge) {
        this.renewalCharge = renewalCharge;
    }

    public String getOneBvEqualantValue() {
        return oneBvEqualantValue;
    }

    public void setOneBvEqualantValue(String oneBvEqualantValue) {
        this.oneBvEqualantValue = oneBvEqualantValue;
    }

    public String getDailyCeiling() {
        return dailyCeiling;
    }

    public void setDailyCeiling(String dailyCeiling) {
        this.dailyCeiling = dailyCeiling;
    }

    public String getPercentTds() {
        return percentTds;
    }

    public void setPercentTds(String percentTds) {
        this.percentTds = percentTds;
    }

    public String getPercentTdsNonpan() {
        return percentTdsNonpan;
    }

    public void setPercentTdsNonpan(String percentTdsNonpan) {
        this.percentTdsNonpan = percentTdsNonpan;
    }

    public String getPercentAdminCharges() {
        return percentAdminCharges;
    }

    public void setPercentAdminCharges(String percentAdminCharges) {
        this.percentAdminCharges = percentAdminCharges;
    }

    public String getRenewalNotificationDays() {
        return renewalNotificationDays;
    }

    public void setRenewalNotificationDays(String renewalNotificationDays) {
        this.renewalNotificationDays = renewalNotificationDays;
    }

    public String getBillPrefix() {
        return billPrefix;
    }

    public void setBillPrefix(String billPrefix) {
        this.billPrefix = billPrefix;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getOtherTax() {
        return otherTax;
    }

    public void setOtherTax(String otherTax) {
        this.otherTax = otherTax;
    }

    public String getBufferCount() {
        return bufferCount;
    }

    public void setBufferCount(String bufferCount) {
        this.bufferCount = bufferCount;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }
}
