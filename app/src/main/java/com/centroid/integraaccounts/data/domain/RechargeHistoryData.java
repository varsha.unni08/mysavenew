package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeHistoryData {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("recharge_type")
    @Expose
    private String rechargeType;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("Payment_Mode")
    @Expose
    private String paymentMode;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("recharge_date")
    @Expose
    private String rechargeDate;
    @SerializedName("rp_id")
    @Expose
    private String rpId;
    @SerializedName("agent_id")
    @Expose
    private String agentId;
    @SerializedName("operator_circler_code")
    @Expose
    private String operatorCode;
    @SerializedName("payment_status")
    @Expose
    private String paymentstatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("gen_status")
    @Expose
    private String genStatus;

    public String getPaymentstatus() {
        return paymentstatus;
    }

    public void setPaymentstatus(String paymentstatus) {
        this.paymentstatus = paymentstatus;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getRefundTransactionid() {
        return refundTransactionid;
    }

    public void setRefundTransactionid(String refundTransactionid) {
        this.refundTransactionid = refundTransactionid;
    }

    @SerializedName("refund_date")
    @Expose
    private String refundDate;
    @SerializedName("refund_transactionid")
    @Expose
    private String refundTransactionid;

    public RechargeHistoryData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRechargeDate() {
        return rechargeDate;
    }

    public void setRechargeDate(String rechargeDate) {
        this.rechargeDate = rechargeDate;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGenStatus() {
        return genStatus;
    }

    public void setGenStatus(String genStatus) {
        this.genStatus = genStatus;
    }
}
