package com.centroid.integraaccounts.data.domain;

import java.util.ArrayList;
import java.util.List;

public class DTHPlansFromServer {

    String description="";
    String plan_name="";
    List<DTHValidity>dthValidities=new ArrayList<>();

    public DTHPlansFromServer() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public List<DTHValidity> getDthValidities() {
        return dthValidities;
    }

    public void setDthValidities(List<DTHValidity> dthValidities) {
        this.dthValidities = dthValidities;
    }
}
