package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Notificationdata implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("reg_id")
    @Expose
    private String regId;
    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("message")
    @Expose
    private Notificationmessage message;


    public Notificationdata() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Notificationmessage getMessage() {
        return message;
    }

    public void setMessage(Notificationmessage message) {
        this.message = message;
    }
}
