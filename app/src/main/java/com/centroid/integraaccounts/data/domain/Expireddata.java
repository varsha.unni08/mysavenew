package com.centroid.integraaccounts.data.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Expireddata {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("expe_date")
    @Expose
    private String expeDate;

    public Expireddata() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExpeDate() {
        return expeDate;
    }

    public void setExpeDate(String expeDate) {
        this.expeDate = expeDate;
    }
}
