
package com.centroid.integraaccounts.com.TokenResult;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Head {

    @SerializedName("responseTimestamp")
    @Expose
    private String responseTimestamp;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("signature")
    @Expose
    private String signature;

    public String getResponseTimestamp() {
        return responseTimestamp;
    }

    public void setResponseTimestamp(String responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

}
