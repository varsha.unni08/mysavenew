package com.centroid.integraaccounts.webserviceHelper;



import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.data.domain.CountryList;
import com.centroid.integraaccounts.data.domain.Currency;
import com.centroid.integraaccounts.data.domain.FeedbackmessageData;
import com.centroid.integraaccounts.data.domain.MVersion;
import com.centroid.integraaccounts.data.domain.Member;
import com.centroid.integraaccounts.data.domain.NotificationList;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.Slider;
import com.centroid.integraaccounts.data.domain.Sponsor;
import com.centroid.integraaccounts.data.domain.StateData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;


import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public class
RestService {
//    private static RestApiInterface RestApiInterface;
//
//
//
//
//    private static RestApiInterface getRestApiInterface;


//    public static RestApiInterface getClient() {
//
//        if (RestApiInterface == null) {
//
//            try {
//
//                OkHttpClient httpclient = new OkHttpClient.Builder()
//                        .addInterceptor(new BasicAuthInterceptor(Utils.razorkeyid, Utils.razorSecretkey))
//                        .build();
//
//                Gson gson = new GsonBuilder()
//                        .setLenient()
//                        .create();
//
////            Retrofit client = new Retrofit.Builder()
////                    .baseUrl(Utils.baseurl)
////                    .addConverterFactory(GsonConverterFactory.create(gson))
////                    .addConverterFactory(GsonConverterFactory.create())
////                    .client(httpClient)
////                    .build();
//
//                Retrofit client = new Retrofit.Builder()
//                        .baseUrl(Utils.baseurl)
//                        .addConverterFactory(GsonConverterFactory.create(gson))
//                        .client(httpclient)
//                        .build();
//
//                RestApiInterface = client.create(RestApiInterface.class);
//            }catch (Exception e)
//            {
//
//            }
//        }
//        return RestApiInterface;
//    }
//
//    public static RestApiInterface getSmsClient() {
//
//       // if (getRestApiInterface == null) {
//        OkHttpClient hclient = new OkHttpClient.Builder()
//                .connectionSpecs(Arrays.asList(ConnectionSpec.COMPATIBLE_TLS))
//                .build();
//
//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();
//
//            Retrofit client = new Retrofit.Builder()
//                    .baseUrl(Utils.smsbaseurl)
//                    .addConverterFactory(new NullOnEmptyConverterFactory())
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                   .client(hclient)
//                    .build();
//
//            getRestApiInterface = client.create(RestApiInterface.class);
//        //}
//        return getRestApiInterface;
//    }


//    public static RestApiInterface getRazorPayClientClient() {
//
//       // if (getRestApiInterface == null) {
////            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
////            // set your desired log level
////            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
////
////            OkHttpClient.Builder builder = new OkHttpClient.Builder();
////            builder.interceptors().add(logging);
//
//            OkHttpClient httpclient = new OkHttpClient.Builder()
//                    .addInterceptor(new BasicAuthInterceptor(Utils.razorkeyid, Utils.razorSecretkey))
//                    .build();
//
//
//
//            Retrofit client = new Retrofit.Builder()
//                    .baseUrl(Utils.razorpayBaseUrl)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(httpclient)
//                    .build();
//
//            getRestApiInterface = client.create(RestApiInterface.class);
//        //}
//        return getRestApiInterface;
//    }




    public interface RestApiInterface {




        @POST("updateActivationdate.php")
        Call<JsonObject> updateActivationdate(@Header("Authorization") String Authorization);

        @POST("v1/orders")
        Call<JsonObject> postOrders(@Body Map<String,Object> jsonObject);



        @GET("httpapi")
        Call<String> SendSms(@Query("token") String token,@Query("sender") String sender,@Query("number")String number,@Query("route")String route,@Query("type")String type,@Query("sms")String sms,@Query("templateid")String templateid);


        @GET("getUserByMobile.php")
        Call<JsonObject> getUserByMobile(@Query("mobile") String mobile);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("UserLogin.php")
        Call<JsonObject> UserLogin(@Field("uuid")String uuid,@Field("mobile") String mobile,@Field("password")String password);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updateNotificationStatus.php")
        Call<JsonObject> updateNotificationStatus(@Header("Authorization") String Authorization,@Field("messageid") String messageid);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("sendMail.php")
        Call<JsonObject> sendMail(@Field("email") String email,@Field("code")String code);




        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("addFeedback.php")
        Call<JsonObject> addFeedback(@Header("Authorization") String Authorization,@Field("message") String message);




        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("UserAuthenticate.php")
        Call<JsonObject> UserAuthenticate(@Field("uuid")String uuid,@Field("country_id")String country_id,@Field("sp_reg_code")int sp_reg_code,@Field("sp_reg_id")int sp_reg_id,@Field("stateid")String stateid,@Field("language")String language,@Field("name")String name,@Field("mobile") String mobile,@Field("password")String password,@Field("email")String email);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("checkMobileExists.php")
        Call<JsonObject> checkMobileExists(@Field("mobile") String mobile);




        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("checkMobileAlreadyExists.php")
        Call<JsonObject> checkMobileAlreadyExists(@Field("mobile") String mobile);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("changePassword.php")
        Call<JsonObject> changePassword(@Field("mobile") String mobile,@Field("password") String password);



        @Headers({"Accept: application/json"})

        @GET("getFeedback.php")
        Call<FeedbackmessageData> getFeedback(@Header("Authorization") String Authorization);


        @Headers({"Accept: application/json"})

        @GET("getCountry.php")
        Call<CountryList> getCountry();


        @Headers({"Accept: application/json"})

        @GET("getState.php")
        Call<StateData> getState(@Query("countryid")String countryid);


       // getMobileAppVersion.php

        @Headers({"Accept: application/json"})

        @GET("getMobileAppVersion.php")
        Call<MVersion> getMobileAppVersion();



        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("checkSponsor.php")
        Call<Sponsor> checkSponsor( @Field("mobile") String device_id);




        @Headers({"Accept: application/json"})

        @GET("getPositionBeforePurchase.php")
        Call<Member> getPositionBeforePurchase(@Header("Authorization") String Authorization);



        @Headers({"Accept: application/json"})

        @GET("getNotifications.php")
        Call<NotificationList> getNotifications(@Header("Authorization") String Authorization);




        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updateDeviceId.php")
        Call<JsonObject> updateDeviceId(@Header("Authorization") String Authorization,@Field("device_id") String device_id);






        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updateDeviceOSId.php")
        Call<JsonObject> updateDeviceOSId(@Header("Authorization") String Authorization,@Field("device_id") String device_id);





        @Headers({"Accept: application/json"})
        @GET("getUserProfile.php")
        Call<Profiledata> getUserProfile(@Header("Authorization") String Authorization);




        @Headers({"Accept: application/json"})
        @GET("getSliderImages.php")
        Call<Slider> getSliderImages(@Header("Authorization") String Authorization);


        @Headers({"Accept: application/json"})
        @GET("getCurrencyByCountryID.php")
        Call<Currency> getCurrencyByCountryID(@Header("Authorization") String Authorization,@Query("countryid")String countryid);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("UserProfileUpdate.php")
        Call<JsonObject> UserProfileUpdate(@Header("Authorization") String Authorization,@Field("name") String name,@Field("user_email")String email,@Field("stateid")String stateid,@Field("language")String language,@Field("country_id")String country_id);




        @Multipart
        @POST("uploadUserProfile.php")
        Call<JsonObject> uploadUserProfile(@Header("Authorization") String x_auth_token,@Part MultipartBody.Part file);


        @Multipart
        @POST("uploadBackupFile.php")
        Call<JsonObject> uploadBackupFile(@Header("Authorization") String x_auth_token,@Part MultipartBody.Part file);




        @Headers({"Accept: application/json"})

        @GET("checkSalesInfo.php")
        Call<JsonObject> checkSalesInfo(@Header("Authorization") String Authorization);


        @Headers({"Accept: application/json"})

        @GET("getSettingsValue.php")
        Call<JsonObject> getSettingsValue(@Header("Authorization") String Authorization);



        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("addSalesInfo.php")
        Call<JsonObject> addSalesInfo(@Header("Authorization") String Authorization,@Field("cash_transaction_id")String cash_transaction_id);



        @Headers({"Accept: application/json"})
        @POST("checkExpiryDate.php")
        Call<JsonObject> checkExpiryDate(@Header("Authorization") String Authorization);


        @Headers({"Accept: application/json"})
        @POST("checkTrialPeriod.php")
        Call<JsonObject> checkTrialPeriod(@Header("Authorization") String Authorization);


        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updatePosition.php")
        Call<JsonObject> updatePosition(@Header("Authorization") String Authorization,@Field("position")String position,@Field("id")int id);



        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updatePositionNext.php")
        Call<JsonObject> updatePositionNext(@Header("Authorization") String Authorization,@Field("position")String position,@Field("id")int id);




        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("updateGoogleDriveFileId.php")
        Call<JsonObject> updateGoogleDriveFileId(@Header("Authorization") String Authorization,@Field("gdrive_fileid")String gdrive_fileid);




        @Headers({"Accept: application/json"})
        @POST("updateRenewalStatus.php")
        Call<JsonObject> updateRenewalStatus(@Header("Authorization") String Authorization);






        @Headers({"Accept: application/json"})
        @GET("getNetworkData.php")
        Call<JsonObject> getNetworkData(@Header("Authorization") String Authorization);
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("checkMobileVersion.php")
//        Call<JsonObject> checkMobileVersion(@Field("ds_appVersion") String ds_version);
//
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("updatepincode.php")
//        Call<JsonObject> updatepincode(@Header("Authorization") String x_auth_token, @Field("ds_pincode") String ds_pincode);
//
//
//
//
//        @Multipart
//        @POST("{api_key}/ADDON_SERVICES/SEND/TSMS")
//        Call<JsonObject>sendOTP(@Path(value = "api_key", encoded = false) String api_key, @Part("From") RequestBody requestBody, @Part("To") RequestBody requestBody_to, @Part("TemplateName") RequestBody TemplateName, @Part("VAR1") RequestBody VAR1, @Part("VAR2") RequestBody VAR2);
//
//
//
//
//        @GET("checkProductStock.php")
//        Call<JsonObject>checkProductStock(@Header("Authorization") String x_auth_token, @Query("cd_item") String cd_item, @Query("cd_branch") String cd_branch);
//
//
//        @GET("getAllPincodes.php")
//        Call<List<Pincode>>getAllPincodes(@Header("Authorization") String x_auth_token);
//
//
//
//
////        @Headers({"Accept: application/json"})
////        @GET("getServiceCharge.php")
////        Call<JsonObject>getServiceCharge(@Header("Authorization") String x_auth_token);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getSliders.php")
//        Call<List<Slider>>getSliders(@Header("Authorization") String x_auth_token);
//
//        //.php
//
//        @Headers({"Accept: application/json"})
//        @GET("getServiceCharge.php")
//        Call<JsonObject>getServiceCharge(@Header("Authorization") String x_auth_token, @Query("cd_branch") String cd_branch);
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("updateDeviceid.php")
//        Call<JsonObject> updateDeviceid(@Header("Authorization") String x_auth_token, @Field("ds_deviceid") String ds_deviceid);
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//
//        @POST("UserLogin.php")
//        Call<ResponseBody> loginRequest(@Query("apicall") String callType, @Field("phone") String phone, @Field("password") String password);
//
//
//        @GET("getOffers.php")
//        Call<List<Offer>>getOfferlist(@Header("Authorization") String x_auth_token);
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("UserLogin.php?apicall=signup")
//        Call<ResponseBody> regUser(@Field("isNewUpdatedUser") String isNewUpdatedUser, @Field("username") String name, @Field("phone") String phone, @Field("email") String email, @Field("password") String password, @Field("gender") String gender, @Field("pincode") String pincode, @Field("cd_branch") String cd_branch);
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("add_to_cart.php")
//        Call<ResponseBody> addToCart(@Header("Authorization") String x_auth_token, @Field("cd_item") String cdItemcode, @Field("quantity") String quantity);
//
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("master/featuredprofiles/{type}/{page}")
//        Call<List<ResponseBody>> getFeaturedUsers(@Header("Authorization") String x_auth_token, @Path("type") String type, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("get_item_subcategories.php")
//        Call<List<Item>> getAllItems(@Header("Authorization") String x_auth_token, @Query("api_key") String call);
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("get_single_item.php")
//        Call<List<Item>> getSingleItem(@Header("Authorization") String x_auth_token, @Query("cd_item") String call);
//
//        @Headers({"Accept: application/json"})
//        @GET("getAll.php")
//        Call<List<Item>> getAll(@Header("Authorization") String x_auth_token, @Query("currentpage") String currentPage);
//
//        @Headers({"Accept: application/json"})
//        @GET("get_featured_item.php")
//        Call<List<Item>> getFeaturedItems(@Header("Authorization") String x_auth_token, @Query("apicall") String call);
//
////        @Headers({"Accept: application/json"})
////        @GET("search_products.php")
////        Call<List<Item>> searchProduct(@Header("Authorization") String x_auth_token, @Query("search") String key, @Query("currentpage") String currentPage);
//
//
//
//        @Headers({"Accept: application/json"})
//             @GET("search_products.php")
//        Call<List<FilteredProduct>> searchProduct(@Header("Authorization") String x_auth_token, @Query("item") String key);
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("CheckMobilenumberExists.php")
//        Call<JsonObject>checkmobile(@Field("ds_phone") String ds_phone);
//
//
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("changePassword.php")
//        Call<JsonObject>changePassword(@Field("ds_phone") String ds_phone, @Field("ds_password") String ds_password);
//
//
//
//
//        @Headers({"Accept: application/json"})
//        @GET("get_catwise_subcategories.php")
//        Call<List<SubCategory>> getSubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("get_featured_item.php")
//        Call<List<Item>> getCategorySubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId, @Query("cd_category") String subCategoryId, @Query("currentpage") String currentPage);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("filter_item.php")
//        Call<List<Item>> getFilterCategorySubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId, @Query("cd_subcategory") String subCategoryId, @Query("currentpage") String currentPage);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getAllItemsByCategory.php")
//        Call<List<FilteredProduct>> getAllItemsByCategory(@Header("Authorization") String x_auth_token, @Query("cd_category") String category, @Query("count") String count, @Query("cd_subcategory") String cd_subcategory);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getProductByMainCategory.php")
//        Call<List<Item>> getProductByMainCategory(@Header("Authorization") String x_auth_token, @Query("cd_maincategory") String cd_maincategory);
//
//
//
//        @Headers({"Accept: application/json"})
//        @GET("get_categories.php")
//        Call<List<Category>> getAllCategories(@Header("Authorization") String x_auth_token);
//
//        @Headers({"Accept: application/json"})
//        @GET("checkDeliveryPincode.php")
//        Call<ResponseBody> checkPincode(@Header("Authorization") String x_auth_token, @Query("pincode") String pincode);
//
//
//
//        @Headers({"Accept: application/json"})
//        @GET("checkPincodeNoAuth.php")
//        Call<ResponseBody> checkPincodeWithoutAuth(@Query("pincode") String pincode);
//
//
//
//
//
//
//        @Headers({"Accept: application/json"})
//        @POST("placeOfferOrder.php")
//        @FormUrlEncoded
//        Call<ResponseBody> placeOfferOrder(@Header("Authorization") String x_auth_token, @Field("ds_pincode") String pincode, @Field("ds_delivery_address") String address, @Field("ds_latitude") String ds_latitude, @Field("ds_longitude") String ds_longitude, @Field("ds_location") String ds_location, @Field("cd_offer") String cd_offer, @Field("cd_qty") int cd_qty, @Field("ds_landmark") String ds_landmark, @Field("cd_branch") String cd_branch);
//
//
//
//        @Headers({"Accept: application/json"})
//        @POST("new_sales_order.php")
//        @FormUrlEncoded
//        Call<ResponseBody> placeOrder(@Header("Authorization") String x_auth_token, @Field("cd_transaction_type") String cd_transaction_type, @Field("cd_refund_status") String cd_refund_status, @Field("ds_transaction_id") String ds_transaction_id, @Field("cd_payment_status") String cd_payment_status, @Field("ds_pincode") String pincode, @Field("ds_delivery_address") String address, @Field("ds_latitude") String ds_latitude, @Field("ds_longitude") String ds_longitude, @Field("ds_location") String ds_location, @Field("ds_landmark") String ds_landmark, @Field("cd_branch") String cd_branch);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        @POST("add_to_wishList.php")
//        @FormUrlEncoded
//        Call<ResponseBody> addWishList(@Header("Authorization") String x_auth_token, @Field("cd_item") String itemCode, @Field("quantity") String quantity);
//
//        @GET("WishListRemove.php")
//        Call<ResponseBody> removeWishList(@Header("Authorization") String x_auth_token, @Query("cd_item") String itemCode);
//
//        @Headers({"Accept: application/json"})
//        @POST("cartP.php")
//        Call<ResponseBody> addCart(@Header("Authorization") String x_auth_token, @Body CartItems cartItems);
//
//        @Headers({"Accept: application/json"})
//        @GET("cancelOrder.php")
//        Call<ResponseBody> cancelOrder(@Header("Authorization") String x_auth_token, @Query("cd_sales_order") String id);
//
//        @Headers({"Accept: application/json"})
//        @GET("ordersView.php")
//        Call<List<OrderItem>> getAllOrder(@Header("Authorization") String x_auth_token, @Query("currentpage") int currentPage);
//
//        @Headers({"Accept: application/json"})
//        @GET("WishListCartView.php")
//        Call<List<Item>> getWishList(@Header("Authorization") String x_auth_token, @Query("apicall") String callType, @Query("currentpage") int currentpage);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getWishlistitem.php")
//        Call<Wishlist> getWishListItems(@Header("Authorization") String x_auth_token, @Query("page") int currentpage);
//
//
//
//        @Headers({"Accept: application/json"})
//        @GET("userProfile.php")
//        Call<List<UserItem>> getUser(@Header("Authorization") String x_auth_token);
//
//        @Headers({"Accept: application/json"})
//        @FormUrlEncoded
//        @POST("editUserProfile.php")
//        Call<ResponseBody> updateUser(@Header("Authorization") String x_auth_token, @Field("name") String name, @Field("email") String email, @Field("pincode") String pincode);
//
//        @Headers({"Accept: application/json"})
//        @GET("newsView.php")
//        Call<List<NewsItem>> getNews(@Header("Authorization") String x_auth_token, @Query("currentpage") int currentpage);
//
//        @GET("wishlist_existance.php")
//        Call<JsonArray>getWishListExistence(@Header("Authorization") String x_auth_token, @Query("cd_item") String cd_item);
//
//
//        @Multipart
//        @POST("UsersEditImage.php")
//        Call<JsonArray> uploadImage(@Header("Authorization") String x_auth_token, @Part MultipartBody.Part file);
//
//
//
//        @GET("UsersGetProfileImage.php")
//        Call<JsonArray>getImage(@Header("Authorization") String x_auth_token);
//
//        @GET("getAds.php")
//        Call<Adinfo>getAds(@Header("Authorization") String x_auth_token);
//
//
//
//        @FormUrlEncoded
//        @POST("cartP.php")
//        Call<JsonObject> addProductCart(@Header("Authorization") String x_auth_token, @Field("cd_item") String cd_item, @Field("quantity") String quantity, @Field("date") String date);
//
//
//        @FormUrlEncoded
//        @POST("updateCartQty.php")
//        Call<JsonObject> updateProductCart(@Header("Authorization") String x_auth_token, @Field("cd_cart") String cd_item, @Field("cd_qty") String quantity);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("CartCount.php")
//        Call<JsonObject> getCartCount(@Header("Authorization") String x_auth_token);
//
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getMyCart.php")
//        Call<CartProduct> getCartProducts(@Header("Authorization") String x_auth_token, @Query("page") int currentpage);
//
//
//        @Headers({"Accept: application/json"})
//        @GET("getCartSum.php")
//        Call<JsonObject> getCartTotal(@Header("Authorization") String x_auth_token);
//
//
//        @FormUrlEncoded
//        @POST("CartremoveItem.php")
//        Call<JsonObject> deleteFromCart(@Header("Authorization") String x_auth_token, @Field("cd_cart") String cd_item);
//
//        @GET("get_suggessions_items.php")
//        Call<List<SearchSuggestions>> getSearchSuggestios(@Header("Authorization") String x_auth_token, @Query("item") String cd_item);
//
//        @GET("orderDetails.php")
//        Call<List<OrderDetails>>getOrderDetails(@Header("Authorization") String x_auth_token, @Query("cd_sales_order") String cd_sales_order);
//
//        @FormUrlEncoded
//        @POST("cancelOrderItem.php")
//        Call<JsonObject>cancelOrderItem(@Header("Authorization") String x_auth_token, @Field("cd_item") String cd_item);
//
//
//        @GET("get_avilable_pincode.php")
//        Call<List<Pincode>>getPincodeSuggestions(@Header("Authorization") String x_auth_token, @Query("pincode") String pincode);
//
//
//        @GET("getPincodeBaseBranch.php")
//        Call<List<Pincode>>getPincodeBaseBranch(@Header("Authorization") String x_auth_token, @Query("pincode") String pincode, @Query("cd_branch") String cd_branch);
//
//
//
//
//
//
//        @GET("getBrachFrompinCode.php")
//        Call<Pincode>getBrachFrompinCode(@Header("Authorization") String x_auth_token, @Query("pincode") String pincode);
//
//
//
//
//
//        @GET("CartTotalView.php")
//        Call<JsonArray>getTotalPriceFromcart(@Header("Authorization") String x_auth_token, @Query("currentpage") int currentpage);
//
//
//        @GET("get_maincategories.php")
//        Call<List<MainCategory>>get_maincategories(@Header("Authorization") String x_auth_token);
//
//
//        @GET("getAllBrands.php")
//        Call<List<Brand>>getAllBrands(@Header("Authorization") String x_auth_token);
//
//
//        @GET("getSimilarProducts.php")
//        Call<List<FilteredProduct>>getSimilarProducts(@Header("Authorization") String x_auth_token, @Query("cd_item") String cd_item);
//
//         /*
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("search/basic/{page}")
//        Call<List<FeaturedUser>> getAllSearched(@Body ProfileSearchRequest profileSearchRequest, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("search/code/{scmid}/{userId}")
//        Call<FeaturedUser> getAllSearchedById(@Path("scmid") String scmId, @Path("userId") String userId);
//
//        @NonNull
//        @POST("user/resendEmailVerify/{userId}")
//        Call<ResponseBody> resendEmailCode(@Path("userId") String userId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/{uid}")
//        Call<UserProfile> getUserProfile(@Header("X-Auth-Token") String x_auth_token, @Path("uid") Integer uid);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/get")
//        Call<UserProfile> getUserProfile(@Header("X-Auth-Token") String x_auth_token);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/bookmarksall/{userId}/{page}")
//        Call<List<FeaturedUser>> getAllBookmarked(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("search/matchingprofile/{userId}/{page}")
//        Call<List<FeaturedUser>> getAllMatchingProfiles(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("search/matchingprofileauto/{userId}/{page}")
//        Call<List<FeaturedUser>> getAllMatchingProfilesAuto(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/bookmarks/{userId}/{page}")
//        Call<BookmarkUserId> getAllBookmarkedUserId(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("page") int page);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("master/countries")
//        Call<List<Country>> getAllCountryCode();
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("master/countries")
//        Call<List<ParishCountry>> getAllParishCountry();
//
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("master/communities")
//        Call<List<CommunityQuickSignUp>> getAllCommunity();
//
//        @NonNull
//        @Headers({"Accept: application/json", "Content-Type: application/json"})
//        @POST("user/signup")
//        Call<UserResponse> userQuickSignUp(@Header("Authorization") String auth_token, @Body QuickSignUpBody signUp);
//
//
//        @NonNull
//        @GET("profile/getLatestPayment/{userId}")
//        Call<PaymentSummaryDTO> getLatestTransaction(@Header("X-Auth-Token") String auth_token, @Path("userId") String userId);
//
//
//        @NonNull
//        @GET("paypal/client_token")
//        Call<UserResponse> getBrianTreeToken();
//
//        @NonNull
//        @GET("paypal/checkout/{nonceFromTheClient}/{amount}/{orderId}/{userId}")
//        Call<ResponseBody> checkOutProcess(@Path("nonceFromTheClient") String nonce, @Path("amount") String amount, @Path("orderId") String orderId, @Path("userId") String userId);
//
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/image/list/{userId}")
//        Call<List<ProfileImageItem>> getAllProfileImages(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/image/profilepic/{userId}/{imageId}")
//        Call<UserResponse> setProfileImages(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("imageId") int imageId);
//
//
//        @NonNull
//        @GET("master/dioces/{userId}")
//        Call<List<Community>> getAllDiocese(@Path("userId") String userId);
//
//        @NonNull
//        @GET("master/parish/{country}/{userId}")
//        Call<List<Diocese>> getAllParish(@Path("country") String country, @Path("userId") String userId);
//
//
//        @NonNull
//        @GET("user/checkemail/{userId}/")
//        Call<ResponseValid> checkEmailId(@Path("userId") String userId);
//
//        @NonNull
//        @GET("user/checkmobile/{country}/{userId}/")
//        Call<ResponseValid> checkMobile(@Path("country") String countryId, @Path("userId") String userId);
//
//
//        @NonNull
//        @GET("master/occupation/")
//        Call<List<Profession>> getAllOccupation();
//
//        @NonNull
//        @GET("master/education/")
//        Call<List<Education>> getAllEducation();
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/primary/{userId}")
//        Call<PrimaryProfileResponse> getPrimaryProfile(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/family/{userId}")
//        Call<FamilyInfoResponse> getFamilyInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/church/{userId}")
//        Call<ParishInfoResponse> getParishInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/professional/{userId}")
//        Call<ProfessionalInfoResponse> getProfessionalInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/showUserIdDoc/{userId}")
//        Call<IdProofResponse> getIdProofResponse(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/partner/{userId}")
//        Call<PartnerInfoResponse> getPartnerInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @GET("master/packages")
//        Call<List<PaymentPlan>> getPaymentPlans();
//
//        @NonNull
//        @POST("profile/upgrade/{userId}")
//        Call<PaymentRequestResponse> getPaymentUserResponse(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body RequestPaymentBody paymentBody);
//
//        @NonNull
//        @FormUrlEncoded
//        @POST("resources/partials/payment/GetRSA.jsp")
//        Call<ResponseBody> getRSAResponse(@FieldMap Map<String, String> params);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/get/{userId}")
//        Call<UserBasicData> getUserBasicInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("profile/activities/{userId}")
//        Call<UserActivitiesData> getAllActivities(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);
//
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/primary/{userId}")
//        Call<UserResponse> updatePrimaryProfile(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body PrimaryProfileData primaryProfileData);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @GET("user/validatemail/{email}/{token}")
//        Call<UserResponse> validateEmail(@Path("email") String emailId, @Path("token") String token);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/family/{userId}")
//        Call<UserResponse> updateFamilyInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body FamilyInfoData familyInfoData);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/church/{userId}")
//        Call<UserResponse> updateParishInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body ChurchInfoData churchInfoData);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/professional/{userId}")
//        Call<UserResponse> updateProfessionalInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body ProfessionalInfoData churchInfoData);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/partner/{userId}")
//        Call<UserResponse> updatePartnerInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body PartnerInfoData partnerInfoData);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/bookmark/{userId}/{currentUserId}")
//        Call<UserResponse> bookMarkUser(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/activity/expressinterest")
//        Call<UserResponse> expressInterest(@Header("X-Auth-Token") String x_auth_token, @Body ExpressInterestRequestBody requestBody);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/interest/{userId}/read/{currentUserId}")
//        Call<UserResponse> readInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/interest/{userId}/accept/{currentUserId}")
//        Call<UserResponse> acceptInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/interest/{userId}/reject/{currentUserId}")
//        Call<UserResponse> rejectInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("profile/forward/{userId}")
//        Call<UserResponse> forwardProfile(@Header("X-Auth-Token") String x_auth_token, @Body ForwardProfileRequestBody requestBody, @Path("userId") String userId);
//
//        @NonNull
//        @Headers({"Accept: application/json"})
//        @POST("user/forgotpassword")
//        Call<UserResponse> resetPassword(@Body UserRequest userRequest);
//
//
//        @NonNull
//        @POST("profile/removebookmark/{userId}/{currentUserId}")
//        Call<UserResponse> unBookMark(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @POST("profile/image/delete/{imageId}/{userId}")
//        Call<UserResponse> deleteProfilePicture(@Header("X-Auth-Token") String x_auth_token, @Path("imageId") String imageId, @Path("userId") String currentUserId);
//
//
//
//        @NonNull
//        @GET("view/contacts/{userId}/{currentUserId}")
//        Call<UserResponse> getUserContacts(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);
//
//        @NonNull
//        @GET("user/sms/{userId}")
//        Call<ResponseBody> getSMS(@Path("userId") String userId);
//
//        @NonNull
//        @GET("user/email/{userId}")
//        Call<ResponseBody> getEmail(@Path("userId") String userId);
//
//
//        @NonNull
//        @GET("profile/activities/interest/sent/{userId}/{page}")
//        Call<List<InterestResponse>> getInterestSent(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user, @Path("page") int page);
//
//        @NonNull
//        @GET("profile/activities/interest/recieved/{userId}/{page}")
//        Call<List<InterestResponse>> getInterestRecieved(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user, @Path("page") int page);
//
//        @NonNull
//        @GET("profile/activities/{type}/{userId}/{page}")
//        Call<List<InterestResponse>> getAllRequested(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);
//
//
//        @NonNull
//        @GET("profile/activities/{type}/{userId}/{page}")
//        Call<List<VisitedResponse>> getAllProfileVisitResponse(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);
//
//        @NonNull
//        @GET("profile/accountsummary/{userId}")
//        Call<AccountSummary> getAccountSummary(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user);
//
//        @NonNull
//        @GET("profile/activities/{type}/{userId}/{page}")
//        Call<List<ContactsResponse>> getAllContactsResponse(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);
//
//        @NonNull
//        @GET("view/profile/{profilecode}/{userId}")
//        Call<ResponseBody> sendUserViews(@Header("X-Auth-Token") String x_auth_token, @Path("profilecode") String profileCode, @Path("userId") String userId);
//
//
//        @NonNull
//        @Multipart
//        @POST("profile/image/upload/{userId}/false/true/1")
//        Call<UserResponse> uploadProfileImage(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile);
//
//        @NonNull
//        @Multipart
//        @POST("profile/image/upload/{userId}/true/false/1")
//        Call<UserResponse> uploadCoverImage(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile);
//
//        @NonNull
//        @Multipart
//        @POST("profile/uploadiddoc/{userId}")
//        Call<UserResponse> uploadIdDocument(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile, @Part("idtype") RequestBody type);
//*/

    }

}
