package com.centroid.integraaccounts.webserviceHelper;

public interface ResponseHandler {


    public void onSuccess(String data);

    public void onFailure(String err);


}
