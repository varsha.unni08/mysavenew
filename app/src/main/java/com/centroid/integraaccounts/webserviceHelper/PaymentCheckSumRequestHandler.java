package com.centroid.integraaccounts.webserviceHelper;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;
import com.centroid.integraaccounts.Constants.Connectivity;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.data.SecuredDataHelper;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class PaymentCheckSumRequestHandler {

    Context context;
    Map<String,String> params;
    ResponseHandler responseHandler;
    String methodename;
    int methodetype;
    String url ="";
    String orderid="";
    public PaymentCheckSumRequestHandler(String orderid, Context context, Map<String, String> params, ResponseHandler responseHandler, String methodename, int methodetype) {
        this.context = context;
        this.params = params;
        this.responseHandler = responseHandler;
        this.methodename = methodename;
        this.methodetype = methodetype;
        this.orderid=orderid;
    }

    public void submitRequest(){

        if(!Utils.isConnectionAvailable(context))
        {

            responseHandler.onFailure("Check your internet connection......");

        }





        else if(Connectivity.isConnectedFast(context))
        {

            RequestQueue requestQueue;

// Instantiate the cache


// Instantiate the RequestQueue with the cache and network.
            //  requestQueue = Volley.newRequestQueue(context);
            requestQueue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
            requestQueue.getCache().clear();

// Start the queue
            requestQueue.start();


            // if(!methodename.startsWith(Utils.WebServiceMethodes.smsMethode)) {




             url=  "https://mysaving.in/IntegraAccount/paytm/initTransaction.php";








// Formulate the request and handle the response.
            StringRequest stringRequest = new StringRequest(methodetype, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if(!Connectivity.isConnectedFast(context))
                            {
                                Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), "Your Internet connection is too slow...", Snackbar.LENGTH_LONG);


                                snackbar.show();
                            }

                            if(responseHandler!=null)
                            {

                                if(response!=null) {

                                    responseHandler.onSuccess(response);
                                }
                            }



                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Handle error

                            Log.e("Errormethode",url);



                            if(responseHandler!=null)
                            {

                                if(!Connectivity.isConnectedFast(context))
                                {
                                    Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), "Your Internet connection is too slow...", Snackbar.LENGTH_LONG);


                                    snackbar.show();
                                }
                                else {
                                   // responseHandler.onFailure("Your Internet connection is too slow...");

                                }

                                if(!Utils.isConnectionAvailable(context))
                                {


                                    Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG);


                                    snackbar.show();

                                    // responseHandler.onFailure("Check your internet connection");


                                }
                                else {


                                    if (error != null) {

                                        try {

                                            if (error.networkResponse != null) {
                                                if (error.networkResponse.data != null) {
                                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                                    JSONObject data = new JSONObject(responseBody);
                                                    JSONArray errors = data.getJSONArray("errors");
                                                    JSONObject jsonMessage = errors.getJSONObject(0);
                                                    String message = jsonMessage.getString("message");

                                                    responseHandler.onFailure("Network Error");
                                                }
                                            }
                                            // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                        } catch (JSONException e) {

                                            responseHandler.onFailure(error.toString());

                                        } catch (UnsupportedEncodingException err) {

                                            responseHandler.onFailure(error.toString());


                                        }

                                    }
                                }
                            }

                        }
                    })
            {

                @Nullable
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    Map<String,String> params = new HashMap<>();

                    params.put("Content-Type","application/x-www-form-urlencoded");
//                    params.put("Authorization",new PreferenceHelper(context).getData(Utils.userkey));
                    params.put("Authorization",new SecuredDataHelper(context).getData(Utils.userkey));


                    return params;
                }



            }







                    ;

// Add the request to the RequestQueue.
            stringRequest.setShouldCache(false);
            requestQueue.getCache().clear();




            RetryPolicy mRetryPolicy = new DefaultRetryPolicy(
                    3000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            stringRequest.setRetryPolicy(mRetryPolicy);



            requestQueue.add(stringRequest);


        }
        else {

           // responseHandler.onFailure("Your Internet connection is too slow...");
//            Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), "Your Internet connection is too slow...", Snackbar.LENGTH_LONG);
//
//
//            snackbar.show();

        }
    }
}
