package com.centroid.integraaccounts.receivers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.widget.Toast;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.SplashActivity;

public class DateTimeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String Task=intent.getStringExtra("Task");


       // Toast.makeText(context,"Time starts now",Toast.LENGTH_SHORT).show();

        String title = "Reminder";

        String message = Task;


        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "mychannel";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel;
// Create a notification and set the notification channel.

        Notification notification = null;

        NotificationManager mNotificationManager;

        Intent in = new Intent(context, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, in, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(context, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)

                    .setChannelId(CHANNEL_ID)
                    .setStyle(new Notification.BigTextStyle().bigText(message))
                    .build();

            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);

        } else {

            notification = new Notification.Builder(context)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setStyle(new Notification.BigTextStyle().bigText(message))

                    .build();
            mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

//                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        notification.setSmallIcon(R.drawable.icon_transperent);
//                        notification.setColor(getResources().getColor(R.color.notification_color));
//                    } else {
//                        notification.setSmallIcon(R.drawable.icon);
//                    }


        MediaPlayer mp;
        mp = MediaPlayer.create(context, R.raw.notifysound);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();


// Issue the notification.
        mNotificationManager.notify(notifyID++, notification);

    }
}
