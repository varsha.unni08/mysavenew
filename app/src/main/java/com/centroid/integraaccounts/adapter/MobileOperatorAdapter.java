package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.services.OperatorSelectionListener;

public class MobileOperatorAdapter extends RecyclerView.Adapter<MobileOperatorAdapter.MobileOperatorHolder> {

    Context context;
//    String arrmonth[] = {"Airtel", "VI", "Jio", "BSNL"};
String arrmonth[] = {"Airtel", "VI", "Jio", "BSNL"};
    String arrspkey[] = {"3", "VIL", "116", "4"};
    int drawable_key[]={R.drawable.airtel,R.drawable.vi,R.drawable.jio,R.drawable.bsnl};
    OperatorSelectionListener operatorSelectionListener;

    public MobileOperatorAdapter(Context context,OperatorSelectionListener operatorSelectionListener) {
        this.context = context;
        this.operatorSelectionListener=operatorSelectionListener;
    }

    public class MobileOperatorHolder extends RecyclerView.ViewHolder{
        ImageView imgoperator;
        TextView txtSpinner;


        public MobileOperatorHolder(@NonNull View itemView) {
            super(itemView);
            txtSpinner=itemView.findViewById(R.id.txtSpinner);
            imgoperator=itemView.findViewById(R.id.imgoperator);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(operatorSelectionListener!=null)
                    {

                        operatorSelectionListener.onOperatorSelected(getAdapterPosition(),arrmonth[getAdapterPosition()]);
                    }

                }
            });
        }


    }

    @NonNull
    @Override
    public MobileOperatorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mobileop_adapter,parent,false);

        return new MobileOperatorHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MobileOperatorHolder holder, int position) {


         holder.imgoperator.setImageResource(drawable_key[position]);
         holder.txtSpinner.setText(arrmonth[position]);

    }

    @Override
    public int getItemCount() {
        return arrspkey.length;
    }
}
