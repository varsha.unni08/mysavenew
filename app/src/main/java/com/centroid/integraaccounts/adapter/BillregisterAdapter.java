package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.util.List;

public class BillregisterAdapter extends RecyclerView.Adapter<BillregisterAdapter.BillHolder> {

    Context context;
    List<Accounts>accounts;

    public BillregisterAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }


    public class BillHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtBillno,txtCustomer,txtAmount;

        public BillHolder(@NonNull View itemView) {
            super(itemView);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtBillno=itemView.findViewById(R.id.txtBillno);
            txtCustomer=itemView.findViewById(R.id.txtCustomer);
            txtAmount=itemView.findViewById(R.id.txtAmount);
        }
    }


    @NonNull
    @Override
    public BillHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.layout_billregister,parent,false);


        return new BillHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BillHolder holder, int position) {

        try {
            holder.setIsRecyclable(false);

            holder.txtdate.setText(accounts.get(position).getACCOUNTS_date());
            holder.txtAmount.setText(accounts.get(position).getACCOUNTS_amount());
            holder.txtBillno.setText(Utils.BillVoucherDetails.billvoucherNumber+accounts.get(position).getBillvouchernumber());

            int id = accounts.get(position).getACCOUNTS_id();

            List<Accounts> accounts = new DatabaseHelper(context).getAccountsDataByEntryId(id + "");

            if (accounts.size() > 0) {

                String cid = accounts.get(0).getACCOUNTS_setupid();

                List<CommonData> commonData = new DatabaseHelper(context).getDataByID(cid, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if (commonData.size() > 0) {

                    JSONObject jso = new JSONObject(commonData.get(0).getData());


                    holder.txtCustomer.setText(jso.getString("Accountname"));

                }

                // holder.txtCustomer
            }

        }catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
