package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.AddAssetActivity;
import com.centroid.integraaccounts.views.AssetListActivity;
import com.centroid.integraaccounts.views.BankBalanceActivity;
import com.centroid.integraaccounts.views.BankVoucherActivity;
import com.centroid.integraaccounts.views.BillVoucherListActivity;
import com.centroid.integraaccounts.views.BudgetListActivity;
import com.centroid.integraaccounts.views.DiaryActivity;
import com.centroid.integraaccounts.views.InsuranceActivity;
import com.centroid.integraaccounts.views.InvestmentListActivity;
import com.centroid.integraaccounts.views.LedgerActivity;
import com.centroid.integraaccounts.views.LiabilitiesActivity;
import com.centroid.integraaccounts.views.ReciptListActivity;
import com.centroid.integraaccounts.views.TasksActivity;
import com.centroid.integraaccounts.views.PaymentVoucherActivity;
import com.centroid.integraaccounts.views.WalletListActivity;

public class MainItemAdapter extends RecyclerView.Adapter<MainItemAdapter.MainItemHolder> {


    Context context;


   // int arr[]={R.string.payment,R.string.receipt,R.string.billing,R.string.bank,R.string.cash,R.string.task,R.string.asset,R.string.liability,R.string.insurance,R.string.investment,R.string.diary,R.string.budget};


    public MainItemAdapter(Context context) {
        this.context = context;
    }



    public class MainItemHolder extends RecyclerView.ViewHolder {

        TextView txt;
        ImageView img;
        LinearLayout layout_bg;


        public MainItemHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            img=itemView.findViewById(R.id.img);
            layout_bg=itemView.findViewById(R.id.layout_bg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (getAdapterPosition())
                    {

                        case 0:

                            Intent intent=new Intent(context, PaymentVoucherActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent);




                            break;


                        case 1 :

                            Intent intent2=new Intent(context, ReciptListActivity.class);
                           // intent2.putExtra("data",2);
                            context.startActivity(intent2);

                            break;

                        case 2 :

//                            Intent intent1=new Intent(context, PaymentVoucherActivity.class);
//                            intent1.putExtra("data",3);
//                            context.startActivity(intent1);

                            Intent intent22=new Intent(context, BillVoucherListActivity.class);
                            // intent2.putExtra("data",2);
                            context.startActivity(intent22);

                            break;

                        case 3:

                            Intent intent3=new Intent(context, WalletListActivity.class);
                            // intent2.putExtra("data",2);
                            context.startActivity(intent3);

                            break;

                        case 4:

                            Intent inten1=new Intent(context, LedgerActivity.class);
                            inten1.putExtra("Bankcash",1);
                            context.startActivity(inten1);

                            break;

                        case 5:

                            Intent intent5=new Intent(context, TasksActivity.class);
                           // intent5.putExtra("data",2);
                            context.startActivity(intent5);

                            break;
                        case 6:

                            Intent intent6=new Intent(context, AssetListActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent6);


                            break;
                        case 7:

                            Intent intent7=new Intent(context, LiabilitiesActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent7);



                            break;
                        case 8:

                            Intent intent8=new Intent(context, InsuranceActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent8);

                            break;
                        case 9:

                            Intent intent9=new Intent(context, InvestmentListActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent9);


                            break;
                        case 10:

                            Intent intent10=new Intent(context, DiaryActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent10);

                            break;
                        case 11:

                            Intent intent11=new Intent(context, BudgetListActivity.class);
                            // intent5.putExtra("data",2);
                            context.startActivity(intent11);

                            break;



                    }


                }
            });
        }
    }

    @NonNull
    @Override
    public MainItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mainitem,parent,false);

        return new MainItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MainItemHolder holder, int position) {

        holder.setIsRecyclable(false);

        String languagedata= LocaleHelper.getPersistedData(context,"en");

        Context conte = LocaleHelper.setLocale(context, languagedata);
        Resources resources = conte.getResources();


        switch (position)
        {
           case 0:

               holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.paymentvouchercolour));
               holder.img.setImageResource(R.drawable.ic_payment);
               holder.txt.setText(resources.getString(R.string.paymentvoucher));
               //holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.tilecolor1));

               break;
            case 1:

                holder.img.setImageResource(R.drawable.ic_receipt_black);
               // holder.txt.setText(R.string.receipt);

                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.receiptcolour));
                holder.txt.setText(resources.getString(R.string.receipt));
               // holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.tilecolor2));

                break;
            case 2:

                holder.img.setImageResource(R.drawable.ic_bill);
               // holder.txt.setText(R.string.billing);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.billvouchercolour));
                holder.txt.setText(resources.getString(R.string.billing));

                break;
            case 3:

                holder.img.setImageResource(R.drawable.ic_account_balance_wallet);
               // holder.txt.setText(R.string.bank);

                holder.txt.setText(resources.getString(R.string.wallet));

                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                break;
            case 4:

                holder.img.setImageResource(R.drawable.ic_monetization);
                //holder.txt.setText(R.string.cash);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.cash));

                break;
            case 5:

                holder.img.setImageResource(R.drawable.ic_assignment);
               // holder.txt.setText(R.string.task);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.task));

                break;
            case 6:

                holder.img.setImageResource(R.drawable.ic_assets);
              //  holder.txt.setText(R.string.asset);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.asset));

                break;
            case 7:

                holder.img.setImageResource(R.drawable.ic_clipboard);
              //  holder.txt.setText(R.string.liability);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.liability));

                break;
            case 8:

                holder.img.setImageResource(R.drawable.ic_life_insurance);
               // holder.txt.setText(R.string.insurance);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.insurance));

                break;
            case 9:

                holder.img.setImageResource(R.drawable.ic_profits);
               // holder.txt.setText(R.string.investment);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.investment));

                break;
            case 10:

                holder.img.setImageResource(R.drawable.ic_journal);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
             //   holder.txt.setText(R.string.diary);

                holder.txt.setText(resources.getString(R.string.diary));

                break;
            case 11:

                holder.img.setImageResource(R.drawable.ic_business_center_);
               // holder.txt.setText(R.string.budget);
                holder.layout_bg.setBackgroundColor(ContextCompat.getColor(context,R.color.appbg));
                holder.txt.setText(resources.getString(R.string.budget));

                break;


        }


    }

    @Override
    public int getItemCount() {
        return 12;
    }
}
