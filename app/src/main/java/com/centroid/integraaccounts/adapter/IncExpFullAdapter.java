package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.IncExpListActivity;

import org.json.JSONObject;

import java.util.List;

public class IncExpFullAdapter extends RecyclerView.Adapter<IncExpFullAdapter.IncExpHolder> {



    Context context;
    List<Accounts>accounts;
    String debitcred;

    int cashbank;

    public IncExpFullAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;

    }

    public class IncExpHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtaccount,txtamount,txttype,txtAccounthold,txtDebit,txtCredit;

        LinearLayout layoutin;


        public IncExpHolder(@NonNull View itemView) {
            super(itemView);
            txttype=itemView.findViewById(R.id.txttype);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtAccounthold=itemView.findViewById(R.id.txtAccounthold);
            layoutin=itemView.findViewById(R.id.layoutin);
            txtDebit=itemView.findViewById(R.id.txtDebit);
            txtCredit=itemView.findViewById(R.id.txtCredit);
        }
    }


    @NonNull
    @Override
    public IncExpHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_incexp,parent,false);

        return new IncExpHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IncExpHolder holder, int position) {
        holder.setIsRecyclable(false);

        if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.paymentvoucher)
        {


            holder.layoutin.setBackgroundColor(ContextCompat.getColor(context,R.color.paymentvouchercolour));


        }


        if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.receiptvoucher)
        {
            holder.layoutin.setBackgroundColor(ContextCompat.getColor(context,R.color.receiptcolour));

        }

        if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.billvoucher)
        {
            holder.layoutin.setBackgroundColor(ContextCompat.getColor(context,R.color.billvouchercolour));

        }

        if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.bankvoucher)
        {

          //  holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.bankvouchercolour));

            holder.layoutin.setBackgroundColor(ContextCompat.getColor(context,R.color.bankvouchercolour));

        }

        if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.journalvoucher)
        {

           // holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.journalvouchercolour));

            holder.layoutin.setBackgroundColor(ContextCompat.getColor(context,R.color.journalvouchercolour));

        }




        holder.txtdate.setText(accounts.get(position).getACCOUNTS_date());

        double d=Double.parseDouble(accounts.get(position).getACCOUNTS_amount());

        if(d<0||String.valueOf(d).contains("-"))
        {
            d=d*-1;
        }





        holder.txtamount.setText(d+" "+context.getString(R.string.rs));




            if (accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                holder.txttype.setText("Debit");
                holder.txtDebit.setText(d+" ");
                holder.txtCredit.setText("");


            } else {

                holder.txttype.setText("Credit");

                holder.txtCredit.setText(d+" ");
                holder.txtDebit.setText("");
            }





        if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("-1")) {



          //  if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0")) {


                List<Accounts> accbyEntry = new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id() + "");

                List<Accounts> accbyId = new DatabaseHelper(context).getAccountsDataBYid(accounts.get(position).getACCOUNTS_entryid() + "");

                if(accbyEntry.size()>0) {


                    List<CommonData> cm = new DatabaseHelper(context).getDataByID(accbyEntry.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                    if (cm.size() > 0) {

                        try {

                            JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                            holder.txtaccount.setText(Utils.getCapsSentences(context,jsonObject.getString("Accountname")));


                        } catch (Exception e) {

                        }

                    }
                    else {

                        holder.txtaccount.setText("Cash");
                    }


                }





                 else   if(accbyId.size()>0) {


                        List<CommonData> cm = new DatabaseHelper(context).getDataByID(accbyId.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                        if (cm.size() > 0) {

                            try {

                                JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                                holder.txtaccount.setText(Utils.getCapsSentences(context,jsonObject.getString("Accountname")));


                            } catch (Exception e) {

                            }

                        }
                        else {

                            holder.txtaccount.setText("Cash");
                        }



                    }




              //  }

                 else {




                    holder.txtaccount.setText("Cash");
                }


          //  }




        }
        else {

            holder.txtaccount.setText("Opening Balance");




        }




    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
