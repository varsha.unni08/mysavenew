package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.VisitCard;
import com.centroid.integraaccounts.data.domain.VisitCardImg;
import com.centroid.integraaccounts.views.FeedbackMessageActivity;
import com.centroid.integraaccounts.views.MainActivity;
import com.centroid.integraaccounts.views.VisitCardFillActivity;
import com.centroid.integraaccounts.views.VisitCardHistoryActivity;

import org.json.JSONObject;

import java.util.List;

public class VisitCardHistoryAdapter extends RecyclerView.Adapter<VisitCardHistoryAdapter.VisitCardHolder> {


    Context context;
    List<VisitCard>commonData;

    public VisitCardHistoryAdapter(Context context, List<VisitCard> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class VisitCardHolder extends RecyclerView.ViewHolder{
        ImageView imgCard;
        TextView txtTitle;

        Button btnDelete;

        public VisitCardHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            imgCard=itemView.findViewById(R.id.imgCard);
            btnDelete=itemView.findViewById(R.id.btnDelete);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent7=new Intent(context, VisitCardFillActivity.class);
                    intent7.putExtra("data", commonData.get(getAdapterPosition()));

                    context.startActivity(intent7);

                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage("Do You Want To Delete History Now ? ");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                            new DatabaseHelper(context).deleteVisitCardData(commonData.get(getAdapterPosition()).getId()+"");

                            commonData.remove(getAdapterPosition());

                            notifyDataSetChanged();


                            ((VisitCardHistoryActivity)context).getVisitCardHistory();


                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();




                }
            });
        }
    }


    @NonNull
    @Override
    public VisitCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_visitcardholder,parent,false);


        return new VisitCardHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitCardHolder holder, int position) {

        try{

            holder.setIsRecyclable(false);

            VisitCard vc=commonData.get(position);

            JSONObject jsonObject1=new JSONObject(vc.getData());


            if(!TextUtils.isEmpty(jsonObject1.getString("name")))
            {
                holder.txtTitle.append("Name : "+jsonObject1.getString("name")+"\n");
            }

            if(!TextUtils.isEmpty(jsonObject1.getString("phone")))
            {
                holder.txtTitle.append("Phone : "+jsonObject1.getString("phone")+"\n");
            }
            if(!TextUtils.isEmpty(jsonObject1.getString("website")))
            {
                holder.txtTitle.append("Website : "+jsonObject1.getString("website")+"\n");
            }

            if(!TextUtils.isEmpty(jsonObject1.getString("designation")))
            {
                holder.txtTitle.append("Designation : "+jsonObject1.getString("designation")+"\n");
            }


          int d=  jsonObject1.getInt("cardbg");



            List<VisitCardImg>vcardimg=new DatabaseHelper(context).getImgData();

            for (int i=0;i<vcardimg.size();i++)
            {

                if(d==i)
                {



                    Drawable image = new BitmapDrawable(BitmapFactory.decodeByteArray(vcardimg.get(i).getArrdata(), 0, vcardimg.get(i).getArrdata().length));




                 holder.imgCard.setImageDrawable(image);

                    break;

                }

            }







        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
