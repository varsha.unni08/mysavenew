package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.UPIapps;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;

import java.util.List;

public class UPIListAdapter  extends RecyclerView.Adapter<UPIListAdapter.UpiHolder> {

    Context context;
    List<UPIapps>upIapps;

    public UPIListAdapter(Context context, List<UPIapps> upIapps) {
        this.context = context;
        this.upIapps = upIapps;
    }

    public class UpiHolder extends RecyclerView.ViewHolder{

        ImageView imgicon;
        TextView txtName;

        public UpiHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            imgicon=itemView.findViewById(R.id.imgicon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ((AddPaymentVoucherActivity)context).selectUpiApp(upIapps.get(getAdapterPosition()));
                }
            });
        }
    }

    @NonNull
    @Override
    public UpiHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_upiapp,parent,false);



        return new UpiHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UpiHolder holder, int position) {
        holder.setIsRecyclable(false);
        holder.imgicon.setImageResource(upIapps.get(position).getIcon());
        holder.txtName.setText(upIapps.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return upIapps.size();
    }
}
