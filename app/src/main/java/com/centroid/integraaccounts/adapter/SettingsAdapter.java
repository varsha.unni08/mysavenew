package com.centroid.integraaccounts.adapter;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.MVersion;
import com.centroid.integraaccounts.data.domain.StateData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.AddBankCashDataActivity;
import com.centroid.integraaccounts.views.AppRenewalActivity;
import com.centroid.integraaccounts.views.AssetListActivity;
import com.centroid.integraaccounts.views.LoginActivity;
import com.centroid.integraaccounts.views.ProfileActivity;
import com.centroid.integraaccounts.views.SettingsActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsHolder> {

    Context ctx;
    public SettingsAdapter(Context m) {

        this.ctx=m;
    }

    int arr[]={R.string.profile,R.string.applock,R.string.databackup,R.string.restoredata,R.string.appupdate,R.string.renewal,R.string.logout,R.string.deleteaccount};

   // int arr[]={R.string.profile,R.string.applock,R.string.databackup,R.string.restoredata,R.string.appupdate,R.string.logout,R.string.deleteaccount};

    public class SettingsHolder extends RecyclerView.ViewHolder{

        TextView txt;
        SwitchCompat sw;
        ImageView img;

        public SettingsHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            sw=itemView.findViewById(R.id.sw);
            img=itemView.findViewById(R.id.img);
            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                    new PreferenceHelper(ctx).putBooleanData(Utils.applockedkey,b);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition())
                    {


                        case 0:

                            Intent newIntent = new Intent(ctx, ProfileActivity.class);

                            ctx.startActivity(newIntent);

                            break;
                        case 1:

                            ((SettingsActivity)ctx).showPin();


                            break;

                        case 2:

                            final String items[]={"Google Drive","Server"};


                            new androidx.appcompat.app.AlertDialog.Builder(ctx)
                                    .setTitle("Backup Your Data Via ")
                                    .setSingleChoiceItems(items, 0, null)
                                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            dialog.dismiss();

                                            int selectedPosition = ((androidx.appcompat.app.AlertDialog)dialog).getListView().getCheckedItemPosition();
                                            String item=items[selectedPosition];

                                            if(selectedPosition==0)
                                            {

                                                String permission="";

                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                                                    permission=Manifest.permission.READ_MEDIA_IMAGES;

                                                }
                                                else{

                                                    permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                                                }


                                                if(ContextCompat.checkSelfPermission(ctx, permission)!=PackageManager.PERMISSION_GRANTED) {

                                                    ((SettingsActivity) ctx).requestWritePermission(permission);

                                                }
                                                else {
//
            //                                        ((SettingsActivity) ctx).uploadFileToDrive();
                                                    ((SettingsActivity) ctx).prepareDataToUpload();
                                                }
                                            }



                                            if(selectedPosition==1)
                                            {
                                                String permission="";

                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {

                                                    permission=Manifest.permission.READ_MEDIA_IMAGES;

                                                }
                                                else{

                                                    permission=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                                                }

                                                if(ContextCompat.checkSelfPermission(ctx, permission)!=PackageManager.PERMISSION_GRANTED) {

                                                    ((SettingsActivity) ctx).requestWritePermission(permission);

                                                }
                                                else {

                                                    ((SettingsActivity) ctx).uploadDataToServer();
                                                }
                                            }



                                        }
                                    })
                                    .show();






                            break;

                        case 3:

                            if(Utils.checkDBTablempty(ctx) ) {

                                final String items1[] = {"From Google Drive", "From Server"};

                                new androidx.appcompat.app.AlertDialog.Builder(ctx)
                                        .setTitle("Restore Your Data Via ")
                                        .setSingleChoiceItems(items1, 0, null)
                                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        })
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                dialog.dismiss();
                                                int selectedPosition = ((androidx.appcompat.app.AlertDialog) dialog).getListView().getCheckedItemPosition();
                                                String item = items1[selectedPosition];

                                                if (selectedPosition == 0) {

                                                    ((SettingsActivity) ctx).downloadDataFromGoogleDrive();
//                                                    Utils.showAlertWithSingle(ctx, "Please Select Emaild ID of Previous Data Backup", new DialogEventListener() {
//                                                        @Override
//                                                        public void onPositiveButtonClicked() {
//
//                                                        }
//
//                                                        @Override
//                                                        public void onNegativeButtonClicked() {
//
//                                                        }
//                                                    });




                                                }


                                                if (selectedPosition == 1) {
                                                    ((SettingsActivity) ctx).downloadDataFromServer();
                                                }

                                            }
                                        })
                                        .show();
                            }
                            else {


                            }



                            break;


                        case 4 :
                            if(!new PreferenceHelper(ctx).getBoolData(Utils.Needversionupdate)) {

                                final ProgressFragment progressFragment = new ProgressFragment();
                                progressFragment.show(((AppCompatActivity) ctx).getSupportFragmentManager(), "fkjfk");


                                Map<String, String> params = new HashMap<>();
                                params.put("timestamp", Utils.getTimestamp());


                                new RequestHandler(ctx, params, new ResponseHandler() {
                                    @Override
                                    public void onSuccess(String data) {

                                        progressFragment.dismiss();

                                        if (data != null) {

                                            MVersion mVersion = new GsonBuilder().create().fromJson(data, MVersion.class);
//                                        if(response!=null)
//                                        {

                                            if (mVersion != null) {

                                                if (mVersion.getStatus() == 1) {


                                                    DisplayMetrics displayMetrics = new DisplayMetrics();
                                                    ((SettingsActivity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

                                                    double h = displayMetrics.heightPixels / 1.2;
                                                    int height = (int) h;
                                                    int width = displayMetrics.widthPixels;

                                                    final Dialog dialog = new Dialog(ctx);
                                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                    dialog.setContentView(R.layout.layout_appupdates);

                                                    final TextView txtversion = dialog.findViewById(R.id.txtversion);
                                                    LinearLayout layout_update = dialog.findViewById(R.id.layout_update);

                                                    Button btnupdate = dialog.findViewById(R.id.btnupdate);

                                                    try {

                                                        if (mVersion.getData() != null) {

                                                            String version = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;

                                                            double liveVersion = Double.parseDouble(mVersion.getData().getAppVersion());


                                                            double currentversion = Double.parseDouble(version);

                                                            if (currentversion < liveVersion) {

                                                                layout_update.setVisibility(View.VISIBLE);
                                                            }

                                                            txtversion.setText(version);

                                                        }

                                                        btnupdate.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {


                                                                try {

                                                                    String link = mVersion.getData().getFilepath();


                                                                    Intent viewIntent =
                                                                            new Intent("android.intent.action.VIEW",
                                                                                    Uri.parse(link));
                                                                    ctx.startActivity(viewIntent);
                                                                } catch (Exception e) {

                                                                }


                                                            }
                                                        });


                                                    } catch (PackageManager.NameNotFoundException e) {
                                                        e.printStackTrace();
                                                    }


                                                    dialog.getWindow().setLayout(width, height);

                                                    dialog.show();

                                                }


                                            }


//                                        }
//                                        else {
//
//
//
//                                        }


                                        }

                                    }

                                    @Override
                                    public void onFailure(String err) {
                                        progressFragment.dismiss();

                                        //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                                    }
                                }, Utils.WebServiceMethodes.getMobileAppVersion + "?timestamp=" + Utils.getTimestamp(), Request.Method.GET).submitRequest();

                            }






                            break;

                        case 5:



                                Intent newIntente = new Intent(ctx, AppRenewalActivity.class);

                                ctx.startActivity(newIntente);



                            break;



                        case 6 :

                            AlertDialog.Builder builder=new AlertDialog.Builder(ctx);
                            builder.setMessage("Do You Want To Logout Now ? ");
                            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();

                                    new PreferenceHelper(ctx).putData(Utils.userkey,"");

                                    Intent newIntent = new Intent(ctx, LoginActivity.class);
                                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                    ctx.startActivity(newIntent);

                                }
                            });
                            builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });

                            builder.show();






                            break;

                        case 7:

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            ((AppCompatActivity)ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            double height = displayMetrics.heightPixels / 1.5;
                            int width = displayMetrics.widthPixels;

                            int a=(int)height;

                            final Dialog dialog = new Dialog(ctx);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.layout_deleteaccount_warning);

                            Button btn_no=dialog.findViewById(R.id.btn_no);

                            Button btn_yes=dialog.findViewById(R.id.btn_yes);

                            TextView txtinfo=dialog.findViewById(R.id.txtinfo);

                            txtinfo.setText(Utils.getCapsSentences(ctx,"Demo account/App License deletion \n\n If deleted , all your credentials will be removed from the database and you cannot login to the app and website anymore \n \n\n" +
                                    " Do you want to contitnue ?"));

                            btn_no.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });

                            btn_yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();

showConfirmationdialog();


                                }
                            });


                            dialog.getWindow().setLayout(width,a);
                            dialog.show();











                            break;



                    }

                }
            });
        }
    }

    @NonNull
    @Override
    public SettingsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_moreadapter,parent,false);


        return new SettingsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsHolder holder, int position) {
        holder.setIsRecyclable(false);
        String languagedata = LocaleHelper.getPersistedData(ctx, "en");
        Context context= LocaleHelper.setLocale(ctx, languagedata);

        Resources resources=context.getResources();
        holder.txt.setText( Utils.getCapsSentences(context,resources.getString(arr[position]))  );
       // holder.txt.setText(resources.getString(arr[position]));

        if(position==1)
        {
            holder.img.setVisibility(View.GONE);
            holder.sw.setVisibility(View.VISIBLE);

            if(new PreferenceHelper(ctx).getBoolData(Utils.applockedkey))
            {
                holder.sw.setChecked(true);
            }
            else {

                holder.sw.setChecked(false);
            }
        }


        if(position==3)
        {


            if(Utils.checkDBTablempty(context) )
            {


                holder.itemView.setAlpha(1.0f);




            }
            else {

                holder.itemView.setAlpha(0.5f);
            }
        }

        if(position==4)
        {

            if(!new PreferenceHelper(ctx).getBoolData(Utils.Needversionupdate)) {

                holder.itemView.setAlpha(1.0f);
            }
            else{


                holder.itemView.setAlpha(0.5f);

            }
        }




    }

    @Override
    public int getItemCount() {
        return arr.length;
    }



    public  void  showConfirmationdialog()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((AppCompatActivity)ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        double height = displayMetrics.heightPixels / 1.7;
        int width = displayMetrics.widthPixels;

        int a=(int)height;

        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_confirmdelete);

        EditText edtPhone=dialog.findViewById(R.id.edtPhone);

        EditText edtPassword=dialog.findViewById(R.id.edtPassword);

        Button btn_submit=dialog.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPhone.getText().toString().equalsIgnoreCase(""))
                {
                    if(!edtPassword.getText().toString().equalsIgnoreCase(""))

                    {


                        String uuid = "";

                        try {

                            String uniquePseudoID = "35" +
                                    Build.BOARD.length() % 10 +
                                    Build.BRAND.length() % 10 +
                                    Build.DEVICE.length() % 10 +
                                    Build.DISPLAY.length() % 10 +
                                    Build.HOST.length() % 10 +
                                    Build.ID.length() % 10 +
                                    Build.MANUFACTURER.length() % 10 +
                                    Build.MODEL.length() % 10 +
                                    Build.PRODUCT.length() % 10 +
                                    Build.TAGS.length() % 10 +
                                    Build.TYPE.length() % 10 +
                                    Build.USER.length() % 10;
                            String serial = Build.getRadioVersion();
                            uuid = new UUID(uniquePseudoID.hashCode(), serial.hashCode()).toString();

                            // ffffffff-8a93-ca89-ffff-ffffa56d5aee

                            ;
                        } catch (SecurityException e) {

                            Log.e("Exception", e.toString());
                        }



                        final ProgressFragment progressFragment = new ProgressFragment();
                        progressFragment.show(((AppCompatActivity) ctx).getSupportFragmentManager(), "fkjfk");

                        Map<String, String> params = new HashMap<>();
                        params.put("uuid", uuid);
                        params.put("mobile", edtPhone.getText().toString().trim());
                        params.put("password", edtPassword.getText().toString().trim());
                        params.put("timestamp", Utils.getTimestamp());


                        new RequestHandler(ctx, params, new ResponseHandler() {
                            @Override
                            public void onSuccess(String data) {
                                progressFragment.dismiss();
                                // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                                try {

                                    JSONObject jsonObject = new JSONObject(data);

                                    if (jsonObject.getInt("status") == 1) {
                                        dialog.dismiss();

                                        final ProgressFragment progressFragment=new ProgressFragment();
                                        progressFragment.show(((AppCompatActivity)ctx).getSupportFragmentManager(),"fkjfk");



                                        Map<String,String> params=new HashMap<>();
                                        params.put("timestamp",Utils.getTimestamp());


                                        new RequestHandler(ctx, params, new ResponseHandler() {
                                            @Override
                                            public void onSuccess(String data) {

                                                progressFragment.dismiss();

                                                if(data!=null)
                                                {

                                                    try{
                                                        JSONObject js=new JSONObject(data);

                                                        if(js.getInt("status")==1)
                                                        {

                                                            new PreferenceHelper(ctx).putData(Utils.userkey,"");

                                                            Intent newIntent = new Intent(ctx, LoginActivity.class);
                                                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                                            ctx.startActivity(newIntent);
                                                        }




                                                    }catch (Exception e)
                                                    {

                                                    }






                                                }

                                            }

                                            @Override
                                            public void onFailure(String err) {
                                                progressFragment.dismiss();

                                                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                                            }
                                        },Utils.WebServiceMethodes.deleteAccount+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();






                                    }
                                    else{

                                        Utils.showAlertWithSingle(ctx,"Your account validation is failed",null);


                                    }


                                } catch (Exception e) {

                                }

                            }

                            @Override
                            public void onFailure(String err) {
                                progressFragment.dismiss();

                                //  Toast.makeText(LoginActivity.this, err, Toast.LENGTH_SHORT).show();

                            }
                        }, Utils.WebServiceMethodes.login, Request.Method.POST).submitRequest();


                    }
                    else{



                        Utils.showAlertWithSingle(ctx,"Enter Password",null);
                    }


                }
                else{

                    Utils.showAlertWithSingle(ctx,"Enter Mobile",null);

                }


            }
        });


        dialog.getWindow().setLayout(width,a);
        dialog.show();


    }
}
