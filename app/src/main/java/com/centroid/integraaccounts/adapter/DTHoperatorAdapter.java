package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.services.OperatorSelectionListener;

public class DTHoperatorAdapter extends RecyclerView.Adapter<DTHoperatorAdapter.DthOperatorHolder> {

    Context context;
    OperatorSelectionListener operatorSelectionListener;

    public DTHoperatorAdapter(Context context,OperatorSelectionListener operatorSelectionListener) {
        this.context = context;
        this.operatorSelectionListener=operatorSelectionListener;
    }

    public class DthOperatorHolder extends RecyclerView.ViewHolder{
        ImageView imgoperator;
        TextView txtSpinner;

        public DthOperatorHolder(@NonNull View itemView) {
            super(itemView);
            txtSpinner=itemView.findViewById(R.id.txtSpinner);
            imgoperator=itemView.findViewById(R.id.imgoperator);
            imgoperator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(operatorSelectionListener!=null)
                    {

                        operatorSelectionListener.onOperatorSelected(getAdapterPosition(),RechargePlans.DTHPlans.arr_plans[getAdapterPosition()]);
                    }

                }
            });

            txtSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(operatorSelectionListener!=null)
                    {

                        operatorSelectionListener.onOperatorSelected(getAdapterPosition(),RechargePlans.DTHPlans.arr_plans[getAdapterPosition()]);
                    }

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(operatorSelectionListener!=null)
                    {

                        operatorSelectionListener.onOperatorSelected(getAdapterPosition(),RechargePlans.DTHPlans.arr_plans[getAdapterPosition()]);
                    }

                }
            });
        }
    }

    @NonNull
    @Override
    public DthOperatorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mobileop_adapter,parent,false);

        return new DthOperatorHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DthOperatorHolder holder, int position) {

        holder.imgoperator.setImageResource(RechargePlans.DTHPlans.arr_plans_img[position]);
        holder.txtSpinner.setText(RechargePlans.DTHPlans.arr_plans[position]);
    }

    @Override
    public int getItemCount() {
        return RechargePlans.DTHPlans.arr_plans_img.length;
    }
}
