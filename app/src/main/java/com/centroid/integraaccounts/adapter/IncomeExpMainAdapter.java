package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.IncExpHead;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.IncomexpenditureActivity;

import java.util.List;

public class IncomeExpMainAdapter extends RecyclerView.Adapter<IncomeExpMainAdapter.IncomeExpHolder> {



    Context context;
    List<LedgerAccount>ledgerincome;
    List<LedgerAccount>ledgerexpense;

    List<IncExpHead>incExpHeads;

    String startdate="",endate="";

    double totalexp=0;
    double totalinc=0;

    public IncomeExpMainAdapter(Context context, List<IncExpHead>incExpHeads,  String startdate,String endate) {
        this.context = context;
        this.incExpHeads = incExpHeads;
        this.startdate=startdate;
        this.endate=endate;

    }

    public class IncomeExpHolder extends RecyclerView.ViewHolder{

        TextView txt;

        ImageView imgdropdown;
        LinearLayout layout_inchead;
        RecyclerView recycler;

        TextView txtAccount,txtDebit,txtcredit,txtAction;

        public IncomeExpHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            imgdropdown=itemView.findViewById(R.id.imgdropdown);
            layout_inchead=itemView.findViewById(R.id.layout_inchead);
            recycler=itemView.findViewById(R.id.recycler);



            txtAccount=itemView.findViewById(R.id.txtAccount);
            txtDebit=itemView.findViewById(R.id.txtDebit);
            txtcredit=itemView.findViewById(R.id.txtcredit);
            txtAction=itemView.findViewById(R.id.txtAction);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(incExpHeads.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (IncExpHead incExpHead:incExpHeads
                             ) {

                            incExpHead.setSelected(0);

                        }



                        incExpHeads.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(incExpHeads.get(getAdapterPosition()).getSelected()==1) {

                        for (IncExpHead incExpHead:incExpHeads
                        ) {

                            incExpHead.setSelected(0);

                        }


                        incExpHeads.get(getAdapterPosition()).setSelected(0);

                    }


                 notifyDataSetChanged();



                }
            });


            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(incExpHeads.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (IncExpHead incExpHead:incExpHeads
                        ) {

                            incExpHead.setSelected(0);

                        }



                        incExpHeads.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(incExpHeads.get(getAdapterPosition()).getSelected()==1) {

                        for (IncExpHead incExpHead:incExpHeads
                        ) {

                            incExpHead.setSelected(0);

                        }


                        incExpHeads.get(getAdapterPosition()).setSelected(0);

                    }


                    notifyDataSetChanged();



                }
            });



            imgdropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(incExpHeads.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (IncExpHead incExpHead:incExpHeads
                        ) {

                            incExpHead.setSelected(0);

                        }



                        incExpHeads.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(incExpHeads.get(getAdapterPosition()).getSelected()==1) {

                        for (IncExpHead incExpHead:incExpHeads
                        ) {

                            incExpHead.setSelected(0);

                        }


                        incExpHeads.get(getAdapterPosition()).setSelected(0);

                    }


                    notifyDataSetChanged();



                }
            });
        }
    }





    @NonNull
    @Override
    public IncomeExpHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        IncomeExpHolder viewHolder=null;




            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_incomeholder,parent,false);

            viewHolder=new IncomeExpHolder(v);




        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull IncomeExpHolder holder, int position) {
        holder.setIsRecyclable(false);
        String languagedata = LocaleHelper.getPersistedData(context, "en");
        Context contex= LocaleHelper.setLocale(context, languagedata);

        Resources resources=contex.getResources();

        holder.txtAccount.setText(resources.getString(R.string.accountname));

                holder.txtDebit.setText(resources.getString(R.string.debit));
        holder.txtcredit.setText(resources.getString(R.string.credit));
        holder.txtAction.setText(resources.getString(R.string.action));




        if(incExpHeads.get(position).getSelected()==0)
        {

            holder.recycler.setVisibility(View.GONE);
            holder.layout_inchead.setVisibility(View.GONE);
            holder.imgdropdown.setImageResource(R.drawable.ic_spinnerr);



        }

        else  if(incExpHeads.get(position).getSelected()==1){


            if(incExpHeads.get(position).getLedgerAccounts().size()>0)
            {

                holder.recycler.setVisibility(View.VISIBLE);
                holder.layout_inchead.setVisibility(View.VISIBLE);
                holder.imgdropdown.setImageResource(R.drawable.ic_spinnerdropup);
                holder.recycler.setLayoutManager(new LinearLayoutManager(context));
                holder.recycler.setAdapter(new LedgerAccountAdapter(context, incExpHeads.get(position).getLedgerAccounts(),startdate,endate));

            }



        }









        switch (position)
        {


            case 0:

              //  holder.txt.setText();

                 totalexp=0;

                for (LedgerAccount ledgerAccount:incExpHeads.get(position).getLedgerAccounts()) {

                    totalexp=totalexp+Double.parseDouble(ledgerAccount.getClosingbalance());



                }

                if(totalexp<0)
                {
                    totalexp=totalexp*-1;
                }


                holder.txt.setText(Utils.getCapsSentences(context,resources.getString(R.string.totalincome))+" : "+totalexp);



                break;

            case 1:

                 totalinc=0;

                for (LedgerAccount ledgerAccount:incExpHeads.get(position).getLedgerAccounts()) {

                    totalinc=totalinc+Double.parseDouble(ledgerAccount.getClosingbalance());



                }

                if(totalinc<0)
                {
                    totalinc=totalinc*-1;
                }
                holder.txt.setText(Utils.getCapsSentences(context,resources.getString(R.string.totalexpense))+" : "+totalinc);





                break;














        }



    }

    @Override
    public int getItemCount() {
        return incExpHeads.size();
    }



}
