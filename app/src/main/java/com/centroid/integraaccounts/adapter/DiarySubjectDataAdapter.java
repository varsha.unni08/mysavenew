package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.DiaryActivity;

import java.util.List;

public class DiarySubjectDataAdapter extends RecyclerView.Adapter<DiarySubjectDataAdapter.DiarySubjectDataholder> {


    Context context;
    List<CommonData>cmd;

    public DiarySubjectDataAdapter(Context context, List<CommonData> cmd) {
        this.context = context;
        this.cmd = cmd;
    }

    public class DiarySubjectDataholder extends RecyclerView.ViewHolder{

        TextView txt;

        ImageView imgdelete;

        public DiarySubjectDataholder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            imgdelete=itemView.findViewById(R.id.imgdelete);


            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage(Utils.getCapsSentences(context,"Your Diary data based on this subject will also be deleted. Do you want to delete this subject ?"));
                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                            CommonData cm=cmd.get(getAdapterPosition());

                            String subjectid=cm.getId();



                            new DatabaseHelper(context).deleteData(subjectid, Utils.DBtables.DIARYSUBJECT_table);

                            ((DiaryActivity)context).deleteDataAccordingtoSubject(subjectid);

                            cmd.remove(getAdapterPosition());
                            notifyDataSetChanged();



                        }
                    });
                    builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();









                }
            });
        }
    }


    @NonNull
    @Override
    public DiarySubjectDataholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diarysubjectadapter,parent,false);


        return new DiarySubjectDataholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DiarySubjectDataholder holder, int position) {

holder.txt.setText(cmd.get(position).getData());



    }

    @Override
    public int getItemCount() {
        return cmd.size();
    }
}
