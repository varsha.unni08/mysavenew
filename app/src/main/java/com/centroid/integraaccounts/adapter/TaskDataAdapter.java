package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.TaskData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.AddnewTaskActivity;
import com.centroid.integraaccounts.views.TasksActivity;

import org.json.JSONObject;

import java.util.List;

public class TaskDataAdapter extends RecyclerView.Adapter<TaskDataAdapter.TaskHolder> {


    Context context;
    List<CommonData>taskData;

    public TaskDataAdapter(Context context, List<CommonData> taskData) {
        this.context = context;
        this.taskData = taskData;
    }

    public class TaskHolder extends RecyclerView.ViewHolder{
        TextView txtdata,txtDate,txtTime,txtStatus;

        TextView txtph,txtdate,txtTimehead,txtStatushead,txtreminddatehead,txtReminddate;

        Button btnEdit,btndelete;

        public TaskHolder(@NonNull View itemView) {
            super(itemView);
            txtdata=itemView.findViewById(R.id.txtdata);
            txtDate=itemView.findViewById(R.id.txtDate);
            txtTime=itemView.findViewById(R.id.txtTime);
            txtStatus=itemView.findViewById(R.id.txtStatus);

            txtph=itemView.findViewById(R.id.txtph);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtTimehead=itemView.findViewById(R.id.txtTimehead);
            txtStatushead=itemView.findViewById(R.id.txtStatushead);

            txtReminddate=itemView.findViewById(R.id.txtReminddate);
            txtreminddatehead=itemView.findViewById(R.id.txtreminddatehead);






            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, AddnewTaskActivity.class);
                    intent.putExtra("task",taskData.get(getAdapterPosition()));

                    context.startActivity(intent);
                }
            });




            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Utils.showAlert(context, "Do You Want To Delete Now ? ", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            int p=getAdapterPosition();

                            new DatabaseHelper(context).deleteData(taskData.get(p).getId(),Utils.DBtables.TABLE_TASK);


                            taskData.remove(p);


                            notifyItemRemoved(p);





                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent=new Intent(context, AddnewTaskActivity.class);
                    intent.putExtra("task",taskData.get(getAdapterPosition()));

                    context.startActivity(intent);
                }
            });
        }


    }


    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_taskdata,parent,false);


        return new TaskHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskHolder holder, int position) {
        holder.setIsRecyclable(false);

//        jsonObject.put("name",edtName.getText().toString());
//        jsonObject.put("date",date);
//        jsonObject.put("time",timeselected);
//        jsonObject.put("status",0);

        try{




            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

            final Resources resources=conte.getResources();



            holder.txtph.setText(resources.getString(R.string.task));
            holder.txtdate.setText(resources.getString(R.string.date));
            holder.txtTimehead.setText(resources.getString(R.string.time));
            holder.txtStatushead.setText(resources.getString(R.string.status));
            holder.txtreminddatehead.setText(Utils.getCapsSentences(context,resources.getString(R.string.remindingdate)));



            String arr[]=resources.getStringArray(R.array.status);





            JSONObject jsonObject=new JSONObject(taskData.get(position).getData());

            String name=jsonObject.getString("name");
            String date=jsonObject.getString("date");
            String time=jsonObject.getString("time");
            if(jsonObject.has("reminddate"))
            {
                holder.txtReminddate.setText(jsonObject.getString("reminddate"));
            }


            int status=jsonObject.getInt("status");

            if(status==0)
            {
                holder.txtStatus.setText(arr[0]);
            }

            if(status==1)
            {
                holder.txtStatus.setText(arr[1]);
                holder.txtStatus.setTextColor(Color.parseColor("#75c044"));
            }

            if(status==2)
            {
                holder.txtStatus.setText(arr[2]);
                holder.txtStatus.setTextColor(Color.parseColor("#A9F00000"));
            }


            holder.txtdata.setText(name);
            holder.txtDate.setText(date);
            holder.txtTime.setText(time);





        }catch (Exception e)
        {

        }




    }

    @Override
    public int getItemCount() {
        return taskData.size();
    }
}
