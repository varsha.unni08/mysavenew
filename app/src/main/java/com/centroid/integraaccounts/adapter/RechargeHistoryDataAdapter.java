package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.RechargeHistoryData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.RechargeHistoryActivity;
import com.centroid.integraaccounts.views.rechargeViews.DTHRechargeDashboardActivity;
import com.centroid.integraaccounts.views.rechargeViews.MobileRechargeDashboardActivity;

import java.util.List;

public class RechargeHistoryDataAdapter extends RecyclerView.Adapter<RechargeHistoryDataAdapter.RechargeHistoryHolder> {

    Context context;
    List<RechargeHistoryData>rechargeHistoryData;

    public RechargeHistoryDataAdapter(Context context, List<RechargeHistoryData> rechargeHistoryData) {
        this.context = context;
        this.rechargeHistoryData = rechargeHistoryData;
    }


    public class RechargeHistoryHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtStatus,txtRefundTransactionid,txtRechargetatus,txtRechargeDate;

        Button btn_retry;
        ImageView imgoperator;

        public RechargeHistoryHolder(@NonNull View itemView) {
            super(itemView);
            btn_retry=itemView.findViewById(R.id.btn_retry);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            imgoperator=itemView.findViewById(R.id.imgoperator);
            txtRechargetatus=itemView.findViewById(R.id.txtRechargetatus);
            txtRefundTransactionid=itemView.findViewById(R.id.txtRefundTransactionid);
            txtRechargeDate=itemView.findViewById(R.id.txtRechargeDate);
            imgoperator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                        if(context instanceof MobileRechargeDashboardActivity) {

                            ((MobileRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                        }
                        else{

                            ((DTHRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                        }



                }
            });

            txtmonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(context instanceof MobileRechargeDashboardActivity) {

                        ((MobileRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                    else{

                        ((DTHRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                }
            });

            txtStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(context instanceof MobileRechargeDashboardActivity) {

                        ((MobileRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                    else{

                        ((DTHRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                }
            });

            txtRechargeDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(context instanceof MobileRechargeDashboardActivity) {

                        ((MobileRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                    else{

                        ((DTHRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                }
            });

            txtRechargetatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(context instanceof MobileRechargeDashboardActivity) {

                        ((MobileRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                    else{

                        ((DTHRechargeDashboardActivity) context).retryRechargeFromHistory(rechargeHistoryData.get(getAdapterPosition()));

                    }
                }
            });



        }
    }

    @NonNull
    @Override
    public RechargeHistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_rechargehistory,parent,false);

        return new RechargeHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RechargeHistoryHolder holder, int position) {

        holder.txtmonth.setText("");
  holder.txtRechargeDate.setText(rechargeHistoryData.get(position).getRechargeDate());

        holder.txtmonth.append(rechargeHistoryData.get(position).getMobileNumber()+"\n ₹ "+rechargeHistoryData.get(position).getAmount());
//        holder.txtmonth.append("\n\nOperator : "+rechargeHistoryData.get(position).getOperator());
        if(rechargeHistoryData.get(position).getRechargeType().equalsIgnoreCase("1")) {
            if (rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Jio")) {

                holder.imgoperator.setImageResource(R.drawable.jio);
            } else if (rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("BSNL")) {
                holder.imgoperator.setImageResource(R.drawable.bsnl);

            } else if (rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Vi")) {

                holder.imgoperator.setImageResource(R.drawable.vi);
            } else {

                holder.imgoperator.setImageResource(R.drawable.airtel);
            }
        }

       else if(rechargeHistoryData.get(position).getRechargeType().equalsIgnoreCase("2"))
        {
            holder.txtmonth.append("\n"+rechargeHistoryData.get(position).getAccountNumber()+"\n");

            if(rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Dish TV"))
            {

                holder.imgoperator.setImageResource(R.drawable.dishtv);
            }
            else if(rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Airtel Digital TV"))
            {
                holder.imgoperator.setImageResource(R.drawable.airteldigitaltv);

            }
            else if(rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Sun Direct"))
            {

                holder.imgoperator.setImageResource(R.drawable.sundirect);
            }
            else if(rechargeHistoryData.get(position).getOperator().equalsIgnoreCase("Tata Sky"))
            {

                holder.imgoperator.setImageResource(R.drawable.ttsky);
            }
            else{

                holder.imgoperator.setImageResource(R.drawable.d2h);
            }


        }

        if(rechargeHistoryData.get(position).getStatus().equalsIgnoreCase("0"))
        {
            holder.txtStatus.setText("Recharge Failed");
            holder.txtStatus.setTextColor(Color.RED);
        }
      else  if(rechargeHistoryData.get(position).getStatus().equalsIgnoreCase("1"))
        {
            holder.txtStatus.setText("Recharge Success");
            holder.txtStatus.setTextColor(Color.parseColor("#0d5c4b"));
        }

        else if(rechargeHistoryData.get(position).getStatus().equalsIgnoreCase("3"))
        {
            holder.txtStatus.setText("Amount Refunded");
            holder.txtStatus.setTextColor(Color.parseColor("#0d5c4b"));

         holder.txtRefundTransactionid.setVisibility(View.VISIBLE);
            holder.txtRefundTransactionid.setText("ID : "+rechargeHistoryData.get(position).getRefundTransactionid());

        }
      else  if(rechargeHistoryData.get(position).getStatus().equalsIgnoreCase("2"))
        {
            holder.txtStatus.setText("Pending");
            holder.txtStatus.setTextColor(Color.BLUE);

        }

        if(rechargeHistoryData.get(position).getPaymentstatus().equalsIgnoreCase("4")) {

            holder.txtRechargetatus.setText("Payment Initiated");
            holder.txtRechargetatus.setTextColor(Color.BLUE);
        }
        else if(rechargeHistoryData.get(position).getPaymentstatus().equalsIgnoreCase("5")) {

            holder.txtRechargetatus.setText("Payment Success");
            holder.txtRechargetatus.setTextColor(Color.parseColor("#0d5c4b"));
        }

        else  if(rechargeHistoryData.get(position).getPaymentstatus().equalsIgnoreCase("6")) {

            holder.txtRechargetatus.setText("Payment Failed");
            holder.txtRechargetatus.setTextColor(Color.RED);
        }
        else{


                holder.txtRechargetatus.setText("Payment Failed");
                holder.txtRechargetatus.setTextColor(Color.RED);

        }







    }

    @Override
    public int getItemCount() {
        return (rechargeHistoryData.size()>=10)?10:rechargeHistoryData.size();
    }
}
