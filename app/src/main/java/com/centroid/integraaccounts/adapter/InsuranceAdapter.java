package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.InsuranceActivity;
import com.centroid.integraaccounts.views.InsuranceEntryActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class InsuranceAdapter extends RecyclerView.Adapter<InsuranceAdapter.InsuranceHolder> {


    Context context;
    List<CommonData>commonData;

    Resources resources;


    public InsuranceAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class InsuranceHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount,txtlictitle,txtph;
        Button btndelete,btnEdit;



        public InsuranceHolder(@NonNull View itemView) {
            super(itemView);

            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtlictitle=itemView.findViewById(R.id.txtlictitle);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);
            txtph=itemView.findViewById(R.id.txtph);



            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, InsuranceEntryActivity.class);
                    intent.putExtra("insurance",commonData.get(getAdapterPosition()));
                    context.startActivity(intent);


                }
            });







            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String languagedata = LocaleHelper.getPersistedData(context, "en");
                    Context cont= LocaleHelper.setLocale(context, languagedata);

                    resources=cont.getResources();


                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            int p=getAdapterPosition();


                            try{

                                CommonData data=commonData.get(p);


                                JSONObject jobj=new JSONObject(data.getData());

                                String taskarray=jobj.getString("Task");

                                if(!taskarray.equalsIgnoreCase(""))
                                {
                                    JSONArray jsonArray=new JSONArray(taskarray);

                                    for (int j=0;j<jsonArray.length();j++)
                                    {

                                        JSONObject jsonObj=jsonArray.getJSONObject(j);

                                        String Taskid=jsonObj.getString("Taskid");

                                        new DatabaseHelper(context).deleteData(Taskid,Utils.DBtables.TABLE_TASK);

                                       // taskid.add(Taskid);

                                    }



                                }






                            }catch (Exception e)
                            {


                            }








                            new DatabaseHelper(context).deleteData(commonData.get(p).getId(), Utils.DBtables.TABLE_INSURANCE);

                            commonData.remove(p);

                            notifyItemRemoved(p);

                            ((InsuranceActivity)context).getInsuranceData();


                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();











                }
            });
        }
    }


    @NonNull
    @Override
    public InsuranceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);

        return new InsuranceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InsuranceHolder holder, int position) {

        try{
            holder.setIsRecyclable(false);


            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context cont= LocaleHelper.setLocale(context, languagedata);

            resources=cont.getResources();



            holder.txtlictitle.setText(resources.getString(R.string.accountname));
            holder.btndelete.setText(resources.getString(R.string.delete));
            holder.btnEdit.setText(resources.getString(R.string.edit));
            holder.txtph.setText(resources.getString(R.string.amount));

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());


            String month= jsonObject.getString("insurance_no");


            List<CommonData> data=new DatabaseHelper(context).getAccountSettingsByID(month);

            if(data.size()>0)
            {

                JSONObject jso=new JSONObject(data.get(0).getData());


                holder.txtmonth.setText(jso.getString("Accountname"));

                // holder.txtvoucher.setText();

            }




            String amount= jsonObject.getString("amount");

            holder.txtamount.setText(amount+" "+context.getString(R.string.rs));
           // holder.txtmonth.setText(month);

        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
