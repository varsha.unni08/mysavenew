package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.MyWebLinksActivity;

import org.json.JSONObject;

import java.util.List;

public class MyWebLinkAdapter extends RecyclerView.Adapter<MyWebLinkAdapter.MyWebLinkHolder> {


    Context context;
    List<CommonData>commonData;

    public MyWebLinkAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    @NonNull
    @Override
    public MyWebLinkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_weblinks,parent,false);

        return new MyWebLinkHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyWebLinkHolder holder, int position) {

        try{

            JSONObject j=new JSONObject(commonData.get(position).getData());
            String WebLink=j.getString("WebLink");
            String Password=j.getString("Password");

            if(j.has("Username")) {

                String Username = j.getString("Username");

                if(!Username.isEmpty())
                {
                    holder.txtusername.setText(Username);
                    holder.layout_username.setVisibility(View.VISIBLE);
                }
                else{

                    holder.layout_username.setVisibility(View.GONE);
                }



            }
            if(j.has("Description")) {
                String Description = j.getString("Description");
                holder.txtDescription.setText(Description);
            }





            holder.txtweblink.setText(WebLink);

            if(!Password.isEmpty()) {

                int l = Password.length();
                String p = "";

                for (int i = 0; i < l; i++) {
                    p = p + "*";
                }

                holder.txtpassword.setText(p);
                holder.layout_password.setVisibility(View.VISIBLE);
            }
            else{

                holder.layout_password.setVisibility(View.GONE);

            }


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }

    public class MyWebLinkHolder extends RecyclerView.ViewHolder{

        TextView txtweblink,txtpassword,txtView,txtusername,txtDescription;
        Button btndelete,btnShare,btnedit;
        LinearLayout layout_username,layout_password;


        public MyWebLinkHolder(@NonNull View itemView) {
            super(itemView);
            txtpassword=itemView.findViewById(R.id.txtpassword);
            txtweblink=itemView.findViewById(R.id.txtweblink);
            txtView=itemView.findViewById(R.id.txtView);
            btndelete=itemView.findViewById(R.id.btndelete);
            btnShare=itemView.findViewById(R.id.btnShare);
            txtusername=itemView.findViewById(R.id.txtusername);
            txtDescription=itemView.findViewById(R.id.txtDescription);
            layout_password=itemView.findViewById(R.id.layout_password);
            layout_username=itemView.findViewById(R.id.layout_username);
            btnedit=itemView.findViewById(R.id.btnedit);

            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((MyWebLinksActivity) context).showDialogForEdit(commonData.get(getAdapterPosition()));

                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{

                        JSONObject j=new JSONObject(commonData.get(getAdapterPosition()).getData());
                        String WebLink=j.getString("WebLink");
                        String sharebody = WebLink;

                        // The value which we will sending through data via
                        // other applications is defined
                        // via the Intent.ACTION_SEND
                        Intent intentt = new Intent(Intent.ACTION_SEND);

                        // setting type of data shared as text
                        intentt.setType("text/plain");
                        intentt.putExtra(Intent.EXTRA_SUBJECT, "Open or Share link via");

                        // Adding the text to share using putExtra
                        intentt.putExtra(Intent.EXTRA_TEXT, sharebody);
                        context.startActivity(Intent.createChooser(intentt, "Share Via"));


                    }catch (Exception e)
                    {

                    }










                }
            });

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.showAlert(context, "Do you want to delete now ?", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            CommonData cm=commonData.get(getAdapterPosition());

                            new DatabaseHelper(context).deleteData(cm.getId(),Utils.DBtables.TABLE_WEBLINKS);

                            ((MyWebLinksActivity)context).getWebLinksData();

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });


                }
            });

            txtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        JSONObject j = new JSONObject(commonData.get(getAdapterPosition()).getData());
                        String WebLink = j.getString("WebLink");
                        String Password = j.getString("Password");


                        Utils.showAlertWithSingle(context, Password, new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });

                    }catch (Exception e)
                    {

                    }


                }
            });
        }
    }



}
