package com.centroid.integraaccounts.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.MVersion;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.BillPaymentActivity;
import com.centroid.integraaccounts.views.DTHPlansActivity;
import com.centroid.integraaccounts.views.DTHRechargeActivity;
import com.centroid.integraaccounts.views.DiaryActivity;
import com.centroid.integraaccounts.views.EmergencyLinkActivity;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.MyWebLinksActivity;
import com.centroid.integraaccounts.views.SettingsActivity;
import com.centroid.integraaccounts.views.TargetActivity;
import com.centroid.integraaccounts.views.TasksActivity;
import com.centroid.integraaccounts.views.VisitCardHistoryActivity;
import com.centroid.integraaccounts.views.rechargeViews.DTHRechargeDashboardActivity;
import com.centroid.integraaccounts.views.rechargeViews.MobileRechargeDashboardActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class UtilitiesAdapter extends RecyclerView.Adapter<UtilitiesAdapter.UtilitiesHolder> {

    Context context;

    public UtilitiesAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public UtilitiesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_mymoney,parent,false);

        return new UtilitiesHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UtilitiesHolder holder, int position) {
        holder.img.setVisibility(View.VISIBLE);
        holder.txt.setVisibility(View.VISIBLE);
        holder.img.setImageResource(Utils.utilitiesimg[position]);

        String languagedata= LocaleHelper.getPersistedData(context,"en");

        Context conte = LocaleHelper.setLocale(context, languagedata);
        Resources resources = conte.getResources();

        holder.txt.setText(Utils.myutilities[position]);
    }

    @Override
    public int getItemCount() {
        return Utils.myutilities.length;
    }

    public class UtilitiesHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView txt;

        public UtilitiesHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            img=itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition())
                    {
                        case 0 :
//                            Intent intent7=new Intent(context, MobileRechargeActivity.class);
//                            //intent.putExtra("data",1);
//                            context.startActivity(intent7);

                            final ProgressFragment progressFragment = new ProgressFragment();
                            progressFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "fkjfk");


                            Map<String, String> params = new HashMap<>();
                            params.put("timestamp", Utils.getTimestamp());


                            new RequestHandler(context, params, new ResponseHandler() {
                                @Override
                                public void onSuccess(String data) {

                                    progressFragment.dismiss();

                                    if (data != null) {

                                        MVersion mVersion = new GsonBuilder().create().fromJson(data, MVersion.class);


                                        if (mVersion != null) {

                                            if (mVersion.getStatus() == 1) {
                                                try {


                                                    String version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;

                                                    double liveVersion = Double.parseDouble(mVersion.getData().getAppVersion());


                                                    double currentversion = Double.parseDouble(version);

                                                    if (currentversion < liveVersion) {

                                                        Utils.showAlertWithSingle(context, "Please update the app", new DialogEventListener() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {
                                                                try {

                                                                    String link = mVersion.getData().getFilepath();


                                                                    Intent viewIntent =
                                                                            new Intent("android.intent.action.VIEW",
                                                                                    Uri.parse(link));
                                                                    context.startActivity(viewIntent);
                                                                } catch (Exception e) {

                                                                }
                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }
                                                        });
                                                    } else {

                                                        Intent intent7 = new Intent(context, MobileRechargeDashboardActivity.class);
                                                        //intent.putExtra("data",1);
                                                        context.startActivity(intent7);
                                                    }
                                                }catch (Exception e)
                                                {

                                                }

                                            }
                                            else{


                                            }


                                        }

                                    }

                                }

                                @Override
                                public void onFailure(String err) {
                                    progressFragment.dismiss();

                                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                                }
                            }, Utils.WebServiceMethodes.getMobileAppVersion + "?timestamp=" + Utils.getTimestamp(), Request.Method.GET).submitRequest();








                            break;



                        case 1 :


//                            Intent intent9=new Intent(context, DTHPlansActivity.class);
//                            //intent.putExtra("data",1);
//                            context.startActivity(intent9);

                            Intent intent9=new Intent(context, DTHRechargeDashboardActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent9);

                            break;

                        case 3:
                            Intent intent5=new Intent(context, MyWebLinksActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent5);

                            break;
                        case 4:


                            Intent intent6=new Intent(context, EmergencyLinkActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent6);

                            break;

                        case 2:


                            Intent intent4=new Intent(context, VisitCardHistoryActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent4);

                            break;




                    }

                }
            });
        }
    }



}
