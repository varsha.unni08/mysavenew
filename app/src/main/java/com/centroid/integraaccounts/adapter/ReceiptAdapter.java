package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;
import com.centroid.integraaccounts.views.AddReceiptActivity;
import com.centroid.integraaccounts.views.ReciptListActivity;

import org.json.JSONObject;

import java.util.List;

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ReceiptHolder> {


    Context context;
    List<Accounts>commonData;


    public ReceiptAdapter(Context context, List<Accounts> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class ReceiptHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtamount,txtaccount,txttype,txtaction;

        ImageView imgedit;

        public ReceiptHolder(@NonNull View itemView) {
            super(itemView);

            txtdate=itemView.findViewById(R.id.txtdate);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txttype=itemView.findViewById(R.id.txttype);
            txtaction=itemView.findViewById(R.id.txtaction);
            imgedit=itemView.findViewById(R.id.imgedit);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddReceiptActivity.class);
                    intent.putExtra("receipt",commonData.get(getAdapterPosition()));
                    intent.putExtra("forupdate",1);
                    context.startActivity(intent);


                }
            });
        }
    }


    @NonNull
    @Override
    public ReceiptHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_paymentvoucheradapter,parent,false);



        return new ReceiptHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceiptHolder holder, int position) {
        holder.setIsRecyclable(false);
        try {

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

          Resources  resources=conte.getResources();
            holder.txtaction.setText(resources.getString(R.string.edit)+"/"+resources.getString(R.string.delete));





                List<CommonData>commonDa=new DatabaseHelper(context).getDataByID(commonData.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if(commonDa.size()>0)
                {

                    JSONObject jso=new JSONObject(commonDa.get(0).getData());



                    holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }








            List<Accounts>accounts=new DatabaseHelper(context).getAccountsDataByEntryId(commonData.get(position).getACCOUNTS_id()+"");

            if(accounts.size()>0)
            {

                Accounts accounts1=accounts.get(0);

              //  String languagedata = LocaleHelper.getPersistedData(context, "en");
               // Context context1= LocaleHelper.setLocale(context, languagedata);

               // Resources resources=context1.getResources();
                String cashbank=accounts1.getACCOUNTS_cashbanktype();
                if (cashbank.equalsIgnoreCase(Utils.CashBanktype.bank+"")) {


                    List<CommonData>commonData=new DatabaseHelper(context).getDataByID(accounts1.getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonData.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonData.get(0).getData());



                        holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                    }




                    //holder.txttype.setText();

                }
                else if (cashbank.equalsIgnoreCase(Utils.CashBanktype.cash+"")) {
                    List<CommonData>commonData=new DatabaseHelper(context).getDataByID(accounts1.getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonData.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonData.get(0).getData());



                        holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                    }


                }


            }

//            else {
//
//
//
//
//
//            }












            String d[]=commonData.get(position).getACCOUNTS_date().split("-");


            holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
            holder.txtamount.setText(commonData.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));

//           if(commonData.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit+"")) {
//
//               holder.txttype.setText("Credit");
//               holder.txttype.setTextColor(Color.parseColor("#75c044"));
//           }
//           else {
//
//               holder.txttype.setText("Debit");
//               holder.txttype.setTextColor(Color.parseColor("#A9F00000"));
//           }
            //holder.txtvoucher.setText(accountname);
            //  holder.txtremarks.setText(commonData.get(position).getACCOUNTS_remarks());


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
