package com.centroid.integraaccounts.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.centroid.integraaccounts.fragments.HomeFragment;
import com.centroid.integraaccounts.fragments.MoreFragment;
import com.centroid.integraaccounts.fragments.ReportFragment;

import java.util.List;

public class HomeAdapter extends FragmentPagerAdapter {

    List<Fragment>fragments;

    public HomeAdapter(FragmentManager fm, List<Fragment>fragments) {
        super(fm);

        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {




        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
