package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.util.List;

public class BankCashAdapter extends RecyclerView.Adapter<BankCashAdapter.BankCashHolder> {


    Context context;
    List<Accounts> accounts;

    public BankCashAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    public class BankCashHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtaccount,txtamount,txttype,txtAccounthold;

        public BankCashHolder(@NonNull View itemView) {
            super(itemView);

            txttype=itemView.findViewById(R.id.txttype);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtAccounthold=itemView.findViewById(R.id.txtAccounthold);
        }
    }


    @NonNull
    @Override
    public BankCashHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_incexp,parent,false);



        return new BankCashHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BankCashHolder holder, int position) {

        holder.setIsRecyclable(false);

        holder.txtdate.setText(accounts.get(position).getACCOUNTS_date());

        holder.txtamount.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));




        if (accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

            holder.txttype.setText("Debit");


        } else {

            holder.txttype.setText("Credit");
        }





        if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("-1")) {




            // if (accbyEntry.size() > 0) {
            // IncExp.add(accbyEntry.get(0));

            if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0")) {


                List<Accounts> accbyEntry = new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id() + "");

                List<Accounts> accbyId = new DatabaseHelper(context).getAccountsDataBYid(accounts.get(position).getACCOUNTS_entryid() + "");

                if(accbyEntry.size()>0) {


                    List<CommonData> cm = new DatabaseHelper(context).getDataByID(accbyEntry.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                    if (cm.size() > 0) {

                        try {

                            JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                            holder.txtaccount.setText(jsonObject.getString("Accountname"));


                        } catch (Exception e) {

                        }

                    } else {

                        holder.txtaccount.setText("Cash");
                    }


                }

                // else {



                else   if(accbyId.size()>0) {


                    List<CommonData> cm = new DatabaseHelper(context).getDataByID(accbyId.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);


                    if (cm.size() > 0) {

                        try {

                            JSONObject jsonObject = new JSONObject(cm.get(0).getData());
                            holder.txtaccount.setText(jsonObject.getString("Accountname"));


                        } catch (Exception e) {

                        }

                    } else {

                        holder.txtaccount.setText("Cash");
                    }




                }




                //  }

                else {




                    holder.txtaccount.setText("Cash");
                }


            }

//            else {
//
//
//
//
//
//
//                holder.txtaccount.setText("Cash");
//            }


        }
        else {

            holder.txtaccount.setText("Opening balance");


//            if(accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit+""))
//            {
//
//                holder.txttype.setText("Debit");
//
//
//            }
//
//            else {
//
//                holder.txttype.setText("Credit");
//            }

        }


//        }
//        else {

//            List<CommonData>cm=new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(),Utils.DBtables.TABLE_ACCOUNTSETTINGS);
//
//
//            if(cm.size()>0)
//            {
//
//                try{
//
//                    JSONObject jsonObject=new JSONObject(cm.get(0).getData());
//                    holder.txtaccount.setText(jsonObject.getString("Accountname"));
//
//
//
//
//                }catch (Exception e)
//                {
//
//                }
//
//            }



        // }
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
