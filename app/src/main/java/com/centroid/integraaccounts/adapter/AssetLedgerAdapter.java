package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.AssetAccountsActivity;
import com.centroid.integraaccounts.views.IncExpListActivity;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

public class AssetLedgerAdapter extends RecyclerView.Adapter<AssetLedgerAdapter.AssetLedgerHolder> {


    Context context;
    List<LedgerAccount>ledgerAccounts;

    public AssetLedgerAdapter(Context context, List<LedgerAccount> ledgerAccounts) {
        this.context = context;
        this.ledgerAccounts = ledgerAccounts;
    }

    public class AssetLedgerHolder extends RecyclerView.ViewHolder{
        TextView txtAccount,txtDebit,txtcredit;


        public AssetLedgerHolder(@NonNull View itemView) {
            super(itemView);
            txtcredit=itemView.findViewById(R.id.txtcredit);
            txtAccount=itemView.findViewById(R.id.txtAccount);
            txtDebit=itemView.findViewById(R.id.txtDebit);


            txtcredit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, AssetAccountsActivity.class);
                    intent.putExtra("accountsetupid", ledgerAccounts.get(getAdapterPosition()).getAccountheadid());

                    intent.putExtra("closebalacebeforedate", ledgerAccounts.get(getAdapterPosition()).getClosingbalance());

                    context.startActivity(intent);

                }
            });
        }
    }


    @NonNull
    @Override
    public AssetLedgerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_assetledger,parent,false);


        return new AssetLedgerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetLedgerHolder holder, int position) {

        try{
            holder.setIsRecyclable(false);

            List<CommonData>commonData=new DatabaseHelper(context).getAccountSettingsByID(ledgerAccounts.get(position).getAccountheadid());

            if(commonData.size()>0)
            {
                CommonData cm=commonData.get(0);

                JSONObject jsonObject = new JSONObject(cm.getData());

                String Accounttype=jsonObject.getString("Accountname");

                holder.txtAccount.setText(Utils.getCapsSentences(context,Accounttype));

            }

            double d=Double.parseDouble(ledgerAccounts.get(position).getClosingbalance());

            if(d<0||String.valueOf(d).contains("-"))
            {
                d=d*-1;
            }

            DecimalFormat decimalFormatter = new DecimalFormat("###################################################");


            holder.txtDebit.setText(decimalFormatter.format(d)+"");


            //holder.txtAccount.setText();




        }catch (Exception e)
        {


        }



    }

    @Override
    public int getItemCount() {
        return ledgerAccounts.size();
    }
}
