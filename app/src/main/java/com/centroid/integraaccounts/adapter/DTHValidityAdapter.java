package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.DTHValidity;
import com.centroid.integraaccounts.views.DTHPlansActivity;
import com.centroid.integraaccounts.views.rechargeViews.dth.Price;

import java.util.List;

public class DTHValidityAdapter extends RecyclerView.Adapter<DTHValidityAdapter.DTHValidityHolder> {

    Context context;
    List<Price> dthValidities;


    public DTHValidityAdapter(Context context, List<Price>  dthValidities) {
        this.context = context;
        this.dthValidities = dthValidities;
    }

    public class DTHValidityHolder extends RecyclerView.ViewHolder{

        Button btn;
        TextView txt,txtDescription;
        CardView card;


        public DTHValidityHolder(@NonNull View itemView) {
            super(itemView);
            btn=itemView.findViewById(R.id.btn);
            txt=itemView.findViewById(R.id.txt);
            txtDescription=itemView.findViewById(R.id.txtDescription);
            card=itemView.findViewById(R.id.card);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((DTHPlansActivity)context).redirectToRecharge(dthValidities.get(getAdapterPosition()).getAmount()+"");
                }
            });
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((DTHPlansActivity)context).redirectToRecharge(dthValidities.get(getAdapterPosition()).getAmount()+"");
                }
            });
            txtDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((DTHPlansActivity)context).redirectToRecharge(dthValidities.get(getAdapterPosition()).getAmount()+"");
                }
            });

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((DTHPlansActivity)context).redirectToRecharge(dthValidities.get(getAdapterPosition()).getAmount()+"");
                }
            });
        }
    }

    @NonNull
    @Override
    public DTHValidityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dthvalidityplans,parent,false);
        return new DTHValidityHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DTHValidityHolder holder, int position) {
        holder.txt.setText("₹ "+dthValidities.get(position).getAmount());
        holder.txtDescription.setText(dthValidities.get(position).getValidity());

        holder.btn.setText(""+dthValidities.get(position).getAmount()+" ₹ For "+dthValidities.get(position).getValidity());

    }

    @Override
    public int getItemCount() {
        return dthValidities.size();
    }
}
