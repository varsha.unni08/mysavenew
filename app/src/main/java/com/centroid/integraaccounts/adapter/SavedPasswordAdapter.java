package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.SavedPasswordActivity;

import org.json.JSONObject;

import java.util.List;

public class SavedPasswordAdapter extends RecyclerView.Adapter<SavedPasswordAdapter.SavePasswordHolder> {


    Context context;
    List<CommonData>commonData;

    public SavedPasswordAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class SavePasswordHolder extends RecyclerView.ViewHolder{
        TextView txtTitle,txtUsername;
        Button btndelete;

        public SavePasswordHolder(@NonNull View itemView) {
            super(itemView);
            txtUsername=itemView.findViewById(R.id.txtUsername);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            btndelete=itemView.findViewById(R.id.btndelete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    ((SavedPasswordActivity)context).setPasswordData(commonData.get(getAdapterPosition()));
                }
            });

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String languagedata = LocaleHelper.getPersistedData(context, "en");
                    Context conte= LocaleHelper.setLocale(context, languagedata);

                   Resources resources=conte.getResources();

                    Utils.showAlert(context, "Do you want to delete the password information", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            new DatabaseHelper(context).deleteData(commonData.get(getAdapterPosition()).getId(), Utils.DBtables.TABLE_PASSWORD);
                            ((SavedPasswordActivity)context).getPasswords();
                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });




                }
            });
        }
    }


    @NonNull
    @Override
    public SavePasswordHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_savepass,parent,false);


        return new SavePasswordHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SavePasswordHolder holder, int position) {
        holder.setIsRecyclable(false);
        try{

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());

            holder.txtTitle.setText(jsonObject.getString("title"));
            holder.txtUsername.setText(jsonObject.getString("username"));



        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
