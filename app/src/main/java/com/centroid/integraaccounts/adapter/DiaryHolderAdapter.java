package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.DiarySubject;
import com.centroid.integraaccounts.data.domain.Diarydata;
import com.centroid.integraaccounts.views.DiaryActivity;
import com.centroid.integraaccounts.views.WriteDiaryActivity;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DiaryHolderAdapter extends RecyclerView.Adapter<DiaryHolderAdapter.DiaryHolder> {


    Context context;
    List<DiarySubject>diarySubjects;
    List<CommonData> diarydata;

    public DiaryHolderAdapter(Context context, List<DiarySubject> diarySubjects,List<CommonData> diarydata) {
        this.context = context;
        this.diarySubjects = diarySubjects;
        this.diarydata=diarydata;
    }

    public class DiaryHolder extends RecyclerView.ViewHolder{

        TextView txt;
        ImageView imgdropdown,imgExport;

        RecyclerView recycler;

        public DiaryHolder(@NonNull View itemView) {
            super(itemView);
            recycler=itemView.findViewById(R.id.recycler);
            imgdropdown=itemView.findViewById(R.id.imgdropdown);
            imgExport=itemView.findViewById(R.id.imgExport);
            txt=itemView.findViewById(R.id.txt);


            imgExport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        List<CommonData> diarydataList = new ArrayList<>();

                        for (CommonData cd : diarydata) {

                            JSONObject jsonObject = new JSONObject(cd.getData());
                            String subject = jsonObject.getString("subject");

                            if (subject.equalsIgnoreCase(diarySubjects.get(getAdapterPosition()).getData())) {
                                diarydataList.add(cd);

                            }


                        }


                        ( (DiaryActivity)context).exportFullSubject(diarydataList);



                    }catch (Exception e)
                    {

                    }


                }
            });





            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(diarySubjects.get(getAdapterPosition()).getSelected()==0)
                    {

                        for (DiarySubject diarySubject:diarySubjects)
                        {

                            diarySubject.setSelected(0);
                        }
                        diarySubjects.get(getAdapterPosition()).setSelected(1);



                    }
                    else {

                        diarySubjects.get(getAdapterPosition()).setSelected(0);

                    }

                    notifyDataSetChanged();

                }
            });


            imgdropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(diarySubjects.get(getAdapterPosition()).getSelected()==0)
                    {

                        for (DiarySubject diarySubject:diarySubjects)
                        {

                            diarySubject.setSelected(0);
                        }
                        diarySubjects.get(getAdapterPosition()).setSelected(1);



                    }
                    else {

                        diarySubjects.get(getAdapterPosition()).setSelected(0);

                    }

                    notifyDataSetChanged();
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(diarySubjects.get(getAdapterPosition()).getSelected()==0)
                    {

                        for (DiarySubject diarySubject:diarySubjects)
                        {

                            diarySubject.setSelected(0);
                        }
                        diarySubjects.get(getAdapterPosition()).setSelected(1);



                    }
                    else {

                        diarySubjects.get(getAdapterPosition()).setSelected(0);

                    }

                    notifyDataSetChanged();
                }
            });
        }
    }

    @NonNull
    @Override
    public DiaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diarysubholder,parent,false);


        return new DiaryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiaryHolder holder, int position) {

        try {
            holder.setIsRecyclable(false);

            if (diarySubjects.get(position).getSelected() == 0) {

                holder.recycler.setVisibility(View.GONE);
                holder.imgdropdown.setImageResource(R.drawable.ic_spinnerr);

            } else {
                holder.recycler.setVisibility(View.VISIBLE);
                holder.imgdropdown.setImageResource(R.drawable.ic_spinnerdropup);

                List<CommonData>diarydataList=new ArrayList<>();

                for (CommonData cd : diarydata) {

                    JSONObject jsonObject = new JSONObject(cd.getData());
                    String subject = jsonObject.getString("subject");

                    if(subject.equalsIgnoreCase(diarySubjects.get(position).getData()))
                    {
                        diarydataList.add(cd);

                    }


                }


                DiaryDataAdapter diaryDataAdapter = new DiaryDataAdapter(context, diarydataList);
                holder.recycler.setLayoutManager(new LinearLayoutManager(context));
                holder.recycler.setAdapter(diaryDataAdapter);

            }


            List<CommonData> commonData = new DatabaseHelper(context).getData(Utils.DBtables.DIARYSUBJECT_table);


            if (commonData.size() > 0) {

                //spinnerSubject.setAdapter(new DiarySubjectAdapter(WriteDiaryActivity.this,commonData));


                for (int i = 0; i < commonData.size(); i++) {

                    if (commonData.get(i).getId().equalsIgnoreCase(diarySubjects.get(position).getData())) {
                        // spinnerSubject.setSelection(i);
                        holder.txt.setText(commonData.get(position).getData());
                        break;
                    }


                }

            }

        }catch (Exception e)
        {

        }




    }

    @Override
    public int getItemCount() {
        return diarySubjects.size();
    }
}
