package com.centroid.integraaccounts.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.Diarydata;
import com.centroid.integraaccounts.views.DiaryActivity;
import com.centroid.integraaccounts.views.WriteDiaryActivity;

import org.json.JSONObject;

import java.util.List;

public class DiaryDataAdapter extends RecyclerView.Adapter<DiaryDataAdapter.DiaryHolder> {

    Context context;
    List<CommonData>diarydata;

    public DiaryDataAdapter(Context context, List<CommonData> diarydata) {
        this.context = context;
        this.diarydata = diarydata;
    }

    public class DiaryHolder extends RecyclerView.ViewHolder{

        TextView txtDate,txtTime,txtStatus;

        public DiaryHolder(@NonNull View itemView) {
            super(itemView);
            txtDate=itemView.findViewById(R.id.txtDate);
            txtTime=itemView.findViewById(R.id.txtTime);
            txtStatus=itemView.findViewById(R.id.txtStatus);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{



                        JSONObject jsonObject=new JSONObject(diarydata.get(getAdapterPosition()).getData());

                        String subject=jsonObject.getString("subject");
                        String date=jsonObject.getString("date");
                        final String content=jsonObject.getString("content");


                        String contentsubj="";

                        List<CommonData>commonData=new DatabaseHelper(context).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);

                        if(commonData.size()>0)
                        {

                            contentsubj=commonData.get(0).getData();
                        }


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    ((AppCompatActivity)context). getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels-10;
                    int width = displayMetrics.widthPixels-10;


                    final Dialog dialog=new Dialog(context);
                    dialog.setTitle("My diary");
                    dialog.setContentView(R.layout.layout_dialogdiary);

                    dialog.getWindow().setLayout(width, height);


                    TextView txtDate=dialog.findViewById(R.id.txtDate);

                    TextView txtSubject=dialog.findViewById(R.id.txtSubject);

                    TextView txtData=dialog.findViewById(R.id.txtData);

                        Button edit=dialog.findViewById(R.id.edit);
                        Button export=dialog.findViewById(R.id.export);

                    txtSubject.setText(contentsubj);
                    txtDate.setText(date);
                    txtData.setText(content);

                        edit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                dialog.dismiss();

                                Intent intent=new Intent(context, WriteDiaryActivity.class);
                                intent.putExtra("diary",diarydata.get(getAdapterPosition()));

                                context.startActivity(intent);


                            }
                        });

                        export.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                dialog.dismiss();


                               //
                                //
                                // ((DiaryActivity)context).exportPdf(diarydata.get(getAdapterPosition()));


                            }
                        });



                    dialog.show();


                    }catch (Exception e)
                    {

                    }


                }
            });
        }
    }

    @NonNull
    @Override
    public DiaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diarydata,parent,false);


        return new DiaryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DiaryHolder holder, int position) {

        try{

            holder.setIsRecyclable(false);

            JSONObject jsonObject=new JSONObject(diarydata.get(position).getData());

            String subject=jsonObject.getString("subject");

            String contentsubj="";

            List<CommonData>commonData=new DatabaseHelper(context).getDataByID(subject, Utils.DBtables.DIARYSUBJECT_table);

            if(commonData.size()>0)
            {

                contentsubj=commonData.get(0).getData();
            }





            String date=jsonObject.getString("date");
            String content=jsonObject.getString("content");



            holder.txtDate.setText(date);
            holder.txtTime.setText(contentsubj);
            holder.txtStatus.setText(content);





        }catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return diarydata.size();
    }
}
