package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.State;

import org.json.JSONObject;

import java.util.List;

public class StateListAdapter extends BaseAdapter {

    Context context;
    List<State>states;


    public StateListAdapter(Context context, List<State> states) {
        this.context = context;
        this.states = states;
    }

    @Override
    public int getCount() {
        return states.size();
    }

    @Override
    public Object getItem(int i) {
        return states.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_statelist,viewGroup,false);

        TextView txt=v.findViewById(R.id.txt);


        try{

        //    JSONObject jsonObject=new JSONObject(commonData.get(i).getData());

            txt.setText(states.get(i).getStateName());



        }catch (Exception e)
        {


        }




        return v;
    }

    @Override
    public View getDropDownView(int i, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_loans,parent,false);

        TextView txt=v.findViewById(R.id.txt);


        try{

            //    JSONObject jsonObject=new JSONObject(commonData.get(i).getData());

            txt.setText(states.get(i).getStateName());



        }catch (Exception e)
        {


        }




        return v;
    }
}
