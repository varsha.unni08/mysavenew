package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.FeedbackMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FeedbackMessageAdapter extends RecyclerView.Adapter<FeedbackMessageAdapter.FeedbackMessageHolder> {

    Context context;
    List<FeedbackMessage>feedbackMessages;


    public FeedbackMessageAdapter(Context context, List<FeedbackMessage> feedbackMessages) {
        this.context = context;
        this.feedbackMessages = feedbackMessages;
    }


    public class FeedbackMessageHolder extends RecyclerView.ViewHolder
    {
        TextView txtMessage,txtReplyMessage;

        public FeedbackMessageHolder(@NonNull View itemView) {
            super(itemView);
            txtMessage=itemView.findViewById(R.id.txtMessage);
            txtReplyMessage=itemView.findViewById(R.id.txtReplyMessage);
        }
    }

    @NonNull
    @Override
    public FeedbackMessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {




                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feedbackmsg,parent,false);
        return new FeedbackMessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedbackMessageHolder holder, int position) {

        try {
            holder.setIsRecyclable(false);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fdate = sdf.parse(feedbackMessages.get(position).getSendDate());
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");

            String feedbackDate=sdf1.format(fdate);



            holder.txtMessage.setText(feedbackDate + "\n\n" + feedbackMessages.get(position).getFeedbackMsg());

            if (feedbackMessages.get(position).getReplyMsg() != null) {
                holder.txtReplyMessage.setVisibility(View.VISIBLE);

                Date rdate = sdf.parse(feedbackMessages.get(position).getReplyDate().toString());

                String repliedDate=sdf1.format(rdate);

                holder.txtReplyMessage.setText("Replied on "+repliedDate + "\n\n" + feedbackMessages.get(position).getReplyMsg());
            }
            else {

                holder.txtReplyMessage.setVisibility(View.GONE);
            }

        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return feedbackMessages.size();
    }
}
