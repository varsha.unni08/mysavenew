package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.DTHPlansFromServer;
import com.centroid.integraaccounts.views.rechargeViews.dth.Pack;

import java.util.ArrayList;
import java.util.List;

public class DTHPlansAdapter extends RecyclerView.Adapter<DTHPlansAdapter.DTHPlanHolder> {

    Context context;
    List<Pack> dthPlansFromServers=new ArrayList<>();

    public DTHPlansAdapter(Context context, List<Pack> packs) {
        this.context = context;
        this.dthPlansFromServers = packs;
    }

    public class DTHPlanHolder extends RecyclerView.ViewHolder{

        TextView txt,txtDescription;
        RecyclerView rec_plans;

        public DTHPlanHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            txtDescription=itemView.findViewById(R.id.txtDescription);
            rec_plans=itemView.findViewById(R.id.rec_plans);
        }
    }


    @NonNull
    @Override
    public DTHPlanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dthplans,parent,false);


        return new DTHPlanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DTHPlanHolder holder, int position) {

        holder.txt.setText(dthPlansFromServers.get(position).getName());

        holder.txtDescription.setText("");
       holder. rec_plans.setLayoutManager(new LinearLayoutManager(context,RecyclerView.HORIZONTAL,false));
       holder.rec_plans.setAdapter(new DTHValidityAdapter(context,dthPlansFromServers.get(position).getPrices()));

    }

    @Override
    public int getItemCount() {
        return dthPlansFromServers.size();
    }
}
