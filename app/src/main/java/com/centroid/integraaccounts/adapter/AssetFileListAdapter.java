package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.AddAssetActivity;
import com.centroid.integraaccounts.views.DocumentManagerActivity;

import org.json.JSONObject;

import java.util.List;

public class AssetFileListAdapter  extends RecyclerView.Adapter<AssetFileListAdapter.AssetFileHolder> {

    Context context;
    List<String>fileList;

    public AssetFileListAdapter(Context context, List<String> fileList) {
        this.context = context;
        this.fileList = fileList;
    }

    public class AssetFileHolder extends RecyclerView.ViewHolder{

        TextView txtAccount;
        Button btndelete,btndownload;

        public AssetFileHolder(@NonNull View itemView) {
            super(itemView);
            btndelete=itemView.findViewById(R.id.btndelete);
            txtAccount=itemView.findViewById(R.id.txtAccount);
            btndownload=itemView.findViewById(R.id.btndownload);

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

            Resources resources=conte.getResources();
            btndownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        JSONObject js=new JSONObject(fileList.get(getAdapterPosition()));

                     String fileid=   js.getString("fileid");
                     String filename=js.getString("filename");




                        Utils.showAlert(context, resources.getString(R.string.doyouwantdownload), new DialogEventListener() {
                            @Override
                            public void onPositiveButtonClicked() {

                                try{

                                    ((AddAssetActivity) context).downloadFile(fileid,filename);
                                }catch (Exception e)
                                {

                                }




                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        });
                    }catch (Exception e)
                    {

                    }
                }
            });

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ( (AddAssetActivity)context).deleteAndRefreshFile(getAdapterPosition());
                }
            });
        }
    }

    @NonNull
    @Override
    public AssetFileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_assetfilelist,parent,false);

        return new AssetFileHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetFileHolder holder, int position) {

        try{

            JSONObject js=new JSONObject(fileList.get(position));


            holder.txtAccount.setText(js.getString("name"));

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

            Resources resources=conte.getResources();

            holder.btndelete.setText(resources.getString(R.string.delete));
            holder.btndownload.setText(resources.getString(R.string.download));




        }catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }
}
