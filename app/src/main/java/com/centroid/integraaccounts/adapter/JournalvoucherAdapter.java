package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddReceiptActivity;
import com.centroid.integraaccounts.views.JournalVoucherActivity;

import org.json.JSONObject;

import java.util.List;

public class JournalvoucherAdapter extends RecyclerView.Adapter<JournalvoucherAdapter.JournalHolder> {


    Context context;
    List<Accounts>accounts;

    public JournalvoucherAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    public class JournalHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtamount,txtaccount,txttype,txtaction;


        public JournalHolder(@NonNull View itemView) {
            super(itemView);

            txtdate=itemView.findViewById(R.id.txtdate);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txttype=itemView.findViewById(R.id.txttype);
            txtaction=itemView.findViewById(R.id.txtaction);
           // imgedit=itemView.findViewById(R.id.imgedit);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, JournalVoucherActivity.class);
                    intent.putExtra("Account",accounts.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });
        }
    }


    @NonNull
    @Override
    public JournalHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_paymentvoucheradapter,parent,false);



        return new JournalHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JournalHolder holder, int position) {

        try {
            holder.setIsRecyclable(false);
            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte = LocaleHelper.setLocale(context, languagedata);

            Resources resources = conte.getResources();
            holder.txtaction.setText(resources.getString(R.string.edit) + "/" + resources.getString(R.string.delete));

            if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0"))
            {

                List<CommonData>commonDa=new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if(commonDa.size()>0)
                {

                    JSONObject jso=new JSONObject(commonDa.get(0).getData());



                    holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }



            }









            List<Accounts>account=new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id()+"");

            if(account.size()>0)
            {

                String id=account.get(0).getACCOUNTS_setupid();

                List<CommonData>commonDa=new DatabaseHelper(context).getDataByID(id+"", Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if(commonDa.size()>0)
                {

                    JSONObject jso=new JSONObject(commonDa.get(0).getData());



                    holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }





            }



            String d[]=accounts.get(position).getACCOUNTS_date().split("-");


            holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
            holder.txtamount.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));



        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {



        return accounts.size();
    }
}
