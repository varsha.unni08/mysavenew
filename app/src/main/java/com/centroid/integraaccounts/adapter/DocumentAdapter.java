package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.DocumentManagerActivity;
import com.centroid.integraaccounts.views.MainActivity;

import org.json.JSONObject;

import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentHolder> {

    Context context;
    List<CommonData>commonData;

    public DocumentAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }


    public class DocumentHolder extends RecyclerView.ViewHolder{

        TextView txtTitle,txtUsername,txtTitlehead,txtusernamehead;
        Button btndownload,btnupdate;

        public DocumentHolder(@NonNull View itemView) {
            super(itemView);
            txtUsername=itemView.findViewById(R.id.txtUsername);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            btndownload=itemView.findViewById(R.id.btndownload);
            btnupdate=itemView.findViewById(R.id.btnupdate);
            txtTitlehead=itemView.findViewById(R.id.txtTitlehead);
            txtusernamehead=itemView.findViewById(R.id.txtusernamehead);

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

            Resources   resources=conte.getResources();

            btnupdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.showAlert(context, resources.getString(R.string.deleteconfirm), new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            try{

                                JSONObject jsonObject=new JSONObject(commonData.get(getAdapterPosition()).getData());

                                ;


                                ((DocumentManagerActivity)context).updateDocuments(commonData.get(getAdapterPosition()).getId(),jsonObject.getString("fileid"),jsonObject.getString("filename"),commonData.get(getAdapterPosition()));

                            }catch (Exception e)
                            {

                            }




                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });
                }
            });




            btndownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.showAlert(context,Utils.getCapsSentences(context, resources.getString(R.string.doyouwantdownload)), new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {

                            try{

                                JSONObject jsonObject=new JSONObject(commonData.get(getAdapterPosition()).getData());

                               ;


                                ((DocumentManagerActivity)context).downloadDocuments(jsonObject.getString("fileid"),jsonObject.getString("filename"));

                            }catch (Exception e)
                            {

                            }




                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }
            });

        }
    }

    @NonNull
    @Override
    public DocumentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_documentadapter,parent,false);

        return new DocumentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentHolder holder, int position) {
        try{
            holder.setIsRecyclable(false);
            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte = LocaleHelper.setLocale(context, languagedata);

          Resources resources = conte.getResources();
          holder.txtTitlehead.setText(resources.getString(R.string.name));
            holder.txtusernamehead.setText(Utils.getCapsSentences(context,resources.getString(R.string.filename)));
            holder.btndownload.setText(resources.getString(R.string.download));
            holder.btnupdate.setText(resources.getString(R.string.delete));

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());

            holder.txtTitle.setText(jsonObject.getString("name"));
            holder.txtUsername.setText(jsonObject.getString("filename"));



        }catch (Exception e)
        {

        }
    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
