package com.centroid.integraaccounts.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.RemindData;
import com.centroid.integraaccounts.views.AddAssetActivity;

import java.util.Calendar;
import java.util.List;

public class ReminddateSelectAdapter extends RecyclerView.Adapter<ReminddateSelectAdapter.RemindSelectHolder> {


    Context context;
    List<RemindData>reminddate;

    public ReminddateSelectAdapter(Context context, List<RemindData> reminddate) {
        this.context = context;
        this.reminddate = reminddate;
    }

    public class RemindSelectHolder extends RecyclerView.ViewHolder{

        TextView txtRemindedDate;

        ImageView imgRemindDate,imgClose;

        EditText edtDescription;


        public RemindSelectHolder(@NonNull View itemView) {
            super(itemView);

            edtDescription=itemView.findViewById(R.id.edtDescription);

            imgRemindDate=itemView.findViewById(R.id.imgRemindDate);
            txtRemindedDate=itemView.findViewById(R.id.txtRemindedDate);
            imgClose=itemView.findViewById(R.id.imgClose);

            edtDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                   reminddate.get(getAdapterPosition()).setDescription(charSequence.toString());
//
//                    notifyDataSetChanged();

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((AddAssetActivity)context).removeDate(getAdapterPosition());
                }
            });

            txtRemindedDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDatePicker(RemindSelectHolder.this,getAdapterPosition());
                }
            });

            imgRemindDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker(RemindSelectHolder.this,getAdapterPosition());
                }
            });


        }
    }


    @NonNull
    @Override
    public RemindSelectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_remindholder,parent,false);



        return new RemindSelectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RemindSelectHolder holder, int position) {
        holder.setIsRecyclable(false);
        holder.txtRemindedDate.setText(reminddate.get(position).getDate());
        holder.edtDescription.setText(reminddate.get(position).getDescription());

        String languagedata = LocaleHelper.getPersistedData(context, "en");
        Context conte= LocaleHelper.setLocale(context, languagedata);

        final Resources resources=conte.getResources();

        holder.edtDescription.setHint(resources.getString(R.string.description));

    }

    @Override
    public int getItemCount() {
        return reminddate.size();
    }


    public List<RemindData>getRemindDataList()
    {

        return reminddate;
    }



    public void showDatePicker(RemindSelectHolder remindSelectHolder,int position)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int m=i1+1;
//                if(code==0) {
//
//                    purchasedate = i2 + "-" + m + "-" + i;
//                    txtdatepurchase.setText(i2+"-"+m+"-"+i);
//                }
//                else {

                   String remind_date = i2 + "-" + m + "-" + i;
                    remindSelectHolder.txtRemindedDate.setText(remind_date);

                    reminddate.get(position).setDate(remind_date);


                    // txtReminddate.setText(i2+"-"+m+"-"+i);




               // }




            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }
}
