package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.AddedAmount;

import java.util.List;

public class AddedAmountAdapter extends RecyclerView.Adapter<AddedAmountAdapter.AddedAmountHolder> {

    Context context;
    List<AddedAmount>addedAmounts;




    public AddedAmountAdapter(Context context, List<AddedAmount> addedAmounts) {
        this.context = context;
        this.addedAmounts = addedAmounts;
    }

    public class AddedAmountHolder extends RecyclerView.ViewHolder{

        TextView txtDate,txtAmount;


        public AddedAmountHolder(@NonNull View itemView) {
            super(itemView);

            txtDate=itemView.findViewById(R.id.txtDate);
            txtAmount=itemView.findViewById(R.id.txtAmount);
        }
    }


    @NonNull
    @Override
    public AddedAmountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View v= LayoutInflater.from(context).inflate(R.layout.layout_addedamount,parent,false);

        return new AddedAmountHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AddedAmountHolder holder, int position) {

        AddedAmount adm=addedAmounts.get(position);

        holder.txtAmount.setText(adm.getAmount());
        holder.txtDate.setText(adm.getDate());

    }

    @Override
    public int getItemCount() {
        return addedAmounts.size();
    }
}
