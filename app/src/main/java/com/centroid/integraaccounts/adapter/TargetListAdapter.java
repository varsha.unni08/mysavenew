package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.centroid.integraaccounts.views.TargetDetailsActivity;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

public class TargetListAdapter extends RecyclerView.Adapter<TargetListAdapter.TargetListHolder> {


    Context context;
    List<CommonData>commonData;


    public TargetListAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class TargetListHolder extends RecyclerView.ViewHolder{

        TextView txtaccname,txtamount,txtpercent,txtTarget,txtName,txtClosingbalance,txtInvestment,txtsavedAmount;

        ProgressBar progressbar;

        ImageView imgarrow;
        LinearLayout layout_investment,layout_closingbalance,layout_savedamount,layout_addedamount,layout_goal;


        public TargetListHolder(@NonNull View itemView) {
            super(itemView);

            progressbar=itemView.findViewById(R.id.progressbar);
            txtaccname=itemView.findViewById(R.id.txtaccname);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtpercent=itemView.findViewById(R.id.txtpercent);
            txtTarget=itemView.findViewById(R.id.txtTarget);
            txtName=itemView.findViewById(R.id.txtName);

            imgarrow=itemView.findViewById(R.id.imgarrow);


            layout_investment=itemView.findViewById(R.id.layout_investment);
            layout_closingbalance=itemView.findViewById(R.id.layout_closingbalance);
            layout_savedamount=itemView.findViewById(R.id.layout_savedamount);
            layout_addedamount=itemView.findViewById(R.id.layout_addedamount);
            layout_goal=itemView.findViewById(R.id.layout_goal);

            txtClosingbalance=itemView.findViewById(R.id.txtClosingbalance);
            txtInvestment=itemView.findViewById(R.id.txtInvestment);
            txtsavedAmount=itemView.findViewById(R.id.txtsavedAmount);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(context, TargetDetailsActivity.class);
                    i.putExtra("data",commonData.get(getAdapterPosition()));
                    context.startActivity(i);
                }
            });

            imgarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(context, TargetDetailsActivity.class);
                    i.putExtra("data",commonData.get(getAdapterPosition()));
                    context.startActivity(i);
                }
            });
        }
    }


    @NonNull
    @Override
    public TargetListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



                View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_targetlistadapter,parent,false);


        return new TargetListHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TargetListHolder holder, int position) {

        try{

            double closingbalance=0;

        CommonData cm=commonData.get(position);



        JSONObject js = new JSONObject(cm.getData());

        String targetamount=js.getString("targetamount");
            String savedamount=js.getString("savedamount");
            double dt=Double.parseDouble(savedamount);
            String target_date=js.getString("target_date");
            String note=js.getString("note");

            String target_categoryid=js.getString("target_categoryid");

            if(js.has("targetname"))
            {

                String targetname=js.getString("targetname");
                holder.txtaccname.setText(targetname+" ");
            }

            if(   js.has("goalreached"))
            {

                holder.layout_goal.setVisibility(View.VISIBLE);

            }





        TargetCategory tg=new DatabaseHelper(context).getTargetCategoryById(target_categoryid);

        if(tg!=null)
        {

            holder.txtName.append(tg.getTask_category());
            holder.txtamount.setText(savedamount);
        }

        if(!savedamount.equalsIgnoreCase(""))
        {

            double sa=Double.parseDouble(savedamount);
            double ta=Double.parseDouble(targetamount);

            double p=sa/ta;

            double percentage=p*100;
            DecimalFormat df = new DecimalFormat("0.00");

            String a=  df.format(percentage);


            holder.txtpercent.setText(a+" %");

            int per=(int)percentage;

            holder.progressbar.setProgress(per);

        }
        else{


            holder.txtpercent.setText("0 %");
        }



            List<CommonData>cmd=new DatabaseHelper(context).getData(Utils.DBtables.TABLE_ADDEDAMOUNT_MILESTONE);

        double d=0;

            for(CommonData cm1:cmd)
            {

                JSONObject j=new JSONObject(cm1.getData());
                String am=j.getString("savedamount");
                String tid=j.getString("targetid");

                if(tid.equalsIgnoreCase(cm.getId())) {

                  double  d1 = Double.parseDouble(am);

                    d=d+d1;

                }



            }

            double savedamount_total=d+dt;






            if(js.has("investment_id")) {

                holder.layout_addedamount.setVisibility(View.VISIBLE);

                holder.layout_closingbalance.setVisibility(View.VISIBLE);



                holder.layout_investment.setVisibility(View.VISIBLE);

                String investment_id = js.getString("investment_id");



                    List<CommonData> data=new DatabaseHelper(context).getAccountSettingsByID(investment_id);

                    if(data.size()>0)
                    {

                        JSONObject jso=new JSONObject(data.get(0).getData());
                        String amount= jso.getString("Amount");

                        holder.txtInvestment.setText(jso.getString("Accountname"));

                        closingbalance=   getClosingBalance(Double.parseDouble(amount),data.get(0).getId(),"","");


                        holder.txtClosingbalance.setText(closingbalance+"");

                        // holder.txtvoucher.setText();

                    }

            //    }

            }
            else{

                closingbalance=0;
                holder.layout_closingbalance.setVisibility(View.GONE);

                holder.layout_addedamount.setVisibility(View.GONE);



                holder.layout_investment.setVisibility(View.GONE);
            }


            holder.txtamount.setText(savedamount_total+"");
            double t=savedamount_total+closingbalance;


            holder.txtsavedAmount.setText(t+"");
            holder.txtTarget.setText(targetamount+"");


          //  double sa=Double.parseDouble(savedamount);
            double ta=Double.parseDouble(targetamount);

            double p=t/ta;

            double percentage=p*100;
            DecimalFormat df = new DecimalFormat("0.00");

            String a=  df.format(percentage);


            holder.txtpercent.setText(a+" %");

            int per=(int)percentage;

            holder.progressbar.setProgress(per);


        }catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }


    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = new DatabaseHelper(context).getAllAccounts();

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }
}
