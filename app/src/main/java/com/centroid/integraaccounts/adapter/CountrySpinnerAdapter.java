package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.CountryData;

import java.util.List;

public class CountrySpinnerAdapter extends BaseAdapter {


    Context context;
    List<CountryData>countryData;

    public CountrySpinnerAdapter(Context context, List<CountryData> countryData) {
        this.context = context;
        this.countryData = countryData;
    }

    @Override
    public int getCount() {
        return countryData.size();
    }

    @Override
    public Object getItem(int i) {
        return countryData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_statelist,viewGroup,false);

        TextView txt=v.findViewById(R.id.txt);


        try{

            //    JSONObject jsonObject=new JSONObject(commonData.get(i).getData());

            txt.setText(countryData.get(i).getCountryName());



        }catch (Exception e)
        {


        }

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_loans,parent,false);

        TextView txt=v.findViewById(R.id.txt);


        try{

            //    JSONObject jsonObject=new JSONObject(commonData.get(i).getData());

            txt.setText(countryData.get(position).getCountryName());



        }catch (Exception e)
        {


        }




        return v;
    }
}
