package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.rechargedomain.RechargeRecords;
import com.centroid.integraaccounts.views.MobileRechargeActivity;
import com.centroid.integraaccounts.views.rechargeViews.domain.mobile.Plan;

import java.util.List;

public class RechargeRecordAdapter extends RecyclerView.Adapter<RechargeRecordAdapter.RechargeRecordHolder> {

    Context context;

    List<Plan>rechargeRecords;

    public RechargeRecordAdapter(Context context, List<Plan>rechargeRecords) {
        this.context = context;
        this.rechargeRecords = rechargeRecords;
    }

    public class RechargeRecordHolder extends RecyclerView.ViewHolder{

        TextView txtRs,txtprofile,txtValidity;
        Button btnSave;


        public RechargeRecordHolder(@NonNull View itemView) {
            super(itemView);
            txtRs=itemView.findViewById(R.id.txtRs);
            txtprofile=itemView.findViewById(R.id.txtprofile);
            btnSave=itemView.findViewById(R.id.btnSave);
            txtValidity=itemView.findViewById(R.id.txtValidity);

            txtValidity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ((MobileRechargeActivity)context).getProfile(rechargeRecords.get(getAdapterPosition()).getAmount()+"");




                }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ((MobileRechargeActivity)context).getProfile(rechargeRecords.get(getAdapterPosition()).getAmount()+"");




                }
            });

            txtRs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((MobileRechargeActivity)context).getProfile(rechargeRecords.get(getAdapterPosition()).getAmount()+"");



                  //  Utils.showAlertWithSingle(context,rechargeRecords.get(getAdapterPosition()).getDesc(),null);




                }
            });

            txtprofile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


              //      Utils.showAlertWithSingle(context,rechargeRecords.get(getAdapterPosition()).getDesc(),null);

                    ((MobileRechargeActivity)context).getProfile(rechargeRecords.get(getAdapterPosition()).getAmount()+"");



                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


             //       Utils.showAlertWithSingle(context,rechargeRecords.get(getAdapterPosition()).getDesc(),null);

                    ((MobileRechargeActivity)context).getProfile(rechargeRecords.get(getAdapterPosition()).getAmount()+"");



                }
            });
        }
    }

    @NonNull
    @Override
    public RechargeRecordHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_rrecords,parent,false);

        return new RechargeRecordHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RechargeRecordHolder holder, int position) {
        holder.txtValidity.setText(""+rechargeRecords.get(position).getValidity());

        holder.txtprofile.setText(rechargeRecords.get(position).getBenefit());
        holder.txtRs.setText("₹ "+rechargeRecords.get(position).getAmount());

    }

    @Override
    public int getItemCount() {
        return rechargeRecords.size();
    }
}
