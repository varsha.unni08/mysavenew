package com.centroid.integraaccounts.adapter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.EmergencyLinkActivity;
import com.centroid.integraaccounts.views.MainActivity;
import com.centroid.integraaccounts.views.MileStoneActivity;
import com.centroid.integraaccounts.views.NewTargetActivity;
import com.centroid.integraaccounts.views.SettingsActivity;
import com.google.api.client.util.Data;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EmergencyLinksAdapter extends RecyclerView.Adapter<EmergencyLinksAdapter.EmergencyLinksHolder> {



    Context context;
    List<CommonData>commonData=new ArrayList<>();

    public EmergencyLinksAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    @NonNull
    @Override
    public EmergencyLinksHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_emergencyweblink,parent,false);


        return new EmergencyLinksHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EmergencyLinksHolder holder, int position) {


        try {

            CommonData cmd=commonData.get(position);


            JSONObject jsonObject = new JSONObject(cmd.getData());


         String emergencydata=   jsonObject.getString("emergencydata");
         String emergencyno=   jsonObject.getString("emergencyno");
            String isAllowDelete=   jsonObject.getString("isAllowDelete");
         holder.txtweblink.setText(emergencydata);
            holder.txtpassword.setText(emergencyno);

            if(isAllowDelete.compareTo("0")==0)
            {

                holder.btndelete.setVisibility(View.GONE);
                holder.btnedit.setVisibility(View.GONE);

            }
            else{

                holder.btndelete.setVisibility(View.VISIBLE);
                holder.btnedit.setVisibility(View.VISIBLE);

            }





        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }


    public class EmergencyLinksHolder extends RecyclerView.ViewHolder{
        TextView txtweblink,txtpassword;
        Button btndelete,btnContact,btnedit;

        LinearLayout layout_action;

        ImageView imgcall,imgwhatsapp,imgsms;

        public EmergencyLinksHolder(@NonNull View itemView) {
            super(itemView);

            txtpassword=itemView.findViewById(R.id.txtpassword);
            txtweblink=itemView.findViewById(R.id.txtweblink);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnContact=itemView.findViewById(R.id.btnContact);
            btnedit=itemView.findViewById(R.id.btnedit);

            layout_action=itemView.findViewById(R.id.layout_action);

            imgcall=itemView.findViewById(R.id.imgcall);
            imgwhatsapp=itemView.findViewById(R.id.imgwhatsapp);
            imgsms=itemView.findViewById(R.id.imgsms);

            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ((EmergencyLinkActivity)context).showEditDialog(commonData.get(getAdapterPosition()));



                }
            });









            imgsms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{
                        CommonData cmd=commonData.get(getAdapterPosition());


                        JSONObject jsonObject = new JSONObject(cmd.getData());

                        String emergencyno=   jsonObject.getString("emergencyno");

                        Intent smsMsgAppVar = new Intent(Intent.ACTION_VIEW);
                        smsMsgAppVar.setData(Uri.parse("sms:" +  emergencyno));
                        smsMsgAppVar.putExtra("sms_body", "hi");
                        context.startActivity(smsMsgAppVar);



                    } catch (Exception e)
                    {

                    }





                }
            });

            imgcall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{

                        CommonData cmd=commonData.get(getAdapterPosition());


                        JSONObject jsonObject = new JSONObject(cmd.getData());

                        String emergencyno=   jsonObject.getString("emergencyno");
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+emergencyno));


                        context.startActivity(intent);


                    }
                    catch (Exception e)
                    {

                    }

                }
            });



            imgwhatsapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PackageManager packageManager = context.getPackageManager();
                    Intent i = new Intent(Intent.ACTION_VIEW);

                    try {

                        CommonData cmd=commonData.get(getAdapterPosition());


                        JSONObject jsonObject = new JSONObject(cmd.getData());

                        String emergencyno=   jsonObject.getString("emergencyno");


                        String url = "https://wa.me/"+emergencyno  ;

                        i.setData(Uri.parse(url));

                        context.startActivity(i);
                        // }
                    } catch (Exception e){
                        e.printStackTrace();


                        Utils.showAlertWithSingle(context,"Cannot redirect to WhatsApp",null);


                    }


                }
            });




            btnContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    try {

                        CommonData cmd=commonData.get(getAdapterPosition());


                        JSONObject jsonObject = new JSONObject(cmd.getData());


                        String emergencydata=   jsonObject.getString("emergencydata");
                        String emergencyno=   jsonObject.getString("emergencyno");
                        String isAllowDelete=   jsonObject.getString("isAllowDelete");


                        if(isAllowDelete.compareTo("0")==0)
                        {

                            Intent intent = new Intent(Intent.ACTION_DIAL);
                           intent.setData(Uri.parse("tel:"+emergencyno));


                            context.startActivity(intent);


                        }
                        else{



                            final String items[]={"WhatsApp","Phone"};


                            new androidx.appcompat.app.AlertDialog.Builder(context)
                                    .setTitle("Contact Via ")
                                    .setSingleChoiceItems(items, 0, null)
                                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            dialog.dismiss();

                                            int selectedPosition = ((androidx.appcompat.app.AlertDialog)dialog).getListView().getCheckedItemPosition();
                                            String item=items[selectedPosition];

                                            if(selectedPosition==0)
                                            {


                                                PackageManager packageManager = context.getPackageManager();
                                                Intent i = new Intent(Intent.ACTION_VIEW);

                                                try {

//                                                    String url = "https://wa.me/919747497967"  ;

                                                    String url = "https://wa.me/"+emergencyno  ;


//                                                    i.setPackage("com.whatsapp");
                                                    i.setData(Uri.parse(url));
//                                                    if (i.resolveActivity(packageManager) != null) {
                                                        context.startActivity(i);
                                                   // }
                                                } catch (Exception e){
                                                    e.printStackTrace();


                                                    Utils.showAlertWithSingle(context,"Cannot redirect to WhatsApp",null);


                                                }



                                            }
                                            else{


                                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                                intent.setData(Uri.parse("tel:"+emergencyno));


                                                context.startActivity(intent);


                                            }





                                        }
                                    })
                                    .show();






                        }





                    } catch (JSONException e) {
                        e.printStackTrace();
                    }









                }
            });

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int p=getAdapterPosition();

                    Utils.showAlert(context, "Do you want to delete now ?", new DialogEventListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            new DatabaseHelper(context).deleteData(commonData.get(p).getId(),Utils.DBtables.TABLE_EMERGENCY);


                            ((EmergencyLinkActivity)context).getEmergencyData();

                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }
                    });



                }
            });
        }
    }



}
