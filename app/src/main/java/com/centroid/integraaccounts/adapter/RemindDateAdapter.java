package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.RemindData;
import com.centroid.integraaccounts.views.AddAssetActivity;

import java.util.List;

public class RemindDateAdapter extends RecyclerView.Adapter<RemindDateAdapter.RemindDateHolder> {


    Context context;
    List<RemindData>reminddates;

    public RemindDateAdapter(Context context, List<RemindData> reminddates) {
        this.context = context;
        this.reminddates = reminddates;
    }

    public class RemindDateHolder extends RecyclerView.ViewHolder{
        TextView txtReminddate;
        ImageView imgclose;


        public RemindDateHolder(@NonNull View itemView) {
            super(itemView);
            imgclose=itemView.findViewById(R.id.imgclose);
            txtReminddate=itemView.findViewById(R.id.txtReminddate);

            imgclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((AddAssetActivity)context).removeDate(getAdapterPosition());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((AddAssetActivity)context).updateData(getAdapterPosition());

                }
            });
        }
    }

    @NonNull
    @Override
    public RemindDateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_reminddates,parent,false);


        return new RemindDateHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RemindDateHolder holder, int position) {
        holder.setIsRecyclable(false);
        holder.txtReminddate.setText(reminddates.get(position).getDate()+"\n\n"+reminddates.get(position).getDescription());



    }

    @Override
    public int getItemCount() {
        return reminddates.size();
    }
}
