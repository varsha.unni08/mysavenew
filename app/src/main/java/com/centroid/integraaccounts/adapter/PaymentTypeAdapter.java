package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.RechargePlans;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.services.OperatorSelectionListener;

public class PaymentTypeAdapter extends RecyclerView.Adapter<PaymentTypeAdapter.PaymentTypeHolder> {

    Context context;
    OperatorSelectionListener operatorSelectionListener;

    int arricons[]={R.drawable.upi,R.drawable.netbanking,
            R.drawable.debitcard,
            R.drawable.creditcard};
    String arr[]={"UPI (no convenience charges)","NET Banking (payment gateway charge @1.5% application)",
            "Debit Card (payment gateway charge @o.4% application)",
            "Credit card (payment gateway charge @2.1% application)"};

    public PaymentTypeAdapter(Context context, OperatorSelectionListener operatorSelectionListener) {
        this.context = context;
        this.operatorSelectionListener=operatorSelectionListener;
    }

    public class PaymentTypeHolder extends RecyclerView.ViewHolder{
        ImageView imgoperator;
        TextView txtSpinner;

        public PaymentTypeHolder(@NonNull View itemView) {
            super(itemView);
            txtSpinner=itemView.findViewById(R.id.txtSpinner);
            imgoperator=itemView.findViewById(R.id.imgoperator);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(operatorSelectionListener!=null)
                    {

                        operatorSelectionListener.onOperatorSelected(getAdapterPosition(), RechargePlans.DTHPlans.arr_plans[getAdapterPosition()]);
                    }
                }
            });

        }
    }


    @NonNull
    @Override
    public PaymentTypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mobileop_adapter,parent,false);

        return new PaymentTypeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentTypeHolder holder, int position) {

        holder.imgoperator.setImageResource(arricons[position]);
        holder.txtSpinner.setText(arr[position]);

    }

    @Override
    public int getItemCount() {
        return arr.length;
    }
}
