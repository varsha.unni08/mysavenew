package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.AddBankCashDataActivity;
import com.centroid.integraaccounts.views.BankBalanceActivity;
import com.centroid.integraaccounts.views.BankDetailsActivity;
import com.centroid.integraaccounts.views.IncExpListActivity;

import org.json.JSONObject;

import java.util.List;

public class CashBankDataAdapter extends RecyclerView.Adapter<CashBankDataAdapter.CashBankholder> {

    Context context;
    List<LedgerAccount>accounts;

    String startdate="",enddate="";

    public CashBankDataAdapter(Context context, List<LedgerAccount> accounts, String startdate, String enddate) {
        this.context = context;
        this.accounts = accounts;
        this.startdate=startdate;
        this.enddate=enddate;
    }

    public class CashBankholder extends RecyclerView.ViewHolder{

        TextView txtAccount,txtClosingbalance,txtaction;


        public CashBankholder(@NonNull View itemView) {
            super(itemView);

            txtaction=itemView.findViewById(R.id.txtaction);
            txtClosingbalance=itemView.findViewById(R.id.txtClosingbalance);
            txtAccount=itemView.findViewById(R.id.txtAccount);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, IncExpListActivity.class);
                    intent.putExtra("accountsetupid",accounts.get(getAdapterPosition()).getAccountheadid());
                    intent.putExtra("enddate",enddate);
                    intent.putExtra("close_balance",accounts.get(getAdapterPosition()).getClosingbalance());

                    intent.putExtra("startdate",startdate);
                    intent.putExtra("closebalacebeforedate",accounts.get(getAdapterPosition()).getClosingbalancebeforedate());

                    intent.putExtra("close_balance",accounts.get(getAdapterPosition()).getClosingbalance());
                    intent.putExtra("cashbank",1);
                    intent.putExtra("debitcredit",accounts.get(getAdapterPosition()).getDebitcreditopening());

                    intent.putExtra("bydate",1);
                    context.startActivity(intent);

                }
            });
//
//            btnEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent=new Intent(context, AddBankCashDataActivity.class);
//                    intent.putExtra("Commondata",commonData.get(getAdapterPosition()));
//                    context.startActivity(intent);
//
//
//                }
//            });
//
//
//
//
//
//            btndelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
//                    builder.setMessage("Do you want to delete now ? ");
//                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//                            dialogInterface.dismiss();
//
//                            int p=getAdapterPosition();
//
//                            new DatabaseHelper(context).deleteCashbankData(commonData.get(p).getId());
//
//                            commonData.remove(p);
//
//                            notifyItemRemoved(p);
//
//
//                        }
//                    });
//                    builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//
//                        }
//                    });
//
//                    builder.show();
//
//                }
//            });


        }
    }

    @NonNull
    @Override
    public CashBankholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutincomeledger,parent,false);



        return new CashBankholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CashBankholder holder, int position) {

        try {
            holder.setIsRecyclable(false);

            double d=Double.parseDouble(accounts.get(position).getClosingbalance());

            if(d<0||String.valueOf(d).contains("-"))
            {
                d=d*-1;
            }



            holder.txtClosingbalance.setText(d + " " + context.getString(R.string.rs));


            if (!accounts.get(position).getAccountheadid().equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(context).getAccountSettingsByID(accounts.get(position).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    holder.txtAccount.setText(jsonObject.getString("Accountname"));


                }
            } else {

                holder.txtAccount.setText("Cash");
            }


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
