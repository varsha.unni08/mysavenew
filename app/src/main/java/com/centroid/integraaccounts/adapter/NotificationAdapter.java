package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.Notificationdata;
import com.centroid.integraaccounts.data.domain.Notificationmessage;
import com.centroid.integraaccounts.views.NotificationdetailsActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {


    Context context;
    List<Notificationmessage>notificationdata;

    public NotificationAdapter(Context context, List<Notificationmessage> notificationdata) {
        this.context = context;
        this.notificationdata = notificationdata;
    }


    public class NotificationHolder extends RecyclerView.ViewHolder{

        TextView txtTitle,txtMessage,txtTime;

        ImageView imgnotification;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);
            txtMessage=itemView.findViewById(R.id.txtMessage);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            txtTime=itemView.findViewById(R.id.txtTime);
            imgnotification=itemView.findViewById(R.id.imgnotification);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Intent intent=new Intent(context, NotificationdetailsActivity.class);
//                    intent.putExtra("Data",notificationdata.get(getAdapterPosition()));
//
//                    context.startActivity(intent);


                }
            });
        }
    }


    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification,parent,false);


        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {

        //2021-03-08 00:00:00
        holder.setIsRecyclable(false);
        holder.txtTitle.setText(notificationdata.get(position).getTitle());

        holder.txtMessage.setText(notificationdata.get(position).getMessage());

       // Glide.with(context).load(notificationdata.get(position).getMessage().getImage()).into(holder.imgnotification);




        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        Date strDate=null;


        try {
            strDate = sdf.parse(notificationdata.get(position).getCreatedDate());

            String actualdate=sdf1.format(strDate);

            holder.txtTime.setText(actualdate);





        } catch (ParseException e) {
            e.printStackTrace();
        }







    }

    @Override
    public int getItemCount() {

        int i=0;

        if(notificationdata.size()>10)
        {
           i=10;
        }
        else {
            i=notificationdata.size();
        }


        return i;
    }
}
