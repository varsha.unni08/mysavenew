package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddBankVoucherActivity;
import com.centroid.integraaccounts.views.JournalVoucherActivity;

import org.json.JSONObject;

import java.util.List;

public class BankvoucherAdapter extends RecyclerView.Adapter<BankvoucherAdapter.BankVoucherHolder> {


    Context context;
    List<Accounts>accounts;

    public BankvoucherAdapter(Context context, List<Accounts> commonData) {
        this.context = context;
        this.accounts = commonData;
    }

    public class BankVoucherHolder extends RecyclerView.ViewHolder{

//        TextView txtremarks,txtvoucher,txttype,txtamount,txtdate;
TextView txtdate,txtamount,txtaccount,txttype,txtaction;

        public BankVoucherHolder(@NonNull View itemView) {
            super(itemView);

            txtdate=itemView.findViewById(R.id.txtdate);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txttype=itemView.findViewById(R.id.txttype);
            txtaction=itemView.findViewById(R.id.txtaction);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddBankVoucherActivity.class);
                    intent.putExtra("Account",accounts.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });

//            txtdate=itemView.findViewById(R.id.txtdate);
//            txtamount=itemView.findViewById(R.id.txtamount);
//           // txtaccountname=itemView.findViewById(R.id.txtaccountname);
//            txttype=itemView.findViewById(R.id.txttype);
//            txtvoucher=itemView.findViewById(R.id.txtvoucher);
//            txtremarks=itemView.findViewById(R.id.txtremarks);
//
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//
//                    Intent intent=new Intent(context, AddBankVoucherActivity.class);
//                    intent.putExtra("Bankvoucher",commonData.get(getAdapterPosition()));
//
//                    context.startActivity(intent);
//                }
//            });
        }
    }


    @NonNull
    @Override
    public BankVoucherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_paymentvoucheradapter,parent,false);



        return new BankVoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BankVoucherHolder holder, int position) {




        try{
            holder.setIsRecyclable(false);

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte = LocaleHelper.setLocale(context, languagedata);

            Resources resources = conte.getResources();
            holder.txtaction.setText(resources.getString(R.string.edit) + "/" + resources.getString(R.string.delete));

            String arr[]=resources.getStringArray(R.array.bankbalanceType);


            if(accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit+""))
            {
//
//
//
//            if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0"))
//            {

                List<CommonData>commonDa=new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if(commonDa.size()>0)
                {

                    JSONObject jso=new JSONObject(commonDa.get(0).getData());



                    holder.txttype.setText(jso.getString("Accountname"));

                }



          //  }
//
//
//
//

                List<Accounts>accopposit=new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id()+"");

//

                if(accopposit.size()>0)
                {



                    List<CommonData>commonDatacash=new DatabaseHelper(context).getDataByID(accopposit.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonDatacash.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonDatacash.get(0).getData());



                        holder.txtaccount.setText(jso.getString("Accountname"));

                    }



                }


//
//
                  //  holder.txtaccount.setText("Cash");
//
//
//
//
//
//
//          //  }
//
//
//
            String d[]=accounts.get(position).getACCOUNTS_date().split("-");


            holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
            holder.txtamount.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));
//
//
//
//
            }
            else {
//
//
                List<CommonData>commonDa=new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if(commonDa.size()>0)
                {

                    JSONObject jso=new JSONObject(commonDa.get(0).getData());



                    holder.txtaccount.setText(jso.getString("Accountname"));

                }
//
//
//
//
                String d[]=accounts.get(position).getACCOUNTS_date().split("-");

               // holder.txttype.setText("Cash");


                holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
                holder.txtamount.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));
//
//               // holder.txttype.setText(arr[1]);

                List<Accounts>accopposit=new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id()+"");

//

                if(accopposit.size()>0)
                {



                    List<CommonData>commonDatacash=new DatabaseHelper(context).getDataByID(accopposit.get(0).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonDatacash.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonDatacash.get(0).getData());



                        holder.txttype.setText(jso.getString("Accountname"));

                    }



                }
            }



        }catch (Exception e)
        {

        }








    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
