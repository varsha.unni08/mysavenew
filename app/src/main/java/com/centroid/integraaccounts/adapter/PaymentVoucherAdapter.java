package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.PaymentVoucher;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;

import org.json.JSONObject;

import java.util.List;

public class PaymentVoucherAdapter extends RecyclerView.Adapter<PaymentVoucherAdapter.PaymentVoucherHolder> {


    Context context;
    List<Accounts>paymentVouchers;

    public PaymentVoucherAdapter(Context context, List<Accounts> paymentVouchers) {
        this.context = context;
        this.paymentVouchers = paymentVouchers;
    }

    public class PaymentVoucherHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtamount,txtaccount,txttype,txtaction;

        ImageView imgedit;

        public PaymentVoucherHolder(@NonNull View itemView) {
            super(itemView);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txttype=itemView.findViewById(R.id.txttype);
            txtaction=itemView.findViewById(R.id.txtaction);
            imgedit=itemView.findViewById(R.id.imgedit);


            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddPaymentVoucherActivity.class);
                    intent.putExtra("paymentVouchers",paymentVouchers.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }


    @NonNull
    @Override
    public PaymentVoucherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_paymentvoucheradapter,parent,false);


        return new PaymentVoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentVoucherHolder holder, int position) {

        holder.setIsRecyclable(false);

        try {



            if(!paymentVouchers.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0"))
            {

            List<CommonData>commonData=new DatabaseHelper(context).getDataByID(paymentVouchers.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if(commonData.size()>0)
            {

                JSONObject jso=new JSONObject(commonData.get(0).getData());



                holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

            }



            }
            else {

                holder.txtaccount.setText("Cash");
               // holder.txttype.setText("Cash");

            }



            List<Accounts>accounts=new DatabaseHelper(context).getAccountsDataByEntryId(paymentVouchers.get(position).getACCOUNTS_id()+"");



            if(accounts.size()>0)
            {

                Accounts accounts1=accounts.get(0);

                String languagedata = LocaleHelper.getPersistedData(context, "en");
                Context context1= LocaleHelper.setLocale(context, languagedata);

               Resources resources=context1.getResources();
                holder.txtaction.setText(resources.getString(R.string.edit)+"/"+resources.getString(R.string.delete));


                String cashbank=accounts1.getACCOUNTS_cashbanktype();
                if (cashbank.equalsIgnoreCase(Utils.CashBanktype.bank+"")) {


                    List<CommonData>commonData=new DatabaseHelper(context).getDataByID(accounts1.getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonData.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonData.get(0).getData());



                        holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                    }




                    //holder.txttype.setText();

                }
                else if (cashbank.equalsIgnoreCase(Utils.CashBanktype.cash+"")) {
                   // holder.txttype.setText(resources.getString(R.string.cashdata));

                    List<CommonData>commonData=new DatabaseHelper(context).getDataByID(accounts1.getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if(commonData.size()>0)
                    {

                        JSONObject jso=new JSONObject(commonData.get(0).getData());



                        holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                    }


                }


            }

//            else {
//
//
//
//
//
//            }












            String d[]=paymentVouchers.get(position).getACCOUNTS_date().split("-");


            holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
            holder.txtamount.setText(paymentVouchers.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));

//           if(paymentVouchers.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit+"")) {
//
//               holder.txttype.setText("Credit");
//               holder.txttype.setTextColor(Color.parseColor("#75c044"));
//           }
//           else {
//
//               holder.txttype.setText("Debit");
//               holder.txttype.setTextColor(Color.parseColor("#A9F00000"));
//           }
            //holder.txtvoucher.setText(accountname);
          //  holder.txtremarks.setText(paymentVouchers.get(position).getACCOUNTS_remarks());


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return paymentVouchers.size();
    }
}
