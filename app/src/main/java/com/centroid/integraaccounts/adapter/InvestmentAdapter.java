package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddinvestActivity;
import com.centroid.integraaccounts.views.InvestmentListActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class InvestmentAdapter extends RecyclerView.Adapter<InvestmentAdapter.InvestHolder> {

    Context context;
    List<CommonData>commonData;

    public InvestmentAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class InvestHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount,txtlictitle,txtph;
        Button btndelete,btnEdit;

        public InvestHolder(@NonNull View itemView) {
            super(itemView);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtlictitle=itemView.findViewById(R.id.txtlictitle);
            txtph=itemView.findViewById(R.id.txtph);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);


            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddinvestActivity.class);
                    intent.putExtra("invest",commonData.get(getAdapterPosition()));
                    context.startActivity(intent);



                }
            });






            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String languagedata = LocaleHelper.getPersistedData(context, "en");
                    Context con= LocaleHelper.setLocale(context, languagedata);

                    Resources resources=con.getResources();


                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            int p=getAdapterPosition();
                            try {
                                JSONObject jsonObject = new JSONObject(commonData.get(p).getData());

                                String taskarray = jsonObject.getString("Task");

                                if (!taskarray.equalsIgnoreCase("")) {
                                    JSONArray jsonArray = new JSONArray(taskarray);

                                    for (int j = 0; j < jsonArray.length(); j++) {

                                        JSONObject jsonObj = jsonArray.getJSONObject(j);

                                        String Taskid = jsonObj.getString("Taskid");

                                        new DatabaseHelper(context).deleteData(Taskid, Utils.DBtables.TABLE_TASK);

                                    }


                                }

                            }catch (Exception e)
                            {

                            }



                         //   new DatabaseHelper(context).deleteInvestData(commonData.get(p).getId());

                            new DatabaseHelper(context).deleteData(commonData.get(p).getId(), Utils.DBtables.INVESTMENT_table);

                            commonData.remove(p);

                            notifyItemRemoved(p);

                            ((InvestmentListActivity)context).getInvestments();


                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();











                }
            });
        }
    }

    @NonNull
    @Override
    public InvestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);


        return new InvestHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvestHolder holder, int position) {

        try{


            holder.setIsRecyclable(false);


            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context con= LocaleHelper.setLocale(context, languagedata);

           Resources resources=con.getResources();



           holder.btnEdit.setText(resources.getString(R.string.edit));
           holder.btndelete.setText(resources.getString(R.string.delete));




            holder.txtlictitle.setText(resources.getString(R.string.name));

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());


            String month= jsonObject.getString("name");

            List<CommonData> data=new DatabaseHelper(context).getAccountSettingsByID(month);

            if(data.size()>0)
            {

                JSONObject jso=new JSONObject(data.get(0).getData());


                holder.txtmonth.setText(jso.getString("Accountname"));

               // holder.txtvoucher.setText();

            }



            String amount= jsonObject.getString("amount");

            holder.txtamount.setText(amount+" "+context.getString(R.string.rs));
            holder.txtph.setText(resources.getString(R.string.amount));


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
