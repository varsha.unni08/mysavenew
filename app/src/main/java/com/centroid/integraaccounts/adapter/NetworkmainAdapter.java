package com.centroid.integraaccounts.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.IncExpHead;
import com.centroid.integraaccounts.data.domain.MainSettings;
import com.centroid.integraaccounts.data.domain.NetworkDashboard;
import com.centroid.integraaccounts.data.domain.NetworkData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.SettingsData;
import com.centroid.integraaccounts.data.domain.SliderShareImage;
import com.centroid.integraaccounts.fragments.NetworkFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.AppRenewalActivity;
import com.centroid.integraaccounts.views.LoginActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkmainAdapter extends RecyclerView.Adapter<NetworkmainAdapter.NetworkmainHolder> {


    Context context;
    List<NetworkDashboard>networkData;
    NetworkData network;
    NetworkFragment networkFragment;

    public NetworkmainAdapter(NetworkFragment networkFragment,Context context, List<NetworkDashboard> networkData, NetworkData network) {
        this.context = context;
        this.networkData = networkData;
        this.network=network;
        this.networkFragment=networkFragment;
    }

    public class NetworkmainHolder extends RecyclerView.ViewHolder{

        TextView txt,txtLink;

        ImageView imgdropdown;
       // LinearLayout layout_inchead;
        RecyclerView recycler;

        Button btnShare,btnViewmore;

        public NetworkmainHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            txtLink=itemView.findViewById(R.id.txtLink);
            imgdropdown=itemView.findViewById(R.id.imgdropdown);
            btnShare=itemView.findViewById(R.id.btnShare);
           // layout_inchead=itemView.findViewById(R.id.layout_inchead);
            recycler=itemView.findViewById(R.id.recycler);
            btnViewmore=itemView.findViewById(R.id.btnViewmore);

            btnViewmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String url = Utils.domain+"index.php/web/signin";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);

                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final ProgressFragment progressFragment=new ProgressFragment();
                    progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"fkjfk");


                    Map<String,String> params=new HashMap<>();
                    params.put("timestamp",Utils.getTimestamp());

                    new RequestHandler(context, params, new ResponseHandler() {
                        @Override
                        public void onSuccess(String data) {
                            progressFragment.dismiss();
                            // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
                            Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);


                            progressFragment.dismiss();
                            if(profiledata!=null)
                            {

                                try{



                                    if(profiledata.getStatus()==1)
                                    {


                                        if(profiledata.getData()!=null)
                                        {


                                            String phonenumber=profiledata.getData().getMobile();
                                            getNetWorkData(phonenumber,context);

                                            // getCurrencyCode();

                                        }





                                    }
                                    else {

                                        //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();

                                        Utils.showAlertWithSingle(context, "failed", new DialogEventListener() {
                                            @Override
                                            public void onPositiveButtonClicked() {

                                            }

                                            @Override
                                            public void onNegativeButtonClicked() {

                                            }
                                        });


                                    }



                                }catch (Exception e)
                                {

                                }



                            }
                        }

                        @Override
                        public void onFailure(String err) {
                            progressFragment.dismiss();

                            //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();

                            Utils.showAlertWithSingle(context, err, new DialogEventListener() {
                                @Override
                                public void onPositiveButtonClicked() {

                                }

                                @Override
                                public void onNegativeButtonClicked() {

                                }
                            });


                        }
                    },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();





                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(getAdapterPosition()==2)
                    {



                        final String items[]={"auto","zigzag","left","right"};

                        int checkedposition=0;

                        for (int i=0;i<items.length;i++)
                        {

                            if (network.getPositionDownlineSetupDefault().equalsIgnoreCase(items[i]))
                            {
                                checkedposition=i;
                                break;
                            }


                        }





                        new AlertDialog.Builder(context)
                                .setSingleChoiceItems(items, checkedposition, null)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();


                                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();



                                        String item=items[selectedPosition];

                                        network.setPosition(item);

                                        if(item.equalsIgnoreCase("left")||item.equalsIgnoreCase("right"))
                                        {

                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));
                                            networkFragment.updatePositionNext(item,Integer.parseInt(network.getId()));
                                            network.setPositionDownlineSetupDefault(item);
                                            network.setPositionNext(item);
                                        }
                                        else {
                                            network.setPositionDownlineSetupDefault(item);
                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));

                                        }








                                    }
                                })
                                .show();

                    }





                    if(networkData.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }



                        networkData.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(networkData.get(getAdapterPosition()).getSelected()==1) {

                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }


                        networkData.get(getAdapterPosition()).setSelected(0);

                    }


                    notifyDataSetChanged();



                }
            });


            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    if(getAdapterPosition()==2)
                    {



                        final String items[]={"auto","zigzag","left","right"};

                        int checkedposition=0;

                        for (int i=0;i<items.length;i++)
                        {

                            if (network.getPositionDownlineSetupDefault().equalsIgnoreCase(items[i]))
                            {
                                checkedposition=i;
                                break;
                            }


                        }





                        new AlertDialog.Builder(context)
                                .setSingleChoiceItems(items, checkedposition, null)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();


                                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();



                                        String item=items[selectedPosition];

                                      //  network.setPosition(item);

                                        if(item.equalsIgnoreCase("left")||item.equalsIgnoreCase("right"))
                                        {

                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));
                                            networkFragment.updatePositionNext(item,Integer.parseInt(network.getId()));
                                            network.setPositionDownlineSetupDefault(item);
                                            network.setPositionNext(item);
                                        }
                                        else {
                                            network.setPositionDownlineSetupDefault(item);
                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));

                                        }








                                    }
                                })
                                .show();

                    }



















                    if(networkData.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }



                        networkData.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(networkData.get(getAdapterPosition()).getSelected()==1) {

                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }


                        networkData.get(getAdapterPosition()).setSelected(0);

                    }


                    notifyDataSetChanged();



                }
            });



            imgdropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    if(getAdapterPosition()==2)
                    {



                        final String items[]={"auto","zigzag","left","right"};

                        int checkedposition=0;

                        for (int i=0;i<items.length;i++)
                        {

                            if (network.getPosition().equalsIgnoreCase(items[i]))
                            {
                                checkedposition=i;
                                break;
                            }


                        }





                        new AlertDialog.Builder(context)
                                .setSingleChoiceItems(items, checkedposition, null)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();


                                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();



                                        String item=items[selectedPosition];



                                        if(item.equalsIgnoreCase("left")||item.equalsIgnoreCase("right"))
                                        {

                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));
                                            networkFragment.updatePositionNext(item,Integer.parseInt(network.getId()));
                                            network.setPosition(item);
                                            network.setPositionDownlineSetupDefault(item);
                                            network.setPositionNext(item);
                                        }
                                        else {
                                            network.setPosition(item);
                                            network.setPositionDownlineSetupDefault(item);
                                            networkFragment.updatePosition(item,Integer.parseInt(network.getId()));

                                        }












                                    }
                                })
                                .show();

                    }














                    if(networkData.get(getAdapterPosition()).getSelected()==0)
                    {


                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }



                        networkData.get(getAdapterPosition()).setSelected(1);





                    }
                    else if(networkData.get(getAdapterPosition()).getSelected()==1) {

                        for (NetworkDashboard incExpHead:networkData
                        ) {

                            incExpHead.setSelected(0);

                        }


                        networkData.get(getAdapterPosition()).setSelected(0);

                    }


                    notifyDataSetChanged();



                }
            });
        }
    }

    @NonNull
    @Override
    public NetworkmainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_networkholder,parent,false);


        return new NetworkmainHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NetworkmainHolder holder, final int position) {
        holder.setIsRecyclable(false);
        String id=new PreferenceHelper(context).getData(Utils.userkey);



        final String link=Utils.newdomain+"/signup?sponserid="+id;
        holder.txtLink.setText(link);
        if(networkData.get(position).getSelected()==0)
        {
            holder.recycler.setVisibility(View.GONE);
            holder.imgdropdown.setImageResource(R.drawable.ic_spinnerr);
            holder.txtLink.setVisibility(View.GONE);
            holder.btnShare.setVisibility(View.GONE);
            holder.btnViewmore.setVisibility(View.GONE);


        }

        else  if(networkData.get(position).getSelected()==1){








            if(position==1)
            {
                holder.btnViewmore.setVisibility(View.GONE);

                holder.recycler.setVisibility(View.GONE);

                holder.txtLink.setVisibility(View.VISIBLE);
                holder.btnShare.setVisibility(View.VISIBLE);


            }
            else if (position==2)
            {
                holder.btnViewmore.setVisibility(View.GONE);

//                final String items[]={"auto","zigzag","left","right"};
//
//                int checkedposition=0;
//
//                for (int i=0;i<items.length;i++)
//                {
//
//                    if (network.getPosition().equalsIgnoreCase(items[i]))
//                    {
//                        checkedposition=i;
//                        break;
//                    }
//
//
//                }
//
//
//
//
//
//                new AlertDialog.Builder(context)
//                        .setSingleChoiceItems(items, checkedposition, null)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                dialog.dismiss();
//
//
//                                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
//
//
//
//                                String item=items[selectedPosition];
//
//                                network.setPosition(item);
//
//                                networkFragment.updatePosition(item,Integer.parseInt(network.getId()));
//
//
//
//
//
//
//
//
//
//                            }
//                        })
//                        .show();


            }



            else {


                holder.txtLink.setVisibility(View.GONE);
                holder.btnShare.setVisibility(View.GONE);


                holder.recycler.setVisibility(View.VISIBLE);
                holder.btnViewmore.setVisibility(View.VISIBLE);



                holder.recycler.setLayoutManager(new GridLayoutManager(context,2));
                holder.recycler.setAdapter(new NetWorkDashBoardAdapter(context,network));








            }
            holder.imgdropdown.setImageResource(R.drawable.ic_spinnerdropup);



        }


        holder.txt.setText(networkData.get(position).getData());

    }

    @Override
    public int getItemCount() {
        return networkData.size();
    }



    public void getNetWorkData(String phonenumber,Context ctx) {

        //change in webservice

        Map<String, String> params = new HashMap<>();
        new RequestHandler(ctx, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                if (data != null) {

                    if (data != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(data);

                            if (jsonObject.getInt("status") == 1) {
                               // showReferDialog();

                                sharelink();


                            } else {

                                Utils.showAlertWithSingle(context, "Sorry,you are not an active member", new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {


                                        //moreFragment.getProfileData();

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });
                            }


                        } catch (Exception e) {


                        }


                    }


                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.showMemberDetails + "?mobile=" + phonenumber+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();
    }


public void sharelink()
{

    ProgressFragment progressFragment=new ProgressFragment();
    progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"sdjfn");



    Map<String,String> params=new HashMap<>();


    new RequestHandler(context, params, new ResponseHandler() {
        @Override
        public void onSuccess(String data) {

            progressFragment.dismiss();

            if(data!=null)
            {

                try{


                    JSONObject jsonObject=new JSONObject(data);


                  //  MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                    if(jsonObject.getInt("status")==1)
                    {

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        List<SliderShareImage>sliderShareImages=new ArrayList<>();

                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            SliderShareImage sliderShare=new GsonBuilder().create().fromJson(jsonObject1.toString(),SliderShareImage.class);
                            sliderShareImages.add(sliderShare);
                        }


                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.layout_settingsshare);
                        dialog.setTitle(R.string.app_name);
                      //  int b=(int)(height/1.3);
                        dialog.getWindow().setLayout(width,height );

                        ViewPager viewPager=dialog.findViewById(R.id.viewpager);
                        TabLayout tabslayout=dialog.findViewById(R.id.tabslayout);

                        final TextView txtDescription=dialog.findViewById(R.id.txtDescription);

                        Button btnShare=dialog.findViewById(R.id.btnShare);


                        FrameLayout.LayoutParams layoutParams=(FrameLayout.LayoutParams)viewPager.getLayoutParams();
                        int w=(int)context.getResources().getDimension(R.dimen.dimen_270dp);


                        double a=w/0.609375;
                        int h=(int)a;
                        layoutParams.width=w;
                        layoutParams.height=h;
                        viewPager.setLayoutParams(layoutParams);

                        tabslayout.removeAllTabs();

                        viewPager.setAdapter(new ShareSliderAdapter(context,sliderShareImages));

                        SliderShareImage sliderShareImg=sliderShareImages.get(0);
                        txtDescription.setText(sliderShareImg.getDescription());

                        for (SliderShareImage sliderShareImage:sliderShareImages)
                        {
                            tabslayout.addTab(tabslayout.newTab());



                        }


                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                tabslayout.getTabAt(position).select();

                                txtDescription.setText(sliderShareImages.get(position).getDescription());

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });


                        tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                viewPager.setCurrentItem(tab.getPosition());

                                txtDescription.setText(sliderShareImages.get(tab.getPosition()).getDescription());

                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                        Button btncopy=dialog.findViewById(R.id.btncopy);
                        TextView txtlink=dialog.findViewById(R.id.txtlink);
                        TextView txtSubDescription=dialog.findViewById(R.id.txtSubDescription);

                        String languagedata = LocaleHelper.getPersistedData(context, "en");
                        Context contex= LocaleHelper.setLocale(context, languagedata);

                        Resources resources=contex.getResources();

                        btncopy.setText(resources.getString(R.string.copylink));



                        String id = new PreferenceHelper(context).getData(Utils.userkey);
                        final String link = Utils.newdomain + "/signup?sponserid=" + id;
                        txtlink.setText(  link );

                        btncopy.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int sdk = android.os.Build.VERSION.SDK_INT;
                                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                    clipboard.setText(link);

                                    Utils.showAlertWithSingle(context, resources.getString(R.string.linkcopied), new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                } else {
                                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                    android.content.ClipData clip = android.content.ClipData.newPlainText("text label",link);
                                    clipboard.setPrimaryClip(clip);

                                    Utils.showAlertWithSingle(context,  resources.getString(R.string.linkcopied), new DialogEventListener() {
                                        @Override
                                        public void onPositiveButtonClicked() {

                                        }

                                        @Override
                                        public void onNegativeButtonClicked() {

                                        }
                                    });
                                }
                            }
                        });

                        txtSubDescription.setText(resources.getString(R.string.sharelinkmsg));

                        btnShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {



                                SliderShareImage sliderShareImage=sliderShareImages.get(viewPager.getCurrentItem());

                                String imgurl=Utils.sliderimageurl+sliderShareImage.getImage();

                                networkFragment.getBitmapData(imgurl,sliderShareImage.getDescription());

                            }
                        });



                        txtDescription.setVisibility(View.GONE);

                        dialog.show();



//                        SettingsData settingsData1=settingsData.getData();
//
//                        String url=Utils.linkimg+settingsData1.getLinkImage();
//
//                        //  String  url="https://mysaving.in/images/saveicon.png";
//
//                        networkFragment.getBitmapData(url);
//
//                                        String imgurl=Utils.imgbaseurl+url;









                    }
                    else {





                    }



                }catch (Exception e)
                {

                }



            }

        }

        @Override
        public void onFailure(String err) {
            progressFragment.dismiss();

            //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

        }
    },Utils.WebServiceMethodes.getSettingsSlider+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();

}





}
