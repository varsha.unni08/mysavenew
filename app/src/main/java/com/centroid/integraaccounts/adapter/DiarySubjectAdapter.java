package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.util.List;

public class DiarySubjectAdapter extends BaseAdapter {


    Context context;
    List<CommonData>commonData;

    public DiarySubjectAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    @Override
    public int getCount() {
        return commonData.size();
    }

    @Override
    public Object getItem(int i) {
        return commonData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int i, View view, ViewGroup parent) {

        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_diarysubjectadapter,parent,false);

        TextView txt=v.findViewById(R.id.txt);

        ImageView imgdelete=v.findViewById(R.id.imgdelete);
        imgdelete.setVisibility(View.GONE);


        try{

            txt.setText(commonData.get(i).getData());

            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

//            JSONObject jsonObject=new JSONObject(commonData.get(i).getData());
//
//            txt.setText(jsonObject.getString("Accountname"));



        }catch (Exception e)
        {


        }




        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View v= layoutInflater.inflate(R.layout.layout_diarysubjectadapter,parent,false);

        TextView txt=v.findViewById(R.id.txt);

        ImageView imgdelete=v.findViewById(R.id.imgdelete);
        imgdelete.setVisibility(View.GONE);

        imgdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        try{

            txt.setText(commonData.get(position).getData());

//            JSONObject jsonObject=new JSONObject(commonData.get(i).getData());
//
//            txt.setText(jsonObject.getString("Accountname"));



        }catch (Exception e)
        {


        }




        return v;
    }
}
