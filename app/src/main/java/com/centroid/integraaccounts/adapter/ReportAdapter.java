package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.AssetLedgerActivity;
import com.centroid.integraaccounts.views.AssetListActivity;
import com.centroid.integraaccounts.views.BankBalanceActivity;
import com.centroid.integraaccounts.views.BillRegisterActivity;
import com.centroid.integraaccounts.views.IncomeExpActivity;
import com.centroid.integraaccounts.views.IncomexpenditureActivity;
import com.centroid.integraaccounts.views.InsuranceActivity;
import com.centroid.integraaccounts.views.InsuranceLedgerActivity;
import com.centroid.integraaccounts.views.InvestmentLedgerActivity;
import com.centroid.integraaccounts.views.InvestmentListActivity;
import com.centroid.integraaccounts.views.LedgerActivity;
import com.centroid.integraaccounts.views.LiabilitiesActivity;
import com.centroid.integraaccounts.views.LiabilityLedgerActivity;
import com.centroid.integraaccounts.views.RechargeHistoryActivity;
import com.centroid.integraaccounts.views.RemindsActivity;
import com.centroid.integraaccounts.views.TransactionsActivity;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportHolder> {


    Context context;



    int arr[]={R.string.transactions,
            R.string.ledgers,R.string.cashbankstatement,
            R.string.incomeandexpenditure,R.string.reminds,
            R.string.listofmyassets,R.string.listmyliabilities,
            R.string.listofmyinsurance,R.string.listofmyinvestments,R.string.billregister};

    public ReportAdapter(Context context) {
        this.context = context;
    }

    public class ReportHolder extends RecyclerView.ViewHolder{

        TextView txt;


        public ReportHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    switch (getAdapterPosition())
                    {

//                        case 0:
//
//
//                            Intent inten022=new Intent(context, RechargeHistoryActivity.class);
//                            context.startActivity(inten022);
//
//
//                            break;

                        case 0:


                            Intent inten0=new Intent(context, TransactionsActivity.class);
                            context.startActivity(inten0);


                            break;


                        case 1:


                            Intent inten01=new Intent(context, LedgerActivity.class);
                            context.startActivity(inten01);


                            break;

                        case 2:

                            Intent inten1=new Intent(context, LedgerActivity.class);
                            inten1.putExtra("Bankcash",1);
                            context.startActivity(inten1);
                            break;

                        case 3:

//                            Intent inten2=new Intent(context, IncomexpenditureActivity.class);

                            Intent inten2=new Intent(context, IncomeExpActivity.class);
                            context.startActivity(inten2);


                            break;

                        case 4:

                            Intent inten4=new Intent(context, RemindsActivity.class);
                            context.startActivity(inten4);


                            break;


                        case 5:

                            Intent inten=new Intent(context, AssetLedgerActivity.class);
                            context.startActivity(inten);
                            break;

                        case 6:

                            Intent inten6=new Intent(context, LiabilityLedgerActivity.class);
                            context.startActivity(inten6);
                            break;



                        case 7:

                            Intent intent12=new Intent(context, InsuranceLedgerActivity.class);
                            context.startActivity(intent12);
                            break;
                        case 8:

                            Intent intent1=new Intent(context, InvestmentLedgerActivity.class);
                            context.startActivity(intent1);
                            break;


                        case 9:

                            Intent intent9=new Intent(context, BillRegisterActivity.class);
                            context.startActivity(intent9);
                            break;




                    }



                }
            });
        }
    }

    @NonNull
    @Override
    public ReportHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_moreadapter,parent,false);


        return new ReportHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportHolder holder, int position) {
        holder.setIsRecyclable(false);
        String languagedata = LocaleHelper.getPersistedData(context, "en");
        Context contex= LocaleHelper.setLocale(context, languagedata);

        Resources resources=contex.getResources();

        holder.txt.setText( Utils.getCapsSentences(context,resources.getString(arr[position]))  );
      //  holder.txt.setText(resources.getString(arr[position]));

    }

    @Override
    public int getItemCount() {
        return arr.length;
    }
}
