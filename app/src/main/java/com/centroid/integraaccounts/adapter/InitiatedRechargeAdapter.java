package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.initiated.InitiatedRecharge;
import com.centroid.integraaccounts.views.rechargeViews.DTHRechargeDashboardActivity;
import com.centroid.integraaccounts.views.rechargeViews.MobileRechargeDashboardActivity;

import java.util.List;

public class InitiatedRechargeAdapter extends RecyclerView.Adapter<InitiatedRechargeAdapter.InitHolder> {


    Context context;
    List<InitiatedRecharge>initiatedRecharges;

    public InitiatedRechargeAdapter(Context context, List<InitiatedRecharge> initiatedRecharges) {
        this.context = context;
        this.initiatedRecharges = initiatedRecharges;
    }

    public class InitHolder extends RecyclerView.ViewHolder{
        TextView txtmonth,txtStatus,txtRefundTransactionid,txtmessage;

        Button btn_retry;
        ImageView imgoperator;

        public InitHolder(@NonNull View itemView) {
            super(itemView);
            btn_retry=itemView.findViewById(R.id.btn_retry);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txtmessage=itemView.findViewById(R.id.txtmessage);
            imgoperator=itemView.findViewById(R.id.imgoperator);
            txtRefundTransactionid=itemView.findViewById(R.id.txtRefundTransactionid);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  //  if(!initiatedRecharges.get(getAdapterPosition()).getRecharge_status().equalsIgnoreCase("1")) {

                        ((MobileRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));




                   // }
                }
            });



//            imgoperator.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(!initiatedRecharges.get(getAdapterPosition()).getPaymentStatus() .equalsIgnoreCase("2")) {
//
//                    //    if(context instanceof MobileRechargeDashboardActivity) {
//
//                         //   ((MobileRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
//
////                        }
////                        else{
////
////                            ((DTHRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
////
////                        }
//
//
//                    }
//                    else{
//
//                        Utils.showAlert(context, "Your recharge is pendinng.Can you retry again?", new DialogEventListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//
//                                if(context instanceof MobileRechargeDashboardActivity) {
//
//
//                                    ((MobileRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//
//                                }
//                                else{
//
//                                    ((DTHRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//
//                                }
//
//
//
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        });
//
//                    }
//                }
//            });
//
//            txtmonth.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(!initiatedRecharges.get(getAdapterPosition()).getStatus().equalsIgnoreCase("2")) {
//
//                        if(context instanceof MobileRechargeDashboardActivity) {
//
//                            ((MobileRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
//
//                        }
//                        else{
//
//                            ((DTHRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
//
//                        }
//                    }
//                    else{
//
//                        Utils.showAlert(context, "Your recharge is pendinng.Can you retry again?", new DialogEventListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//
//                                if(context instanceof MobileRechargeDashboardActivity) {
//
//                                    ((MobileRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//                                }
//                                else{
//
//                                    ((DTHRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//
//                                }
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        });
//
//                    }
//                }
//            });
//
//            txtStatus.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(!initiatedRecharges.get(getAdapterPosition()).getStatus().equalsIgnoreCase("2")) {
//
//                        if(context instanceof MobileRechargeDashboardActivity) {
//
//                            ((MobileRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
//
//                        }
//                        else{
//
//                            ((DTHRechargeDashboardActivity) context).retryRechargeFromInitiatedHistory(initiatedRecharges.get(getAdapterPosition()));
//
//                        }
//                    }
//                    else{
//
//                        Utils.showAlert(context, "Your recharge is pendinng.Can you retry again?", new DialogEventListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//                                if(context instanceof MobileRechargeDashboardActivity) {
//
//                                    ((MobileRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//                                }
//                                else{
//
//                                    ((DTHRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//
//                                }
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        });
//
//                    }
//                }
//            });
//
//
//            btn_retry.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(initiatedRecharges.get(getAdapterPosition()).getStatus().equalsIgnoreCase("2")) {
//
//                        ((MobileRechargeDashboardActivity) context).retryRechargeInit(initiatedRecharges.get(getAdapterPosition()));
//
//
//                    }
//                }
//            });
        }
    }


    @NonNull
    @Override
    public InitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_initiatedrecharge,parent,false);


        return new InitHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InitHolder holder, int position) {
        holder.txtmonth.setText("");

        holder.txtmonth.append(initiatedRecharges.get(position).getMobileNumber()+"\n"+initiatedRecharges.get(position).getAmount());
//        holder.txtmonth.append("\n\nOperator : "+initiatedRecharges.get(position).getOperator());

        if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Jio"))
        {

            holder.imgoperator.setImageResource(R.drawable.jio);
        }
        else if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("BSNL"))
        {
            holder.imgoperator.setImageResource(R.drawable.bsnl);

        }
        else if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Vi"))
        {

            holder.imgoperator.setImageResource(R.drawable.vi);
        }
        else{

            holder.imgoperator.setImageResource(R.drawable.airtel);
        }


//        holder.txtmonth.append("\n\nPayment Type : "+initiatedRecharges.get(position).getPaymentMode()+"\n");
        if(initiatedRecharges.get(position).getRechargeType().equalsIgnoreCase("2"))
        {
            holder.txtmonth.append("\n"+initiatedRecharges.get(position).getAmount()+"\n");

            if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Dish TV"))
            {

                holder.imgoperator.setImageResource(R.drawable.dishtv);
            }
            else if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Airtel Digital TV"))
            {
                holder.imgoperator.setImageResource(R.drawable.airteldigitaltv);

            }
            else if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Sun Direct"))
            {

                holder.imgoperator.setImageResource(R.drawable.sundirect);
            }
            else if(initiatedRecharges.get(position).getOperator().equalsIgnoreCase("Tata Sky"))
            {

                holder.imgoperator.setImageResource(R.drawable.ttsky);
            }
            else{

                holder.imgoperator.setImageResource(R.drawable.d2h);
            }


        }

        if(initiatedRecharges.get(position).getPaymentStatus().equalsIgnoreCase("0"))
        {
            holder.txtStatus.setText("Payment Failed");
            holder.txtStatus.setTextColor(Color.RED);
        }
        if(initiatedRecharges.get(position).getPaymentStatus().equalsIgnoreCase("1"))
        {
            holder.txtStatus.setText("Payment Success");
            holder.txtStatus.setTextColor(Color.parseColor("#0d5c4b"));
        }



        if(initiatedRecharges.get(position).getPaymentStatus().equalsIgnoreCase("2"))
        {
            holder.txtStatus.setText("Payment Initiated");
            holder.txtStatus.setTextColor(Color.BLUE);

            holder.btn_retry.setVisibility(View.VISIBLE);
        }



        if(initiatedRecharges.get(position).getRecharge_status().equalsIgnoreCase("3"))
        {
            holder.txtmessage.setText("Refunded");
            holder.txtmessage.setTextColor(Color.parseColor("#0d5c4b"));

            holder.txtRefundTransactionid.setVisibility(View.VISIBLE);
            holder.txtRefundTransactionid.setText("Refund ID : "+initiatedRecharges.get(position).getRefundTransactionId());

        }
        else if(initiatedRecharges.get(position).getRecharge_status().equalsIgnoreCase("1"))
        {
            holder.txtmessage.setText("Recharge Success");
            holder.txtmessage.setTextColor(Color.parseColor("#0d5c4b"));
        }

        else if(initiatedRecharges.get(position).getRecharge_status().equalsIgnoreCase("0"))
        {
            holder.txtmessage.setText("Recharge Failed");
            holder.txtmessage.setTextColor(Color.RED);
        }

        else if(initiatedRecharges.get(position).getRecharge_status().equalsIgnoreCase("2"))
        {
            holder.txtmessage.setText("Recharge Pending");
            holder.txtmessage.setTextColor(Color.BLUE);
        }

    }

    @Override
    public int getItemCount() {
        return initiatedRecharges.size();
    }
}
