package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Budget;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.fragments.SetBudgetFragment;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.views.AddBudgetActivity;
import com.centroid.integraaccounts.views.BudgetListActivity;
import com.centroid.integraaccounts.views.LoginActivity;

import org.json.JSONObject;

import java.util.List;

public class BudgetDataAdapter extends RecyclerView.Adapter<BudgetDataAdapter.BudgetHolder> {
    String arrmonth[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    Context context;
    List<CommonData>budgets;
    SetBudgetFragment setBudgetFragment;

    public BudgetDataAdapter(Context context, List<CommonData> budgets, SetBudgetFragment setBudgetFragment) {
        this.context = context;
        this.budgets = budgets;
        this.setBudgetFragment=setBudgetFragment;
    }

    public class BudgetHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount;

        ImageView imgedit;
        //Button btndelete,btnEdit;


        public BudgetHolder(@NonNull View itemView) {
            super(itemView);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            imgedit=itemView.findViewById(R.id.imgedit);

//            btndelete=itemView.findViewById(R.id.btndelete);
//            btnEdit=itemView.findViewById(R.id.btnEdit);


            imgedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddBudgetActivity.class);
                    intent.putExtra("budget",budgets.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });






//            btndelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
//                    builder.setMessage("Do you want to delete now ? ");
//                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//                            dialogInterface.dismiss();
//
//                            int p=getAdapterPosition();
//
//                            Budget budget=budgets.get(getAdapterPosition());
//
//                            new DatabaseHelper(context).deleteBudgetData(budget.getId());
//
//
//                            budgets.remove(p);
//
//                            notifyItemRemoved(p);
//
//                            if(setBudgetFragment!=null) {
//
//
//                                setBudgetFragment.getBudgetData();
//                            }
//                            else {
//
//
//                                ((BudgetListActivity)context).getBudgetData();
//
//                            }
//
//
//
//
//
//                        }
//                    });
//                    builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//
//                        }
//                    });
//
//                    builder.show();
//                }
//            });
        }
    }

    @NonNull
    @Override
    public BudgetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budgettally,parent,false);


        return new BudgetHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BudgetHolder holder, int position) {

        try{
            holder.setIsRecyclable(false);

            JSONObject jsonObject=new JSONObject(budgets.get(position).getData());


            String month= jsonObject.getString("month");

            String amount= jsonObject.getString("amount");

            holder.txtamount.setText(amount+" "+context.getString(R.string.rs));
            holder.txtmonth.setText(month);

        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return budgets.size();
    }
}
