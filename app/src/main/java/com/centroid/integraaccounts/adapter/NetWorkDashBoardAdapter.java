package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.NetworkData;

public class NetWorkDashBoardAdapter extends RecyclerView.Adapter<NetWorkDashBoardAdapter.NetWorkDashBoardHolder> {


    Context context;
    NetworkData networkData;

    public NetWorkDashBoardAdapter(Context context, NetworkData networkData) {
        this.context = context;
        this.networkData = networkData;
    }

    public class NetWorkDashBoardHolder extends RecyclerView.ViewHolder{

        TextView txt;

        public NetWorkDashBoardHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
        }
    }

    @NonNull
    @Override
    public NetWorkDashBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_networkdashboardadapter,parent,false);


        return new NetWorkDashBoardHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NetWorkDashBoardHolder holder, int position) {
        holder.setIsRecyclable(false);
        switch (position)
        {

            case 0:

                holder.txt.setText("Total payment received \n"+"0");


                break;
            case 1:

                holder.txt.setText("Total matched \n"+networkData.getBinaryMatched());
                break;

            case 2:
                holder.txt.setText("Carry \nLeft : "+networkData.getCarryLeft()+"\nRight : "+networkData.getCarryRight());

                break;
            case 3:

                holder.txt.setText("Binary \nLeft : "+networkData.getBinaryLeft()+"\nRight : "+networkData.getBinaryRight());


                break;
            case 4:

                holder.txt.setText(""+networkData.getBinaryLeft()+"\nLeft");

                break;
            case 5:
                holder.txt.setText(""+networkData.getBinaryRight()+"\nRight");

                break;

            case 6:

                holder.txt.setText("Binary : "+networkData.getBinaryAmt()+"\nReferal : "+networkData.getReferralCommissionAmt()+"");

                break;
            case 7:
                holder.txt.setText("Level : "+networkData.getLevelAmt()+"\nAcheivement bonus : "+networkData.getAchievementAmt());

                break;



        }

    }

    @Override
    public int getItemCount() {
        return 8;
    }
}
