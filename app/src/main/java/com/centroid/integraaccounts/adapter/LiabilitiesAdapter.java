package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddLiabilitiesActivity;
import com.centroid.integraaccounts.views.LiabilitiesActivity;
import com.centroid.integraaccounts.views.LoanListActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

public class LiabilitiesAdapter extends RecyclerView.Adapter<LiabilitiesAdapter.LiabilityHolder> {

    Context context;
    List<CommonData>commonData;

    public LiabilitiesAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class LiabilityHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount,txtlictitle,txtph,txtdot2,txtdot1;
        Button btndelete,btnEdit;


    public LiabilityHolder(@NonNull View itemView) {
        super(itemView);

        txtamount=itemView.findViewById(R.id.txtamount);
        txtmonth=itemView.findViewById(R.id.txtmonth);
        txtlictitle=itemView.findViewById(R.id.txtlictitle);
        txtph=itemView.findViewById(R.id.txtph);

        btndelete=itemView.findViewById(R.id.btndelete);
        btnEdit=itemView.findViewById(R.id.btnEdit);

        txtdot2=itemView.findViewById(R.id.txtdot2);
        txtdot1=itemView.findViewById(R.id.txtdot1);



        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

Intent i=new Intent(context, AddLiabilitiesActivity.class);
i.putExtra("CommonData",commonData.get(getAdapterPosition()));

                context.startActivity(i);

            }
        });






        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String languagedata = LocaleHelper.getPersistedData(context, "en");
                Context conte= LocaleHelper.setLocale(context, languagedata);

                Resources resources=conte.getResources();


                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setMessage(resources.getString(R.string.deleteconfirm));
                builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                        try {

                            int p = getAdapterPosition();

                            JSONObject jsonObject=new JSONObject(commonData.get(p).getData());

                            String taskid=jsonObject.getString("task");


                            if(jsonObject.getString("loantype").equalsIgnoreCase("EMI"))
                            {

                                JSONArray jsonArray=new JSONArray(taskid);

                                for (int j=0;j<jsonArray.length();j++)
                                {

                                    JSONObject jsonObject1=jsonArray.getJSONObject(j);

                                    String t=jsonObject1.getString("taskid");

                                    new DatabaseHelper(context).deleteData(t, Utils.DBtables.TABLE_TASK);


                                }


                            }
                            else {


                                new DatabaseHelper(context).deleteData(taskid, Utils.DBtables.TABLE_TASK);





                            }


                            new DatabaseHelper(context).deleteData(commonData.get(p).getId(), Utils.DBtables.TABLE_LIABILITY);


                            commonData.remove(p);

                            notifyItemRemoved(p);

                            ((LiabilitiesActivity) context).getAllLiabilities();
                        }catch (Exception e)
                        {

                        }


                    }
                });
                builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });

                builder.show();
            }
        });

    }
}


    @NonNull
    @Override
    public LiabilityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);



        return new LiabilityHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LiabilityHolder holder, int position) {

        holder.setIsRecyclable(false);
       holder.btnEdit.setVisibility(View.VISIBLE);

        String languagedata = LocaleHelper.getPersistedData(context, "en");
        Context conte= LocaleHelper.setLocale(context, languagedata);

       Resources resources=conte.getResources();

       holder.txtlictitle.setText(resources.getString(R.string.accountname));
       holder.txtph.setText(resources.getString(R.string.amount));
       holder.btndelete.setText(resources.getString(R.string.delete));


        try{

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());


            String js=jsonObject.getString("loan");


            List<CommonData> data=new DatabaseHelper(context).getDataByID(js, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if(data.size()>0)
            {

               // {"Accountname":"house loan ","Amount":"2500","Accounttype":"Liability account","Type":"Credit"}

                JSONObject jso=new JSONObject(data.get(0).getData());


                holder.txtmonth.setText(jso.getString("Accountname"));


                String Type=jso.getString("Type");


                String Amount = jsonObject.getString("openingbalance");


                holder.txtamount.setText(Amount+" "+resources.getString(R.string.rs));

            }



        }catch (Exception e)
        {

            Log.e("ERR",e.toString());
        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }



    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = Utils.getAllAccountBeforeIncludesDate(context, selected_date);

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }
}
