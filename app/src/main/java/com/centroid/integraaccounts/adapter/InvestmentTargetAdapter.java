package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.dialogs.InvestMentFragment;
import com.centroid.integraaccounts.views.InvestmentLedgerActivity;
import com.centroid.integraaccounts.views.NewTargetActivity;

import org.json.JSONObject;

import java.util.List;

public class InvestmentTargetAdapter extends RecyclerView.Adapter<InvestmentTargetAdapter.InvestmentHolder> {


    Context context;
    List<CommonData> cmd;

    InvestMentFragment investMentFragment;


    public InvestmentTargetAdapter(Context context, List<CommonData> cmd, InvestMentFragment investMentFragment) {
        this.context = context;
        this.cmd = cmd;

        this.investMentFragment=investMentFragment;
    }

    public class InvestmentHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount,txtlictitle,txtph,txtmessage;
        Button btndelete,btnEdit;

        public InvestmentHolder(@NonNull View itemView) {
            super(itemView);

            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtlictitle=itemView.findViewById(R.id.txtlictitle);
            txtph=itemView.findViewById(R.id.txtph);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);
            txtmessage=itemView.findViewById(R.id.txtmessage);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(cmd.get(getAdapterPosition()).getIsalreadyMapped()==0) {


                        investMentFragment.onSelectInvestment(cmd.get(getAdapterPosition()));
                    }
                }
            });
        }
    }


    @NonNull
    @Override
    public InvestmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);



        return new InvestmentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvestmentHolder holder, int position) {
        try{


            holder.setIsRecyclable(false);

            holder.btndelete.setVisibility(View.GONE);
            holder.btnEdit.setVisibility(View.GONE);


            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context con= LocaleHelper.setLocale(context, languagedata);

            Resources resources=con.getResources();



            holder.btnEdit.setText(resources.getString(R.string.edit));
            holder.btndelete.setText(resources.getString(R.string.delete));




            holder.txtlictitle.setText(resources.getString(R.string.name));


            if(isAlreadyMapped(cmd.get(position).getId()))
            {

                cmd.get(position).setIsalreadyMapped(1);
                holder.txtmessage.setText("Already used");
                holder.txtmessage.setVisibility(View.VISIBLE);
            }
            else{

                cmd.get(position).setIsalreadyMapped(0);
                holder.txtmessage.setVisibility(View.GONE);
            }




          //  JSONObject jsonObject=new JSONObject(cmd.get(position).getData());


          //  String accounttype= jsonObject.getString("Accounttype");

//            List<CommonData> data=new DatabaseHelper(context).getAccountSettingsByID(month);




//            if(data.size()>0)
//            {

                JSONObject jso=new JSONObject(cmd.get(position).getData());


                holder.txtmonth.setText(jso.getString("Accountname"));

                // holder.txtvoucher.setText();

                String amount= jso.getString("Amount");

             double closingbalance=   getClosingBalance(Double.parseDouble(amount),cmd.get(position).getId(),"","");
                holder.txtamount.setText(closingbalance+" "+context.getString(R.string.rs));

         //   }






            holder.txtph.setText(resources.getString(R.string.amount));


        }catch (Exception e)
        {

        }
    }

    @Override
    public int getItemCount() {
        return cmd.size();
    }

    public boolean isAlreadyMapped(String id)
    {

        boolean a=false;

      List<CommonData>cmd=   new DatabaseHelper(context).getData( Utils.DBtables.TABLE_TARGET);

      if(cmd.size()>0)
      {

          for(CommonData c:cmd)
          {

              try{

                  JSONObject jj=new JSONObject(c.getData());

                  if(jj.has("investment_id"))
                  {

                      String investment_id=jj.getString("investment_id");

                      if(investment_id.equalsIgnoreCase(id))
                      {

                          a=true;
                          break;
                      }







                  }



              }
              catch (Exception e)
              {

              }


          }




      }

      return a;

    }


    public double getClosingBalance(double openingbalance, String id, String selected_date,String type)
    {
        List<Accounts> allAccountbeforedate = new DatabaseHelper(context).getAllAccounts();

        double closingbalancebeforemonth = 0;

        double creditamount=0,debitamount=0;




        if (allAccountbeforedate.size() > 0) {

            for (Accounts acc : allAccountbeforedate) {


                if(!id.equalsIgnoreCase("0")) {


                    if (acc.getACCOUNTS_setupid().equalsIgnoreCase(id)) {

                        if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {

                            openingbalance = openingbalance - Double.parseDouble(acc.getACCOUNTS_amount());
                            // debitorCredit=Utils.Cashtype.credit;

                        } else if (acc.getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit + "")) {

                            openingbalance = openingbalance + Double.parseDouble(acc.getACCOUNTS_amount());

                            // debitorCredit=Utils.Cashtype.debit;
                        }


                    }
                }



            }


            closingbalancebeforemonth = openingbalance;
            //list for checking closing balance before selected month and date


        }
        else {

            closingbalancebeforemonth=openingbalance;





        }

        return closingbalancebeforemonth;

    }
}
