package com.centroid.integraaccounts.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.AddBankVoucherActivity;
import com.centroid.integraaccounts.views.AddBillVoucherActivity;
import com.centroid.integraaccounts.views.AddPaymentVoucherActivity;
import com.centroid.integraaccounts.views.AddReceiptActivity;
import com.centroid.integraaccounts.views.BillVoucherListActivity;
import com.centroid.integraaccounts.views.PaymentVoucherActivity;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BillVoucherAdapter extends RecyclerView.Adapter<BillVoucherAdapter.BillVoucherHolder> {



    Context context;
    List<Accounts>accounts;

    public BillVoucherAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    public class BillVoucherHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtamount,txtaccount,txttype,txtaction,txtdownload;


        public BillVoucherHolder(@NonNull View itemView) {
            super(itemView);

            txtdate=itemView.findViewById(R.id.txtdate);
            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txttype=itemView.findViewById(R.id.txttype);
            txtaction=itemView.findViewById(R.id.txtaction);
            txtdownload=itemView.findViewById(R.id.txtdownload);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent=new Intent(context, AddBillVoucherActivity.class);
                    intent.putExtra("Account",accounts.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });

            txtdownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    Intent intent=new Intent(context, AddReceiptActivity.class);
                    intent.putExtra("generate",1);
                    intent.putExtra("receipt",accounts.get(getAdapterPosition()));
                    context.startActivity(intent);


//                    Utils.showAlert(context, "Do you want to download receipt now ? ", new DialogEventListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//                            //((BillVoucherListActivity)context).showReceiptVoucherPdf(accounts.get(getAdapterPosition()));
//
//
//
//
//                        }
//
//                        @Override
//                        public void onNegativeButtonClicked() {
//
//                        }
//                    });



                }
            });
        }
    }

    @NonNull
    @Override
    public BillVoucherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_paymentvoucheradapter,parent,false);


        return new BillVoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BillVoucherHolder holder, int position) {

        try {

            holder.setIsRecyclable(false);

            List<Accounts>accounts_billid=new DatabaseHelper(context).getAccountsDataByBillid(accounts.get(position).getACCOUNTS_id()+"");

            if(accounts_billid.size()>0)
            {
                holder.txtdownload.setVisibility(View.GONE);
            }
            else {

                holder.txtdownload.setVisibility(View.VISIBLE);
            }


            String d[] = accounts.get(position).getACCOUNTS_date().split("-");

            holder.txtdate.setText(d[0] + "/" + d[1] + "/" + d[2]);

            if (accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.credit + "")) {


                if(accounts.get(position).getACCOUNTS_entryid().equalsIgnoreCase("0")) {
                    String id = accounts.get(position).getACCOUNTS_id() + "";

                    String setupid = accounts.get(position).getACCOUNTS_setupid();

                    List<CommonData> commonDa = new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                    if (commonDa.size() > 0) {

                        JSONObject jso = new JSONObject(commonDa.get(0).getData());


                        holder.txttype.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                    }

                }
            }


            holder.txtamount.setText(accounts.get(position).getACCOUNTS_amount()+context.getString(R.string.rs));



           List<Accounts>  acc1=new DatabaseHelper(context).getAccountsDataByEntryId(accounts.get(position).getACCOUNTS_id()+"");

            if(acc1.size()>0)
            {

                String setupid=acc1.get(0).getACCOUNTS_setupid();

                List<CommonData> commonDa = new DatabaseHelper(context).getDataByID(setupid, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if (commonDa.size() > 0) {

                    JSONObject jso = new JSONObject(commonDa.get(0).getData());


                    holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }

            }


           // holder.txttype



        }catch (Exception e)
        {

        }


    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
