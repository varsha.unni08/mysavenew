package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;

import org.json.JSONObject;

import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionHolder> {


    Context context;
    List<Accounts>accounts;

    public TransactionsAdapter(Context context, List<Accounts> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    public class TransactionHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtaccount,txtdebit,txtcredit;


        public TransactionHolder(@NonNull View itemView) {
            super(itemView);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txtdebit=itemView.findViewById(R.id.txtdebit);
            txtcredit=itemView.findViewById(R.id.txtcredit);
        }
    }


    @NonNull
    @Override
    public TransactionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_transaction,parent,false);


        return new TransactionHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionHolder holder, int position) {
        holder.setIsRecyclable(false);
        try {


            if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.paymentvoucher)
            {

                holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.paymentvouchercolour));


            }


            if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.receiptvoucher)
            {

                holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.receiptcolour));

            }

            if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.billvoucher)
            {

                holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.billvouchercolour));

            }

            if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.bankvoucher)
            {

                holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.bankvouchercolour));

            }

            if(accounts.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.journalvoucher)
            {

                holder.itemView.setBackgroundColor(ContextCompat.getColor(context,R.color.journalvouchercolour));

            }




            String d[] = accounts.get(position).getACCOUNTS_date().split("-");


            holder.txtdate.setText(d[0] + "/" + d[1] + "/" + d[2]);


            if(!accounts.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(context).getDataByID(accounts.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if (commonData.size() > 0) {

                    JSONObject jso = new JSONObject(commonData.get(0).getData());


                    holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }


            }
            else {

                holder.txtaccount.setText("Cash");

            }


            if(accounts.get(position).getACCOUNTS_type().equalsIgnoreCase(Utils.Cashtype.debit+""))
            {

                holder.txtdebit.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));

                holder.txtcredit.setText("");

            }
            else {

                holder.txtdebit.setText("");

                holder.txtcredit.setText(accounts.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));

            }




        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
