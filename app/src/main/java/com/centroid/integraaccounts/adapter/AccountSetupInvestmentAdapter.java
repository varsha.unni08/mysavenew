package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.dialogs.SpinnerDialogInvestmentFragment;
import com.centroid.integraaccounts.fragments.ExpenseDialogFragment;
import com.centroid.integraaccounts.fragments.SpinnerDialogFragment;

import org.json.JSONObject;

import java.util.List;

public class AccountSetupInvestmentAdapter extends RecyclerView.Adapter<AccountSetupInvestmentAdapter.AccountSetupHolder> {

    Context context;
    List<CommonData> commonData;
    SpinnerDialogInvestmentFragment spinnerDialogFragment;

    public AccountSetupInvestmentAdapter(Context context, List<CommonData> commonData, SpinnerDialogInvestmentFragment spinnerDialogFragment) {
        this.context = context;
        this.commonData = commonData;
        this.spinnerDialogFragment=spinnerDialogFragment;
    }

    @NonNull
    @Override
    public AccountSetupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());

        View v= layoutInflater.inflate(R.layout.layout_loans,parent,false);
        return new AccountSetupHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountSetupHolder holder, int position) {

        try{
            holder.setIsRecyclable(false);
            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());

            holder.txt.setText(Utils.getCapsSentences(context,jsonObject.getString("Accountname")));



        }catch (Exception e)
        {


        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }

    public class AccountSetupHolder extends RecyclerView.ViewHolder{
        TextView txt;

        public AccountSetupHolder(@NonNull View itemView) {
            super(itemView);
            txt =itemView.findViewById(R.id.txt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    if(spinnerDialogFragment instanceof SpinnerDialogInvestmentFragment) {
//
//                        SpinnerDialogInvestmentFragment sp=(SpinnerDialogInvestmentFragment) spinnerDialogFragment;


                        spinnerDialogFragment.onClickAccountSetup(commonData.get(getAdapterPosition()));

                   // }

                }
            });

            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    if(spinnerDialogFragment instanceof SpinnerDialogFragment) {
//
//                        SpinnerDialogFragment sp=(SpinnerDialogFragment) spinnerDialogFragment;


                        spinnerDialogFragment.onClickAccountSetup(commonData.get(getAdapterPosition()));

//                    }
//                    else {
//
//                        ExpenseDialogFragment sp=(ExpenseDialogFragment) spinnerDialogFragment;
//
//
//                        sp.onClickAccountSetup(commonData.get(getAdapterPosition()));
//
//                    }
                }
            });
        }
    }




}
