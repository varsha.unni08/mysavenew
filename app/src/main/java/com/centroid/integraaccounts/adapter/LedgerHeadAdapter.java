package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.IncExpListActivity;
import com.centroid.integraaccounts.views.IncomexpenditureActivity;

import org.json.JSONObject;

import java.util.List;

public class LedgerHeadAdapter extends RecyclerView.Adapter<LedgerHeadAdapter.IncomeLedgerHolder> {



    Context context;
    List<LedgerAccount>accounts;

    String month="",year="";

    public LedgerHeadAdapter(Context context, List<LedgerAccount> accounts,String month,String year) {
        this.context = context;
        this.accounts = accounts;
        this.month=month;
        this.year=year;
    }

    public class IncomeLedgerHolder extends RecyclerView.ViewHolder{

        TextView txtAccount,txtClosingbalance,txtaction;


        public IncomeLedgerHolder(@NonNull View itemView) {
            super(itemView);
            txtaction=itemView.findViewById(R.id.txtaction);
            txtClosingbalance=itemView.findViewById(R.id.txtClosingbalance);
            txtAccount=itemView.findViewById(R.id.txtAccount);

            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, IncExpListActivity.class);
                    intent.putExtra("accountsetupid",accounts.get(getAdapterPosition()).getAccountheadid());
                    intent.putExtra("month",month);
                    intent.putExtra("close_balance",accounts.get(getAdapterPosition()).getClosingbalance());

                    intent.putExtra("year",year);

                  //  intent.putExtra("accountsetupid", ledgerAccounts.get(getAdapterPosition()).getAccountheadid());
                    intent.putExtra("enddate", "");
                    //intent.putExtra("close_balance", ledgerAccounts.get(getAdapterPosition()).getClosingbalance());
                    intent.putExtra("fromledger", 1);
                    intent.putExtra("startdate", "");
                    intent.putExtra("closebalacebeforedate", accounts.get(getAdapterPosition()).getClosingbalancebeforedate());
                    intent.putExtra("debitcredit", accounts.get(getAdapterPosition()).getDebitcreditopening());
                    intent.putExtra("bydate", 0);
                    context.startActivity(intent);

                }
            });
        }
    }


    @NonNull
    @Override
    public IncomeLedgerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutincomeledger,parent,false);





        return new IncomeLedgerHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IncomeLedgerHolder holder, int position) {
        holder.setIsRecyclable(false);
        try {


            double d=Double.parseDouble(accounts.get(position).getClosingbalance());

            if(d<0||String.valueOf(d).contains("-"))
            {
                d=d*-1;
            }




            holder.txtClosingbalance.setText(d + " " + context.getString(R.string.rs));


            if (!accounts.get(position).getAccountheadid().equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(context).getAccountSettingsByID(accounts.get(position).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    holder.txtAccount.setText(jsonObject.getString("Accountname"));


                }
            } else {

                holder.txtAccount.setText("Cash");
            }


        }catch (Exception e)
        {

        }
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }
}
