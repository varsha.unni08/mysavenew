package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.AccountSettingsListActivity;
import com.centroid.integraaccounts.views.BankVoucherActivity;
import com.centroid.integraaccounts.views.BillVoucherListActivity;
import com.centroid.integraaccounts.views.BudgetListActivity;
import com.centroid.integraaccounts.views.JournalVoucherActivity;
import com.centroid.integraaccounts.views.JournalvoucherListActivity;
import com.centroid.integraaccounts.views.LedgerActivity;
import com.centroid.integraaccounts.views.PaymentVoucherActivity;
import com.centroid.integraaccounts.views.ReciptListActivity;
import com.centroid.integraaccounts.views.WalletListActivity;

public class MyMoneyItemAdapter extends RecyclerView.Adapter<MyMoneyItemAdapter.MyMoneyHolder> {

    Context context;

    public MyMoneyItemAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyMoneyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

       //

        View v= LayoutInflater.from(context).inflate(R.layout.layout_mymoney,parent,false);



        return new MyMoneyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyMoneyHolder holder, int position) {

        if(!Utils.mymoneydetails[position].equalsIgnoreCase("")) {

            holder.img.setVisibility(View.VISIBLE);
            holder.txt.setVisibility(View.VISIBLE);

            holder.img.setImageResource(Utils.mymoneydatas[position]);

            String languagedata= LocaleHelper.getPersistedData(context,"en");

            Context conte = LocaleHelper.setLocale(context, languagedata);
            Resources resources = conte.getResources();

            switch (position)
            {

                case 0:
                    holder.txt.setText(resources.getString(R.string.payment));

                    break;

                case 1:
                    holder.txt.setText(resources.getString(R.string.receipt));

                    break;

                case 2:
                    holder.txt.setText(resources.getString(R.string.wallet));

                    break;


                case 3:
                    holder.txt.setText(resources.getString(R.string.budget));

                    break;

                case 4:
                    holder.txt.setText(resources.getString(R.string.bank));

                    break;

                case 5:
                    holder.txt.setText(resources.getString(R.string.journalvoucher));

                    break;


                case 9:
                    holder.txt.setText(resources.getString(R.string.billing));

                    break;

                case 10:
                    holder.txt.setText(resources.getString(R.string.cashbankstatement));

                    break;

                case 11:
                    holder.txt.setText(resources.getString(R.string.accountsettings));

                    break;
            }




        }
//        else{
//            holder.img.setVisibility(View.GONE);
//            holder.txt.setText("");
//
//
//        }



    }

    @Override
    public int getItemCount() {
        return Utils.mymoneydatas.length;
    }

    public class MyMoneyHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView txt;


        public MyMoneyHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            img=itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   switch (getAdapterPosition())
                   {

                       case 0:

                           Intent intent=new Intent(context, PaymentVoucherActivity.class);
                           //intent.putExtra("data",1);
                           context.startActivity(intent);
                           break;

                       case 1:

                           Intent intent2=new Intent(context, ReciptListActivity.class);
                           // intent2.putExtra("data",2);
                           context.startActivity(intent2);
                           break;

                       case 2:

                           Intent intent3=new Intent(context, WalletListActivity.class);
                           // intent2.putExtra("data",2);
                           context.startActivity(intent3);
                           break;

                       case 3:

                           Intent intent11=new Intent(context, BudgetListActivity.class);
                           // intent5.putExtra("data",2);
                           context.startActivity(intent11);
                           break;

                       case 4:

                           Intent inten1=new Intent(context, BankVoucherActivity.class);

                           context.startActivity(inten1);
                           break;

                       case 5:

                           Intent inten6=new Intent(context, JournalvoucherListActivity.class);
//                           inten6.putExtra("Bankcash",1);
                           context.startActivity(inten6);
                           break;
                       case 9:

                           Intent inten7=new Intent(context, BillVoucherListActivity.class);
//                           inten6.putExtra("Bankcash",1);
                           context.startActivity(inten7);
                           break;

                       case 10:

                           Intent inten8=new Intent(context, LedgerActivity.class);
                           inten8.putExtra("Bankcash",1);
                           context.startActivity(inten8);
                           break;

                       case 11:

                           Intent inten9=new Intent(context, AccountSettingsListActivity.class);
//                           inten9.putExtra("Bankcash",1);
                           context.startActivity(inten9);
                           break;


                   }




                }
            });
        }
    }

}
