package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.Slider;
import com.centroid.integraaccounts.data.domain.SliderImage;
import com.centroid.integraaccounts.views.ProfileActivity;

import java.util.List;

public class NetWorkSliderAdapter extends PagerAdapter {

    Context context;
    int sliderImages[];

    public NetWorkSliderAdapter(Context context, int sliderImages[]) {
        this.context = context;
        this.sliderImages = sliderImages;
    }

    @Override
    public int getCount() {
        return sliderImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return o==view;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {


        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_slider,
                container, false);

        ImageView img=layout.findViewById(R.id.img);

//        Log.e("Image",Utils.sliderimageurl+sliderImages.get(position).getImagepath());

//        Glide.with(context).load(Utils.sliderimageurl+sliderImages.get(position).getImagepath()).into(img);

        img.setImageResource(sliderImages[position]);




        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
