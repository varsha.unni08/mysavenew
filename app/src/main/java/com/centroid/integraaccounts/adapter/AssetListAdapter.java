package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.AddAssetActivity;
import com.centroid.integraaccounts.views.AssetListActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class AssetListAdapter extends RecyclerView.Adapter<AssetListAdapter.AssetHolder> {

    Context context;
    List<CommonData>commonData;

    public AssetListAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class AssetHolder extends RecyclerView.ViewHolder{

        TextView txtmonth,txtamount,txtlictitle,txtph,txtdot2,txtdot1;
        Button btndelete,btnEdit;


        public AssetHolder(@NonNull View itemView) {
            super(itemView);

            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtlictitle=itemView.findViewById(R.id.txtlictitle);
            txtph=itemView.findViewById(R.id.txtph);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);

            txtdot2=itemView.findViewById(R.id.txtdot2);
            txtdot1=itemView.findViewById(R.id.txtdot1);

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    String languagedata = LocaleHelper.getPersistedData(context, "en");
                    Context conte= LocaleHelper.setLocale(context, languagedata);

                    Resources resources=conte.getResources();

                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage(resources.getString(R.string.deleteconfirm));
                    builder.setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            int p=getAdapterPosition();

                            CommonData cm=commonData.get(p);

                            try{

                                JSONObject jsonObject=new JSONObject(cm.getData());
                                JSONArray jsonArray=jsonObject.getJSONArray("remind_date");

                                for (int j=0;j<jsonArray.length();j++)
                                {
                                    JSONObject jsonObject1=jsonArray.getJSONObject(j);

                                    String id=jsonObject1.getString("Task");

                                    new DatabaseHelper(context).deleteData(id,Utils.DBtables.TABLE_TASK);


                                }





                            }catch (Exception e)
                            {


                            }




                            new DatabaseHelper(context).deleteData(commonData.get(p).getId(), Utils.DBtables.TABLE_ASSET);

                            commonData.remove(p);














                            notifyItemRemoved(p);










                        }
                    });
                    builder.setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();


                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddAssetActivity.class);
                    intent.putExtra("data",commonData.get(getAdapterPosition()));
                    context.startActivity(intent);



                }
            });
        }
    }

    @NonNull
    @Override
    public AssetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);



        return new AssetHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetHolder holder, int position) {
//        holder.txtph.setVisibility(View.GONE);
//        holder.txtlictitle.setVisibility(View.GONE);
//
//        holder.txtdot1.setVisibility(View.GONE);
//        holder.txtdot2.setVisibility(View.GONE);

        try{

            holder.setIsRecyclable(false);
            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context conte= LocaleHelper.setLocale(context, languagedata);

           Resources resources=conte.getResources();

            holder.txtlictitle.setText(resources.getString(R.string.accountname));
            holder.txtph.setText(resources.getString(R.string.amount));

            holder.btnEdit.setText(resources.getString(R.string.edit));

            holder.btndelete.setText(resources.getString(R.string.delete));




//            jsonObject.put("Type",spPaymentAcc.getSelectedItem().toString());
//            jsonObject.put("monthyear",monthyear);
//            jsonObject.put("bankdata","");
//            jsonObject.put("amount",edtAmount.getText().toString());

//            jsonObject.put("name",edtassetname.getText().toString());
//            jsonObject.put("amount",edtassetamount.getText().toString());
//            jsonObject.put("purchase_date",purchasedate);
//            jsonObject.put("remind_date",remind_date);
//            jsonObject.put("remarks",edtremarks.getText().toString());

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());



                String assetid=jsonObject.getString("name");

            List<CommonData> data=new DatabaseHelper(context).getDataByID(assetid, Utils.DBtables.TABLE_ACCOUNTSETTINGS);

            if(data.size()>0)
            {

                JSONObject jso=new JSONObject(data.get(0).getData());


                holder.txtmonth.setText(jso.getString("Accountname"));

                // holder.txtvoucher.setText();

            }






            holder.txtamount.setText(jsonObject.getString("amount")+" "+conte.getString(R.string.rs));


        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
