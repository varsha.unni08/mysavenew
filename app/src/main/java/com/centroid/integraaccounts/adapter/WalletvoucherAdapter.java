package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.WalletActivity;
import com.centroid.integraaccounts.views.WalletListActivity;


import org.json.JSONObject;

import java.util.List;

public class WalletvoucherAdapter extends RecyclerView.Adapter<WalletvoucherAdapter.WalletVoucherHolder> {


    Context context;
    List<Accounts>paymentVouchers;
    double totalamount;

    public WalletvoucherAdapter(Context context, List<Accounts> accounts,double totalamount) {
        this.context = context;
        this.paymentVouchers = accounts;
        this.totalamount=totalamount;
    }

    public class WalletVoucherHolder extends RecyclerView.ViewHolder{

        TextView txtdate,txtaccount,txtamount,txttype;


        public WalletVoucherHolder(@NonNull View itemView) {
            super(itemView);

            txtamount=itemView.findViewById(R.id.txtamount);
            txtaccount=itemView.findViewById(R.id.txtaccount);
            txtdate=itemView.findViewById(R.id.txtdate);
            txttype=itemView.findViewById(R.id.txttype);

            txttype.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                  Intent i = new Intent(context, WalletActivity.class);
                  i.putExtra("data", paymentVouchers.get(getAdapterPosition()));
                    if(!paymentVouchers.get(getAdapterPosition()).getACCOUNTS_setupid().equalsIgnoreCase("0")) {
                        i.putExtra("paymentvoucher", 1);
                    }
                    else if(paymentVouchers.get(getAdapterPosition()).getACCOUNTS_vouchertype()==Utils.VoucherType.wallet)
                    {
                        i.putExtra("paymentvoucher", 0);
                    }
                  i.putExtra("balance", totalamount);


                  context.startActivity(i);


                }
            });
        }


    }

    @NonNull
    @Override
    public WalletVoucherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_walletvoucher,parent,false);



        return new WalletVoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WalletVoucherHolder holder, int position) {

        try {
            holder.setIsRecyclable(false);

            String languagedata = LocaleHelper.getPersistedData(context, "en");
            Context cont= LocaleHelper.setLocale(context, languagedata);

          Resources resources=cont.getResources();

            holder.txttype.setText(resources.getString(R.string.edit)+"/"+resources.getString(R.string.delete));

            if(paymentVouchers.get(position).getACCOUNTS_vouchertype()==Utils.VoucherType.wallet)
            {



                holder.txtaccount.setText("Money Added To Wallet");

                String d[]=paymentVouchers.get(position).getACCOUNTS_date().split("-");


                holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
                holder.txtamount.setText("+ "+paymentVouchers.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));


            }













          else  if (!paymentVouchers.get(position).getACCOUNTS_setupid().equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(context).getDataByID(paymentVouchers.get(position).getACCOUNTS_setupid(), Utils.DBtables.TABLE_ACCOUNTSETTINGS);

                if (commonData.size() > 0) {

                    JSONObject jso = new JSONObject(commonData.get(0).getData());


                    holder.txtaccount.setText(Utils.getCapsSentences(context,jso.getString("Accountname")));

                }

                String d[]=paymentVouchers.get(position).getACCOUNTS_date().split("-");


                holder.txtdate.setText(d[0]+"/"+d[1]+"/"+d[2]);
                holder.txtamount.setText(" - "+paymentVouchers.get(position).getACCOUNTS_amount()+" "+context.getString(R.string.rs));


            }



        }catch (Exception e)
        {

        }
    }

    @Override
    public int getItemCount() {
        return paymentVouchers.size();
    }
}
