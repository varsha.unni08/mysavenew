package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.dialogs.TargetFragment;
import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;
import com.centroid.integraaccounts.views.AssetLedgerActivity;
import com.centroid.integraaccounts.views.BillRegisterActivity;
import com.centroid.integraaccounts.views.IncomeExpActivity;
import com.centroid.integraaccounts.views.InsuranceLedgerActivity;
import com.centroid.integraaccounts.views.InvestmentLedgerActivity;
import com.centroid.integraaccounts.views.LedgerActivity;
import com.centroid.integraaccounts.views.LiabilityLedgerActivity;
import com.centroid.integraaccounts.views.RemindsActivity;
import com.centroid.integraaccounts.views.TransactionsActivity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

public class TargetCategoryAdapter extends RecyclerView.Adapter<TargetCategoryAdapter.TargetCategoryHolder> {


    Context context;
    List<TargetCategory> tfc;
    TargetFragment tfg;

    public TargetCategoryAdapter(Context context, List<TargetCategory> tfc, TargetFragment tfg) {
        this.context = context;
        this.tfc = tfc;
        this.tfg=tfg;
    }

    public class TargetCategoryHolder extends RecyclerView.ViewHolder{

        ImageView imgtarget;

        TextView txtHead;


        public TargetCategoryHolder(@NonNull View itemView) {
            super(itemView);
            txtHead=itemView.findViewById(R.id.txtHead);

            imgtarget=itemView.findViewById(R.id.imgtarget);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(tfg!=null)
                    {

                        tfg.OnCategorySelected(tfc.get(getAdapterPosition()));
                    }




                }
            });


        }

    }

    @NonNull
    @Override
    public TargetCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_targetcategory,parent,false);


        return new TargetCategoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TargetCategoryHolder holder, int position) {


        byte[] b=tfc.get(position).getImage();

        if(b!=null) {

            try {



                Drawable image = new BitmapDrawable(context.getResources(), BitmapFactory.decodeByteArray(b, 0, b.length));





                holder.imgtarget.setImageDrawable(image);
            }catch (Exception e)
            {

            }


        }

        holder.txtHead.setText(tfc.get(position).getTask_category());



    }

    @Override
    public int getItemCount() {
        return tfc.size();
    }
}
