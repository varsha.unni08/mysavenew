package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.Accounts;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.views.AccountsettingsActivity;
import com.centroid.integraaccounts.views.LoginActivity;

import org.json.JSONObject;

import java.util.List;

public class AccountSettingsAdapter extends RecyclerView.Adapter<AccountSettingsAdapter.AccountsettingHolder> {

    Context context;
    List<CommonData>commonData;

    public AccountSettingsAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }


    public class AccountsettingHolder extends RecyclerView.ViewHolder{

        TextView txtaccname,txtcategory,txtopening,txttype,txtYear;
        Button btndelete,btnedit;

        LinearLayout layout_year;

        public AccountsettingHolder(@NonNull View itemView) {
            super(itemView);

            txttype=itemView.findViewById(R.id.txttype);
            txtaccname=itemView.findViewById(R.id.txtaccname);
            txtcategory=itemView.findViewById(R.id.txtcategory);
            txtopening=itemView.findViewById(R.id.txtopening);
            btndelete=itemView.findViewById(R.id.btndelete);
            btnedit=itemView.findViewById(R.id.btnedit);
            layout_year=itemView.findViewById(R.id.layout_year);
            txtYear=itemView.findViewById(R.id.txtYear);


            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AccountsettingsActivity.class);
                    intent.putExtra("Commondata",commonData.get(getAdapterPosition()));
                    context.startActivity(intent);


                }
            });






            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage("Do You Want To Delete Now ? ");
                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            int p=getAdapterPosition();

                            new DatabaseHelper(context).deleteAccountsettingData(commonData.get(p).getId());

                            Utils.playSimpleTone(context);



                            commonData.remove(p);

                            notifyItemRemoved(p);


                        }
                    });
                    builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();











                }
            });
        }
    }


    @NonNull
    @Override
    public AccountsettingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_accsettings,parent,false);


        return new AccountsettingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountsettingHolder holder, int position) {

        try{

            holder.setIsRecyclable(false);




            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());


            holder.txtaccname.setText(Utils.getCapsSentences(context,jsonObject.getString("Accountname")));
            holder.txtcategory.setText(Utils.getCapsSentences(context,jsonObject.getString("Accounttype")));
            holder.txtopening.setText(Utils.getCapsSentences(context,jsonObject.getString("Amount")));
            holder.txttype.setText(Utils.getCapsSentences(context,jsonObject.getString("Type")));

            if(jsonObject.has("year"))
            {
                holder.layout_year.setVisibility(View.VISIBLE);
                holder.txtYear.setText(jsonObject.getString("year"));
            }
            else {
                holder.layout_year.setVisibility(View.GONE);
            }


            List<Accounts>accounts=new DatabaseHelper(context).getAccountsDataBYsetupid(commonData.get(position).getId());

            if(accounts.size()>0)
            {
                holder.btndelete.setVisibility(View.GONE);
            }






        }catch (Exception e)
        {

            Log.e("TAGGG",e.toString());
        }




    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
