package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.DiaryActivity;
import com.centroid.integraaccounts.views.DocumentManagerActivity;
import com.centroid.integraaccounts.views.ReciptListActivity;
import com.centroid.integraaccounts.views.SavedPasswordActivity;
import com.centroid.integraaccounts.views.TargetActivity;
import com.centroid.integraaccounts.views.TasksActivity;
import com.centroid.integraaccounts.views.VisitCardHistoryActivity;
import com.google.android.gms.tasks.Task;

public class MyLifeHomeAdapter extends RecyclerView.Adapter<MyLifeHomeAdapter.MyLifeHolder> {


    Context context;

    public MyLifeHomeAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyLifeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_mymoney,parent,false);

        return new MyLifeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyLifeHolder holder, int position) {

        holder.img.setVisibility(View.VISIBLE);
        holder.txt.setVisibility(View.VISIBLE);
        holder.img.setImageResource(Utils.mylifesimg[position]);
//        holder.txt.setText(Utils.mylifes[position]);


        String languagedata= LocaleHelper.getPersistedData(context,"en");

        Context conte = LocaleHelper.setLocale(context, languagedata);
        Resources resources = conte.getResources();

        switch (position)
        {

            case 0:
                holder.txt.setText(resources.getString(R.string.task));

                break;

            case 1:
                holder.txt.setText(resources.getString(R.string.diary));

                break;

            case 2:
                holder.txt.setText(resources.getString(R.string.mydream));

                break;






        }



    }

    @Override
    public int getItemCount() {
        return Utils.mylifes.length;
    }

    public class MyLifeHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView txt;

        public MyLifeHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            img=itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition())
                    {

                        case 0:
                            Intent intent4=new Intent(context, TasksActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent4);

                            break;

                        case 1:
                            Intent intent2=new Intent(context, DiaryActivity.class);
                            // intent2.putExtra("data",2);
                            context.startActivity(intent2);

                            break;


                        case 2:
                            Intent intent5=new Intent(context, TargetActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent5);

                            break;


                        case 3:
                            Intent intent7=new Intent(context, VisitCardHistoryActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent7);

                            break;
                    }

                }
            });
        }
    }



}
