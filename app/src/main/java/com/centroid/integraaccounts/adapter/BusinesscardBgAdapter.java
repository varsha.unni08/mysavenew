package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.VisitCardImg;
import com.centroid.integraaccounts.views.VisitCardFillActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class BusinesscardBgAdapter extends PagerAdapter {

    Context context;
    List<Drawable>imgid;
    List<VisitCardImg>vcardimg;

    public BusinesscardBgAdapter(Context context, List<Drawable> imgid,List<VisitCardImg>vcardimg) {
        this.context = context;
        this.imgid = imgid;
        this.vcardimg=vcardimg;
    }

    @Override
    public int getCount() {
        return imgid.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {



        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_businesscard,
                container, false);

        ImageView img=layout.findViewById(R.id.img);
        FloatingActionButton fab_camera=layout.findViewById(R.id.fab_camera);


        fab_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((VisitCardFillActivity)context).changeBackground(position,vcardimg.get(position).getId());
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;

        double height = width/1.63;

        int h=(int)height;

        FrameLayout.LayoutParams layoutParams=new FrameLayout.LayoutParams(width,h);
        img.setLayoutParams(layoutParams);

        Drawable a=imgid.get(position);
        img.setImageDrawable(a);

        container.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
