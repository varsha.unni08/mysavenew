package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.SliderShareImage;

import java.util.List;

public class ShareSliderAdapter extends PagerAdapter {

    Context context;
    List<SliderShareImage>sliderShareImages;

    public ShareSliderAdapter(Context context, List<SliderShareImage> sliderShareImages) {
        this.context = context;
        this.sliderShareImages = sliderShareImages;
    }

    @Override
    public int getCount() {
        return sliderShareImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_slider,
                container, false);

        ImageView img=layout.findViewById(R.id.img);

        Log.e("Image", Utils.sliderimageurl+sliderShareImages.get(position).getImage());

        Glide.with(context).load(Utils.sliderimageurl+sliderShareImages.get(position).getImage()).into(img);
        //1.6


        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
