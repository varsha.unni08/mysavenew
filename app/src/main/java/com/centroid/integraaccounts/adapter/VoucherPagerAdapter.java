package com.centroid.integraaccounts.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.centroid.integraaccounts.fragments.SetBudgetFragment;
import com.centroid.integraaccounts.fragments.VoucherFragment;

import java.util.List;

public class VoucherPagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Voucher","Set budget"};
    List<Fragment>fragments;

    public VoucherPagerAdapter(@NonNull FragmentManager fm, List<Fragment>fragments) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragments=fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment=null;

        if(position==0)
        {
            fragment=new VoucherFragment();
        }
        else {
            fragment=new SetBudgetFragment();
        }



        return fragment;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
