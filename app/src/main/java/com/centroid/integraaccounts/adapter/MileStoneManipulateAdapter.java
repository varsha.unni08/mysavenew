package com.centroid.integraaccounts.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.dialogs.MileStoneListFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.views.MileStoneAddedActivity;
import com.centroid.integraaccounts.views.MileStoneEditActivity;
import com.google.api.client.util.Data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MileStoneManipulateAdapter extends RecyclerView.Adapter<MileStoneManipulateAdapter.MileStoneManipulateHolder> {

    Context context;
    List<MileStone> mileStones=new ArrayList<>();


    public MileStoneManipulateAdapter(Context context, List<MileStone> mileStones) {
        this.context = context;
        this.mileStones = mileStones;

    }

    public class MileStoneManipulateHolder extends RecyclerView.ViewHolder{



        TextView txtstartdate,txtenddate,txtAmount;

        ImageView imgdropdown;
        Button btnsubmit,btndelete;



        public MileStoneManipulateHolder(@NonNull View itemView) {
            super(itemView);



            txtstartdate=itemView.findViewById(R.id.txtstartdate);
            txtenddate=itemView.findViewById(R.id.txtenddate);
            txtAmount=itemView.findViewById(R.id.txtAmount);




            imgdropdown=itemView.findViewById(R.id.imgdropdown);
            btndelete=itemView.findViewById(R.id.btndelete);

            btnsubmit=itemView.findViewById(R.id.btnsubmit);

            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ((MileStoneEditActivity)context).setupDataForEditing(getAdapterPosition());



                }
            });


            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   Utils.showAlert(context, "Do You Want to Delete Now ?", new DialogEventListener() {
                       @Override
                       public void onPositiveButtonClicked() {

                           int p=getAdapterPosition();

                           new DatabaseHelper(context).deleteData(mileStones.get(p).getId(), Utils.DBtables.TABLE_MILESTONE);

                           mileStones.remove(p);
                       notifyItemRemoved(p);





                       }

                       @Override
                       public void onNegativeButtonClicked() {

                       }
                   });

                }
            });

            imgdropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(context, MileStoneAddedActivity.class);
                    i.putExtra("mileStone",mileStones.get(getAdapterPosition()));


                    context.startActivity(i);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i=new Intent(context, MileStoneAddedActivity.class);
                    i.putExtra("mileStone",mileStones.get(getAdapterPosition()));


                    context.startActivity(i);

                  //  mileStoneListFragment.setupDataForEditing(getAdapterPosition());

                }
            });












        }
    }


    @NonNull
    @Override
    public MileStoneManipulateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_milestone_data,parent,false);

        return new MileStoneManipulateHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MileStoneManipulateHolder holder, int position) {



        holder.txtAmount.setText(mileStones.get(position).getAmount());

        holder.txtstartdate.setText(mileStones.get(position).getStart_date());

        holder.txtenddate.setText(mileStones.get(position).getEnd_date());











    }

    @Override
    public int getItemCount() {
        return mileStones.size();
    }



}
