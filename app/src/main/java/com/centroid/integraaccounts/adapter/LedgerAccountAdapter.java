package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.data.domain.LedgerAccount;
import com.centroid.integraaccounts.views.BankDetailsActivity;
import com.centroid.integraaccounts.views.IncExpListActivity;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;

public class LedgerAccountAdapter extends RecyclerView.Adapter<LedgerAccountAdapter.LedgerHolder> {



    Context context;
    List<LedgerAccount>ledgerAccounts;
    String startdate="",enddate="";

    public LedgerAccountAdapter(Context context, List<LedgerAccount> ledgerAccounts,String startdate,String enddate) {
        this.context = context;
        this.ledgerAccounts = ledgerAccounts;
        this.startdate=startdate;
        this.enddate=enddate;
    }

    public class LedgerHolder extends RecyclerView.ViewHolder{

        TextView txtAccount,txtClosingbalance,txtaction,txtdebitcredit;



        public LedgerHolder(@NonNull View itemView) {
            super(itemView);

            txtaction=itemView.findViewById(R.id.txtaction);
            txtClosingbalance=itemView.findViewById(R.id.txtClosingbalance);
            txtAccount=itemView.findViewById(R.id.txtAccount);
            txtdebitcredit=itemView.findViewById(R.id.txtdebitcredit);


            txtaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        String Type="";

                        String acchid = ledgerAccounts.get(getAdapterPosition()).getAccountheadid();

                        List<CommonData> commonData = new DatabaseHelper(context).getAccountSettingsByID(ledgerAccounts.get(getAdapterPosition()).getAccountheadid());

                        if (commonData.size() > 0) {

                            CommonData cm = commonData.get(0);

                            JSONObject jsonObject = new JSONObject(cm.getData());

                            Type=  jsonObject.getString("Accounttype");


                        }


                        //if(!Type.equalsIgnoreCase("Bank")) {

                            Intent intent = new Intent(context, IncExpListActivity.class);
                            intent.putExtra("accountsetupid", ledgerAccounts.get(getAdapterPosition()).getAccountheadid());
                            intent.putExtra("enddate", enddate);
                            intent.putExtra("close_balance", ledgerAccounts.get(getAdapterPosition()).getClosingbalance());
                            intent.putExtra("fromledger", 1);
                            intent.putExtra("startdate", startdate);
                            intent.putExtra("closebalacebeforedate", ledgerAccounts.get(getAdapterPosition()).getClosingbalancebeforedate());
                            intent.putExtra("debitcredit", ledgerAccounts.get(getAdapterPosition()).getDebitcreditopening());
                            intent.putExtra("bydate", 1);
                            context.startActivity(intent);
//                        }
//                        else {
//
//                            Intent intent = new Intent(context, BankDetailsActivity.class);
//                            intent.putExtra("accountsetupid", ledgerAccounts.get(getAdapterPosition()).getAccountheadid());
//                            intent.putExtra("enddate", enddate);
//                            intent.putExtra("close_balance", ledgerAccounts.get(getAdapterPosition()).getClosingbalance());
//                            intent.putExtra("fromledger", 1);
//                            intent.putExtra("startdate", startdate);
//                            intent.putExtra("closebalacebeforedate", ledgerAccounts.get(getAdapterPosition()).getClosingbalancebeforedate());
//                            intent.putExtra("debitcredit", ledgerAccounts.get(getAdapterPosition()).getDebitcreditopening());
//                            intent.putExtra("bydate", 1);
//                            context.startActivity(intent);
//
//                        }

                    }catch (Exception e)
                    {

                    }
                }
            });

        }
    }


    @NonNull
    @Override
    public LedgerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutincomeledger,parent,false);



        return new LedgerHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LedgerHolder holder, int position) {
        holder.setIsRecyclable(false);

        try {


            holder.txtdebitcredit.setVisibility(View.VISIBLE);


            double d=Double.parseDouble(ledgerAccounts.get(position).getClosingbalance());

            if(d<0||String.valueOf(d).contains("-"))
            {
                d=d*-1;
            }

            DecimalFormat decimalFormatter = new DecimalFormat("###################################################");


            if(ledgerAccounts.get(position).getDebitorcredit().equalsIgnoreCase(Utils.Cashtype.debit+"")) {

                holder.txtClosingbalance.setText(decimalFormatter.format(d) + " " + context.getString(R.string.rs));
            }
            else {

                holder.txtdebitcredit.setText(decimalFormatter.format(d)  + " " + context.getString(R.string.rs));
            }




            if (!ledgerAccounts.get(position).getAccountheadid().equalsIgnoreCase("0")) {

                List<CommonData> commonData = new DatabaseHelper(context).getAccountSettingsByID(ledgerAccounts.get(position).getAccountheadid());

                if (commonData.size() > 0) {

                    CommonData cm = commonData.get(0);

                    JSONObject jsonObject=new JSONObject(cm.getData());

                    holder.txtAccount.setText(Utils.getCapsSentences(context,jsonObject.getString("Accountname")));


                }
            } else {

                holder.txtAccount.setText("Cash");
            }


//            if(ledgerAccounts.get(position).getDebitorcredit().equalsIgnoreCase(Utils.Cashtype.debit+""))
//            {
//
//                holder.txtdebitcredit.setText("Debit");
//            }
//            else {
//
//                holder.txtdebitcredit.setText("Credit");
//
//            }



        }catch (Exception e)
        {

        }

    }

    @Override
    public int getItemCount() {
        return ledgerAccounts.size();
    }
}
