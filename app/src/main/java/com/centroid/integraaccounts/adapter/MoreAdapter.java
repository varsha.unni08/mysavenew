package com.centroid.integraaccounts.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.NetworkDashboard;
import com.centroid.integraaccounts.data.domain.NetworkData;
import com.centroid.integraaccounts.data.domain.Profiledata;
import com.centroid.integraaccounts.data.domain.SliderShareImage;
import com.centroid.integraaccounts.fragments.MoreFragment;
import com.centroid.integraaccounts.fragments.NetworkFragment;
import com.centroid.integraaccounts.interfaces.DialogEventListener;
import com.centroid.integraaccounts.preferencehelper.PreferenceHelper;
import com.centroid.integraaccounts.progress.ProgressFragment;
import com.centroid.integraaccounts.views.AddBankCashDataActivity;
import com.centroid.integraaccounts.views.AddBudgetActivity;
import com.centroid.integraaccounts.views.DescriptionActivity;
import com.centroid.integraaccounts.views.FeedbackMessageActivity;
import com.centroid.integraaccounts.views.LoginActivity;
import com.centroid.integraaccounts.views.MainActivity;
import com.centroid.integraaccounts.views.OTPActivity;
import com.centroid.integraaccounts.views.ProfileActivity;
import com.centroid.integraaccounts.webserviceHelper.RequestHandler;
import com.centroid.integraaccounts.webserviceHelper.ResponseHandler;
import com.centroid.integraaccounts.webserviceHelper.RestService;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreAdapter extends RecyclerView.Adapter<MoreAdapter.MoreHolder> {

    int arr[]={R.string.howtouse,R.string.helponwhatsapp,R.string.mailus,R.string.aboutus,R.string.privacypolicy,R.string.termsandconditions,R.string.feedback};

    //R.string.referearn

    Context context;
    MoreFragment moreFragment;

    public MoreAdapter(Context context, MoreFragment moreFragment) {
        this.context=context;
        this.moreFragment=moreFragment;

    }

    public class MoreHolder extends RecyclerView.ViewHolder{

        TextView txt;


        public MoreHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    switch (getAdapterPosition()+1)
                    {



                        case 1 :

                            Intent intent=new Intent(context, DescriptionActivity.class);
                            intent.putExtra(Utils.Contents.Content, Utils.Contents.howtouse);

                            context.startActivity(intent);



                            break;

                        case 2:

                            String url="https://api.whatsapp.com/send?phone="+Utils.whatsapp;
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            context.startActivity(i);
                            break;

                        case 3:

                            Intent email = new Intent(Intent.ACTION_SEND);
                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "mail@integrasoftware.in"});
                            email.putExtra(Intent.EXTRA_SUBJECT, "");
                            email.putExtra(Intent.EXTRA_TEXT, "");
                            email.setType("message/rfc822");
                            context.startActivity(Intent.createChooser(email, "Send email"));
                            break;

                        case 6:


                            Intent browse = new Intent(Intent. ACTION_VIEW, Uri. parse(Utils.domain+"index.php/web/memTerms1"));
                            context.startActivity(browse);



                            break;


                        case 5:

                            String privacypolicy=Utils.domain+"index.php/web/memTerms2";

                            Intent browse1 = new Intent(Intent. ACTION_VIEW, Uri. parse(privacypolicy));
                            context.startActivity(browse1);


//                            Intent intent11=new Intent(context, DescriptionActivity.class);
//                            intent11.putExtra(Utils.Contents.Content, Utils.Contents.privacypolicy);
//
//                            context.startActivity(intent11);


                            break;

                        case 4:

                            String aboutus=Utils.domain+"index.php/web/about";

                            Intent browse11 = new Intent(Intent. ACTION_VIEW, Uri. parse(aboutus));
                            context.startActivity(browse11);

//                            Intent intent5=new Intent(context, DescriptionActivity.class);
//                            intent5.putExtra(Utils.Contents.Content, Utils.Contents.aboutus);
//
//                            context.startActivity(intent5);


                            break;

                        case 7:

                            Intent intent7=new Intent(context, FeedbackMessageActivity.class);
                            //intent5.putExtra(Utils.Contents.Content, Utils.Contents.aboutus);

                            context.startActivity(intent7);




                            break;






//                        case 8:
//
//                            final ProgressFragment progressFragment=new ProgressFragment();
//                            progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"fkjfk");
//
//
//                            Map<String,String> params=new HashMap<>();
//                             params.put("timestamp",Utils.getTimestamp());
//
//                            new RequestHandler(context, params, new ResponseHandler() {
//                                @Override
//                                public void onSuccess(String data) {
//                                    progressFragment.dismiss();
//                                    // Toast.makeText(LoginActivity.this,data,Toast.LENGTH_SHORT).show();
//                                    Profiledata profiledata=new GsonBuilder().create().fromJson(data,Profiledata.class);
//
//
//                                    progressFragment.dismiss();
//                                    if(profiledata!=null)
//                                    {
//
//                                        try{
//
//
//
//                                            if(profiledata.getStatus()==1)
//                                            {
//
//
//                                                if(profiledata.getData()!=null)
//                                                {
//
//
//                                                    String phonenumber=profiledata.getData().getMobile();
//                                                    getNetWorkData(phonenumber,context);
//
//                                                    // getCurrencyCode();
//
//                                                }
//
//
//
//
//
//                                            }
//                                            else {
//
//                                                //   Toast.makeText(getActivity()," failed",Toast.LENGTH_SHORT).show();
//
//                                                Utils.showAlertWithSingle(context, "failed", new DialogEventListener() {
//                                                    @Override
//                                                    public void onPositiveButtonClicked() {
//
//                                                    }
//
//                                                    @Override
//                                                    public void onNegativeButtonClicked() {
//
//                                                    }
//                                                });
//
//
//                                            }
//
//
//
//                                        }catch (Exception e)
//                                        {
//
//                                        }
//
//
//
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(String err) {
//                                    progressFragment.dismiss();
//
//                                    //   Toast.makeText(getActivity(),err,Toast.LENGTH_SHORT).show();
//
//                                    Utils.showAlertWithSingle(context, err, new DialogEventListener() {
//                                        @Override
//                                        public void onPositiveButtonClicked() {
//
//                                        }
//
//                                        @Override
//                                        public void onNegativeButtonClicked() {
//
//                                        }
//                                    });
//
//
//                                }
//                            },Utils.WebServiceMethodes.getUserDetails, Request.Method.POST).submitRequest();
//
//
//
//                            break;



                    }


                }
            });
        }
    }


    @NonNull
    @Override
    public MoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_moreadapter,parent,false);

        return new MoreHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreHolder holder, int position) {
        holder.setIsRecyclable(false);
        String languagedata = LocaleHelper.getPersistedData(context, "en");
        Context contex= LocaleHelper.setLocale(context, languagedata);

      Resources resources=contex.getResources();

      //  holder.txt.setText(resources.getString(arr[position]));

        holder.txt.setText( Utils.getCapsSentences(context,resources.getString(arr[position]))  );

    }

    @Override
    public int getItemCount() {
        return arr.length;
    }




    public void getNetWorkData(String phonenumber,Context ctx) {







        Map<String, String> params = new HashMap<>();
        new RequestHandler(ctx, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {


                if (data != null) {

                    if (data != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(data);

                            if (jsonObject.getInt("status") == 1) {

                                JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                NetworkData  networkDashboard=new GsonBuilder().create().fromJson(jsonObject1.toString(),NetworkData.class);


                                showReferDialog(networkDashboard);


                            } else {

                                Utils.showAlert(context, Utils.getCapsSentences(context,"You are not purchased this application.do you want to purchase now ? "), new DialogEventListener() {
                                    @Override
                                    public void onPositiveButtonClicked() {


                                        moreFragment.getProfileData();

                                    }

                                    @Override
                                    public void onNegativeButtonClicked() {

                                    }
                                });
                            }


                        } catch (Exception e) {


                        }


                    }


                }

            }

            @Override
            public void onFailure(String err) {
                // progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, Utils.WebServiceMethodes.showMemberDetails + "?mobile=" + phonenumber+"&timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();
    }







        public void showReferDialog(NetworkData networkData)
    {



            ProgressFragment progressFragment=new ProgressFragment();
            progressFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"sdjfn");



            Map<String,String> params=new HashMap<>();


            new RequestHandler(context, params, new ResponseHandler() {
                @Override
                public void onSuccess(String data) {

                    progressFragment.dismiss();

                    if(data!=null)
                    {

                        try{


                            JSONObject jsonObject=new JSONObject(data);


                            //  MainSettings settingsData=new GsonBuilder().create().fromJson(data, MainSettings.class);

                            if(jsonObject.getInt("status")==1)
                            {

                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                List<SliderShareImage>sliderShareImages=new ArrayList<>();

                                for (int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    SliderShareImage sliderShare=new GsonBuilder().create().fromJson(jsonObject1.toString(),SliderShareImage.class);
                                    sliderShareImages.add(sliderShare);
                                }


                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int height = displayMetrics.heightPixels;
                                int width = displayMetrics.widthPixels;
                                final Dialog dialog = new Dialog(context);
                                dialog.setContentView(R.layout.layout_settingsshare);
                                dialog.setTitle(R.string.app_name);
                                int b=(int)(height/1);
                                dialog.getWindow().setLayout(width,height );

                                ViewPager viewPager=dialog.findViewById(R.id.viewpager);
                                TabLayout tabslayout=dialog.findViewById(R.id.tabslayout);

                                final TextView txtDescription=dialog.findViewById(R.id.txtDescription);
                                TextView txtSubDescription=dialog.findViewById(R.id.txtSubDescription);

                                Button btnShare=dialog.findViewById(R.id.btnShare);
                                Button btnpurchase=dialog.findViewById(R.id.btnpurchase);

                                Button btncopy=dialog.findViewById(R.id.btncopy);
                                TextView txtlink=dialog.findViewById(R.id.txtlink);


                                String languagedata = LocaleHelper.getPersistedData(context, "en");
                                Context contex= LocaleHelper.setLocale(context, languagedata);

                                Resources resources=contex.getResources();

                                btncopy.setText(resources.getString(R.string.copylink));



                                String id = new PreferenceHelper(context).getData(Utils.userkey);
                                final String link = Utils.newdomain + "/signup?sponserid=" + id;
                                txtlink.setText(  link );

                                btncopy.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        int sdk = android.os.Build.VERSION.SDK_INT;
                                        if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                                            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                            clipboard.setText(link);

                                            Utils.showAlertWithSingle(context, resources.getString(R.string.linkcopied), new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });
                                        } else {
                                            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                            android.content.ClipData clip = android.content.ClipData.newPlainText("text label",link);
                                            clipboard.setPrimaryClip(clip);

                                            Utils.showAlertWithSingle(context,  resources.getString(R.string.linkcopied), new DialogEventListener() {
                                                @Override
                                                public void onPositiveButtonClicked() {

                                                }

                                                @Override
                                                public void onNegativeButtonClicked() {

                                                }
                                            });
                                        }
                                    }
                                });

                                txtSubDescription.setText(resources.getString(R.string.sharelinkmsg));


                                if(!networkData.getMember_status().equalsIgnoreCase("active"))
                                {



                                    btnpurchase.setVisibility(View.VISIBLE);
                                }
                                else {
                                    // btnPurchaseFreemember.setVisibility(View.GONE);


                                }

                                btnpurchase.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        moreFragment.getProfileData();
                                    }
                                });

                                FrameLayout.LayoutParams layoutParams=(FrameLayout.LayoutParams)viewPager.getLayoutParams();

                                int w=(int)contex.getResources().getDimension(R.dimen.dimen_270dp);
                                double a=w/0.609375;
                                int h=(int)a;
                                layoutParams.width=w;
                                layoutParams.height=h;
                                viewPager.setLayoutParams(layoutParams);

                                tabslayout.removeAllTabs();

                                viewPager.setAdapter(new ShareSliderAdapter(context,sliderShareImages));

                                SliderShareImage sliderShareImg=sliderShareImages.get(0);
                                txtDescription.setText(sliderShareImg.getDescription());

                                for (SliderShareImage sliderShareImage:sliderShareImages)
                                {
                                    tabslayout.addTab(tabslayout.newTab());



                                }


                                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                    @Override
                                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                    }

                                    @Override
                                    public void onPageSelected(int position) {
                                        tabslayout.getTabAt(position).select();

                                        txtDescription.setText(sliderShareImages.get(position).getDescription());

                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int state) {

                                    }
                                });


                                tabslayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {
                                        viewPager.setCurrentItem(tab.getPosition());

                                        txtDescription.setText(sliderShareImages.get(tab.getPosition()).getDescription());

                                    }

                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });



                                btnShare.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {



                                        SliderShareImage sliderShareImage=sliderShareImages.get(viewPager.getCurrentItem());

                                        String imgurl=Utils.sliderimageurl+sliderShareImage.getImage();

                                        moreFragment.getBitmapData(imgurl,sliderShareImage.getDescription());

                                    }
                                });


                                txtDescription.setVisibility(View.GONE);

                                dialog.show();



//                        SettingsData settingsData1=settingsData.getData();
//
//                        String url=Utils.linkimg+settingsData1.getLinkImage();
//
//                        //  String  url="https://mysaving.in/images/saveicon.png";
//
//                        networkFragment.getBitmapData(url);
//
//                                        String imgurl=Utils.imgbaseurl+url;









                            }
                            else {





                            }



                        }catch (Exception e)
                        {

                        }



                    }

                }

                @Override
                public void onFailure(String err) {
                    progressFragment.dismiss();

                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                }
            },Utils.WebServiceMethodes.getSettingsSlider+"?timestamp="+Utils.getTimestamp(), Request.Method.GET).submitRequest();

        }
//        Calendar calendar = Calendar.getInstance();
//        final int year = calendar.get(Calendar.YEAR);
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        double height = displayMetrics.heightPixels / 1.2;
//
//        int h=(int)height;
//        int width = displayMetrics.widthPixels;
//
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.layout_refer);
//
//        TextView txtLink=dialog.findViewById(R.id.txtLink);
//        Button btnShare=dialog.findViewById(R.id.btnShare);
//        ImageView imageView3=dialog.findViewById(R.id.imageView3);
//
//        String id=new PreferenceHelper(context).getData(Utils.userkey);
//
//
//
//
//
//
//        final String link=Utils.domain+"index.php/web/signup?sponserid="+id;
//
//        txtLink.setText(link);
//
//
//        btnShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                dialog.dismiss();
//
////                Intent sendIntent = new Intent();
////                sendIntent.setAction(Intent.ACTION_SEND);
////                sendIntent.putExtra(Intent.EXTRA_TEXT, link);
////                sendIntent.setType("text/plain");
////                context.startActivity(sendIntent);
//
//                moreFragment.getDataFromSettings();
//
//            }
//        });
//
//
//
//
//
//
//        dialog.getWindow().setLayout(width,h);
//
//        dialog.show();











}
