package com.centroid.integraaccounts.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.views.AssetListActivity;
import com.centroid.integraaccounts.views.DocumentManagerActivity;
import com.centroid.integraaccounts.views.InsuranceActivity;
import com.centroid.integraaccounts.views.InvestmentListActivity;
import com.centroid.integraaccounts.views.LiabilitiesActivity;
import com.centroid.integraaccounts.views.PaymentVoucherActivity;
import com.centroid.integraaccounts.views.SavedPasswordActivity;

public class MyBelongingsAdapter extends RecyclerView.Adapter<MyBelongingsAdapter.MyBelongingsHolder> {


    Context context;

    public MyBelongingsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyBelongingsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_mymoney,parent,false);


        return new MyBelongingsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBelongingsHolder holder, int position) {

        holder.img.setVisibility(View.VISIBLE);
        holder.txt.setVisibility(View.VISIBLE);
        holder.img.setImageResource(Utils.mybelongingsimg[position]);
      //  holder.txt.setText(Utils.mybelongings[position]);


        String languagedata= LocaleHelper.getPersistedData(context,"en");

        Context conte = LocaleHelper.setLocale(context, languagedata);
        Resources resources = conte.getResources();

        switch (position)
        {

            case 0:
                holder.txt.setText(resources.getString(R.string.asset));

                break;

            case 1:
                holder.txt.setText(resources.getString(R.string.liability));

                break;

            case 2:
                holder.txt.setText(resources.getString(R.string.insurance));

                break;


            case 3:
                holder.txt.setText(resources.getString(R.string.investment));

                break;

            case 4:
                holder.txt.setText(resources.getString(R.string.passwordmanager));

                break;

            case 5:
                holder.txt.setText(resources.getString(R.string.documentmanager));

                break;



        }




    }

    @Override
    public int getItemCount() {
        return Utils.mybelongings.length;
    }

    public class MyBelongingsHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView txt;
        public MyBelongingsHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
            img=itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (getAdapterPosition())
                    {

                        case 0:
                            Intent intent=new Intent(context, AssetListActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent);


                            break;

                        case 1:
                            Intent intent1=new Intent(context, LiabilitiesActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent1);


                            break;

                        case 2:
                            Intent intent2=new Intent(context, InsuranceActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent2);


                            break;

                        case 3:
                            Intent intent3=new Intent(context, InvestmentListActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent3);


                            break;


                        case 4:
                            Intent intent4=new Intent(context, SavedPasswordActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent4);


                            break;

                        case 5:
                            Intent intent5=new Intent(context, DocumentManagerActivity.class);
                            //intent.putExtra("data",1);
                            context.startActivity(intent5);


                            break;



                    }
                }
            });
        }
    }


}
