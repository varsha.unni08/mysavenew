package com.centroid.integraaccounts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.DatabaseHelper;
import com.centroid.integraaccounts.data.domain.CommonData;
import com.centroid.integraaccounts.views.LoanListActivity;

import org.json.JSONObject;

import java.util.List;

public class LoanAdapter extends RecyclerView.Adapter<LoanAdapter.LoanHolder> {

    Context context;
    List<CommonData>commonData;

    public LoanAdapter(Context context, List<CommonData> commonData) {
        this.context = context;
        this.commonData = commonData;
    }

    public class LoanHolder extends RecyclerView.ViewHolder{


        TextView txtmonth,txtamount,txtlictitle,txtph,txtdot2,txtdot1;
        Button btndelete,btnEdit;

        public LoanHolder(@NonNull View itemView) {
            super(itemView);

            txtamount=itemView.findViewById(R.id.txtamount);
            txtmonth=itemView.findViewById(R.id.txtmonth);
            txtlictitle=itemView.findViewById(R.id.txtlictitle);
            txtph=itemView.findViewById(R.id.txtph);

            btndelete=itemView.findViewById(R.id.btndelete);
            btnEdit=itemView.findViewById(R.id.btnEdit);

            txtdot2=itemView.findViewById(R.id.txtdot2);
            txtdot1=itemView.findViewById(R.id.txtdot1);

            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    builder.setMessage("Do You Want To Delete Now ? ");
                    builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();

                            int p=getAdapterPosition();

                            new DatabaseHelper(context).deleteLoanData(commonData.get(p).getId());

                            commonData.remove(p);

                            notifyItemRemoved(p);

                            ((LoanListActivity)context).getLoanData();


                        }
                    });
                    builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    builder.show();


                }
            });
        }
    }


    @NonNull
    @Override
    public LoanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_budget,parent,false);



        return new LoanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LoanHolder holder, int position) {
        holder.setIsRecyclable(false);
        holder.txtph.setVisibility(View.GONE);
        holder.txtlictitle.setVisibility(View.GONE);

        holder.txtdot1.setVisibility(View.GONE);
        holder.txtdot2.setVisibility(View.GONE);
        holder.btnEdit.setVisibility(View.GONE);


        try{

            JSONObject jsonObject=new JSONObject(commonData.get(position).getData());



            holder.txtmonth.setText(jsonObject.getString("name"));








            holder.txtamount.setText(jsonObject.getString("amount")+" Rs");


        }catch (Exception e)
        {


        }





    }

    @Override
    public int getItemCount() {
        return commonData.size();
    }
}
