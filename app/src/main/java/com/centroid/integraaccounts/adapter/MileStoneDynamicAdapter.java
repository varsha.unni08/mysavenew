package com.centroid.integraaccounts.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.data.domain.MileStone;
import com.centroid.integraaccounts.views.IncomeExpActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MileStoneDynamicAdapter extends RecyclerView.Adapter<MileStoneDynamicAdapter.MileStoneHolder> {


    Context context;
    List<MileStone>mileStones;
    int forAdd;

    public MileStoneDynamicAdapter(Context context, List<MileStone>mileStones,int forAdd) {
        this.context = context;
        this.mileStones = mileStones;
        this.forAdd=forAdd;
    }

    public class MileStoneHolder extends RecyclerView.ViewHolder{

        Button btnsubmit,btndelete;

        TextView txtstartdatepick,txtenddatepick,txtMileStoneAdded,txtwarning;

        ImageView imgendDatepick,imgstartDatepick,imgdropdown;

        EditText edtNumber;

        LinearLayout layout_views;


        public MileStoneHolder(@NonNull View itemView) {
            super(itemView);
            btnsubmit=itemView.findViewById(R.id.btnsubmit);
            txtstartdatepick=itemView.findViewById(R.id.txtstartdatepick);
            txtenddatepick=itemView.findViewById(R.id.txtenddatepick);
            imgendDatepick=itemView.findViewById(R.id.imgendDatepick);
            imgstartDatepick=itemView.findViewById(R.id.imgstartDatepick);
            txtMileStoneAdded=itemView.findViewById(R.id.txtMileStoneAdded);
            imgdropdown=itemView.findViewById(R.id.imgdropdown);
            txtwarning=itemView.findViewById(R.id.txtwarning);

            edtNumber=itemView.findViewById(R.id.edtNumber);

            layout_views=itemView.findViewById(R.id.layout_views);

            btndelete=itemView.findViewById(R.id.btndelete);

            imgdropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(mileStones.get(getAdapterPosition()).getSelected()==0)
                    {

                        mileStones.get(getAdapterPosition()).setSelected(1);
                        notifyDataSetChanged();


                    }
                    else{

                        mileStones.get(getAdapterPosition()).setSelected(0);
                        notifyDataSetChanged();

                    }

                }
            });


            btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int p=getAdapterPosition();

                    mileStones.remove(p);

                    notifyItemRemoved(p);
                }
            });


            edtNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    if(!charSequence.toString().equalsIgnoreCase(""))
                    {

                        String amount=charSequence.toString();

                        double d=Double.parseDouble(amount);

                        mileStones.get(getAdapterPosition()).setAmount(d+"");

                    }
                    else{


                        mileStones.get(getAdapterPosition()).setAmount("0");
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!mileStones.get(getAdapterPosition()).getStart_date().equalsIgnoreCase(""))
                    {
                        if(!mileStones.get(getAdapterPosition()).getEnd_date().equalsIgnoreCase(""))
                        {

                            if(!mileStones.get(getAdapterPosition()).getAmount().equalsIgnoreCase(""))
                            {

                                if(mileStones.get(getAdapterPosition()).getSelected()==0)
                                {

                                    mileStones.get(getAdapterPosition()).setSelected(1);
                                    notifyDataSetChanged();


                                }





                            }
                            else{


                                Utils.showAlertWithSingle(context,"select end date",null);
                            }



                        }
                        else{


                            Utils.showAlertWithSingle(context,"select end date",null);
                        }



                    }
                    else{

                        Utils.showAlertWithSingle(context,"select start date",null);

                    }

                }
            });


            txtstartdatepick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setMileStoneDate(0,getAdapterPosition());
                }
            });

            txtenddatepick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setMileStoneDate(1,getAdapterPosition());
                }
            });

        }
    }


    @NonNull
    @Override
    public MileStoneHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_milestone,parent,false);


        return new MileStoneHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MileStoneHolder holder, int position) {

        if(mileStones.get(position).isIswarningexist())
        {
            holder.txtwarning.setText(mileStones.get(position).getWarningmessage());
            holder.txtwarning.setVisibility(View.VISIBLE);
        }
        else{

            holder.txtwarning.setVisibility(View.GONE);
            holder.txtwarning.setText("");
        }



        if(!mileStones.get(position).getStart_date().equalsIgnoreCase("")) {

            holder.txtstartdatepick.setText(mileStones.get(position).getStart_date());
        }
        else{

            holder.txtstartdatepick.setText("Start date");
        }

        if(!mileStones.get(position).getEnd_date().equalsIgnoreCase("")) {

            holder.txtenddatepick.setText(mileStones.get(position).getEnd_date());
        }
        else{

            holder.txtenddatepick.setText("End date");
        }

        holder.edtNumber.setText(mileStones.get(position).getAmount()+"");




        if(mileStones.get(position).getSelected()==1)
        {

           holder. layout_views.setVisibility(View.GONE);
           holder.txtMileStoneAdded.setVisibility(View.VISIBLE);
           holder.imgdropdown.setImageResource(R.drawable.ic_keyboard_arrow_right_);

            holder.txtMileStoneAdded.setText("Start date : "+mileStones.get(position).getStart_date()+"\nEnd date : "+mileStones.get(position).getEnd_date()+"\nAmount:"+mileStones.get(position).getAmount());

        }
        else{

            holder. layout_views.setVisibility(View.VISIBLE);
            holder.txtMileStoneAdded.setVisibility(View.GONE);

            holder.imgdropdown.setImageResource(R.drawable.ic_keyboard_arrow_down);

        }



    }

    @Override
    public int getItemCount() {
        return mileStones.size();
    }

    public List<MileStone>getMileStoneList()
    {
        return mileStones;
    }



    public void setMileStoneDate(int code,int position)
    {
        Calendar mCalender = Calendar.getInstance();
        int year = mCalender.get(Calendar.YEAR);
        int month = mCalender.get(Calendar.MONTH);
        int dayOfMonth = mCalender.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog=new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                String message="";
                boolean isvalid=true;

                Date stdate=null;
                Date eddate=null;
                SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

                try {


                    int m = i1 + 1;

                    if (code == 0) {

                        String startdate = i2 + "-" + m + "-" + i;


                        if(mileStones.size()>1)
                        {
                            int p=mileStones.size()-2;

                            MileStone mls=mileStones.get(p);

                            stdate = myFormat.parse(startdate);

                         eddate = myFormat.parse(mls.getEnd_date());


                         if(!mileStones.get(p).getEnd_date().equalsIgnoreCase(""))
                         {



                             if(stdate.after(eddate))
                             {

                                 mileStones.get(position).setIswarningexist(false);
                                 mileStones.get(position).setWarningmessage("");
                                 mileStones.get(position).setStart_date(startdate);
                                 notifyDataSetChanged();

                             }
                             else{

                                 isvalid=false;
                                 message="The start date should be greater than the end date of the previous milestone.";
                                 mileStones.get(position).setIswarningexist(true);
                                 mileStones.get(position).setWarningmessage(message);
                                 notifyDataSetChanged();

                             }




                         }
                         else{

                             isvalid=false;
                             message="Enter the end date in previous milestone";
                             mileStones.get(p).setIswarningexist(true);
                             mileStones.get(p).setWarningmessage(message);
                             notifyDataSetChanged();

                         }





                        }

                        else if(mileStones.size()==1)
                        {
                            int p=mileStones.size()-1;

                            MileStone mls=mileStones.get(p);




                            if(!mileStones.get(p).getStart_date().equalsIgnoreCase("")) {

                                if (!mileStones.get(p).getEnd_date().equalsIgnoreCase("")) {

                                    stdate = myFormat.parse(startdate);

                                    eddate = myFormat.parse(mls.getEnd_date());


                                    if (stdate.after(eddate)) {

                                        mileStones.get(position).setIswarningexist(false);
                                        mileStones.get(position).setWarningmessage("");
                                        mileStones.get(position).setStart_date(startdate);
                                        notifyDataSetChanged();

                                    } else {

                                        isvalid = false;
                                        message = "The start date should be greater than the end date of the previous milestone.";
                                        mileStones.get(position).setIswarningexist(true);
                                        mileStones.get(position).setWarningmessage(message);
                                        notifyDataSetChanged();

                                    }


                                } else {

                                    isvalid = false;
                                    message = "Enter the end date in previous milestone";
                                    mileStones.get(p).setIswarningexist(true);
                                    mileStones.get(p).setWarningmessage(message);
                                    notifyDataSetChanged();

                                }

                            }
                            else{

                                mileStones.get(p).setIswarningexist(false);
                                mileStones.get(p).setWarningmessage("");
                                mileStones.get(p).setStart_date(startdate);
                                notifyDataSetChanged();
                            }



                        }


                        else{

                            mileStones.get(position).setStart_date(startdate);

                            notifyDataSetChanged();

                        }






                    } else {

                        String endate = i2 + "-" + m + "-" + i;

                        if(!mileStones.get(position).getStart_date().equalsIgnoreCase("")&&!endate.equalsIgnoreCase(""))
                        {

                            Date stdate_compare = myFormat.parse(mileStones.get(position).getStart_date());

                            Date enddate_compare = myFormat.parse(endate);


                            if(stdate_compare.before(enddate_compare))
                            {

                                mileStones.get(position).setIswarningexist(false);
                                mileStones.get(position).setWarningmessage("");
                                mileStones.get(position).setEnd_date(endate);
                                notifyDataSetChanged();

                            }
                            else if(stdate_compare.equals(enddate_compare))
                            {

                                isvalid=false;
                                message="Start date and end date cannot be equal ";
                                mileStones.get(position).setIswarningexist(true);
                                mileStones.get(position).setWarningmessage(message);
                                notifyDataSetChanged();

                            }


                            else{
                                isvalid=false;
                                message="Select start date and end date properly  ";
                                mileStones.get(position).setIswarningexist(true);
                                mileStones.get(position).setWarningmessage(message);
                                notifyDataSetChanged();

                            }




                        }
                        else{

                            isvalid=false;
                            message="Select start date and end date";
                            mileStones.get(position).setIswarningexist(true);
                            mileStones.get(position).setWarningmessage(message);
                            notifyDataSetChanged();
                        }






                    }
                }catch (Exception e)
                {
                    isvalid=false;
                    message="Error occured";
                    mileStones.get(position).setIswarningexist(true);
                    mileStones.get(position).setWarningmessage(message);
                    notifyDataSetChanged();
                }


            }
        },year,month,dayOfMonth);

        datePickerDialog.show();

    }
}
