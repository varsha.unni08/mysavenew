package com.centroid.integraaccounts.app;

import java.util.concurrent.Executor;

public class DirectExecutor implements Executor {


    @Override
    public void execute(Runnable runnable) {

        runnable.run();

    }
}
