package com.centroid.integraaccounts.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.centroid.integraaccounts.Constants.LocaleHelper;
import com.centroid.integraaccounts.services.ReminderServices;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

public class IntegraApp extends Application {




    @Override
    public void onCreate() {
        super.onCreate();

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            getApplicationContext().startForegroundService(new Intent(getApplicationContext(), ReminderServices.class));
//        } else {
//            getApplicationContext().startService(new Intent(getApplicationContext(), ReminderServices.class));
//        }

     //   startService(new Intent(getApplicationContext(), ReminderServices.class));

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {


            @Override

            public void uncaughtException(Thread thread, Throwable ex) {

                handleUncaughtException(thread, ex);

            }

        });







//        try {
//            // Google Play will install latest OpenSSL
//            ProviderInstaller.installIfNeeded(getApplicationContext());
//            SSLContext sslContext;
//            sslContext = SSLContext.getInstance("TLSv1.2");
//            sslContext.init(null, null, null);
//            sslContext.createSSLEngine();
//        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
//                | NoSuchAlgorithmException | KeyManagementException e) {
//            e.printStackTrace();
//        }
    }



    public void handleUncaughtException (Thread thread, Throwable e)

    {

        String stackTrace = Log.getStackTraceString(e);

        String message = e.getMessage();





        Intent intent = new Intent (Intent.ACTION_SEND);

        intent.setType("message/rfc822");

        intent.putExtra (Intent.EXTRA_EMAIL, new String[] {"varsha.unni08@gmail.com","antonykj623@gmail.com"});

        intent.putExtra (Intent.EXTRA_SUBJECT, "MyApp Crash log file");

        intent.putExtra (Intent.EXTRA_TEXT, stackTrace);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application

        startActivity(intent);





    }

}
