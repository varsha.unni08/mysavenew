package com.centroid.integraaccounts.app;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Div;
import com.itextpdf.layout.layout.LayoutArea;
import com.itextpdf.layout.layout.LayoutResult;
import com.itextpdf.layout.layout.RootLayoutArea;
import com.itextpdf.layout.renderer.DivRenderer;
import com.itextpdf.layout.renderer.DocumentRenderer;

public class CustomDocumentRenderer extends DocumentRenderer {


    public CustomDocumentRenderer(Document document) {
        super(document);
    }

    @Override
    protected LayoutArea updateCurrentArea(LayoutResult overflowResult) {
        LayoutArea area = super.updateCurrentArea(overflowResult); // margins are applied on this level
        Rectangle newBBox = area.getBBox().clone();

        // apply border
        float[] borderWidths = {5, 5, 5, 5};
        newBBox.applyMargins(borderWidths[0], borderWidths[1], borderWidths[2], borderWidths[3], false);

        // this div will be added as a background
        Div div = new Div()
                .setWidth(newBBox.getWidth())
                .setHeight(newBBox.getHeight())
                .setBorder(new SolidBorder(5))
                .setBackgroundColor(ColorConstants.WHITE);
        addChild(new DivRenderer(div));

        // apply padding
        float[] paddingWidths = {20, 20, 20, 20};
        newBBox.applyMargins(paddingWidths[0], paddingWidths[1], paddingWidths[2], paddingWidths[3], false);

        return (currentArea = new RootLayoutArea(area.getPageNumber(), newBBox));
    }
}
