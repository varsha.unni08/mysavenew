package com.centroid.integraaccounts.OpenPay;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.centroid.integraaccounts.Constants.Utils;
import com.centroid.integraaccounts.R;
import com.centroid.integraaccounts.paymentdata.domain.CashFreeTransactionData;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PaymentWebViewActivity extends AppCompatActivity {

    WebView wv1;

    String name ="",email="",phone="",amount="",id_transaction="";
    String res = "";

//    String responseurl="https://mysaveapp.com/rechargeAPI/paymentgateway/dataform.php";

    String responseurl="https://mysaveapp.com/rechargeAPINew/paymentgateway/dataform.php";


    String resulturl="https://mysaveapp.com/rechargeAPINew/paymentgateway/result.php";

    String url="",requestid="",spkey="",operator="",rechargeamount="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view);
        getSupportActionBar().hide();

        Intent i=getIntent();


        name=i.getStringExtra("name");
        email=i.getStringExtra("email");
        phone=i.getStringExtra("phone");
        amount=i.getStringExtra("amount");

        requestid=i.getStringExtra("requestid");
        spkey=i.getStringExtra("spkey");
        operator=i.getStringExtra("operator");
        rechargeamount=i.getStringExtra("rechargeamount");
        id_transaction=i.getStringExtra("id_transaction");


        url="https://mysaveapp.com/rechargeAPINew/paymentgateway/dataform.php?id_transaction="+id_transaction+"&name="+name+"&email="+email+"&phone="+phone+"&amount="+amount+"&timestamp="+ Utils.getTimestamp()+"&requestid="+requestid+"&spkey="+spkey+"&operator="+operator+"&rechargeamount="+rechargeamount;


   //     url="https://mysaveapp.com/rechargeAPI/paymentgateway/dataform.php?id_transaction="+id_transaction+"name="+name+"&email="+email+"&phone="+phone+"&amount="+amount+"&timestamp="+ Utils.getTimestamp()+"&requestid="+requestid+"&spkey="+spkey+"&operator="+operator+"&rechargeamount="+rechargeamount;


        wv1=findViewById(R.id.webview);

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setSupportMultipleWindows(true);
        wv1.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.setWebViewClient(new MyBrowser());
       // wv1.setWebChromeClient(new MyBrowserChromeClient());
        wv1.loadUrl(url);


    }

//    private class MyBrowserChromeClient extends WebChromeClient {
//
//        @Override
//        public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result)
//        {
//            Log.e("alert url", url);
//           // Toast.makeText(PaymentWebViewActivity.this, message, Toast.LENGTH_SHORT).show();
//            //result.confirm();
//            return true;
//        };
//
//
//    }

    private class MyBrowser extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.e("TAAG",url);

            if(url.contains(resulturl))
            {

                getResponseFromPayment(url);

            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {



                view.loadUrl(url);



            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {





        }
    }


    public void getResponseFromPayment(String urldata)
    {



        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {


                try{

                    StringBuilder response  = new StringBuilder();

                    URL url = new URL(urldata);
                    HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
                    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
                    {
                        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
                        String strLine = null;
                        while ((strLine = input.readLine()) != null)
                        {
                            response.append(strLine);
                        }
                        input.close();
                    }
                    else{


                    }
                     res = response.toString();

                    Log.e("Response From Payment", res);


                }catch (Exception e)
                {

                    res ="";
                    Log.e("Response From Payment","error");
                }

                if(res!=null)
                {

                    if(!res.isEmpty())
                    {

                        String d[]=res.trim().split(":");

                        if(d.length>0)
                        {
                            if(d[0].contains("Your transaction is successful")) {

                                String a = d[1];

//                                String b[] = a.split(":");

                                if(!a.isEmpty())
                                {
                                    String transactionid=a;

                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("result",transactionid);
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();

                                }

                            }
                            else
                            {
                                Intent returnIntent = new Intent();

                                setResult(Activity.RESULT_CANCELED,returnIntent);
                                finish();

                            }


//                            else{
//
//                                Intent returnIntent = new Intent();
//
//                                setResult(Activity.RESULT_CANCELED,returnIntent);
//                                finish();
//
//                            }

                        }


                    }

                }




            }
        });

    }


}