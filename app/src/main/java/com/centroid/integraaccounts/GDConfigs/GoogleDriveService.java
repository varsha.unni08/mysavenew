package com.centroid.integraaccounts.GDConfigs;

import android.content.Context;

public class GoogleDriveService {

    Context activity;
    GoogleDriveConfig config;

    public GoogleDriveService() {
    }

    public Context getActivity() {
        return activity;
    }

    public void setActivity(Context activity) {
        this.activity = activity;
    }

    public GoogleDriveConfig getConfig() {
        return config;
    }

    public void setConfig(GoogleDriveConfig config) {
        this.config = config;
    }
}
