package com.centroid.integraaccounts.GDConfigs;

import java.io.File;

public interface ServiceListener {

    void loggedIn(); ;//1
    void fileDownloaded(File f); //2
    void cancelled(); //3
    void handleError(Exception e);
}
