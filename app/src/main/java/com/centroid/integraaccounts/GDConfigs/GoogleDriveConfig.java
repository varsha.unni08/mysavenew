package com.centroid.integraaccounts.GDConfigs;

import java.util.List;

public class GoogleDriveConfig {

    String activityTitle = null;
    List<String> mimeTypes = null;

    public GoogleDriveConfig() {
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public List<String> getMimeTypes() {
        return mimeTypes;
    }

    public void setMimeTypes(List<String> mimeTypes) {
        this.mimeTypes = mimeTypes;
    }
}

