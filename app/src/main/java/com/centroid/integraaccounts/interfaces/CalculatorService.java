package com.centroid.integraaccounts.interfaces;

public interface CalculatorService {

    void setAmount(double amount);
}
