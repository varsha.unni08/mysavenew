package com.centroid.integraaccounts.interfaces;

import com.centroid.integraaccounts.paymentdata.domain.TargetCategory;

public interface TargetCategoryServices {


    public void onSelectTargetCategory(TargetCategory tgt);
}
