package com.centroid.integraaccounts.interfaces;

import com.centroid.integraaccounts.data.domain.CommonData;

public interface AccountSetupEventListener {


    public void getSelectedAccountSetup(CommonData commonData);
}
