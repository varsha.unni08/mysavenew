package com.centroid.integraaccounts.interfaces;

import com.centroid.integraaccounts.data.domain.MileStone;

import java.util.List;

public interface MileStoneListener {


    public void onMileStoneCompleted(MileStone mileStones);
}
