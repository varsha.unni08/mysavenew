package com.centroid.integraaccounts.interfaces;

import com.centroid.integraaccounts.data.domain.CommonData;

public interface InvestMentListener {

    public void onSelectInvestment(CommonData cmd);
}
