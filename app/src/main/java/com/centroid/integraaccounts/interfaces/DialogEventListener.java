package com.centroid.integraaccounts.interfaces;

public interface DialogEventListener {


     void onPositiveButtonClicked();

     void onNegativeButtonClicked();

}
